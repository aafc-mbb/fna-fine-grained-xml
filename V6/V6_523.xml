<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alcea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 689. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species alcea;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250101087</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alcea</taxon_name>
    <taxon_name authority="(Cavanilles) K. Koch" date="unknown" rank="variety">fastigiata</taxon_name>
    <taxon_hierarchy>genus Malva;species alcea;variety fastigiata</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Hollyhock or Vervain mallow</other_name>
  <other_name type="common_name">mauve alcée</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.3–1.3 m, usually stellate-canescent.</text>
      <biological_entity id="o6871" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="1.3" to_unit="m" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="stellate-canescent" value_original="stellate-canescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely hirsute proximally, stellate-hairy distally, hairs often pustulose.</text>
      <biological_entity id="o6872" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o6873" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="relief" src="d0_s1" value="pustulose" value_original="pustulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules deciduous, lanceolate, slightly falcate, 5 (–10) × 1–2.5 mm, ciliate;</text>
      <biological_entity id="o6874" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6875" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="10" to_unit="mm" />
        <character name="length" src="d0_s2" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles of lower leaves 1.5–2.5 times as long as blade, reduced distally to 1/2 blade length, stellate-hairy;</text>
      <biological_entity id="o6876" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6877" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o6879" is_modifier="false" name="length" src="d0_s3" value="1.5-2.5 times as long as blade" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o6878" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6879" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o6880" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o6877" id="r774" name="part_of" negation="false" src="d0_s3" to="o6878" />
    </statement>
    <statement id="d0_s4">
      <text>blade 2–8 × 2–8 cm, base cordate to somewhat truncate, those most distal sometimes wide-cuneate, surfaces stellate-hairy, proximal leaf-blades cordate-orbiculate, margins crenate to dentate, shallowly lobed, apex rounded, distal leaf-blades deeply (3–) 5-lobed, lobe margins obtusely dentate or pinnatifid, apex narrowly acute.</text>
      <biological_entity id="o6881" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6882" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6883" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate to somewhat" value_original="cordate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6884" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="wide-cuneate" value_original="wide-cuneate" />
      </biological_entity>
      <biological_entity id="o6885" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6886" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate-orbiculate" value_original="cordate-orbiculate" />
      </biological_entity>
      <biological_entity id="o6887" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="dentate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o6888" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6889" name="leaf-blade" name_original="leaf-blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="(3-)5-lobed" value_original="(3-)5-lobed" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o6890" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o6891" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary or distal flowers in racemes.</text>
      <biological_entity id="o6892" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6894" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o6895" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
      <relation from="o6894" id="r775" name="in" negation="false" src="d0_s5" to="o6895" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels conspicuously jointed distally, 1.4–2 cm, not much longer in fruit;</text>
      <biological_entity id="o6897" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets distinct, not adnate to calyx, ovate or ovate-deltate to obovate, narrowed to base, 5–8 (–12) × 2.5 (–5) mm, shorter than calyx, margins entire, surfaces stellate-hairy or glabrate.</text>
      <biological_entity id="o6896" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="conspicuously; distally" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
        <character constraint="in fruit" constraintid="o6897" is_modifier="false" modifier="not much" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6898" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="to calyx" constraintid="o6899" is_modifier="false" modifier="not" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="ovate-deltate" name="shape" notes="" src="d0_s7" to="obovate" />
        <character constraint="to base" constraintid="o6900" is_modifier="false" name="shape" src="d0_s7" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s7" to="5" to_unit="mm" />
        <character name="width" notes="" src="d0_s7" unit="mm" value="2.5" value_original="2.5" />
        <character constraint="than calyx" constraintid="o6901" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6899" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o6900" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o6901" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o6902" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6903" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 9–12 (–15) mm, lobes enclosing mericarps, stellate-hairy;</text>
      <biological_entity id="o6904" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6905" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6906" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o6907" name="mericarp" name_original="mericarps" src="d0_s8" type="structure" />
      <relation from="o6906" id="r776" name="enclosing" negation="false" src="d0_s8" to="o6907" />
    </statement>
    <statement id="d0_s9">
      <text>petals usually bright pink, rarely white, 20–35 mm, length 2.5–3 times calyx;</text>
      <biological_entity id="o6908" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6909" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="bright pink" value_original="bright pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character constraint="calyx" constraintid="o6910" is_modifier="false" name="length" src="d0_s9" value="2.5-3 times calyx" value_original="2.5-3 times calyx" />
      </biological_entity>
      <biological_entity id="o6910" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminal column 9–10 mm, sparsely stellate-hairy;</text>
      <biological_entity id="o6911" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o6912" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 18–20-branched;</text>
      <biological_entity id="o6913" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6914" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="18-20-branched" value_original="18-20-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 18–20.</text>
      <biological_entity id="o6915" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6916" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Schizocarps 4–8 mm diam.;</text>
      <biological_entity id="o6917" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 18–20, black, 2.4–2.8 mm, apical surface and margins rounded, smooth or faintly ridged, glabrous or sparsely hairy.</text>
      <biological_entity id="o6918" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s14" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s14" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6919" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="faintly" name="shape" src="d0_s14" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="apical" id="o6920" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="faintly" name="shape" src="d0_s14" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds brown, 2.5 mm. 2n = 84.</text>
      <biological_entity id="o6921" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6922" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides, old farm sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="old farm sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., N.S., Ont., Que., Sask.; Conn., Idaho, Ind., Maine, Mass., Mich., N.H., N.J., N.Y., Ohio, Pa., R.I., Vt., Wash., Wis.; Europe; w Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva alcea is found in most of Europe, but is rare in the Mediterranean region, and barely extends into Turkey in western Asia. The leaf shape, indument, and shape and size of the petals are variable, the most extreme forms having deeply 2-fid petals and deeply divided distal leaves with narrow, almost simple lobes. It occasionally hybridizes with M. sylvestris (Malva ×egarensis Cadevall) and M. moschata (Malva ×intermedia Boreau).</discussion>
  <discussion>Malva alcea is sparingly naturalized in North America, primarily in New England and around the Great Lakes into eastern Canada; it is sometimes cultivated as an ornamental and naturalizes locally.</discussion>
  
</bio:treatment>