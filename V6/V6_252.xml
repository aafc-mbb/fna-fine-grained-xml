<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 14. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species pinetorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100937</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(Greene) GreeneE" date="unknown" rank="variety">pinetorum</taxon_name>
    <taxon_hierarchy>genus Viola;species purpurea;variety pinetorum</taxon_hierarchy>
  </taxon_identification>
  <number>46.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 3–18 cm, cespitose or not.</text>
      <biological_entity id="o1790" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" src="d0_s0" value="not" value_original="not" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, prostrate or erect, leafy proximally and distally, puberulent or canescent to gray-tomentose, sometimes glabrous, on caudex from subligneous rhizome.</text>
      <biological_entity id="o1791" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="canescent" modifier="distally" name="pubescence" src="d0_s1" to="gray-tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1792" name="caudex" name_original="caudex" src="d0_s1" type="structure" />
      <biological_entity id="o1793" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
      </biological_entity>
      <relation from="o1791" id="r199" name="on" negation="false" src="d0_s1" to="o1792" />
      <relation from="o1792" id="r200" name="from" negation="false" src="d0_s1" to="o1793" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o1794" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 1–4;</text>
      <biological_entity constraint="basal" id="o1795" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules adnate to petiole forming 2 linearlanceolate wings, margins entire or laciniate, apex of each wing free, tips usually filamentous;</text>
      <biological_entity constraint="basal" id="o1796" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1797" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="to petiole" constraintid="o1798" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o1798" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o1799" name="wing" name_original="wings" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1800" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o1801" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o1802" name="wing" name_original="wing" src="d0_s4" type="structure" />
      <biological_entity id="o1803" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s4" value="filamentous" value_original="filamentous" />
      </biological_entity>
      <relation from="o1798" id="r201" name="forming" negation="false" src="d0_s4" to="o1799" />
      <relation from="o1801" id="r202" name="part_of" negation="false" src="d0_s4" to="o1802" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 2.3–9.5 cm, puberulent or canescent;</text>
      <biological_entity constraint="basal" id="o1804" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o1805" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.3" from_unit="cm" name="some_measurement" src="d0_s5" to="9.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade purple-tinted abaxially or not, usually linear to narrowly lanceolate, oblanceolate or obovate, or lanceolate-elliptic, rarely ovate, 1.3–5 × 0.3–2.5 cm, base attenuate, margins usually lacerate, dentate, or serrate, sometimes entire, usually undulate, ciliate, apex acute, mucronulate, surfaces puberulent to canescent or gray-tomentose;</text>
      <biological_entity constraint="basal" id="o1806" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o1807" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially; abaxially; not" name="coloration" src="d0_s6" value="purple-tinted" value_original="purple-tinted" />
        <character char_type="range_value" from="usually linear" name="shape" src="d0_s6" to="narrowly lanceolate oblanceolate or obovate or lanceolate-elliptic" />
        <character char_type="range_value" from="usually linear" name="shape" src="d0_s6" to="narrowly lanceolate oblanceolate or obovate or lanceolate-elliptic" />
        <character char_type="range_value" from="usually linear" name="shape" src="d0_s6" to="narrowly lanceolate oblanceolate or obovate or lanceolate-elliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1808" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o1809" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1810" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o1811" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s6" to="canescent or gray-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: stipules lanceolate, oblanceolate, or linear-oblong, margins entire or lacerate, apex acute to acuminate;</text>
      <biological_entity constraint="cauline" id="o1812" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1813" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o1814" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1815" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o1816" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
      <relation from="o1812" id="r203" name="to" negation="false" src="d0_s7" to="o1813" />
      <relation from="o1813" id="r204" name="except" negation="false" src="d0_s7" to="o1814" />
    </statement>
    <statement id="d0_s8">
      <text>petiole 0.9–8.3 cm;</text>
      <biological_entity constraint="cauline" id="o1817" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o1818" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o1819" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s8" to="8.3" to_unit="cm" />
      </biological_entity>
      <relation from="o1817" id="r205" name="to" negation="false" src="d0_s8" to="o1818" />
      <relation from="o1818" id="r206" name="except" negation="false" src="d0_s8" to="o1819" />
    </statement>
    <statement id="d0_s9">
      <text>blade 2.8–9.6 × 0.3–1.4 cm, length 4–11 times width.</text>
      <biological_entity constraint="cauline" id="o1820" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="4-11" value_original="4-11" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1821" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o1822" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" src="d0_s9" to="9.6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s9" to="1.4" to_unit="cm" />
      </biological_entity>
      <relation from="o1820" id="r207" name="to" negation="false" src="d0_s9" to="o1821" />
      <relation from="o1821" id="r208" name="except" negation="false" src="d0_s9" to="o1822" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 2.9–11.5 cm, puberulent or canescent.</text>
      <biological_entity id="o1823" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.9" from_unit="cm" name="some_measurement" src="d0_s10" to="11.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals lanceolate, margins ciliate, auricles 0.5–1 mm;</text>
      <biological_entity id="o1824" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1825" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1826" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1827" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals deep lemon-yellow adaxially, upper 2 red to purple-brown abaxially, lower 3 dark brown-veined, lateral 2 bearded, lowest 5–12 mm, spur color same as petals, gibbous, 1.5–3 mm;</text>
      <biological_entity id="o1828" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1829" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s12" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="red" modifier="abaxially" name="coloration" src="d0_s12" to="purple-brown" />
        <character is_modifier="false" name="position" src="d0_s12" value="lower" value_original="lower" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark brown-veined" value_original="dark brown-veined" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1830" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1831" name="petal" name_original="petals" src="d0_s12" type="structure" />
      <relation from="o1830" id="r209" name="as" negation="false" src="d0_s12" to="o1831" />
    </statement>
    <statement id="d0_s13">
      <text>style head bearded;</text>
      <biological_entity id="o1832" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="style" id="o1833" name="head" name_original="head" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>cleistogamous flowers axillary.</text>
      <biological_entity id="o1834" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1835" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="position" src="d0_s14" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules ovoid, 3.5–7 mm, puberulent.</text>
      <biological_entity id="o1836" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds medium to dark-brown, 2–3.5 mm. 2n = 12.</text>
      <biological_entity id="o1837" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="medium" value_original="medium" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1838" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Flowers of Viola pinetorum have been observed to close up in late afternoon then fully reopen the following morning.</discussion>
  <discussion>Although E. O. Wooton and P. C. Standley (1915) reported Viola pinetorum from New Mexico, the plant was probably V. nuttallii. K. W. Allred (2008) noted that V. pinetorum occurs in California; he did not recognize it in New Mexico.</discussion>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 6.5–18 cm, not cespitose; basal leaf blades 0.7–2.5 cm wide, surfaces puberulent; peduncles 3.4–11.5 cm.</description>
      <determination>46a Viola pinetorum var. pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 3–7(–9) cm, usually cespitose; basal leaf blades 0.3–1 cm wide, surfaces canescent, sometimes appearing gray-tomentose; peduncles 2.9–6(–7) cm.</description>
      <determination>46b Viola pinetorum var. grisea</determination>
    </key_statement>
  </key>
</bio:treatment>