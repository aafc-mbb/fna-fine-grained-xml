<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lechea</taxon_name>
    <taxon_name authority="Hodgdon" date="unknown" rank="species">mensalis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>40: 92, plate 489, figs. 1 – 4. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus lechea;species mensalis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250101228</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Mountain pinweed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o18292" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: basal produced;</text>
      <biological_entity id="o18293" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18294" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>flowering erect, 15–25 cm, sericeous.</text>
      <biological_entity id="o18295" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves of flowering-stems alternate;</text>
      <biological_entity id="o18296" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o18297" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure" />
      <relation from="o18296" id="r1940" name="part_of" negation="false" src="d0_s3" to="o18297" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear, 8–15 × 0.7–1.2 mm, apex acute to rounded, abaxial surface sparsely pilose on midvein and margins, adaxial glabrous.</text>
      <biological_entity id="o18298" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18299" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18300" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="on margins" constraintid="o18302" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o18301" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o18302" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o18303" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1 per axil, 1.5–2 mm.</text>
      <biological_entity id="o18304" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="per axil" constraintid="o18305" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18305" name="axil" name_original="axil" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 1.9–2 mm, outer sepals longer than inner.</text>
      <biological_entity id="o18306" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18307" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="outer" id="o18308" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="than inner sepals" constraintid="o18309" is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="inner" id="o18309" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules ovoid, 1.6–1.8 × 1.2–1.3 mm, ± equaling calyx.</text>
      <biological_entity id="o18310" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s7" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18311" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1 (2).</text>
      <biological_entity id="o18312" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer; fruiting fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Lechea mensalis is known from open woodlands dominated by species of oak and juniper at only two locations in the United States: the Guadalupe Mountains of southeastern New Mexico and adjacent Texas and the Chisos Mountains of west Texas; it also occurs in adjacent Coahuila, Mexico.</discussion>
  
</bio:treatment>