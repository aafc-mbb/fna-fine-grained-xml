<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">114</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Langsdorff ex Gingins in A. P. de Candolle and A. L. P. P. de Candolle" date="unknown" rank="species">japonica</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>1: 295. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species japonica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242354614</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Japanese violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, acaulescent, not stoloniferous, 3–10 cm;</text>
      <biological_entity id="o14321" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome thick, fleshy.</text>
      <biological_entity id="o14322" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, ca. 5, ascending to erect;</text>
      <biological_entity id="o14323" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character name="quantity" src="d0_s2" value="5" value_original="5" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules ± oblong, 2-fid, proximal margins entire, distal ± serrate, apex acuminate;</text>
      <biological_entity id="o14324" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14325" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14326" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o14327" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole narrowly winged distally, 1–14 cm, usually glabrous;</text>
      <biological_entity id="o14328" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly; distally" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade unlobed, broadly ovate or triangular-ovate, 3–8 × 3–5.5 cm, base cordate, margins crenate, usually eciliate, apex acute or ± obtuse, surfaces sparsely puberulent.</text>
      <biological_entity id="o14329" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular-ovate" value_original="triangular-ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s5" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14330" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o14331" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o14332" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o14333" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3–6 cm, glabrous or pubescent, bracteoles near middle.</text>
      <biological_entity id="o14334" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o14335" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure" />
      <biological_entity id="o14336" name="middle" name_original="middle" src="d0_s6" type="structure" />
      <relation from="o14335" id="r1564" name="near" negation="false" src="d0_s6" to="o14336" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals broadly lanceolate, margins eciliate, auricles 6–8 mm;</text>
      <biological_entity id="o14337" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14338" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o14339" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o14340" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals light violet or whitish violet on both surfaces, lowest 3 occasionally white basally, often dark violet-veined, lateral 2 sparsely bearded or beardless, lowest 17–20 mm, spur pale to dark violet, elongated, 5–10 mm;</text>
      <biological_entity id="o14341" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o14342" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="light violet" value_original="light violet" />
        <character constraint="on surfaces" constraintid="o14343" is_modifier="false" name="coloration" src="d0_s8" value="whitish violet" value_original="whitish violet" />
        <character is_modifier="false" name="position" notes="" src="d0_s8" value="lowest" value_original="lowest" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" modifier="occasionally; basally" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="dark violet-veined" value_original="dark violet-veined" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="beardless" value_original="beardless" />
        <character is_modifier="false" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14343" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o14344" name="spur" name_original="spur" src="d0_s8" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s8" to="dark violet" />
        <character is_modifier="false" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style head beardless;</text>
      <biological_entity id="o14345" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="style" id="o14346" name="head" name_original="head" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="beardless" value_original="beardless" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cleistogamous flowers present.</text>
      <biological_entity id="o14347" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14348" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ellipsoid, 8–10 mm, glabrous.</text>
      <biological_entity id="o14349" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds unknown.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 48.</text>
      <biological_entity constraint="2n" id="o14351" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="48" value_original="48" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="o14350" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s14" value="flowering" value_original="flowering" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Gardens and ruderal areas;</text>
    </statement>
    <statement id="d0_s16">
      <text>10–50 m;</text>
    </statement>
    <statement id="d0_s17">
      <text>introduced;</text>
      <biological_entity id="o14352" name="area" name_original="areas" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="m" name="some_measurement" src="d0_s16" to="50" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Mass.;</text>
    </statement>
    <statement id="d0_s19">
      <text>Asia (China, Japan, Korea).</text>
      <biological_entity id="o14353" name="mass" name_original="mass" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gardens and ruderal areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gardens" />
        <character name="habitat" value="ruderal areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mass.; Asia (China, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>