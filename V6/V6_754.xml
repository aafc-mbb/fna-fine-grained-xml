<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">399</other_info_on_meta>
    <other_info_on_meta type="mention_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="illustration_page">396</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">hudsonia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 5. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus hudsonia;species tomentosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101240</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hudsonia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">ericoides</taxon_name>
    <taxon_name authority="(Nuttall) N. H. Nickerson &amp; J. E. Skog" date="unknown" rank="subspecies">tomentosa</taxon_name>
    <taxon_hierarchy>genus Hudsonia;species ericoides;subspecies tomentosa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 40 cm.</text>
      <biological_entity id="o18216" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually appressed (to stems);</text>
      <biological_entity id="o18217" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade mostly lanceolate-ovate, scalelike, 1–2 (–3+) mm, surfaces densely sericeous to tomentose.</text>
      <biological_entity id="o18218" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s2" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18219" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character char_type="range_value" from="densely sericeous" name="pubescence" src="d0_s2" to="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels 0–1 (–5) mm.</text>
      <biological_entity id="o18220" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepal apices rounded, sometimes mucronate, retuse, or 2-fid;</text>
      <biological_entity id="o18221" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="sepal" id="o18222" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals yellow;</text>
      <biological_entity id="o18223" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18224" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovaries glabrous or glabrescent.</text>
      <biological_entity id="o18225" name="flower" name_original="flowers" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 20.</text>
      <biological_entity id="o18226" name="ovary" name_original="ovaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18227" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly open, sandy sites, beaches, dunes, swales, pine barrens, pine-oak woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sites" modifier="mostly open" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="pine-oak woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.B., Nfld. and Labr. (Labr.), N.W.T., N.S., Ont., P.E.I., Que., Sask.; Conn., Del., Ill., Ind., Iowa, Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., N.Dak., Ohio, R.I., S.C., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Some plants included here in Hudsonia tomentosa are intermediate to plants typical of H. ericoides for some traits. They may be hybrids or backcrosses.</discussion>
  
</bio:treatment>