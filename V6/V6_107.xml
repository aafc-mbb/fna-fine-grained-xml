<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Norman K. B. Robson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">69</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">CLUSIACEAE</taxon_name>
    <taxon_hierarchy>family CLUSIACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10203</other_info_on_name>
  </taxon_identification>
  <number>0</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees [lianas], evergreen, usually glabrous.</text>
      <biological_entity id="o5814" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves decussate [whorled], estipulate;</text>
      <biological_entity id="o5816" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="decussate" value_original="decussate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="estipulate" value_original="estipulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole usually present;</text>
      <biological_entity id="o5817" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade margins entire;</text>
      <biological_entity constraint="blade" id="o5818" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>glandular canals not containing hypericin or pseudohypericin.</text>
      <biological_entity constraint="glandular" id="o5819" name="canal" name_original="canals" src="d0_s4" type="structure" />
      <biological_entity id="o5820" name="pseudohypericin" name_original="pseudohypericin" src="d0_s4" type="substance" />
      <relation from="o5819" id="r661" name="containing" negation="false" src="d0_s4" to="o5820" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, dichasial, thyrsoid, or corymbiform or flowers solitary.</text>
      <biological_entity id="o5821" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="dichasial" value_original="dichasial" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thyrsoid" value_original="thyrsoid" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="thyrsoid" value_original="thyrsoid" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <biological_entity id="o5822" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels absent or relatively stout.</text>
      <biological_entity id="o5823" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="relatively" name="fragility_or_size" src="d0_s6" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual [bisexual], actinomorphic, hypogynous;</text>
      <biological_entity id="o5824" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" name="position" src="d0_s7" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals persistent, [2–] 4 [–14], imbricate or decussate;</text>
      <biological_entity id="o5825" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="14" />
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="decussate" value_original="decussate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals persistent [deciduous], [3–] 6 (–8) [–14], without ventral scale;</text>
      <biological_entity constraint="ventral" id="o5827" name="scale" name_original="scale" src="d0_s9" type="structure" />
      <relation from="o5826" id="r662" name="without" negation="false" src="d0_s9" to="o5827" />
    </statement>
    <statement id="d0_s10">
      <text>[stamens distinct or fasciculate; filaments nearly as wide as anthers; anthers usually dehiscing longitudinally, sometimes locellate, eglandular];</text>
      <biological_entity id="o5826" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s9" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="8" />
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistils 1;</text>
      <biological_entity id="o5828" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary superior, 3–12-locular;</text>
    </statement>
    <statement id="d0_s13">
      <text>placentation axile;</text>
      <biological_entity id="o5829" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="superior" value_original="superior" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-12-locular" value_original="3-12-locular" />
        <character is_modifier="false" name="placentation" src="d0_s13" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 1–6+ per locule, anatropous, bitegmic, tenuinucellate;</text>
      <biological_entity id="o5830" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o5831" from="1" name="quantity" src="d0_s14" to="6" upper_restricted="false" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s14" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="bitegmic" value_original="bitegmic" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="tenuinucellate" value_original="tenuinucellate" />
      </biological_entity>
      <biological_entity id="o5831" name="locule" name_original="locule" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>styles absent [relatively short];</text>
      <biological_entity id="o5832" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigmas [3–] 6–9 (–12).</text>
      <biological_entity id="o5833" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s16" to="6" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="12" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s16" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits capsular [berrylike, drupelike], dehiscence septifragal.</text>
      <biological_entity id="o5834" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="septifragal" value_original="septifragal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds arillate, embryo usually green, straight, cotyledons minute [absent], endosperm absent.</text>
      <biological_entity id="o5835" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o5836" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s18" value="green" value_original="green" />
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o5837" name="cotyledon" name_original="cotyledons" src="d0_s18" type="structure">
        <character is_modifier="false" name="size" src="d0_s18" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o5838" name="endosperm" name_original="endosperm" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, Asia, Africa, Indian Ocean Islands (Madagascar); pantropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
        <character name="distribution" value="pantropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 15, species ca. 700 (1 in the flora).</discussion>
  <references>
    <reference>Stevens, P. F. 2007. Clusiaceae—Guttiferae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 10+ vols. Berlin etc. Vol. 9, pp. 48–66.</reference>
  </references>
  
</bio:treatment>