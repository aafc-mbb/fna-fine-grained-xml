<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">344</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="unknown" rank="species">malviflora</taxon_name>
    <taxon_name authority="(Eastwood) Wiggins in L. Abrams and R. S. Ferris" date="unknown" rank="subspecies">rostrata</taxon_name>
    <place_of_publication>
      <publication_title>in L. Abrams and R. S. Ferris, Ill. Fl. Pacific States</publication_title>
      <place_in_publication>3: 105. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species malviflora;subspecies rostrata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101146</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sidalcea</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">rostrata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 80. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sidalcea;species rostrata</taxon_hierarchy>
  </taxon_identification>
  <number>19g.</number>
  <other_name type="common_name">Sea-cliff checkerbloom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (0.2–) 0.3–0.5 (–0.6) m, with short, thick, rather woody taproot or caudex, without rooting rhizomes.</text>
      <biological_entity id="o16818" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.2" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.3" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="0.5" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16819" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character is_modifier="true" modifier="rather" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o16820" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o16821" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <relation from="o16818" id="r1789" name="with" negation="false" src="d0_s0" to="o16819" />
      <relation from="o16818" id="r1790" name="with" negation="false" src="d0_s0" to="o16820" />
      <relation from="o16818" id="r1791" name="without" negation="false" src="d0_s0" to="o16821" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or procumbent, base decumbent, usually not rooting, usually coarsely, densely, softly bristly-tomentose or long stellate-hairy, basal hairs 1–2 mm.</text>
      <biological_entity id="o16822" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
      </biological_entity>
      <biological_entity id="o16823" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" modifier="usually coarsely; coarsely; densely; softly" name="pubescence" src="d0_s1" value="bristly-tomentose" value_original="bristly-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16824" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o16825" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules proximal sometimes purplish, distal green, wide-lanceolate to ovate, 7–8 × 2–4 mm;</text>
      <biological_entity id="o16826" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16827" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" from="wide-lanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles of basal leaves 6–15 cm, 2 times as long as blade, reduced distally, much less than 1/2 times as long as blade, distalmost blades subsessile;</text>
      <biological_entity id="o16828" name="petiole" name_original="petioles" src="d0_s4" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character constraint="blade" constraintid="o16830" is_modifier="false" name="length" src="d0_s4" value="2 times as long as blade" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="blade" constraintid="o16831" is_modifier="false" name="length" src="d0_s4" value="0-1/2 times as long as blade" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16829" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16830" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o16831" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity constraint="distalmost" id="o16832" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <relation from="o16828" id="r1792" name="part_of" negation="false" src="d0_s4" to="o16829" />
    </statement>
    <statement id="d0_s5">
      <text>blade often reniform, unlobed, 2–6 cm wide, margins crenate, sometimes deeply so, surfaces densely, softly or coarsely bristly-hairy.</text>
      <biological_entity id="o16833" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16834" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o16835" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes deeply; deeply; softly; coarsely" name="pubescence" src="d0_s5" value="bristly-hairy" value_original="bristly-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences spicate to subcapitate, usually dense, unbranched, often 10+-flowered, often 4–7 cm, flowers mostly open at same time and obviously overlapping, rarely not overlapping, proximalmost in axils of reduced leaves, fruits usually spaced and not overlapping on a somewhat elongated axis;</text>
      <biological_entity id="o16836" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character char_type="range_value" from="spicate" name="architecture" src="d0_s6" to="subcapitate" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="10+-flowered" value_original="10+-flowered" />
        <character char_type="range_value" from="4" from_unit="cm" modifier="often" name="some_measurement" src="d0_s6" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16837" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="at time" constraintid="o16838" is_modifier="false" modifier="mostly" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" modifier="obviously" name="arrangement" notes="" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="rarely not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o16838" name="time" name_original="time" src="d0_s6" type="structure" />
      <biological_entity constraint="proximalmost" id="o16839" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o16840" name="axil" name_original="axils" src="d0_s6" type="structure" />
      <biological_entity id="o16841" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o16842" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s6" value="spaced" value_original="spaced" />
        <character constraint="on axis" constraintid="o16843" is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o16843" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="somewhat" name="length" src="d0_s6" value="elongated" value_original="elongated" />
      </biological_entity>
      <relation from="o16839" id="r1793" name="in" negation="false" src="d0_s6" to="o16840" />
      <relation from="o16840" id="r1794" name="part_of" negation="false" src="d0_s6" to="o16841" />
    </statement>
    <statement id="d0_s7">
      <text>bracts cuneate 2-fid, proximal separated to base, 5 mm, usually equaling or longer than pedicels, margins sometimes toothed.</text>
      <biological_entity id="o16844" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character constraint="to base" constraintid="o16845" is_modifier="false" name="arrangement" src="d0_s7" value="separated" value_original="separated" />
        <character name="some_measurement" notes="" src="d0_s7" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
        <character constraint="than pedicels" constraintid="o16846" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o16845" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o16846" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o16847" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–2 mm.</text>
      <biological_entity id="o16848" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx green (without purplish tint), 7–11 mm, densely stellate-puberulent and coarsely bristly, bristles often on swollen pad, hairs at base shorter, denser, marginal hairs longer;</text>
      <biological_entity id="o16849" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16850" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="stellate-puberulent" value_original="stellate-puberulent" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s9" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o16851" name="bristle" name_original="bristles" src="d0_s9" type="structure" />
      <biological_entity id="o16852" name="pad" name_original="pad" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o16853" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="density" notes="" src="d0_s9" value="denser" value_original="denser" />
      </biological_entity>
      <biological_entity id="o16854" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o16855" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o16851" id="r1795" name="on" negation="false" src="d0_s9" to="o16852" />
      <relation from="o16853" id="r1796" name="at" negation="false" src="d0_s9" to="o16854" />
    </statement>
    <statement id="d0_s10">
      <text>petals pink to rose, usually pale-veined, (10–) 23–24 mm;</text>
      <biological_entity id="o16856" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16857" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="rose" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="pale-veined" value_original="pale-veined" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="23" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="23" from_unit="mm" name="some_measurement" src="d0_s10" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminal column 8 mm;</text>
      <biological_entity id="o16858" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="staminal" id="o16859" name="column" name_original="column" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers white;</text>
      <biological_entity id="o16860" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16861" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas (6 or) 7 or 8.</text>
      <biological_entity id="o16862" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16863" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s13" unit="or" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Schizocarps 5–7 mm diam.;</text>
      <biological_entity id="o16864" name="schizocarp" name_original="schizocarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>mericarps (6 or) 7 or 8, 3.5–4 mm, sparsely glandularpuberulent, prominently reticulate-veined, rugose, and pitted, mucro 0.3–0.5 (–1) mm.</text>
      <biological_entity id="o16865" name="mericarp" name_original="mericarps" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s15" unit="or" value="8" value_original="8" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; prominently" name="architecture" src="d0_s15" value="reticulate-veined" value_original="reticulate-veined" />
        <character is_modifier="false" name="relief" src="d0_s15" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s15" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o16866" name="mucro" name_original="mucro" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 2–3 mm.</text>
      <biological_entity id="o16867" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Apr–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, coastal bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal bluffs" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies rostrata intergrades with subspp. patula and purpurea; it has been confused with Sidalcea hickmanii because of its unlobed and similar cauline leaves; it lacks involucellar bractlets. The unlobed and similarly shaped leaves, congested flowers, and dense, soft-bristly hairs together are distinctive. It has been included within subsp. malviflora but is very different, the unlobed leaves and shaggy hairs being the most noteworthy differences. Subspecies rostrata is known from the central and southern north coast to the north-central coast in Marin, southern Mendocino, San Mateo, and Sonoma counties.</discussion>
  
</bio:treatment>