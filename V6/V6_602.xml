<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">346</other_info_on_meta>
    <other_info_on_meta type="illustration_page">331</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">glaucescens</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 77. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species glaucescens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101124</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sidalcea</taxon_name>
    <taxon_name authority="Congdon" date="unknown" rank="species">montana</taxon_name>
    <taxon_hierarchy>genus Sidalcea;species montana</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Waxy checkerbloom</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.2–0.5 (–0.7) m, glaucous, with taproot and caudex, without rhizomes.</text>
      <biological_entity id="o6638" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.7" to_unit="m" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.5" to_unit="m" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o6639" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <biological_entity id="o6640" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o6641" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o6638" id="r751" name="with" negation="false" src="d0_s0" to="o6639" />
      <relation from="o6638" id="r752" name="with" negation="false" src="d0_s0" to="o6640" />
      <relation from="o6638" id="r753" name="without" negation="false" src="d0_s0" to="o6641" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually few-to-many, clustered, sprawling or decumbent to ascending, rarely erect, not rooting, solid, glaucous, proximally usually stellate-puberulent, sometimes glabrous, distally glabrous.</text>
      <biological_entity id="o6642" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="few" modifier="usually" name="quantity" src="d0_s1" to="many" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="sprawling" value_original="sprawling" />
        <character name="growth_form_or_orientation" src="d0_s1" value="decumbent to ascending" value_original="decumbent to ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="proximally usually" name="pubescence" src="d0_s1" value="stellate-puberulent" value_original="stellate-puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline, basal leaves 9 or fewer or deciduous;</text>
      <biological_entity id="o6643" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6644" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" unit="or feweror" value="9" value_original="9" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate, (2–) 3–5 (–6) × 0.5–1.5 mm;</text>
      <biological_entity id="o6645" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles of basal and proximal cauline leaves 6–14 cm, 3–4 times as long as blades, reduced distally to 1/2 times to as long as blades;</text>
      <biological_entity id="o6646" name="petiole" name_original="petioles" src="d0_s4" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="14" to_unit="cm" />
        <character constraint="blade" constraintid="o6648" is_modifier="false" name="length" src="d0_s4" value="3-4 times as long as blades" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="blade" constraintid="o6649" is_modifier="false" name="length" src="d0_s4" value="0-1/2 times to as long as blades" />
      </biological_entity>
      <biological_entity constraint="basal and proximal cauline" id="o6647" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6648" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o6649" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <relation from="o6646" id="r754" name="part_of" negation="false" src="d0_s4" to="o6647" />
    </statement>
    <statement id="d0_s5">
      <text>blade reniform-orbiculate, palmately 5 (–7) -lobed, deeply incised, 2–6 (–8) × 2–6 (–8) cm, glaucous, surfaces glabrous or minutely stellate-puberulent, lobes shallowly dentate, more deeply divided on distal leaves, margins entire, distalmost sometimes linear, unlobed.</text>
      <biological_entity id="o6650" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="reniform-orbiculate" value_original="reniform-orbiculate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s5" value="5(-7)-lobed" value_original="5(-7)-lobed" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o6651" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s5" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
      <biological_entity id="o6652" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character constraint="on distal leaves" constraintid="o6653" is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6653" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6654" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o6655" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ascending, open, calyces not conspicuously overlapping except sometimes in bud, usually unbranched, 3–10 (–20) -flowered, elongate, 1-sided, 8–20 cm, axis curved between flowers, sometimes zigzag in appearance;</text>
      <biological_entity id="o6656" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o6657" name="calyx" name_original="calyces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not conspicuously" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-10(-20)-flowered" value_original="3-10(-20)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6658" name="bud" name_original="bud" src="d0_s6" type="structure" />
      <biological_entity id="o6659" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character constraint="between flowers" constraintid="o6660" is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o6660" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6661" name="appearance" name_original="appearance" src="d0_s6" type="structure" />
      <relation from="o6657" id="r755" name="except" negation="false" src="d0_s6" to="o6658" />
      <relation from="o6659" id="r756" modifier="sometimes" name="zigzag in" negation="false" src="d0_s6" to="o6661" />
    </statement>
    <statement id="d0_s7">
      <text>bracts linear to lanceolate, distinct or connate and 2-fid, 5 mm, proximal divided to base, distal often undivided, shorter than to equaling pedicels.</text>
      <biological_entity id="o6662" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="lanceolate" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character constraint="to base" constraintid="o6663" is_modifier="false" name="shape" src="d0_s7" value="divided" value_original="divided" />
        <character constraint="to base" constraintid="o6664" is_modifier="false" name="position_or_shape" notes="" src="d0_s7" value="distal" value_original="distal" />
        <character constraint="than to equaling pedicels" constraintid="o6665" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6663" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o6664" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s7" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o6665" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="true" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–3 (–10) mm;</text>
    </statement>
    <statement id="d0_s9">
      <text>involucellar bractlets absent.</text>
      <biological_entity id="o6666" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6667" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual or unisexual and pistillate, plants gynodioecious;</text>
      <biological_entity id="o6668" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6669" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="gynodioecious" value_original="gynodioecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calyx 5–10 mm, enlarging in fruit, hairy, hairs scattered, minute, stellate and sometimes capitate, glandular;</text>
      <biological_entity id="o6670" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character constraint="in fruit" constraintid="o6671" is_modifier="false" name="size" src="d0_s11" value="enlarging" value_original="enlarging" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6671" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
      <biological_entity id="o6672" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s11" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="size" src="d0_s11" value="minute" value_original="minute" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s11" value="stellate" value_original="stellate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s11" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals pink to pink-purple, pale-veined at least when dry, pistillate (7–) 9–12 mm, bisexual 15–20 (–25) mm;</text>
      <biological_entity id="o6673" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s12" to="pink-purple" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s12" value="pale-veined" value_original="pale-veined" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>staminal column 4–7 mm, stellate-hairy;</text>
      <biological_entity constraint="staminal" id="o6674" name="column" name_original="column" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers pale-yellow or pinkish to white;</text>
      <biological_entity id="o6675" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s14" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 6–8.</text>
      <biological_entity id="o6676" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s15" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Schizocarps 5–7 mm diam.;</text>
      <biological_entity id="o6677" name="schizocarp" name_original="schizocarps" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>mericarps 6–8, 3–3.5 mm, roughened, sides reticulate-veined and deeply pitted, back reticulate-veined and glandularpuberulent, mucro 0.3–1 mm.</text>
      <biological_entity id="o6678" name="mericarp" name_original="mericarps" src="d0_s17" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s17" to="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s17" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o6679" name="side" name_original="sides" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="reticulate-veined" value_original="reticulate-veined" />
        <character is_modifier="false" modifier="deeply" name="relief" src="d0_s17" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="back" name="architecture" src="d0_s17" value="reticulate-veined" value_original="reticulate-veined" />
      </biological_entity>
      <biological_entity id="o6680" name="mucro" name_original="mucro" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 2 mm. 2n = 40.</text>
      <biological_entity id="o6681" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character name="some_measurement" src="d0_s18" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6682" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, grassy meadows, open, usually red fir, juniper, or ponderosa pine forests, often serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="grassy meadows" />
        <character name="habitat" value="open" />
        <character name="habitat" value="red fir" modifier="usually" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(900–)1500–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3000" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sidalcea glaucescens is a relatively common, generally low-growing plant of relatively high elevations; it occurs from the central and northern Sierra Nevada to the southern Cascade and Klamath ranges and to north of Reno, Nevada. It usually can be distinguished by its highly glaucous, waxy stems and leaves, 3–5-lobed, entire-margined leaves, and basal leaves that wither by the time fruit is mature; additionally, proximal flowers are spaced several centimeters apart and leafy-bracted, and the inflorescence axis is curved between flowers. It has been confused with S. asprella, S. elegans, and S. multifida, to which it appears to be closely related. It can generally be distinguished from S. multifida by its 5(–7)-lobed leaves, the lobes shallowly incised or entire, its nonpersisting, fewer basal leaves, and its more-procumbent habit. Sidalcea elegans and S. virgata in southwestern Oregon also have been confused with S. glaucescens.</discussion>
  
</bio:treatment>