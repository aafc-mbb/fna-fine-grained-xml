<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="illustration_page">85</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Brathys</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">denticulatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>190. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section brathys;species denticulatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100871</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypericum</taxon_name>
    <taxon_name authority="Michaux ex Willdenow" date="unknown" rank="species">angulosum</taxon_name>
    <taxon_hierarchy>genus Hypericum;species angulosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">denticulatum</taxon_name>
    <taxon_name authority="(Britton) S. F. Blake" date="unknown" rank="variety">ovalifolium</taxon_name>
    <taxon_hierarchy>genus H.;species denticulatum;variety ovalifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">laevigatum</taxon_name>
    <taxon_hierarchy>genus H.;species laevigatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virgatum</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="variety">ovalifolium</taxon_name>
    <taxon_hierarchy>genus H.;species virgatum;variety ovalifolium</taxon_hierarchy>
  </taxon_identification>
  <number>29.</number>
  <other_name type="common_name">Coppery St. John’s wort</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs perennial, erect, branching at usually aerenchymatous base and in inflorescence, 2–7 dm.</text>
      <biological_entity id="o14389" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character constraint="at usually aerenchymatous base and in inflorescence" constraintid="o14390" is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14390" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes 4-lined.</text>
      <biological_entity id="o14391" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o14392" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (main-stem) spreading to appressed, sessile;</text>
      <biological_entity id="o14393" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually broadly to narrowly ovate, rarely elliptic or lanceolate, 4–20 × 5–15 (–18) mm, mostly shorter than internodes, leathery, margins plane, apex acute to subrounded, densely gland-dotted, basal veins 1–5, if 1, midrib with 2–3 pairs of branches.</text>
      <biological_entity id="o14394" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character constraint="than internodes" constraintid="o14395" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="mostly shorter" value_original="mostly shorter" />
        <character is_modifier="false" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o14395" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o14396" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o14397" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="subrounded" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14398" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="5" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14399" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <biological_entity id="o14400" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o14401" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <relation from="o14399" id="r1567" name="with" negation="false" src="d0_s3" to="o14400" />
      <relation from="o14400" id="r1568" name="part_of" negation="false" src="d0_s3" to="o14401" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences broadly pyramidal to corymbiform, to 25-flowered, branching mostly dichasial.</text>
      <biological_entity id="o14402" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character char_type="range_value" from="corymbiform" name="architecture" src="d0_s4" to="25-flowered" />
        <character char_type="range_value" from="corymbiform" name="architecture" src="d0_s4" to="25-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s4" value="dichasial" value_original="dichasial" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 5–13 mm diam.;</text>
      <biological_entity id="o14403" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals ovate or lanceolate to elliptic or obovate, subequal, 3–8 × 1.5–4 mm, margins sometimes ciliate, not setulose-ciliate, apex acute;</text>
      <biological_entity id="o14404" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="elliptic or obovate" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14405" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s6" value="setulose-ciliate" value_original="setulose-ciliate" />
      </biological_entity>
      <biological_entity id="o14406" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals orange-yellow, obovate, 5–10 mm;</text>
      <biological_entity id="o14407" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange-yellow" value_original="orange-yellow" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 50–80, irregularly grouped;</text>
      <biological_entity id="o14408" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s8" to="80" />
        <character is_modifier="false" modifier="irregularly" name="arrangement" src="d0_s8" value="grouped" value_original="grouped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 2–4 mm;</text>
      <biological_entity id="o14409" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas clavate.</text>
      <biological_entity id="o14410" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid to rostrate-subglobose, 3–5 × 2–3 mm.</text>
      <biological_entity id="o14411" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="rostrate-subglobose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 0.4–0.7 mm;</text>
      <biological_entity id="o14412" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>testa obscurely linear-reticulate to finely ribbed-scalariform.</text>
      <biological_entity id="o14413" name="testa" name_original="testa" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture_or_coloration_or_relief" src="d0_s13" value="linear-reticulate" value_original="linear-reticulate" />
        <character is_modifier="false" modifier="finely" name="arrangement" src="d0_s13" value="ribbed-scalariform" value_original="ribbed-scalariform" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet woods, marshes, bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Del., Ga., N.J., N.Y., N.C., Pa., S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>D. H. Webb (1980) regarded the disjunct populations in North Carolina and Tennessee as possible relicts and the Alabama one as due to recent introduction. J. R. Allison (2011) agreed and, in his opinion, the Pennsylvania and Virginia records are historical, and Hypericum denticulatum is likely adventive in Georgia.</discussion>
  <references>
    <reference>Allison, J. R. 2011. Synopsis of the Hypericum denticulatum complex. Castanea 76: 99–115.</reference>
  </references>
  
</bio:treatment>