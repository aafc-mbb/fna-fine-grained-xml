<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="illustration_page">104</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="genus">hybanthus</taxon_name>
    <taxon_name authority="(Ortega) Baillon" date="unknown" rank="species">verticillatus</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Pl.</publication_title>
      <place_in_publication>4: 344. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus hybanthus;species verticillatus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100893</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Ortega" date="unknown" rank="species">verticillata</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Pl. Descr. Dec.</publication_title>
      <place_in_publication>4: 50. 1797</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species verticillata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hybanthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verticillatus</taxon_name>
    <taxon_name authority="(A. Gray) Cory &amp; H. B. Parks" date="unknown" rank="variety">platyphyllus</taxon_name>
    <taxon_hierarchy>genus Hybanthus;species verticillatus;variety platyphyllus</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Baby-slippers</other_name>
  <other_name type="common_name">nodding green-violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subshrubs, perennial, 10–40 cm, from ligneous rhizome.</text>
      <biological_entity constraint="plants" id="o19753" name="subshrub" name_original="subshrubs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19754" name="rhizome" name_original="rhizome" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="ligneous" value_original="ligneous" />
      </biological_entity>
      <relation from="o19753" id="r2089" name="from" negation="false" src="d0_s0" to="o19754" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–20, clustered, erect, simple or branched from proximal nodes, glabrous or strigose to pilose.</text>
      <biological_entity id="o19755" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="from proximal nodes" constraintid="o19756" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="strigose" name="pubescence" notes="" src="d0_s1" to="pilose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o19756" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal opposite or subopposite, distal usually alternate, petiolate or sessile;</text>
      <biological_entity id="o19757" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o19758" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="subopposite" value_original="subopposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19759" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules linear-subulate and minute to leaflike, 3–40 mm, glabrous or hirsute, gland-tipped;</text>
      <biological_entity id="o19760" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19761" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" name="size" src="d0_s3" value="minute" value_original="minute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0–1 mm;</text>
      <biological_entity id="o19762" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o19763" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear, narrowly elliptic, lanceolate, or oblanceolate, 1–5.5 (–6) × 0.1–0.8 (–1.1) cm, base attenuate, margins usually entire, ciliate or eciliate, apex acute to acuminate, surfaces glabrous, strigose, or pilose.</text>
      <biological_entity id="o19764" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o19765" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s5" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19766" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o19767" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o19768" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity id="o19769" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1-flowered;</text>
      <biological_entity id="o19770" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>peduncle usually pendant at anthesis, sometimes horizontal or erect, usually pendant in fruit, 3–14 mm, hirsute to densely puberulent, occasionally glabrous on segment distal to joint;</text>
      <biological_entity id="o19771" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="pendant" value_original="pendant" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s7" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="in fruit" constraintid="o19772" is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="pendant" value_original="pendant" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="14" to_unit="mm" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s7" to="densely puberulent" />
        <character constraint="on segment" constraintid="o19773" is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19772" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o19773" name="segment" name_original="segment" src="d0_s7" type="structure">
        <character constraint="to joint" constraintid="o19774" is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o19774" name="joint" name_original="joint" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracteoles present.</text>
      <biological_entity id="o19775" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals appressed to corolla, ovate to lanceolate, margins ciliate or eciliate, apex acute;</text>
      <biological_entity id="o19776" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19777" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character constraint="to corolla" constraintid="o19778" is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o19778" name="corolla" name_original="corolla" src="d0_s9" type="structure" />
      <biological_entity id="o19779" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o19780" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals: upper greenish white, cream, or yellowish, with purple tips, oblong, 2–2.5 mm, apex acute to rounded, glabrous;</text>
      <biological_entity id="o19781" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="upper" value_original="upper" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19782" name="tip" name_original="tips" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o19783" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19781" id="r2090" name="with" negation="false" src="d0_s10" to="o19782" />
    </statement>
    <statement id="d0_s11">
      <text>laterals similar to upper except 3 mm;</text>
      <biological_entity id="o19784" name="petal" name_original="petals" src="d0_s11" type="structure" />
      <biological_entity id="o19785" name="lateral" name_original="laterals" src="d0_s11" type="structure">
        <character constraint="except 3 mm" is_modifier="false" name="position" src="d0_s11" value="upper" value_original="upper" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lowest greenish white, cream, or yellowish, sometimes tinged purplish, 2.5–6 mm, claw 2–3 × 0.5 mm, distal limb broadly ovate to orbiculate, ± concave at anthesis, 1–3 mm wide, apex rounded to obtusely angled, adaxial surface usually bearded basally and on claw;</text>
      <biological_entity id="o19786" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="tinged purplish" value_original="tinged purplish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19787" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19788" name="limb" name_original="limb" src="d0_s12" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s12" to="orbiculate" />
        <character constraint="at anthesis" is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19789" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s12" to="obtusely angled" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19790" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually; basally" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o19791" name="claw" name_original="claw" src="d0_s12" type="structure" />
      <relation from="o19790" id="r2091" name="on" negation="false" src="d0_s12" to="o19791" />
    </statement>
    <statement id="d0_s13">
      <text>presence of cleistogamous flowers not determined.</text>
      <biological_entity id="o19792" name="petal" name_original="petals" src="d0_s13" type="structure" />
      <biological_entity id="o19793" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules ovoid to globose, 4–7 mm.</text>
      <biological_entity id="o19794" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="globose" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 6, dark-brown to shiny black, subglobose, ± flattened with angular edges, or broadly ovoid, 1.8–2.5 (–3) mm.</text>
      <biological_entity id="o19796" name="edge" name_original="edges" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s15" value="angular" value_original="angular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16, 24, 32.</text>
      <biological_entity id="o19795" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="6" value_original="6" />
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s15" to="shiny black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character constraint="with edges" constraintid="o19796" is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19797" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry plains, gravelly soil in prairies, mesas, fields, desert grasslands, pinyon-juniper woodlands, chaparral, rocky slopes, forest edges, oak and mesquite savannahs, riparian habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry plains" />
        <character name="habitat" value="gravelly soil" constraint="in prairies , mesas , fields , desert grasslands , pinyon-juniper woodlands , chaparral , rocky slopes , forest edges , oak and mesquite savannahs" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="desert grasslands" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="mesquite savannahs" />
        <character name="habitat" value="habitats" modifier="riparian" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Kans., N.Mex., Okla., Tex.; Mexico (Chihuahua, Coahuila, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>