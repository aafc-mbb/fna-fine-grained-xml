<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="M. S. Baker" date="unknown" rank="species">clauseniana</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>4: 194. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species clauseniana</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100909</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Clausen’s violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, acaulescent, not stoloniferous, 3–20 cm;</text>
      <biological_entity id="o15465" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="acaulescent" value_original="acaulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizome short, slender, fleshy.</text>
      <biological_entity id="o15466" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, 2–10, often prostrate, sometimes ascending;</text>
      <biological_entity id="o15467" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="10" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules narrowly lanceolate, margins faintly glandular-toothed, apex acute or obtuse;</text>
      <biological_entity id="o15468" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o15469" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="faintly" name="shape" src="d0_s3" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity id="o15470" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–11 cm, glabrous;</text>
      <biological_entity id="o15471" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="11" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade unlobed, ± deltate, 3–5 × 4–5 cm, base ± truncate, margins serrate, ciliate or eciliate, apex obtuse, surfaces usually glabrous, sometimes sparsely pubescent abaxially.</text>
      <biological_entity id="o15472" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15473" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o15474" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o15475" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o15476" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely; abaxially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 8–14 cm, glabrous.</text>
      <biological_entity id="o15477" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s6" to="14" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals lanceolate to ovate, margins eciliate or ciliate around auricles, auricles 1–2 mm;</text>
      <biological_entity id="o15478" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15479" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovate" />
      </biological_entity>
      <biological_entity id="o15480" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="eciliate" value_original="eciliate" />
        <character constraint="around auricles" constraintid="o15481" is_modifier="false" name="pubescence" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15481" name="auricle" name_original="auricles" src="d0_s7" type="structure" />
      <biological_entity id="o15482" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals light violet on both surfaces, lower 3 purple-veined, all beardless, lowest petal 8–20 mm, spur white, gibbous, 2–3 mm;</text>
      <biological_entity id="o15483" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15484" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character constraint="on surfaces" constraintid="o15485" is_modifier="false" name="coloration" src="d0_s8" value="light violet" value_original="light violet" />
        <character is_modifier="false" name="position" notes="" src="d0_s8" value="lower" value_original="lower" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="purple-veined" value_original="purple-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="beardless" value_original="beardless" />
      </biological_entity>
      <biological_entity id="o15485" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity constraint="lowest" id="o15486" name="petal" name_original="petal" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15487" name="spur" name_original="spur" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style head beardless;</text>
      <biological_entity id="o15488" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="style" id="o15489" name="head" name_original="head" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="beardless" value_original="beardless" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cleistogamous flowers on prostrate to ascending peduncles.</text>
      <biological_entity id="o15490" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15491" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o15492" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="prostrate" is_modifier="true" name="orientation" src="d0_s10" to="ascending" />
      </biological_entity>
      <relation from="o15491" id="r1667" name="on" negation="false" src="d0_s10" to="o15492" />
    </statement>
    <statement id="d0_s11">
      <text>Capsules oblong, 8–10 mm, glabrous.</text>
      <biological_entity id="o15493" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark-brown to black, 1.2–2 mm. 2n = 44.</text>
      <biological_entity id="o15494" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s12" to="black" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15495" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hanging gardens, seeps, springs, shady areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hanging gardens" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="shady areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Viola clauseniana is endemic to Zion National Park, Washington County. M. S. Baker reported that its seeds were minutely roughed, a characteristic not recorded for other Viola species. Viola clauseniana was originally thought to be closely related to the acaulescent blue violets, most notably V. nephrophylla (S. L. Welsh et al. 1987; L. E. McKinney 1992). After contemplating the 2n = 44 chromosome count obtained by J. Clausen (1964), H. E. Ballard (pers. comm.) suggested that V. clauseniana might be more closely related with the stemless white violets (for example, V. blanda) than with V. nephrophylla. Viola clauseniana was considered a distinct species by N. H. Holmgren (2005d) and T. Marcussen and T. Karlsson (2010).</discussion>
  
</bio:treatment>