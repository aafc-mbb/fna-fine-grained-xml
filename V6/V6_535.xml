<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="genus">malvastrum</taxon_name>
    <taxon_name authority="(Linnaeus) Torrey in W. H. Emory" date="1859" rank="species">americanum</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 38. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malvastrum;species americanum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">200013748</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 687. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malva;species americana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">M.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">spicata</taxon_name>
    <taxon_hierarchy>genus M.;species spicata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malvastrum</taxon_name>
    <taxon_name authority="(Linnaeus) A. Gray" date="unknown" rank="species">spicatum</taxon_name>
    <taxon_hierarchy>genus Malvastrum;species spicatum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">American false mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, or subshrubs, (0.5–) 1–2 m, often bushy-branched in distal 1/2.</text>
      <biological_entity id="o9759" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.5" from_unit="m" name="atypical_some_measurement" notes="" src="d0_s0" to="1" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" notes="" src="d0_s0" to="2" to_unit="m" />
        <character constraint="in distal 1/2" constraintid="o9761" is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="bushy-branched" value_original="bushy-branched" />
        <character name="growth_form" value="herb" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9761" name="1/2" name_original="1/2" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, canescent, hairs tufted (not appressed), 6–8-rayed, infrequently glabrate.</text>
      <biological_entity id="o9762" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o9763" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="infrequently" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, lanceolate, subfalcate, 3–5 × 1 mm, apex acuminate;</text>
      <biological_entity id="o9764" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9765" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subfalcate" value_original="subfalcate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9766" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 35–80 mm on proximal leaves, reduced to 10–15 mm on distal leaves and usually on xerophytes;</text>
      <biological_entity id="o9767" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9768" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="on proximal leaves" constraintid="o9769" from="35" from_unit="mm" name="some_measurement" src="d0_s3" to="80" to_unit="mm" />
        <character constraint="on distal leaves" constraintid="o9770" is_modifier="false" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9769" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o9770" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade wide-ovate to ovatelanceolate, very shallowly 3-lobed in distal 1/2 or unlobed (in most plants in the flora area), varying from 5–12 × 4–10 cm on proximal leaves to 2–4 × 1.5–3 cm on distal leaves, usually 1–2 times longer than wide, 2 times longer than petiole of proximal leaves to 3–5 times longer on distal leaves, base slightly cordate or rounded to truncate or cuneate, margins dentate to denticulate, apex acute, surfaces stellate-hairy, hairs (5–) 6–12-rayed.</text>
      <biological_entity id="o9771" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9772" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="wide-ovate" name="shape" src="d0_s4" to="ovatelanceolate" />
        <character constraint="in " constraintid="o9774" is_modifier="false" modifier="very shallowly" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="l_w_ratio" notes="" src="d0_s4" value="1-2" value_original="1-2" />
        <character constraint="on distal leaves" constraintid="o9780" is_modifier="false" name="length_or_size" src="d0_s4" value="2 times longer than petiole of proximal leaves" />
        <character char_type="range_value" from="2 times longer than petiole of proximal leaves" name="length_or_size" src="d0_s4" to="longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9773" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o9774" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="true" name="variability" src="d0_s4" value="varying" value_original="varying" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" constraint="in " constraintid="o9776" from="4" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9775" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o9776" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="true" name="variability" src="d0_s4" value="varying" value_original="varying" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" constraint="on distal leaves" constraintid="o9777" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9777" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9778" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o9779" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o9780" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9781" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate or cuneate" />
      </biological_entity>
      <biological_entity id="o9782" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s4" to="denticulate" />
      </biological_entity>
      <biological_entity id="o9783" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9784" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o9785" name="hair" name_original="hairs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: first 1 or 2 flowers solitary, axillary, remainder in dense terminal spikes 3–10 cm, these terminating each branch;</text>
      <biological_entity id="o9786" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or flowers" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or flowers" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o9787" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9788" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <relation from="o9786" id="r1045" name="in" negation="false" src="d0_s5" to="o9787" />
      <relation from="o9786" id="r1046" name="terminating each" negation="false" src="d0_s5" to="o9788" />
    </statement>
    <statement id="d0_s6">
      <text>floral bracts 2-fid, 4–5 × 2 mm.</text>
      <biological_entity id="o9789" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o9790" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.1–3 mm, not lengthening in fruit;</text>
      <biological_entity id="o9792" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>involucellar bractlets adnate basally to calyx for 1.5–2 mm, lanceolate, subfalcate, 5–7 × 0.8–1.5 mm, equaling to barely exceeding calyx lobes, apex acute to acuminate.</text>
      <biological_entity id="o9791" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character constraint="in fruit" constraintid="o9792" is_modifier="false" modifier="not" name="length" src="d0_s7" value="lengthening" value_original="lengthening" />
      </biological_entity>
      <biological_entity id="o9793" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character constraint="to calyx" constraintid="o9794" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="for 1.5-2 mm" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subfalcate" value_original="subfalcate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o9794" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <biological_entity constraint="calyx" id="o9795" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o9796" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
      <relation from="o9793" id="r1047" modifier="barely" name="exceeding" negation="false" src="d0_s8" to="o9795" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx connate 1/4–1/3 its length, broadly campanulate, 5–6 mm, to 6–10 mm in fruit, surface densely hirsute, hairs scattered, appressed, apically directed, 1–1.5 mm, mixed with minute, closely appressed, 5–8-rayed, stellate hairs;</text>
      <biological_entity id="o9797" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9798" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="length" src="d0_s9" to="1/3" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o9799" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9799" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o9800" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o9801" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="apically" name="orientation" src="d0_s9" value="directed" value_original="directed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
        <character constraint="with hairs" constraintid="o9802" is_modifier="false" name="arrangement" src="d0_s9" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o9802" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="true" modifier="closely" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s9" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla widespreading, orange-yellow, 12–17 mm diam., petals obovate, shortly asymmetrically lobed, 6–10 × 4–6 mm, exceeding calyx by 2–3 mm;</text>
      <biological_entity id="o9803" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o9804" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange-yellow" value_original="orange-yellow" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s10" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9805" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="shortly asymmetrically" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9806" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <relation from="o9805" id="r1048" name="exceeding" negation="false" src="d0_s10" to="o9806" />
    </statement>
    <statement id="d0_s11">
      <text>staminal column 2–3 mm, stellate-puberulent;</text>
      <biological_entity id="o9807" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="staminal" id="o9808" name="column" name_original="column" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style (9 or) 10–15 (–18) -branched.</text>
      <biological_entity id="o9809" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o9810" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="10-15(-18)-branched" value_original="10-15(-18)-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Schizocarps 4–6 mm diam.;</text>
      <biological_entity id="o9811" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps tardily shed from calyx, (9 or) 10–15 (–18), 1.5–3 × 1.5–2 × 0.8 mm, margins angled, sides radially ribbed, narrowly-notched, with 1 minute, proximal-apical mucro to 0.1 mm, minutely hirsute, hairs ascending, restricted to top, simple, 0.1–0.5 mm.</text>
      <biological_entity id="o9812" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="18" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="15" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
        <character name="height" src="d0_s14" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
      <biological_entity id="o9813" name="calyx" name_original="calyx" src="d0_s14" type="structure" />
      <biological_entity id="o9814" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o9815" name="side" name_original="sides" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s14" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="narrowly-notched" value_original="narrowly-notched" />
        <character is_modifier="false" modifier="minutely" name="pubescence" notes="" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="proximal-apical" id="o9816" name="mucro" name_original="mucro" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="true" name="size" src="d0_s14" value="minute" value_original="minute" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9817" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o9812" id="r1049" name="shed from" negation="false" src="d0_s14" to="o9813" />
      <relation from="o9815" id="r1050" name="with" negation="false" src="d0_s14" to="o9816" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5 mm. 2n = 24.</text>
      <biological_entity id="o9818" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9819" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering nearly year-round when sufficiently wet and warm; frost-sensitive.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="nearly;  when sufficiently wet and warm" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, usually secondary and disturbed habitats, near coast</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="secondary" modifier="open usually" constraint="near coast" />
        <character name="habitat" value="disturbed habitats" constraint="near coast" />
        <character name="habitat" value="coast" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Tex.; Mexico; West Indies; Central America; South America (to Argentina); introduced in Asia (China), Africa (Cape Verde Island), Pacific Islands (Indonesia), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (to Argentina)" establishment_means="native" />
        <character name="distribution" value="in Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="Africa (Cape Verde Island)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Indonesia)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Malvastrum americanum from within the flora area tend to be shorter, and to have smaller, narrower unlobed leaves than those of the wet Tropics. The calyces and mericarps can attach easily to clothing and fur. The species is more cold-sensitive than M. coromandelianum and is not as widespread or weedy.</discussion>
  
</bio:treatment>