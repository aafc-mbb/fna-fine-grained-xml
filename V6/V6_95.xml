<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Walter C. Holmes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">DATISCACEAE</taxon_name>
    <taxon_hierarchy>family DATISCACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10255</other_info_on_name>
  </taxon_identification>
  <number>0</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, perennial.</text>
      <biological_entity id="o1179" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves alternate, pinnately incised to compound, sometimes simple;</text>
      <biological_entity id="o1181" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s1" value="incised" value_original="incised" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules absent;</text>
      <biological_entity id="o1182" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole present.</text>
      <biological_entity id="o1183" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary fascicles.</text>
      <biological_entity id="o1184" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers usually unisexual (mostly bisexual in Datisca glomerata);</text>
      <biological_entity id="o1185" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>calyx unequally 4–9-lobed;</text>
      <biological_entity id="o1186" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="unequally" name="shape" src="d0_s6" value="4-9-lobed" value_original="4-9-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 0;</text>
      <biological_entity id="o1187" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminate flowers: stamens [6–] 8–12 [–25];</text>
      <biological_entity id="o1188" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o1189" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="25" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments distinct, very short;</text>
      <biological_entity id="o1190" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o1191" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate flowers: calyx-tube adnate to ovary;</text>
      <biological_entity id="o1192" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1193" name="calyx-tube" name_original="calyx-tube" src="d0_s10" type="structure">
        <character constraint="to ovary" constraintid="o1194" is_modifier="false" name="fusion" src="d0_s10" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o1194" name="ovary" name_original="ovary" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>ovary inferior, 3–8-carpellate;</text>
      <biological_entity id="o1195" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1196" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="inferior" value_original="inferior" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-8-carpellate" value_original="3-8-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>placentation parietal;</text>
      <biological_entity id="o1197" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="placentation" src="d0_s12" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 24–64;</text>
      <biological_entity id="o1198" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1199" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" from="24" name="quantity" src="d0_s13" to="64" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, threadlike, deeply forked at apex [subulate or longer with capitate-peltate or clavellate stigmas];</text>
      <biological_entity id="o1200" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1201" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s14" value="thread-like" value_original="threadlike" />
        <character constraint="at apex" constraintid="o1202" is_modifier="false" modifier="deeply" name="shape" src="d0_s14" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o1202" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>bisexual flowers often with staminodes.</text>
      <biological_entity id="o1203" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o1204" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o1205" name="staminode" name_original="staminodes" src="d0_s15" type="structure" />
      <relation from="o1204" id="r129" name="with" negation="false" src="d0_s15" to="o1205" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsular, opening apically between styles.</text>
      <biological_entity id="o1206" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="capsular" value_original="capsular" />
      </biological_entity>
      <biological_entity id="o1207" name="style" name_original="styles" src="d0_s16" type="structure" />
      <relation from="o1206" id="r130" name="opening" negation="false" src="d0_s16" to="o1207" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 100–300;</text>
      <biological_entity id="o1208" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s17" to="300" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>embryo straight;</text>
      <biological_entity id="o1209" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>endosperm little or none.</text>
      <biological_entity id="o1210" name="endosperm" name_original="endosperm" src="d0_s19" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s19" value="0" value_original="none" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico, sw Asia (Mediterranean region to the Himalayas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
        <character name="distribution" value="sw Asia (Mediterranean region to the Himalayas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genus 1, species 2 (1 in the flora).</discussion>
  
</bio:treatment>