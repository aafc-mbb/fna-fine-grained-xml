<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">40</other_info_on_meta>
    <other_info_on_meta type="illustration_page">37</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Hooker f. in G. Bentham and J. D. Hooker" date="unknown" rank="genus">ctenolepis</taxon_name>
    <taxon_name authority="(Stocks) Hooker f. in D. Oliver" date="unknown" rank="species">cerasiformis</taxon_name>
    <place_of_publication>
      <publication_title>in D. Oliver, Fl. Trop. Afr.</publication_title>
      <place_in_publication>2: 558. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus ctenolepis;species cerasiformis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250077265</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zehneria</taxon_name>
    <taxon_name authority="Stocks" date="unknown" rank="species">cerasiformis</taxon_name>
    <place_of_publication>
      <publication_title>Hooker’s J. Bot. Kew Gard. Misc.</publication_title>
      <place_in_publication>4: 149. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Zehneria;species cerasiformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Blastania</taxon_name>
    <taxon_name authority="(Stocks) A. Meeuse" date="unknown" rank="species">cerasiformis</taxon_name>
    <taxon_hierarchy>genus Blastania;species cerasiformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="Kotschy &amp; Peyritsch" date="unknown" rank="species">fimbristipula</taxon_name>
    <taxon_hierarchy>genus B.;species fimbristipula</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous or subscabrous except sparsely hairy at nodes.</text>
      <biological_entity id="o18387" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="subscabrous" value_original="subscabrous" />
        <character constraint="at nodes" constraintid="o18388" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18388" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1–5 cm;</text>
      <biological_entity id="o18389" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o18390" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipuliform bracts, suborbiculate, 6–15 (–20) mm, basally clasping stem, margins stiffly spreading-ciliate;</text>
      <biological_entity id="o18391" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18392" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="stipuliform" value_original="stipuliform" />
      </biological_entity>
      <biological_entity id="o18393" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="basally" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o18394" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="stiffly" name="architecture_or_pubescence_or_shape" src="d0_s2" value="spreading-ciliate" value_original="spreading-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly ovate to broadly ovate-cordate or suborbiculate, usually distinctly and deeply lobed, 3.5–9 cm, lobes obovate to ovate-oblong or elliptic, base cuneate-attenuate, surfaces scabrous at least on veins and margins.</text>
      <biological_entity id="o18395" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18396" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="broadly ovate-cordate or suborbiculate" />
        <character is_modifier="false" modifier="usually distinctly; distinctly; deeply" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s3" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18397" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="ovate-oblong or elliptic" />
      </biological_entity>
      <biological_entity id="o18398" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate-attenuate" value_original="cuneate-attenuate" />
      </biological_entity>
      <biological_entity id="o18399" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character constraint="on margins" constraintid="o18401" is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o18400" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o18401" name="margin" name_original="margins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Floral bracts 1–3 mm.</text>
      <biological_entity constraint="floral" id="o18402" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 2–7 mm (pistillate), 20–40 mm (staminate).</text>
      <biological_entity id="o18403" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s5" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium 0.5–1 mm;</text>
      <biological_entity id="o18404" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18405" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals reflexed, 0.5–1 mm.</text>
      <biological_entity id="o18406" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18407" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits 1.3–1.5 cm, peduncles 2.5–7 mm.</text>
      <biological_entity id="o18408" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18409" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds whitish, 7.5–11 mm.</text>
      <biological_entity id="o18410" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chrome ore piles</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ore piles" modifier="chrome" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md.; Africa; introduced also in Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ctenolepis cerasiformis was collected by C. F. Reed in 1958 at temporary unloading sites for chrome ore from cargo ships in the Port of Baltimore. The plants are distinctive in their annual duration, deeply lobed leaves, foliaceous stipuliform bracts with stiffly ciliate margins, tiny white flowers, and small, few-seeded, smooth, red, berrylike fruits on relatively short peduncles.</discussion>
  
</bio:treatment>