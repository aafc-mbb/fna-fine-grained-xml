<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Meghan G. Mendenhall,Paul A. Fryxell†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">299</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Fabricius" date="1759" rank="genus">MALVAVISCUS</taxon_name>
    <place_of_publication>
      <publication_title>Enum.,</publication_title>
      <place_in_publication>155. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malvaviscus;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, malva mallow, and viscidus, sticky, alluding to sap</other_info_on_name>
    <other_info_on_name type="fna_id">119574</other_info_on_name>
  </taxon_identification>
  <number>37.</number>
  <other_name type="common_name">Turk’s-cap</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [trees], in M. arboreus var. drummondii forming dense clones propagated by root proliferation.</text>
      <biological_entity id="o6044" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o6045" name="m" name_original="m" src="d0_s0" type="structure" />
      <biological_entity id="o6046" name="clone" name_original="clones" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o6047" name="root" name_original="root" src="d0_s0" type="structure" />
      <relation from="o6044" id="r675" name="in" negation="false" src="d0_s0" to="o6045" />
      <relation from="o6045" id="r676" name="forming" negation="false" src="d0_s0" to="o6046" />
      <relation from="o6044" id="r677" name="propagated by" negation="false" src="d0_s0" to="o6047" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, hairy or glabrous, not viscid.</text>
      <biological_entity id="o6048" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules deciduous, linear or subulate;</text>
      <biological_entity id="o6049" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6050" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to broadly ovate, unlobed or 3–5-lobed, base rounded to cordate, stigmas capitate.</text>
      <biological_entity id="o6051" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6052" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="broadly ovate unlobed or 3-5-lobed" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="broadly ovate unlobed or 3-5-lobed" />
      </biological_entity>
      <biological_entity id="o6053" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
      <biological_entity id="o6054" name="stigma" name_original="stigmas" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruits schizocarpic berries, erect [pendent], not inflated, oblate, fleshy, glabrous;</text>
      <biological_entity id="o6055" name="fruit" name_original="fruits" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6056" name="berry" name_original="berries" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="schizocarpic" value_original="schizocarpic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mericarps 5, 1-celled, without dorsal spur, indehiscent.</text>
      <biological_entity id="o6057" name="mericarp" name_original="mericarps" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-celled" value_original="1-celled" />
        <character is_modifier="false" name="dehiscence" notes="" src="d0_s5" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o6058" name="spur" name_original="spur" src="d0_s5" type="structure" />
      <relation from="o6057" id="r678" name="without" negation="false" src="d0_s5" to="o6058" />
    </statement>
    <statement id="d0_s6">
      <text>Seeds 1 per mericarp, ellipsoid wedge-shaped, glabrous.</text>
      <biological_entity id="o6060" name="mericarp" name_original="mericarp" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>x = 14.</text>
      <biological_entity id="o6059" name="seed" name_original="seeds" src="d0_s6" type="structure">
        <character constraint="per mericarp" constraintid="o6060" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="wedge--shaped" value_original="wedge--shaped" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o6061" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc, se United States, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc" establishment_means="native" />
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (1 in the flora).</discussion>
  <discussion>Malvaviscus penduliflorus de Candolle is found in gardens throughout the Old and New World Tropics and subtropics. This species appears to be a sterile hybrid; it never fruits and is not known in the wild.</discussion>
  <discussion>Malvaviscus is readily distinguished by its auriculate petals and red, edible, baccate fruits. This fruit type is very rare in the Malvoideae. The genera thought to be most closely related to Malvaviscus are Anotea (de Candolle) Ulbrich and Pavonia. Of the two, only Pavonia, with dry fruits, occurs within the flora area. Morphological variation within Malvaviscus has led to proposals of more than 50 specific names. Much of the variation is due to the existence of populational forms and their intergradation. Modern treatments recognize from one to a dozen species.</discussion>
  <discussion>Only Malvaviscus arboreus var. drummondii is common in the flora area; two other taxa may also be encountered. Variety drummondii has uniformly three-lobed, broad, cordate leaves and is the only taxon in Malvaviscus to form clones through root proliferation and to occur in a fully temperate climate. Malvaviscus arboreus var. arboreus and M. penduliflorus de Candolle have leaves that vary from unlobed to shallowly lobed and are usually much longer than wide and have rounded bases. In the flora area, var. arboreus (including M. arboreus var. mexicanus) occurs rarely in the Rio Grande valley of southern Texas and is also infrequently cultivated from Texas to Florida. Malvaviscus penduliflorus is frequently cultivated in the Rio Grande plains of Texas and eastward to Florida and throughout the Tropics; it is not known to occur in the wild. Although plants may persist after cultivation, this species does not truly escape domestication in the flora area or elsewhere. Plants of M. penduliflorus are more robust than those of M. arboreus and have spectacular, pendulous flowers up to 7 cm long. Unlike other species of Malvaviscus, the stamens are not fully exserted although the styles and stigmas generally are. Individuals are not known to set fruit and are propagated by cuttings. Malvaviscus penduliflorus may be found where there is evidence of a homestead, such as remnants of a building foundation.</discussion>
  <references>
    <reference>Schery, R. W. 1942. Monograph of Malvaviscus. Ann. Missouri Bot. Gard. 29: 183–244.</reference>
    <reference>Turner, B. L. and M. G. Mendenhall. 1993. Revision of Malvaviscus (Malvaceae). Ann. Missouri Bot. Gard. 80: 439–457.</reference>
  </references>
  
</bio:treatment>