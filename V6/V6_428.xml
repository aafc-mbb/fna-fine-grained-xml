<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Fryxell" date="1982" rank="genus">BILLIETURNERA</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>9: 195. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus billieturnera;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Billie Lee Turner, b. 1925, American botanist</other_info_on_name>
    <other_info_on_name type="fna_id">103951</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="Clement" date="unknown" rank="section">Icanifolia</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>180: 60. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sida;section Icanifolia</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs.</text>
      <biological_entity id="o13186" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or procumbent, softly hairy, not viscid.</text>
      <biological_entity id="o13187" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="softly" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, broadly oblanceolate;</text>
      <biological_entity id="o13188" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13189" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade broadly oblanceolate, unlobed, not dissected or parted, base cuneate, margins dentate to subentire.</text>
      <biological_entity id="o13190" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13191" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s3" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity id="o13192" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13193" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s3" to="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, solitary flowers;</text>
      <biological_entity id="o13194" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o13195" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucel absent.</text>
      <biological_entity id="o13196" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx not accrescent, not inflated, not completely enclosing fruits, deeply divided, lobes unribbed, narrowly triangular;</text>
      <biological_entity id="o13197" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13198" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s6" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o13199" name="fruit" name_original="fruits" src="d0_s6" type="structure" />
      <biological_entity id="o13200" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="unribbed" value_original="unribbed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o13198" id="r1426" modifier="not completely" name="enclosing" negation="false" src="d0_s6" to="o13199" />
    </statement>
    <statement id="d0_s7">
      <text>corolla pale-yellow;</text>
      <biological_entity id="o13201" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13202" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column included;</text>
      <biological_entity id="o13203" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o13204" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 5-branched;</text>
      <biological_entity id="o13205" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13206" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-branched" value_original="5-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas capitate.</text>
      <biological_entity id="o13207" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13208" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits schizocarps, erect, not inflated, variable in shape, papery, stellate-hairy, apically dehiscent;</text>
      <biological_entity constraint="fruits" id="o13209" name="schizocarp" name_original="schizocarps" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="variable" value_original="variable" />
        <character is_modifier="false" name="texture" src="d0_s11" value="papery" value_original="papery" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" modifier="apically" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>mericarps 5, 1-celled, with abaxial keel and apical spine 1–1.5 mm.</text>
      <biological_entity id="o13210" name="mericarp" name_original="mericarps" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-celled" value_original="1-celled" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13211" name="keel" name_original="keel" src="d0_s12" type="structure" />
      <biological_entity constraint="apical" id="o13212" name="spine" name_original="spine" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o13210" id="r1427" name="with" negation="false" src="d0_s12" to="o13211" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1 per mericarp, minutely and obscurely pubescent.</text>
      <biological_entity id="o13214" name="mericarp" name_original="mericarp" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>x = 8.</text>
      <biological_entity id="o13213" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character constraint="per mericarp" constraintid="o13214" name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" modifier="minutely; obscurely" name="pubescence" notes="" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="x" id="o13215" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., ne Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Although originally included in Sida (because the mericarps are one-seeded), Billieturnera is more closely related to Abutilon than it is to Sida.</discussion>
  
</bio:treatment>