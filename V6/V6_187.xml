<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>R. John Little</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="mention_page">106</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="genus">HYBANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Syst. Pl.,</publication_title>
      <place_in_publication>2, 17. 1760</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus HYBANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hybos, hump, and anthos, flower, alluding to recurved pedicels</other_info_on_name>
    <other_info_on_name type="fna_id">115952</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque ex Britton &amp; A. Brown" date="unknown" rank="genus">Cubelium</taxon_name>
    <taxon_hierarchy>genus Cubelium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Ventenat" date="unknown" rank="genus">Ionidium</taxon_name>
    <taxon_hierarchy>genus Ionidium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Vandelli" date="unknown" rank="genus">Pombalia</taxon_name>
    <taxon_hierarchy>genus Pombalia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, annual or perennial, [subshrubs, shrubs], caulescent, homophyllous, glabrous or hairy.</text>
      <biological_entity id="o18564" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="homophyllous" value_original="homophyllous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="homophyllous" value_original="homophyllous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems persistent, 1–20, usually erect, sometimes suberect or prostrate, leafy, simple or branched, from thick, fleshy or subligneous, branched or unbranched rhizome or taproot.</text>
      <biological_entity id="o18566" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o18567" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o18568" name="taproot" name_original="taproot" src="d0_s1" type="structure" />
      <relation from="o18566" id="r1959" name="from" negation="false" src="d0_s1" to="o18567" />
      <relation from="o18566" id="r1960" name="from" negation="false" src="d0_s1" to="o18568" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline, simple, proximal usually opposite, distal usually alternate, petiolate or sessile;</text>
      <biological_entity id="o18569" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18570" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18571" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules not adnate to petiole, linear-subulate to leaflike, usually shorter than leaves;</text>
      <biological_entity id="o18572" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character constraint="to petiole" constraintid="o18573" is_modifier="false" modifier="not" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="linear-subulate" name="shape" notes="" src="d0_s3" to="leaflike" />
        <character constraint="than leaves" constraintid="o18574" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o18573" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o18574" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade not overlapping basally, linear, lanceolate to oblanceolate, ovate to obovate, or elliptic, surfaces not mottled.</text>
      <biological_entity id="o18575" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not; basally" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate ovate" />
      </biological_entity>
      <biological_entity id="o18576" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="mottled" value_original="mottled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary in leaf-axils, 1–3 (4) -flowered or in poorly defined racemes;</text>
      <biological_entity id="o18577" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character constraint="in leaf-axils" constraintid="o18578" is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="1-3(4)-flowered" value_original="1-3(4)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="in poorly defined racemes" value_original="in poorly defined racemes" />
      </biological_entity>
      <biological_entity id="o18578" name="leaf-axil" name_original="leaf-axils" src="d0_s5" type="structure" />
      <biological_entity id="o18579" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="poorly" name="prominence" src="d0_s5" value="defined" value_original="defined" />
      </biological_entity>
      <relation from="o18577" id="r1961" name="in" negation="false" src="d0_s5" to="o18579" />
    </statement>
    <statement id="d0_s6">
      <text>peduncle jointed;</text>
      <biological_entity id="o18580" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles present or absent.</text>
      <biological_entity id="o18581" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals subequal, not auriculate;</text>
      <biological_entity id="o18582" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18583" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>upper 2 and lateral 2 petals not showy, 0.5–5 mm, lowest petal showy, larger than others, narrowed at middle, angular-deltate, elliptic, oblong, orbiculate, or ovate, upper and lateral petals usually glabrous, lower sometimes bearded adaxially;</text>
      <biological_entity id="o18584" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="upper and lateral" id="o18585" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18586" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="showy" value_original="showy" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o18587" name="petal" name_original="petal" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="showy" value_original="showy" />
        <character constraint="than others" constraintid="o18588" is_modifier="false" name="size" src="d0_s9" value="larger" value_original="larger" />
        <character constraint="at middle lateral petals" constraintid="o18589" is_modifier="false" name="shape" src="d0_s9" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="angular-deltate" value_original="angular-deltate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o18588" name="other" name_original="others" src="d0_s9" type="structure" />
      <biological_entity constraint="middle and lateral" id="o18589" name="petal" name_original="petals" src="d0_s9" type="structure" />
      <biological_entity constraint="upper and lateral" id="o18590" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower and lateral" id="o18591" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes; adaxially" name="pubescence" src="d0_s9" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>spur gibbous;</text>
      <biological_entity id="o18592" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o18593" name="spur" name_original="spur" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="gibbous" value_original="gibbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens connate, lowest 2 filaments not spurred with nectary;</text>
      <biological_entity id="o18594" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o18595" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s11" value="lowest" value_original="lowest" />
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18596" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character constraint="with nectary" constraintid="o18597" is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s11" value="spurred" value_original="spurred" />
      </biological_entity>
      <biological_entity id="o18597" name="nectary" name_original="nectary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>cleistogamous flowers present or absent.</text>
      <biological_entity id="o18598" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18599" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid, globose, obtusely trigonous, oblong, or ellipsoid, glabrous.</text>
      <biological_entity id="o18600" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s13" value="trigonous" value_original="trigonous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds (3–) 6–9, globose to slightly flattened, glabrous [hairy], with whitish elaiosome.</text>
      <biological_entity id="o18602" name="elaiosome" name_original="elaiosome" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
      </biological_entity>
      <relation from="o18601" id="r1962" name="with" negation="false" src="d0_s14" to="o18602" />
    </statement>
    <statement id="d0_s15">
      <text>x = 4.</text>
      <biological_entity id="o18601" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s14" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s14" to="9" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="slightly flattened" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o18603" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Asia, Africa, Australia; tropical and subtropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="tropical and subtropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 100–115 (5 in the flora).</discussion>
  <discussion>In 2010, M. N. Seo et al. concluded that polyploidy likely played a role in speciation in Hybanthus and Seo et al. (2011) proposed an infrageneric classification of Hybanthus based on foliar micromorphology. Research using trnL/trnL–F and rbcL plastid regions shows that Hybanthus is polyphyletic and can be segregated into nine morphologically and biogeographically distinct groups (G. A. Wahlert et al. 2014). Molecular phylogenetic and morphological evidence will be used to circumscribe the nine Hybanthus lineages as separate genera (Wahlert et al.). Hybanthus would be reduced to two or three species and H. concolor would be placed in Cubelium (H. E. Ballard, pers. comm.), a position supported in some molecular studies, for example, T. Marcussen et al. (2010). The other four species in the flora area would be placed in Pombalia (Ballard, pers. comm.).</discussion>
  <discussion>The overall outline of the lowest petal (sometimes referred to as the anterior petal) is usually pandurate or panduriform and is one of the more diagnostic characters of Hybanthus. The distal limb is usually enlarged and is longer and wider than the upper and lateral petals. The distal limb of some species rolls inward after anthesis.</discussion>
  <discussion>The method by which capsules dehisce is not discussed in the major references that treat Hybanthus. Because of the similarity to Viola capsules, it may be assumed that their dehiscence is similar. Although capsules of Viola are sometimes described as “explosively dehiscent,” the capsules of at least some North American species open relatively slowly. As the capsule</discussion>
  <discussion>valves dry out, they contract and squeeze the seeds causing them to be ballistically ejected. J. de Paula-Souza (pers. comm.) reported that Violaceae capsules in South America are not truly “explosive,” but rather the seeds are expelled as the capsule valves dry out.</discussion>
  <references>
    <reference>Morton, C. V. 1944. Studies of tropical American plants. The genus Hybanthus in continental North America. Contr. U.S. Natl. Herb. 29: 74–82.</reference>
    <reference>Morton, C. V. 1971. Some types and range extensions in Hybanthus (Violaceae). Phytologia 21: 56–62.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds white to cream.</description>
      <determination>2 Hybanthus concolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds dark brown to black with white to gray mottling, brownish black to black, or shiny black</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf margins crenate to serrate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf margins entire or remotely crenulate-dentate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 1.5–7(–10.5) cm; petioles (3–)4–7 mm; petals white, purple-tinged, or violet, lowest petal 8–12 mm; bracteoles present; Arizona.</description>
      <determination>1 Hybanthus attenuatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 0.3–3 cm; petioles 0.5–4 mm; petals white, lowest petal 1.5–3.7 mm; bracteoles absent; Georgia, New Jersey.</description>
      <determination>4 Hybanthus parviflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lowest petal 5–9.5 mm, limb 4–10 mm wide, with basal purple patch.</description>
      <determination>3 Hybanthus linearifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lowest petal 2.5–6 mm, limb 1–3 mm wide, without basal purple patch.</description>
      <determination>5 Hybanthus verticillatus</determination>
    </key_statement>
  </key>
</bio:treatment>