<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Sterculioideae</taxon_name>
    <taxon_name authority="Schott &amp; Endlicher" date="unknown" rank="genus">BRACHYCHITON</taxon_name>
    <place_of_publication>
      <publication_title>Melet. Bot.,</publication_title>
      <place_in_publication>34. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily sterculioideae;genus brachychiton;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek brachys, short, and chiton, tunic, evidently alluding to covering of short hairs on seeds</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">104436</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [shrubs], trunks often swollen proximally, plants monoecious.</text>
      <biological_entity id="o13025" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13026" name="trunk" name_original="trunks" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often; proximally" name="shape" src="d0_s0" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules caducous [persistent];</text>
      <biological_entity id="o13028" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13029" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade often lobed, base usually cuneate to truncate or cordate, margins usually entire or serrate, palmately or pinnately veined.</text>
      <biological_entity id="o13030" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13031" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o13032" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually cuneate" name="shape" src="d0_s2" to="truncate or cordate" />
      </biological_entity>
      <biological_entity id="o13033" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s2" value="or" value_original="or" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s2" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers functionally unisexual;</text>
      <biological_entity id="o13034" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="functionally" name="reproduction" src="d0_s3" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals ± valvate, basally connate;</text>
      <biological_entity id="o13035" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_dehiscence" src="d0_s4" value="valvate" value_original="valvate" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>nectaries 0 or 10–16;</text>
      <biological_entity id="o13036" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens (staminodes in pistillate flowers) [10–] 25–30;</text>
      <biological_entity id="o13037" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s6" to="25" to_inclusive="false" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s6" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers often crowded;</text>
      <biological_entity id="o13038" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary stalked, carpels 5, distinct;</text>
      <biological_entity id="o13039" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o13040" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 5, connate;</text>
      <biological_entity id="o13041" name="style" name_original="styles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas 5.</text>
      <biological_entity id="o13042" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Follicles woody, outsides glabrous, insides stellate-hairy.</text>
      <biological_entity id="o13043" name="follicle" name_original="follicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds hairy.</text>
    </statement>
    <statement id="d0_s13">
      <text>x = 20.</text>
      <biological_entity id="o13044" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="x" id="o13045" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Calif.; Pacific Islands (Papua New Guinea), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Papua New Guinea)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 31 (1 in the flora).</discussion>
  <references>
    <reference>Guymer, G. P. 1988. A taxonomic revision of Brachychiton (Sterculiaceae). Austral. Syst. Bot. 1: 199–323.</reference>
  </references>
  
</bio:treatment>