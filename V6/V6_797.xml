<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Salisbury" date="unknown" rank="family">droseraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">drosera</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 211. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family droseraceae;genus drosera;species brevifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250015186</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drosera</taxon_name>
    <taxon_name authority="E. L. Reed" date="unknown" rank="species">annua</taxon_name>
    <taxon_hierarchy>genus Drosera;species annua</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="Shinners" date="unknown" rank="species">leucantha</taxon_name>
    <taxon_hierarchy>genus D.;species leucantha</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Shortleaf or dwarf sundew</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not forming winter hibernaculae, rosettes to 2 (–3.5) cm diam.;</text>
      <biological_entity id="o21442" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21443" name="rosette" name_original="rosettes" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s0" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s0" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stem base not bulbous-cormose.</text>
      <biological_entity constraint="stem" id="o21444" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="bulbous-cormose" value_original="bulbous-cormose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves prostrate;</text>
      <biological_entity id="o21445" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules absent or reduced to 1 or 2 minute hairs;</text>
      <biological_entity id="o21446" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character constraint="to hairs" constraintid="o21447" is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21447" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole often not differentiated from blade, dilated distally, 0.5–1 cm, glabrous;</text>
      <biological_entity id="o21448" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="from blade" constraintid="o21449" is_modifier="false" modifier="often not" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s4" value="dilated" value_original="dilated" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21449" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade cuneate, 0.4–1 cm × 5–12 mm, usually longer than petiole.</text>
      <biological_entity id="o21450" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
        <character constraint="than petiole" constraintid="o21451" is_modifier="false" name="length_or_size" src="d0_s5" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o21451" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1–8-flowered;</text>
      <biological_entity id="o21452" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-8-flowered" value_original="1-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>scapes (1–) 4–9 cm, stipitate-glandular.</text>
      <biological_entity id="o21453" name="scape" name_original="scapes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels stipitate-glandular.</text>
      <biological_entity id="o21454" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 15 mm diam.;</text>
      <biological_entity id="o21455" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="diameter" src="d0_s9" unit="mm" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals distinct, oblong-ovate, 2.5–3.5 × 1.5–2.5 mm, stipitate-glandular;</text>
      <biological_entity id="o21456" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white to rose-pink, obovate, 4–8 × 2–3 mm.</text>
      <biological_entity id="o21457" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="rose-pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 3 mm.</text>
      <biological_entity id="o21458" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds black, obovoid or oblong, 0.3–0.5 mm, base caudate, crateriform, pits in 10–12 rows.</text>
      <biological_entity id="o21459" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21460" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="caudate" value_original="caudate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="crateriform" value_original="crateriform" />
      </biological_entity>
      <biological_entity id="o21462" name="row" name_original="rows" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
      <relation from="o21461" id="r2274" name="in" negation="false" src="d0_s13" to="o21462" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 20.</text>
      <biological_entity id="o21461" name="pit" name_original="pits" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o21463" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May(–Dec).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sandy-peaty pinelands and roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy-peaty pinelands" modifier="moist" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Kans., Ky., La., Miss., N.C., Okla., S.C., Tenn., Tex., Va.; Mexico; West Indies (Cuba); Central America; South America (Brazil).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Drosera brevifolia is the smallest and perhaps the most widespread species of the genus in the Southeast. It may be rare or local throughout its range, and may act as an annual, especially if the habitat dries out. The flowers are large for the size of the plant, and the stipitate-glandular scapes, pedicels, and sepals are quite distinctive. The species is easy to grow in cultivation.</discussion>
  
</bio:treatment>