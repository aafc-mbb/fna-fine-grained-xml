<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Kellogg" date="1854" rank="genus">marah</taxon_name>
    <taxon_name authority="(Cogniaux) Greene" date="unknown" rank="species">watsonii</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>2: 36. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus marah;species watsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100811</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinocystis</taxon_name>
    <taxon_name authority="Cogniaux" date="unknown" rank="species">watsonii</taxon_name>
    <place_of_publication>
      <publication_title>in A. L. P. P. de Candolle and C. de Candolle, Monogr. Phan.</publication_title>
      <place_in_publication>3: 819. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Echinocystis;species watsonii</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Taw manroot</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades deeply 5-lobed, 3–8 cm wide, surfaces glaucous abaxially.</text>
      <biological_entity id="o11893" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s0" value="5-lobed" value_original="5-lobed" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s0" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11894" name="surface" name_original="surfaces" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: sepals (pistillate) linear-subulate, 1 mm;</text>
      <biological_entity id="o11895" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o11896" name="sepal" name_original="sepals" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-subulate" value_original="linear-subulate" />
        <character name="some_measurement" src="d0_s1" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petals 4–5 mm (pistillate) or 2.5–3 mm (staminate), corolla white, deeply cupulate to campanulate;</text>
      <biological_entity id="o11897" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o11898" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character name="some_measurement" src="d0_s2" value="2.5-3 mm" value_original="2.5-3 mm" />
      </biological_entity>
      <biological_entity id="o11899" name="corolla" name_original="corolla" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character char_type="range_value" from="deeply cupulate" name="shape" src="d0_s2" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>staminodia present in pistillate flowers.</text>
      <biological_entity id="o11900" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o11901" name="staminodium" name_original="staminodia" src="d0_s3" type="structure">
        <character constraint="in flowers" constraintid="o11902" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11902" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsules often striped dark green, globose or depressed-globose to short-ellipsoid, 2–3.5 cm, surface smooth to sparsely echinate or muriculate, spinules weak, 1–2 mm.</text>
      <biological_entity id="o11903" name="capsule" name_original="capsules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="striped dark" value_original="striped dark" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="depressed-globose" name="shape" src="d0_s4" to="short-ellipsoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11904" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s4" to="sparsely echinate" />
        <character is_modifier="false" name="relief" src="d0_s4" value="muriculate" value_original="muriculate" />
      </biological_entity>
      <biological_entity id="o11905" name="spinule" name_original="spinules" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="weak" value_original="weak" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seeds 1–4, globose, not compressed, 11–14 mm.</text>
      <biological_entity id="o11906" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Oak and pine-oak grasslands, oak woodlands, yellow pine woodlands, chaparral, rocky slopes and outcrops, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine-oak grasslands" modifier="oak and" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="yellow pine woodlands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–800(–1300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Marah watsonii is distributed in about 15 counties in north-central California.</discussion>
  
</bio:treatment>