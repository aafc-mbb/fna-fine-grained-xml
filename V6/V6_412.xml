<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ALTHAEA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 686. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 307. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus althaea;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Althaea, wife of King Oeneus of Aetolia or Calydon</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">101219</other_info_on_name>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Marshmallow</other_name>
  <other_name type="common_name">guimauve</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, usually hairy, hairs stellate or simple, not viscid.</text>
      <biological_entity id="o22567" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22568" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s0" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent.</text>
      <biological_entity id="o22569" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent or caducous, subulate or linear to lanceolate, simple to 2 or 3-fid;</text>
      <biological_entity id="o22570" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22571" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-fid" value_original="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade reniform, deltate-ovate, or ovate, bluntly or acutely lobed or palmately parted, base cuneate, truncate, obtuse, or cordate, margins crenate to coarsely dentate or serrate.</text>
      <biological_entity id="o22572" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22573" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="bluntly" name="shape" src="d0_s3" value="or" value_original="or" />
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="parted" value_original="parted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="bluntly" name="shape" src="d0_s3" value="or" value_original="or" />
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="parted" value_original="parted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="bluntly" name="shape" src="d0_s3" value="or" value_original="or" />
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="parted" value_original="parted" />
      </biological_entity>
      <biological_entity id="o22574" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o22575" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, solitary flowers or in 2–4-flowered fascicles, these concentrated on ends of stems and branches;</text>
      <biological_entity id="o22576" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o22577" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character constraint="on ends" constraintid="o22578" is_modifier="false" modifier="in 2-4-flowered fascicles" name="arrangement_or_density" src="d0_s4" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity id="o22578" name="end" name_original="ends" src="d0_s4" type="structure" />
      <biological_entity id="o22579" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <biological_entity id="o22580" name="branch" name_original="branches" src="d0_s4" type="structure" />
      <relation from="o22578" id="r2376" name="part_of" negation="false" src="d0_s4" to="o22579" />
      <relation from="o22578" id="r2377" name="part_of" negation="false" src="d0_s4" to="o22580" />
    </statement>
    <statement id="d0_s5">
      <text>involucel present, bractlets persistent, 6–9 (–12), connate basally.</text>
      <biological_entity id="o22581" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o22582" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="12" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="9" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx not or slightly accrescent, not inflated, campanulate, 5-lobed, lobes connate basally, not ribbed, ovate or lanceolate, apex obtuse, acuminate, or apiculate;</text>
      <biological_entity id="o22583" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o22584" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity id="o22585" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o22586" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla rotate to wide-campanulate, white or pink to lilac-purple or bluish, petals asymmetric, schizocarpic achenes, erect, not inflated, reniform to orbicular, rounded to angled, unwinged, not or slightly indurate, stellate-pilose-hairy or glabrous, indehiscent;</text>
      <biological_entity id="o22587" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o22588" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character char_type="range_value" from="rotate" name="shape" src="d0_s7" to="wide-campanulate" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s7" to="lilac-purple or bluish" />
      </biological_entity>
      <biological_entity id="o22589" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity id="o22590" name="achene" name_original="achenes" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="schizocarpic" value_original="schizocarpic" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s7" to="orbicular rounded" />
        <character char_type="range_value" from="reniform" name="shape" src="d0_s7" to="orbicular rounded" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="unwinged" value_original="unwinged" />
        <character is_modifier="false" modifier="not; slightly" name="texture" src="d0_s7" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-pilose-hairy" value_original="stellate-pilose-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="dehiscence" src="d0_s7" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>mericarps [8–] 10–20 [–24], 1-celled.</text>
      <biological_entity id="o22591" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o22592" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s8" to="10" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="24" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="20" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-celled" value_original="1-celled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 1 per mericarp, reniform-round, minutely rugulose, glabrous.</text>
      <biological_entity id="o22594" name="mericarp" name_original="mericarp" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>x = 21.</text>
      <biological_entity id="o22593" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character constraint="per mericarp" constraintid="o22594" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="reniform-round" value_original="reniform-round" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s9" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o22595" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="21" value_original="21" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, w, c Asia (Mediterranean region).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="w" establishment_means="introduced" />
        <character name="distribution" value="c Asia (Mediterranean region)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6–12 (4 in the flora).</discussion>
  <discussion>Two of the species of Althaea in the flora area occur in cultivation and rarely escape. The perennial species generally have stellate pubescence and the annual species generally have simple hairs; both have pink to purple anthers; the annuals are thought by some to be better placed within the genus Malva. Althaea is very similar to Alcea, and the latter has been combined with it by various authors. Hybrids between the two (×Alcathaea Hinsley) have been described, as have hybrids between Malva and Althaea (×Malvalthaea Iljin). See discussion under 13. Alcea for more information.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals, hispid-hirsute; petals to 1 1/2 times as long as calyx; staminal columns glabrous; anthers pale pink to almost white (yellow).</description>
      <determination>3 Althaea hirsuta</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials, stellate-hairy; petals (1.1–)2–3 times as long as calyx; staminal columns clavate-hairy or glabrous; anthers purple</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves unlobed or at most 3(–5)-lobed 1/2 to midrib; mericarps densely stellate-hairy.</description>
      <determination>4 Althaea officinalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves palmately divided 1/2+ to midrib, 5-lobed; mericarps glabrous or stellate-hairy</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles many-flowered; mericarps stellate-pilose-hairy.</description>
      <determination>1 Althaea armeniaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Peduncles 2–3-flowered; mericarps glabrous.</description>
      <determination>2 Althaea cannabina</determination>
    </key_statement>
  </key>
</bio:treatment>