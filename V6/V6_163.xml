<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Brathys</taxon_name>
    <taxon_name authority="(Greville &amp; Hooker) Torrey &amp; A. Gray" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 165. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section brathys;species drummondii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242416676</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sarothra</taxon_name>
    <taxon_name authority="Greville &amp; Hooker" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Misc.</publication_title>
      <place_in_publication>3: 236, plate 107. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sarothra;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brathys</taxon_name>
    <taxon_name authority="(Greville &amp; Hooker) Spach" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_hierarchy>genus Brathys;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <number>43.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, erect, branches strict, in distal 1/2, 1–8 dm, wiry.</text>
      <biological_entity id="o10144" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o10145" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="course" src="d0_s0" value="strict" value_original="strict" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10146" name="1/2" name_original="1/2" src="d0_s0" type="structure" />
      <relation from="o10145" id="r1103" name="in" negation="false" src="d0_s0" to="o10146" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes 4-lined.</text>
      <biological_entity id="o10147" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o10148" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect to suberect, sessile;</text>
      <biological_entity id="o10149" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="suberect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade linear or linear-subulate to linearlanceolate, 5–22 × 0.5–1 mm, subcoriaceous, margins recurved to revolute, apex acute to obtuse, basal vein 1, midrib unbranched.</text>
      <biological_entity id="o10150" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o10151" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o10152" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10153" name="vein" name_original="vein" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o10154" name="midrib" name_original="midrib" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences narrowly to broadly triangular, 1–12-flowered, branching mostly monochasial.</text>
      <biological_entity id="o10155" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-12-flowered" value_original="1-12-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s4" value="monochasial" value_original="monochasial" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 5–8 mm diam.;</text>
      <biological_entity id="o10156" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals narrowly oblong to linearlanceolate, subequal, 3–7 × 0.7–1.3 mm, apex acute;</text>
      <biological_entity id="o10157" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="to apex" constraintid="o10158" is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o10158" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3" from_unit="mm" is_modifier="true" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" is_modifier="true" name="width" src="d0_s6" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals golden yellow to orange-yellow, oblong, 4–7 mm;</text>
      <biological_entity id="o10159" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="golden yellow" name="coloration" src="d0_s7" to="orange-yellow" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 10–22, separate or obscurely 3-fascicled;</text>
      <biological_entity id="o10160" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="22" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="separate" value_original="separate" />
        <character is_modifier="false" modifier="obscurely" name="arrangement" src="d0_s8" value="3-fascicled" value_original="3-fascicled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles (0.5–) 0.8–1.5 mm;</text>
      <biological_entity id="o10161" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas broadly capitate.</text>
      <biological_entity id="o10162" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules narrowly ovoid to ovoid-cylindric, 3.5–7 × 2.5–3 mm, length 1–1.2 times sepals.</text>
      <biological_entity id="o10163" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly ovoid" name="shape" src="d0_s11" to="ovoid-cylindric" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character constraint="sepal" constraintid="o10164" is_modifier="false" name="length" src="d0_s11" value="1-1.2 times sepals" value_original="1-1.2 times sepals" />
      </biological_entity>
      <biological_entity id="o10164" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 0.9–1.1 mm;</text>
      <biological_entity id="o10165" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>testa finely scalariform.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 24.</text>
      <biological_entity id="o10166" name="testa" name_original="testa" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="finely" name="arrangement" src="d0_s13" value="scalariform" value_original="scalariform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10167" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early fall (Jul–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy or clay soil in open woods, old fields, waste or rocky places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" constraint="in open woods , old fields , waste or rocky places" />
        <character name="habitat" value="sandy" constraint="in open woods , old fields , waste or rocky places" />
        <character name="habitat" value="clay soil" constraint="in open woods , old fields , waste or rocky places" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="waste" />
        <character name="habitat" value="rocky places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Miss., Mo., N.C., Ohio, Okla., S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypericum drummondii is closely related to H. gentianoides.</discussion>
  
</bio:treatment>