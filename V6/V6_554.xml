<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
    <other_info_on_meta type="illustration_page">304</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">napaea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">dioica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 686. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus napaea;species dioica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220008991</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Glade mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subscapose, 1–2.2 (–3) m, from stout taproot.</text>
      <biological_entity id="o4305" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2.2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4306" name="taproot" name_original="taproot" src="d0_s0" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o4305" id="r497" name="from" negation="false" src="d0_s0" to="o4306" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves at base of plant largest and with longest petioles, progressively smaller and shorter-petioled distally, passing into bracts of inflorescence;</text>
      <biological_entity id="o4307" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="progressively" name="size" notes="" src="d0_s1" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="shorter-petioled" value_original="shorter-petioled" />
      </biological_entity>
      <biological_entity id="o4308" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o4309" name="plant" name_original="plant" src="d0_s1" type="structure" />
      <biological_entity constraint="longest" id="o4310" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <biological_entity id="o4311" name="bract" name_original="bracts" src="d0_s1" type="structure" />
      <biological_entity id="o4312" name="inflorescence" name_original="inflorescence" src="d0_s1" type="structure" />
      <relation from="o4307" id="r498" name="at" negation="false" src="d0_s1" to="o4308" />
      <relation from="o4308" id="r499" name="part_of" negation="false" src="d0_s1" to="o4309" />
      <relation from="o4307" id="r500" name="with" negation="false" src="d0_s1" to="o4310" />
      <relation from="o4307" id="r501" name="passing into" negation="false" src="d0_s1" to="o4311" />
      <relation from="o4307" id="r502" name="passing into" negation="false" src="d0_s1" to="o4312" />
    </statement>
    <statement id="d0_s2">
      <text>petiole to 1.2 m (at base of plant);</text>
      <biological_entity id="o4313" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="1.2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade deeply 5–9 (–11) -lobed, 5–50 × 7–65 cm, mostly somewhat wider than long, lobe apices acute or, especially on distal leaves, acuminate, surfaces simple or stellate-hairy (especially abaxially).</text>
      <biological_entity id="o4314" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="5-9(-11)-lobed" value_original="5-9(-11)-lobed" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="50" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="width" src="d0_s3" to="65" to_unit="cm" />
        <character constraint="than long , lobe apices" constraintid="o4315" is_modifier="false" name="width" src="d0_s3" value="mostly somewhat wider" value_original="mostly somewhat wider" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o4315" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character name="shape" src="d0_s3" value="," value_original="," />
      </biological_entity>
      <biological_entity constraint="distal" id="o4316" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4317" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o4315" id="r503" name="on" negation="false" src="d0_s3" to="o4316" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx lobes 1.5–2 mm, shorter than tube;</text>
      <biological_entity id="o4318" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="calyx" id="o4319" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character constraint="than tube" constraintid="o4320" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4320" name="tube" name_original="tube" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals 5–10 mm;</text>
      <biological_entity id="o4321" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4322" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>staminate flowers with 16–20 stamens, carpels 0;</text>
      <biological_entity id="o4323" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4324" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4325" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o4326" name="carpel" name_original="carpels" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o4324" id="r504" name="with" negation="false" src="d0_s6" to="o4325" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers with column of abortive stamens.</text>
      <biological_entity id="o4327" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4328" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4329" name="column" name_original="column" src="d0_s7" type="structure" />
      <biological_entity id="o4330" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="true" name="development" src="d0_s7" value="abortive" value_original="abortive" />
      </biological_entity>
      <relation from="o4328" id="r505" name="with" negation="false" src="d0_s7" to="o4329" />
      <relation from="o4329" id="r506" name="part_of" negation="false" src="d0_s7" to="o4330" />
    </statement>
    <statement id="d0_s8">
      <text>Mericarps 4–5 mm, rugose abaxially, especially near margins, and often laterally when mature, glabrous.</text>
      <biological_entity id="o4331" name="mericarp" name_original="mericarps" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s8" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o4332" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often laterally; laterally; when mature" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4331" id="r507" modifier="especially" name="near" negation="false" src="d0_s8" to="o4332" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds 3–4 mm. 2n = 30.</text>
      <biological_entity id="o4333" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4334" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–mid summer(–early fall).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid summer" from="early" />
        <character name="flowering time" char_type="atypical_range" to="early fall" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, flood plains, meadows and thickets, roadsides, alkaline soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="soil" modifier="alkaline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill., Ind., Iowa, Minn., Ohio, Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Napaea dioica is the only dioecious member of Malvoideae native in the Western Hemisphere. Reports of glade mallow in areas outside its natural range derive from confusion between it and Sida hermaphrodita (for example, Virginia; H. H. Iltis 1963) or from garden escapes (for example, Vermont; J. P. Brown 1941).</discussion>
  <discussion>The degree, distribution, and type of indument are variable. Forma stellata Fassett includes plants with mostly stellate hairs.</discussion>
  <discussion>Napaea dioica is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>