<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">392</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lechea</taxon_name>
    <taxon_name authority="Wilbur" date="unknown" rank="species">lakelae</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>76: 481. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus lechea;species lakelae</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101223</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Lakela’s pinweed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o11031" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: basal not produced;</text>
      <biological_entity id="o11032" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o11033" name="stem" name_original="stems" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>flowering erect, (20–) 30–40 cm, glabrous.</text>
      <biological_entity id="o11034" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves of flowering-stems alternate;</text>
      <biological_entity id="o11035" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o11036" name="flowering-stem" name_original="flowering-stems" src="d0_s3" type="structure" />
      <relation from="o11035" id="r1215" name="part_of" negation="false" src="d0_s3" to="o11036" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to narrowly elliptic, 7–15 × 0.5–1.5 mm, apex rounded to acute, surfaces glabrous.</text>
      <biological_entity id="o11037" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="linear to narrowly" value_original="linear to narrowly" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11038" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o11039" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 1 per axil, 0.7–2 mm.</text>
      <biological_entity id="o11040" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character constraint="per axil" constraintid="o11041" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11041" name="axil" name_original="axil" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx 1.5–1.9 mm, slightly indurate, discolored basally, outer sepals shorter than inner.</text>
      <biological_entity id="o11042" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11043" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s6" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s6" value="discolored" value_original="discolored" />
      </biological_entity>
      <biological_entity constraint="outer" id="o11044" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character constraint="than inner sepals" constraintid="o11045" is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="inner" id="o11045" name="sepal" name_original="sepals" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Capsules ovoid to globose, 1.2–1.6 × 1–1.4 mm, ± equaling calyx.</text>
      <biological_entity id="o11046" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s7" to="globose" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s7" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11047" name="calyx" name_original="calyx" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 2–3.</text>
      <biological_entity id="o11048" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer; fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy, open sites, coastal woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="dry" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="coastal woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Lechea lakelae is known only from coastal scrub woodlands in Collier County; it has not been collected since 1987 and may have been extirpated.</discussion>
  
</bio:treatment>