<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Laurence J. Dorr</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Nuttall" date="1821" rank="genus">CALLIRHOË</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>2: 181. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus callirhoë;</taxon_hierarchy>
    <other_info_on_name type="etymology">Derivation uncertain; possibly Greek kallos, beautiful, and rhoias, corn poppy, alluding to resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">105128</other_info_on_name>
  </taxon_identification>
  <number>20.</number>
  <other_name type="common_name">Poppy mallow</other_name>
  <other_name type="common_name">wine cup</other_name>
  <other_name type="common_name">wild hollyhock</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, perennial, or sometimes biennial, hairy, hairs stellate, 4-rayed, and/or simple, or plants glabrous and glaucous.</text>
      <biological_entity id="o15593" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15594" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending, or decumbent.</text>
      <biological_entity id="o15596" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, caducous, or tardily deciduous, ovate, linearlanceolate to subulate, auriculate, or rhombic-ovate;</text>
      <biological_entity id="o15597" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15598" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rhombic-ovate" value_original="rhombic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rhombic-ovate" value_original="rhombic-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade often pedate, suborbiculate, cordate, ovate, triangular, or hastate, palmately cleft or entire and crenate, base truncate, cordate, or sagittate to hastate, margins 1 per carpel;</text>
      <biological_entity id="o15599" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o15600" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s3" value="pedate" value_original="pedate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="hastate" value_original="hastate" />
        <character is_modifier="false" modifier="palmately" name="architecture_or_shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o15601" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="sagittate" name="shape" src="d0_s3" to="hastate" />
        <character char_type="range_value" from="sagittate" name="shape" src="d0_s3" to="hastate" />
      </biological_entity>
      <biological_entity id="o15602" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="per carpel" constraintid="o15603" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15603" name="carpel" name_original="carpel" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>styles 10–28-branched;</text>
      <biological_entity id="o15604" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o15605" name="style" name_original="styles" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="10-28-branched" value_original="10-28-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stigmas introrsely decurrent, filiform.</text>
      <biological_entity id="o15606" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15607" name="stigma" name_original="stigmas" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="introrsely" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits schizocarps, erect, not inflated, oblate or depressed-discoid, indurate, reticulate and rugose, strigose or glabrous, indehiscent or dehiscent (annual species only);</text>
      <biological_entity constraint="fruits" id="o15608" name="schizocarp" name_original="schizocarps" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="depressed-discoid" value_original="depressed-discoid" />
        <character is_modifier="false" name="texture" src="d0_s6" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s6" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="relief" src="d0_s6" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="dehiscence" src="d0_s6" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s6" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>mericarps 10–28, 2-celled, prominently obtusely beaked or not, drying tan or brown, distal locule sterile, lower 1-seeded.</text>
      <biological_entity id="o15609" name="mericarp" name_original="mericarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="28" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-celled" value_original="2-celled" />
        <character is_modifier="false" modifier="prominently obtusely" name="architecture_or_shape" src="d0_s7" value="beaked" value_original="beaked" />
        <character name="architecture_or_shape" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15610" name="locule" name_original="locule" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="position" src="d0_s7" value="lower" value_original="lower" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-seeded" value_original="1-seeded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1 per locule, reniform or reniform-pyriform (annual species only), glabrous.</text>
      <biological_entity id="o15612" name="locule" name_original="locule" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>x = 14, 15.</text>
      <biological_entity id="o15611" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character constraint="per locule" constraintid="o15612" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s8" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="reniform-pyriform" value_original="reniform-pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o15613" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="14" value_original="14" />
        <character name="quantity" src="d0_s9" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 9 (9 in the flora).</discussion>
  <discussion>Several species of Callirhoë are gynodioecious; populations of C. alcaeoides, C. involucrata, and C. leiocarpa have individuals with either bisexual or functionally pistillate (that is, male-sterile) flowers. In these species the functionally pistillate flowers can be recognized by their reduced number of anther sacs, failure of these anther sacs to dehisce, stigmatic lobes often conspicuous at early anthesis, reduced petal size, and in C. alcaeoides shorter calyx lobe length. A few populations of C. pedata in Arkansas exhibit a corolla size dimorphism suggesting that this species too may be gynodioecious. Several species of Callirhoë are cultivated and may escape. All taxa of this genus occur within the flora area except C. involucrata var. tenuissima Palmer ex Baker f., which is wholly Mexican.</discussion>
  <references>
    <reference>Bates, D. M., L. J. Dorr, and O. J. Blanchard. 1989. Chromosome numbers in Callirhoë (Malvaceae). Brittonia 41: 143–151.</reference>
    <reference>Dorr, L. J. 1990. A revision of the North American genus Callirhoë (Malvaceae). Mem. New York Bot. Gard. 56: 1–75.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucellar bractlets (1–)3</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucellar bractlets 0</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyx lobes divergent in bud, not forming point.</description>
      <determination>5 Callirhoë involucrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyx lobes valvate in bud, forming apiculate or acuminate point</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucellar bractlets spatulate or obovate; mericarps dehiscent; leaf blades triangular or ovate-lanceolate, unlobed or shallowly 3- or 5-lobed.</description>
      <determination>9 Callirhoë triangulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucellar bractlets linear, lanceolate, or ovate; mericarps indehiscent; leaf blades cordate, ovate, suborbiculate, hastate, or triangular, 3-, 5-, or 7-lobed</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems 1(–6), stiffly erect, densely hairy (hairs stellate, 6–8-rayed); mericarps hairy.</description>
      <determination>8 Callirhoë scabriuscula</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems 1–10, weakly erect, ascending, or decumbent, glabrate or hairy (hairs stellate, mostly 4-rayed); mericarps sparsely hairy</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade lobes broad, oblong or obovate; involucellar bractlets lanceolate or ovate, 8–22 × 1–4 mm; stems hairy (hairs stellate, 4-rayed and often simple, spreading or retrorse).</description>
      <determination>6 Callirhoë bushii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade lobes narrowly lanceolate, linear, linear-falcate, or lanceolate-falcate; involucellar bractlets narrowly linear, 2–10.5 × 0.1–0.7 mm; stems hairy (hairs stellate, 4-rayed or simple) or sometimes glabrate.</description>
      <determination>7 Callirhoë papaver</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Annuals (biennials); stipules auriculate; mericarp beaks subtended by 3-lobed collars.</description>
      <determination>4 Callirhoë leiocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perennials; stipules linear-lanceolate, lanceolate to ovate or subulate; mericarp beaks subtended by 2-lobed, weakly developed collars or collars absent</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mericarps hairy, beaks prominent, protruding beyond seed-containing portions, forming distal 1/4–1/3 of each mericarp</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mericarps glabrous or sparingly hairy, beaks not prominent, not or only slightly elevated beyond seed-containing portions, forming less than distal 1/4 of each mericarp</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences racemose; flowers (in population samples) usually bisexual, rarely functionally pistillate; petals usually reddish purple, rarely white or pink.</description>
      <determination>2 Callirhoë pedata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences racemose, racemes often appearing corymbose or subumbellate; flowers (in population samples) bisexual or functionally pistillate; petals white, pink, or mauve.</description>
      <determination>3 Callirhoë alcaeoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stipules caducous; inflorescences paniculate; leaf blades (3–)5–10-lobed.</description>
      <determination>1 Callirhoë digitata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stipules persistent; inflorescences racemose; leaf blades 3–5-lobed</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems glabrous; leaf blades with simple hairs abaxially.</description>
      <determination>2 Callirhoë pedata</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Stems hairy (hairs stellate, 4-rayed or simple) or sometimes glabrate; leaf blades hairy (hairs 4-rayed and simple) abaxially.</description>
      <determination>7 Callirhoë papaver</determination>
    </key_statement>
  </key>
</bio:treatment>