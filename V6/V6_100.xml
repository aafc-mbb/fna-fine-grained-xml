<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">62</other_info_on_meta>
    <other_info_on_meta type="illustration_page">60</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">begoniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">begonia</taxon_name>
    <taxon_name authority="Willdenow" date="1805" rank="species">cucullata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4: 414. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family begoniaceae;genus begonia;species cucullata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242433861</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Wax begonia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (rhizomatous), usually glabrous, sometimes sparsely hairy.</text>
      <biological_entity id="o12267" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems [10–] 30–70 [–100] cm.</text>
      <biological_entity id="o12268" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules lanceolate to oblong, 7–19 × 3–6 mm;</text>
      <biological_entity id="o12269" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12270" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="19" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 5–46 mm, glabrous;</text>
      <biological_entity id="o12271" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12272" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="46" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade asymmetric, ovate to ± reniform, (28–) 46–72 [–80] × (28–) 33–85 mm, base cuneate on shorter side, usually rounded on longer one, margins not lobed, crenate, teeth apices setose, otherwise eciliate, apex obtuse, surfaces glabrous (or glabrate to sparsely hairy in Alabama specimens).</text>
      <biological_entity id="o12273" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12274" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="more or less reniform" />
        <character char_type="range_value" from="28" from_unit="mm" name="atypical_length" src="d0_s4" to="46" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="72" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="46" from_unit="mm" name="length" src="d0_s4" to="72" to_unit="mm" />
        <character char_type="range_value" from="28" from_unit="mm" name="atypical_width" src="d0_s4" to="33" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="33" from_unit="mm" name="width" src="d0_s4" to="85" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12275" name="base" name_original="base" src="d0_s4" type="structure">
        <character constraint="on shorter side" constraintid="o12276" is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character constraint="on longer one" constraintid="o12277" is_modifier="false" modifier="usually" name="shape" notes="" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o12276" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="longer" id="o12277" name="one" name_original="one" src="d0_s4" type="structure" />
      <biological_entity id="o12278" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity constraint="teeth" id="o12279" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s4" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o12280" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o12281" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 22–75 mm (in fruit);</text>
      <biological_entity id="o12282" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="22" from_unit="mm" name="some_measurement" src="d0_s5" to="75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts lanceolate to ovate.</text>
      <biological_entity id="o12283" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers white to pink;</text>
      <biological_entity id="o12284" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminate: tepals 4, outer 2 (sepals) suborbiculate or reniform, 7–10 mm, inner 2 (petals) narrowly obovate, 5–7 mm;</text>
      <biological_entity id="o12285" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12286" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s8" value="outer" value_original="outer" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s8" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s8" value="inner" value_original="inner" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 24–33;</text>
      <biological_entity id="o12287" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12288" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="24" name="quantity" src="d0_s9" to="33" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate: tepals 4 or 5, obovate, 6–9 mm.</text>
      <biological_entity id="o12289" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o12290" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s10" unit="or" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules 8–15 × 6–12 mm, larger wings deltate-rounded, 10–17 mm wide, smaller 3.5–5 mm wide.</text>
      <biological_entity id="o12291" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 34, 56 (South America).</text>
      <biological_entity constraint="larger" id="o12292" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate-rounded" value_original="deltate-rounded" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s11" to="17" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s11" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12293" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="34" value_original="34" />
        <character name="quantity" src="d0_s12" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams in swamps, floodplain woodlands, cabbage palmetto hummocks, wet ditches, wet and mucky soil, often in shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" constraint="in swamps , floodplain woodlands , cabbage palmetto hummocks , wet ditches , wet and mucky soil , often in shade" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="floodplain woodlands" />
        <character name="habitat" value="cabbage palmetto hummocks" />
        <character name="habitat" value="wet ditches" />
        <character name="habitat" value="wet" />
        <character name="habitat" value="mucky soil" />
        <character name="habitat" value="shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Fla., Ga.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Begonia cucullata is found throughout Florida and recently has been collected in nearby Alabama (Conecuh County). The Alabama plants are typical of this species except that the leaves and petioles are glabrate to sparsely hairy; it may require further study. The distinction between varieties is not always clear (B. G. Schubert 1954) and they are not recognized here. Florida specimens often are treated as var. hookeri (de Candolle) L. B. Smith &amp; B. G. Schubert, a name that has been put in synonymy of var. cucullata (L. B. Smith et al. 1986). From the morphologic variation observed, it is possible that this species escaped repeatedly from cultivation. It appears to be able to produce abundant seeds; capsules are regularly found on specimens and sometimes are abundant. Known horticulturally as B. semperflorens, B. cucullata has played an important role in ornamental horticulture.</discussion>
  <references>
    <reference>Schubert, B. G. 1954. Begonia cucullata and its varieties. Natl. Hort. Mag. 1954(Oct.): 244–248.</reference>
  </references>
  
</bio:treatment>