<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">293</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="Allioni" date="unknown" rank="species">nicaeensis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Pedem.</publication_title>
      <place_in_publication>2: 40. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species nicaeensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250023532</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Bull mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or biennial, 0.2–0.6 m, sparsely to densely hairy, hairs both simple and stellate.</text>
      <biological_entity id="o7944" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="0.2" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7945" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent or trailing to ascending, villous-hirsute.</text>
      <biological_entity id="o7946" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character name="growth_form" src="d0_s1" value="trailing to ascending" value_original="trailing to ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous-hirsute" value_original="villous-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, ovate to broadly ovate, 4–6 × 3–5 mm;</text>
      <biological_entity id="o7947" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7948" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="broadly ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–5 times as long as blade;</text>
      <biological_entity id="o7949" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7950" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o7951" is_modifier="false" name="length" src="d0_s3" value="2-5 times as long as blade" />
      </biological_entity>
      <biological_entity id="o7951" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade semicircular or reniform, 2–4 × 2–4 cm, sometimes to 12 cm in young plants, base cordate to nearly truncate, distalmost leaves sometimes wide-cuneate, margins crenate or dentate, undulate, or with 5–7 shallow, lobes, apex obtuse, rounded, or acute, surfaces sparsely hairy, hairs simple.</text>
      <biological_entity id="o7952" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7953" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="semicircular" value_original="semicircular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" constraint="in plants" constraintid="o7954" from="0" from_unit="cm" modifier="sometimes" name="some_measurement" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7954" name="plant" name_original="plants" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o7955" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="nearly truncate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o7956" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="wide-cuneate" value_original="wide-cuneate" />
      </biological_entity>
      <biological_entity id="o7957" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o7958" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o7959" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="with 5-7 shallow" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7960" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7961" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences stellate;</text>
      <biological_entity id="o7962" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals pink to lavender, drying bluish, usually with darker veins, 5–15 mm, subequal to or length slightly less than 2 times calyx, glabrous or nearly so;</text>
      <biological_entity id="o7963" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s6" to="lavender" />
        <character is_modifier="false" name="condition" src="d0_s6" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bluish" value_original="bluish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="15" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character constraint="calyx" constraintid="o7965" is_modifier="false" modifier="slightly" name="length" src="d0_s6" value="0-2 times calyx" value_original="0-2 times calyx" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s6" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o7964" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o7965" name="calyx" name_original="calyx" src="d0_s6" type="structure" />
      <relation from="o7963" id="r855" modifier="usually" name="with" negation="false" src="d0_s6" to="o7964" />
    </statement>
    <statement id="d0_s7">
      <text>staminal column 2–2.5 (–3) mm, densely, retrorsely puberulent, hairs simple;</text>
      <biological_entity constraint="staminal" id="o7966" name="column" name_original="column" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="densely; retrorsely" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o7967" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 7–10-branched;</text>
      <biological_entity id="o7968" name="style" name_original="style" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="7-10-branched" value_original="7-10-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas 7–10.</text>
      <biological_entity id="o7969" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Schizocarps 6–7 mm diam.;</text>
      <biological_entity id="o7970" name="schizocarp" name_original="schizocarps" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mericarps 7–10, 3 mm, thick, as wide as long, margins sharp-angled but not winged, conspicuously, deeply reticulate-pitted apically, surfaces densely hirsute or glabrous.</text>
      <biological_entity id="o7971" name="mericarp" name_original="mericarps" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="width" src="d0_s11" value="thick" value_original="thick" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o7972" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="sharp-angled" value_original="sharp-angled" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="conspicuously; deeply; apically" name="relief" src="d0_s11" value="reticulate-pitted" value_original="reticulate-pitted" />
      </biological_entity>
      <biological_entity id="o7973" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds dark-brown, 2–2.5 mm. 2n = 42.</text>
      <biological_entity id="o7974" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7975" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400(–1200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Calif.; s Europe; w Asia; n Africa; introduced also in Mexico, South America (Argentina, Chile), Atlantic Islands (Macaronesia), Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="South America (Chile)" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva nicaeensis has been collected as a waif in Massachusetts and New Jersey and has been reported in British Columbia and Montana; vouchers have not been found. One vouchered collection has been reported from Mobile County, Alabama. In the flora area, it is found most commonly in the Mediterranean climate of California. It may not be established elsewhere within our range. In some older treatments it was identified as or included within M. rotundifolia, a name rejected because of its inconsistent use for this as well as for M. pusilla and other species. It is similar to M. sylvestris, except for its decumbent habit and smaller flowers.</discussion>
  
</bio:treatment>