<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">MALVA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 687. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 308. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin name derived from Greek malacho, to soften, alluding to emollient qualities of some species</other_info_on_name>
    <other_info_on_name type="fna_id">119571</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(de Candolle) Alefeld" date="unknown" rank="genus">Axolopha</taxon_name>
    <taxon_hierarchy>genus Axolopha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Medikus" date="unknown" rank="genus">Bismalva</taxon_name>
    <taxon_hierarchy>genus Bismalva</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="genus">Saviniona</taxon_name>
    <taxon_hierarchy>genus Saviniona</taxon_hierarchy>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Mallow</other_name>
  <other_name type="common_name">mauve</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, biennial, or perennial, subshrubs, or shrubs, glabrous or hairy, hairs stellate or simple.</text>
      <biological_entity id="o17493" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o17496" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, ascending, or trailing.</text>
      <biological_entity id="o17497" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent or deciduous, linear, lanceolate, triangular, or ovate to ± falcate;</text>
      <biological_entity id="o17498" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o17499" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="more or less falcate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="more or less falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade orbiculate or reniform, unlobed or palmately 3–7 (–9) -lobed or divided, base cordate to truncate, margins crenate to dentate.</text>
      <biological_entity id="o17500" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17501" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-7(-9)-lobed" value_original="3-7(-9)-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" src="d0_s3" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s3" value="3-7(-9)-lobed" value_original="3-7(-9)-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o17502" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s3" to="truncate" />
      </biological_entity>
      <biological_entity id="o17503" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences usually axillary, flowers usually in fascicles, sometimes solitary, sometimes terminal racemes;</text>
      <biological_entity id="o17504" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o17505" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="in fascicles; sometimes" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o17506" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>involucel present, bractlets persistent, 3, distinct or basally connate (or recurved pedicel), not inflated, oblate-discoid, usually depressed in center, around broad axis, without persistent swollen style base, ± indurate, glabrous or hairy;</text>
      <biological_entity id="o17507" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17508" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s5" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="oblate-discoid" value_original="oblate-discoid" />
        <character constraint="in center" constraintid="o17509" is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="depressed" value_original="depressed" />
        <character is_modifier="false" modifier="more or less" name="texture" notes="" src="d0_s5" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17509" name="center" name_original="center" src="d0_s5" type="structure" />
      <biological_entity id="o17510" name="axis" name_original="axis" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="around" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity constraint="style" id="o17511" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="shape" src="d0_s5" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o17508" id="r1864" name="around broad" negation="false" src="d0_s5" to="o17510" />
      <relation from="o17508" id="r1865" name="without" negation="false" src="d0_s5" to="o17511" />
    </statement>
    <statement id="d0_s6">
      <text>mericarps 6–15 (–20), drying tan or brown, 1-celled, wedge-shaped (triangular in cross-section), oblong to reniform, beak or cusp absent, sides thin and papery or thicker, margins usually edged, apex rounded, indehiscent.</text>
      <biological_entity id="o17512" name="mericarp" name_original="mericarps" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-celled" value_original="1-celled" />
        <character is_modifier="false" name="shape" src="d0_s6" value="wedge--shaped" value_original="wedge--shaped" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="reniform" />
      </biological_entity>
      <biological_entity id="o17513" name="beak" name_original="beak" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17514" name="cusp" name_original="cusp" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o17515" name="side" name_original="sides" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s6" value="papery" value_original="papery" />
        <character is_modifier="false" name="width" src="d0_s6" value="thicker" value_original="thicker" />
      </biological_entity>
      <biological_entity id="o17516" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="edged" value_original="edged" />
      </biological_entity>
      <biological_entity id="o17517" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="dehiscence" src="d0_s6" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds 1 per mericarp, adherent to mericarp wall, usually not readily separated from it, reniform-rounded, notched, glabrous.</text>
      <biological_entity id="o17519" name="mericarp" name_original="mericarp" src="d0_s7" type="structure" />
      <biological_entity constraint="mericarp" id="o17520" name="wall" name_original="wall" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>x = 21.</text>
      <biological_entity id="o17518" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character constraint="per mericarp" constraintid="o17519" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character constraint="to mericarp wall" constraintid="o17520" is_modifier="false" name="fusion" notes="" src="d0_s7" value="adherent" value_original="adherent" />
        <character constraint="from it" is_modifier="false" modifier="usually not readily" name="arrangement" notes="" src="d0_s7" value="separated" value_original="separated" />
        <character is_modifier="false" name="shape" src="d0_s7" value="reniform-rounded" value_original="reniform-rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="notched" value_original="notched" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o17521" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="21" value_original="21" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Eurasia, n Africa (especially Mediterranean region); introduced nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa (especially Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="nearly worldwide" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 30–40 (11 in the flora).</discussion>
  <discussion>Some species of Malva are weedy; five or six in the flora area generally occur in cultivation as ornamentals or as vegetables and occasionally escape. Some species previously treated within Lavatera (see M. F. Ray 1995, 1998) are here included in Malva based upon molecular evidence. Traditionally, Lavatera and Malva were separated by the presence of partially connate relatively wide involucellar bractlets in the former and distinct generally narrow bractlets in the latter. The annual species of Althaea (sect. Hirsutae Iljin ex Olyanitskaya &amp; Tzvelev) may also belong within Malva, but have been kept separate here. Nomenclature in Althaea, Lavatera, and Malva is still in flux and a satisfactory classification is not yet available. Intergeneric hybrids among some species of all three genera suggest a close relationship.</discussion>
  <references>
    <reference>Ray, M. F. 1995. Systematics of Lavatera and Malva (Malvaceae, Malveae)—A new perspective. Pl. Syst. Evol. 198: 29–53.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, 1–4 m; petals 25–45 mm; calyces 12–15 mm; mericarps 6–10.</description>
      <determination>3 Malva assurgentiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs or subshrubs, 0.2–3 m; petals 3–35(–45) mm (if subshrubs, petals to 20 mm); calyces 3–10(–15) mm; mericarps 6–15(–20)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaf blades deeply (3–)5–7-lobed; petals 20–35 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal leaf blades unlobed or shallowly lobed; petals 3–30(–45) mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hairs stellate-canescent; involucellar bractlets ovate or ovate-deltate to obovate; mericarps 18–20, glabrous or sparsely hairy.</description>
      <determination>1 Malva alcea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hairs usually simple, sometimes stellate; involucellar bractlets linear to narrowly oblanceolate or elliptic; mericarps 11–15, densely hirsute apically.</description>
      <determination>4 Malva moschata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals (12–)16–30(–45) mm, length 2 1/2–3(–4) times calyx; involucellar bractlets distinct, sometimes adnate to calyx in basal 1 mm.</description>
      <determination>10 Malva sylvestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 3–15(–20) mm, length 1–2(–5) times calyx; involucellar bractlets distinct or partly connate and/or adnate to calyx (if petals longer than 14 mm, involucellar bractlets connate)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Involucellar bractlets connate in proximal 1/3–1/2, adnate to calyx, ovate-deltate, ovate, or round; petals 10–20 mm; stems usually erect, rarely prostrate, 1–3 m</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Involucellar bractlets distinct, basally adnate to calyx (in M. nicaeensis) or not, filiform, linear, lanceolate, oblong-lanceolate, ovate, or obovate; petals 3–15 mm; stems erect or ascending to decumbent, procumbent, prostrate, or trailing, 0.2–0.8(–2.5) m</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucel longer than calyx; leaf blade surfaces densely soft-stellate-hairy; petals rose to lavender with 5 darker veins; stem base usually woody.</description>
      <determination>2 Malva arborea</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucel shorter than calyx; leaf blade surfaces sparsely stellate-hairy; petals pale pink to white, usually with 3 darker veins; stem base not woody.</description>
      <determination>8 Malva pseudolavatera</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucellar bractlets basally adnate to calyx, broadly lanceolate to ovate or obovate; petals drying bluish, usually with darker veins.</description>
      <determination>6 Malva nicaeensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucellar bractlets not adnate to calyx, filiform, linear, lanceolate, or oblong-lanceolate; petals drying pinkish or whitish, or faded, veins not much darker</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Involucellar bractlets filiform to linear; calyces accrescent, lobes spreading outward exposing mericarps; petals 3–4.5(–5) mm, white to pale lilac; mericarp margins narrowly winged, toothed.</description>
      <determination>7 Malva parviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Involucellar bractlets linear, oblanceolate, or lanceolate; calyces not accrescent, or, if so, lobes usually enclosing mericarps; petals (3–)5–13 mm, pale lilac, pink, pinkish, purplish, to nearly white or whitish; mericarp margins not winged, sometimes toothed</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems erect; plants 0.5–2.5 m; leaf blades 3–10(–25) cm; pedicels stout and rigid in fruit.</description>
      <determination>11 Malva verticillata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stems prostrate or trailing to ascending; plants usually 0.2–0.6 m; leaf blades 1–3.5(–6) cm; pedicels slender and flexible in fruit</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals 6–13 mm, length 2 times calyx; mericarps hairy, smooth to slightly roughened or reticulate.</description>
      <determination>5 Malva neglecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals 3–6 mm, length subequal to or slightly exceeding calyx; mericarps hairy or glabrate, strongly rugose-reticulate.</description>
      <determination>9 Malva pusilla</determination>
    </key_statement>
  </key>
</bio:treatment>