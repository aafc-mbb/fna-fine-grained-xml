<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="Rose" date="1893" rank="species">orcuttii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>1: 289. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species orcuttii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101194</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Carrizo Creek globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually annual, sometimes biennial.</text>
      <biological_entity id="o7775" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s0" value="biennial" value_original="biennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, yellow, 5–12 dm, stellate-canescent.</text>
      <biological_entity id="o7776" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s1" to="12" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-canescent" value_original="stellate-canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades yellow-green to pale green, trullate with straight or rounded appearance or triangular with subhastate lobes, apical lobes distinctly triangular, 2-lobed, 3–5 cm, not rugose, base tapered to truncate, margins entire or wavy, surfaces stellate-canescent.</text>
      <biological_entity id="o7777" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s2" to="pale green" />
        <character constraint="with " constraintid="o7778" is_modifier="false" name="shape" src="d0_s2" value="trullate" value_original="trullate" />
      </biological_entity>
      <biological_entity id="o7778" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="subhastate" value_original="subhastate" />
        <character is_modifier="true" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="true" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity constraint="apical" id="o7779" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o7780" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="tapered to truncate" value_original="tapered to truncate" />
      </biological_entity>
      <biological_entity id="o7781" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity id="o7782" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-canescent" value_original="stellate-canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences paniculate, crowded, many-flowered, flowers clustered, tip not leafy;</text>
      <biological_entity id="o7783" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="many-flowered" value_original="many-flowered" />
      </biological_entity>
      <biological_entity id="o7784" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets green to tan.</text>
      <biological_entity id="o7785" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o7786" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 4–7 mm;</text>
      <biological_entity id="o7787" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o7788" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals red-orange, 10–12 mm;</text>
      <biological_entity id="o7789" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o7790" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow.</text>
      <biological_entity id="o7791" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o7792" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps ± hemispheric;</text>
      <biological_entity id="o7793" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 12–17, 2.5–3 × 2–3 mm, chartaceous, nonreticulate dehiscent part 30–40% of height, tip rounded, indehiscent part wider than dehiscent part.</text>
      <biological_entity id="o7794" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="17" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o7795" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o7796" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="30-40%" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o7797" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="than dehiscent part" constraintid="o7798" is_modifier="false" name="width" src="d0_s9" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o7798" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 1 per mericarp, brown, glabrous or pubescent.</text>
      <biological_entity id="o7800" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 10.</text>
      <biological_entity id="o7799" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character constraint="per mericarp" constraintid="o7800" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7801" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, sandy, ± alkaline places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline places" modifier="dry sandy" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>?50–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="50" from_unit="m" constraint="? " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif.; Mexico (Baja California, Baja California Sur, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphaeralcea orcuttii has distinctive trullate leaf blades and an erect habit.</discussion>
  
</bio:treatment>