<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Norman K. B. Robson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HYPERICUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 783. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 341. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus HYPERICUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hyper, above, and eikon, image, alluding to ancient Greek custom of decorating religious figures with Hypericum species to ward off evil spirits</other_info_on_name>
    <other_info_on_name type="fna_id">116180</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Ascyrum</taxon_name>
    <taxon_hierarchy>genus Ascyrum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small" date="unknown" rank="genus">Crookea</taxon_name>
    <taxon_hierarchy>genus Crookea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small" date="unknown" rank="genus">Sanidophyllum</taxon_name>
    <taxon_hierarchy>genus Sanidophyllum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Sarothra</taxon_name>
    <taxon_hierarchy>genus Sarothra</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">St. John’s wort</other_name>
  <other_name type="common_name">St. Andrew’s cross</other_name>
  <other_name type="common_name">millepertuis</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, subshrubs, or shrubs [trees], sometimes rhizomatous, glabrous or hairy, with glandular canals, lacunae, or dots containing resins or waxes (amber), essential oils (pale, translucent), and/or, sometimes, hypericin and pseudohypericin (black or red) in various parts.</text>
      <biological_entity id="o1870" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="subshrub" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o1873" name="canal" name_original="canals" src="d0_s0" type="structure" />
      <biological_entity id="o1874" name="lacuna" name_original="lacunae" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s0" value="dots" value_original="dots" />
      </biological_entity>
      <biological_entity id="o1875" name="resins" name_original="resins" src="d0_s0" type="substance" />
      <biological_entity id="o1876" name="wax" name_original="waxes" src="d0_s0" type="substance" />
      <biological_entity id="o1877" name="oil" name_original="oils" src="d0_s0" type="substance" />
      <biological_entity id="o1878" name="pseudohypericin" name_original="pseudohypericin" src="d0_s0" type="substance" />
      <biological_entity id="o1879" name="part" name_original="parts" src="d0_s0" type="structure">
        <character is_modifier="true" name="variability" src="d0_s0" value="various" value_original="various" />
      </biological_entity>
      <relation from="o1870" id="r213" name="with" negation="false" src="d0_s0" to="o1873" />
      <relation from="o1870" id="r214" name="with" negation="false" src="d0_s0" to="o1873" />
      <relation from="o1874" id="r215" name="containing" negation="false" src="d0_s0" to="o1875" />
      <relation from="o1874" id="r216" name="containing" negation="false" src="d0_s0" to="o1876" />
      <relation from="o1878" id="r217" modifier="sometimes" name="in" negation="false" src="d0_s0" to="o1879" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes terete (not lined) or 2-lined, 4-lined, or 6-lined at first (lines usually raised), then sometimes becoming angled, terete, or winged;</text>
      <biological_entity id="o1880" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o1881" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="2-lined" value_original="2-lined" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="6-lined" value_original="6-lined" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="6-lined" value_original="6-lined" />
        <character is_modifier="false" modifier="sometimes becoming" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark smooth or striate, sometimes corky, punctiform.</text>
      <biological_entity id="o1882" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o1883" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_texture" src="d0_s2" value="corky" value_original="corky" />
        <character is_modifier="false" name="shape" src="d0_s2" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal, cymose, 2+-flowered, or flowers solitary, branching stellate [cupulate];</text>
      <biological_entity id="o1884" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2+-flowered" value_original="2+-flowered" />
      </biological_entity>
      <biological_entity id="o1885" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals persistent or deciduous, (3–) 4–5, distinct or ± connate, margins sometimes glandular-ciliate;</text>
      <biological_entity id="o1886" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o1887" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glandular-ciliate" value_original="glandular-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals persistent or deciduous, (3–) 4–5 [–6], contorted, yellow to orange, sometimes red-tinged;</text>
      <biological_entity id="o1888" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s5" to="orange" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="red-tinged" value_original="red-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stamens persistent or deciduous, (5–) 10–300 (–650), in continuous or interrupted ring or in (3–) 4–5 fascicles, fascicles distinct or connate, each with 1–60+ stamens;</text>
      <biological_entity id="o1889" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s6" to="10" to_inclusive="false" />
        <character char_type="range_value" from="300" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="650" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="300" />
      </biological_entity>
      <biological_entity id="o1890" name="stamen" name_original="stamens" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s6" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o1889" id="r218" name="in continuous or interrupted ring or in" negation="false" src="d0_s6" to="o1890" />
    </statement>
    <statement id="d0_s7">
      <text>filaments distinct or basally connate;</text>
      <biological_entity id="o1891" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s7" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers yellow to orange, oblong to ellipsoid, almost isodiametric, sometimes with amber or black gland on connective;</text>
      <biological_entity id="o1892" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="orange" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="ellipsoid" />
        <character is_modifier="false" modifier="almost" name="architecture_or_shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o1893" name="amber" name_original="amber" src="d0_s8" type="substance" />
      <biological_entity id="o1894" name="connective" name_original="connective" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="black gland" value_original="black gland" />
      </biological_entity>
      <relation from="o1892" id="r219" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o1893" />
      <relation from="o1892" id="r220" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o1894" />
    </statement>
    <statement id="d0_s9">
      <text>staminode fascicles 0 [3];</text>
      <biological_entity id="o1895" name="staminode" name_original="staminode" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="fascicles" value_original="fascicles" />
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character name="atypical_quantity" src="d0_s9" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 2–5-merous;</text>
    </statement>
    <statement id="d0_s11">
      <text>placentation axile to parietal;</text>
      <biological_entity id="o1896" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-5-merous" value_original="2-5-merous" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="placentation" value_original="placentation" />
        <character char_type="range_value" from="axile" name="arrangement" src="d0_s11" to="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 2+ on each placenta;</text>
      <biological_entity id="o1897" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="on placenta" constraintid="o1898" from="2" name="quantity" src="d0_s12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1898" name="placenta" name_original="placenta" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>styles distinct or ± connate basally, spreading to ± appressed.</text>
      <biological_entity id="o1899" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="more or less; basally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="more or less appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 2–5-valved, sometimes with glandular vittae or vesicles.</text>
      <biological_entity id="o1900" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="2-5-valved" value_original="2-5-valved" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o1901" name="vitta" name_original="vittae" src="d0_s14" type="structure" />
      <biological_entity id="o1902" name="vesicle" name_original="vesicles" src="d0_s14" type="structure" />
      <relation from="o1900" id="r221" modifier="sometimes" name="with" negation="false" src="d0_s14" to="o1901" />
      <relation from="o1900" id="r222" modifier="sometimes" name="with" negation="false" src="d0_s14" to="o1902" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds narrowly cylindric to ellipsoid, sometimes carinate;</text>
      <biological_entity id="o1903" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="narrowly cylindric" name="shape" src="d0_s15" to="ellipsoid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s15" value="carinate" value_original="carinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>testa foveolate or reticulate to scalariform [papillose].</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 12, 9–7, 6 (dihaploid).</text>
      <biological_entity id="o1904" name="testa" name_original="testa" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="relief" src="d0_s16" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="scalariform" value_original="scalariform" />
      </biological_entity>
      <biological_entity constraint="x" id="o1905" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s17" to="7" />
        <character name="quantity" src="d0_s17" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 490 (54 in the flora).</discussion>
  <discussion>Shrubs with deciduous leaves, petals, and stamens belong to either Hypericum sect. Ascyreia Choisy (with five stamen fascicles and five styles) or sect. Androsaemum (Duhamel) Godron (with five stamen fascicles and three or four styles). These are all introductions, mostly garden escapes. Those in sect. Ascyreia include: Hypericum calycinum Linnaeus, a low shrub with creeping stolons and flowers 50–95 mm diam. that has been found in California, Oregon, and Washington; H. ×moserianum Luquet ex André, its hybrid with H. patulum Thunberg, a low (sterile?) branching shrub with red anthers; and H. hookerianum Wight &amp; Arnott, a shrub to 2 m tall with narrow leaves and a dense ring of relatively short stamens, recorded from California (its identity requires confirmation). In sect. Androsaemum, H. androsaemum is a deciduous shrub with relatively small flowers and baccate fruits that ripen from cherry-red to black; it has been found in British Columbia and in California and Washington.</discussion>
  <discussion>Introduced herbaceous species with three stamen fascicles and three styles include: Hypericum hirsutum, with hairy stems and leaves (Ontario); H. tetrapterum, with four-winged internodes and lanceolate sepals (British Columbia and Washington); H. pulchrum with cordate leaves and red-tinged petals (Newfoundland, St. Pierre and Miquelon); and H. humifusum Linnaeus, a procumbent herb with unequal sepals (British Columbia).</discussion>
  <references>
    <reference>Adams, W. P. 1957. A revision of the genus Ascyrum (Hypericaceae). Rhodora 59: 73–95.</reference>
    <reference>Adams, W. P. 1962b. Studies in the Guttiferae. II. Taxonomic and distributional observations in North American taxa. Rhodora 64: 231–242.</reference>
    <reference>Adams, W. P. and N. K. B. Robson. 1961. A re-evaluation of the generic status of Ascyrum and Crookea (Guttiferae). Rhodora 63: 10–16.</reference>
    <reference>Crockett, S. L. 2003. Phytochemical and Biosystematic Investigations of New and Old World Hypericum Species (Clusiaceae). Ph.D. dissertation. University of Mississippi.</reference>
    <reference>Robson, N. K. B. 1981. Studies in the genus Hypericum L. (Guttiferae) 2. Characters of the genus. Bull. Brit. Mus. (Nat. Hist.), Bot. 8: 55–226.</reference>
    <reference>Robson, N. K. B. 1985. Studies in the genus Hypericum L. (Guttiferae) 8. Sections 29. Brathys (part 2) and 30. Trigynobrathys. Bull. Brit. Mus. (Nat. Hist.), Bot. 20: 1–151.</reference>
    <reference>Robson, N. K. B. 1994. Studies in the genus Hypericum L. (Guttiferae) 6. Sections 20. Myriandra to 28. Elodes. Bull. Nat. Hist. Mus. London, Bot. 26: 75–217.</reference>
    <reference>Robson, N. K. B. 2001. Studies in the genus Hypericum L. (Guttiferae) 4(1). Sections 7. Roscyna to 9. Hypericum sensu lato (part 1). Bull. Nat. Hist. Mus. London, Bot. 31: 37–88.</reference>
    <reference>Robson, N. K. B. 2002. Studies in the genus Hypericum L. (Guttiferae) 4(2). Section 9. Hypericum sensu lato (part 2): subsection 1. Hypericum series 1. Hypericum. Bull. Nat. Hist. Mus. London, Bot. 32: 61–123.</reference>
    <reference>Robson, N. K. B. 2006. Studies in the genus Hypericum (Guttiferae) 4(3). Section 9. Hypericum sensu lato (part 3): subsection 1. Hypericum series 2. Senanensia, subsection 2. Erecta and section 9b. Graveolentia. Syst. Biodivers. 4: 19–98.</reference>
    <reference>Robson, N. K. B. 2012. Studies in the genus Hypericum L. (Hypericaceae) 9. Addenda, corrigenda, keys, lists and general discussion. Phytotaxa 72: 1–111.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs, subshrubs, or shrubs, black glands absent; stamens in continuous or interrupted ring or in 4 or 5 barely discernable fascicles, each of 1 or 2 stamens</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs or shrubs, black and/or red glands usually present throughout, sometimes absent; stamens in 5 fascicles, each of 2+ stamens</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs (perennial), subshrubs, or shrubs; petals deciduous; stamens usually persistent, sometimes deciduous, 30–650, in continuous ring or in 4–5 barely discernable fascicles; styles ± appressed, bases distinct.</description>
      <determination>1a Hypericum sect. Myriandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs (annual or perennial); petals persistent; stamens persistent, (5–)10–80, usually in continuous or interrupted ring, sometimes in 5 barely discernable fascicles; styles ± spreading, bases distinct.</description>
      <determination>1b Hypericum informal sect. group Brathys</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs; leaves deciduous (base articulated); style bases distinct.</description>
      <determination>1c Hypericum sect. Webbia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Herbs (perennial); leaves persistent or tardily deciduous (base not articulated) or; style bases ± connate or distinct</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs, black glands absent; flowers 40–70 mm diam.; stamens 150, in 5 fascicles, fascicles usually distinct, rarely 1 pair connate; styles ± appressed, bases ± connate or distinct.</description>
      <determination>1d Hypericum sect. Roscyna</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs, black glands usually on leaves, sepals, and petals and, sometimes, on stems and anthers; flowers 6–35 mm diam.; stamens 20–109, in 5 fascicles, fascicles connate (as 2 + 2 + 1); styles spreading, bases distinct.</description>
      <determination>1e Hypericum informal sect. group Hypericum</determination>
    </key_statement>
  </key>
</bio:treatment>