<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="(Spach) R. Keller in H. G. A. Engler and K. Prantl" date="1893" rank="section">MYRIANDRA</taxon_name>
    <taxon_name authority="(Small) W. P. Adams &amp; N. Robson" date="unknown" rank="species">edisonianum</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>63: 15. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section myriandra;species edisonianum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100866</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ascyrum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">edisonianum</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>868, 1505. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ascyrum;species edisonianum</taxon_hierarchy>
  </taxon_identification>
  <number>26.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect, sometimes unbranched proximal to inflorescence, 3–15 dm.</text>
      <biological_entity id="o14919" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character constraint="to inflorescence" constraintid="o14920" is_modifier="false" name="position" src="d0_s0" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o14920" name="inflorescence" name_original="inflorescence" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes 4–6-lined at first, soon 2-lined.</text>
      <biological_entity id="o14921" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o14922" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-6-lined" value_original="4-6-lined" />
        <character is_modifier="false" modifier="soon" name="architecture" src="d0_s1" value="2-lined" value_original="2-lined" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades elliptic, 15–26 × 5–8 (–11) mm, base not articulated, cuneate to subrounded, with glandlike auricles, margins subrecurved to subincrassate, apex obtuse to acute, midrib with to 4 pairs of branches.</text>
      <biological_entity id="o14923" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="26" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14924" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="articulated" value_original="articulated" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="subrounded" />
      </biological_entity>
      <biological_entity id="o14925" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="glandlike" value_original="glandlike" />
      </biological_entity>
      <biological_entity id="o14926" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="subrecurved" value_original="subrecurved" />
      </biological_entity>
      <biological_entity id="o14927" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o14928" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity id="o14929" name="pair" name_original="pairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o14930" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <relation from="o14924" id="r1619" name="with" negation="false" src="d0_s2" to="o14925" />
      <relation from="o14928" id="r1620" name="with" negation="false" src="d0_s2" to="o14929" />
      <relation from="o14929" id="r1621" name="part_of" negation="false" src="d0_s2" to="o14930" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 1-flowered, branching from apical node repeatedly pseudodichotomous, without branches from proximal nodes.</text>
      <biological_entity id="o14931" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-flowered" value_original="1-flowered" />
        <character constraint="from apical node" constraintid="o14932" is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="apical" id="o14932" name="node" name_original="node" src="d0_s3" type="structure" />
      <biological_entity id="o14933" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o14934" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <relation from="o14931" id="r1622" name="without" negation="false" src="d0_s3" to="o14933" />
      <relation from="o14933" id="r1623" name="from" negation="false" src="d0_s3" to="o14934" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 15–20 mm diam.;</text>
      <biological_entity id="o14935" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="diameter" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals persistent, enclosing capsule, 4, unequal, outer broadly ovate, 8–17 × 5–9 mm, apex acute to subacuminate, inner linearlanceolate, 5–6 × 0.6–1.2 mm, apex acuminate;</text>
      <biological_entity id="o14936" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s5" value="4" value_original="4" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o14937" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o14938" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14939" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="subacuminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o14940" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14941" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o14936" id="r1624" name="enclosing" negation="false" src="d0_s5" to="o14937" />
    </statement>
    <statement id="d0_s6">
      <text>petals 4, bright-yellow, obovate, 10–18 mm;</text>
      <biological_entity id="o14942" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens persistent, 70–80;</text>
      <biological_entity id="o14943" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="70" name="quantity" src="d0_s7" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ovary 3–4-merous.</text>
      <biological_entity id="o14944" name="ovary" name_original="ovary" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-4-merous" value_original="3-4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules narrowly pyramidal-ovoid, 5–8 × 3–4 mm.</text>
      <biological_entity id="o14945" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="pyramidal-ovoid" value_original="pyramidal-ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds not carinate, 0.8 mm;</text>
      <biological_entity id="o14946" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="carinate" value_original="carinate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>testa reticulate.</text>
      <biological_entity id="o14947" name="testa" name_original="testa" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s11" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering probably year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="probably" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy areas in pine flatwoods, pond margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy areas" constraint="in pine flatwoods , pond margins" />
        <character name="habitat" value="pine flatwoods" />
        <character name="habitat" value="pond margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Hypericum edisonianum differs from H. crux-andreae in the smaller, thicker, obtuse to acute leaves with glandlike auricles and the pseudodichotomously branched inflorescence.</discussion>
  
</bio:treatment>