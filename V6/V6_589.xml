<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="illustration_page">316</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">sidalcea</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">asprella</taxon_name>
    <taxon_name authority="[E F]" date="unknown" rank="subspecies">asprella</taxon_name>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species asprella;subspecies asprella;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101111</other_info_on_name>
  </taxon_identification>
  <number>1a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (0.3–) 0.5–1 (–1.2) m, with caudex or usually compact rootstocks or rhizomes to 10 (–30) cm × 4 mm.</text>
      <biological_entity id="o10204" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.3" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="0.5" to_inclusive="false" to_unit="m" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="1.2" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10205" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <biological_entity id="o10206" name="rootstock" name_original="rootstocks" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="usually" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s0" to="10" to_unit="cm" />
        <character name="width" src="d0_s0" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10207" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="usually" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s0" to="10" to_unit="cm" />
        <character name="width" src="d0_s0" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <relation from="o10204" id="r1106" name="with" negation="false" src="d0_s0" to="o10205" />
      <relation from="o10204" id="r1107" name="with" negation="false" src="d0_s0" to="o10206" />
      <relation from="o10204" id="r1108" name="with" negation="false" src="d0_s0" to="o10207" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sometimes supported by other vegetation, sometimes proximally decumbent and often rooting to erect, roughly minutely or long-stellate-hairy, hairs spreading, usually on swollen bases, minutely hairy to glabrate distally.</text>
      <biological_entity id="o10208" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes; sometimes; sometimes proximally" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="roughly minutely; minutely" name="pubescence" src="d0_s1" value="long-stellate-hairy" value_original="long-stellate-hairy" />
      </biological_entity>
      <biological_entity id="o10209" name="vegetation" name_original="vegetation" src="d0_s1" type="structure" />
      <biological_entity id="o10210" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="minutely hairy" modifier="distally" name="pubescence" notes="" src="d0_s1" to="glabrate" />
      </biological_entity>
      <biological_entity id="o10211" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o10208" id="r1109" modifier="sometimes" name="supported by other" negation="false" src="d0_s1" to="o10209" />
      <relation from="o10210" id="r1110" modifier="usually" name="on" negation="false" src="d0_s1" to="o10211" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline, evenly distributed, not concentrated at stem base, gradually reduced distally;</text>
      <biological_entity id="o10212" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="evenly" name="arrangement" notes="" src="d0_s2" value="distributed" value_original="distributed" />
        <character constraint="at stem base" constraintid="o10214" is_modifier="false" modifier="not" name="arrangement_or_density" src="d0_s2" value="concentrated" value_original="concentrated" />
        <character is_modifier="false" modifier="gradually; distally" name="size" notes="" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10213" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="stem" id="o10214" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (1–) 5–10 (–15) cm, those of proximal leaves 4 times as long as blade, reduced distally to 1/2 times to as long as blade;</text>
      <biological_entity id="o10215" name="petiole" name_original="petiole" src="d0_s3" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character constraint="blade" constraintid="o10217" is_modifier="false" name="length" src="d0_s3" value="4 times as long as blade" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character constraint="blade" constraintid="o10218" is_modifier="false" name="length" src="d0_s3" value="0-1/2 times to as long as blade" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10216" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10217" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o10218" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o10215" id="r1111" name="part_of" negation="false" src="d0_s3" to="o10216" />
    </statement>
    <statement id="d0_s4">
      <text>blades: proximal usually lobed, distal palmately (5–) 7-lobed, (3–) 5–12 (–15) cm wide, base with wide sinus to truncate, margins crenate-serrate, lobe tips oblong, usually 3-toothed (sometimes entire), distalmost 3–5-parted;</text>
      <biological_entity id="o10219" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o10220" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10221" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="(5-)7-lobed" value_original="(5-)7-lobed" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10222" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o10223" name="sinus" name_original="sinus" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o10224" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o10225" name="tip" name_original="tips" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o10226" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="3-5-parted" value_original="3-5-parted" />
      </biological_entity>
      <relation from="o10222" id="r1112" name="with" negation="false" src="d0_s4" to="o10223" />
    </statement>
    <statement id="d0_s5">
      <text>surfaces stellate-puberulent, sometimes with some simple hairs.</text>
      <biological_entity id="o10227" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o10228" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-puberulent" value_original="stellate-puberulent" />
      </biological_entity>
      <biological_entity id="o10229" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o10228" id="r1113" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o10229" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences erect, unbranched or branched, 8–15 (–30) -flowered, sometimes 1-sided;</text>
      <biological_entity id="o10230" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="8-15(-30)-flowered" value_original="8-15(-30)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximalmost bract usually leaflike, 15 × 12 mm, distal bracts linear, 3 mm, shorter than pedicel.</text>
      <biological_entity constraint="proximalmost" id="o10231" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="leaflike" value_original="leaflike" />
        <character name="length" src="d0_s7" unit="mm" value="15" value_original="15" />
        <character name="width" src="d0_s7" unit="mm" value="12" value_original="12" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10232" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character constraint="than pedicel" constraintid="o10233" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o10233" name="pedicel" name_original="pedicel" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–5 (–10) mm.</text>
      <biological_entity id="o10234" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx 5–7 mm, to 7–10 mm in fruit;</text>
      <biological_entity id="o10235" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10236" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10237" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10237" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals pink to pale-purple, pistillate usually (5–) 10–15 mm, bisexual (15–) 20–25 mm;</text>
      <biological_entity id="o10238" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10239" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="pale-purple" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s10" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas (6 or) 7 or 8.</text>
      <biological_entity id="o10240" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10241" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s11" unit="or" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Schizocarps 6–8 mm diam.;</text>
      <biological_entity id="o10242" name="schizocarp" name_original="schizocarps" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mericarps (6 or) 7 or 8, 3–4 mm;</text>
    </statement>
    <statement id="d0_s14">
      <text>prominently reticulate-veined, pitted, margins and back rugose-pitted but less so on back, sometimes glabrous, mucro 0.5–0.8 mm.</text>
      <biological_entity id="o10243" name="mericarp" name_original="mericarps" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s13" unit="or" value="8" value_original="8" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="reticulate-veined" value_original="reticulate-veined" />
        <character is_modifier="false" name="relief" src="d0_s14" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity id="o10244" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character constraint="less on back" constraintid="o10245" is_modifier="false" modifier="back" name="relief" src="d0_s14" value="rugose-pitted" value_original="rugose-pitted" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10245" name="back" name_original="back" src="d0_s14" type="structure" />
      <biological_entity id="o10246" name="mucro" name_original="mucro" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.5 mm. 2n = 20, 40, 60.</text>
      <biological_entity id="o10247" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10248" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="quantity" src="d0_s15" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jun(–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woodlands, wet meadow margins, foothill woodlands, conifer forests, sometimes serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="wet meadow margins" />
        <character name="habitat" value="foothill woodlands" />
        <character name="habitat" value="conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1000(–1800) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies asprella has been confused with subsp. nana, Sidalcea celata, S. elegans, and S. glaucescens, and it previously has been placed within S. malviflora. Taller individuals can be confused with S. celata and S. gigantea; the adaxial leaf surfaces of S. celata generally have simple or two-branched hairs and those of the others have stellate hairs. Sidalcea gigantea always has long, retrorse bristle hairs at the stem base, and well-developed rhizomes. Sidalcea celata usually has dense, short, retrorse simple hairs at the stem base and rarely some stellate hairs; subsp. asprella always has stellate hairs at the stem base, but they can be less dense and short-scabrous or longer and softer. All three taxa occur in Shasta County, California, and some specimens from there may not be easily determined to species; young plants of S. gigantea may be easily mistaken as subsp. asprella before the stems and rhizomes are fully developed, and the fruits essentially match in both species. Subspecies asprella appears to be replaced by subsp. nana in northern California and Oregon, and it is especially difficult to be certain of identifications in that area.</discussion>
  <other_name type="common_name">Harsh checkerbloom</other_name>
  
</bio:treatment>