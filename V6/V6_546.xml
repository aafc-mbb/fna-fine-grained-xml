<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">301</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Jaubert &amp; Spach" date="1855" rank="genus">malvella</taxon_name>
    <taxon_name authority="(A. Gray) Fryxell" date="1974" rank="species">lepidota</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>19: 101. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malvella;species lepidota;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101093</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">lepidota</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 18. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sida;species lepidota</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Disella</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">lepidota</taxon_name>
    <taxon_hierarchy>genus Disella;species lepidota</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lepidota</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">depauperata</taxon_name>
    <taxon_hierarchy>genus S.;species lepidota;variety depauperata</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate, trailing, densely hairy with mixture of intergrading stellate and silvery-lepidote hairs.</text>
      <biological_entity id="o23260" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="trailing" value_original="trailing" />
        <character constraint="with mixture" constraintid="o23261" is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23261" name="mixture" name_original="mixture" src="d0_s0" type="structure" />
      <biological_entity id="o23262" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="true" name="pubescence" src="d0_s0" value="silvery-lepidote" value_original="silvery-lepidote" />
      </biological_entity>
      <relation from="o23261" id="r2463" name="part_of" negation="false" src="d0_s0" to="o23262" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1/2–2 times as long as blade;</text>
      <biological_entity id="o23263" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o23264" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character constraint="blade" constraintid="o23265" is_modifier="false" name="length" src="d0_s1" value="1/2-2 times as long as blade" />
      </biological_entity>
      <biological_entity id="o23265" name="blade" name_original="blade" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>blade ± triangular to somewhat ovate, mostly 1–2 cm, usually 1–2 (–3) times longer than wide, base truncate or cuneate, margins irregularly dentate, apex acute, surfaces densely hairy, hairs predominantly stellate abaxially, predominantly silvery-lepidote adaxially.</text>
      <biological_entity id="o23266" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23267" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s2" value="triangular to somewhat" value_original="triangular to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1-2(-3)" value_original="1-2(-3)" />
      </biological_entity>
      <biological_entity id="o23268" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o23269" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o23270" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o23271" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23272" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="predominantly; abaxially" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
        <character is_modifier="false" modifier="predominantly; adaxially" name="pubescence" src="d0_s2" value="silvery-lepidote" value_original="silvery-lepidote" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pedicels long, subequal to subtending leaves;</text>
      <biological_entity id="o23274" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o23273" id="r2464" name="subtending" negation="false" src="d0_s3" to="o23274" />
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets usually 0, sometimes 3, filiform.</text>
      <biological_entity id="o23273" name="pedicel" name_original="pedicels" src="d0_s3" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="size" src="d0_s3" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o23275" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character modifier="sometimes" name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: calyx 6–8 mm, silvery-lepidote, lobes cordate-ovate, bases plicate, apex acuminate;</text>
      <biological_entity id="o23276" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o23277" name="calyx" name_original="calyx" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="silvery-lepidote" value_original="silvery-lepidote" />
      </biological_entity>
      <biological_entity id="o23278" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate-ovate" value_original="cordate-ovate" />
      </biological_entity>
      <biological_entity id="o23279" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o23280" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals whitish or pale-yellow, sometimes fading rose, asymmetric, 10–15 mm;</text>
      <biological_entity id="o23281" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o23282" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="fading rose" value_original="fading rose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens pallid, glabrous, staminal column antheriferous at apex;</text>
      <biological_entity id="o23283" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o23284" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pallid" value_original="pallid" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="staminal" id="o23285" name="column" name_original="column" src="d0_s7" type="structure">
        <character constraint="at apex" constraintid="o23286" is_modifier="false" name="architecture" src="d0_s7" value="antheriferous" value_original="antheriferous" />
      </biological_entity>
      <biological_entity id="o23286" name="apex" name_original="apex" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>style ca. 7-branched, pallid, glabrous.</text>
      <biological_entity id="o23287" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23288" name="style" name_original="style" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="7-branched" value_original="7-branched" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pallid" value_original="pallid" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Schizocarps 5–6 mm diam.</text>
      <biological_entity id="o23289" name="schizocarp" name_original="schizocarps" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Heavy, saline soil on mud flats, lake shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline soil" modifier="heavy" constraint="on mud flats , lake shores" />
        <character name="habitat" value="mud flats" />
        <character name="habitat" value="lake shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, San Luis Potosí, Sonora, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malvella lepidota is somewhat intermediate between the other two species in our range, having both stellate hairs and lepidote scales, and sometimes having an involucel. It is less common and less weedy than M. leprosa.</discussion>
  
</bio:treatment>