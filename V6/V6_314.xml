<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Roussel" date="unknown" rank="family">passifloraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">passiflora</taxon_name>
    <taxon_name authority="Jussieu" date="1805" rank="species">mexicana</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>6: 108, plate 38, fig. 2. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family passifloraceae;genus passiflora;species mexicana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100982</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems subangular, glabrous.</text>
      <biological_entity id="o12423" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="subangular" value_original="subangular" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves not pungent, glabrous;</text>
      <biological_entity id="o12424" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="odor_or_shape" src="d0_s1" value="pungent" value_original="pungent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules linear-subulate, 1.5–2.5 × 0.5 mm, eglandular;</text>
      <biological_entity id="o12425" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole eglandular;</text>
      <biological_entity id="o12426" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade roughly symmetric, 1.5–7 (–15) × 2.5–8 (–14) cm, moderately to deeply 2-lobed, margins entire;</text>
      <biological_entity id="o12427" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="roughly" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="14" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="moderately to deeply" name="shape" src="d0_s4" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o12428" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial fine veins weakly to moderately raised, abaxial nectaries not along leaf margins, usually in 2 lines and rarely extending into leaf lobes at least on flowering-stems.</text>
      <biological_entity constraint="abaxial" id="o12429" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity id="o12430" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="fine" value_original="fine" />
        <character is_modifier="false" modifier="weakly to moderately" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12431" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o12432" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o12433" name="line" name_original="lines" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o12434" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <biological_entity id="o12435" name="flowering-stem" name_original="flowering-stems" src="d0_s5" type="structure" />
      <relation from="o12431" id="r1316" name="along" negation="false" src="d0_s5" to="o12432" />
      <relation from="o12431" id="r1317" modifier="usually" name="in" negation="false" src="d0_s5" to="o12433" />
      <relation from="o12431" id="r1318" name="extending into" negation="false" src="d0_s5" to="o12434" />
      <relation from="o12434" id="r1319" modifier="rarely" name="on" negation="false" src="d0_s5" to="o12435" />
    </statement>
    <statement id="d0_s6">
      <text>Floral bracts linear-subulate, 1–4 × 0.5 mm, margins entire, eglandular.</text>
      <biological_entity constraint="floral" id="o12436" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o12437" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: floral-tube absent;</text>
      <biological_entity id="o12438" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12439" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals green, 13–17 × 4–6 mm;</text>
      <biological_entity id="o12440" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o12441" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals green, 3–4 × 1 mm;</text>
      <biological_entity id="o12442" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12443" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corona filament whorls 2, outer filaments red, becoming purple, linear, terete, 5–12 mm.</text>
      <biological_entity id="o12444" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="filament" id="o12445" name="whorl" name_original="whorls" src="d0_s10" type="structure" constraint_original="corona filament">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12446" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" modifier="becoming" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Berries purple-black, globose to subellipsoid, 8–16 × 7–14 mm.</text>
      <biological_entity id="o12447" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple-black" value_original="purple-black" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s11" to="subellipsoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riparian woodlands, semiarid shrublands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian woodlands" />
        <character name="habitat" value="semiarid shrublands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The leaf shape of Passiflora mexicana is varies greatly within individual plants. Lateral leaf lobes are much shorter and apically rounded on slower-growing or flowering branches, but are relatively long and truncate on vigorously-growing, nonflowering branches, suckers, and young plants. A striking feature of this species is that as the flowers become less receptive to pollination the coronal filaments and limen (disc at base of the androgynophore) dramatically change color; the corona from red to purple, the limen from orange to yellow or white. The typically fetid flowers of this species may be wasp-pollinated (J. M. MacDougal and R. McVaugh 2001).</discussion>
  <discussion>Passiflora mexicana may consist of a small complex of species.</discussion>
  
</bio:treatment>