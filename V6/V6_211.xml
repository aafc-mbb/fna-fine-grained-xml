<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="M. S. Baker &amp; J. C. Clausen" date="unknown" rank="species">charlestonensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>8: 58. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species charlestonensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100908</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(M. S. Baker &amp; J. C. Clausen) S. L. Welsh &amp; Reveal" date="unknown" rank="variety">charlestonensis</taxon_name>
    <taxon_hierarchy>genus Viola;species purpurea;variety charlestonensis</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Charleston Mountain or Charleston violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 6–15 cm.</text>
      <biological_entity id="o15788" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, prostrate, decumbent, or erect, leafy proximally and distally, 1/2–2/3 subterranean, glabrous or puberulent, on caudex from usually vertical, subligneous rhizome.</text>
      <biological_entity id="o15789" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="1/2" modifier="distally" name="quantity" src="d0_s1" to="2/3" />
        <character is_modifier="false" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o15790" name="caudex" name_original="caudex" src="d0_s1" type="structure" />
      <biological_entity id="o15791" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="usually" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
      </biological_entity>
      <relation from="o15789" id="r1691" name="on" negation="false" src="d0_s1" to="o15790" />
      <relation from="o15790" id="r1692" name="from" negation="false" src="d0_s1" to="o15791" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o15792" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 1–3;</text>
      <biological_entity constraint="basal" id="o15793" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules adnate to petiole, forming 2 linearlanceolate wings, margins entire or sparingly lacerate, apex of each wing free, acuminate;</text>
      <biological_entity constraint="basal" id="o15794" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o15795" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="to petiole" constraintid="o15796" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o15796" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o15797" name="wing" name_original="wings" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15798" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sparingly" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o15799" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o15800" name="wing" name_original="wing" src="d0_s4" type="structure" />
      <relation from="o15795" id="r1693" name="forming" negation="false" src="d0_s4" to="o15797" />
      <relation from="o15799" id="r1694" name="part_of" negation="false" src="d0_s4" to="o15800" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 3.5–13.5 cm, densely short-puberulent;</text>
      <biological_entity constraint="basal" id="o15801" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o15802" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s5" to="13.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="short-puberulent" value_original="short-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade purplish abaxially (often dark purple-veined), grayish adaxially with prominent whitish veins (from dense hairs), usually orbiculate to broadly ovate, sometimes reniform, thick, 1–3.5 × 1.1–3.3 cm, base attenuate or truncate, margins entire, ciliate mostly on proximal half of blade, apex acute to obtuse, mucronulate, surfaces densely short-puberulent;</text>
      <biological_entity constraint="basal" id="o15803" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15804" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character constraint="with veins" constraintid="o15805" is_modifier="false" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="usually orbiculate" name="shape" notes="" src="d0_s6" to="broadly ovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s6" to="3.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15805" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o15806" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o15807" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character constraint="on proximal leaves" constraintid="o15808" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15808" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15809" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o15810" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o15811" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="short-puberulent" value_original="short-puberulent" />
      </biological_entity>
      <relation from="o15808" id="r1695" name="part_of" negation="false" src="d0_s6" to="o15809" />
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: stipules deltate to lanceolate, apex acute;</text>
      <biological_entity constraint="cauline" id="o15812" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o15813" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o15814" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o15815" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o15812" id="r1696" name="to" negation="false" src="d0_s7" to="o15813" />
      <relation from="o15813" id="r1697" name="except" negation="false" src="d0_s7" to="o15814" />
    </statement>
    <statement id="d0_s8">
      <text>petiole 1.9–3.4 cm;</text>
      <biological_entity constraint="cauline" id="o15816" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o15817" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o15818" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.9" from_unit="cm" name="some_measurement" src="d0_s8" to="3.4" to_unit="cm" />
      </biological_entity>
      <relation from="o15816" id="r1698" name="to" negation="false" src="d0_s8" to="o15817" />
      <relation from="o15817" id="r1699" name="except" negation="false" src="d0_s8" to="o15818" />
    </statement>
    <statement id="d0_s9">
      <text>blade ovate or elliptic to deltate, 0.7–3.1 × 0.6–2.2 cm, length 0.7–1.9 times width, base usually attenuate, sometimes subcordate, margins entire, ciliate.</text>
      <biological_entity constraint="cauline" id="o15819" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" notes="" src="d0_s9" to="3.1" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" notes="" src="d0_s9" to="2.2" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s9" value="0.7-1.9" value_original="0.7-1.9" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15820" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o15821" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="deltate" />
      </biological_entity>
      <biological_entity id="o15822" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o15823" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o15819" id="r1700" name="to" negation="false" src="d0_s9" to="o15820" />
      <relation from="o15820" id="r1701" name="except" negation="false" src="d0_s9" to="o15821" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 1.7–6.6 cm, pubescent.</text>
      <biological_entity id="o15824" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s10" to="6.6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals linearlanceolate, margins ciliate or eciliate, auricles 0.5–1 mm;</text>
      <biological_entity id="o15825" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15826" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o15827" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o15828" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals deep lemon-yellow adaxially, upper 2 usually conspicuously reddish-brown to brownish purple abaxially, lateral 2 streaked or solid reddish-brown, lower 3 and sometimes upper 2 dark brown-veined proximally, lateral 2 bearded, lowest 8–13 mm, spur usually reddish-brown, sometimes yellowish, gibbous, 0.4–2 mm, glabrous or scabrous abaxially;</text>
      <biological_entity id="o15829" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15830" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s12" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="usually conspicuously reddish-brown" modifier="abaxially" name="coloration" src="d0_s12" to="brownish purple" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="streaked" value_original="streaked" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="solid" value_original="solid" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="position" src="d0_s12" value="lower" value_original="lower" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s12" value="dark brown-veined" value_original="dark brown-veined" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15831" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style head bearded;</text>
      <biological_entity id="o15832" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="style" id="o15833" name="head" name_original="head" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>cleistogamous flowers axillary.</text>
      <biological_entity id="o15834" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15835" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="position" src="d0_s14" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules spherical, 4.5–9 mm, puberulent.</text>
      <biological_entity id="o15836" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="spherical" value_original="spherical" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds black, 3.4–3.5 mm. 2n = 12.</text>
      <biological_entity id="o15837" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="black" value_original="black" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15838" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On limestone hills, slopes, and dry washes beneath Pinus monophylla, P. ponderosa, Juniperus osteosperma, and/or Cercocarpus sp.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone hills" modifier="on" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="dry washes" constraint="beneath pinus" />
        <character name="habitat" value="monophylla" />
        <character name="habitat" value="p. ponderosa" />
        <character name="habitat" value="osteosperma" />
        <character name="habitat" value="sp" modifier="and\/or" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Viola charlestonensis is known only from the Spring Mountains (previously called Charleston Mountains) in Nevada and Zion National Park, Utah. M. S. Baker (in I. W. Clokey 1945) stated that E. C. Jaeger reportedly collected it at Jacob’s Pool, Arizona, in July 1926. The location of this single Arizona collection may be an error (R. J. Little 2001).</discussion>
  <discussion>M. S. Baker and J. Clausen (in I. W. Clokey 1945) stated that Viola charlestonensis is the only species in Becker’s Nuttallianae group with the spur pubescent on the exterior. In some populations in the Spring Mountains, Nevada, the spur and the midvein on the abaxial surface of the lowest petal to ± the middle of the lowest petal are covered ± densely with short hairs. Scattered hairs are also present on the abaxial surface of the lateral and upper petals. In other populations in the Spring Mountains, short hairs are mostly absent on the spur, lowest petal, and abaxial surfaces of lateral and upper petals.</discussion>
  <discussion>M. S. Baker (in I. W. Clokey 1945) commented that he observed numerous sterile flowers and relatively few mature capsules and seeds of Viola charlestonensis plants when he visited the Spring Mountains in June 1937. Similar observations were made of V. charlestonensis plants in the Spring Mountains at one location in 2009 and of three other locations in 2010.</discussion>
  
</bio:treatment>