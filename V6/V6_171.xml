<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">98</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Hypericum</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">concinnum</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>300. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section hypericum;species concinnum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100884</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypericum</taxon_name>
    <taxon_name authority="R. Keller" date="unknown" rank="species">seleri</taxon_name>
    <taxon_hierarchy>genus Hypericum;species seleri</taxon_hierarchy>
  </taxon_identification>
  <number>47.</number>
  <other_name type="common_name">Gold wire</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs erect or ascending, rarely with rooting base, bushy, 1.5–3.3 dm.</text>
      <biological_entity id="o22760" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form" notes="" src="d0_s0" value="bushy" value_original="bushy" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3.3" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22761" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <relation from="o22760" id="r2406" modifier="rarely" name="with" negation="false" src="d0_s0" to="o22761" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: internodes (at least some) 4-lined, without black glands.</text>
      <biological_entity id="o22762" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o22763" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="4-lined" value_original="4-lined" />
      </biological_entity>
      <biological_entity id="o22764" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="black" value_original="black" />
      </biological_entity>
      <relation from="o22763" id="r2407" name="without" negation="false" src="d0_s1" to="o22764" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading, sessile or petiolate (to 0.5 mm);</text>
      <biological_entity id="o22765" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly elliptic or narrowly oblong to linear, usually conduplicate, sometimes falcate, 13–22 × 1.5–8 mm, base cuneate, margins plane, apex acute to subacute, midrib with 2–4 pairs of branches, black glands marginal.</text>
      <biological_entity id="o22766" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character constraint="to base, margins, apex" constraintid="o22767, o22768, o22769" is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o22767" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="usually" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="true" modifier="sometimes" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="13" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
      <biological_entity id="o22768" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="usually" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="true" modifier="sometimes" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="13" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
      <biological_entity id="o22769" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="true" modifier="usually" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="true" modifier="sometimes" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="13" from_unit="mm" is_modifier="true" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" is_modifier="true" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="subacute" />
      </biological_entity>
      <biological_entity id="o22770" name="midrib" name_original="midrib" src="d0_s3" type="structure" />
      <biological_entity id="o22771" name="pair" name_original="pairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o22772" name="branch" name_original="branches" src="d0_s3" type="structure" />
      <biological_entity id="o22773" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character is_modifier="false" name="position" src="d0_s3" value="marginal" value_original="marginal" />
      </biological_entity>
      <relation from="o22770" id="r2408" name="with" negation="false" src="d0_s3" to="o22771" />
      <relation from="o22771" id="r2409" name="part_of" negation="false" src="d0_s3" to="o22772" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences subcapitate to cylindric, 1–7-flowered.</text>
      <biological_entity id="o22774" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="subcapitate" name="shape" src="d0_s4" to="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 20–35 mm diam.;</text>
      <biological_entity id="o22775" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s5" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals markedly imbricate, spreading in fruit, broadly to narrowly ovate, unequal, 6–9 × 2–3 mm, apex acute to acuminate;</text>
      <biological_entity id="o22776" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="markedly" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character constraint="in fruit" constraintid="o22777" is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" notes="" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22777" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o22778" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals yellow, obovate or oblong-obovate, (10–) 12–15 mm;</text>
      <biological_entity id="o22779" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-obovate" value_original="oblong-obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 40–80 (–100);</text>
      <biological_entity id="o22780" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="100" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anther gland amber;</text>
      <biological_entity id="o22781" name="anther" name_original="anther" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="gland amber" value_original="gland amber" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 6–9 mm.</text>
      <biological_entity id="o22782" name="style" name_original="styles" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, 6–9 × 4–4.5 mm, with longitudinal vittae.</text>
      <biological_entity id="o22783" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22784" name="vitta" name_original="vittae" src="d0_s11" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s11" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o22783" id="r2410" name="with" negation="false" src="d0_s11" to="o22784" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds not carinate, 1 mm;</text>
      <biological_entity id="o22785" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="carinate" value_original="carinate" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>testa minutely and shallowly pitted.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity id="o22786" name="testa" name_original="testa" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="relief" src="d0_s13" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22787" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (May–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="May-Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes, chaparral, yellow pine forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="yellow pine forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600(–900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypericum concinnum is known from the Sierra Nevada from Mariposa County to Shasta County and the North Coast Ranges from Marin County to Mendocino County. It is isolated, taxonomically and geographically, from its nearest relative, which seems to be the northeastern Asian H. ascyron subsp. gebleri (Ledebour) N. Robson.</discussion>
  
</bio:treatment>