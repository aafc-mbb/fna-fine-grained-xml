<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">133</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="A. M. Powell &amp; Wauer" date="unknown" rank="species">guadalupensis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>14: 1, fig. 1. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species guadalupensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100917</other_info_on_name>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Guadalupe Mountains violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 1–10 cm.</text>
      <biological_entity id="o6295" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, decumbent to erect, leafy proximally and distally, glabrous, on caudex from fleshy rhizome.</text>
      <biological_entity id="o6296" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6297" name="caudex" name_original="caudex" src="d0_s1" type="structure" />
      <biological_entity id="o6298" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o6296" id="r702" name="on" negation="false" src="d0_s1" to="o6297" />
      <relation from="o6297" id="r703" name="from" negation="false" src="d0_s1" to="o6298" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
      <biological_entity id="o6299" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate to ovate or oblong-lanceolate or linear, margins sparingly glandular-fimbriate, apex acute;</text>
      <biological_entity id="o6300" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate or oblong-lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o6301" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparingly" name="shape" src="d0_s3" value="glandular-fimbriate" value_original="glandular-fimbriate" />
      </biological_entity>
      <biological_entity id="o6302" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–6 cm, glabrous;</text>
      <biological_entity id="o6303" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to ovate-deltate or ovatelanceolate, 1.2–2.4 × 0.7–1.3 cm, base broadly cuneate to rounded or truncate, margins entire or with 1–3 crenations on proximal 1/2, eciliate, apex acute to rounded, surfaces glabrous, sometimes with a few short hairs on veins abaxially.</text>
      <biological_entity id="o6304" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovate-deltate or ovatelanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s5" to="2.4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s5" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6305" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s5" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o6306" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="with 1-3 crenations" value_original="with 1-3 crenations" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o6307" name="crenation" name_original="crenations" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6308" name="1/2" name_original="1/2" src="d0_s5" type="structure" />
      <biological_entity id="o6309" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o6310" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6311" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o6312" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <relation from="o6306" id="r704" name="with" negation="false" src="d0_s5" to="o6307" />
      <relation from="o6307" id="r705" name="on" negation="false" src="d0_s5" to="o6308" />
      <relation from="o6310" id="r706" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o6311" />
      <relation from="o6311" id="r707" name="on" negation="false" src="d0_s5" to="o6312" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3.5–6 cm, glabrous.</text>
      <biological_entity id="o6313" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals linear to linearlanceolate, margins eciliate, auricles 0.5–1.5 mm;</text>
      <biological_entity id="o6314" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6315" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="to linearlanceolate" constraintid="o6316" is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o6316" name="linearlanceolate" name_original="linearlanceolate" src="d0_s7" type="structure" />
      <biological_entity id="o6317" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o6318" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals deep lemon-yellow adaxially, upper 2 reddish-brown abaxially, lateral 2 and lowest dark brown-veined basally, lateral 2 bearded, lowest 7–11 mm, spur yellow, gibbous, 1–1.4 mm;</text>
      <biological_entity id="o6319" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6320" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s8" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s8" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s8" value="dark brown-veined" value_original="dark brown-veined" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s8" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6321" name="spur" name_original="spur" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style head bearded;</text>
      <biological_entity id="o6322" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="style" id="o6323" name="head" name_original="head" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>cleistogamous flowers absent.</text>
      <biological_entity id="o6324" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6325" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules ovoid, 3–4.5 mm, glabrous.</text>
      <biological_entity id="o6326" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds light-brown, ± 2 mm. 2n = 24.</text>
      <biological_entity id="o6327" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light-brown" value_original="light-brown" />
        <character modifier="more or less" name="some_measurement" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6328" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings and narrow ledges on limestone rock faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" />
        <character name="habitat" value="narrow ledges" constraint="on limestone rock" />
        <character name="habitat" value="limestone rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Viola guadalupensis is known only from the eastern rim of the Guadalupe Mountains in Culberson County. Powell and Wauer noted that it is the only yellow-flowered violet known in the Guadalupe Mountains and appears to be related to V. nuttallii and V. vallicola. K. W. Allred (2008) stated that a report of this species in New Mexico by J. T. Kartesz and C. A. Meacham (1999) requires verification. K. Haskins (pers. comm.) reported that experiments are currently being conducted to propagate plants of V. guadalupensis via cell tissue culture.</discussion>
  <discussion>Chloroplast (trnL-F spacer) and low-copy nuclear gene (GPI) phylogenies indicate that Viola guadalupensis is an alloploid that originated through hybridization between an unidentified member of subsect. Canadenses (the paternal parent) and a member of the V. nuttallii complex</discussion>
  <discussion>(the maternal parent), of sect. Chamaemelanium (T. Marcussen et al. 2011). Evidence reported by these authors from a fossil-calibrated relaxed clock dating analysis showed the estimated maximum age of V. guadalupensis to be (5.7–)8.6(–11.6) million years.</discussion>
  
</bio:treatment>