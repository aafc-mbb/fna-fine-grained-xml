<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sida</taxon_name>
    <taxon_name authority="(Linnaeus) Rusby" date="unknown" rank="species">hermaphrodita</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>5: 223. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sida;species hermaphrodita;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101102</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Napaea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">hermaphrodita</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 686. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Napaea;species hermaphrodita</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Virginia mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–2.5 (–5) m.</text>
      <biological_entity id="o4722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, minutely stellate-hairy when young, soon glabrate.</text>
      <biological_entity id="o4723" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules free from petiole, linearlanceolate, 3–4 mm, shorter than petiole;</text>
      <biological_entity id="o4724" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4725" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character constraint="from petiole" constraintid="o4726" is_modifier="false" name="fusion" src="d0_s2" value="free" value_original="free" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s2" to="4" to_unit="mm" />
        <character constraint="than petiole" constraintid="o4727" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4726" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o4727" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole to 0.9 mm, shorter than blade, glabrous;</text>
      <biological_entity id="o4728" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4729" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.9" to_unit="mm" />
        <character constraint="than blade" constraintid="o4730" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4730" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade palmately 5–7-lobed, maplelike, to 24 cm, ± as long as wide, smaller upward, base cordate, margins serrate, apex long-acuminate, surfaces glabrous.</text>
      <biological_entity id="o4731" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4732" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="maplelike" value_original="maplelike" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="24" to_unit="cm" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o4733" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4734" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4735" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4736" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="true" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4732" id="r542" modifier="more or less" name="as long as" negation="false" src="d0_s4" to="o4733" />
      <relation from="o4732" id="r543" modifier="more or less" name="as long as" negation="false" src="d0_s4" to="o4734" />
      <relation from="o4732" id="r544" modifier="more or less" name="as long as" negation="false" src="d0_s4" to="o4735" />
      <relation from="o4732" id="r545" modifier="more or less" name="as long as" negation="false" src="d0_s4" to="o4736" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, subumbellate, 2–10-flowered pedunculate corymbs, forming terminal panicles.</text>
      <biological_entity id="o4737" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
      </biological_entity>
      <biological_entity id="o4738" name="corymb" name_original="corymbs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="2-10-flowered" value_original="2-10-flowered" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4739" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <relation from="o4738" id="r546" name="forming" negation="false" src="d0_s5" to="o4739" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx dark-pigmented basally, unribbed, not angulate, 4–5 mm, minutely stellate-hairy, lobes wide-triangular;</text>
      <biological_entity id="o4740" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4741" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s6" value="dark-pigmented" value_original="dark-pigmented" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="unribbed" value_original="unribbed" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="angulate" value_original="angulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s6" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o4742" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="wide-triangular" value_original="wide-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, 8–10 mm;</text>
      <biological_entity id="o4743" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4744" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column hairy;</text>
      <biological_entity id="o4745" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o4746" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 8-branched.</text>
      <biological_entity id="o4747" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4748" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="8-branched" value_original="8-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Schizocarps subconic, 6–8 mm diam., minutely stellate-hairy;</text>
      <biological_entity id="o4749" name="schizocarp" name_original="schizocarps" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subconic" value_original="subconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>mericarps 8, not reticulate, apex beaked.</text>
      <biological_entity id="o4750" name="mericarp" name_original="mericarps" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
        <character is_modifier="false" modifier="not" name="architecture_or_coloration_or_relief" src="d0_s11" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 28.</text>
      <biological_entity id="o4751" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4752" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams, roadsides, railroad embankments, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad embankments" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; D.C., Ind., Ky., Md., Mich., Ohio, Pa., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Some occurrences of Sida hermaphrodita may be the result of escapes from cultivation. It is generally rare except locally common along the Kanawha and Ohio rivers in Ohio and West Virginia (D. M. Spooner et al. 1985); it has been extirpated from Tennessee. Reports from Massachusetts, New Jersey, and New York refer to garden escapes. The species may or may not be native in Michigan.</discussion>
  
</bio:treatment>