<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David M. Bates,Malvastrum A. Gray subsect. Pedunculosa A. Gray in A. Gray et al., Syn. Fl. N. Amer. 1(1,2): 308. 1897</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Greene" date="1906" rank="genus">EREMALCHE</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 208. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus eremalche;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek eremia, desert, and alkea, mallow, alluding to habitat</other_info_on_name>
    <other_info_on_name type="fna_id">111914</other_info_on_name>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Desert mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, sometimes gynodioecious.</text>
      <biological_entity id="o14948" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s0" value="gynodioecious" value_original="gynodioecious" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate to erect, sparsely to densely hairy.</text>
      <biological_entity id="o14949" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules caducous, subulate;</text>
      <biological_entity id="o14950" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14951" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade reniform-round, unlobed or palmately 3-, 5-, [7-] cleft, base truncate to cordate, margins usually toothed, sometimes entire.</text>
      <biological_entity id="o14952" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14953" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform-round" value_original="reniform-round" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character name="shape" src="d0_s3" value="palmately" value_original="palmately" />
        <character name="quantity" src="d0_s3" value="[7" value_original="[7" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o14954" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s3" to="cordate" />
      </biological_entity>
      <biological_entity id="o14955" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary, solitary flowers in lower axils, congested and ± corymbose at branch tips;</text>
      <biological_entity id="o14956" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14958" name="axil" name_original="axils" src="d0_s4" type="structure" />
      <biological_entity constraint="branch" id="o14959" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <relation from="o14957" id="r1625" name="in" negation="false" src="d0_s4" to="o14958" />
    </statement>
    <statement id="d0_s5">
      <text>involucellar bractlets persistent, 3, distinct.</text>
      <biological_entity id="o14957" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s4" value="congested" value_original="congested" />
        <character constraint="at branch tips" constraintid="o14959" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s4" value="corymbose" value_original="corymbose" />
      </biological_entity>
      <biological_entity id="o14960" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx not accrescent, not inflated, lobed to beyond middle, lobes not ribbed, acuminate;</text>
      <biological_entity id="o14961" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o14962" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character constraint="to middle flowers" constraintid="o14963" is_modifier="false" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o14963" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o14964" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla campanulate, white to rose, lavender, or mauve, drying purple or blue;</text>
      <biological_entity id="o14965" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14966" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose lavender or mauve drying purple or blue" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose lavender or mauve drying purple or blue" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose lavender or mauve drying purple or blue" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="rose lavender or mauve drying purple or blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column included;</text>
      <biological_entity id="o14967" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o14968" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 9–36-carpellate;</text>
      <biological_entity id="o14969" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14970" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="9-36-carpellate" value_original="9-36-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 1 per carpel;</text>
      <biological_entity id="o14971" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14972" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character constraint="per carpel" constraintid="o14973" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14973" name="carpel" name_original="carpel" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>styles 9–36-branched (branches equal in number to carpels);</text>
      <biological_entity id="o14974" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14975" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="9-36-branched" value_original="9-36-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas terminal, capitate.</text>
      <biological_entity id="o14976" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14977" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits schizocarps, oblique, not inflated, depressed-discoid, edge corrugate or reticulate, glabrous;</text>
      <biological_entity constraint="fruits" id="o14978" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s13" value="oblique" value_original="oblique" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="depressed-discoid" value_original="depressed-discoid" />
      </biological_entity>
      <biological_entity id="o14979" name="edge" name_original="edge" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 9–36, drying black, 1-celled, without spur, ± spheric, unarmed, lateral walls ± disintegrated, indehiscent.</text>
      <biological_entity id="o14980" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s14" to="36" />
        <character is_modifier="false" name="condition" src="d0_s14" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-celled" value_original="1-celled" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s14" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o14981" name="spur" name_original="spur" src="d0_s14" type="structure" />
      <biological_entity constraint="lateral" id="o14982" name="wall" name_original="walls" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <relation from="o14980" id="r1626" name="without" negation="false" src="d0_s14" to="o14981" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds ascending, 1 per mericarp, black, obovoid-reniform, glabrous.</text>
      <biological_entity id="o14984" name="mericarp" name_original="mericarp" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>x = 10.</text>
      <biological_entity id="o14983" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
        <character constraint="per mericarp" constraintid="o14984" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid-reniform" value_original="obovoid-reniform" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="x" id="o14985" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades unlobed; petals magenta-spotted at base; mericarps 2.8–3.5 mm.</description>
      <determination>1 Eremalche rotundifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves lobed or parted; petals not spotted; mericarps 1.4–1.8 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers bisexual; petals 4–5.5 mm, ± equaling calyx; plants prostrate to decumbent.</description>
      <determination>2 Eremalche exilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers bisexual or pistillate; petals 5.5–20(–25) mm, equaling or exceeding calyx; plants erect, main stem unbranched or with ascending branches from base.</description>
      <determination>3 Eremalche parryi</determination>
    </key_statement>
  </key>
</bio:treatment>