<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">180</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu ex Roussel" date="unknown" rank="family">passifloraceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">passiflora</taxon_name>
    <taxon_name authority="Poeppig ex Masters in C. F. P. von Martius et al." date="1872" rank="species">pallens</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. P. von Martius et al., Fl. Bras.</publication_title>
      <place_in_publication>13(1): 567, plate 128, fig. 4. 1872</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family passifloraceae;genus passiflora;species pallens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100990</other_info_on_name>
  </taxon_identification>
  <number>16.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems terete when young, glabrous.</text>
      <biological_entity id="o5861" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="when young" name="shape" src="d0_s0" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves weakly to moderately pungent, glabrous;</text>
      <biological_entity id="o5862" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="weakly to moderately" name="odor_or_shape" src="d0_s1" value="pungent" value_original="pungent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules subreniform, 10–20 × 5–14 mm, eglandular;</text>
      <biological_entity id="o5863" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subreniform" value_original="subreniform" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="14" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole glandular, glands clavate;</text>
      <biological_entity id="o5864" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o5865" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade roughly symmetric, 1.5–6 × 2.5–9 cm, shallowly 3-lobed, middle lobe as long as or longer than lateral lobes, margins serrate basally;</text>
      <biological_entity id="o5866" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="roughly" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="9" to_unit="cm" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="middle" id="o5867" name="lobe" name_original="lobe" src="d0_s4" type="structure">
        <character constraint="than lateral lobes" constraintid="o5868" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5868" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o5869" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial fine veins moderately raised, abaxial nectaries absent.</text>
      <biological_entity constraint="abaxial" id="o5870" name="nectary" name_original="nectaries" src="d0_s5" type="structure" />
      <biological_entity id="o5871" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="fine" value_original="fine" />
        <character is_modifier="false" modifier="moderately" name="prominence" src="d0_s5" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5872" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Floral bracts ovate, 10–20 × 9–12 mm, margins basally serrate to glandular-serrate basally.</text>
      <biological_entity constraint="floral" id="o5873" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5874" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="basally serrate" modifier="basally" name="architecture_or_shape" src="d0_s6" to="glandular-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: floral-tube cuplike, 3–5 mm deep;</text>
      <biological_entity id="o5875" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5876" name="floral-tube" name_original="floral-tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuplike" value_original="cuplike" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals white, 30–35 × 7–12 mm;</text>
      <biological_entity id="o5877" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5878" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, 20–30 × 6–10 mm;</text>
      <biological_entity id="o5879" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5880" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s9" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corona filament whorls 4, outer filaments green basally, white apically, with alternating lines of purple, linear, terete, 7–15 mm.</text>
      <biological_entity id="o5881" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="filament" id="o5882" name="whorl" name_original="whorls" src="d0_s10" type="structure" constraint_original="corona filament">
        <character name="quantity" src="d0_s10" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5883" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" modifier="apically" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="of purple" name="arrangement_or_course_or_shape" notes="" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s10" value="terete" value_original="terete" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5884" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="alternating" value_original="alternating" />
      </biological_entity>
      <relation from="o5883" id="r664" name="with" negation="false" src="d0_s10" to="o5884" />
    </statement>
    <statement id="d0_s11">
      <text>Berries yellow to yellow-orange, ovoid, 30–50 × 25–35 mm.</text>
      <biological_entity id="o5885" name="berry" name_original="berries" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="yellow-orange" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s11" to="50" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="width" src="d0_s11" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins of and sunny gaps within mesic to wet tropical to subtropical woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of and sunny gaps within mesic to wet tropical to subtropical woodlands" />
        <character name="habitat" value="sunny gaps" constraint="within mesic to wet tropical to subtropical woodlands" />
        <character name="habitat" value="subtropical" />
        <character name="habitat" value="mesic to wet tropical to subtropical woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Cuba, Hispaniola).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leaf and stipule variegation, although rare within subg. Passiflora, is occasionally found in Passiflora pallens, and may serve to camouflage plants from predators in the dappled shade of their habitats. Listed as endangered in Florida, in the flora area it is found only in the southernmost part of the state.</discussion>
  
</bio:treatment>