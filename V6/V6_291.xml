<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Elliott" date="unknown" rank="species">tripartita</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>1: 302. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species tripartita</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100970</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">hastata</taxon_name>
    <taxon_name authority="Gingins" date="unknown" rank="variety">glaberrima</taxon_name>
    <taxon_hierarchy>genus Viola;species hastata;variety glaberrima</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hastata</taxon_name>
    <taxon_name authority="(Elliott) A. Gray" date="unknown" rank="variety">tripartita</taxon_name>
    <taxon_hierarchy>genus V.;species hastata;variety tripartita</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tripartita</taxon_name>
    <taxon_name authority="(Gingins) R. M. Harper" date="unknown" rank="variety">glaberrima</taxon_name>
    <taxon_hierarchy>genus V.;species tripartita;variety glaberrima</taxon_hierarchy>
  </taxon_identification>
  <number>68.</number>
  <other_name type="common_name">Three-parted or Piedmont or threepart violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 10–40 cm.</text>
      <biological_entity id="o21556" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (2), erect, leafless proximally, leafy distally, glabrous or puberulent, from subligneous rhizome.</text>
      <biological_entity id="o21557" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s1" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="leafless" value_original="leafless" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o21558" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
      </biological_entity>
      <relation from="o21557" id="r2281" name="from" negation="false" src="d0_s1" to="o21558" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o21559" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 0 (–2);</text>
      <biological_entity constraint="basal" id="o21560" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules ovate, not leaflike, margins entire, apex acute to acuminate, surfaces glabrous or puberulent;</text>
      <biological_entity constraint="basal" id="o21561" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21562" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o21563" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21564" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o21565" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 9–11.5 cm, glabrous or puberulent;</text>
      <biological_entity constraint="basal" id="o21566" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o21567" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s5" to="11.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade unlobed, ovate, or 3–5-lobed, 4–5 × 1–5 cm, base cordate, margins entire or crenate-serrate, ciliate or eciliate, apex acute, surfaces glabrous or ± puberulent;</text>
      <biological_entity constraint="basal" id="o21568" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o21569" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21570" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o21571" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o21572" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21573" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: restricted to distal ends of stems;</text>
      <biological_entity constraint="cauline" id="o21574" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o21575" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o21576" name="end" name_original="ends" src="d0_s7" type="structure" />
      <biological_entity id="o21577" name="stem" name_original="stems" src="d0_s7" type="structure" />
      <relation from="o21574" id="r2282" name="to" negation="false" src="d0_s7" to="o21575" />
      <relation from="o21575" id="r2283" name="except" negation="false" src="d0_s7" to="o21576" />
      <relation from="o21576" id="r2284" name="part_of" negation="false" src="d0_s7" to="o21577" />
    </statement>
    <statement id="d0_s8">
      <text>stipules ovate to oblong;</text>
      <biological_entity constraint="cauline" id="o21578" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o21579" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o21580" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong" />
      </biological_entity>
      <relation from="o21578" id="r2285" name="to" negation="false" src="d0_s8" to="o21579" />
      <relation from="o21579" id="r2286" name="except" negation="false" src="d0_s8" to="o21580" />
    </statement>
    <statement id="d0_s9">
      <text>petiole 0.7–7.2 cm, glabrous or puberulent;</text>
      <biological_entity constraint="cauline" id="o21581" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21582" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o21583" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s9" to="7.2" to_unit="cm" />
      </biological_entity>
      <relation from="o21581" id="r2287" name="to" negation="false" src="d0_s9" to="o21582" />
      <relation from="o21582" id="r2288" name="except" negation="false" src="d0_s9" to="o21583" />
    </statement>
    <statement id="d0_s10">
      <text>blade unlobed, ovate or deltate, or 3-lobed (if 3-lobed, lateral lobes falcate, middle rhombic, longer than others, lobes may appear petiolate; unlobed and 3-lobed leaves can occur on same plant), 1–6 × 0.5–5.5 cm, base truncate to cuneate, margins serrate, ciliate or eciliate, surfaces glabrous or pubescent.</text>
      <biological_entity constraint="cauline" id="o21584" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s10" to="5.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21585" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity id="o21586" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o21587" name="base" name_original="base" src="d0_s10" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s10" to="cuneate" />
      </biological_entity>
      <biological_entity id="o21588" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o21589" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o21584" id="r2289" name="to" negation="false" src="d0_s10" to="o21585" />
      <relation from="o21585" id="r2290" name="except" negation="false" src="d0_s10" to="o21586" />
    </statement>
    <statement id="d0_s11">
      <text>Peduncles 1.5–4 cm, glabrous or pubescent.</text>
      <biological_entity id="o21590" name="peduncle" name_original="peduncles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals lanceolate to ovate, margins ciliate or eciliate, auricles 0.1–0.5 mm;</text>
      <biological_entity id="o21591" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21592" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="ovate" />
      </biological_entity>
      <biological_entity id="o21593" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o21594" name="auricle" name_original="auricles" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals lemon-yellow adaxially, upper 2, rarely others, brownish purple abaxially, lowest and usually lateral 2 brownish purple-veined, lateral 2 and sometimes lowest bearded, lowest 10–18 mm, spur yellow, gibbous, 0.5–2 mm;</text>
      <biological_entity id="o21595" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21596" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s13" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s13" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" modifier="abaxially" name="coloration" notes="" src="d0_s13" value="brownish purple" value_original="brownish purple" />
        <character is_modifier="false" name="position" src="d0_s13" value="lowest" value_original="lowest" />
        <character is_modifier="false" modifier="usually" name="position" src="d0_s13" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish purple-veined" value_original="brownish purple-veined" />
        <character is_modifier="false" name="position" src="d0_s13" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s13" value="lowest" value_original="lowest" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s13" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21597" name="other" name_original="others" src="d0_s13" type="structure" />
      <biological_entity id="o21598" name="spur" name_original="spur" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style head bearded;</text>
      <biological_entity id="o21599" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="style" id="o21600" name="head" name_original="head" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>cleistogamous flowers axillary.</text>
      <biological_entity id="o21601" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o21602" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="position" src="d0_s15" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsules ovoid to ellipsoid, 9–12 mm, glabrous.</text>
      <biological_entity id="o21603" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="ellipsoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s16" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds beige, bronze, or brown, 2.4–3 mm. 2n = 12.</text>
      <biological_entity id="o21604" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="beige" value_original="beige" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="bronze" value_original="bronze" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21605" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Ky., Miss., N.C., Ohio, Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Some authors recognize two varieties of Viola tripartita based on lobed versus unlobed leaves. F. L. Lévesque and P. M. Dansereau (1966) suggested that leaf variation is the only character difference between vars. tripartita and glaberrima. N. H. Russell (1965) stated that V. tripartita plants with lobed and unlobed leaves are sympatric and frequently intergrade and did not recognize them as distinct. The situation with two leaf forms in V. tripartita is similar to V. lobata, which also has two leaf forms.</discussion>
  
</bio:treatment>