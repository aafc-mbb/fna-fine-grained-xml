<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lechea</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">pulchella</taxon_name>
    <taxon_name authority="(Hodgdon) Sorrie &amp; Weakley" date="unknown" rank="variety">ramosissima</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 370. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus lechea;species pulchella;variety ramosissima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101232</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lechea</taxon_name>
    <taxon_name authority="Britton &amp; Hollick" date="unknown" rank="species">leggettii</taxon_name>
    <taxon_name authority="Hodgdon" date="unknown" rank="variety">ramosissima</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>40, 119, plate 491, fig. 3. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lechea;species leggettii;variety ramosissima</taxon_hierarchy>
  </taxon_identification>
  <number>10c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Pedicels 1.6–2.2 mm, ± equaling or longer than calyx.</text>
      <biological_entity id="o8640" name="pedicel" name_original="pedicels" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s0" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s0" value="equaling" value_original="equaling" />
        <character constraint="than calyx" constraintid="o8641" is_modifier="false" name="length_or_size" src="d0_s0" value="more or less equaling or longer" />
      </biological_entity>
      <biological_entity id="o8641" name="calyx" name_original="calyx" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Calyces greenish purple at maturity, 1.6–1.8 mm.</text>
      <biological_entity id="o8642" name="calyx" name_original="calyces" src="d0_s1" type="structure">
        <character constraint="at maturity , 1.6-1.8 mm" is_modifier="false" name="coloration" src="d0_s1" value="greenish purple" value_original="greenish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Capsules loosely racemose.</text>
      <biological_entity id="o8643" name="capsule" name_original="capsules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s2" value="racemose" value_original="racemose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seeds 2 (–3).</text>
      <biological_entity id="o8644" name="seed" name_original="seeds" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer; fruiting late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil of pine barrens, pine-oak woodlands, sandhills, savannas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" constraint="of pine barrens , pine-oak woodlands , sandhills ," />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="sandhills" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>