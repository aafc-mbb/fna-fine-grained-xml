<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
    <other_info_on_meta type="illustration_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Fabricius" date="1759" rank="genus">malvaviscus</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">arboreus</taxon_name>
    <place_of_publication>
      <publication_title>Diss.</publication_title>
      <place_in_publication>3: 131. 1787</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malvaviscus;species arboreus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200013750</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hibiscus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">malvaviscus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 694. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hibiscus;species malvaviscus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–3 [–10] m.</text>
      <biological_entity id="o17845" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades elliptic to broadly ovate, 4–20 [–25] × 3–12 cm, surfaces sparsely to densely hairy with simple and stellate trichomes.</text>
      <biological_entity id="o17846" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s1" to="broadly ovate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s1" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17848" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucellar bractlets linear-spatulate.</text>
      <biological_entity id="o17847" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character constraint="with trichomes" constraintid="o17848" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17849" name="bractlet" name_original="bractlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-spatulate" value_original="linear-spatulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers ascending or erect;</text>
      <biological_entity id="o17850" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>calyx persistent, lobes connate for 1/2–2/3 their lengths, 8–15 mm, glabrous or hirsute;</text>
      <biological_entity id="o17851" name="calyx" name_original="calyx" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o17852" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character constraint="for 1/2-2/3 their lengths" is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals imbricate at anthesis, asymmetrically obovate-cuneate and auriculate toward base, 1.5–4 (–5) cm;</text>
      <biological_entity id="o17853" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="asymmetrically" name="shape" src="d0_s5" value="obovate-cuneate" value_original="obovate-cuneate" />
        <character constraint="toward base" constraintid="o17854" is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" notes="" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17854" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>staminal column with 5 apical teeth;</text>
      <biological_entity constraint="staminal" id="o17855" name="column" name_original="column" src="d0_s6" type="structure" />
      <biological_entity constraint="apical" id="o17856" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="5" value_original="5" />
      </biological_entity>
      <relation from="o17855" id="r1891" name="with" negation="false" src="d0_s6" to="o17856" />
    </statement>
    <statement id="d0_s7">
      <text>stigmas exserted.</text>
      <biological_entity id="o17857" name="stigma" name_original="stigmas" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits berrylike, usually red, not winged, 8–13 [–16] × 10–17 mm, fleshy, edible;</text>
      <biological_entity id="o17858" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="berrylike" value_original="berrylike" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="17" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="toxicity" src="d0_s8" value="edible" value_original="edible" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps broadly ellipsoid wedge-shaped, smooth, glabrous.</text>
      <biological_entity id="o17859" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="wedge--shaped" value_original="wedge--shaped" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds reniform, 3/4 as wide as long.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 56.</text>
      <biological_entity id="o17860" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="reniform" value_original="reniform" />
        <character name="quantity" src="d0_s10" value="3/4" value_original="3/4" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17861" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., Tex.; Mexico, West Indies, Central America, n South America (Colombia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America (Colombia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually unlobed, longer than wide, apex acute, base rounded (to subcordate); stems stellate-hairy to glabrate (glabrous).</description>
      <determination>1a Malvaviscus arboreus var. arboreus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades deeply 3-lobed, at least as long as wide, apex obtuse (acute), base usually strongly cordate; stems glabrous proximally, densely and minutely tomentose distally.</description>
      <determination>1b Malvaviscus arboreus var. drummondii</determination>
    </key_statement>
  </key>
</bio:treatment>