<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">caespitosa</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>12: 4. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species caespitosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101171</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Tufted globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o13069" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–5), erect, gray-green, 2–2.5 dm, white-gray canescent.</text>
      <biological_entity id="o13070" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white-gray" value_original="white-gray" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades gray-green or green, ovate, unlobed or 3-lobed, 5–8 cm, wrinkled, not rugose, base cuneate, margins coarsely toothed, surfaces stellate-pubescent.</text>
      <biological_entity id="o13071" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="5" from_unit="cm" name="distance" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" name="relief" src="d0_s2" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o13072" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13073" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o13074" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemose, open, 3–6-flowered, tip not leafy;</text>
      <biological_entity id="o13075" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-6-flowered" value_original="3-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets tan.</text>
      <biological_entity id="o13076" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o13077" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="tan" value_original="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 11–15 mm;</text>
      <biological_entity id="o13078" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o13079" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals red-orange, 15–22 mm;</text>
      <biological_entity id="o13080" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o13081" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="red-orange" value_original="red-orange" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow.</text>
      <biological_entity id="o13082" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o13083" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps flattened-spheric;</text>
      <biological_entity id="o13084" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened-spheric" value_original="flattened-spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 13, 3–6 × 2–4.5 mm, chartaceous, nonreticulate dehiscent part 60% of height, tip rounded, indehiscent part not wider than dehiscent part.</text>
      <biological_entity id="o13085" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o13086" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o13087" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="60%" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o13088" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="indehiscent" value_original="indehiscent" />
        <character constraint="than dehiscent part" constraintid="o13089" is_modifier="false" name="width" src="d0_s9" value="not wider" value_original="not wider" />
      </biological_entity>
      <biological_entity id="o13089" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds 1 or 2 per mericarp, brown or black, minutely pubescent.</text>
      <biological_entity id="o13090" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or per" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13091" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades thick, gray-green, densely pubescent; staminal column 3.5–5 mm; Beaver and Millard counties, Utah.</description>
      <determination>3a Sphaeralcea caespitosa var. caespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades thin, green, sparsely pubescent; staminal column 6–9 mm; Nye County, Nevada.</description>
      <determination>3b Sphaeralcea caespitosa var. williamsiae</determination>
    </key_statement>
  </key>
</bio:treatment>