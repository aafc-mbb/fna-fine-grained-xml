<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
    <other_info_on_meta type="illustration_page">384</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">thymelaeaceae</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="genus">thymelaea</taxon_name>
    <taxon_name authority="(Linnaeus) Cosson &amp; Germain" date="unknown" rank="species">passerina</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Anal. Fl. Paris ed.</publication_title>
      <place_in_publication>2, 360. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thymelaeaceae;genus thymelaea;species passerina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250062934</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellera</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">passerina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 559. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stellera;species passerina</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Passarine annuelle</other_name>
  <other_name type="common_name">spurge flax</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems green or yellow-green, turning red in fall.</text>
      <biological_entity id="o10068" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o10069" name="fall" name_original="fall" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="red" value_original="red" />
      </biological_entity>
      <relation from="o10068" id="r1092" name="turning" negation="false" src="d0_s0" to="o10069" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves upright;</text>
      <biological_entity id="o10070" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="upright" value_original="upright" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole to 1 mm;</text>
      <biological_entity id="o10071" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 0.6–1.5 × 0.1–0.2 cm, stiff, herbaceous to coriaceous, apex acute.</text>
      <biological_entity id="o10072" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s3" to="0.2" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s3" to="coriaceous" />
      </biological_entity>
      <biological_entity id="o10073" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences cymose, 1–7-flowered;</text>
      <biological_entity id="o10074" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 1.5 cm, with tuft of white trichomes at base.</text>
      <biological_entity id="o10075" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o10076" name="tuft" name_original="tuft" src="d0_s5" type="structure" />
      <biological_entity id="o10077" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o10078" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o10075" id="r1093" name="with" negation="false" src="d0_s5" to="o10076" />
      <relation from="o10076" id="r1094" name="part_of" negation="false" src="d0_s5" to="o10077" />
      <relation from="o10076" id="r1095" name="at" negation="false" src="d0_s5" to="o10078" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium green to yellow, tubular, becoming urceolate, 2–3 mm, appressed-hairy;</text>
      <biological_entity id="o10079" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10080" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="yellow" />
        <character is_modifier="false" name="shape" src="d0_s6" value="tubular" value_original="tubular" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calyx lobes minute;</text>
      <biological_entity id="o10081" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="calyx" id="o10082" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens: distal whorl inserted at throat;</text>
      <biological_entity id="o10083" name="stamen" name_original="stamens" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o10084" name="whorl" name_original="whorl" src="d0_s8" type="structure" />
      <biological_entity id="o10085" name="throat" name_original="throat" src="d0_s8" type="structure" />
      <relation from="o10084" id="r1096" name="inserted at" negation="false" src="d0_s8" to="o10085" />
    </statement>
    <statement id="d0_s9">
      <text>distal anthers subexserted;</text>
      <biological_entity id="o10086" name="stamen" name_original="stamens" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o10087" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subexserted" value_original="subexserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 0.75 mm, apex hairy;</text>
      <biological_entity id="o10088" name="stamen" name_original="stamens" src="d0_s10" type="structure" />
      <biological_entity id="o10089" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.75" value_original="0.75" />
      </biological_entity>
      <biological_entity id="o10090" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.7–0.8 mm, glabrous, becoming ± eccentric in fruit;</text>
      <biological_entity id="o10091" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o10092" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character constraint="in fruit" constraintid="o10093" is_modifier="false" modifier="becoming more or less" name="position" src="d0_s11" value="eccentric" value_original="eccentric" />
      </biological_entity>
      <biological_entity id="o10093" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stigma exserted.</text>
      <biological_entity id="o10094" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o10095" name="stigma" name_original="stigma" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules pyriform.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o10096" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10097" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (late Jun–Aug); fruiting summer–fall (Jul–early Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="late Jun-Aug" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="early Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, disturbed ground, including prairies and old fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="disturbed ground" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="old fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ill., Iowa, Kans., Mich., Miss., Nebr., Ohio, Tex., Wash., Wis.; s Europe; Mediterranean regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="Mediterranean regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Thymelaea passerina is sometimes accidentally introduced in fodder and spreads quickly. It is sometimes mistaken for one of the delicate species of Polygala.</discussion>
  
</bio:treatment>