<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">289</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">malva</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">moschata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 690. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malva;species moschata;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242416822</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Musk-mallow</other_name>
  <other_name type="common_name">mauve musquée</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.3–1.3 m, hairs usually spreading, simple, sometimes stellate-hairy distally.</text>
      <biological_entity id="o8121" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="1.3" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o8122" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s0" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, sparsely hirsute proximally, stellate-hairy distally.</text>
      <biological_entity id="o8123" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, linear to narrowly oblong-lanceolate, 3–8 × 2–3 mm;</text>
      <biological_entity id="o8124" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8125" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="narrowly oblong-lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s2" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles of proximal leaf-blades 3 times as long as blade, reduced to 1/2 blade length distally, midstem petioles 2 times as long as blade, hairs simple;</text>
      <biological_entity id="o8126" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o8127" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o8129" is_modifier="false" name="length" src="d0_s3" value="3 times as long as blade" />
        <character constraint="to blade" constraintid="o8130" is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8128" name="leaf-blade" name_original="leaf-blades" src="d0_s3" type="structure" />
      <biological_entity id="o8129" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o8130" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o8131" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o8132" is_modifier="false" name="length" src="d0_s3" value="2 times as long as blade" />
      </biological_entity>
      <biological_entity id="o8132" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o8133" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o8127" id="r864" name="part_of" negation="false" src="d0_s3" to="o8128" />
    </statement>
    <statement id="d0_s4">
      <text>distal blades usually round to reniform, deeply 5–7-lobed, lobes acutely 2-pinnatifid, 2–6 × 5–6 cm, base deeply cordate, margins irregularly toothed, apex rounded, obtuse, or acute, surfaces glabrous or sparsely hairy, hairs simple or stellate.</text>
      <biological_entity id="o8134" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o8135" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually round" name="shape" src="d0_s4" to="reniform" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="5-7-lobed" value_original="5-7-lobed" />
      </biological_entity>
      <biological_entity id="o8136" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="acutely" name="shape" src="d0_s4" value="2-pinnatifid" value_original="2-pinnatifid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8137" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o8138" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o8139" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8140" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8141" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, flowers solitary or in fascicles, often appearing short-racemose or subumbellate terminally, long-stalked.</text>
      <biological_entity id="o8142" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o8143" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="in fascicles" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="in fascicles" value_original="in fascicles" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s5" value="short-racemose" value_original="short-racemose" />
        <character is_modifier="false" modifier="terminally" name="architecture" src="d0_s5" value="subumbellate" value_original="subumbellate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="long-stalked" value_original="long-stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels (0.5–) 0.8–2.5 cm, to 10–35 cm in fruit, hairs simple;</text>
      <biological_entity id="o8144" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8145" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8145" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets distinct, not adnate to calyx, linear to narrowly oblanceolate or elliptic, 5 × 1–1.5 mm, to 7–8 mm in fruit, length 1/2 calyx, margins entire, surfaces glabrous or sparsely hirsute and long-ciliate.</text>
      <biological_entity id="o8146" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o8147" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character constraint="to calyx" constraintid="o8148" is_modifier="false" modifier="not" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s7" to="narrowly oblanceolate or elliptic" />
        <character name="area" src="d0_s7" unit="mm" value="5×1-1.5" value_original="5×1-1.5" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8149" from="7" from_unit="mm" name="area" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8148" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o8149" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o8150" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o8151" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="length" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8152" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx reticulate-veined, 6–8 mm, to 15 mm in fruit, outer surface hairy, hairs both simple and stellate;</text>
      <biological_entity id="o8153" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8154" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="reticulate-veined" value_original="reticulate-veined" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o8155" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8155" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity constraint="outer" id="o8156" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8157" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals bright pink to pale-purple or white, 20–35 mm, length 2.5–3 times calyx;</text>
      <biological_entity id="o8158" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8159" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="bright pink" name="coloration" src="d0_s9" to="pale-purple or white" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character constraint="calyx" constraintid="o8160" is_modifier="false" name="length" src="d0_s9" value="2.5-3 times calyx" value_original="2.5-3 times calyx" />
      </biological_entity>
      <biological_entity id="o8160" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminal column 7–8 (–10) mm, glabrate;</text>
      <biological_entity id="o8161" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o8162" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 11–15-branched;</text>
      <biological_entity id="o8163" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8164" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="11-15-branched" value_original="11-15-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 11–15.</text>
      <biological_entity id="o8165" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8166" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Schizocarps 9–11 mm diam.;</text>
      <biological_entity id="o8167" name="schizocarp" name_original="schizocarps" src="d0_s13" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s13" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 11–15, black, 1.5–2 mm, apical face and margins rounded, sides thin and papery, smooth, surfaces densely hirsute at least apically.</text>
      <biological_entity id="o8168" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s14" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="black" value_original="black" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8169" name="face" name_original="face" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8170" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o8171" name="side" name_original="sides" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s14" value="papery" value_original="papery" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o8172" name="surface" name_original="surfaces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="densely; at-least apically; apically" name="pubescence" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1.2–1.5 mm. 2n = 42.</text>
      <biological_entity id="o8173" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8174" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Del., D.C., Idaho, Ill., Ind., Ky., Maine, Md., Mass., Mich., Mo., Mont., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; sw Asia (Turkey); n Africa; introduced also in South America (Chile), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia (Turkey)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in South America (Chile)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Malva moschata is native from Spain to the British Isles, Poland, southern Russia, and Turkey. It has become naturalized in North America, especially in temperate northern and coastal areas. It is widely cultivated as an ornamental and frequently escapes. It occasionally hybridizes with M. sylvestris (Malva ×inodora Ponert) and M. alcea (Malva ×intermedia Boreau). It is similar to M. alcea, from which it can be distinguished by its narrower involucellar bractlets and densely hirsute mericarps.</discussion>
  
</bio:treatment>