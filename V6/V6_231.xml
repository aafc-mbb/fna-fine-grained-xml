<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="N. H. Holmgren &amp; P. K. Holmgren" date="unknown" rank="species">lithion</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>44: 300, fig. 1A – D. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species lithion</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100923</other_info_on_name>
  </taxon_identification>
  <number>31.</number>
  <other_name type="common_name">Rock violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 5–15 cm.</text>
      <biological_entity id="o20145" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, ascending to erect, glabrous, on caudex from fleshy rhizome.</text>
      <biological_entity id="o20146" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20147" name="caudex" name_original="caudex" src="d0_s1" type="structure" />
      <biological_entity id="o20148" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <relation from="o20146" id="r2131" name="on" negation="false" src="d0_s1" to="o20147" />
      <relation from="o20147" id="r2132" name="from" negation="false" src="d0_s1" to="o20148" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o20149" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 1–4;</text>
      <biological_entity constraint="basal" id="o20150" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules unknown;</text>
      <biological_entity constraint="basal" id="o20151" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20152" name="stipule" name_original="stipules" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–10 cm, glabrous, sometimes finely puberulent;</text>
      <biological_entity constraint="basal" id="o20153" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20154" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes finely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade broadly ovate or deltate to broadly deltate, 1–2.5 (–2.9) × 0.6–2.2 (–2.6) cm, base usually cordate to truncate, rarely rounded, margins crenate-dentate, eciliate, apex acute, surfaces glabrous;</text>
      <biological_entity constraint="basal" id="o20155" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o20156" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="deltate" name="shape" src="d0_s6" to="broadly deltate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2.9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="2.6" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s6" to="2.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20157" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually cordate" name="shape" src="d0_s6" to="truncate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o20158" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="crenate-dentate" value_original="crenate-dentate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o20159" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o20160" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: stipules lanceolate, margins usually fimbriate-toothed, sometimes entire, apex attenuate or acute;</text>
      <biological_entity constraint="cauline" id="o20161" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o20162" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o20163" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o20164" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="fimbriate-toothed" value_original="fimbriate-toothed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20165" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o20161" id="r2133" name="to" negation="false" src="d0_s7" to="o20162" />
      <relation from="o20162" id="r2134" name="except" negation="false" src="d0_s7" to="o20163" />
    </statement>
    <statement id="d0_s8">
      <text>petiole 1.1–3.7 cm, sometimes finely puberulent;</text>
      <biological_entity constraint="cauline" id="o20166" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes finely" name="pubescence" notes="" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20167" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o20168" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.1" from_unit="cm" name="some_measurement" src="d0_s8" to="3.7" to_unit="cm" />
      </biological_entity>
      <relation from="o20166" id="r2135" name="to" negation="false" src="d0_s8" to="o20167" />
      <relation from="o20167" id="r2136" name="except" negation="false" src="d0_s8" to="o20168" />
    </statement>
    <statement id="d0_s9">
      <text>blade ovate to deltate, 0.7–2.2 × 0.4–1.2 cm, base sometimes rounded on distal blades.</text>
      <biological_entity constraint="cauline" id="o20169" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" notes="" src="d0_s9" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" notes="" src="d0_s9" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20170" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o20171" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="deltate" />
      </biological_entity>
      <biological_entity id="o20172" name="base" name_original="base" src="d0_s9" type="structure">
        <character constraint="on distal blades" constraintid="o20173" is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20173" name="blade" name_original="blades" src="d0_s9" type="structure" />
      <relation from="o20169" id="r2137" name="to" negation="false" src="d0_s9" to="o20170" />
      <relation from="o20170" id="r2138" name="except" negation="false" src="d0_s9" to="o20171" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 3–6 (–10) cm, glabrous.</text>
      <biological_entity id="o20174" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals lanceolate, margins eciliate, auricles 0.5–1 mm;</text>
      <biological_entity id="o20175" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20176" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o20177" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o20178" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals blue to pale violet on both surfaces with yellow area basally, lower 3 purple-veined, lowest with yellow area, lateral 2 bearded, lowest 5.5–11 mm, spur white to pale violet, gibbous, 0.5–1.3 mm;</text>
      <biological_entity id="o20179" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o20180" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="on surfaces" constraintid="o20181" from="blue" name="coloration" src="d0_s12" to="pale violet" />
      </biological_entity>
      <biological_entity id="o20181" name="surface" name_original="surfaces" src="d0_s12" type="structure" />
      <biological_entity id="o20182" name="area" name_original="area" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="lower" id="o20183" name="area" name_original="area" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="purple-veined" value_original="purple-veined" />
        <character constraint="with area" constraintid="o20184" is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character is_modifier="false" name="position" notes="" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20184" name="area" name_original="area" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o20185" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="pale violet" />
        <character is_modifier="false" name="shape" src="d0_s12" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <relation from="o20181" id="r2139" name="with" negation="false" src="d0_s12" to="o20182" />
    </statement>
    <statement id="d0_s13">
      <text>style head bearded;</text>
      <biological_entity id="o20186" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="style" id="o20187" name="head" name_original="head" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>cleistogamous flowers unknown.</text>
      <biological_entity id="o20188" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20189" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules subglobose, ca. 5 mm, glabrous.</text>
      <biological_entity id="o20190" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="5" value_original="5" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, ca. 1.8 mm.</text>
      <biological_entity id="o20191" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="1.8" value_original="1.8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet cracks and crevices, narrow ledges of rock outcrops, shaded northeast-facing avalanche chutes, cirque headwalls, subalpine conifer zone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet cracks" modifier="seasonally" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="narrow ledges" constraint="of rock outcrops" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="northeast-facing avalanche chutes" modifier="shaded" />
        <character name="habitat" value="cirque headwalls" />
        <character name="habitat" value="subalpine conifer zone" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2300–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Viola lithion is known only from the White Pine Range in Nevada and the Pilot Range straddling the Nevada-Utah border. It is related to V. canadensis and V. flettii.</discussion>
  
</bio:treatment>