<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sida</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">urens</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>10: 1145. 1759</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sida;species urens;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242430138</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">verticillata</taxon_name>
    <taxon_hierarchy>genus Sida;species verticillata</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Bristly sida</other_name>
  <other_name type="common_name">tropical fanpetals</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, perennial, often scandent, 0.5–1.5 m.</text>
      <biological_entity id="o5938" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="scandent" value_original="scandent" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="scandent" value_original="scandent" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1.5" to_unit="m" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or reclining, with simple 1.5–3 mm hairs mixed with shorter stellate hairs, rarely only stellate-hairy.</text>
      <biological_entity id="o5940" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
        <character is_modifier="false" modifier="rarely only" name="pubescence" notes="" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o5941" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="1.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character constraint="with shorter hairs" constraintid="o5942" is_modifier="false" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o5942" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o5940" id="r671" name="with" negation="false" src="d0_s1" to="o5941" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules free from petiole, 1-veined, subulate, 2–5 mm;</text>
      <biological_entity id="o5943" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5944" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character constraint="from petiole" constraintid="o5945" is_modifier="false" name="fusion" src="d0_s2" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5945" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–30 mm, 1/4–1/2 (to nearly equaling) blade length, pubescence like stem;</text>
      <biological_entity id="o5946" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5947" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity id="o5948" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o5949" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="like" value_original="like" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to triangular, 4–9 cm, 1.5–2 times longer than wide, base cordate, margins crenate-serrate or coarsely serrate to base, apex acuminate or attenuate, surfaces sparsely pubescent, abaxial surface stellate-pubescent, adaxial surface stellate-pubescent or with simple, often antrorsely-oriented hairs.</text>
      <biological_entity id="o5950" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5951" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="triangular" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="9" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="1.5-2" value_original="1.5-2" />
      </biological_entity>
      <biological_entity id="o5952" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o5953" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
        <character constraint="to base" constraintid="o5954" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o5954" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o5955" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o5956" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o5957" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o5958" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with simple , often antrorsely-oriented hairs" />
      </biological_entity>
      <biological_entity id="o5959" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s4" value="antrorsely-oriented" value_original="antrorsely-oriented" />
      </biological_entity>
      <relation from="o5958" id="r672" name="with" negation="false" src="d0_s4" to="o5959" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, dense, subsessile, 3–8- glabrous or nearly so;</text>
      <biological_entity id="o5960" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="3-8-glabrous" value_original="3-8-glabrous" />
        <character name="pubescence" src="d0_s5" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>mericarps 5, 3 × 1.5 mm, laterally faintly striate to smooth, apex muticous.</text>
      <biological_entity id="o5961" name="mericarp" name_original="mericarps" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="5" value_original="5" />
        <character name="length" src="d0_s6" unit="mm" value="3" value_original="3" />
        <character name="width" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="laterally faintly striate" name="pubescence" src="d0_s6" to="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>2n = 32.</text>
      <biological_entity id="o5962" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="muticous" value_original="muticous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5963" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Mexico; West Indies; Central America; South America; introduced also in Africa, Indian Ocean Islands (Madagascar), Pacific Islands (Hawaii).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="also in Africa" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sida urens was found only recently (2008) in Broward County. The species is easily distinguished by its long-acuminate beaked flower buds, setose calyx, cordate-acuminate leaves, and tendency to have long, reclining stems. It is rather common in tropical regions.</discussion>
  
</bio:treatment>