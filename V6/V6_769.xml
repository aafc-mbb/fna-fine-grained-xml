<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">403</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cistaceae</taxon_name>
    <taxon_name authority="Spach" date="1836" rank="genus">crocanthemum</taxon_name>
    <taxon_name authority="(E. P. Bicknell) E. P. Bicknell" date="unknown" rank="species">propinquum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>40: 615. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cistaceae;genus crocanthemum;species propinquum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101253</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Helianthemum</taxon_name>
    <taxon_name authority="E. P. Bicknell" date="unknown" rank="species">propinquum</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton, Man. Fl. N. States ed.</publication_title>
      <place_in_publication>2, 1069. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Helianthemum;species propinquum</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Low or creeping frostweed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs.</text>
      <biological_entity id="o5056" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems scattered on horizontal rootstocks, ascending to erect, 10–27 (–35) cm, stellate-pubescent to stellate-tomentose.</text>
      <biological_entity id="o5057" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="on rootstocks" constraintid="o5058" is_modifier="false" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="ascending" name="orientation" notes="" src="d0_s1" to="erect" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="35" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="27" to_unit="cm" />
        <character char_type="range_value" from="stellate-pubescent" name="pubescence" src="d0_s1" to="stellate-tomentose" />
      </biological_entity>
      <biological_entity id="o5058" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
      <biological_entity id="o5059" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–5 mm;</text>
      <biological_entity id="o5060" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic to oblanceolate, gradually narrowed to base, 10–30 × 3–6 (–8) mm, surfaces stellate-tomentose abaxially, stellate-pubescent adaxially, without simple hairs, lateral-veins raised abaxially.</text>
      <biological_entity id="o5061" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="oblanceolate" />
        <character constraint="to base" constraintid="o5062" is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5062" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o5063" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="stellate-tomentose" value_original="stellate-tomentose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <biological_entity id="o5064" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o5065" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s4" value="raised" value_original="raised" />
      </biological_entity>
      <relation from="o5063" id="r587" name="without" negation="false" src="d0_s4" to="o5064" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, cymes;</text>
      <biological_entity id="o5066" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5067" name="cyme" name_original="cymes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>chasmogamous flowers 2–6 per cyme, cleistogamous in glomerules, 1–6 flowers per glomerule, on lateral leafy branches 1–3 cm, flowering 1–3 months later than chasmogamous.</text>
      <biological_entity id="o5068" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="chasmogamous" value_original="chasmogamous" />
        <character char_type="range_value" constraint="per cyme" constraintid="o5069" from="2" name="quantity" src="d0_s6" to="6" />
        <character constraint="in glomerules" constraintid="o5070" is_modifier="false" name="reproduction" notes="" src="d0_s6" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o5069" name="cyme" name_original="cyme" src="d0_s6" type="structure" />
      <biological_entity id="o5070" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o5071" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s6" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" name="condition" src="d0_s6" value="later than chasmogamous" value_original="later than chasmogamous" />
      </biological_entity>
      <biological_entity id="o5072" name="glomerule" name_original="glomerule" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o5073" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <relation from="o5071" id="r588" name="per" negation="false" src="d0_s6" to="o5072" />
      <relation from="o5071" id="r589" name="on" negation="false" src="d0_s6" to="o5073" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels (2–) 8–14 (–22) mm;</text>
      <biological_entity id="o5074" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="22" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 1.5–3.5 × 0.3 mm.</text>
      <biological_entity id="o5075" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="3.5" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Chasmogamous flowers: outer sepals linear, 1–3 (–4) × 0.4–0.9 mm, inner sepals ovate-elliptic, 5–8 × 2.3–4.5 mm, apex acute;</text>
      <biological_entity id="o5076" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5077" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5078" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5079" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals obovate, 8–10 (–13) × 6–12 mm;</text>
      <biological_entity id="o5080" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o5081" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>capsules 3.7–5.3 × 3–4 mm, glabrous.</text>
      <biological_entity id="o5082" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="chasmogamous" value_original="chasmogamous" />
      </biological_entity>
      <biological_entity id="o5083" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s11" to="5.3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cleistogamous flowers: outer sepals rudimentary, 0.2–0.5 × 0.2 mm, inner sepals ovate, 2–2.5 × 1.5–2.2 mm, apex acute;</text>
      <biological_entity id="o5084" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o5085" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s12" to="0.5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5086" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5087" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>capsules 1.5–2.2 × 1.3–2 mm, glabrous.</text>
      <biological_entity id="o5088" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity id="o5089" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woodlands, rock outcrops, sandplain grasslands, maritime heathlands, clearings, fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="sandplain grasslands" />
        <character name="habitat" value="maritime heathlands" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn., Del., D.C., Ga., Md., Mass., N.H., N.J., N.Y., N.C., Pa., R.I., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The shorter stature, tiny outer sepals on cleistogamous flowers, and horizontal rootstocks distinguish Crocanthemum propinquum from C. bicknellii, which is often twice as tall, has elongate sepals, and has a caudex. Crocanthemum propinquum is disjunct from the coastal plain of Virginia to the southern Appalachian Mountains of Georgia, North Carolina, and Tennessee.</discussion>
  
</bio:treatment>