<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">hibiscus</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="species">clypeatus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">clypeatus</taxon_name>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus hibiscus;species clypeatus;subspecies clypeatus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101061</other_info_on_name>
  </taxon_identification>
  <number>21a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 2–5 m.</text>
      <biological_entity id="o23194" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: new growth ± stellate and usually simple-hairy, line of fine, curved hairs absent or obscured.</text>
      <biological_entity id="o23196" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o23197" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="simple-hairy" value_original="simple-hairy" />
      </biological_entity>
      <biological_entity id="o23198" name="line" name_original="line" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s1" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o23199" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o23198" id="r2450" name="consist_of" negation="false" src="d0_s1" to="o23199" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules subulate, 3–18 mm;</text>
      <biological_entity id="o23200" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23201" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole of principal leaves 1/2 to equaling blade, finely hairy;</text>
      <biological_entity id="o23202" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o23203" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" notes="" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="principal" id="o23204" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="to blade" constraintid="o23205" name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o23205" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o23203" id="r2451" name="consist_of" negation="false" src="d0_s3" to="o23204" />
    </statement>
    <statement id="d0_s4">
      <text>blade ovate to orbiculate, angulate-lobate (maple or grapelike) or less frequently unlobed, 10–22 × 9–23 cm, base deeply and narrowly cordate to truncate, margins remotely and shallowly denticulate or undulate, apex acute to acuminate, surfaces stellate-tomentose, more densely so abaxially, nectary absent from base of midvein abaxially.</text>
      <biological_entity id="o23206" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23207" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="angulate-lobate" value_original="angulate-lobate" />
        <character is_modifier="false" modifier="less frequently" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="22" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="width" src="d0_s4" to="23" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23208" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly cordate" name="shape" src="d0_s4" to="truncate" />
      </biological_entity>
      <biological_entity id="o23209" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o23210" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o23211" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stellate-tomentose" value_original="stellate-tomentose" />
      </biological_entity>
      <biological_entity id="o23212" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character constraint="from base" constraintid="o23213" is_modifier="false" modifier="densely; abaxially" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23213" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o23214" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <relation from="o23213" id="r2452" name="part_of" negation="false" src="d0_s4" to="o23214" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves, or subcorymbose by reduction of internodes and subtending leaves.</text>
      <biological_entity id="o23215" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o23216" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character constraint="by reduction" constraintid="o23219" is_modifier="false" name="arrangement" notes="" src="d0_s5" value="subcorymbose" value_original="subcorymbose" />
      </biological_entity>
      <biological_entity id="o23217" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o23218" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o23219" name="reduction" name_original="reduction" src="d0_s5" type="structure" />
      <biological_entity id="o23220" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity id="o23221" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o23216" id="r2453" name="in" negation="false" src="d0_s5" to="o23217" />
      <relation from="o23217" id="r2454" name="part_of" negation="false" src="d0_s5" to="o23218" />
      <relation from="o23219" id="r2455" name="part_of" negation="false" src="d0_s5" to="o23220" />
      <relation from="o23216" id="r2456" name="subtending" negation="false" src="d0_s5" to="o23221" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels jointed at base or not at all, 3–10 cm (often conspicuously elongated), subequal to or exceeding petioles, finely densely hairy;</text>
      <biological_entity id="o23223" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23224" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23225" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o23226" name="petiole" name_original="petioles" src="d0_s6" type="structure" />
      <biological_entity id="o23227" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23228" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="at " constraintid="o23232" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23229" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23230" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23231" name="petiole" name_original="petioles" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o23232" name="petiole" name_original="petioles" src="d0_s6" type="structure" />
      <relation from="o23226" id="r2457" modifier="at all" name="at" negation="false" src="d0_s6" to="o23227" />
      <relation from="o23226" id="r2458" modifier="at all; at base or , , subequal to or exceeding petioles" name="at" negation="false" src="d0_s6" to="o23228" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets 7–10, linear-subulate to narrowly triangular, often randomly curved (sickle-shaped), 1–3 cm, margins not ciliate, densely short stellate-hairy.</text>
      <biological_entity id="o23222" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="at " constraintid="o23226" is_modifier="false" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
        <character is_modifier="false" modifier="finely densely" name="pubescence" notes="" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23233" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="linear-subulate" name="shape" src="d0_s7" to="narrowly triangular" />
        <character is_modifier="false" modifier="often randomly" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23234" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="densely" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers horizontal;</text>
      <biological_entity id="o23235" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx divided 1/3 length, campanulate, 3–4.5 cm, somewhat larger in fruit, more densely hairy basally, lobes triangular, apices short-acuminate, nectaries absent;</text>
      <biological_entity id="o23236" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="divided" value_original="divided" />
        <character name="length" src="d0_s9" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
        <character constraint="in fruit" constraintid="o23237" is_modifier="false" modifier="somewhat" name="size" src="d0_s9" value="larger" value_original="larger" />
        <character is_modifier="false" modifier="densely; basally" name="pubescence" notes="" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23237" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o23238" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o23239" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity id="o23240" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla funnelform to narrowly campanulate, somewhat bilateral, petals dull red or dull orange, 2 upper little or not at all recurved, 3 lower recurved or revolute, 4–5.5 × 1–2 cm, margins apically entire or repand, tomentose throughout abaxially;</text>
      <biological_entity id="o23241" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s10" value="bilateral" value_original="bilateral" />
      </biological_entity>
      <biological_entity id="o23242" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orange" value_original="orange" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character constraint="at " constraintid="o23244" is_modifier="false" name="position" src="d0_s10" value="upper" value_original="upper" />
        <character is_modifier="false" modifier="throughout abaxially; abaxially" name="pubescence" notes="" src="d0_s10" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23243" name="recurved" name_original="recurved" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o23244" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s10" value="revolute" value_original="revolute" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="5.5" to_unit="cm" />
        <character char_type="range_value" constraint="at " constraintid="o23246" from="1" from_unit="cm" name="width" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23245" name="recurved" name_original="recurved" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o23246" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s10" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s10" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminal column declinate to recurved, pale yellowish to dull orange, 2.8–4.2 cm, bearing filaments in apical 1/3–1/2, free portion of filaments secund, 4–8 (–10) mm;</text>
      <biological_entity constraint="staminal" id="o23247" name="column" name_original="column" src="d0_s11" type="structure">
        <character char_type="range_value" from="declinate" name="orientation" src="d0_s11" to="recurved" />
        <character char_type="range_value" from="pale yellowish" name="coloration" src="d0_s11" to="dull orange" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="some_measurement" src="d0_s11" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s11" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23248" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <biological_entity id="o23249" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="position" src="d0_s11" value="apical" value_original="apical" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s11" to="1/2" />
        <character is_modifier="true" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o23250" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <relation from="o23247" id="r2459" name="bearing" negation="false" src="d0_s11" to="o23248" />
      <relation from="o23247" id="r2460" name="in" negation="false" src="d0_s11" to="o23249" />
      <relation from="o23249" id="r2461" name="part_of" negation="false" src="d0_s11" to="o23250" />
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow to orange;</text>
      <biological_entity id="o23251" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles dull red to dull orange, 3–6 mm;</text>
      <biological_entity id="o23252" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dull" value_original="dull" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s13" to="dull orange" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas dull red to dull orange.</text>
      <biological_entity id="o23253" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dull" value_original="dull" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s14" to="dull orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules dull orange, ovoid to obovoid, 2.5–5 cm, apex apiculate, hispid, densely underlain by minute, coarse stellate hairs, hairs orangish.</text>
      <biological_entity id="o23254" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="obovoid" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s15" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23255" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o23256" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="minute" value_original="minute" />
        <character is_modifier="true" name="relief" src="d0_s15" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s15" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o23257" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s15" value="orangish" value_original="orangish" />
      </biological_entity>
      <relation from="o23255" id="r2462" modifier="densely" name="underlain by" negation="false" src="d0_s15" to="o23256" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, mottled, subglobose, 3.5–4 mm, glabrous.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20 (Mexico: Veracruz).</text>
      <biological_entity id="o23258" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23259" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riparian woodlands, alluvial soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riparian woodlands" />
        <character name="habitat" value="alluvial soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; ne, se Mexico; West Indies (Hispaniola, Jamaica, Puerto Rico, Saint Croix); Central America (Belize, Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="ne" establishment_means="native" />
        <character name="distribution" value="se Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
        <character name="distribution" value="West Indies (Jamaica)" establishment_means="native" />
        <character name="distribution" value="West Indies (Puerto Rico)" establishment_means="native" />
        <character name="distribution" value="West Indies (Saint Croix)" establishment_means="native" />
        <character name="distribution" value="Central America (Belize)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies clypeatus was collected for the first time in the flora area in 1994; it was not identified until 2009. It is known in the flora area only from near the Rio Grande in Hidalgo County and deserves state and federal protection. Hummingbird visits to H. clypeatus have been observed in Belize and Mexico (D. M. Bates, pers. comm.; J. C. Meerman 1993); bats are suspected as the primary agents for pollination (O. J. Blanchard 1976).</discussion>
  
</bio:treatment>