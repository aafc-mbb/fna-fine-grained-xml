<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Cavanilles" date="1785" rank="genus">ANODA</taxon_name>
    <place_of_publication>
      <publication_title>Diss.</publication_title>
      <place_in_publication>1: 38, plate 10, fig. 3, plate 11, figs. 1, 2. 1785</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus anoda;</taxon_hierarchy>
    <other_info_on_name type="etymology">Ceylonese vernacular name for a species of Abutilon</other_info_on_name>
    <other_info_on_name type="fna_id">101895</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Medikus" date="unknown" rank="genus">Cavanillea</taxon_name>
    <taxon_hierarchy>genus Cavanillea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(A. Gray) Wooton &amp; Standley" date="unknown" rank="genus">Sidanoda</taxon_name>
    <taxon_hierarchy>genus Sidanoda</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or subshrubs, annual.</text>
      <biological_entity id="o4427" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent, hispid or stellate-hairy to glabrescent.</text>
      <biological_entity id="o4429" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character char_type="range_value" from="stellate-hairy" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules deciduous, inconspicuous, usually linear;</text>
      <biological_entity id="o4430" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4431" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade usually linear, lanceolate, oblong, or ovate to triangular, sometimes lobed, base truncate, cordate, or cuneate, margins dentate to entire.</text>
      <biological_entity id="o4432" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4433" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="triangular" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="triangular" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o4434" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4435" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="dentate" name="architecture_or_shape" src="d0_s3" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary solitary flowers or terminal racemes or panicles;</text>
      <biological_entity id="o4436" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="axillary" id="o4437" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary terminal" id="o4438" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity constraint="axillary terminal" id="o4439" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucel absent.</text>
      <biological_entity id="o4440" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx accrescent or not, not inflated, ribbed or not, base rounded, lobes ovate to triangular, apex acute or acuminate;</text>
      <biological_entity id="o4441" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4442" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="not" src="d0_s6" value="," value_original="," />
        <character is_modifier="false" modifier="not" name="not" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="not" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity id="o4443" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4444" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="triangular" />
      </biological_entity>
      <biological_entity id="o4445" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla yellow, lavender, or purplish, rarely white;</text>
      <biological_entity id="o4446" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4447" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>staminal column included;</text>
      <biological_entity id="o4448" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o4449" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 5–19-branched;</text>
      <biological_entity id="o4450" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4451" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-19-branched" value_original="5-19-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas usually abruptly capitate.</text>
      <biological_entity id="o4452" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4453" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually abruptly" name="architecture_or_shape" src="d0_s10" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits schizocarps, erect, not inflated, oblate, not indurate, hairy;</text>
      <biological_entity constraint="fruits" id="o4454" name="schizocarp" name_original="schizocarps" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblate" value_original="oblate" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>mericarps 5–19, 1-celled, with or without spur at dorsal angle, lateral walls usually disintegrating at maturity, irregularly dehiscent.</text>
      <biological_entity id="o4455" name="mericarp" name_original="mericarps" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="19" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-celled" value_original="1-celled" />
      </biological_entity>
      <biological_entity id="o4456" name="spur" name_original="spur" src="d0_s12" type="structure" />
      <biological_entity constraint="lateral" id="o4457" name="wall" name_original="walls" src="d0_s12" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="dehiscence" src="d0_s12" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <relation from="o4455" id="r514" name="with or without" negation="false" src="d0_s12" to="o4456" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1 per mericarp, sometimes enclosed in persistent reticulate endocarp.</text>
      <biological_entity id="o4459" name="mericarp" name_original="mericarp" src="d0_s13" type="structure" />
      <biological_entity id="o4460" name="endocarp" name_original="endocarp" src="d0_s13" type="structure">
        <character is_modifier="true" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character is_modifier="true" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <relation from="o4458" id="r515" modifier="sometimes" name="enclosed in" negation="false" src="d0_s13" to="o4460" />
    </statement>
    <statement id="d0_s14">
      <text>x = 15.</text>
      <biological_entity id="o4458" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character constraint="per mericarp" constraintid="o4459" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="x" id="o4461" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 23 (7 in the flora).</discussion>
  <discussion>Anoda is predominantly Mexican in both distribution and maximum diversity, the South American occurrences being predominantly of the weedy A. cristata.</discussion>
  <references>
    <reference>Bates, D. M. 1987. Chromosome numbers and evolution in Anoda and Periptera (Malvaceae). Aliso 11: 523–531.</reference>
    <reference>Fryxell, P. A. 1987. Revision of the genus Anoda (Malvaceae). Aliso 11: 485–522.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually lavender to purplish, rarely white</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals pale to bright yellow</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants frequently decumbent; petals 8–26(–30) mm, manifestly exceeding calyx, lavender or purplish, rarely white; adaxial leaf surface with appressed, simple hairs 1 mm; mericarps with dorsal spurs 1.5–4 mm.</description>
      <determination>3 Anoda cristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarps 10 or 11, without dorsal spurs; endocarp present; midstem leaves with 3 narrowly linear lobes; staminal columns usually glabrous.</description>
      <determination>6 Anoda reticulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarps 6–8, with evident, small, dorsal spurs; endocarp incompletely developed or absent; midstem leaves ovate to hastate to triangular; staminal columns hairy.</description>
      <determination>7 Anoda thurberi</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade surfaces with simple, appressed hairs adaxially; petals bright yellow; fruits hirsute, mericarps 10–12, dorsally spurred.</description>
      <determination>4 Anoda lanceolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade surfaces minutely tomentose; petals pale yellow, sometimes fading with reddish blush; fruits minutely or densely hairy, mericarps 5–13, dorsally rounded or spurred</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Mericarps 10–13, dorsal spur 1–2 mm; endocarp present; petals 6–8 mm, not fading reddish.</description>
      <determination>2 Anoda crenatiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Calyces 5–7 mm; mericarps 5; fruits 6 mm diam.; leaf blades membranous, concolorous, broadly ovate, apex acuminate.</description>
      <determination>1 Anoda abutiloides</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Calyces 3–5 mm; mericarps 5–8; fruits 4–5 mm diam.; leaf blades coriaceous, discolorous, often narrowly oblong or linear, apex acute.</description>
      <determination>5 Anoda pentaschista</determination>
    </key_statement>
  </key>
</bio:treatment>