<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Solander ex Corrêa" date="unknown" rank="genus">THESPESIA</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>9: 290, plate 25, fig. 1. 1807</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus thespesia;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek thespesios, divine, wondrous, or excellent, alluding to planting in sacred groves and use for carving religious sculpture</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">132809</other_info_on_name>
  </taxon_identification>
  <number>50</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [shrubs].</text>
      <biological_entity id="o12102" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous or hairy when young, usually glandular-punctate, not viscid.</text>
      <biological_entity id="o12103" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="usually" name="coloration_or_relief" src="d0_s1" value="glandular-punctate" value_original="glandular-punctate" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent or deciduous, lanceolate or falcate;</text>
      <biological_entity id="o12104" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12105" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate, unlobed [3-lobulate], base deeply cordate [shallowly cordate to ± truncate], margins entire, surfaces glabrate [hairy], with abaxial foliar nectaries.</text>
      <biological_entity id="o12106" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12107" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o12108" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o12109" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12110" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="abaxial foliar" id="o12111" name="nectary" name_original="nectaries" src="d0_s3" type="structure" />
      <relation from="o12110" id="r1297" name="with" negation="false" src="d0_s3" to="o12111" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences axillary solitary flowers, [sometimes aggregated apically];</text>
      <biological_entity id="o12112" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity constraint="axillary" id="o12113" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>involucel present, bractlets caducous, 3, distinct.</text>
      <biological_entity id="o12114" name="involucel" name_original="involucel" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12115" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: calyx not accrescent, not inflated, lobes truncate [to 5-lobed], not ribbed;</text>
      <biological_entity id="o12116" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o12117" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s6" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o12118" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corolla yellow [white or rose], with [without] maroon spot at base, usually fading pinkish orange;</text>
      <biological_entity id="o12119" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o12120" name="corolla" name_original="corolla" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character constraint="at base" constraintid="o12121" is_modifier="false" name="coloration" src="d0_s7" value="maroon spot" value_original="maroon spot" />
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s7" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="fading pinkish" value_original="fading pinkish" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity id="o12121" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>staminal column usually included;</text>
      <biological_entity id="o12122" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="staminal" id="o12123" name="column" name_original="column" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ovary 3–5-carpellate, style 3–5-branched;</text>
      <biological_entity id="o12124" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o12125" name="ovary" name_original="ovary" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-5-carpellate" value_original="3-5-carpellate" />
      </biological_entity>
      <biological_entity id="o12126" name="style" name_original="style" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-5-branched" value_original="3-5-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stigmas clavate.</text>
      <biological_entity id="o12127" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12128" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits capsules, erect, somewhat inflated, oblate, coriaceous [ligneous], lepidote [glabrous or hairy], indehiscent [dehiscent].</text>
      <biological_entity constraint="fruits" id="o12129" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblate" value_original="oblate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="lepidote" value_original="lepidote" />
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds 3–5 per locule, short-hairy [glabrous].</text>
      <biological_entity id="o12131" name="locule" name_original="locule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>x = 13.</text>
      <biological_entity id="o12130" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o12131" from="3" name="quantity" src="d0_s12" to="5" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity constraint="x" id="o12132" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="13" value_original="13" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Fla.; Asia, Africa, Pacific Islands (Papua New Guinea), Australia; introduced also in Mexico, West Indies, n South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Papua New Guinea)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="n South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 17 (1 in the flora).</discussion>
  <references>
    <reference>Fosberg, F. R. and M.-H. Sachet. 1972. Thespesia populnea (L.) Solander ex Corrêa and Thespesia populneoides (Roxburgh) Kosteletzky (Malvaceae). Smithsonian Contr. Bot. 7: 1–13.</reference>
  </references>
  
</bio:treatment>