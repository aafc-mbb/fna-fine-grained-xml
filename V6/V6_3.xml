<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">8</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">momordica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">balsamina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1009. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus momordica;species balsamina</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220008723</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems pubescent to glabrescent.</text>
      <biological_entity id="o19279" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s0" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 1–4 (–6) cm;</text>
      <biological_entity id="o19280" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19281" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade broadly ovate or reniform to orbiculate, palmately 3–5-lobed, 1–9 (–12) cm, base cordate, lobes broadly ovate or rhombic-ovate, sinuses 80–90% to base, margins sinuate-dentate, leaf lobes and teeth apiculate, surfaces glabrous or sparsely hairy.</text>
      <biological_entity id="o19282" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19283" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="reniform" name="shape" src="d0_s2" to="orbiculate" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s2" value="3-5-lobed" value_original="3-5-lobed" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19284" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o19285" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rhombic-ovate" value_original="rhombic-ovate" />
      </biological_entity>
      <biological_entity id="o19286" name="sinuse" name_original="sinuses" src="d0_s2" type="structure" />
      <biological_entity id="o19287" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o19288" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sinuate-dentate" value_original="sinuate-dentate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o19289" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o19290" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o19291" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o19286" id="r2033" modifier="80-90%" name="to" negation="false" src="d0_s2" to="o19287" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: staminate peduncles bracteate near apex, bracts sessile, broadly ovate-cordate to reniform, margins dentate to denticulate;</text>
      <biological_entity id="o19292" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o19293" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character constraint="near apex" constraintid="o19294" is_modifier="false" name="architecture" src="d0_s3" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o19294" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o19295" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="broadly ovate-cordate" name="shape" src="d0_s3" to="reniform" />
      </biological_entity>
      <biological_entity id="o19296" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s3" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate peduncles ebracteate or bracteate at base to submedially.</text>
      <biological_entity id="o19297" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o19298" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="ebracteate" value_original="ebracteate" />
        <character constraint="at base" constraintid="o19299" is_modifier="false" name="architecture" src="d0_s4" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o19299" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Petals yellow, obovate, 8–15 mm.</text>
      <biological_entity id="o19300" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruits orange-red, broadly ovoid, 2.5–4 (–7) cm, beak becoming less prominent at maturity, surface minutely tuberculate, muriculate in longitudinal rows.</text>
      <biological_entity id="o19301" name="fruit" name_original="fruits" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19302" name="beak" name_original="beak" src="d0_s6" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="becoming less" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o19303" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s6" value="tuberculate" value_original="tuberculate" />
        <character constraint="in rows" constraintid="o19304" is_modifier="false" name="relief" src="d0_s6" value="muriculate" value_original="muriculate" />
      </biological_entity>
      <biological_entity id="o19304" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s6" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seeds ovate-oblong, 9–12 mm. 2n = 22.</text>
      <biological_entity id="o19305" name="seed" name_original="seeds" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-oblong" value_original="ovate-oblong" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19306" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, disturbed areas, roadsides, fencerows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fencerows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Fla., La., N.Mex., Okla., Tex.; Asia; Africa; introduced also in Mexico, West Indies, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reports of Momordica balsamina from Alabama and Texas are not documented. Naturalized occurrences of the species elsewhere in the flora area are scattered and uncommon.</discussion>
  
</bio:treatment>