<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">althaea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">hirsuta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 687. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus althaea;species hirsuta;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250101029</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Rough marshmallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs annual, 0.1–0.6 m.</text>
      <biological_entity id="o5090" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to decumbent, simple or branched at base, coarsely hairy, hairs simple, rigid, stellate, pustulate-based.</text>
      <biological_entity id="o5091" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character constraint="at base" constraintid="o5092" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="coarsely" name="pubescence" notes="" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o5092" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o5093" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="pustulate-based" value_original="pustulate-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules persistent, lanceolate, simple, 5–8 mm, hirsute;</text>
      <biological_entity id="o5094" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o5095" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole as long as blade in proximal leaves, much reduced in distal leaves;</text>
      <biological_entity id="o5096" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5097" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="in distal leaves" constraintid="o5100" is_modifier="false" modifier="much" name="size" notes="" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5098" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o5099" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o5100" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o5097" id="r590" name="as long as" negation="false" src="d0_s3" to="o5098" />
      <relation from="o5098" id="r591" name="in" negation="false" src="d0_s3" to="o5099" />
    </statement>
    <statement id="d0_s4">
      <text>blade reniform, those of proximal leaves shallowly 3–5-lobed, those of distal leaves deeply, palmately 3–5-lobed or divided, 1–4 × 1.5–3.5 (–4) cm, lobes broad, blunt to acute, margins crenate, to pinnatifid, surfaces hispid-hairy.</text>
      <biological_entity id="o5101" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5102" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="3-5-lobed" value_original="3-5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5103" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o5104" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5105" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="broad" value_original="broad" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o5106" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="pinnatifid" />
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="pinnatifid" />
      </biological_entity>
      <biological_entity id="o5107" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid-hairy" value_original="hispid-hairy" />
      </biological_entity>
      <relation from="o5102" id="r592" name="part_of" negation="false" src="d0_s4" to="o5103" />
      <relation from="o5102" id="r593" name="part_of" negation="false" src="d0_s4" to="o5104" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers, long-pedicellate, infrequently racemose-congested distally.</text>
      <biological_entity id="o5108" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="long-pedicellate" value_original="long-pedicellate" />
        <character is_modifier="false" modifier="infrequently; distally" name="architecture_or_arrangement" src="d0_s5" value="racemose-congested" value_original="racemose-congested" />
      </biological_entity>
      <biological_entity id="o5109" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels/peduncles 0.5–8 mm, 8 cm proximally, reduced distally;</text>
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets 7 or 8, spreading-erect, lanceolate, 4–13 × 1–4 mm, 1/2+ as long as calyx, slightly accrescent, rigid in fruit, hispid.</text>
      <biological_entity id="o5110" name="peduncle" name_original="pedicels/peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character modifier="proximally" name="some_measurement" src="d0_s6" unit="cm" value="8" value_original="8" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5111" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" unit="or" value="7" value_original="7" />
        <character name="quantity" src="d0_s7" unit="or" value="8" value_original="8" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading-erect" value_original="spreading-erect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as calyx" constraintid="o5112" from="1/2" name="quantity" src="d0_s7" upper_restricted="false" />
        <character is_modifier="false" modifier="slightly" name="size" notes="" src="d0_s7" value="accrescent" value_original="accrescent" />
        <character constraint="in fruit" constraintid="o5113" is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o5112" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
      <biological_entity id="o5113" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 15 mm, lobes erect, lanceolate-acuminate, 5–15 × 1–3 mm, 2 times as long as tube, pustular-hispid;</text>
      <biological_entity id="o5114" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5115" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="15" value_original="15" />
      </biological_entity>
      <biological_entity id="o5116" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character constraint="tube" constraintid="o5117" is_modifier="false" name="length" src="d0_s8" value="2 times as long as tube" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pustular-hispid" value_original="pustular-hispid" />
      </biological_entity>
      <biological_entity id="o5117" name="tube" name_original="tube" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>petals pinkish lilac, fading bluish, 12–20 mm, 1 1-1/2 times as long as calyx, apex entire or slightly notched;</text>
      <biological_entity id="o5118" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5119" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish lilac" value_original="pinkish lilac" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="fading bluish" value_original="fading bluish" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character constraint="calyx" constraintid="o5120" is_modifier="false" name="length" src="d0_s9" value="1-1-1/2 times as long as calyx" />
      </biological_entity>
      <biological_entity id="o5120" name="calyx" name_original="calyx" src="d0_s9" type="structure" />
      <biological_entity id="o5121" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminal column 0.5–1 mm, glabrous, sometimes sparsely glandular;</text>
      <biological_entity id="o5122" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o5123" name="column" name_original="column" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers in upper 1/2, pale-pink to almost white (yellow);</text>
      <biological_entity id="o5124" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5125" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="pale-pink to almost" value_original="pale-pink to almost" />
        <character is_modifier="false" modifier="almost" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5126" name="1/2" name_original="1/2" src="d0_s11" type="structure" />
      <relation from="o5125" id="r594" name="in" negation="false" src="d0_s11" to="o5126" />
    </statement>
    <statement id="d0_s12">
      <text>stigmas 10–15.</text>
      <biological_entity id="o5127" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5128" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits concealed by erect, accrescent calyx, 8–10 mm diam.;</text>
      <biological_entity id="o5129" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character constraint="by calyx" constraintid="o5130" is_modifier="false" name="prominence" src="d0_s13" value="concealed" value_original="concealed" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" notes="" src="d0_s13" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5130" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="true" name="size" src="d0_s13" value="accrescent" value_original="accrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mericarps 10–15, brown, unwinged, reniform, with strong, radiating ridges on lateral face, 3 mm, margins rounded, surface glabrous, obscurely ridged abaxially.</text>
      <biological_entity id="o5131" name="mericarp" name_original="mericarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="15" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="unwinged" value_original="unwinged" />
        <character is_modifier="false" name="shape" src="d0_s14" value="reniform" value_original="reniform" />
        <character name="some_measurement" notes="" src="d0_s14" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o5132" name="ridge" name_original="ridges" src="d0_s14" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s14" value="strong" value_original="strong" />
        <character is_modifier="true" name="arrangement" src="d0_s14" value="radiating" value_original="radiating" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5133" name="face" name_original="face" src="d0_s14" type="structure" />
      <biological_entity id="o5134" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o5135" name="surface" name_original="surface" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="obscurely; abaxially" name="shape" src="d0_s14" value="ridged" value_original="ridged" />
      </biological_entity>
      <relation from="o5131" id="r595" name="with" negation="false" src="d0_s14" to="o5132" />
      <relation from="o5132" id="r596" name="on" negation="false" src="d0_s14" to="o5133" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds brown, reniform-round, 1–1.3 × 0.7–1.4 (–2.5) mm, minutely rugulose, glabrous.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 42.</text>
      <biological_entity id="o5136" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="reniform-round" value_original="reniform-round" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s15" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s15" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5137" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; N.Y., Pa.; Europe; w, sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>There are very few recent North American collections of Althaea hirsuta. There is an old specimen of this plant from British Columbia dated 1924, but the species has not been collected in the province since then. Current molecular data suggest that it may belong within Malva.</discussion>
  <discussion>The name Althaea hispida Moench, which pertains here, is superfluous and illegitimate.</discussion>
  
</bio:treatment>