<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="illustration_page">417</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Link" date="unknown" rank="family">tamaricaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tamarix</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="unknown" rank="species">parviflora</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>3: 97. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family tamaricaceae;genus tamarix;species parviflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">250076731</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Small-flower tamarisk</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, to 5 m.</text>
      <biological_entity id="o5904" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade lanceolate, 2–2.5 mm.</text>
      <biological_entity id="o5906" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o5907" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 1.5–4 cm × 3–5 mm;</text>
      <biological_entity id="o5908" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bract exceeding pedicel, not reaching calyx tip.</text>
      <biological_entity id="o5909" name="bract" name_original="bract" src="d0_s3" type="structure" />
      <biological_entity id="o5910" name="pedicel" name_original="pedicel" src="d0_s3" type="structure" />
      <biological_entity constraint="calyx" id="o5911" name="tip" name_original="tip" src="d0_s3" type="structure" />
      <relation from="o5909" id="r665" name="exceeding" negation="false" src="d0_s3" to="o5910" />
      <relation from="o5909" id="r666" name="reaching" negation="true" src="d0_s3" to="o5911" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers 4-merous;</text>
      <biological_entity id="o5912" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-merous" value_original="4-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 1–1.5 mm, margins entire or denticulate;</text>
      <biological_entity id="o5913" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5914" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals oblong to ovate, 2 mm;</text>
      <biological_entity id="o5915" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="ovate" />
        <character name="some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>antisepalous stamens 4, filaments confluent with nectar disc lobes, all originating from edge of disc.</text>
      <biological_entity constraint="antisepalous" id="o5916" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="disc" id="o5918" name="lobe" name_original="lobes" src="d0_s7" type="structure" constraint_original="nectar disc" />
      <biological_entity id="o5919" name="edge" name_original="edge" src="d0_s7" type="structure" />
      <biological_entity id="o5920" name="disc" name_original="disc" src="d0_s7" type="structure" />
      <relation from="o5917" id="r667" name="originating from" negation="false" src="d0_s7" to="o5919" />
      <relation from="o5917" id="r668" name="part_of" negation="false" src="d0_s7" to="o5920" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 24.</text>
      <biological_entity id="o5917" name="filament" name_original="filaments" src="d0_s7" type="structure" constraint="disc" constraint_original="disc; disc">
        <character constraint="with nectar disc lobes" constraintid="o5918" is_modifier="false" name="arrangement" src="d0_s7" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5921" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Riverways, lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riverways" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ariz., Calif., Colo., Kans., Miss., Nev., N.Mex., N.C., Okla., Oreg., Tex., Utah, Wash.; s Europe; n Africa; introduced also in Mexico (Baja California), South America (Argentina), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico (Baja California)" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The name Tamarix tetrandra Pallas has been misapplied to T. parviflora.</discussion>
  
</bio:treatment>