<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">hibiscus</taxon_name>
    <taxon_name authority="S. Watson" date="1886" rank="species">biseptus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>21: 418. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus hibiscus;species biseptus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101054</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Arizona rose-mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, to 1 m, herbage sparsely simple and stellate-hairy throughout, few to many-armed stellate hairs 2–3-dimensionally radiate.</text>
      <biological_entity id="o10516" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o10517" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o10518" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="many-armed" value_original="many-armed" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="false" modifier="2-3-dimensionally" name="architecture_or_arrangement" src="d0_s0" value="radiate" value_original="radiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems also with a line of simple, fine, curved hairs decurrent from leaf base and extending from node to node.</text>
      <biological_entity id="o10519" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o10520" name="line" name_original="line" src="d0_s1" type="structure">
        <character constraint="from leaf base" constraintid="o10522" is_modifier="false" name="shape" src="d0_s1" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o10521" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" name="course" src="d0_s1" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o10522" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o10523" name="node" name_original="node" src="d0_s1" type="structure" />
      <biological_entity id="o10524" name="node" name_original="node" src="d0_s1" type="structure" />
      <relation from="o10519" id="r1149" name="with" negation="false" src="d0_s1" to="o10520" />
      <relation from="o10520" id="r1150" name="part_of" negation="false" src="d0_s1" to="o10521" />
      <relation from="o10519" id="r1151" name="extending from" negation="false" src="d0_s1" to="o10523" />
      <relation from="o10519" id="r1152" name="to" negation="false" src="d0_s1" to="o10524" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules narrowly triangular, (4–) 5–10 mm;</text>
      <biological_entity id="o10525" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10526" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1/2 to ± equaling blade, with fine curved hairs adaxially;</text>
      <biological_entity id="o10527" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10528" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="to blade" constraintid="o10529" name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o10529" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s3" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o10530" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="fine" value_original="fine" />
        <character is_modifier="true" name="course" src="d0_s3" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o10528" id="r1153" name="with" negation="false" src="d0_s3" to="o10530" />
    </statement>
    <statement id="d0_s4">
      <text>blade transversely to broadly ovate, 3 (–5) -lobed or parted, mostly 2–8.5 × 3–9 cm, base broadly cuneate to truncate or rounded or cordate, margins irregularly and sometimes doubly serrate or crenate-serrate, apex acute to short-acuminate, lobes narrowly elliptic to lanceolate, margins entire basally, coarsely serrate distally, surfaces scabridulous abaxially, less so adaxially, nectary present abaxially on midvein near base.</text>
      <biological_entity id="o10531" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10532" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="3(-5)-lobed" value_original="3(-5)-lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="parted" value_original="parted" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10533" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly cuneate" modifier="mostly" name="shape" src="d0_s4" to="truncate or rounded or cordate" />
      </biological_entity>
      <biological_entity id="o10534" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o10535" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="short-acuminate" />
      </biological_entity>
      <biological_entity id="o10536" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o10537" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="coarsely; distally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o10538" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o10539" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character constraint="on midvein" constraintid="o10540" is_modifier="false" modifier="less; adaxially" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10540" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o10541" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o10540" id="r1154" name="near" negation="false" src="d0_s4" to="o10541" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves.</text>
      <biological_entity id="o10542" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o10543" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o10544" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o10545" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o10543" id="r1155" name="in" negation="false" src="d0_s5" to="o10544" />
      <relation from="o10544" id="r1156" name="part_of" negation="false" src="d0_s5" to="o10545" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels obscurely jointed below apices, to 17.5 cm, usually exceeding subtending leaves, sometimes much elongated;</text>
      <biological_entity id="o10547" name="apex" name_original="apices" src="d0_s6" type="structure" />
      <biological_entity constraint="subtending" id="o10548" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o10546" id="r1157" modifier="usually" name="exceeding" negation="false" src="d0_s6" to="o10548" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets 8–12, linear-subulate, 1.5–2.5 cm, margins ciliate.</text>
      <biological_entity id="o10546" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character constraint="below apices" constraintid="o10547" is_modifier="false" modifier="obscurely" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s6" to="17.5" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes much" name="length" src="d0_s6" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o10549" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10550" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers horizontal to ascending;</text>
      <biological_entity id="o10551" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx neither accrescent nor inflated, it and involucel in fruit tinged basally with pink or purple, divided 4/5+ length, broadly campanulate, (1.4–) 2–2.8 cm, equaling or usually exceeding involucel, lobes narrowly lanceolate-triangular, margins ciliate, apices attenuate, nectaries absent;</text>
      <biological_entity id="o10552" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="accrescent" value_original="accrescent" />
        <character is_modifier="false" name="shape" src="d0_s9" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o10553" name="involucel" name_original="involucel" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="divided" value_original="divided" />
        <character char_type="range_value" from="4/5" name="length" src="d0_s9" upper_restricted="false" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="2.8" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s9" value="usually exceeding involucel" value_original="usually exceeding involucel" />
      </biological_entity>
      <biological_entity id="o10554" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="basally; basally; basally" name="coloration" src="d0_s9" value="tinged basally with pink or tinged basally with purple" />
      </biological_entity>
      <biological_entity id="o10555" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="lanceolate-triangular" value_original="lanceolate-triangular" />
      </biological_entity>
      <biological_entity id="o10556" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o10557" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o10558" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o10553" id="r1158" name="in" negation="false" src="d0_s9" to="o10554" />
    </statement>
    <statement id="d0_s10">
      <text>corolla rotate, petals cream [white], with maroon lines or spot basally, these sometimes faint or absent, asymmetrically obovate to broadly obovate, 3–4.5 × 1.8–2.6 cm, margins ± entire, sparingly hairy abaxially where exposed in bud;</text>
      <biological_entity id="o10559" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rotate" value_original="rotate" />
      </biological_entity>
      <biological_entity id="o10560" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="sometimes" name="prominence" notes="" src="d0_s10" value="faint" value_original="faint" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="asymmetrically obovate" name="shape" src="d0_s10" to="broadly obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s10" to="2.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10561" name="line" name_original="lines" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="maroon" value_original="maroon" />
      </biological_entity>
      <biological_entity id="o10562" name="spot" name_original="spot" src="d0_s10" type="structure" />
      <biological_entity id="o10563" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character constraint="in bud" constraintid="o10564" is_modifier="false" modifier="where exposed" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10564" name="bud" name_original="bud" src="d0_s10" type="structure" />
      <relation from="o10560" id="r1159" name="with" negation="false" src="d0_s10" to="o10561" />
      <relation from="o10560" id="r1160" name="with" negation="false" src="d0_s10" to="o10562" />
    </statement>
    <statement id="d0_s11">
      <text>staminal column straight, pale-yellow or maroon, 1.1–1.5 cm, bearing filaments throughout, free portion of filaments not secund, 1.5–2.5 mm;</text>
      <biological_entity constraint="staminal" id="o10565" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="maroon" value_original="maroon" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10566" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <biological_entity id="o10567" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="secund" value_original="secund" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10568" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <relation from="o10565" id="r1161" modifier="throughout" name="bearing" negation="false" src="d0_s11" to="o10566" />
      <relation from="o10567" id="r1162" name="part_of" negation="false" src="d0_s11" to="o10568" />
    </statement>
    <statement id="d0_s12">
      <text>pollen yellow to orange-red;</text>
      <biological_entity id="o10569" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s12" to="orange-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles white to maroon, 1–2 mm;</text>
      <biological_entity id="o10570" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="maroon" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas white to maroon.</text>
      <biological_entity id="o10571" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules dull yellow-green with darker medial stripe on each valve, ovoid to subglobose, 1–1.5 cm, much shorter than calyces, apex apiculate, glabrous.</text>
      <biological_entity id="o10572" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character constraint="with medial stripe" constraintid="o10573" is_modifier="false" name="coloration" src="d0_s15" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="ovoid" name="shape" notes="" src="d0_s15" to="subglobose" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s15" to="1.5" to_unit="cm" />
        <character constraint="than calyces" constraintid="o10575" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity constraint="medial" id="o10573" name="stripe" name_original="stripe" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s15" value="darker" value_original="darker" />
      </biological_entity>
      <biological_entity id="o10574" name="valve" name_original="valve" src="d0_s15" type="structure" />
      <biological_entity id="o10575" name="calyx" name_original="calyces" src="d0_s15" type="structure" />
      <biological_entity id="o10576" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o10573" id="r1163" name="on" negation="false" src="d0_s15" to="o10574" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, angulately reniform-ovoid, strongly depressed laterally, 2.5–3.5 mm, silky-hairy dorsally, glabrous laterally, with slightly raised pale-yellow zone near hilum.</text>
      <biological_entity id="o10578" name="zone" name_original="zone" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="slightly" name="prominence" src="d0_s16" value="raised" value_original="raised" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o10579" name="hilum" name_original="hilum" src="d0_s16" type="structure" />
      <relation from="o10577" id="r1164" name="with" negation="false" src="d0_s16" to="o10578" />
      <relation from="o10578" id="r1165" name="near" negation="false" src="d0_s16" to="o10579" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 22 (Mexico: Sinaloa).</text>
      <biological_entity id="o10577" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="angulately" name="shape" src="d0_s16" value="reniform-ovoid" value_original="reniform-ovoid" />
        <character is_modifier="false" modifier="strongly; laterally" name="shape" src="d0_s16" value="depressed" value_original="depressed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="pubescence" src="d0_s16" value="silky-hairy" value_original="silky-hairy" />
        <character is_modifier="false" modifier="laterally" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10580" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert scrub and grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert scrub" />
        <character name="habitat" value="grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Baja California, Chihuahua, Jalisco, Nayarit, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Jalisco)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nayarit)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hibiscus biseptus is known from the southern Arizona counties of Maricopa, Pima, Pinal, and Santa Cruz.</discussion>
  
</bio:treatment>