<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">hibiscus</taxon_name>
    <taxon_name authority="Allioni" date="unknown" rank="species">laevis</taxon_name>
    <place_of_publication>
      <publication_title>Auct. Syn. Meth. Stirp. Taurin.,</publication_title>
      <place_in_publication>31. 1773</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus hibiscus;species laevis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416655</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hibiscus</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">coccineus</taxon_name>
    <taxon_name authority="Hochreutiner" date="unknown" rank="variety">virginicus</taxon_name>
    <taxon_hierarchy>genus Hibiscus;species coccineus;variety virginicus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">militaris</taxon_name>
    <taxon_hierarchy>genus H.;species militaris</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Halberd-leaved or smooth rose-mallow</other_name>
  <other_name type="common_name">sweating-weed</other_name>
  <other_name type="common_name">military hibiscus</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, to 2.5 m, herbage glabrous or nearly so throughout.</text>
      <biological_entity id="o22935" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o22936" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s0" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often glaucous.</text>
      <biological_entity id="o22937" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules caducous, linear-subulate, 2–10 mm;</text>
      <biological_entity id="o22938" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22939" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1/2 to somewhat exceeding blade;</text>
      <biological_entity id="o22940" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22941" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o22942" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <relation from="o22941" id="r2428" modifier="somewhat" name="exceeding" negation="false" src="d0_s3" to="o22942" />
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly to broadly ovate or triangular or lanceolate-ovate, usually hastately 3 (–5) -lobed, sometimes unlobed, 6–18 × 3–16 cm, base cordate to truncate, lobes, especially middle one, ovate to triangular, to 3 times as long as wide, margins crenate-serrate to serrate or serrate-dentate, apex acuminate to long-acuminate, surfaces glabrous, nectary absent.</text>
      <biological_entity id="o22943" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o22944" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character is_modifier="false" modifier="usually hastately" name="shape" src="d0_s4" value="3(-5)-lobed" value_original="3(-5)-lobed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="18" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="16" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22945" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s4" to="truncate" />
      </biological_entity>
      <biological_entity id="o22946" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <biological_entity id="o22947" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="especially" name="position" src="d0_s4" value="middle" value_original="middle" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="triangular" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="0-3" value_original="0-3" />
      </biological_entity>
      <biological_entity id="o22948" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate-serrate" name="architecture_or_shape" src="d0_s4" to="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate-dentate" value_original="serrate-dentate" />
      </biological_entity>
      <biological_entity id="o22949" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="long-acuminate" />
      </biological_entity>
      <biological_entity id="o22950" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22951" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers in axils of distal leaves.</text>
      <biological_entity id="o22952" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o22953" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o22954" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o22955" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o22953" id="r2429" name="in" negation="false" src="d0_s5" to="o22954" />
      <relation from="o22954" id="r2430" name="part_of" negation="false" src="d0_s5" to="o22955" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels jointed distally, 1–10 cm, 1/3 to slightly exceeding subtending petioles;</text>
      <biological_entity constraint="subtending" id="o22957" name="petiole" name_original="petioles" src="d0_s6" type="structure" />
      <relation from="o22956" id="r2431" modifier="slightly" name="exceeding" negation="false" src="d0_s6" to="o22957" />
    </statement>
    <statement id="d0_s7">
      <text>involucellar bractlets (8 or) 9–15 (or 16), linear-subulate, 1–3 cm, margins not ciliate.</text>
      <biological_entity id="o22956" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s6" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character name="quantity" src="d0_s6" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o22958" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s7" to="15" />
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22959" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers horizontal;</text>
      <biological_entity id="o22960" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calyx divided 1/3–1/2 length, broadly cylindric-campanulate, 2.5–3 cm, conspicuously larger in fruit, lobes broadly triangular, apices acute, surfaces glabrous, nectaries absent;</text>
      <biological_entity id="o22961" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="divided" value_original="divided" />
        <character char_type="range_value" from="1/3" name="length" src="d0_s9" to="1/2" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="cylindric-campanulate" value_original="cylindric-campanulate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character constraint="in fruit" constraintid="o22962" is_modifier="false" modifier="conspicuously" name="size" src="d0_s9" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o22962" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o22963" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o22964" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o22965" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22966" name="nectary" name_original="nectaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla broadly funnelform, petals pink to white, red basally, obovate, 5–8 × 2–5 cm, apical margins entire to repand, finely hairy abaxially where exposed in bud;</text>
      <biological_entity id="o22967" name="corolla" name_original="corolla" src="d0_s10" type="structure" />
      <biological_entity id="o22968" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="white" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s10" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s10" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o22969" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s10" to="repand" />
        <character constraint="in bud" constraintid="o22970" is_modifier="false" modifier="where exposed" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22970" name="bud" name_original="bud" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>staminal column straight, pale-pink to white, 2.5–4 cm, ca. 1/2 as long as petals, bearing filaments nearly throughout, free portion of filaments not secund, 2–4 mm;</text>
      <biological_entity constraint="staminal" id="o22971" name="column" name_original="column" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s11" to="white" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character constraint="as-long-as petals" constraintid="o22972" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o22972" name="petal" name_original="petals" src="d0_s11" type="structure" />
      <biological_entity id="o22973" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <biological_entity id="o22974" name="portion" name_original="portion" src="d0_s11" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="secund" value_original="secund" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22975" name="filament" name_original="filaments" src="d0_s11" type="structure" />
      <relation from="o22971" id="r2432" modifier="nearly throughout; throughout" name="bearing" negation="false" src="d0_s11" to="o22973" />
      <relation from="o22974" id="r2433" name="part_of" negation="false" src="d0_s11" to="o22975" />
    </statement>
    <statement id="d0_s12">
      <text>pollen pale-pink to white;</text>
      <biological_entity id="o22976" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s12" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles pale-pink to white, 5–12 mm;</text>
      <biological_entity id="o22977" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale-pink" name="coloration" src="d0_s13" to="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigmas pink.</text>
      <biological_entity id="o22978" name="stigma" name_original="stigmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules brown, ovoid, 1.8–3 cm, apex truncate, apiculate, glabrous.</text>
      <biological_entity id="o22979" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s15" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22980" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds reddish-brown to brown, reniform-globose, 3–5 mm, hairy, hairs reddish.</text>
      <biological_entity id="o22981" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s16" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform-globose" value_original="reniform-globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>2n = 38.</text>
      <biological_entity id="o22982" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22983" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges of freshwater lakes, larger, slow-moving streams, floodplain pools, wet roadside ditches, artificial ponds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of freshwater" />
        <character name="habitat" value="freshwater" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="slow-moving streams" modifier="larger" />
        <character name="habitat" value="floodplain pools" />
        <character name="habitat" value="wet roadside ditches" />
        <character name="habitat" value="artificial ponds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mich., Minn., Miss., Mo., Nebr., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>B. P. G. Hochreutiner (1900) inexplicably merged Hibiscus laevis with the very different H. coccineus.</discussion>
  <discussion>Hibiscus laevis has been recorded from Pelee Island in southernmost Ontario, but the 1904 collection is thought to have represented a short-lived population, as the species apparently has not since been found there (R. L. Stuckey 1968b). A report from the Bronx, New York (R. DeCandido 1991), well northeast of its closest previously known occurrence, is likely of an introduction or an escape.</discussion>
  <discussion>There are reports of recent northward spread of Hibiscus laevis along larger streams (C. C. Deam 1940; M. L. Roberts and R. L. Stuckey 1992; Stuckey 1968b; F. H. Utech 1970). R. B. Kaul (pers. comm.) reported similar upriver increases in Nebraska on the Elkhorn, Missouri, and Platt rivers. Hibiscus laevis sometimes forms natural hybrids with H. moscheutos subsp. moscheutos, usually in man-made habitats (O. J. Blanchard 1976).</discussion>
  
</bio:treatment>