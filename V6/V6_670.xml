<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">365</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="A. St.-Hilaire in A. St.-Hilaire et al." date="1825" rank="genus">sphaeralcea</taxon_name>
    <taxon_name authority="Torrey ex A. Gray" date="1849" rank="species">incana</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 23. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sphaeralcea;species incana;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101188</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphaeralcea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">incana</taxon_name>
    <taxon_name authority="Kearney" date="unknown" rank="subspecies">cuneata</taxon_name>
    <taxon_hierarchy>genus Sphaeralcea;species incana;subspecies cuneata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">incana</taxon_name>
    <taxon_name authority="(Kearney) Kearney" date="unknown" rank="variety">cuneata</taxon_name>
    <taxon_hierarchy>genus S.;species incana;variety cuneata</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Gray globemallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o19214" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–5 (–7), erect, yellow to yellow-green, (4–) 6–18 (–30) dm, rubbery when fresh, sometimes appearing slightly ridged or fasciated when dry, densely canescent.</text>
      <biological_entity id="o19215" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s1" to="yellow-green" />
        <character char_type="range_value" from="4" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="6" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="30" to_unit="dm" />
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s1" to="18" to_unit="dm" />
        <character is_modifier="false" modifier="when fresh; sometimes; slightly" name="shape" src="d0_s1" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="when dry" name="fusion" src="d0_s1" value="fasciated" value_original="fasciated" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades light green to yellow-green, deltate or elongate-deltate, unlobed or weakly 3-lobed, center lobe sometimes elongate, lobes relatively broad, 3–5 (–7) cm, not rugose, rubbery, base cuneate or truncate to cordate, margins entire or crenulate to undulate, surfaces stellate-pubescent.</text>
      <biological_entity id="o19216" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s2" to="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate-deltate" value_original="elongate-deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate-deltate" value_original="elongate-deltate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="center" id="o19217" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o19218" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s2" value="broad" value_original="broad" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o19219" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s2" to="cordate" />
      </biological_entity>
      <biological_entity id="o19220" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="crenulate" name="shape" src="d0_s2" to="undulate" />
      </biological_entity>
      <biological_entity id="o19221" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences paniculate, crowded, flowers long-interrupted, tip not leafy;</text>
      <biological_entity id="o19222" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o19223" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="long-interrupted" value_original="long-interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>involucellar bractlets green to tan.</text>
      <biological_entity id="o19224" name="tip" name_original="tip" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o19225" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="tan" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 3.5–6.5 mm, spheric in bud;</text>
      <biological_entity id="o19226" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19227" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6.5" to_unit="mm" />
        <character constraint="in bud" constraintid="o19228" is_modifier="false" name="shape" src="d0_s5" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity id="o19228" name="bud" name_original="bud" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petals red-orange to pale-red, 10–17 mm;</text>
      <biological_entity id="o19229" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19230" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="red-orange" name="coloration" src="d0_s6" to="pale-red" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow.</text>
      <biological_entity id="o19231" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19232" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Schizocarps hemispheric;</text>
      <biological_entity id="o19233" name="schizocarp" name_original="schizocarps" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>mericarps 11–15, 4–5.5 × 2–2.5 mm, chartaceous, nonreticulate dehiscent part 60–75% of height, tip reflexed, indehiscent part not wider than dehiscent part, sides faintly to prominently and finely reticulate with thin-translucent areolae.</text>
      <biological_entity id="o19234" name="mericarp" name_original="mericarps" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s9" to="15" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o19235" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s9" value="dehiscent" value_original="dehiscent" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="orientation" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="width" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="with areolae" constraintid="o19244" is_modifier="false" modifier="60-75%; prominently; finely" name="architecture_or_coloration_or_relief" notes="" src="d0_s9" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o19240" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="orientation" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="width" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
      </biological_entity>
      <biological_entity id="o19242" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="orientation" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="width" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
      </biological_entity>
      <biological_entity id="o19243" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="orientation" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="width" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
        <character constraint="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" constraintid="o19236, o19238, o19239" is_modifier="false" name="dehiscence" src="d0_s9" value="of height , tip reflexed , indehiscent part not wider than dehiscent part , sides" />
      </biological_entity>
      <biological_entity id="o19236" name="tip" name_original="tip" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
      </biological_entity>
      <biological_entity id="o19238" name="part" name_original="part" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
      </biological_entity>
      <biological_entity id="o19239" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="height" value_original="height" />
      </biological_entity>
      <biological_entity id="o19244" name="areola" name_original="areolae" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s9" value="thin-translucent" value_original="thin-translucent" />
      </biological_entity>
      <relation from="o19235" id="r2024" modifier="60-75%" name="part_of" negation="false" src="d0_s9" to="o19240" />
      <relation from="o19235" id="r2025" modifier="60-75%" name="part_of" negation="false" src="d0_s9" to="o19242" />
      <relation from="o19235" id="r2026" modifier="60-75%" name="part_of" negation="false" src="d0_s9" to="o19243" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds 2 per mericarp, brown, glabrous or pubescent.</text>
      <biological_entity id="o19246" name="mericarp" name_original="mericarp" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 10, 20.</text>
      <biological_entity id="o19245" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character constraint="per mericarp" constraintid="o19246" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19247" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Coahuila, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sphaeralcea incana is often seen in relatively small groups. Fresh stems tend to be very rubbery and flower buds notably rounded.</discussion>
  
</bio:treatment>