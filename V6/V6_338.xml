<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Margaret M. Hanes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="mention_page">192</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Sterculioideae</taxon_name>
    <place_of_publication>
      <publication_title>Outlines Bot.,</publication_title>
      <place_in_publication>821, 1119. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily Sterculioideae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100992</other_info_on_name>
  </taxon_identification>
  <number>0</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees [shrubs].</text>
      <biological_entity id="o19533" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades unlobed or lobed, ultimate margins entire or serrate.</text>
      <biological_entity id="o19534" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o19535" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences axillary or terminal, paniculate [racemose].</text>
      <biological_entity id="o19536" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s2" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers usually functionally unisexual, rarely bisexual;</text>
      <biological_entity id="o19537" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually functionally" name="reproduction" src="d0_s3" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s3" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>epicalyx absent;</text>
      <biological_entity id="o19538" name="epicalyx" name_original="epicalyx" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals persistent or late-deciduous, (4–) 5 (–8), connate, petaloid, nectaries usually at base on adaxial surface, sometimes absent;</text>
      <biological_entity id="o19539" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s5" value="late-deciduous" value_original="late-deciduous" />
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petaloid" value_original="petaloid" />
      </biological_entity>
      <biological_entity id="o19540" name="nectary" name_original="nectaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19541" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o19542" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <relation from="o19540" id="r2068" name="at" negation="false" src="d0_s5" to="o19541" />
      <relation from="o19541" id="r2069" name="on" negation="false" src="d0_s5" to="o19542" />
    </statement>
    <statement id="d0_s6">
      <text>petals absent;</text>
      <biological_entity id="o19543" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>androgynophore present;</text>
      <biological_entity id="o19544" name="androgynophore" name_original="androgynophore" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens [4–] 10–30, basally connate;</text>
      <biological_entity id="o19545" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s8" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="30" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 2-thecate;</text>
      <biological_entity id="o19546" name="anther" name_original="anthers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>staminodes absent;</text>
      <biological_entity id="o19547" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>gynoecium apocarpous.</text>
      <biological_entity id="o19548" name="gynoecium" name_original="gynoecium" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="apocarpous" value_original="apocarpous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits follicles [nuts].</text>
      <biological_entity constraint="fruits" id="o19549" name="follicle" name_original="follicles" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1–22 [–144], glabrous or hairy.</text>
      <biological_entity id="o19550" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="22" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="144" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s13" to="22" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia, Pacific Islands, Australia; pantropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
        <character name="distribution" value="pantropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Sterculidae</other_name>
  <discussion>Genera ca. 12, species ca. 400 (2 genera, 2 species in the flora).</discussion>
  <discussion>Genera in Sterculioideae historically have been recognized as a morphological group (H. W. Schott and S. L. Endlicher 1832; A. L. Takhtajan 1997; P. Wilkie et al. 2006) due to the presence of mostly unisexual flowers with androgynophores and without petals. Individually, these elements are found throughout Malvaceae; their combination is exclusive to Sterculioideae. Generic relationships within Sterculioideae are complicated. Firmiana is closely related to Hildegardia Schott &amp; Endlicher and, as currently circumscribed, is not monophyletic (Wilkie et al.).</discussion>
  <references>
    <reference>Wilkie, P. et al. 2006. Phylogenetic relationships within the subfamily Sterculioideae (Malvaceae/Sterculiaceae-Sterculieae) using the chloroplast gene ndhF. Syst. Bot. 31: 160–170.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Follicles woody; seeds hairy.</description>
      <determination>1 Brachychiton</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Follicles chartaceous; seeds glabrous.</description>
      <determination>2 Firmiana</determination>
    </key_statement>
  </key>
</bio:treatment>