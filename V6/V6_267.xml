<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Kellogg" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(M. S. Baker &amp; J. C. Clausen) J. T. Howell" date="unknown" rank="variety">dimorpha</taxon_name>
    <place_of_publication>
      <publication_title>Mentzelia</publication_title>
      <place_in_publication>1: 8. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species purpurea;variety dimorpha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100951</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="M. S. Baker &amp; J. C. Clausen" date="unknown" rank="subspecies">dimorpha</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>10: 122, plates 4 [right lower center], 8, fig. 8. 1949</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Viola;species purpurea;subspecies dimorpha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">V.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="M. S. Baker &amp; J. C. Clausen" date="unknown" rank="subspecies">geophyta</taxon_name>
    <taxon_hierarchy>genus V.;species purpurea;subspecies geophyta</taxon_hierarchy>
  </taxon_identification>
  <number>51c.</number>
  <other_name type="common_name">Dimorphic mountain violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–25 cm.</text>
      <biological_entity id="o647" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading to erect, usually not buried, usually elongated by end of season, puberulent.</text>
      <biological_entity id="o648" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="usually not" name="location" src="d0_s1" value="buried" value_original="buried" />
        <character constraint="by end" constraintid="o649" is_modifier="false" modifier="usually" name="length" src="d0_s1" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o649" name="end" name_original="end" src="d0_s1" type="structure" />
      <biological_entity id="o650" name="season" name_original="season" src="d0_s1" type="structure" />
      <relation from="o649" id="r70" name="part_of" negation="false" src="d0_s1" to="o650" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal: 1–6;</text>
      <biological_entity id="o651" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o652" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1.8–8.5 cm, puberulent;</text>
      <biological_entity id="o653" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o654" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.8" from_unit="cm" name="some_measurement" src="d0_s3" to="8.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually purple-tinted abaxially, green adaxially, ± orbiculate to ovate, 0.8–3 × 0.5–2.2 cm, base cordate or truncate, margins irregularly dentate or crenate with 3–4 rounded and/or pointed teeth per side, apex obtuse to rounded, surfaces puberulent;</text>
      <biological_entity id="o655" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o656" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually; abaxially" name="coloration" src="d0_s4" value="purple-tinted" value_original="purple-tinted" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="less orbiculate" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o657" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o658" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character constraint="with teeth" constraintid="o659" is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o659" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="4" />
        <character is_modifier="true" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s4" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity id="o660" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o661" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o662" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o659" id="r71" name="per" negation="false" src="d0_s4" to="o660" />
    </statement>
    <statement id="d0_s5">
      <text>cauline: petiole 0.5–4.1 cm, puberulent;</text>
      <biological_entity constraint="cauline" id="o663" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o664" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="4.1" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade ovate to lanceolate to ± oblong, 1.2–2.7 × 0.7–1.9 cm, length 0.8–2.5 times width, base attenuate, oblique or not, subcordate, or truncate, margins entire, crenate, serrate, or repand-denticulate, apex acute to obtuse, surfaces canescent.</text>
      <biological_entity constraint="cauline" id="o665" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o666" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s6" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s6" to="1.9" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="0.8-2.5" value_original="0.8-2.5" />
      </biological_entity>
      <biological_entity id="o667" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="orientation_or_shape" src="d0_s6" value="oblique" value_original="oblique" />
        <character name="orientation_or_shape" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o668" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand-denticulate" value_original="repand-denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand-denticulate" value_original="repand-denticulate" />
      </biological_entity>
      <biological_entity id="o669" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity id="o670" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 4.6–6 cm, puberulent.</text>
      <biological_entity id="o671" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.6" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Lowest petals 6–11 mm.</text>
      <biological_entity constraint="lowest" id="o672" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 5–6.5 mm.</text>
      <biological_entity id="o673" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds dark-brown, 2.2–2.9 mm.</text>
      <biological_entity id="o674" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–July.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="July" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine, fir, cedar forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cedar forests" modifier="pine fir" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>