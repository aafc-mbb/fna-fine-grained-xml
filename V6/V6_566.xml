<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sida</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">abutilifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Sida no. 12. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sida;species abutilifolia;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101098</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sida</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">diffusa</taxon_name>
    <taxon_hierarchy>genus Sida;species diffusa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">filicaulis</taxon_name>
    <taxon_hierarchy>genus S.;species filicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">procumbens</taxon_name>
    <taxon_hierarchy>genus S.;species procumbens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="L’Héritier" date="unknown" rank="species">supina</taxon_name>
    <taxon_hierarchy>genus S.;species supina</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Creeping sida</other_name>
  <other_name type="common_name">hierba del buen día</other_name>
  <other_name type="common_name">axocatzín</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 0.3–0.6 (–1) m.</text>
      <biological_entity id="o13312" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="0.6" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems procumbent, stellate-hairy, hairs multirayed, usually also with simple 1–2 mm hairs.</text>
      <biological_entity id="o13313" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity id="o13314" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character constraint="with" is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13315" name="hair" name_original="hairs" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves distributed evenly along stems;</text>
      <biological_entity id="o13316" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="along stems" constraintid="o13317" is_modifier="false" name="arrangement" src="d0_s2" value="distributed" value_original="distributed" />
      </biological_entity>
      <biological_entity id="o13317" name="stem" name_original="stems" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>stipules inconspicuous, free from petiole, subulate, 1.5–3 mm;</text>
      <biological_entity id="o13318" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="inconspicuous" value_original="inconspicuous" />
        <character constraint="from petiole" constraintid="o13319" is_modifier="false" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13319" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–15 mm, 1/2 to equaling or exceeding blade, often with simple 1–2 mm hairs;</text>
      <biological_entity id="o13320" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="variability" src="d0_s4" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s4" value="exceeding blade" value_original="exceeding blade" />
        <character constraint="with" is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13321" name="hair" name_original="hairs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to oblong, to 1.5+ cm, 1.5–3 times longer than wide, base cordate, margins crenate to base, apex obtuse to acute, surfaces hairy.</text>
      <biological_entity id="o13322" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1.5-3" value_original="1.5-3" />
      </biological_entity>
      <biological_entity id="o13323" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o13324" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character constraint="to base" constraintid="o13325" is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o13325" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o13326" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o13327" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary solitary flowers.</text>
      <biological_entity id="o13328" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="axillary" id="o13329" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels slender, 1–2.5 cm, 2–5 times as long as calyx.</text>
      <biological_entity id="o13330" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character constraint="calyx" constraintid="o13331" is_modifier="false" name="length" src="d0_s7" value="2-5 times as long as calyx" />
      </biological_entity>
      <biological_entity id="o13331" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx angulate, 4–5 (–7) mm, hirsute, lobes ovate-acuminate;</text>
      <biological_entity id="o13332" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o13333" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="angulate" value_original="angulate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o13334" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate-acuminate" value_original="ovate-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white, 5–6 (–10) mm;</text>
      <biological_entity id="o13335" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o13336" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminal column puberulent;</text>
      <biological_entity id="o13337" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="staminal" id="o13338" name="column" name_original="column" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 5-branched.</text>
      <biological_entity id="o13339" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13340" name="style" name_original="style" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-branched" value_original="5-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Schizocarps conic, 4 mm diam., hairy;</text>
      <biological_entity id="o13341" name="schizocarp" name_original="schizocarps" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character name="diameter" src="d0_s12" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mericarps 5, 2–3 mm, basal portion slightly rugose, apex spined, spines 0.1–0.5 mm, antrorsely hairy.</text>
      <biological_entity id="o13342" name="mericarp" name_original="mericarps" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13343" name="portion" name_original="portion" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o13344" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 14</text>
      <biological_entity id="o13345" name="spine" name_original="spines" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13346" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, arid areas, disturbed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="arid areas" />
        <character name="habitat" value="disturbed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Fla., N.Mex., Okla., Tex.; Mexico; West Indies; Central America (Costa Rica, Guatemala); South America (Brazil, Colombia, Ecuador, French Guiana, Guyana, Peru, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America (Costa Rica)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="South America (Brazil)" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="South America (French Guiana)" establishment_means="native" />
        <character name="distribution" value="South America (Guyana)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">abutifolia</other_name>
  <discussion>Sida abutilifolia is apparently native from the southern United States to northern South America. Within the flora area, the procumbent-prostrate even mat-forming habit with freely branched, long, flexible stems is quite distinctive.</discussion>
  
</bio:treatment>