<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">253</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="illustration_page">257</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">hibiscus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">grandiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 46. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus hibiscus;species grandiflorus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101058</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hibiscus</taxon_name>
    <taxon_name authority="Helwig" date="unknown" rank="species">urbanii</taxon_name>
    <taxon_hierarchy>genus Hibiscus;species urbanii</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Swamp rose-mallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, to 3 m.</text>
      <biological_entity id="o18803" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous or rarely stellate-hairy on younger parts, without line of minute, curved length 1/2–1 times petioles, glabrate or finely hairy;</text>
      <biological_entity id="o18805" name="part" name_original="parts" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="younger" value_original="younger" />
      </biological_entity>
      <biological_entity id="o18806" name="line" name_original="line" src="d0_s1" type="structure" />
      <biological_entity id="o18807" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character is_modifier="true" name="length" src="d0_s1" value="minute" value_original="minute" />
        <character is_modifier="true" name="course" src="d0_s1" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o18804" id="r1982" name="without" negation="false" src="d0_s1" to="o18806" />
      <relation from="o18806" id="r1983" name="part_of" negation="false" src="d0_s1" to="o18807" />
    </statement>
    <statement id="d0_s2">
      <text>involucellar bractlets 9–13, linear-subulate, 1.3–2.7 cm, margins not ciliate, velvety-hairy.</text>
      <biological_entity id="o18804" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character constraint="on parts" constraintid="o18805" is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18808" name="bractlet" name_original="bractlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s2" to="13" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-subulate" value_original="linear-subulate" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s2" to="2.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18809" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="velvety-hairy" value_original="velvety-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers horizontal or ascending;</text>
      <biological_entity id="o18810" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>calyx divided to ± middle, broadly campanulate, 2.9–6 cm, larger in fruit, lobes triangular, apices acute to subcaudate, velvety-hairy, nectaries absent;</text>
      <biological_entity id="o18811" name="calyx" name_original="calyx" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s4" value="middle" value_original="middle" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.9" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character constraint="in fruit" constraintid="o18812" is_modifier="false" name="size" src="d0_s4" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o18812" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
      <biological_entity id="o18813" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o18814" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="subcaudate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="velvety-hairy" value_original="velvety-hairy" />
      </biological_entity>
      <biological_entity id="o18815" name="nectary" name_original="nectaries" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>corolla broadly to narrowly funnelform, petals pale-pink to white, red basally, narrowly obovate, usually not conspicuously overlapping, 8.5–14 × 4–8.5 cm, apical margins repand, finely hairy abaxially where exposed in bud;</text>
      <biological_entity id="o18816" name="corolla" name_original="corolla" src="d0_s5" type="structure" />
      <biological_entity id="o18817" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="pale-pink" modifier="narrowly" name="coloration" src="d0_s5" to="white" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="usually not conspicuously" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="8.5" from_unit="cm" name="length" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s5" to="8.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o18818" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character constraint="in bud" constraintid="o18819" is_modifier="false" modifier="where exposed" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18819" name="bud" name_original="bud" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>staminal column straight, pink to white, 6.2–9.5 cm, length 2/3 petals, bearing filaments throughout its length, free portion of filaments secund, 3–9 mm;</text>
      <biological_entity constraint="staminal" id="o18820" name="column" name_original="column" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s6" to="white" />
        <character char_type="range_value" from="6.2" from_unit="cm" name="some_measurement" src="d0_s6" to="9.5" to_unit="cm" />
        <character name="quantity" src="d0_s6" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o18821" name="petal" name_original="petals" src="d0_s6" type="structure" />
      <biological_entity id="o18822" name="filament" name_original="filaments" src="d0_s6" type="structure" />
      <biological_entity id="o18823" name="portion" name_original="portion" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="false" name="length" src="d0_s6" value="secund" value_original="secund" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18824" name="filament" name_original="filaments" src="d0_s6" type="structure" />
      <relation from="o18820" id="r1984" name="bearing" negation="false" src="d0_s6" to="o18822" />
      <relation from="o18823" id="r1985" name="part_of" negation="false" src="d0_s6" to="o18824" />
    </statement>
    <statement id="d0_s7">
      <text>pollen yellow;</text>
      <biological_entity id="o18825" name="pollen" name_original="pollen" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles white, 7–17 mm;</text>
      <biological_entity id="o18826" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas yellow.</text>
      <biological_entity id="o18827" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules light to dark-brown, ovoid to subglobose, 2.2–3.5 cm, apex apiculate, hispid with simple, yellowish-brown to reddish-brown hairs.</text>
      <biological_entity id="o18828" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s10" to="dark-brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="subglobose" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="some_measurement" src="d0_s10" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18829" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="apiculate" value_original="apiculate" />
        <character constraint="with hairs" constraintid="o18830" is_modifier="false" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o18830" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character char_type="range_value" from="yellowish-brown" is_modifier="true" name="coloration" src="d0_s10" to="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds brown to reddish-brown, reniform-globose, 2.8–3.1 mm, verrucose-papillose.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 38.</text>
      <biological_entity id="o18831" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s11" to="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="reniform-globose" value_original="reniform-globose" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="verrucose-papillose" value_original="verrucose-papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18832" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Freshwater and brackish marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brackish" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., S.C., Tex.; West Indies (w Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies (w Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The flowers of Hibiscus grandiflorus first open in the evening, emit a pleasant fragrance, and are pollinated by sphingid moths (O. J. Blanchard 1976). It is relatively common only in Florida. Hibiscus grandiflorus is sometimes cultivated and has been found to be hardy as far north as Illinois (S. R. Hill, pers. comm.).</discussion>
  
</bio:treatment>