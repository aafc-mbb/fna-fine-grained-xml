<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">198</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Dippel" date="unknown" rank="subfamily">Grewioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">corchorus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">tridens</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>2: 566. 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily grewioideae;genus corchorus;species tridens;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242421563</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Horn-fruited jute</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants herbs, annual.</text>
      <biological_entity constraint="plants" id="o1211" name="herb" name_original="herbs" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, 3–6 dm, mostly glabrous.</text>
      <biological_entity id="o1212" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 5–15 (–25) mm;</text>
      <biological_entity id="o1213" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1214" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong-lanceolate to elliptic-obovate, linearlanceolate, or narrowly elliptic, 1.5–9 (–12) cm, base rounded, margins crenate to serrate, each of proximal pair of teeth usually prolonged into caudate-setaceous point 3–5 mm, apex acute to acuminate, surfaces glabrous or sparsely hirsute on veins.</text>
      <biological_entity id="o1215" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1216" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s3" to="elliptic-obovate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1217" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o1218" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s3" to="serrate" />
        <character constraint="into point" constraintid="o1221" is_modifier="false" modifier="usually" name="length" src="d0_s3" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1219" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1220" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o1221" name="point" name_original="point" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="caudate-setaceous" value_original="caudate-setaceous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1222" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o1223" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="on veins" constraintid="o1224" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o1224" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <relation from="o1218" id="r131" name="part_of" negation="false" src="d0_s3" to="o1219" />
      <relation from="o1218" id="r132" name="part_of" negation="false" src="d0_s3" to="o1220" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences solitary flowers or fasciculate or cymose, 2 or 3 (or 4) -flowered.</text>
      <biological_entity id="o1225" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o1226" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s4" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="fasciculate" value_original="fasciculate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-flowered" value_original="3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0–1 mm.</text>
      <biological_entity id="o1227" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals linear-oblong to narrowly elliptic or narrowly obovate, 3–5 mm, not awned, glabrous;</text>
      <biological_entity id="o1228" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o1229" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s6" to="narrowly elliptic or narrowly obovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s6" value="awned" value_original="awned" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals 3–5 mm;</text>
      <biological_entity id="o1230" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1231" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 10–15 (–20).</text>
      <biological_entity id="o1232" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1233" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="20" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules cylindric, terete, not wing-angled, 3-valved, each valve terminated by bifurcate awn 1.5–2 mm, 20–40 × 1.5–2 mm, glabrous.</text>
      <biological_entity id="o1234" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="wing-angled" value_original="wing-angled" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-valved" value_original="3-valved" />
      </biological_entity>
      <biological_entity id="o1236" name="awn" name_original="awn" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="bifurcate" value_original="bifurcate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o1235" id="r133" name="terminated by" negation="false" src="d0_s9" to="o1236" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 14.</text>
      <biological_entity id="o1235" name="valve" name_original="valve" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" notes="" src="d0_s9" to="40" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1237" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.J., Pa.; s Asia (India, Pakistan); introduced also in Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="s Asia (India)" establishment_means="native" />
        <character name="distribution" value="s Asia (Pakistan)" establishment_means="native" />
        <character name="distribution" value="also in Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Corchorus tridens is included for New Jersey and Pennsylvania based on documentation posted on the PLANTS website. No information is available on the abundance, but presumably it is rare or a waif, as other accounts do not include the species as naturalized in the flora area.</discussion>
  
</bio:treatment>