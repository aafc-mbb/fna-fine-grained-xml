<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Janice G. Saunders</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Byttnerioideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HERMANNIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 673. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 304. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily byttnerioideae;genus hermannia;</taxon_hierarchy>
    <other_info_on_name type="etymology">For Paul Hermann, 1646 – 1695, German-born Dutch botanist and explorer</other_info_on_name>
    <other_info_on_name type="fna_id">115164</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Burstworts</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, [shrubs], prostrate to erect;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o23497" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems hairy, hairs usually stellate, sometimes intermixed with capitate-glandular and subsessile glandular-hairs.</text>
      <biological_entity id="o23498" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23499" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity constraint="capitate-glandular" id="o23500" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <relation from="o23499" id="r2478" modifier="sometimes" name="intermixed with" negation="false" src="d0_s2" to="o23500" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves petiolate;</text>
      <biological_entity id="o23501" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules deciduous, foliaceous, narrowly dimidiate-lanceolate or narrowly dimidiate-ovate, triangular, margins simple-bristled;</text>
      <biological_entity id="o23502" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="dimidiate-lanceolate" value_original="dimidiate-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="dimidiate-ovate" value_original="dimidiate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o23503" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="simple-bristled" value_original="simple-bristled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade usually unlobed, rarely lobed, margins dentate or staminodes 0;</text>
      <biological_entity id="o23504" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o23505" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o23506" name="staminode" name_original="staminodes" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments ligulate, very compressed, adnate to petal base and gynophore or ovary base, distally free, incurved, not abruptly dilated, expanded region narrowly oblong from base to above anther base, apex acuminate or acute, glabrous;</text>
      <biological_entity id="o23507" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" modifier="very" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
        <character constraint="to petal base" constraintid="o23508" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="distally" name="fusion" notes="" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="not abruptly" name="shape" src="d0_s6" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity constraint="petal" id="o23508" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23509" name="gynophore" name_original="gynophore" src="d0_s6" type="structure" />
      <biological_entity constraint="ovary" id="o23510" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23511" name="region" name_original="region" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="expanded" value_original="expanded" />
        <character constraint="from base" constraintid="o23512" is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o23512" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="anther" id="o23513" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o23514" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o23512" id="r2479" name="to" negation="false" src="d0_s6" to="o23513" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 2-thecate, lanceolate, [1–] 2–3.5 [–10] mm, inflexed, connivent to style, longitudinally dehiscent;</text>
      <biological_entity id="o23515" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="inflexed" value_original="inflexed" />
        <character constraint="to style" constraintid="o23516" is_modifier="false" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
        <character is_modifier="false" modifier="longitudinally" name="dehiscence" notes="" src="d0_s7" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o23516" name="style" name_original="style" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>thecae with rim ciliate from simple hairs, apex acuminate, slightly twisted, gland at apex only (H. texana) or also at theca base (H. pauciflora);</text>
      <biological_entity id="o23517" name="theca" name_original="thecae" src="d0_s8" type="structure" />
      <biological_entity id="o23518" name="rim" name_original="rim" src="d0_s8" type="structure">
        <character constraint="from hairs" constraintid="o23519" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o23519" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o23520" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o23521" name="gland" name_original="gland" src="d0_s8" type="structure" />
      <biological_entity id="o23522" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity constraint="theca" id="o23523" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o23517" id="r2480" name="with" negation="false" src="d0_s8" to="o23518" />
      <relation from="o23521" id="r2481" name="at" negation="false" src="d0_s8" to="o23522" />
      <relation from="o23522" id="r2482" name="at" negation="false" src="d0_s8" to="o23523" />
    </statement>
    <statement id="d0_s9">
      <text>gynoecium syncarpous, 5-carpellate, stipitate, 5-angled, locules opposite sepals;</text>
      <biological_entity id="o23524" name="gynoecium" name_original="gynoecium" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="syncarpous" value_original="syncarpous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-carpellate" value_original="5-carpellate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="5-angled" value_original="5-angled" />
      </biological_entity>
      <biological_entity id="o23525" name="locule" name_original="locules" src="d0_s9" type="structure" />
      <biological_entity id="o23526" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary 5-locular;</text>
      <biological_entity id="o23527" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s10" value="5-locular" value_original="5-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 4–14 per locule, ascending or horizontal, anatropous or amphitropous;</text>
      <biological_entity id="o23528" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o23529" from="4" name="quantity" src="d0_s11" to="14" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="amphitropous" value_original="amphitropous" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="amphitropous" value_original="amphitropous" />
      </biological_entity>
      <biological_entity id="o23529" name="locule" name_original="locule" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>styles persistent, 5, shortly exserted, presumably connate at anthesis (connate, distinct, or partially distinct dried), filiform;</text>
      <biological_entity id="o23530" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="shortly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
        <character constraint="at anthesis" is_modifier="false" modifier="presumably" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas inconspicuous, terete and 1-dentate (acute) or filiform and often few-minutely papillate at apex, rarely truncate, inconspicuous.</text>
      <biological_entity id="o23531" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s13" value="1-dentate" value_original="1-dentate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="filiform" value_original="filiform" />
        <character constraint="at apex" constraintid="o23532" is_modifier="false" modifier="often few-minutely" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" notes="" src="d0_s13" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o23532" name="apex" name_original="apex" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits capsules, 5-locular, in apical view 5-angled, 5-lobed, parted between angles, in lateral view emarginate at apex, margins curved, stipitate, valve margins dark-rimmed, dentate, teeth terminated by hairy tubercles not elsewhere on fruit (H. pauciflora) or hairy processes (H. texana), denser on valves, stellate-pubescent.</text>
      <biological_entity constraint="fruits" id="o23533" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" modifier="in-apical-view" name="shape" src="d0_s14" value="5-angled" value_original="5-angled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="5-lobed" value_original="5-lobed" />
        <character constraint="between angles" constraintid="o23534" is_modifier="false" name="shape" src="d0_s14" value="parted" value_original="parted" />
        <character constraint="at apex" constraintid="o23535" is_modifier="false" modifier="in-lateral-view" name="architecture_or_shape" notes="" src="d0_s14" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o23534" name="angle" name_original="angles" src="d0_s14" type="structure" />
      <biological_entity id="o23535" name="apex" name_original="apex" src="d0_s14" type="structure" />
      <biological_entity id="o23536" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="stipitate" value_original="stipitate" />
      </biological_entity>
      <biological_entity constraint="valve" id="o23537" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="dark-rimmed" value_original="dark-rimmed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o23538" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character constraint="on valves" constraintid="o23542" is_modifier="false" name="density" notes="" src="d0_s14" value="denser" value_original="denser" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
      <biological_entity id="o23539" name="tubercle" name_original="tubercles" src="d0_s14" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23540" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
      <biological_entity id="o23541" name="process" name_original="processes" src="d0_s14" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23542" name="valve" name_original="valves" src="d0_s14" type="structure" />
      <relation from="o23538" id="r2483" name="terminated by" negation="false" src="d0_s14" to="o23539" />
      <relation from="o23539" id="r2484" name="on" negation="false" src="d0_s14" to="o23540" />
      <relation from="o23539" id="r2485" name="on" negation="false" src="d0_s14" to="o23541" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0–8 per locule, brown, crescentiform-reniform, chalazal end wider, other end acute, large-pitted;</text>
      <biological_entity id="o23543" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per locule" constraintid="o23544" from="0" name="quantity" src="d0_s15" to="8" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="crescentiform-reniform" value_original="crescentiform-reniform" />
      </biological_entity>
      <biological_entity id="o23544" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity constraint="chalazal" id="o23545" name="end" name_original="end" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o23546" name="end" name_original="end" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
        <character is_modifier="false" name="relief" src="d0_s15" value="large-pitted" value_original="large-pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>elaiosome conspicuous, white;</text>
      <biological_entity id="o23547" name="elaiosome" name_original="elaiosome" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endosperm present;</text>
      <biological_entity id="o23548" name="endosperm" name_original="endosperm" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>embryo curved, chlorophyllous;</text>
      <biological_entity id="o23549" name="embryo" name_original="embryo" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="curved" value_original="curved" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="chlorophyllous" value_original="chlorophyllous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cotyledons flat, narrowly elliptic or oblongelliptic.</text>
    </statement>
    <statement id="d0_s20">
      <text>x = 6.</text>
      <biological_entity id="o23550" name="cotyledon" name_original="cotyledons" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s19" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblongelliptic" value_original="oblongelliptic" />
      </biological_entity>
      <biological_entity constraint="x" id="o23551" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw, sc United States, Mexico, Central America (Guatemala), w Asia (Saudi Arabia), Africa, Australia; subtropical and tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="sc United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="w Asia (Saudi Arabia)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="subtropical and tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 180 (2 in the flora area).</discussion>
  <discussion>Hermannia is primarily southern African, with other species found scattered outside Africa, notably former Gilesia biniflora F. Mueller from Australia, distinguished by totally free filaments. Only four species are exclusively from the Americas, found in Mexico and across its national borders. One species extends north to Arizona, and one extends north to Texas. Two are found</discussion>
  <discussion>outside the flora area: H. palmeri Rose from rocky granitic hillsides, sandy mesas, and coastal dunes of Baja California Sur and H. inflata Link &amp; Otto from tropical dry deciduous forests in mountains of southern Mexico and northern Guatemala.</discussion>
  <discussion>I. C. Verdoorn (1980) placed the only two fringed-capsuled species in southern Africa in subg. Hermannia, the basal subgenus, and she related them to the two American species that are fringed-capsuled: H. texana (Texas and Mexico) and H. palmeri (Baja California Sur). Both species from the flora area would lie also within subg. Hermannia in her key to taxa by possession of “narrowly oblong stamen filaments with the expanded portion of the filaments overlapping the anther bases.”</discussion>
  <discussion>The coherence of styles and connivance of anthers at anthesis (M. Jenny 1985, 1988), nectaries (S. Vogel 2000), and ovule/seed number per carpel need further study in the field to investigate possible cryptic floral differentiation between plants of Hermannia that would indicate the initial stages of the well-developed floral and pollen distylous dimorphisms of Melochia and Waltheria.</discussion>
  <references>
    <reference>Rose, J. N. 1897. A synopsis of American species of Hermannia. Contr. U.S. Natl. Herb. 5: 130–131.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 3.9–5.8 mm; leaf blades olive, with terminal trichome at each tooth apex never simple, rather stellate or stellate-bristled and slightly exserted from stellate trichomes of tooth sides; distal petal lamina densely capitate-glandular abaxially; capsules: endocarp corneous, rigid, valve margins fringed, processes filiform, flexible, 0.9–3.2 mm; trichome rays of terminal trichome of fringe process with trichome rays ascending, fine, and only slightly firmer or slightly longer than trichome rays of hairs below process apex; seeds: elaiosome narrowly transversely rhombic.</description>
      <determination>1 Hermannia texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Styles 1.7–3 mm; leaf blades red to red-rimmed, with terminal trichome at each tooth apex with 1 simple thin seta or seta 2-rayed exserted to 2 times length of stellate trichomes of tooth sides; distal petal lamina subglabrous abaxially; capsules: endocarp chartaceous, slightly sclerified, flexible, valve margins tuberculate, tubercles rigid, 0.2–1.8 mm; terminal trichome of tubercle apex with trichome rays usually planar (spreading); seeds: elaiosome narrowly transversely elliptic.</description>
      <determination>2 Hermannia pauciflora</determination>
    </key_statement>
  </key>
</bio:treatment>