<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">160</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Batsch" date="unknown" rank="family">violaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">viola</taxon_name>
    <taxon_name authority="Torrey in War Department [U.S.]" date="unknown" rank="species">sheltonii</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 67, plate 2. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species sheltonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100965</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Viola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sheltonii</taxon_name>
    <taxon_name authority="(Greene) A. Nelson" date="unknown" rank="variety">biternata</taxon_name>
    <taxon_hierarchy>genus Viola;species sheltonii;variety biternata</taxon_hierarchy>
  </taxon_identification>
  <number>61.</number>
  <other_name type="common_name">Shelton’s violet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, caulescent, not stoloniferous, 3–27 cm.</text>
      <biological_entity id="o13560" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="27" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3, prostrate, decumbent, or erect, glabrous or sparsely puberulent, from short, often vertical, deep-seated or usually shallow, subligneous rhizome.</text>
      <biological_entity id="o13561" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="3" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o13562" name="rhizome" name_original="rhizome" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="true" name="location" src="d0_s1" value="deep-seated" value_original="deep-seated" />
        <character is_modifier="true" modifier="usually" name="depth" src="d0_s1" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="texture" src="d0_s1" value="subligneous" value_original="subligneous" />
      </biological_entity>
      <relation from="o13561" id="r1487" name="from" negation="false" src="d0_s1" to="o13562" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o13563" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: 1–3, palmately compound, leaflets 3;</text>
      <biological_entity constraint="basal" id="o13564" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s3" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity id="o13565" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules lanceolate-ovate, margins laciniate with gland-tipped projections, apex acute to acuminate;</text>
      <biological_entity constraint="basal" id="o13566" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13567" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate-ovate" value_original="lanceolate-ovate" />
      </biological_entity>
      <biological_entity id="o13568" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="with projections" constraintid="o13569" is_modifier="false" name="shape" src="d0_s4" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <biological_entity id="o13569" name="projection" name_original="projections" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o13570" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 8.6–21 cm, glabrous or sparsely puberulent;</text>
      <biological_entity constraint="basal" id="o13571" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o13572" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="8.6" from_unit="cm" name="some_measurement" src="d0_s5" to="21" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade reniform or ovate to ± orbiculate, 2–7 × 2–11 cm, coriaceous, base tapered, each leaflet cleft or dissected into 3 ± obovate lobes, each lobe further divided into 2–3 oblanceolate, pandurate, spatulate, oblong, lanceolate, or elliptic, lobes 2–10 mm wide, margins entire, ciliate or eciliate, apex acute to obtuse, mucronulate, surfaces glabrous or sparsely puberulent;</text>
      <biological_entity constraint="basal" id="o13573" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o13574" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="more or less orbiculate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="11" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o13575" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13576" name="leaflet" name_original="leaflet" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cleft" value_original="cleft" />
        <character constraint="into lobes" constraintid="o13577" is_modifier="false" name="shape" src="d0_s6" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity id="o13577" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="3" value_original="3" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o13578" name="lobe" name_original="lobe" src="d0_s6" type="structure">
        <character constraint="into 2-3 oblanceolate , pandurate , spatulate , oblong" is_modifier="false" modifier="further" name="shape" src="d0_s6" value="divided" value_original="divided" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o13579" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13580" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o13581" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o13582" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal except: stipules ovate to lanceolate, margin projections gland-tipped or eglandular, apex long-acuminate;</text>
      <biological_entity constraint="cauline" id="o13583" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o13584" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o13585" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="margin" id="o13586" name="projection" name_original="projections" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o13587" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <relation from="o13583" id="r1488" name="to" negation="false" src="d0_s7" to="o13584" />
      <relation from="o13584" id="r1489" name="except" negation="false" src="d0_s7" to="o13585" />
    </statement>
    <statement id="d0_s8">
      <text>petiole 5.5–12 cm;</text>
      <biological_entity constraint="cauline" id="o13588" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o13589" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o13590" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s8" to="12" to_unit="cm" />
      </biological_entity>
      <relation from="o13588" id="r1490" name="to" negation="false" src="d0_s8" to="o13589" />
      <relation from="o13589" id="r1491" name="except" negation="false" src="d0_s8" to="o13590" />
    </statement>
    <statement id="d0_s9">
      <text>blade 1.2–6.3 × 1.2–10.5 cm.</text>
      <biological_entity constraint="cauline" id="o13591" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o13592" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o13593" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s9" to="6.3" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="width" src="d0_s9" to="10.5" to_unit="cm" />
      </biological_entity>
      <relation from="o13591" id="r1492" name="to" negation="false" src="d0_s9" to="o13592" />
      <relation from="o13592" id="r1493" name="except" negation="false" src="d0_s9" to="o13593" />
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 5–19 cm, glabrous or sparsely puberulent.</text>
      <biological_entity id="o13594" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="19" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals lanceolate, margins ciliate or eciliate, auricles 0.5–1 mm;</text>
      <biological_entity id="o13595" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13596" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o13597" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="eciliate" value_original="eciliate" />
      </biological_entity>
      <biological_entity id="o13598" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals deep lemon-yellow adaxially, upper 2 dark-brown to brownish purple abaxially, lower 3 and sometimes upper 2 brownish purple-veined, lateral 2 bearded or beardless, lowest 7–18 mm, spur yellowish with brownish purple specks, gibbous, 1–2 mm;</text>
      <biological_entity id="o13599" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o13600" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s12" value="lemon-yellow" value_original="lemon-yellow" />
        <character is_modifier="false" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="dark-brown" modifier="abaxially" name="coloration" src="d0_s12" to="brownish purple" />
        <character is_modifier="false" name="position" src="d0_s12" value="lower" value_original="lower" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s12" value="upper" value_original="upper" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brownish purple-veined" value_original="brownish purple-veined" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="beardless" value_original="beardless" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13601" name="spur" name_original="spur" src="d0_s12" type="structure">
        <character constraint="with specks" constraintid="o13602" is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="gibbous" value_original="gibbous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13602" name="speck" name_original="specks" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="brownish purple" value_original="brownish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style head bearded;</text>
      <biological_entity id="o13603" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="style" id="o13604" name="head" name_original="head" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>cleistogamous flowers axillary.</text>
      <biological_entity id="o13605" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o13606" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="cleistogamous" value_original="cleistogamous" />
        <character is_modifier="false" name="position" src="d0_s14" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules oblong to ovoid, 6–8 mm, glabrous or puberulent.</text>
      <biological_entity id="o13607" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="ovoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brownish, shiny, ca. 2.5 mm. 2n = 12.</text>
      <biological_entity id="o13608" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s16" value="shiny" value_original="shiny" />
        <character name="some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13609" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Red fir, yellow pine, mixed evergreen, chaparral, oak woodlands, rich or gravelly soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="red fir" />
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="mixed evergreen" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak woodlands" />
        <character name="habitat" value="rich" />
        <character name="habitat" value="gravelly soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Colo., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The cleistogamous flowers of Viola sheltonii are borne on long, prostrate peduncles usually buried in duff around the plant. Mature cleistogamous capsules are usually hidden and the dehisced seeds remain close to the parent plant. Some populations of V. sheltonii produce only cleistogamous flowers (D. Klaber 1976).</discussion>
  
</bio:treatment>