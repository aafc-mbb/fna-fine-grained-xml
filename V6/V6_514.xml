<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">malvaceae</taxon_name>
    <taxon_name authority="Burnett" date="unknown" rank="subfamily">Malvoideae</taxon_name>
    <taxon_name authority="Greene" date="1906" rank="genus">malacothamnus</taxon_name>
    <taxon_name authority="(Munz &amp; I. M. Johnston) Kearney" date="unknown" rank="species">clementinus</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>6: 127. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus malacothamnus;species clementinus;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101080</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malvastrum</taxon_name>
    <taxon_name authority="Munz &amp; I. M. Johnston" date="unknown" rank="species">clementinum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>51: 296. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Malvastrum;species clementinum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sphaeralcea</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="species">orbiculata</taxon_name>
    <taxon_name authority="(Munz &amp; I. M. Johnston) Jepson" date="unknown" rank="variety">clementina</taxon_name>
    <taxon_hierarchy>genus Sphaeralcea;species orbiculata;variety clementina</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">San Clemente Island bushmallow</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 0.4–1 m, branches slender, indument white or grayish, sparse to dense, shaggy-tomentose, canescent, hairs mostly stellate, stalked, 10–30-armed.</text>
      <biological_entity id="o18269" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.4" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
      <biological_entity id="o18270" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o18271" name="indument" name_original="indument" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character char_type="range_value" from="sparse" name="density" src="d0_s0" to="dense" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="shaggy-tomentose" value_original="shaggy-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity id="o18272" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="10-30-armed" value_original="10-30-armed" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ± round, 3-lobed or 5-lobed, to 5 (–8) cm, thin to moderately thick, surfaces: adaxial dark green and glabrate, abaxial soft-tomentose, lobes triangular to rounded, basal sinus not overlapping.</text>
      <biological_entity id="o18273" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s1" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="5-lobed" value_original="5-lobed" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="distance" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="thin" name="width" src="d0_s1" to="moderately thick" />
      </biological_entity>
      <biological_entity id="o18274" name="surface" name_original="surfaces" src="d0_s1" type="structure" />
      <biological_entity constraint="adaxial" id="o18275" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18276" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="soft-tomentose" value_original="soft-tomentose" />
      </biological_entity>
      <biological_entity id="o18277" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="triangular" name="shape" src="d0_s1" to="rounded" />
      </biological_entity>
      <biological_entity constraint="basal" id="o18278" name="sinus" name_original="sinus" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s1" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences short-spicate, flower clusters subsessile, congested;</text>
      <biological_entity id="o18279" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="short-spicate" value_original="short-spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>involucellar bractlets filiform to linear, 3–9 × 0.5 mm, 3/4 to exceeding calyx length.</text>
      <biological_entity id="o18280" name="flower" name_original="flower" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="cluster" value_original="cluster" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o18281" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
        <character name="quantity" src="d0_s3" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o18282" name="calyx" name_original="calyx" src="d0_s3" type="structure" />
      <relation from="o18281" id="r1939" name="exceeding" negation="false" src="d0_s3" to="o18282" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: calyx campanulate, 5–9 mm, lobes coherent, narrowly triangular, 3.5–6.5 × 1.5–2.3 mm, ca. 2 times as long as wide, 2–3 times tube length, apex long-acute to acuminate, densely villous;</text>
      <biological_entity id="o18283" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o18284" name="calyx" name_original="calyx" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18285" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="coherent" value_original="coherent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s4" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="2" value_original="2" />
        <character constraint="tube" constraintid="o18286" is_modifier="false" name="length" src="d0_s4" value="2-3 times tube length" value_original="2-3 times tube length" />
      </biological_entity>
      <biological_entity id="o18286" name="tube" name_original="tube" src="d0_s4" type="structure" />
      <biological_entity id="o18287" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="long-acute" name="shape" src="d0_s4" to="acuminate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals white, fading lavender, 1.5 cm.</text>
      <biological_entity id="o18288" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o18289" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="fading lavender" value_original="fading lavender" />
        <character name="some_measurement" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Mericarps 2–3 mm. 2n = 34.</text>
      <biological_entity id="o18290" name="mericarp" name_original="mericarps" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18291" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal sage scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal sage scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Malacothamnus clementinus is known from San Clemente Island and is considered endangered. In leaf characters it is similar to M. fasciculatus; in indument and calyx characters, it approaches M. fremontii. The petals are often not overlapping, a feature rare in Malacothamnus. Malacothamnus clementinus tends to propagate mainly via rhizomes and rarely produces fertile seeds.</discussion>
  <discussion>Malacothamnus clementinus is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>