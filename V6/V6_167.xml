<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">hypericaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hypericum</taxon_name>
    <taxon_name authority="(Spach) R. Keller in H. G. A. Engler and K. Prantl" date="1893" rank="section">ROSCYNA</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>95[III,6]: 211. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus hypericum;section ROSCYNA</taxon_hierarchy>
    <other_info_on_name type="fna_id">317119</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Spach" date="unknown" rank="section">Roscyna</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., s é r.</publication_title>
      <place_in_publication>2, 5: 364. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>section Roscyna</taxon_hierarchy>
  </taxon_identification>
  <number>1d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, black glands absent.</text>
      <biological_entity id="o8526" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o8527" name="gland" name_original="glands" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="black" value_original="black" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent (base not articulated).</text>
      <biological_entity id="o8528" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers 40–70 mm diam.;</text>
      <biological_entity id="o8529" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="diameter" src="d0_s2" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals persistent, 5;</text>
      <biological_entity id="o8530" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals persistent, 5;</text>
      <biological_entity id="o8531" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens persistent, 150, in 5 fascicles, fascicles usually distinct, rarely 1 pair connate;</text>
      <biological_entity id="o8532" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s5" value="150" value_original="150" />
        <character is_modifier="false" modifier="in 5 fascicles" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
        <character modifier="rarely" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ovary (4–) 5-merous;</text>
    </statement>
    <statement id="d0_s7">
      <text>placentation axile;</text>
      <biological_entity id="o8533" name="ovary" name_original="ovary" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="(4-)5-merous" value_original="(4-)5-merous" />
        <character is_modifier="false" name="placentation" src="d0_s7" value="axile" value_original="axile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles ± appressed, bases ± connate or distinct;</text>
      <biological_entity id="o8534" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o8535" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stigmas ± broadly capitate.</text>
      <biological_entity id="o8536" name="stigma" name_original="stigmas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less broadly" name="architecture_or_shape" src="d0_s9" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds ± carinate.</text>
      <biological_entity id="o8537" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="carinate" value_original="carinate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America, ne, e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
        <character name="distribution" value="ne" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>The other species in the section, Hypericum przewalskii Maximowicz, is endemic to China.</discussion>
  
</bio:treatment>