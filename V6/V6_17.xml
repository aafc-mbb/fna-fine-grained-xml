<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 15:34:56</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">18</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Schrader" date="1831" rank="genus">CYCLANTHERA</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (Göttingen)</publication_title>
      <place_in_publication>1831: 2. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus CYCLANTHERA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kyklos, circle, and antheros, blooming, alluding to single, ringlike stamen</other_info_on_name>
    <other_info_on_name type="fna_id">108824</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Paul G. Wilson" date="unknown" rank="genus">Cremastopus</taxon_name>
    <taxon_hierarchy>genus Cremastopus</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Bur cucumber</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, monoecious [dioecious], climbing;</text>
      <biological_entity id="o8828" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems annual, glabrous or hairy;</text>
      <biological_entity id="o8829" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>roots fibrous;</text>
      <biological_entity id="o8830" name="root" name_original="roots" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>tendrils unbranched or 2 [–3] -branched, branches unequal in length.</text>
      <biological_entity id="o8831" name="tendril" name_original="tendrils" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="2[-3]-branched" value_original="2[-3]-branched" />
      </biological_entity>
      <biological_entity id="o8832" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves simple or compound;</text>
      <biological_entity id="o8833" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade lanceolate to orbiculate, [unlobed or shallowly 3–9-lobed to palmately 3–5-lobed] 3–7-foliolate (3–7-palmatisect), [divisions narrowly broadly lanceolate to elliptic-lanceolate or oblanceolate to obspatulate] base attenuate to cordate, margins coarsely serrate to shallowly or deeply lobed [to serrulate or denticulate], abaxial surface glabrous, adaxial surface scabrous with hair-bases.</text>
      <biological_entity id="o8834" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="orbiculate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-7-foliolate" value_original="3-7-foliolate" />
      </biological_entity>
      <biological_entity id="o8835" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cordate" />
      </biological_entity>
      <biological_entity id="o8836" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="coarsely serrate" name="shape" src="d0_s5" to="shallowly or deeply lobed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8837" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8838" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character constraint="with hair-bases" constraintid="o8839" is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o8839" name="hair-base" name_original="hair-bases" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: staminate flowers in axillary racemes or narrow panicles with or without well-differentiated main axis;</text>
      <biological_entity id="o8840" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o8841" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o8842" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o8843" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="main" id="o8844" name="axis" name_original="axis" src="d0_s6" type="structure">
        <character is_modifier="true" name="variability" src="d0_s6" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
      <relation from="o8841" id="r926" name="in" negation="false" src="d0_s6" to="o8842" />
      <relation from="o8841" id="r927" name="in" negation="false" src="d0_s6" to="o8843" />
      <relation from="o8843" id="r928" name="with or without" negation="false" src="d0_s6" to="o8844" />
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers solitary, usually from same axils as staminate, peduncles erect at apex;</text>
      <biological_entity id="o8845" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o8846" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o8847" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <biological_entity id="o8848" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character constraint="at apex" constraintid="o8849" is_modifier="false" modifier="as staminate" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o8849" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o8846" id="r929" modifier="usually" name="from" negation="false" src="d0_s7" to="o8847" />
    </statement>
    <statement id="d0_s8">
      <text>floral bracts caducous, filiform.</text>
      <biological_entity id="o8850" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity constraint="floral" id="o8851" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: hypanthium campanulate to shallowly cupulate;</text>
      <biological_entity id="o8852" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8853" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s9" to="shallowly cupulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals vestigial [5, ovate-triangular to triangular];</text>
      <biological_entity id="o8854" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8855" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, connate 1/3 [–2/3] length, white [greenish or yellowish], oblong-ovate to triangular, 1–1.5 [–5] mm, glabrous, minutely papillate-glandular adaxially, corolla rotate [campanulate].</text>
      <biological_entity id="o8856" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8857" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/3" name="length" src="d0_s11" to="2/3" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s11" to="triangular" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely; adaxially" name="architecture_or_function_or_pubescence" src="d0_s11" value="papillate-glandular" value_original="papillate-glandular" />
      </biological_entity>
      <biological_entity id="o8858" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rotate" value_original="rotate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: stamens 5;</text>
      <biological_entity id="o8859" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8860" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted at receptacle base, connate into central column, 0.2–2 mm (anthers sessile or subsessile to short-columnar);</text>
      <biological_entity id="o8861" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8862" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character constraint="into central column" constraintid="o8864" is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="receptacle" id="o8863" name="base" name_original="base" src="d0_s13" type="structure" />
      <biological_entity constraint="central" id="o8864" name="column" name_original="column" src="d0_s13" type="structure" />
      <relation from="o8862" id="r930" name="inserted at" negation="false" src="d0_s13" to="o8863" />
    </statement>
    <statement id="d0_s14">
      <text>thecae connate-fused into continuous, horizontal ring as a single structure (synanther), forming a head, dehiscing by continuous slit, connective broad;</text>
      <biological_entity id="o8865" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8866" name="theca" name_original="thecae" src="d0_s14" type="structure">
        <character constraint="into ring" constraintid="o8867" is_modifier="false" name="fusion" src="d0_s14" value="connate-fused" value_original="connate-fused" />
        <character constraint="by slit" constraintid="o8870" is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o8867" name="ring" name_original="ring" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="continuous" value_original="continuous" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="horizontal" value_original="horizontal" />
      </biological_entity>
      <biological_entity id="o8868" name="structure" name_original="structure" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o8869" name="head" name_original="head" src="d0_s14" type="structure" />
      <biological_entity id="o8870" name="slit" name_original="slit" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o8871" name="connective" name_original="connective" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o8867" id="r931" name="as" negation="false" src="d0_s14" to="o8868" />
      <relation from="o8866" id="r932" name="forming a" negation="false" src="d0_s14" to="o8869" />
    </statement>
    <statement id="d0_s15">
      <text>pistillodes absent.</text>
      <biological_entity id="o8872" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8873" name="pistillode" name_original="pistillodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: ovary usually 1-locular, ovoid to elliptic-ovoid or subglobose;</text>
      <biological_entity id="o8874" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8875" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s16" value="1-locular" value_original="1-locular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="elliptic-ovoid or subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules (1–) 2–6 per locule;</text>
      <biological_entity id="o8876" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8877" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="2" to_inclusive="false" />
        <character char_type="range_value" constraint="per locule" constraintid="o8878" from="2" name="quantity" src="d0_s17" to="6" />
      </biological_entity>
      <biological_entity id="o8878" name="locule" name_original="locule" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1, short-columnar;</text>
      <biological_entity id="o8879" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8880" name="style" name_original="style" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s18" value="short-columnar" value_original="short-columnar" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigma 1, depressed-subglobose;</text>
      <biological_entity id="o8881" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8882" name="stigma" name_original="stigma" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s19" value="depressed-subglobose" value_original="depressed-subglobose" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>staminodes absent.</text>
      <biological_entity id="o8883" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8884" name="staminode" name_original="staminodes" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits fleshy-capsular, green to light green, narrowly obliquely ovoid to obliquely globose or obovoid (or subglobose to broadly ellipsoid), gibbous, acute to obtuse at both ends, densely echinate [to smooth or very sparsely echinate], irregularly and explosively dehiscent.</text>
      <biological_entity id="o8885" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="fleshy-capsular" value_original="fleshy-capsular" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s21" to="light green" />
        <character char_type="range_value" from="narrowly obliquely ovoid" name="shape" src="d0_s21" to="obliquely globose or obovoid gibbous acute" />
        <character char_type="range_value" from="narrowly obliquely ovoid" name="shape" src="d0_s21" to="obliquely globose or obovoid gibbous acute" />
        <character char_type="range_value" constraint="at ends" constraintid="o8886" from="narrowly obliquely ovoid" name="shape" src="d0_s21" to="obliquely globose or obovoid gibbous acute" />
        <character is_modifier="false" modifier="densely" name="architecture" notes="" src="d0_s21" value="echinate" value_original="echinate" />
        <character is_modifier="false" modifier="irregularly; explosively" name="dehiscence" src="d0_s21" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o8886" name="end" name_original="ends" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Seeds (1–) 4–12, ovoid to oblong, compressed, not arillate, margins not winged but sculptured, surface [smooth, roughened, muriculate, or] tuberculate.</text>
      <biological_entity id="o8887" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s22" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s22" to="12" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s22" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s22" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity id="o8888" name="margin" name_original="margins" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="relief" src="d0_s22" value="sculptured" value_original="sculptured" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>x = 16.</text>
      <biological_entity id="o8889" name="surface" name_original="surface" src="d0_s22" type="structure">
        <character is_modifier="false" name="relief" src="d0_s22" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="x" id="o8890" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, c United States, Mexico, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="c United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 35 (4 in the flora).</discussion>
  <discussion>Species of Cyclanthera are characterized by their annual duration, relatively small flowers, androecium of a single, horizontal, continuous, circular theca, and gibbous, echinate, placental arm. Nectaries are derived from secretory hairs on the hypanthium (S. Vogel 1981). The northern Mexican Cremastopus was distinguished by the production of a single ovule, but in all other diagnostic features it is similar to Cyclanthera (D. M. Kearns and E. C. Jones 1992).</discussion>
  <discussion>Fruits of Cyclanthera are usually echinate; only C. pedata (Linnaeus) Schrader and two other species lack spinules. Wild relatives of C. pedata are spiny-fruited and the occurrence of spinules within the species is variable. Cyclanthera pedata is cultivated in Central America and South America for its large, virtually hollow, edible fruits, which are used like bell peppers, mostly to be filled and baked (B. E. Hammel 2009).</discussion>
  <references>
    <reference>Jones, C. E. 1969. A Revision of the Genus Cyclanthera (Cucurbitaceae). Ph.D. dissertation. Indiana University.</reference>
    <reference>Nesom, G. L. 2014. Taxonomy of Cyclanthera (Cucurbitaceae) in the USA. Phytoneuron 2014-11: 1–17.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Staminate inflorescences (1–)7–27-flowered, usually without lateral branches, floriferous portion 0.2–2.2 cm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Staminate inflorescences (12–)18–90(–130)-flowered, laterally branched, floriferous portion 3–7 or (0.5–)1.2–6 cm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting peduncles 10–30 mm; staminate corollas 4.2–6.3 mm diam.; anther heads 1.4–2.2(–2.8) mm diam., consistently ciliate.</description>
      <determination>2 Cyclanthera naudiniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting peduncles 2–6 mm; staminate corollas 2.5–2.8 mm diam.; anther heads 0.6–0.8 mm diam., glabrous.</description>
      <determination>3 Cyclanthera gracillima</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate inflorescences (50–)65–90(–130)-flowered, lateral branches 3–25 mm; terminal leaflet margins coarsely serrate to lobed.</description>
      <determination>1 Cyclanthera dissecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Staminate inflorescences (12–)18–70-flowered, lateral branches 3–7 mm; terminal leaflet margins shallowly to coarsely serrate but not lobed.</description>
      <determination>4 Cyclanthera stenura</determination>
    </key_statement>
  </key>
</bio:treatment>