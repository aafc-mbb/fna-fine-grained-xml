<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/14 19:02:33</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">cucurbitaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">cucurbita</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">melopepo</taxon_name>
    <taxon_name authority="(Scheele) G. L. Nesom" date="unknown" rank="variety">texana</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2011-13: 24. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus cucurbita;species melopepo;subspecies texana;variety texana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100839</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tristemon</taxon_name>
    <taxon_name authority="Scheele" date="unknown" rank="species">texanus</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>21: 586. 1848 (as texanum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tristemon;species texanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cucurbita</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pepo</taxon_name>
    <taxon_name authority="(Scheele) D. S. Decker" date="unknown" rank="variety">texana</taxon_name>
    <taxon_hierarchy>genus Cucurbita;species pepo;variety texana</taxon_hierarchy>
  </taxon_identification>
  <number>6b.1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Pepos usually green-and-white-striped at maturity, sometimes yellow.</text>
      <biological_entity id="o116" name="pepo" name_original="pepos" src="d0_s0" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="green-and-white-striped" value_original="green-and-white-striped" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Seeds: germination within 3–7 days.</text>
      <biological_entity id="o117" name="seed" name_original="seeds" src="d0_s1" type="structure" />
      <biological_entity id="o118" name="day" name_original="days" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s1" to="7" />
      </biological_entity>
      <relation from="o117" id="r18" name="within" negation="false" src="d0_s1" to="o118" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream beds, lake shores, marsh banks, low woods, dunes, disturbed sandy sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream beds" />
        <character name="habitat" value="lake shores" />
        <character name="habitat" value="marsh banks" />
        <character name="habitat" value="low woods" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="disturbed sandy sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Isozyme and RAPD data indicate that var. texana is limited to south-central Texas (D. S. Decker et al. 1993, 2002b; C. W. Cowan and B. D. Smith 1993). Attribution of var. texana to New Mexico apparently has been erroneous. Plants associated with the Mississippi River drainage are var. ozarkana, according to the molecular analyses.</discussion>
  <discussion>Molecular differentiation between var. ozarkana and var. texana suggests that reproductive isolation has allowed an accumulation of differences; H. Teppner (2004) regarded morphological variability as too overlapping to allow their unarbitrary separation. Molecular data reliably separate them and clearly delimit var. fraterna as well; morphology needs to be restudied. Fruits of var. ozarkana usually are ivory white at maturity versus striped in var. texana, but this is not completely consistent (T. C. Andres and H. B. Tukey 1995; pers. obs.).</discussion>
  
</bio:treatment>