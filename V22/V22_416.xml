<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Genuini</taxon_name>
    <taxon_name authority="Willdenow" date="1799" rank="species">arcticus</taxon_name>
    <taxon_name authority="(Willdenow ex Roemer &amp; Schultes) Balslev" date="1983" rank="variety">mexicanus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>35: 308. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus genuini;species arcticus;variety mexicanus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000095</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">mexicanus</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>7(1): 178. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species mexicanus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">balticus</taxon_name>
    <taxon_name authority="(Willdenow ex Roemer &amp; Schultes) Kuntze" date="unknown" rank="variety">mexicanus</taxon_name>
    <taxon_hierarchy>genus Juncus;species balticus;variety mexicanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 2–8 dm.</text>
      <biological_entity id="o9442" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms usually compressed.</text>
      <biological_entity id="o9443" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade distinct, somewhat flattened, to 20 cm or more.</text>
      <biological_entity id="o9444" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o9445" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: tepals usually brown or blackish, 3.5–4.5 (–5.5) mm;</text>
      <biological_entity id="o9446" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o9447" name="tepal" name_original="tepals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>filaments 0.2–0.4 mm, anthers 1.2–2.2 mm.</text>
      <biological_entity id="o9448" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o9449" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9450" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s4" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Intermittent or permanent water courses, around springs, and in meadows in sandy or gravelly soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="water courses" modifier="intermittent or permanent" constraint="around springs" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="meadows" modifier="and in" constraint="in sandy or gravelly soils" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., N.Mex., Nev., Utah; Mexico; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9c</number>
  <other_name type="common_name">Mexican rush</other_name>
  <discussion>Juncus arcticus var. mexicanus is the common phase of the species in the extreme southwest United States and Mexico; northward the variety becomes sporadic in its occurrence. Rare individuals having leaves with blades are sometimes found in otherwise bladeless populations in more northern areas but it is doubtful that these oddities represent an equivalent variant.</discussion>
  
</bio:treatment>