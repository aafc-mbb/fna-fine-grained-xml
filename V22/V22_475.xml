<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Kral" date="1966" rank="species">isoetifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>2: 227, plate [p. 252], fig 2. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species isoetifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000470</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, densely cespitose, (15–) 20–30 (–40) cm, base not abruptly bulbous.</text>
      <biological_entity id="o15916" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o15917" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not abruptly" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems compact.</text>
      <biological_entity id="o15918" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect or ascending, 4–15 cm;</text>
      <biological_entity id="o15919" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths glossy brown or redbrown, chaffy;</text>
      <biological_entity id="o15920" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="chaffy" value_original="chaffy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade green, filiform or narrowly linear, twisted, to 1 mm wide, smooth.</text>
      <biological_entity id="o15921" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths exceeded by leaves;</text>
      <biological_entity id="o15922" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o15923" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o15924" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o15923" id="r2064" name="exceeded by" negation="false" src="d0_s5" to="o15924" />
    </statement>
    <statement id="d0_s6">
      <text>scapes linear, nearly terete, 0.5 (–0.7) mm wide, smooth, not ribbed;</text>
      <biological_entity id="o15925" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o15926" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="width" src="d0_s6" to="0.7" to_unit="mm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes ellipsoid to obovoid, 5–7 (–10) mm;</text>
      <biological_entity id="o15927" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o15928" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="obovoid" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts ca. 4.5 mm, margins nearly entire, apex rounded.</text>
      <biological_entity id="o15929" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o15930" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="4.5" value_original="4.5" />
      </biological_entity>
      <biological_entity id="o15931" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15932" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included, reddish-brown, linear-curvate, 4 mm, keel firm, ciliate;</text>
      <biological_entity id="o15933" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o15934" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-curvate" value_original="linear-curvate" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o15935" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding in morning, blade obovate, 4 mm;</text>
      <biological_entity id="o15936" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15937" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in morning" constraintid="o15938" is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <biological_entity id="o15938" name="morning" name_original="morning" src="d0_s10" type="structure" />
      <biological_entity id="o15939" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o15940" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15941" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds translucent, ellipsoid, 0.5 mm, distinctly longitudinally ribbed, with fainter transverse lines.</text>
      <biological_entity id="o15943" name="line" name_original="lines" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="fainter" value_original="fainter" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o15942" id="r2065" name="with" negation="false" src="d0_s12" to="o15943" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o15942" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="distinctly longitudinally" name="architecture_or_shape" src="d0_s12" value="ribbed" value_original="ribbed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15944" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sphagnous bogs, low pine savanna, shores of dolines, coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sphagnous bogs" />
        <character name="habitat" value="low pine savanna" />
        <character name="habitat" value="shores" constraint="of dolines , coastal plain" />
        <character name="habitat" value="dolines" />
        <character name="habitat" value="coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <discussion>Xyris isoetifolia, locally abundant only in northwest Florida, is most often mistaken for X. baldwiniana but is distinguishable by its bearded staminodes and more distinctly ribbed, shorter seeds.</discussion>
  
</bio:treatment>