<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Thouars" date="1806" rank="genus">halophila</taxon_name>
    <taxon_name authority="Eiseman" date="1980" rank="species">johnsonii</taxon_name>
    <place_of_publication>
      <publication_title>Aquatic Botany</publication_title>
      <place_in_publication>9: 16, fig. 1. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus halophila;species johnsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000078</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes with 2 scales at each node.</text>
      <biological_entity id="o7731" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <biological_entity id="o7732" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7733" name="node" name_original="node" src="d0_s0" type="structure" />
      <relation from="o7731" id="r1031" name="with" negation="false" src="d0_s0" to="o7732" />
      <relation from="o7732" id="r1032" name="at" negation="false" src="d0_s0" to="o7733" />
    </statement>
    <statement id="d0_s1">
      <text>Erect stems absent.</text>
      <biological_entity id="o7734" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves apparently attached to rhizome, 5–25 × 1–4 mm;</text>
      <biological_entity id="o7735" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to rhizome" constraintid="o7736" is_modifier="false" modifier="apparently" name="fixation" src="d0_s2" value="attached" value_original="attached" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" notes="" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7736" name="rhizome" name_original="rhizome" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade linear-lanceolate, base cuneate, margins entire, glabrous, apex round to round-acute.</text>
      <biological_entity id="o7737" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o7738" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7739" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7740" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s3" to="round-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: spathe 2-leaved, enclosing pistillate flowers.</text>
      <biological_entity id="o7741" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o7742" name="spathe" name_original="spathe" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-leaved" value_original="2-leaved" />
      </biological_entity>
      <biological_entity id="o7743" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o7742" id="r1033" name="enclosing" negation="false" src="d0_s4" to="o7743" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers unisexual, staminate and pistillate on different plants;</text>
      <biological_entity id="o7744" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o7745" is_modifier="false" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7745" name="plant" name_original="plants" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>staminate flowers unknown;</text>
      <biological_entity id="o7746" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate flowers sessile, ca. 5 mm.</text>
      <biological_entity id="o7747" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits ovoid.</text>
      <biological_entity id="o7748" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds not seen.</text>
      <biological_entity id="o7749" name="seed" name_original="seeds" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy substrates of marine waters</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy substrates" constraint="of marine waters" />
        <character name="habitat" value="marine waters" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-2–0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="2" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <references>
    <reference>Eiseman, N. J. and C. McMillan. 1980. A new species of seagrass, Halophila johnsonii, from the Atlantic coast of Florida. Aquatic Bot. 9: 15–19.</reference>
  </references>
  
</bio:treatment>