<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Borner" date="1912" rank="genus">stuckenia</taxon_name>
    <taxon_name authority="(Persoon) Börner" date="1912" rank="species">filiformis</taxon_name>
    <place_of_publication>
      <publication_title>Flora fur das deutsche Volk</publication_title>
      <place_in_publication>713. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus stuckenia;species filiformis</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000373</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potamogeton</taxon_name>
    <taxon_name authority="Persoon" date="unknown" rank="species">filiformis</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 152. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potamogeton;species filiformis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems freely branching proximally, sparsely branching distally, subterete, (10–) 20–60 (–100) cm.</text>
      <biological_entity id="o1803" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="freely; proximally" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="sparsely; distally" name="architecture" src="d0_s0" value="branching" value_original="branching" />
        <character is_modifier="false" name="shape" src="d0_s0" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: length and width of those on main-stem only slightly larger than those on branches;</text>
      <biological_entity id="o1804" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o1805" name="main-stem" name_original="main-stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>stipules with stipular sheaths often inflated on proximal portion of stem, 1–4 (–9.5) cm, summit of midstem stipules tight to stem, ca. ± same width as stem, ligule 2–20 mm, distinct, especially on distal stipules;</text>
      <biological_entity id="o1806" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1807" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1808" name="stipular" name_original="stipular" src="d0_s2" type="structure">
        <character constraint="on proximal portion" constraintid="o1810" is_modifier="false" modifier="often" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o1809" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character constraint="on proximal portion" constraintid="o1810" is_modifier="false" modifier="often" name="shape" src="d0_s2" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1810" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o1811" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o1812" name="summit" name_original="summit" src="d0_s2" type="structure">
        <character constraint="to stem" constraintid="o1814" is_modifier="false" name="arrangement_or_density" src="d0_s2" value="tight" value_original="tight" />
        <character constraint="as stem" constraintid="o1815" is_modifier="false" modifier="more or less" name="character" notes="" src="d0_s2" value="width" value_original="width" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o1813" name="stipule" name_original="stipules" src="d0_s2" type="structure" />
      <biological_entity id="o1814" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o1815" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o1816" name="ligule" name_original="ligule" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1817" name="stipule" name_original="stipules" src="d0_s2" type="structure" />
      <relation from="o1807" id="r232" name="with" negation="false" src="d0_s2" to="o1808" />
      <relation from="o1807" id="r233" name="with" negation="false" src="d0_s2" to="o1809" />
      <relation from="o1810" id="r234" name="part_of" negation="false" src="d0_s2" to="o1811" />
      <relation from="o1812" id="r235" name="part_of" negation="false" src="d0_s2" to="o1813" />
      <relation from="o1816" id="r236" modifier="especially" name="on" negation="false" src="d0_s2" to="o1817" />
    </statement>
    <statement id="d0_s3">
      <text>blade filiform or slenderly linear, 1–15 cm × 0.2–2 (–3.7) mm, apex notched, blunt, or short-apiculate;</text>
      <biological_entity id="o1818" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1819" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1820" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="notched" value_original="notched" />
        <character is_modifier="false" name="shape" src="d0_s3" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s3" value="short-apiculate" value_original="short-apiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s3" value="short-apiculate" value_original="short-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins 1–3.</text>
      <biological_entity id="o1821" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1822" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: peduncles terminal, erect, filiform to slender, 2–10 (–15) cm;</text>
      <biological_entity id="o1823" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o1824" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spikes cylindric to moniliform, 5–55 mm;</text>
      <biological_entity id="o1825" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o1826" name="spike" name_original="spikes" src="d0_s6" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s6" to="moniliform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="55" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>verticels 2–6 (–9).</text>
      <biological_entity id="o1827" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o1828" name="verticel" name_original="verticels" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits dark-brown, obovoid, 2–3 × 1.5–2.4 mm;</text>
      <biological_entity id="o1829" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>beak inconspicuous.</text>
      <biological_entity id="o1830" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Iowa, Maine, Mich., Minn., Mont., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Oreg., Pa., S.Dak., Utah, Vt., Wash., Wis., Wyo.; nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Threadleaf-pondweed</other_name>
  <discussion>Three distinct subspecies seemingly apparently occur in North America. They are separated mainly by the size of the plants and the peduncle characteristics. Stuckenia filiformis subsp. occidentalis typically grows in cold deep water, standing or with a strong current. This variety tends to become robust and is easily confused with Stuckenia vaginata and S. striata. Stuckenia filiformis subsp. alpina is a much smaller plant typically growing in standing waters. Stuckenia filiformis subsp. filiformis is restricted to the far north. In the intermountain region of western United States is a more robust form that is quite similar to S. filiformis subsp. alpina but although it has previously been recognized as Potamogeton [Stuckenia] filiformis var. macounii Morong (J. L. Reveal 1977b).</discussion>
  <discussion>Subspecies ca. 5 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 20–100 cm; stipules on proximal portion of stem inflated, disintegrating with age; fruits often absent.</description>
      <determination>3c Stuckenia filiformis subsp. occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 10–30 cm; stipules on proximal portion of stem tightly clasping or slightly enlarged, persistent; fruits common.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles Peduncles with flowers and/or fruits 4 cm or more apart; leaves 0.2–0.5 mm wide.</description>
      <determination>3a Stuckenia filiformis subsp. Filiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles with flowers and/or fruits less than 4 cm apart; leaves to 1 mm widee.</description>
      <determination>3b Stuckenia filiformis subsp. alpina</determination>
    </key_statement>
  </key>
</bio:treatment>