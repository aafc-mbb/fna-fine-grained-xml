<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schultz Schultzenstein" date="unknown" rank="family">arecaceae</taxon_name>
    <taxon_name authority="Griffith" date="1844" rank="subfamily">CORYPHOIDEAE</taxon_name>
    <taxon_name authority="Martius in S. L. Endlicher" date="1837" rank="tribe">CORYPHEAE</taxon_name>
    <taxon_name authority="Saakov" date="1954" rank="subtribe">LIVISTONINAE</taxon_name>
    <taxon_name authority="H. Wendland" date="unknown" rank="genus">Washingtonia</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zeitung (Berlin)</publication_title>
      <place_in_publication>37: 68, 148. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family arecaceae;subfamily coryphoideae;tribe corypheae;subtribe livistoninae;genus washingtonia;</taxon_hierarchy>
    <other_info_on_name type="etymology">fFor George Washington, 1732–1799, American patriot and first president of the United States</other_info_on_name>
    <other_info_on_name type="fna_id">134783</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Sudworth" date="unknown" rank="genus">Neowashingtonia</taxon_name>
    <taxon_hierarchy>genus Neowashingtonia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tree palms.</text>
      <biological_entity id="o9162" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9163" name="palm" name_original="palms" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s0" value="tree" value_original="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems solitary, erect, tall, massive in one species (more than 10020 cm diam.) (100–150 cm diam.), partly or completely covered with old leaf-bases and marcescent dry leaves forming conspicuous skirt around trunk.</text>
      <biological_entity id="o9164" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="height" src="d0_s1" value="tall" value_original="tall" />
        <character constraint="in species" constraintid="o9165" is_modifier="false" name="size" src="d0_s1" value="massive" value_original="massive" />
      </biological_entity>
      <biological_entity id="o9165" name="species" name_original="species" src="d0_s1" type="taxon_name" />
      <biological_entity id="o9166" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <biological_entity id="o9167" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
        <character is_modifier="true" name="condition_or_texture" src="d0_s1" value="dry" value_original="dry" />
      </biological_entity>
      <biological_entity id="o9168" name="skirt" name_original="skirt" src="d0_s1" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o9169" name="trunk" name_original="trunk" src="d0_s1" type="structure" />
      <relation from="o9164" id="r1209" modifier="completely; partly" name="covered with" negation="false" src="d0_s1" to="o9166" />
      <relation from="o9164" id="r1210" modifier="completely; partly" name="covered with" negation="false" src="d0_s1" to="o9167" />
      <relation from="o9164" id="r1211" modifier="partly" name="forming" negation="false" src="d0_s1" to="o9168" />
      <relation from="o9164" id="r1212" modifier="partly" name="around" negation="false" src="d0_s1" to="o9169" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheath fibers soft;</text>
      <biological_entity id="o9170" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="sheath" id="o9171" name="fiber" name_original="fibers" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole split at base, conspicuously armed with teeth along margins, sometimes unarmed in tall plants;</text>
      <biological_entity id="o9172" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="petiole" id="o9173" name="split" name_original="split" src="d0_s3" type="structure">
        <character constraint="with teeth" constraintid="o9175" is_modifier="false" modifier="conspicuously" name="architecture" notes="" src="d0_s3" value="armed" value_original="armed" />
        <character constraint="in plants" constraintid="o9177" is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s3" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o9174" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o9175" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity id="o9176" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <biological_entity id="o9177" name="plant" name_original="plants" src="d0_s3" type="structure">
        <character is_modifier="true" name="height" src="d0_s3" value="tall" value_original="tall" />
      </biological_entity>
      <relation from="o9173" id="r1213" name="at" negation="false" src="d0_s3" to="o9174" />
      <relation from="o9175" id="r1214" name="along" negation="false" src="d0_s3" to="o9176" />
    </statement>
    <statement id="d0_s4">
      <text>abaxial hastula absent;</text>
      <biological_entity id="o9178" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o9179" name="hastulum" name_original="hastula" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>adaxial hastula irregularly shaped, margin becoming tattered, fibrous;</text>
      <biological_entity id="o9180" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o9181" name="hastulum" name_original="hastula" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="irregularly--shaped" value_original="irregularly--shaped" />
      </biological_entity>
      <biological_entity id="o9182" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="tattered" value_original="tattered" />
        <character is_modifier="false" name="texture" src="d0_s5" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa prominent;</text>
      <biological_entity id="o9183" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o9184" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade costapalmate;</text>
      <biological_entity id="o9185" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o9186" name="blade" name_original="blade" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>plication induplicate;</text>
      <biological_entity id="o9187" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o9188" name="plication" name_original="plication" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s8" value="induplicate" value_original="induplicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>segments lanceolate, basally connate, bearing fibers between segments;</text>
      <biological_entity id="o9189" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <biological_entity id="o9190" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o9191" name="fiber" name_original="fibers" src="d0_s9" type="structure" />
      <biological_entity id="o9193.o9190." name="segment-segment" name_original="segments" src="d0_s9" type="structure" />
      <relation from="o9190" id="r1215" name="bearing" negation="false" src="d0_s9" to="o9191" />
    </statement>
    <statement id="d0_s10">
      <text>apices 2-cleft or irregularly tattered into fibers.</text>
      <biological_entity id="o9194" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <biological_entity id="o9195" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2-cleft" value_original="2-cleft" />
        <character constraint="into fibers" constraintid="o9196" is_modifier="false" modifier="irregularly" name="shape" src="d0_s10" value="tattered" value_original="tattered" />
      </biological_entity>
      <biological_entity id="o9196" name="fiber" name_original="fibers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences axillary within crown of leaves, paniculate, arching well beyond leaves, with 2 orders of branching;</text>
      <biological_entity id="o9197" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character constraint="within crown" constraintid="o9198" is_modifier="false" name="position" src="d0_s11" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s11" value="paniculate" value_original="paniculate" />
        <character constraint="beyond leaves" constraintid="o9200" is_modifier="false" name="orientation" src="d0_s11" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o9198" name="crown" name_original="crown" src="d0_s11" type="structure" />
      <biological_entity id="o9199" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o9200" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o9201" name="order" name_original="orders" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o9198" id="r1216" name="part_of" negation="false" src="d0_s11" to="o9199" />
      <relation from="o9197" id="r1217" modifier="of branching" name="with" negation="false" src="d0_s11" to="o9201" />
    </statement>
    <statement id="d0_s12">
      <text>prophyll leathery;</text>
      <biological_entity id="o9202" name="prophyll" name_original="prophyll" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>rachis bracts very conspicuous, tubular at base, distally flattened and leathery, long;</text>
      <biological_entity constraint="rachis" id="o9203" name="bract" name_original="bracts" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="very" name="prominence" src="d0_s13" value="conspicuous" value_original="conspicuous" />
        <character constraint="at base" constraintid="o9204" is_modifier="false" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s13" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="texture" src="d0_s13" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o9204" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>rachillae glabrous.</text>
      <biological_entity id="o9205" name="rachilla" name_original="rachillae" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Flowers bisexual, borne singly along rachillae, short pedicellate;</text>
      <biological_entity id="o9206" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o9207" name="rachilla" name_original="rachillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s15" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o9206" id="r1218" name="borne" negation="false" src="d0_s15" to="o9207" />
    </statement>
    <statement id="d0_s16">
      <text>perianth 2-seriate;</text>
      <biological_entity id="o9208" name="perianth" name_original="perianth" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s16" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>calyx cupulate, 3-lobed, apices and margins irregular;</text>
      <biological_entity id="o9209" name="calyx" name_original="calyx" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cupulate" value_original="cupulate" />
      </biological_entity>
      <biological_entity id="o9210" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity id="o9211" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s17" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 3, long, chaffy, basally connate into tube;</text>
      <biological_entity id="o9212" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character is_modifier="false" name="length_or_size" src="d0_s18" value="long" value_original="long" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="chaffy" value_original="chaffy" />
        <character constraint="into tube" constraintid="o9213" is_modifier="false" modifier="basally" name="fusion" src="d0_s18" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o9213" name="tube" name_original="tube" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>stamens 6, adnate briefly to petals;</text>
      <biological_entity id="o9214" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="6" value_original="6" />
        <character constraint="to petals" constraintid="o9215" is_modifier="false" name="fusion" src="d0_s19" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o9215" name="petal" name_original="petals" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>pistils 3, distinct basally, glabrous;</text>
      <biological_entity id="o9216" name="pistil" name_original="pistils" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>styles connate, slender, long;</text>
      <biological_entity id="o9217" name="style" name_original="styles" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="connate" value_original="connate" />
        <character is_modifier="false" name="size" src="d0_s21" value="slender" value_original="slender" />
        <character is_modifier="false" name="length_or_size" src="d0_s21" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma inconspicuous.</text>
      <biological_entity id="o9218" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s22" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Fruits drupes, blackish, ellipsoid;</text>
      <biological_entity constraint="fruits" id="o9219" name="drupe" name_original="drupes" src="d0_s23" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s23" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="shape" src="d0_s23" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>exocarp smooth;</text>
      <biological_entity id="o9220" name="exocarp" name_original="exocarp" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s24" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>mesocarp thin, fleshy;</text>
      <biological_entity id="o9221" name="mesocarp" name_original="mesocarp" src="d0_s25" type="structure">
        <character is_modifier="false" name="width" src="d0_s25" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s25" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>endocarp thin.</text>
      <biological_entity id="o9222" name="endocarp" name_original="endocarp" src="d0_s26" type="structure">
        <character is_modifier="false" name="width" src="d0_s26" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Seeds ellipsoid;</text>
      <biological_entity id="o9223" name="seed" name_original="seeds" src="d0_s27" type="structure">
        <character is_modifier="false" name="shape" src="d0_s27" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>endosperm homogeneous;</text>
      <biological_entity id="o9224" name="endosperm" name_original="endosperm" src="d0_s28" type="structure">
        <character is_modifier="false" name="variability" src="d0_s28" value="homogeneous" value_original="homogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>embryo basal;</text>
      <biological_entity id="o9225" name="embryo" name_original="embryo" src="d0_s29" type="structure">
        <character is_modifier="false" name="position" src="d0_s29" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>eophyll undivided, lanceolate.</text>
    </statement>
    <statement id="d0_s31">
      <text>n = 18.</text>
      <biological_entity id="o9226" name="eophyll" name_original="eophyll" src="d0_s30" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s30" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s30" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="n" id="o9227" name="chromosome" name_original="" src="d0_s31" type="structure">
        <character name="quantity" src="d0_s31" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Fan palm</other_name>
  <other_name type="common_name">palmier evantail de Californie</other_name>
  <discussion>The two species are easy to distinguish when cultivated side by side, but they are sometimes difficult to identify from herbarium specimens. Analysis of flavonoids (S. Zona and R. Scogin 1988) can reliably distinguish the two species.</discussion>
  <discussion>Species 2 (2 in the flora).</discussion>
  <references>
    <reference>Bailey, L. H. 1936. Washingtonia. Gentes Herb. 4: 51–82.</reference>
    <reference>Zona, S. and R. Scogin. 1988. Flavonoid aglycones and C-glycosides of the palm genus Washingtonia (Arecaceae: Coryphoideae). SouthW. Naturalist. 33: 498.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trunk greater than 100 cm diam.</description>
      <determination>1 Washingtonia filifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trunk less than 80 cm diam.</description>
      <determination>2 Washingtonia robusta</determination>
    </key_statement>
  </key>
</bio:treatment>