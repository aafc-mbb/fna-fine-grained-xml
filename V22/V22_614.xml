<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">zingiberaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">zingiber</taxon_name>
    <taxon_name authority="(Linnaeus) Smith" date="1806" rank="species">zerumbet</taxon_name>
    <place_of_publication>
      <publication_title>Exotic Botany</publication_title>
      <place_in_publication>2: 105. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zingiberaceae;genus zingiber;species zerumbet</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200028481</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amomum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">zerumbet</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 1. 1753 F I</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amomum;species zerumbet;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades oblanceolate or narrowly elliptical, 30–32 × 6–7 cm (smaller distally).</text>
      <biological_entity id="o4136" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s0" value="elliptical" value_original="elliptical" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s0" to="32" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s0" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences erect, 7–11 × [3–] 5–6 cm;</text>
      <biological_entity id="o4137" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s1" to="11" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_width" src="d0_s1" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bracts of main axis green when young, becoming red;</text>
      <biological_entity id="o4138" name="bract" name_original="bracts" src="d0_s2" type="structure" constraint="axis" constraint_original="axis; axis">
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <biological_entity constraint="main" id="o4139" name="axis" name_original="axis" src="d0_s2" type="structure" />
      <relation from="o4138" id="r554" name="part_of" negation="false" src="d0_s2" to="o4139" />
    </statement>
    <statement id="d0_s3">
      <text>proximal bracts reniform or very broadly ovate, concave, 2–3 × 3–4 cm, apex broadly rounded;</text>
      <biological_entity constraint="proximal" id="o4140" name="bract" name_original="bracts" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="very broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4141" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>distal bracts smaller but otherwise similar to proximal bracts, ca. 1 × 2 cm.</text>
      <biological_entity constraint="distal" id="o4142" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character constraint="otherwise to proximal bracts" constraintid="o4143" is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character name="length" notes="" src="d0_s4" unit="cm" value="1" value_original="1" />
        <character name="width" notes="" src="d0_s4" unit="cm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o4143" name="bract" name_original="bracts" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: perianth and staminodes pale-yellow.</text>
      <biological_entity id="o4144" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4145" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity id="o4146" name="staminode" name_original="staminodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering fall (Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="fall" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; native, Asia (India and the Malay Peninsula).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Asia (India and the Malay Peninsula)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>In Asia, the rhizomes of Zingiber zerumbet are used as a spice in much the same way as those of Z. officinale. In North America, escaped populations are known only from two sites in Gainesville, Florida.</discussion>
  
</bio:treatment>