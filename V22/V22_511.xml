<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">alismataceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Sagittaria</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 993. 1753; Gen. Pl. ed. 5; 429, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family alismataceae;genus Sagittaria</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin sagitta, arrow</other_info_on_name>
    <other_info_on_name type="fna_id">129016</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="T. Durand" date="unknown" rank="genus">Lophotocarpus</taxon_name>
    <taxon_hierarchy>genus Lophotocarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, rarely annual, submersed, floating-leaved, or emersed, glabrous to sparsely pubescent;</text>
      <biological_entity id="o8447" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="floating-leaved" value_original="floating-leaved" />
        <character is_modifier="false" name="location" src="d0_s0" value="emersed" value_original="emersed" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s0" to="sparsely pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes often present, occasionally terminated by tubers;</text>
      <biological_entity id="o8448" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8449" name="tuber" name_original="tubers" src="d0_s1" type="structure" />
      <relation from="o8448" id="r1120" modifier="occasionally" name="terminated by" negation="false" src="d0_s1" to="o8449" />
    </statement>
    <statement id="d0_s2">
      <text>stolons often present;</text>
      <biological_entity id="o8450" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corms absent;</text>
      <biological_entity id="o8451" name="corm" name_original="corms" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>tubers white to brown, smooth.</text>
      <biological_entity id="o8452" name="tuber" name_original="tubers" src="d0_s4" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s4" to="brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Roots septate.</text>
      <biological_entity id="o8453" name="root" name_original="roots" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="septate" value_original="septate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves sessile or petiolate;</text>
      <biological_entity id="o8454" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole terete to triangular;</text>
      <biological_entity id="o8455" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s7" to="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade with translucent markings absent, linear to obovate, base attenuate to hastate or sagittate, margins entire, apex round to acute.</text>
      <biological_entity id="o8456" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s8" to="obovate" />
      </biological_entity>
      <biological_entity id="o8457" name="marking" name_original="markings" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8458" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s8" to="hastate or sagittate" />
      </biological_entity>
      <biological_entity id="o8459" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8460" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
      <relation from="o8456" id="r1121" name="with" negation="false" src="d0_s8" to="o8457" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences racemes, panicles, rarely umbels, of 1–17 whorls, erect, emersed or floating, rarely submersed;</text>
      <biological_entity constraint="inflorescences" id="o8461" name="raceme" name_original="racemes" src="d0_s9" type="structure" />
      <biological_entity id="o8462" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="location" src="d0_s9" value="emersed" value_original="emersed" />
        <character is_modifier="false" name="location" src="d0_s9" value="floating" value_original="floating" />
        <character is_modifier="false" modifier="rarely" name="location" src="d0_s9" value="submersed" value_original="submersed" />
      </biological_entity>
      <biological_entity id="o8463" name="umbel" name_original="umbels" src="d0_s9" type="structure" />
      <biological_entity id="o8464" name="whorl" name_original="whorls" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="17" />
      </biological_entity>
      <relation from="o8462" id="r1122" name="consist_of" negation="false" src="d0_s9" to="o8464" />
    </statement>
    <statement id="d0_s10">
      <text>bracts coarse or delicate, apex obtuse to acute, smooth or papillose proximally to distally.</text>
      <biological_entity id="o8465" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="coarse" value_original="coarse" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity id="o8466" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="proximally to distally; distally" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers unisexual, the proximal rarely with ring of sterile stamens;</text>
      <biological_entity id="o8467" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8468" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8469" name="ring" name_original="ring" src="d0_s11" type="structure" />
      <biological_entity id="o8470" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o8468" id="r1123" name="with" negation="false" src="d0_s11" to="o8469" />
      <relation from="o8469" id="r1124" name="part_of" negation="false" src="d0_s11" to="o8470" />
    </statement>
    <statement id="d0_s12">
      <text>staminate flowers pedicellate, distal to pistillate flowers;</text>
      <biological_entity id="o8471" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s12" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o8472" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistillate flowers mostly pedicellate, rarely sessile;</text>
      <biological_entity id="o8473" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s13" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>bracts subtending pedicels, lanceolate, shorter than pedicels, apex obtuse to acute;</text>
      <biological_entity id="o8474" name="bract" name_original="bracts" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character constraint="than pedicels" constraintid="o8476" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8475" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o8476" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o8477" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="acute" />
      </biological_entity>
      <relation from="o8474" id="r1125" name="subtending" negation="false" src="d0_s14" to="o8475" />
    </statement>
    <statement id="d0_s15">
      <text>pedicels ascending to recurved;</text>
      <biological_entity id="o8478" name="pedicel" name_original="pedicels" src="d0_s15" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s15" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>receptacle convex;</text>
      <biological_entity id="o8479" name="receptacle" name_original="receptacle" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals recurved in staminate flowers, recurved to erect in pistillate flowers, often sculptured, herbaceous to leathery;</text>
      <biological_entity id="o8480" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character constraint="in flowers" constraintid="o8481" is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
        <character char_type="range_value" constraint="in flowers" constraintid="o8482" from="recurved" name="orientation" notes="" src="d0_s17" to="erect" />
        <character is_modifier="false" modifier="often" name="relief" notes="" src="d0_s17" value="sculptured" value_original="sculptured" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s17" to="leathery" />
      </biological_entity>
      <biological_entity id="o8481" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8482" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals white, rarely with pink spot or tinge, entire;</text>
      <biological_entity id="o8483" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" modifier="with pink spot or tinge" name="architecture_or_shape" src="d0_s18" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens 7–30;</text>
      <biological_entity id="o8484" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s19" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>filaments linear to dilated, glabrous to pubescent;</text>
      <biological_entity id="o8485" name="filament" name_original="filaments" src="d0_s20" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s20" to="dilated" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s20" to="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>pistils to 1500 or more, spirally arranged, not radiating in starlike pattern, distinct;</text>
      <biological_entity id="o8486" name="pistil" name_original="pistils" src="d0_s21" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s21" to="1500" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s21" value="arranged" value_original="arranged" />
        <character constraint="in pattern" constraintid="o8487" is_modifier="false" modifier="not" name="arrangement" src="d0_s21" value="radiating" value_original="radiating" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o8487" name="pattern" name_original="pattern" src="d0_s21" type="structure">
        <character is_modifier="true" name="shape" src="d0_s21" value="starlike" value_original="starlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovules 1;</text>
      <biological_entity id="o8488" name="ovule" name_original="ovules" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>styles terminal.</text>
      <biological_entity id="o8489" name="style" name_original="styles" src="d0_s23" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s23" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Fruits without longitudinal ribs, compressed, abaxially keeled or not, abaxial wings often present, lateral wing often present, 1, curved, glands present.</text>
      <biological_entity id="o8490" name="fruit" name_original="fruits" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s24" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s24" value="keeled" value_original="keeled" />
        <character name="shape" src="d0_s24" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o8491" name="rib" name_original="ribs" src="d0_s24" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s24" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8492" name="wing" name_original="wings" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o8493" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s24" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s24" value="1" value_original="1" />
        <character is_modifier="false" name="course" src="d0_s24" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o8490" id="r1126" name="without" negation="false" src="d0_s24" to="o8491" />
    </statement>
    <statement id="d0_s25">
      <text>x = 11.</text>
      <biological_entity id="o8494" name="gland" name_original="glands" src="d0_s24" type="structure">
        <character is_modifier="false" name="presence" src="d0_s24" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o8495" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly Western Hemisphere; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly Western Hemisphere" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Sagittaire</other_name>
  <discussion>Species ca. 30 (24 in the flora).</discussion>
  <references>
    <reference>Beal, E. O., J. W. Wooten, and R. B. Kaul. 1982. Review of Sagittaria engelmanniana complex (Alismataceae) with environmental correlations. Syst. Bot. 7: 417–432.</reference>
    <reference>Bogin, C. 1955. Revision of the genus Sagittaria (Alismataceae). Mem. New York Bot. Gard. 9: 179–233.</reference>
    <reference>Rataj, K. 1972. Revision of the genus Sagittaria. Part II. (The species of West Indies, Central and South America). Annot. Zool. Bot. 78.</reference>
    <reference>Wooten, J. W. 1973. Taxonomy of seven species of Sagittaria from eastern North America. Brittonia 24: 64–74.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting pedicels recurved or rarely spreading; pistillate sepals mostly erect and closely enclosing flower or fruiting head, occasionally spreading to recurved.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruiting pedicels spreading to ascending or absent; pistillate sepals mostly spreading to recurved, not enclosing flower.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves, at least some, emersed.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves submersed, floating, rarely emersed, or plants stranded on shore.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting heads 1.2–2.1 cm diam.; leaf blade hastate to sagittate</description>
      <determination>3 Sagittaria montevidensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruiting heads 0.7–1.2 cm diam.; leaf blade linear-ovate to lance-elliptic.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Achenes abaxially keeled, mostly more than 2 mm; faces not tuberculate, often with glands; California.</description>
      <determination>5 Sagittaria sanfordii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Achenes not abaxially keeled, mostly less than 2 mm, faces tuberculate, without glands; e of Rocky Mountains.</description>
      <determination>6 Sagittaria platyphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Achene faces tuberculate; floating leaves with sagittate blades present at least on some plants of population.</description>
      <determination>2 Sagittaria guayanensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Achene faces not tuberculate; floating leaves absent or, if present, with unlobed or hastate blades.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruiting pedicels 0.2–1.1 cm; phyllodia lenticular in cross section; tidal muds; mostly brackish waters.</description>
      <determination>1 Sagittaria subulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruiting pedicels 1.5–6.5 cm; phyllodia flattened in cross section; rarely tidal muds; mostly fresh waters.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves with blades and petioles usually present on some plants; some plants often stranded along shore, these usually with expanded leaf blades.</description>
      <determination>7 Sagittaria filiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves all phyllodia; plants almost always submersed, rarely stranded, these without expanded leaf blades.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllodia 50–250 cm; Florida springs.</description>
      <determination>8 Sagittaria kurziana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Phyllodia 12–53 cm; streams and lakes, New Mexico.</description>
      <determination>4 Sagittaria demersa</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Filaments pubescent to tomentulose (except S. fasciculata).</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Filaments glabrous.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves all phyllodia, nearly terete; ne United States.</description>
      <determination>9 Sagittaria teres</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves with blades and petioles or if phyllodia, then flattened to triangular in cross section; mostly widespread.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pistillate flowers sessile to subsessile.</description>
      <determination>16 Sagittaria rigida</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pistillate flowers obviously pedicellate.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Filaments cylindric.</description>
      <determination>14 Sagittaria lancifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Filaments dilated.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Rhizomes present, coarse; stolons and corms absent.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Rhizomes absent or if present, then not coarse; corms and/or stolons present.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Abaxial wing of fruit ± entire; plants widespread</description>
      <determination>13 Sagittaria graminea</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Abaxial wing of fruit scalloped or toothed; ne Alabama</description>
      <determination>12 Sagittaria secundifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Filaments exceeding anthers in length.</description>
      <determination>17 Sagittaria cristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Filaments shorter than or equaling anthers in length.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Blades of emersed leaves 0.5 cm or more wide; w Carolinas.</description>
      <determination>11 Sagittaria fasciculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Blades of emersed leaves, if present, 0.4(–0.5) cm or less wide; se coastal plain.</description>
      <determination>10 Sagittaria isoetiformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Emersed leaf blades linear to ovate.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Emersed leaf blades cordate, sagittate, or hastate.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Bracts papillose.</description>
      <determination>15 Sagittaria papillosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Bracts not papillose.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Emersed plants with erect to ascending petioles; leaf blades lanceolate to ovate.</description>
      <determination>18 Sagittaria ambigua</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Emersed plants with recurved petioles; leaf blades linear to sagittate.</description>
      <determination>19 Sagittaria cuneata</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Bracts distinct or connate much less than ¼ total length.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Bracts connate at least ¼ total length.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Flowers in 2–4 whorls; achenes with facial glands.</description>
      <determination>20 Sagittaria engelmanniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Flowers in 5–12 whorls; achenes without facial glands.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Petiole winged in cross section; achene beak strongly recurved.</description>
      <determination>21 Sagittaria australis</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Petiole ridged in cross section; achene beak ascending apically.</description>
      <determination>22 Sagittaria brevirostra</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Achene beak 1–2 mm, horizontal.</description>
      <determination>24 Sagittaria latifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Achene beak 0.1–0.6 mm, erect or incurved.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Emersed plants with recurved petioles and linear to sagittate blades; basal lobes equal to or shorter than remainder of blade; submersed leaves phyllodial, floating leaves cordate to sagittate</description>
      <determination>19 Sagittaria cuneata</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Emersed plants with ascending to erect petioles and sagittate blades; basal lobes longer than remainder of blade; submersed and floating leaves absent.</description>
      <determination>23 Sagittaria longiloba</determination>
    </key_statement>
  </key>
</bio:treatment>