<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Palisot de Beauvois ex Desvaux" date="unknown" rank="family">eriocaulaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Eriocaulon</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 87. 1753; Gen. Pl. ed. 5; 38, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family eriocaulaceae;genus Eriocaulon</taxon_hierarchy>
    <other_info_on_name type="etymology">derived from Greek erion, wool, and caulos, stalk</other_info_on_name>
    <other_info_on_name type="fna_id">112020</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, often cespitose, rosulate.</text>
      <biological_entity id="o8122" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="rosulate" value_original="rosulate" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots: larger roots unbranched, pale, septate, thickened, spongy.</text>
      <biological_entity id="o8123" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity constraint="larger" id="o8124" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="pale" value_original="pale" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="septate" value_original="septate" />
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="texture" src="d0_s1" value="spongy" value_original="spongy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems rarely sparingly branched, short or elongate.</text>
      <biological_entity id="o8125" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely sparingly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves many ranked in flat or high spiral;</text>
      <biological_entity id="o8126" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character constraint="in flat or high spiral" is_modifier="false" name="arrangement" src="d0_s3" value="ranked" value_original="ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade basally pale, distally greener, linear-attenuate or triangular-acuminate, lingulate, narrowing gradually or abruptly from base, base noticeably lacunate, less distinctly so distally.</text>
      <biological_entity id="o8127" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s4" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s4" value="greener" value_original="greener" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-attenuate" value_original="linear-attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular-acuminate" value_original="triangular-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lingulate" value_original="lingulate" />
        <character constraint="from base" constraintid="o8128" is_modifier="false" modifier="gradually" name="width" src="d0_s4" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o8128" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o8129" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="noticeably" name="relief" src="d0_s4" value="lacunate" value_original="lacunate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths tubular, orifice oblique (often 2–3-cleft);</text>
      <biological_entity id="o8130" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o8131" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o8132" name="orifice" name_original="orifice" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scapes 1–several per rosette, glabrous;</text>
      <biological_entity id="o8133" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o8134" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per rosette" constraintid="o8135" from="1" is_modifier="false" name="quantity" src="d0_s6" to="several" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8135" name="rosette" name_original="rosette" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>heads pale to dark, white, gray, or gray-brown, hemispheric to globose or short-cylindric;</text>
      <biological_entity id="o8136" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o8137" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s7" to="dark white gray or gray-brown" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s7" to="dark white gray or gray-brown" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s7" to="dark white gray or gray-brown" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s7" to="dark white gray or gray-brown" />
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="globose or short-cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>receptacle hairy or glabrous;</text>
      <biological_entity id="o8138" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o8139" name="receptacle" name_original="receptacle" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>involucral-bracts obscured or not obscured by inflorescence, pale to dark, chaffy or scarious;</text>
      <biological_entity id="o8140" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o8141" name="involucral-bract" name_original="involucral-bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="obscured" value_original="obscured" />
        <character constraint="by inflorescence" constraintid="o8142" is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="obscured" value_original="obscured" />
        <character char_type="range_value" from="pale" name="coloration" notes="" src="d0_s9" to="dark" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="chaffy" value_original="chaffy" />
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8142" name="inflorescence" name_original="inflorescence" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>receptacular-bracts narrower, thinner than involucral-bracts, often scarious.</text>
      <biological_entity id="o8143" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <biological_entity id="o8144" name="receptacular-bract" name_original="receptacular-bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="narrower" value_original="narrower" />
        <character constraint="than involucral-bracts" constraintid="o8145" is_modifier="false" name="width" src="d0_s10" value="thinner" value_original="thinner" />
        <character is_modifier="false" modifier="often" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8145" name="involucral-bract" name_original="involucral-bracts" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers mostly with staminate and pistillate on same plants, 2–3-merous;</text>
      <biological_entity id="o8146" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="on plants" constraintid="o8147" is_modifier="false" modifier="with staminate" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="2-3-merous" value_original="2-3-merous" />
      </biological_entity>
      <biological_entity id="o8147" name="plant" name_original="plants" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>sepals 2 (–3), adnate to stipelike base, boatshaped, scarious, apex often covered with multicellular hairs, hairs mealy white or translucent, frequently club-shaped;</text>
      <biological_entity id="o8148" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="3" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="boat-shaped" value_original="boat-shaped" />
        <character is_modifier="false" name="texture" src="d0_s12" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o8149" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <biological_entity id="o8150" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o8151" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o8152" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="mealy" value_original="mealy" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="translucent" value_original="translucent" />
        <character is_modifier="false" modifier="frequently" name="shape" src="d0_s12" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o8150" id="r1073" modifier="often" name="covered with" negation="false" src="d0_s12" to="o8151" />
    </statement>
    <statement id="d0_s13">
      <text>petals 2 (–3), narrower, shorter than sepals, apex hairy, hairs club-shaped, glands adaxial, subapical, dark, rarely pale.</text>
      <biological_entity id="o8153" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="3" />
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="width" src="d0_s13" value="narrower" value_original="narrower" />
        <character constraint="than sepals" constraintid="o8154" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8154" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o8155" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8156" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <biological_entity id="o8157" name="gland" name_original="glands" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="position" src="d0_s13" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark" value_original="dark" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Staminate flowers: androphore apically dilated stalk;</text>
      <biological_entity id="o8158" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8159" name="androphore" name_original="androphore" src="d0_s14" type="structure" />
      <biological_entity id="o8160" name="stalk" name_original="stalk" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="apically" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals separated from sepals by androphore, diverging as lobes from apex;</text>
      <biological_entity id="o8161" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8162" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character constraint="from sepals" constraintid="o8163" is_modifier="false" name="arrangement" src="d0_s15" value="separated" value_original="separated" />
        <character constraint="as lobes" constraintid="o8165" is_modifier="false" name="orientation" notes="" src="d0_s15" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o8163" name="sepal" name_original="sepals" src="d0_s15" type="structure" />
      <biological_entity id="o8164" name="androphore" name_original="androphore" src="d0_s15" type="structure" />
      <biological_entity id="o8165" name="lobe" name_original="lobes" src="d0_s15" type="structure" />
      <biological_entity id="o8166" name="apex" name_original="apex" src="d0_s15" type="structure" />
      <relation from="o8163" id="r1074" name="by" negation="false" src="d0_s15" to="o8164" />
      <relation from="o8165" id="r1075" name="from" negation="false" src="d0_s15" to="o8166" />
    </statement>
    <statement id="d0_s16">
      <text>stamens 3–4 or 6, 2–3 alternating with petals;</text>
      <biological_entity id="o8167" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8168" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s16" to="4" />
        <character name="quantity" src="d0_s16" value="6" value_original="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s16" to="3" />
        <character constraint="with petals" constraintid="o8169" is_modifier="false" name="arrangement" src="d0_s16" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o8169" name="petal" name_original="petals" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>apex of staminal column with 2–3 glands, glands unappendaged;</text>
      <biological_entity id="o8170" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8171" name="apex" name_original="apex" src="d0_s17" type="structure" />
      <biological_entity constraint="staminal" id="o8172" name="column" name_original="column" src="d0_s17" type="structure" />
      <biological_entity id="o8173" name="gland" name_original="glands" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s17" to="3" />
      </biological_entity>
      <biological_entity id="o8174" name="gland" name_original="glands" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="unappendaged" value_original="unappendaged" />
      </biological_entity>
      <relation from="o8171" id="r1076" name="part_of" negation="false" src="d0_s17" to="o8172" />
      <relation from="o8171" id="r1077" name="with" negation="false" src="d0_s17" to="o8173" />
    </statement>
    <statement id="d0_s18">
      <text>filaments arising from androphore rim;</text>
      <biological_entity id="o8175" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8176" name="filament" name_original="filaments" src="d0_s18" type="structure">
        <character constraint="from androphore rim" constraintid="o8177" is_modifier="false" name="orientation" src="d0_s18" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="androphore" id="o8177" name="rim" name_original="rim" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 2-locular, 4-sporangiate, dorsifixed, usually versatile, well exserted at anthesis, jet black (except in E. cinereum).</text>
      <biological_entity id="o8178" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o8179" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s19" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="fixation" src="d0_s19" value="dorsifixed" value_original="dorsifixed" />
        <character is_modifier="false" modifier="usually" name="fixation" src="d0_s19" value="versatile" value_original="versatile" />
        <character constraint="at anthesis" is_modifier="false" modifier="well" name="position" src="d0_s19" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="jet black" value_original="jet black" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Pistillate flowers: gynophore separating petals from sepals, stipelike;</text>
      <biological_entity id="o8180" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8181" name="gynophore" name_original="gynophore" src="d0_s20" type="structure" />
      <biological_entity id="o8182" name="petal" name_original="petals" src="d0_s20" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s20" value="separating" value_original="separating" />
        <character is_modifier="false" name="shape" notes="" src="d0_s20" value="stipelike" value_original="stipelike" />
      </biological_entity>
      <biological_entity id="o8183" name="sepal" name_original="sepals" src="d0_s20" type="structure" />
      <relation from="o8182" id="r1078" name="from" negation="false" src="d0_s20" to="o8183" />
    </statement>
    <statement id="d0_s21">
      <text>pistil 2 (–3) -carpellate;</text>
      <biological_entity id="o8184" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8185" name="pistil" name_original="pistil" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="2(-3)-carpellate" value_original="2(-3)-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>style 1, unappendaged, style-branches 2 (–3).</text>
      <biological_entity id="o8186" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o8187" name="style" name_original="style" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="unappendaged" value_original="unappendaged" />
      </biological_entity>
      <biological_entity id="o8188" name="style-branch" name_original="style-branches" src="d0_s22" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s22" to="3" />
        <character name="quantity" src="d0_s22" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly pantropic, mostly aquatic or on wet, mainly acidic substrates.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly pantropic" establishment_means="native" />
        <character name="distribution" value="mostly aquatic or on wet" establishment_means="native" />
        <character name="distribution" value="mainly acidic substrates" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Pipewort</other_name>
  <other_name type="common_name">button-rods</other_name>
  <other_name type="common_name">hat-pins</other_name>
  <other_name type="common_name">eriocaulon</other_name>
  <discussion>Species ca. 400 (11 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Receptacle and/or base of flowers copiously hairy; some or most receptacular bracts and perianth parts with chalk white hairs; heads white, 5 mm or more in full flower or in fruit.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Receptacle and/or base of flowers glabrous or sparingly hairy; receptacular bracteoles and/or perianth glabrous or hairy, hairs club-shaped, clear or white; heads dark gray or white, usually less than 7.5 mm in full flower or in fruit.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads hard, very slightly flattened when pressed; scape sheaths shorter than most leaves; involucral bracts straw-colored, apex acute; receptacular bracteoles pale, apex narrowly acuminate; pistillate flower petals adaxially glabrescent; terminal cells of club-shaped hairs of perianth whitened, basal cells often uncongested, transparent; plants of moist but seldom aquatic or permanently wet situations</description>
      <determination>5 Eriocaulon decangulare</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads soft, much flattened when pressed; scape sheaths longer than most leaves; involucral bracts gray or dark, apex rounded or obtuse; receptacular bracteoles gray to dark gray, apex acute; pistillate flower petals adaxially villous; all cells of club-shaped hairs on perianth mealy white; plants in aquatic or wet substrates.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mature heads 10–20 mm wide; leaves 5–30 cm; petals of staminate flower conspicuously unequal</description>
      <determination>4 Eriocaulon compressum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mature heads 5–10 mm wide; leaves (1–)2–5(–7) cm; petals of staminate flower nearly equal</description>
      <determination>6 Eriocaulon texense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 6, pistil 3-carpellate.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens 4, pistil 2-carpellate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Anthers yellow; apex of receptacular bracteoles acute</description>
      <determination>10 Eriocaulon cinereum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Anthers black, apex of receptacular bracteoles obtuse</description>
      <determination>11 Eriocaulon microcephalum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads 4–10 mm wide at maturity; outer involucral bracts usually reflexed, obscured by bracteoles and flowers.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Heads seldom as wide as 5 mm; outer involucral bracts not reflexed, not obscured by bracteoles and flowers.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">All bracts of staminate and pistillate flowers straw-colored or pale with grayish midzone; sepals of pistillate flowers basally pale, darkening distally to grayish, gray-green, or gray-brown; heads (young or mature) very pale; seeds faintly rectangular-reticulate, often papillate in lines; s coastal plain</description>
      <determination>7 Eriocaulon lineare</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Inner involucral bracts, receptacular bracts, and sepals darkened, usually gray to near black; young heads dark; seeds very faintly reticulate, not papillate; n and/or montane</description>
      <determination>8 Eriocaulon aquaticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Bracts straw-colored, greenish, or light gray to gray, dull, margins often erose or lacerate, apex blunt to obtuse; scapes linear; plants of brackish substrates</description>
      <determination>2 Eriocaulon parkeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Bracts dark, gray to blackish, very lustrous, margins all nearly entire (except Eriocaulon nigrobracteatum), apex acute; scapes filiform; plants of acidic substrates.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracts narrowly ovate to oblong or spatulate, apex acute; bracts and perianth parts (except petals in some cases) glabrous; seed conspicuously pale-reticulate</description>
      <determination>1 Eriocaulon ravenelii</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracts wider in outline, apex rounded or apiculate; bracts (margins and apex) and perianth hairy; seed not pale-reticulate.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals of pistillate flowers stipitate, suborbiculate-rhombic; outer involucral bracts straw-colored, inner and receptacular bracts dark gray, gray-green, or gray-brown</description>
      <determination>3 Eriocaulon koernickianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals of pistillate flowers short-stipitate or nearly sessile, oblong; involucral and receptacular bracts blackish or with pale base</description>
      <determination>9 Eriocaulon nigrobracteatum</determination>
    </key_statement>
  </key>
</bio:treatment>