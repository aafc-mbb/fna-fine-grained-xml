<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Juss." date="unknown" rank="family">araceae</taxon_name>
    <taxon_name authority="Schott in H. W. Schott and S. L. Endlicher" date="1832" rank="genus">Colocasia</taxon_name>
    <place_of_publication>
      <publication_title>in H. W. Schott and S. L. Endlicher,Meletemata Botanica</publication_title>
      <place_in_publication>18. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family araceae;genus Colocasia</taxon_hierarchy>
    <other_info_on_name type="etymology">classical Greek name derived from an old Middle Eastern name colcas or culcas</other_info_on_name>
    <other_info_on_name type="fna_id">107697</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, wetland [or terrestrial].</text>
      <biological_entity id="o15195" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stolons with nodes produced at or near surface;</text>
      <biological_entity id="o15196" name="stolon" name_original="stolons" src="d0_s1" type="structure" />
      <biological_entity id="o15197" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o15198" name="surface" name_original="surface" src="d0_s1" type="structure" />
      <relation from="o15196" id="r1994" name="with" negation="false" src="d0_s1" to="o15197" />
      <relation from="o15197" id="r1995" name="produced at" negation="false" src="d0_s1" to="o15198" />
    </statement>
    <statement id="d0_s2">
      <text>corms underground [aboveground], tuberous.</text>
      <biological_entity id="o15199" name="corm" name_original="corms" src="d0_s2" type="structure">
        <character is_modifier="false" name="location" src="d0_s2" value="underground" value_original="underground" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves appearing before flowers, several, clustered apically, erect;</text>
      <biological_entity id="o15200" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" modifier="apically" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o15201" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <relation from="o15200" id="r1996" name="appearing before" negation="false" src="d0_s3" to="o15201" />
    </statement>
    <statement id="d0_s4">
      <text>petiole usually longer than blade;</text>
      <biological_entity id="o15202" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="than blade" constraintid="o15203" is_modifier="false" name="length_or_size" src="d0_s4" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o15203" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade green to dark green or glaucous blue-green adaxially, simple, peltate, ovate or sagittate-cordate, basal lobes rounded, apex mucronate;</text>
      <biological_entity id="o15204" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" modifier="adaxially" name="coloration" src="d0_s5" to="dark green or glaucous blue-green" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sagittate-cordate" value_original="sagittate-cordate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15205" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15206" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary lateral-veins parallel, secondary lateral-veins netted.</text>
      <biological_entity constraint="primary" id="o15207" name="lateral-vein" name_original="lateral-veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o15208" name="lateral-vein" name_original="lateral-veins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s6" value="netted" value_original="netted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences: peduncle erect, shorter than leaves, apex not swollen;</text>
      <biological_entity id="o15209" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o15210" name="peduncle" name_original="peduncle" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character constraint="than leaves" constraintid="o15211" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15211" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o15212" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>spathe tube green;</text>
      <biological_entity id="o15213" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity constraint="spathe" id="o15214" name="tube" name_original="tube" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>spathe blade orange, opening basally and reflexing apically at anthesis to expose spadix;</text>
      <biological_entity id="o15215" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity constraint="spathe" id="o15216" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character constraint="at spadix" constraintid="o15217" is_modifier="false" modifier="basally" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
      <biological_entity id="o15217" name="spadix" name_original="spadix" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="anthesis" value_original="anthesis" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>spadix slender, tapering, usually terminated by sterile appendage.</text>
      <biological_entity id="o15218" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure" />
      <biological_entity id="o15219" name="spadix" name_original="spadix" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o15220" name="appendage" name_original="appendage" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o15219" id="r1997" modifier="usually" name="terminated by" negation="false" src="d0_s10" to="o15220" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers unisexual, staminate and pistillate on same plant;</text>
      <biological_entity id="o15221" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character constraint="on plant" constraintid="o15222" is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15222" name="plant" name_original="plant" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pistillate flowers covering base of spadix, staminate flowers apical, sterile flowers between pistillate and staminate flowers;</text>
      <biological_entity id="o15223" name="flower" name_original="flowers" src="d0_s12" type="structure" constraint="spadix" constraint_original="spadix; spadix">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o15224" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o15225" name="spadix" name_original="spadix" src="d0_s12" type="structure" />
      <biological_entity id="o15226" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position" src="d0_s12" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity constraint="between flowers" constraintid="o15228" id="o15227" name="flower" name_original="flowers" src="d0_s12" type="structure" constraint_original="between  flowers, ">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o15228" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o15223" id="r1998" name="covering" negation="false" src="d0_s12" to="o15224" />
      <relation from="o15223" id="r1999" name="part_of" negation="false" src="d0_s12" to="o15225" />
    </statement>
    <statement id="d0_s13">
      <text>perianth absent.</text>
      <biological_entity id="o15229" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits greenish to whitish or red.</text>
      <biological_entity id="o15230" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s14" to="whitish or red" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 0–5 (–35), mucilage probably present.</text>
      <biological_entity id="o15231" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="35" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>x = 7.</text>
      <biological_entity id="o15232" name="mucilage" name_original="mucilage" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="probably" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o15233" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Southeastern Asia, 1 species cultivated and escaping in the tropics and subtropics worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Southeastern Asia" establishment_means="native" />
        <character name="distribution" value="1 species cultivated and escaping in the tropics and subtropics worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Taro</other_name>
  <other_name type="common_name">dasheen</other_name>
  <discussion>Species in the genus Colocasia have received little attention except for C. esculenta, commonly called taro, which is cultivated throughout the tropics and subtropics for its starchy, edible corms. The origin of the cultivated species is uncertain, but all other species in the genus occur in northeastern India and southeastern Asia. Prior to modernization, taro was especially important on the Pacific Islands. Hawaii was a main center of taro cultivation, and the crop played an important role in native culture. About 150 varieties of C. esculenta were developed on Hawaii, including ones those specifically grown for poi, a fermented paste made from crushed, cooked corms (A. B. Greenwell 1947).</discussion>
  <discussion>Colocasia esculenta was probably brought to the Caribbean and North America from Africa as part of the slave trade. In the southeastern United States, taro was commonly cultivated in the kitchen gardens of slaves and their free descendants (W. Bartram 1791). In the early 1900s, the U.S. Department of Agriculture attempted a campaign to introduce taro as a new root crop in frost-free zones in the southern United States (O. W. Barrett and O. F. Cook 1910). Promotional literature, which included cultivation techniques and recipes for use, was distributed to encourage farmers to try this new crop, but taro was never accepted as a substitute for potatoes (R. A. Young 1936).</discussion>
  <discussion>Plants of Colocasia esculenta are known by many common names, including taro, cocoyam, dasheen, eddo, malanga, tannia, and others. Many of these same names are also applied to species of Xanthosoma, a New World aroid also cultivated for its starchy corms, which is often confused with Colocasia (see S. K. O’Hair and M. P. Asokan 1986 for a review of edible aroids). Edible taxa in the two genera can be readily distinguished by the peltate leaves in Colocasia and absence of a sterile tip on the spadix in Xanthosoma.</discussion>
  <discussion>Species 7 (1 in the flora).</discussion>
  <references>
    <reference>Matthews, P. 1991. A possible tropical wildtype taro: Colocasia esculenta var. aquatilis. Bull. Indo-Pacific Prehist. Assoc. 11: 69–81.</reference>
    <reference>Wang J.-K. and S. Higa, eds. 1983. Taro, a Review of Colocasia esculenta and Its Potentials. Honolulu.</reference>
  </references>
  
</bio:treatment>