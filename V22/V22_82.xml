<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schultz Schultzenstein" date="unknown" rank="family">arecaceae</taxon_name>
    <taxon_name authority="Griffith" date="1844" rank="subfamily">CORYPHOIDEAE</taxon_name>
    <taxon_name authority="Drude in C. F. P. von Martius et al." date="1881" rank="tribe">PHOENICEAE</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">Phoenix</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1188. 1753; Gen. Pl. ed. 5, 496. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family arecaceae;subfamily coryphoideae;tribe phoeniceae;genus phoenix;</taxon_hierarchy>
    <other_info_on_name type="etymology">derivation uncertain, perhaps for the Phoenicians, known for a dye that was similar in color to ripening dates; name used by Theophrastus for the date palm</other_info_on_name>
    <other_info_on_name type="fna_id">125080</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Adanson" date="unknown" rank="genus">Dachel</taxon_name>
    <taxon_hierarchy>genus Dachel;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Elate</taxon_name>
    <taxon_hierarchy>genus Elate;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Miller" date="unknown" rank="genus">Palma</taxon_name>
    <taxon_hierarchy>genus Palma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems solitary or clustered, erect or ascending [subterranean], slender to massive, often clothed in old leaf-bases.</text>
      <biological_entity id="o7010" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="slender" name="size" src="d0_s0" to="massive" />
      </biological_entity>
      <biological_entity id="o7011" name="leaf-base" name_original="leaf-bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <relation from="o7010" id="r927" modifier="often" name="clothed in" negation="false" src="d0_s0" to="o7011" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: sheath fibers soft;</text>
      <biological_entity id="o7012" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="sheath" id="o7013" name="fiber" name_original="fibers" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s1" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole not split at base, armed, base not split, not forming crownshaft;</text>
      <biological_entity id="o7014" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7015" name="petiole" name_original="petiole" src="d0_s2" type="structure" />
      <biological_entity id="o7016" name="split" name_original="split" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="armed" value_original="armed" />
      </biological_entity>
      <biological_entity id="o7017" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o7018" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o7019" name="split" name_original="split" src="d0_s2" type="structure" />
      <biological_entity id="o7020" name="crownshaft" name_original="crownshaft" src="d0_s2" type="structure" />
      <relation from="o7016" id="r928" name="at" negation="false" src="d0_s2" to="o7017" />
      <relation from="o7018" id="r929" name="forming" negation="true" src="d0_s2" to="o7020" />
    </statement>
    <statement id="d0_s3">
      <text>blade pinnate;</text>
      <biological_entity id="o7021" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7022" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>plication induplicate;</text>
      <biological_entity id="o7023" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7024" name="plication" name_original="plication" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="induplicate" value_original="induplicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>segments lanceolate, in 1 or more planes;</text>
      <biological_entity id="o7025" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7026" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apices acute;</text>
      <biological_entity id="o7027" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o7028" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal segments modified into stout spines.</text>
      <biological_entity id="o7029" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o7030" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character constraint="into spines" constraintid="o7031" is_modifier="false" name="development" src="d0_s7" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity id="o7031" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character is_modifier="true" name="fragility_or_size" src="d0_s7" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences axillary within crown of leaves, paniculate, ascending, much shorter than leaves, with 1 order of branching, alike in staminate and pistillate plants;</text>
      <biological_entity id="o7032" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character constraint="within crown" constraintid="o7033" is_modifier="false" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s8" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="than leaves" constraintid="o7035" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o7033" name="crown" name_original="crown" src="d0_s8" type="structure" />
      <biological_entity id="o7034" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o7035" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o7036" name="order" name_original="order" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7037" name="plant" name_original="plants" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="branching" value_original="branching" />
        <character is_modifier="true" name="variability" src="d0_s8" value="alike" value_original="alike" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o7033" id="r930" name="part_of" negation="false" src="d0_s8" to="o7034" />
      <relation from="o7032" id="r931" name="with" negation="false" src="d0_s8" to="o7036" />
      <relation from="o7036" id="r932" name="part_of" negation="false" src="d0_s8" to="o7037" />
    </statement>
    <statement id="d0_s9">
      <text>prophyll often caducous, conspicuous, becoming boatshaped, short;</text>
      <biological_entity id="o7038" name="prophyll" name_original="prophyll" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="duration" src="d0_s9" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s9" value="boat-shaped" value_original="boat-shaped" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peduncular bracts absent;</text>
      <biological_entity constraint="peduncular" id="o7039" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>rachillae glabrous.</text>
      <biological_entity id="o7040" name="rachilla" name_original="rachillae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers borne singly along rachillae;</text>
      <biological_entity id="o7041" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7042" name="rachilla" name_original="rachillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o7041" id="r933" name="borne" negation="false" src="d0_s12" to="o7042" />
    </statement>
    <statement id="d0_s13">
      <text>calyx cupulate, 3-lobed;</text>
      <biological_entity id="o7043" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals 3, free, valvate;</text>
      <biological_entity id="o7044" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s14" value="valvate" value_original="valvate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 6, free;</text>
      <biological_entity id="o7045" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pistillode inconspicuous or absent.</text>
      <biological_entity id="o7046" name="pistillode" name_original="pistillode" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pistillate flowers borne singly on rachillae;</text>
      <biological_entity id="o7047" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7048" name="rachilla" name_original="rachillae" src="d0_s17" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s17" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o7047" id="r934" name="borne" negation="false" src="d0_s17" to="o7048" />
    </statement>
    <statement id="d0_s18">
      <text>calyx cupulate, 3-lobed;</text>
      <biological_entity id="o7049" name="calyx" name_original="calyx" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>petals 3, imbricate, free;</text>
      <biological_entity id="o7050" name="petal" name_original="petals" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s19" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>staminodial ring cupulate or deeply 6-lobed;</text>
      <biological_entity constraint="staminodial" id="o7051" name="ring" name_original="ring" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s20" value="6-lobed" value_original="6-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>pistils 3 (only 1 developing), distinct;</text>
      <biological_entity id="o7052" name="pistil" name_original="pistils" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigmas small.</text>
      <biological_entity id="o7053" name="stigma" name_original="stigmas" src="d0_s22" type="structure">
        <character is_modifier="false" name="size" src="d0_s22" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Fruits drupes, berrylike, fleshy;</text>
      <biological_entity constraint="fruits" id="o7054" name="drupe" name_original="drupes" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="berrylike" value_original="berrylike" />
        <character is_modifier="false" name="texture" src="d0_s23" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>exocarp blackish brown, smooth;</text>
      <biological_entity id="o7055" name="exocarp" name_original="exocarp" src="d0_s24" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s24" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s24" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>mesocarp fleshy or fibrous;</text>
      <biological_entity id="o7056" name="mesocarp" name_original="mesocarp" src="d0_s25" type="structure">
        <character is_modifier="false" name="texture" src="d0_s25" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="texture" src="d0_s25" value="fibrous" value_original="fibrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>endocarp papery.</text>
      <biological_entity id="o7057" name="endocarp" name_original="endocarp" src="d0_s26" type="structure">
        <character is_modifier="false" name="texture" src="d0_s26" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Seeds 1, elongate;</text>
      <biological_entity id="o7058" name="seed" name_original="seeds" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s27" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>endosperm homogeneous;</text>
      <biological_entity id="o7059" name="endosperm" name_original="endosperm" src="d0_s28" type="structure">
        <character is_modifier="false" name="variability" src="d0_s28" value="homogeneous" value_original="homogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>embryo lateral [basal];</text>
      <biological_entity id="o7060" name="embryo" name_original="embryo" src="d0_s29" type="structure">
        <character is_modifier="false" name="position" src="d0_s29" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>eophyll undivided, lanceolate.</text>
    </statement>
    <statement id="d0_s31">
      <text>xn = 18.</text>
      <biological_entity id="o7061" name="eophyll" name_original="eophyll" src="d0_s30" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s30" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s30" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; widespread, native to Eastern Hemisphere, including the Canary and Cape Verde iIslands, s Europe, Africa (including Madagascar), s Asia, and Philippines.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="widespread" establishment_means="introduced" />
        <character name="distribution" value="native to Eastern Hemisphere" establishment_means="introduced" />
        <character name="distribution" value="including the Canary and Cape Verde iIslands" establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="introduced" />
        <character name="distribution" value="Africa (including Madagascar)" establishment_means="introduced" />
        <character name="distribution" value="s Asia" establishment_means="introduced" />
        <character name="distribution" value="and Philippines" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Date palm</other_name>
  <other_name type="common_name">palmier dattier</other_name>
  <discussion>Several species of Phoenix are cultivated as ornamentals in Florida and California, although identification can be difficult since because the species are dioecious and apparently hybridize with great ease. Phoenix dactylifera Linnaeus, the date palm, is grown as a commercial crop in southern California and Arizona and as an ornamental palm in Florida, but it seems noninvasive. It can be recognized by its massive trunk (eventually bearing basal offshoots) and its stiff, ascending, glaucous leaves. In Florida, P. roebelenii O'Brien (pygmy date palm), with its solitary trunk less than 15 cm in diam., is also cultivated as an ornamental although it does not seem to escape. Other species of Phoenix are occasionally cultivated in warm parts of the United States. Elements of cultivated species of Phoenix entering the flora may be of uncertain parentage.</discussion>
  <discussion>Two species, Phoenix canariensis and P. reclinata, have escaped and are sporadically naturalized in southern Florida and, to a much lesser extent, in California. Phoenix dactylifera is reportedly naturalized in California (E. McClintock 1993), but I have seen no specimens.</discussion>
  <discussion>Species 137 (2 in the flora).</discussion>
  <references>
    <reference>Austin, D. F. 1978. Exotic plants and their effects in southeastern Florida. Environm. Conservation 5: 25–34.</reference>
    <reference>Barrow, S. C. 1998. A monograph of Phoenix L. (Palmae: Coryphoideae). Kew Bull. 53: 513–545.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trunk solitary, 55–70 cm diam</description>
      <determination>1 Phoenix canariensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trunks multiple, 10–15 cm diam</description>
      <determination>2 Phoenix reclinata</determination>
    </key_statement>
  </key>
</bio:treatment>