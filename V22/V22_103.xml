<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert R. Haynes</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">Najadaceae</taxon_name>
    <taxon_hierarchy>family Najadaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10603</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, rarely perennial, not rhizomatous, caulescent;</text>
      <biological_entity id="o1114" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="caulescent" value_original="caulescent" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>turions absent.</text>
      <biological_entity id="o1115" name="turion" name_original="turions" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves nearly opposite or appearing whorled, submersed, sessile;</text>
      <biological_entity id="o1116" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character name="arrangement" src="d0_s2" value="appearing" value_original="appearing" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="location" src="d0_s2" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath not persisting longer than blade, not leaving scar when shed, not ligulate, not auriculate or rarely auriculate;</text>
      <biological_entity id="o1117" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s3" value="persisting" value_original="persisting" />
        <character constraint="than blade" constraintid="o1118" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1118" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o1119" name="scar" name_original="scar" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not; when shed; not" name="architecture" src="d0_s3" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <relation from="o1117" id="r150" name="leaving" negation="true" src="d0_s3" to="o1119" />
    </statement>
    <statement id="d0_s4">
      <text>intravaginal squamules scales, 2, linear.</text>
      <biological_entity constraint="squamules" id="o1120" name="scale" name_original="scales" src="d0_s4" type="structure" constraint_original="intravaginal squamules">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary, solitary flowers or cymes, often subtended by spathe as involucre, sessile or short-pedunculate.</text>
      <biological_entity id="o1121" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o1122" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-pedunculate" value_original="short-pedunculate" />
      </biological_entity>
      <biological_entity id="o1123" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-pedunculate" value_original="short-pedunculate" />
      </biological_entity>
      <biological_entity id="o1124" name="spathe" name_original="spathe" src="d0_s5" type="structure" />
      <biological_entity id="o1125" name="involucre" name_original="involucre" src="d0_s5" type="structure" />
      <relation from="o1122" id="r151" modifier="often" name="subtended by" negation="false" src="d0_s5" to="o1124" />
      <relation from="o1122" id="r152" modifier="often" name="subtended by" negation="false" src="d0_s5" to="o1125" />
      <relation from="o1123" id="r153" modifier="often" name="subtended by" negation="false" src="d0_s5" to="o1124" />
      <relation from="o1123" id="r154" modifier="often" name="subtended by" negation="false" src="d0_s5" to="o1125" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers unisexual, staminate and pistillate on same or separate plants;</text>
      <biological_entity id="o1127" name="plant" name_original="plants" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="separate" value_original="separate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>subtending bracts absent;</text>
      <biological_entity id="o1126" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o1127" is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1128" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <relation from="o1126" id="r155" name="subtending" negation="false" src="d0_s7" to="o1128" />
    </statement>
    <statement id="d0_s8">
      <text>perianth absent;</text>
      <biological_entity id="o1129" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 1;</text>
      <biological_entity id="o1130" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers dehiscing irregularly;</text>
      <biological_entity id="o1131" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s10" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pollen spheric;</text>
      <biological_entity id="o1132" name="pollen" name_original="pollen" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistils 1, not stipitate;</text>
      <biological_entity id="o1133" name="pistil" name_original="pistils" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules basal, anatropous.</text>
      <biological_entity id="o1134" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="basal" value_original="basal" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="anatropous" value_original="anatropous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits achenelike.</text>
      <biological_entity id="o1135" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="achenelike" value_original="achenelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 1;</text>
      <biological_entity id="o1136" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryo straight.</text>
      <biological_entity id="o1137" name="embryo" name_original="embryo" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>197</number>
  <other_name type="common_name">Naiad Family</other_name>
  <other_name type="common_name">or Water-nymph Family</other_name>
  <discussion>Genus Genera 1, species 40 (8 species in the flora).</discussion>
  <references>
    <reference>Haynes, R. R. 1977. The Najadaceae in the southeastern United States. J. Arnold Arbor. 58: 161–170.</reference>
    <reference>Thorne, R. F. 1993c. Hydrocharitaceae. In: J. C. Hickman, ed. 1993. The Jepson Manual: Higher Plants of California. Berkeley, Los Angeles, and London. Pp. 1150–1151.</reference>
    <reference>Shaffer-Fehre, M. 1991. The endotegmen tuberculae: An account of little-known structures from the seed coat of the Hydrocharitoideae (Hydrocharitaceae) and Najas (Najadaceae). Bot. J. Linn. Soc. 107: 169–188.</reference>
    <reference>Shaffer-Fehre, M. 1991b. The position of Najas within the subclass Alismatidae (Monocotyledones) in the light of new evidence from seed coat structures in the Hydrocharitoideae (Hydrocharitales). Bot. J. Linn. Soc. 107: 189–209.</reference>
  </references>
  
</bio:treatment>