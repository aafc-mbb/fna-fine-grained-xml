<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Luzula</taxon_name>
    <taxon_name authority="Kirschner" date="1990" rank="species">pallidula</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>39: 110. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus luzula;species pallidula;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">222000238</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Wahlenberg" date="unknown" rank="species">pallescens</taxon_name>
    <taxon_hierarchy>genus Juncus;species pallescens;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="(Wahlenberg) Besser" date="unknown" rank="species">pallescens</taxon_name>
    <taxon_hierarchy>genus Luzula;species pallescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes thickened.</text>
      <biological_entity id="o7966" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s0" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms cespitose, 9–35 cm.</text>
      <biological_entity id="o7967" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal leaves 6–11.5 cm × 1.5–4 mm, apex not callous, sparingly ciliate.</text>
      <biological_entity id="o7968" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o7969" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7970" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="callous" value_original="callous" />
        <character is_modifier="false" modifier="sparingly" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences umbellate-paniculate;</text>
      <biological_entity id="o7971" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="umbellate-paniculate" value_original="umbellate-paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>glomerules 4–30 (each with 9–24 flowers), central glomerules sessile or nearly sessile, cylindric, 6–10 × 4 mm;</text>
      <biological_entity id="o7972" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="30" />
      </biological_entity>
      <biological_entity constraint="central" id="o7973" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character name="width" src="d0_s4" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches straight, erect, to 3 cm;</text>
      <biological_entity id="o7974" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal inflorescence bract conspicuous, leaflike, equal to much longer than inflorescence;</text>
      <biological_entity constraint="inflorescence" id="o7975" name="bract" name_original="bract" src="d0_s6" type="structure" constraint_original="proximal inflorescence">
        <character is_modifier="false" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="variability" src="d0_s6" value="equal" value_original="equal" />
        <character constraint="than inflorescence" constraintid="o7976" is_modifier="false" name="length_or_size" src="d0_s6" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o7976" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts clear, sometimes variegated with purple;</text>
      <biological_entity id="o7977" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="clear" value_original="clear" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="variegated with purple" value_original="variegated with purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteole margins dentate to lacerate.</text>
      <biological_entity constraint="bracteole" id="o7978" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s8" to="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: tepals clear to straw-colored throughout or centers brown with clear margins and apex, 1.5–2.6 mm;</text>
      <biological_entity id="o7979" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o7980" name="tepal" name_original="tepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="clear" modifier="throughout" name="coloration" src="d0_s9" to="straw-colored" />
      </biological_entity>
      <biological_entity id="o7981" name="center" name_original="centers" src="d0_s9" type="structure">
        <character constraint="with apex" constraintid="o7983" is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7982" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity id="o7983" name="apex" name_original="apex" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>outer whorl exceeding inner whorl, (outer whorl apex awned);</text>
      <biological_entity id="o7984" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="outer" id="o7985" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o7986" name="whorl" name_original="whorl" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s10" value="exceeding" value_original="exceeding" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers equaling to 1.5 times filament length.</text>
      <biological_entity id="o7987" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7988" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules light or dark reddish, shining, spheric, usually equaling inner tepal whorl.</text>
      <biological_entity id="o7989" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="shining" value_original="shining" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spheric" value_original="spheric" />
      </biological_entity>
      <biological_entity constraint="tepal" id="o7990" name="whorl" name_original="whorl" src="d0_s12" type="structure" constraint_original="inner tepal">
        <character is_modifier="true" modifier="usually" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds translucent brown, ellipsoid, 0.7–1 mm;</text>
      <biological_entity id="o7991" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="translucent brown" value_original="translucent brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>caruncle 0.2–0.3 mm. 2n = 12.</text>
      <biological_entity id="o7992" name="caruncle" name_original="caruncle" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7993" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting early–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="early" />
        <character name="fruiting time" char_type="range_value" to="late summer" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to wet woods, grassy places, and clearings on rocky places and barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to wet woods" />
        <character name="habitat" value="grassy places" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="rocky places" modifier="on" />
        <character name="habitat" value="barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., Nfld. and Labr. (Nfld.), Que.; N.Y., Vt.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16</number>
  <discussion>Basal leaves of Luzula pallidula are sparingly ciliate.</discussion>
  <discussion>For discussion of the change of the widely known name for this species, see J. Kirschner (1990).</discussion>
  
</bio:treatment>