<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Banks &amp; Solander ex K. D. König" date="1805" rank="genus">Thalassia</taxon_name>
    <place_of_publication>
      <publication_title>Annals of Botany</publication_title>
      <place_in_publication>2: 96. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus Thalassia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek thalass, sea</other_info_on_name>
    <other_info_on_name type="fna_id">132680</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, of marine waters.</text>
      <biological_entity id="o4399" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes present;</text>
      <biological_entity id="o4400" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaf-bearing branches arising from rhizomes at distances of several internodes;</text>
      <biological_entity id="o4401" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="leaf-bearing" value_original="leaf-bearing" />
        <character constraint="from rhizomes" constraintid="o4402" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o4402" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure" />
      <biological_entity id="o4403" name="distance" name_original="distances" src="d0_s2" type="structure" />
      <biological_entity id="o4404" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="several" value_original="several" />
      </biological_entity>
      <relation from="o4402" id="r586" name="at" negation="false" src="d0_s2" to="o4403" />
      <relation from="o4403" id="r587" name="part_of" negation="false" src="d0_s2" to="o4404" />
    </statement>
    <statement id="d0_s3">
      <text>stolons absent.</text>
      <biological_entity id="o4405" name="stolon" name_original="stolons" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Erect stems rooted in substrate, unbranched, short.</text>
      <biological_entity id="o4406" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character constraint="in substrate" constraintid="o4407" is_modifier="false" name="architecture" src="d0_s4" value="rooted" value_original="rooted" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o4407" name="substrate" name_original="substrate" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves 2–6, basal, submersed, sessile;</text>
      <biological_entity id="o4408" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4409" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="location" src="d0_s5" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade linear, base tapering to stem;</text>
      <biological_entity id="o4410" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o4411" name="base" name_original="base" src="d0_s6" type="structure">
        <character constraint="to stem" constraintid="o4412" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o4412" name="stem" name_original="stem" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>apex obtuse;</text>
      <biological_entity id="o4413" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>midvein without lacunae along side (s), blade uniform in color throughout;</text>
      <biological_entity id="o4414" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o4415" name="lacuna" name_original="lacunae" src="d0_s8" type="structure" />
      <biological_entity id="o4416" name="side" name_original="side" src="d0_s8" type="structure" />
      <relation from="o4414" id="r588" name="without" negation="false" src="d0_s8" to="o4415" />
      <relation from="o4415" id="r589" name="along" negation="false" src="d0_s8" to="o4416" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial surfacely without prickles;</text>
      <biological_entity id="o4417" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s8" value="uniform" value_original="uniform" />
        <character constraint="without prickles" constraintid="o4418" is_modifier="false" name="position" src="d0_s9" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity id="o4418" name="prickle" name_original="prickles" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>intravaginal scales entire.</text>
      <biological_entity constraint="intravaginal" id="o4419" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences 1-flowered to cymose, pedunculate;</text>
      <biological_entity id="o4420" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character char_type="range_value" from="1-flowered" name="architecture" src="d0_s11" to="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>spathes not winged.</text>
      <biological_entity id="o4421" name="spathe" name_original="spathes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers unisexual, staminate and pistillate on different plants, submersed, short-pedicellate to nearly sessile;</text>
      <biological_entity id="o4422" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o4423" is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="location" notes="" src="d0_s13" value="submersed" value_original="submersed" />
        <character char_type="range_value" from="short-pedicellate" name="architecture" src="d0_s13" to="nearly sessile" />
      </biological_entity>
      <biological_entity id="o4423" name="plant" name_original="plants" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>petals absent.</text>
      <biological_entity id="o4424" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Staminate flowers: filaments distinct;</text>
      <biological_entity id="o4425" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4426" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers linear;</text>
      <biological_entity id="o4427" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4428" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pollen embedded in gelatinous matrix, in moniliform chains.</text>
      <biological_entity id="o4429" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o4430" name="pollen" name_original="pollen" src="d0_s17" type="structure" />
      <biological_entity id="o4431" name="matrix" name_original="matrix" src="d0_s17" type="structure">
        <character is_modifier="true" name="texture" src="d0_s17" value="gelatinous" value_original="gelatinous" />
      </biological_entity>
      <biological_entity id="o4432" name="chain" name_original="chains" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="moniliform" value_original="moniliform" />
      </biological_entity>
      <relation from="o4430" id="r590" name="embedded in" negation="false" src="d0_s17" to="o4431" />
      <relation from="o4430" id="r591" name="in" negation="false" src="d0_s17" to="o4432" />
    </statement>
    <statement id="d0_s18">
      <text>Pistillate flowers: ovary 1-locular;</text>
      <biological_entity id="o4433" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4434" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s18" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles 6–8, not 2-fid.</text>
      <biological_entity id="o4435" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4436" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s19" to="8" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s19" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits spheric, echinate, dehiscing into 6–8 irregular valves.</text>
      <biological_entity id="o4437" name="fruit" name_original="fruits" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="echinate" value_original="echinate" />
        <character constraint="into valves" constraintid="o4438" is_modifier="false" name="dehiscence" src="d0_s20" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o4438" name="valve" name_original="valves" src="d0_s20" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s20" to="8" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s20" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds pyriform;</text>
      <biological_entity id="o4439" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="pyriform" value_original="pyriform" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>seed-coat ephemeral.</text>
      <biological_entity id="o4440" name="seed-coat" name_original="seed-coat" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Central America, Africa, Asia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Turtle-grass</other_name>
  <discussion>Species 2 (1 in flora).</discussion>
  <references>
    <reference>Grey, W. F. and M. D. Moffler. 1978. Flowering of the seagrass Thalassia testudinum (Hydrocharitaceae) in the Tampa Bay, Florida area. Aquatic Bot. 5: 251–259.</reference>
    <reference>Moffler, M. D., M. J. Durako, and W. F. Grey. 1981. Observations on the reproductive ecology of Thalassia testudinum (Hydrocharitaceae) in Tampa Bay, Florida. Aquatic Bot. 10: 183–187.</reference>
    <reference>Moore, D. R. 1963. Distribution of the sea grass, Thalassia, in the United States. Bull. Mar. Sci. Gulf Caribbean 13: 329–342.</reference>
    <reference>Orpurt, P. R. and L. L. Boral. 1964. The flowers and seeds of Thalassia testudinum K&amp;ouml;nig. Bull. Mar. Sci. Gulf Caribbean 14: 296–302.</reference>
  </references>
  
</bio:treatment>