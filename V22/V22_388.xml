<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Poiophylli</taxon_name>
    <taxon_name authority="Coville" date="1896" rank="species">confusus</taxon_name>
    <place_of_publication>
      <publication_title>Proceedings of the Biological Society of Washington</publication_title>
      <place_in_publication>10: 127. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus poiophylli;species confusus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000113</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Osterhout" date="unknown" rank="species">exilis</taxon_name>
    <taxon_hierarchy>genus Juncus;species exilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, 3–5 dm.</text>
      <biological_entity id="o7817" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes densely branched.</text>
      <biological_entity id="o7818" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (1–) 5–15 (–25).</text>
      <biological_entity id="o7819" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="5" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="25" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1–3.</text>
      <biological_entity id="o7820" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal, 2–4;</text>
      <biological_entity id="o7821" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 0.3–0.7 mm, apex usually rounded, scarious to membranaceous;</text>
      <biological_entity id="o7822" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7823" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="scarious" name="texture" src="d0_s5" to="membranaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade flat, 3–15 cm × 0.4–1 mm, margins entire.</text>
      <biological_entity id="o7824" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7825" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–25-flowered, congested, 1–2.5 × 1–2 cm;</text>
      <biological_entity id="o7826" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-25-flowered" value_original="3-25-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="congested" value_original="congested" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary bract usually exceeding inflorescence.</text>
      <biological_entity constraint="primary" id="o7827" name="bract" name_original="bract" src="d0_s8" type="structure" />
      <biological_entity id="o7828" name="inflorescence" name_original="inflorescence" src="d0_s8" type="structure" />
      <relation from="o7827" id="r1043" modifier="usually" name="exceeding" negation="false" src="d0_s8" to="o7828" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers not secund;: bracteoles 2;</text>
      <biological_entity id="o7829" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o7830" name="bracteole" name_original="bracteoles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals dark green to blackish, usually with brownish midstripe, lanceolate to lanceolate-ovate, 3.5–4.3 mm, margins clear;</text>
      <biological_entity id="o7831" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o7832" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s10" to="blackish" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="lanceolate-ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7833" name="midstripe" name_original="midstripe" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o7834" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="clear" value_original="clear" />
      </biological_entity>
      <relation from="o7832" id="r1044" modifier="usually" name="with" negation="false" src="d0_s10" to="o7833" />
    </statement>
    <statement id="d0_s11">
      <text>outer and inner series nearly equal;</text>
      <biological_entity id="o7835" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity constraint="outer and inner" id="o7836" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6, filaments 0.6–0.9 mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o7837" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o7838" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o7839" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7840" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 0.1 mm.</text>
      <biological_entity id="o7841" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o7842" name="style" name_original="style" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules tan or darker, 3-locular, nearly globose to widely obvoid, 2.5–3.5 × 1.3–1.8 mm, shorter than perianth.</text>
      <biological_entity id="o7843" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="darker" value_original="darker" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s14" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o7845" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" is_modifier="true" name="length" src="d0_s14" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" is_modifier="true" name="width" src="d0_s14" to="1.8" to_unit="mm" />
        <character is_modifier="true" modifier="widely" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7844" name="perianth" name_original="perianth" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" is_modifier="true" name="length" src="d0_s14" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" is_modifier="true" name="width" src="d0_s14" to="1.8" to_unit="mm" />
        <character is_modifier="true" modifier="widely" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <relation from="o7844" id="r1045" name="to" negation="false" src="d0_s14" to="o7845" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds yellowish, obovoid to ellipsioid, 0.4–0.5 mm, not tailed.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 80.</text>
      <biological_entity id="o7846" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish" value_original="yellowish" />
        <character constraint="to ellipsioid , 0.4-0.5 mm" is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7847" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, open grasslands and meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="open grasslands" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Ariz., Calif., Colo., Idaho, Nev., N.Mex., Mont., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20</number>
  
</bio:treatment>