<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="treatment_page">120</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schultz Schultzenstein" date="unknown" rank="family">arecaceae</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">ARECOIDEAE</taxon_name>
    <taxon_name authority="Martius in S. L. Endlicher" date="1837" rank="tribe">COCOEAE</taxon_name>
    <taxon_name authority="Saakov" date="1954" rank="subtribe">BUTIINAE</taxon_name>
    <taxon_name authority="Martius" date="1826" rank="genus">Syagrus</taxon_name>
    <place_of_publication>
      <publication_title>Palm. Fam.</publication_title>
      <place_in_publication>18. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family arecaceae;subfamily arecoideae;tribe cocoeae;subtribe butiinae;genus syagrus;</taxon_hierarchy>
    <other_info_on_name type="etymology">classical name, derivation unknown, but a name used by Pliny for a kind of palm</other_info_on_name>
    <other_info_on_name type="fna_id">131952</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems solitary, erect, robust, unarmed, bearing conspicuous nodal rings.</text>
      <biological_entity id="o2686" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity constraint="nodal" id="o2687" name="ring" name_original="rings" src="d0_s0" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s0" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o2686" id="r350" name="bearing" negation="false" src="d0_s0" to="o2687" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole unarmed;</text>
      <biological_entity id="o2688" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2689" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sheath not forming crownshaft;</text>
      <biological_entity id="o2690" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2691" name="sheath" name_original="sheath" src="d0_s2" type="structure" />
      <biological_entity id="o2692" name="crownshaft" name_original="crownshaft" src="d0_s2" type="structure" />
      <relation from="o2691" id="r351" name="forming" negation="false" src="d0_s2" to="o2692" />
    </statement>
    <statement id="d0_s3">
      <text>blade pinnate, unarmed;</text>
      <biological_entity id="o2693" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2694" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>plication reduplicate;</text>
      <biological_entity id="o2695" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2696" name="plication" name_original="plication" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>segments regularly arranged in multiple planes, apices acute to 2-cleft.</text>
      <biological_entity id="o2697" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o2698" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character constraint="in planes" constraintid="o2699" is_modifier="false" modifier="regularly" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o2699" name="plane" name_original="planes" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="multiple" value_original="multiple" />
      </biological_entity>
      <biological_entity id="o2700" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="2-cleft" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences axillary within crown of leaves, paniculate, 1 order of branching, ascending, becoming pendulous in fruit;</text>
      <biological_entity id="o2701" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character constraint="within crown" constraintid="o2702" is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="paniculate" value_original="paniculate" />
        <character constraint="of fruit" constraintid="o2704" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2702" name="crown" name_original="crown" src="d0_s6" type="structure" />
      <biological_entity id="o2703" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o2704" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="branching" value_original="branching" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="true" modifier="becoming" name="orientation" src="d0_s6" value="pendulous" value_original="pendulous" />
      </biological_entity>
      <relation from="o2702" id="r352" name="part_of" negation="false" src="d0_s6" to="o2703" />
    </statement>
    <statement id="d0_s7">
      <text>prophyll short;</text>
      <biological_entity id="o2705" name="prophyll" name_original="prophyll" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncular bract woody, beaked, splitting abaxially, becoming boatshaped.</text>
      <biological_entity constraint="peduncular" id="o2706" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="beaked" value_original="beaked" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_dehiscence" src="d0_s8" value="splitting" value_original="splitting" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s8" value="boat-shaped" value_original="boat-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers unisexual, sessile, borne in triads of 1 pistillate flower flanked by 2 staminate, staminate flowers borne singly along distal portions of rachillae.</text>
      <biological_entity id="o2707" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o2708" name="triad" name_original="triads" src="d0_s9" type="structure" />
      <biological_entity id="o2709" name="flower" name_original="flower" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o2710" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2711" name="portion" name_original="portions" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="singly" value_original="singly" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2712" name="rachilla" name_original="rachillae" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="singly" value_original="singly" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o2707" id="r353" name="borne in" negation="false" src="d0_s9" to="o2708" />
      <relation from="o2707" id="r354" name="consist_of" negation="false" src="d0_s9" to="o2709" />
      <relation from="o2709" id="r355" name="flanked by" negation="false" src="d0_s9" to="o2710" />
      <relation from="o2709" id="r356" name="flanked by" negation="false" src="d0_s9" to="o2711" />
      <relation from="o2709" id="r357" name="flanked by" negation="false" src="d0_s9" to="o2712" />
    </statement>
    <statement id="d0_s10">
      <text>Staminate flowers: sepals 3, connate;</text>
      <biological_entity id="o2713" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2714" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 3, free, valvate, leathery;</text>
      <biological_entity id="o2715" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2716" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s11" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6, free;</text>
      <biological_entity id="o2717" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2718" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers linear;</text>
      <biological_entity id="o2719" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2720" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pistillode with 3 minute lobes.</text>
      <biological_entity id="o2721" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o2722" name="pistillode" name_original="pistillode" src="d0_s14" type="structure" />
      <biological_entity id="o2723" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s14" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o2722" id="r358" name="with" negation="false" src="d0_s14" to="o2723" />
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers borne basally on rachillae, massive;</text>
      <biological_entity id="o2724" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="size" src="d0_s15" value="massive" value_original="massive" />
      </biological_entity>
      <biological_entity id="o2725" name="rachilla" name_original="rachillae" src="d0_s15" type="structure" />
      <relation from="o2724" id="r359" name="borne" negation="false" src="d0_s15" to="o2725" />
    </statement>
    <statement id="d0_s16">
      <text>sepals 3, imbricate, free;</text>
      <biological_entity id="o2726" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 3, imbricate, free;</text>
      <biological_entity id="o2727" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="arrangement" src="d0_s17" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>staminode a low annular ring at base of pistil;</text>
      <biological_entity id="o2728" name="staminode" name_original="staminode" src="d0_s18" type="structure" />
      <biological_entity id="o2729" name="ring" name_original="ring" src="d0_s18" type="structure">
        <character is_modifier="true" name="position" src="d0_s18" value="low" value_original="low" />
        <character is_modifier="true" name="shape" src="d0_s18" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity id="o2730" name="base" name_original="base" src="d0_s18" type="structure" />
      <biological_entity id="o2731" name="pistil" name_original="pistil" src="d0_s18" type="structure" />
      <relation from="o2729" id="r360" name="at" negation="false" src="d0_s18" to="o2730" />
      <relation from="o2730" id="r361" name="part_of" negation="false" src="d0_s18" to="o2731" />
    </statement>
    <statement id="d0_s19">
      <text>pistil 1, large;</text>
      <biological_entity id="o2732" name="pistil" name_original="pistil" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" name="size" src="d0_s19" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 1;</text>
      <biological_entity id="o2733" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>style indistinct;</text>
      <biological_entity id="o2734" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s21" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigmas 3.</text>
      <biological_entity id="o2735" name="stigma" name_original="stigmas" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Fruits drupes, ovoid, less than 4 cm diam.;</text>
      <biological_entity constraint="fruits" id="o2736" name="drupe" name_original="drupes" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s23" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>exocarp orange, thin, smooth;</text>
      <biological_entity id="o2737" name="exocarp" name_original="exocarp" src="d0_s24" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s24" value="orange" value_original="orange" />
        <character is_modifier="false" name="width" src="d0_s24" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s24" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>mesocarp fleshy, oily;</text>
      <biological_entity id="o2738" name="mesocarp" name_original="mesocarp" src="d0_s25" type="structure">
        <character is_modifier="false" name="texture" src="d0_s25" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="coating" src="d0_s25" value="oily" value_original="oily" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>endocarp thick, bony, irregularly folded into seed, with 3 basal germination pores.</text>
      <biological_entity id="o2739" name="endocarp" name_original="endocarp" src="d0_s26" type="structure">
        <character is_modifier="false" name="width" src="d0_s26" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s26" value="osseous" value_original="bony" />
        <character constraint="into seed" constraintid="o2740" is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s26" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o2740" name="seed" name_original="seed" src="d0_s26" type="structure" />
      <biological_entity constraint="basal" id="o2741" name="pore" name_original="pores" src="d0_s26" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s26" value="3" value_original="3" />
        <character is_modifier="true" name="character" src="d0_s26" value="germination" value_original="germination" />
      </biological_entity>
      <relation from="o2739" id="r362" name="with" negation="false" src="d0_s26" to="o2741" />
    </statement>
    <statement id="d0_s27">
      <text>Seeds irregular with hollow cavity;</text>
      <biological_entity id="o2742" name="seed" name_original="seeds" src="d0_s27" type="structure">
        <character constraint="with cavity" constraintid="o2743" is_modifier="false" name="architecture_or_course" src="d0_s27" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity id="o2743" name="cavity" name_original="cavity" src="d0_s27" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s27" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>endosperm homogeneous;</text>
      <biological_entity id="o2744" name="endosperm" name_original="endosperm" src="d0_s28" type="structure">
        <character is_modifier="false" name="variability" src="d0_s28" value="homogeneous" value_original="homogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>embryo basal;</text>
      <biological_entity id="o2745" name="embryo" name_original="embryo" src="d0_s29" type="structure">
        <character is_modifier="false" name="position" src="d0_s29" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s30">
      <text>eophyll undivided, lanceolate.</text>
      <biological_entity id="o2746" name="eophyll" name_original="eophyll" src="d0_s30" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s30" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="shape" src="d0_s30" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, West Indies (Lesser Antilles), South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies (Lesser Antilles)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17</number>
  <other_name type="common_name">Queen palm</other_name>
  <discussion>Species 32 (1 in the flora).</discussion>
  
</bio:treatment>