<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Agardh" date="unknown" rank="family">aponogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus f." date="1782" rank="genus">Aponogeton</taxon_name>
    <place_of_publication>
      <publication_title>Supplementum Plantarum</publication_title>
      <place_in_publication>32.; 214. 1782</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aponogetonaceae;genus Aponogeton</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, from aquatic habitat</other_info_on_name>
    <other_info_on_name type="fna_id">102317</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs with milky sap.</text>
      <biological_entity id="o3484" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o3485" name="sap" name_original="sap" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="milky" value_original="milky" />
      </biological_entity>
      <relation from="o3484" id="r473" name="with" negation="false" src="d0_s0" to="o3485" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves sheathing at base;</text>
      <biological_entity id="o3486" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o3487" is_modifier="false" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o3487" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>axillary scales present.</text>
      <biological_entity constraint="axillary" id="o3488" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences branched [unbranched], projected above water.</text>
      <biological_entity id="o3489" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers bilaterally symmetric, sessile;</text>
      <biological_entity id="o3490" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="bilaterally" name="architecture_or_shape" src="d0_s4" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stamens 6–18– [50];</text>
      <biological_entity id="o3491" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="50" value_original="50" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s5" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>filaments distinct;</text>
      <biological_entity id="o3492" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 2-locular;</text>
      <biological_entity id="o3493" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s7" value="2-locular" value_original="2-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistils 2–6 [–9], sessile.</text>
      <biological_entity id="o3494" name="pistil" name_original="pistils" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="9" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits: beak terminal [lateral], curved or straight.</text>
      <biological_entity id="o3495" name="fruit" name_original="fruits" src="d0_s9" type="structure" />
      <biological_entity id="o3496" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds: endosperm absent;</text>
      <biological_entity id="o3497" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity id="o3498" name="endosperm" name_original="endosperm" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>seed-coat single or double.</text>
      <biological_entity id="o3499" name="seed" name_original="seeds" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>x = 8.</text>
      <biological_entity id="o3500" name="seed-coat" name_original="seed-coat" src="d0_s11" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s11" value="single" value_original="single" />
        <character name="quantity" src="d0_s11" value="double" value_original="double" />
      </biological_entity>
      <biological_entity constraint="x" id="o3501" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America; native, Africa; tropical regions, Eastern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="native" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="tropical regions" establishment_means="native" />
        <character name="distribution" value="Eastern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Species 47 (1 in the flora).</discussion>
  
</bio:treatment>