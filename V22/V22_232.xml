<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">najadaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">najas</taxon_name>
    <taxon_name authority="(Sprengel) Magnus" date="1870" rank="species">guadalupensis</taxon_name>
    <taxon_name authority="(R. R. Haynes &amp; Wentz) R. R. Haynes &amp; Hellquist" date="1996" rank="subspecies">floridana</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>6: 371. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family najadaceae;genus najas;species guadalupensis;subspecies floridana</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000259</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Najas</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">guadalupensis</taxon_name>
    <taxon_name authority="R. R. Haynes &amp; Wentz" date="unknown" rank="variety">floridana</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>5: 262. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Najas;species guadalupensis;variety floridana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 7–51 cm × 0.1–1.7 mm.</text>
      <biological_entity id="o10325" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s0" to="51" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s0" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 0.9–3.3 cm;</text>
      <biological_entity id="o10326" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s1" to="3.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>sheath 1.2–2.5 mm wide, apex rounded;</text>
      <biological_entity id="o10327" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10328" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade 0.5–2.1 mm wide, teeth 18–42 per side, visible to unaided eye, apex round obtuse to acuminate.</text>
      <biological_entity id="o10329" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10330" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o10331" from="18" name="quantity" src="d0_s3" to="42" />
        <character constraint="to eye" constraintid="o10332" is_modifier="false" name="prominence" notes="" src="d0_s3" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o10331" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o10332" name="eye" name_original="eye" src="d0_s3" type="structure" />
      <biological_entity id="o10333" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="round" value_original="round" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 1 per axil.</text>
      <biological_entity id="o10334" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character constraint="per axil" constraintid="o10335" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o10335" name="axil" name_original="axil" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Staminate flowers 1.5–2.4 mm;</text>
      <biological_entity id="o10336" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers 1-loculed.</text>
      <biological_entity id="o10337" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-loculed" value_original="1-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers 1–3.4 mm.</text>
      <biological_entity id="o10338" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seeds 1.6–2.2 × 0.3–0.8 mm;</text>
    </statement>
    <statement id="d0_s9">
      <text>aeroleareoles of testa in 20 longitudinal rows.</text>
      <biological_entity id="o10340" name="testa" name_original="testa" src="d0_s9" type="structure" />
      <biological_entity id="o10341" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="20" value_original="20" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o10339" id="r1371" name="part_of" negation="false" src="d0_s9" to="o10340" />
      <relation from="o10339" id="r1372" name="in" negation="false" src="d0_s9" to="o10341" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 48.</text>
      <biological_entity id="o10339" name="seed" name_original="seeds" src="d0_s8" type="structure" constraint="testa" constraint_original="testa; testa">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10342" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lakes, streams, and canals</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="canals" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3c</number>
  
</bio:treatment>