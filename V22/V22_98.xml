<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">alismataceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">damasonium</taxon_name>
    <taxon_name authority="Torrey in G. Bentham" date="1857" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>in G. Bentham,Plantas Hartwegianas imprimis Mexicanas.</publication_title>
      <place_in_publication>341. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family alismataceae;genus damasonium;species californicum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220003827</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaerocarpus</taxon_name>
    <taxon_name authority="(Torrey) Small" date="unknown" rank="species">californicus</taxon_name>
    <taxon_hierarchy>genus Machaerocarpus;species californicus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, to 35 cm.</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves erect;</text>
      <biological_entity id="o1" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 3–15 cm;</text>
      <biological_entity id="o2" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade absent or narrowly lanceolate to ovate, 3–9 × 0.5–3 cm, margins entire.</text>
      <biological_entity id="o3" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s3" value="narrowly lanceolate to ovate" value_original="narrowly lanceolate to ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences racemes, rarely panicles;</text>
      <biological_entity constraint="inflorescences" id="o5" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
      <biological_entity id="o6" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>scapes erect or ascending;</text>
      <biological_entity id="o7" name="scape" name_original="scapes" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts broadly ovatelanceolate, thin, scarious.</text>
      <biological_entity id="o8" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 12–22 mm wide;</text>
      <biological_entity id="o9" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals persistent, green, oblong or ovate, somewhat hooded, 4–5 mm;</text>
      <biological_entity id="o10" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s8" value="hooded" value_original="hooded" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white with yellow patch near base, rarely pink, broadly cuneate, rhombic, 6–10 mm, erose distally;</text>
      <biological_entity id="o11" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character constraint="near base" constraintid="o12" is_modifier="false" name="coloration" src="d0_s9" value="yellow patch" value_original="yellow patch" />
        <character is_modifier="false" modifier="rarely" name="coloration" notes="" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o12" name="base" name_original="base" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stamens 2–3 mm, 2 in front of each petal;</text>
      <biological_entity id="o13" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character constraint="in-front-of petal" constraintid="o14" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14" name="petal" name_original="petal" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pistils 6–10 in single ring on receptacle;</text>
      <biological_entity id="o15" name="pistil" name_original="pistils" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in ring" constraintid="o16" from="6" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o16" name="ring" name_original="ring" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o17" name="receptacle" name_original="receptacle" src="d0_s11" type="structure" />
      <relation from="o16" id="r0" name="on" negation="false" src="d0_s11" to="o17" />
    </statement>
    <statement id="d0_s12">
      <text>ovules 1;</text>
      <biological_entity id="o18" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles erect in anthesis, soon spreading radially, elongate, stout.</text>
      <biological_entity id="o19" name="style" name_original="styles" src="d0_s13" type="structure">
        <character constraint="in anthesis" is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="soon; radially" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s13" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits horned by enlargement of style, ribbed on angles, with prominent angular shoulder and depressed flat faces, 0.5 × 3–5.5 mm;</text>
      <biological_entity id="o20" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character constraint="by enlargement" constraintid="o21" is_modifier="false" name="shape" src="d0_s14" value="horned" value_original="horned" />
        <character constraint="on angles" constraintid="o23" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s14" value="ribbed" value_original="ribbed" />
        <character name="length" notes="" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s14" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21" name="enlargement" name_original="enlargement" src="d0_s14" type="structure" />
      <biological_entity id="o22" name="style" name_original="style" src="d0_s14" type="structure" />
      <biological_entity id="o23" name="angle" name_original="angles" src="d0_s14" type="structure" />
      <biological_entity id="o24" name="shoulder" name_original="shoulder" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s14" value="angular" value_original="angular" />
      </biological_entity>
      <biological_entity id="o25" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="depressed" value_original="depressed" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o21" id="r1" name="part_of" negation="false" src="d0_s14" to="o22" />
      <relation from="o20" id="r2" name="with" negation="false" src="d0_s14" to="o24" />
      <relation from="o20" id="r3" name="with" negation="false" src="d0_s14" to="o25" />
    </statement>
    <statement id="d0_s15">
      <text>beak 3–6 mm.</text>
      <biological_entity id="o26" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernal pools, margins of intermittent streams, or on mud</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pools" modifier="vernal" />
        <character name="habitat" value="margins" constraint="of intermittent streams" />
        <character name="habitat" value="intermittent streams" />
        <character name="habitat" value="mud" modifier="or on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  
</bio:treatment>