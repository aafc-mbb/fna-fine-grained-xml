<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Juss." date="unknown" rank="family">araceae</taxon_name>
    <taxon_name authority="Schott in H. W. Schott and S. L. Endlicher" date="1832" rank="genus">colocasia</taxon_name>
    <taxon_name authority="(Linnaeus) Schott in H. W. Schott and S. L. Endlicher" date="1832" rank="species">esculenta</taxon_name>
    <place_of_publication>
      <publication_title>in H. W. Schott and S. L. Endlicher,Meletemata Botanica</publication_title>
      <place_in_publication>18. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family araceae;genus colocasia;species esculenta</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200027262</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">esculentum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 965. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arum;species esculentum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Colocasia</taxon_name>
    <taxon_name authority="Schott" date="unknown" rank="species">antiquorum</taxon_name>
    <taxon_hierarchy>genus Colocasia;species antiquorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Corms underground, starchy;</text>
      <biological_entity id="o10903" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character is_modifier="false" name="location" src="d0_s0" value="underground" value_original="underground" />
        <character is_modifier="false" name="texture" src="d0_s0" value="starchy" value_original="starchy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons elongate, with nodes produced at or near surface, spreading horizontally.</text>
      <biological_entity id="o10904" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o10905" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o10906" name="surface" name_original="surface" src="d0_s1" type="structure" />
      <relation from="o10904" id="r1446" name="with" negation="false" src="d0_s1" to="o10905" />
      <relation from="o10905" id="r1447" name="produced at" negation="false" src="d0_s1" to="o10906" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole green, often purple apically, 30–80 (–180) cm, spongy and filled with air spaces;</text>
      <biological_entity id="o10907" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10908" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" modifier="often; apically" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="180" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="spongy" value_original="spongy" />
      </biological_entity>
      <biological_entity id="o10909" name="air" name_original="air" src="d0_s2" type="structure" />
      <biological_entity id="o10910" name="space" name_original="spaces" src="d0_s2" type="structure" />
      <relation from="o10908" id="r1448" name="filled with" negation="false" src="d0_s2" to="o10909" />
      <relation from="o10908" id="r1449" name="filled with" negation="false" src="d0_s2" to="o10910" />
    </statement>
    <statement id="d0_s3">
      <text>blade green to dark green or glaucous blue-green on adaxial surface, usually with red or purple spot at point of petiole attachment, peltate for 2.5–7 cm, 17–70 × 10–40 cm;</text>
      <biological_entity id="o10911" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10912" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="on adaxial surface" constraintid="o10913" from="green" name="coloration" src="d0_s3" to="dark green or glaucous blue-green" />
        <character constraint="for 2.5-7 cm" is_modifier="false" name="architecture" notes="" src="d0_s3" value="peltate" value_original="peltate" />
        <character char_type="range_value" from="17" from_unit="cm" name="length" src="d0_s3" to="70" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s3" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10913" name="surface" name_original="surface" src="d0_s3" type="structure" />
      <biological_entity id="o10914" name="point" name_original="point" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="purple spot" value_original="purple spot" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o10915" name="attachment" name_original="attachment" src="d0_s3" type="structure" />
      <relation from="o10912" id="r1450" modifier="usually" name="with" negation="false" src="d0_s3" to="o10914" />
      <relation from="o10914" id="r1451" name="part_of" negation="false" src="d0_s3" to="o10915" />
    </statement>
    <statement id="d0_s4">
      <text>primary lateral-veins parallel, secondary lateral-veins netted, forming collective vein between primary lateral-veins;</text>
      <biological_entity id="o10916" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="primary" id="o10917" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o10918" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s4" value="netted" value_original="netted" />
      </biological_entity>
      <biological_entity constraint="collective" id="o10919" name="vein" name_original="vein" src="d0_s4" type="structure" />
      <biological_entity constraint="primary" id="o10920" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure" />
      <biological_entity id="o10921.o10920." name="primary-lateral-vein-primary-lateral-vein" name_original="primary lateral-veins" src="d0_s4" type="structure" />
      <relation from="o10918" id="r1452" name="forming" negation="false" src="d0_s4" to="o10919" />
    </statement>
    <statement id="d0_s5">
      <text>apex mucronate.</text>
      <biological_entity id="o10922" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o10923" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: spathe 20–35 cm;</text>
      <biological_entity id="o10924" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o10925" name="spathe" name_original="spathe" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s6" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tube green;</text>
      <biological_entity id="o10926" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o10927" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade orange outside and in, opening basally and reflexing apically at anthesis to expose spadix, more than 3 times longer than tube;</text>
      <biological_entity id="o10928" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o10929" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character constraint="at spadix" constraintid="o10930" is_modifier="false" modifier="basally" name="orientation" src="d0_s8" value="reflexing" value_original="reflexing" />
        <character constraint="tube" constraintid="o10931" is_modifier="false" name="length_or_size" notes="" src="d0_s8" value="3+ times longer than tube" />
      </biological_entity>
      <biological_entity id="o10930" name="spadix" name_original="spadix" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="anthesis" value_original="anthesis" />
      </biological_entity>
      <biological_entity id="o10931" name="tube" name_original="tube" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>spadix 9–15 cm.</text>
      <biological_entity id="o10932" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o10933" name="spadix" name_original="spadix" src="d0_s9" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: pistillate flowers pea green, interspersed with white pistillodes;</text>
      <biological_entity id="o10934" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10935" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pea green" value_original="pea green" />
      </biological_entity>
      <biological_entity id="o10936" name="pistillode" name_original="pistillodes" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <relation from="o10935" id="r1453" name="interspersed with" negation="false" src="d0_s10" to="o10936" />
    </statement>
    <statement id="d0_s11">
      <text>ovaries 1-locular;</text>
      <biological_entity id="o10937" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10938" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s11" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 36–67;</text>
      <biological_entity id="o10939" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10940" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" from="36" name="quantity" src="d0_s12" to="67" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sterile flowers white to pale-yellow;</text>
      <biological_entity id="o10941" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10942" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s13" to="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>staminate flowers and sterile tip pale orange, stamens 3–6, connate.</text>
      <biological_entity id="o10943" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o10944" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale orange" value_original="pale orange" />
      </biological_entity>
      <biological_entity id="o10945" name="tip" name_original="tip" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="true" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale orange" value_original="pale orange" />
      </biological_entity>
      <biological_entity id="o10946" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="6" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits orange.</text>
      <biological_entity id="o10947" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1–1.5 mm, not observed in flora area.</text>
      <biological_entity id="o10949" name="area" name_original="area" src="d0_s16" type="structure" />
      <relation from="o10948" id="r1454" name="observed in flora" negation="true" src="d0_s16" to="o10949" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 28, 42 (Old World).</text>
      <biological_entity id="o10948" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10950" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="28" value_original="28" />
        <character name="quantity" src="d0_s17" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Usually forming large colonies along streams, ponds, ditches, canals, and other wet areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="large colonies" modifier="usually forming" constraint="along streams , ponds , ditches , canals , and other wet areas" />
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="canals" />
        <character name="habitat" value="other wet areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Fla., Ga., La., Miss., Tex.; Mexico; West Indies; Bermuda; Central America; South America; Asia; Africa; Indian Ocean Islands; Pacific Islands; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Bermuda" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Elephant's-ear</other_name>
  <other_name type="common_name">wild taro</other_name>
  <discussion>Weedy plants of Colocasia esculenta in the United States are essentially all one morphologic form (usually with long stolons and with a red to purple spot on the adaxially surface of on the leaf opposite the point junction where of the petiole joins the leaf and blade and with long stolons). This taxon has been called C. esculenta var. aquatilis Hasskarl in some treatments (K. A. Wilson 1960). Other forms of C. esculenta are cultivated in the flora area both for food and as ornamentals. The species is extremely variable and many varieties have been recognized taxonomically with little of agreement on the application of names. Because of their weedy status and their infrequent flowering, specimens of C. esculenta are not frequently collected, and the distribution indicated here reflects this deficiency. Plants may occur beyond the boundary outlined on the map, but the species does not become established in areas subjected to cold temperatures.</discussion>
  
</bio:treatment>