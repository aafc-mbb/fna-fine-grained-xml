<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Elias Landolt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">Lemnaceae</taxon_name>
    <taxon_hierarchy>family Lemnaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10488</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, mostly perennial (L.emna aequinoctialis and L. perpusilla also annual), aquatic, floating or submersed, reduced to small green bodies called fronds corresponding partly to leaf and partly to stem.</text>
      <biological_entity id="o17292" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="location" src="d0_s0" value="floating" value_original="floating" />
        <character is_modifier="false" name="location" src="d0_s0" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="size" src="d0_s0" value="reduced" value_original="reduced" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o17293" name="body" name_original="bodies" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="small green" value_original="small green" />
      </biological_entity>
      <biological_entity id="o17294" name="frond" name_original="fronds" src="d0_s0" type="structure" />
      <biological_entity id="o17295" name="leaf" name_original="leaf" src="d0_s0" type="structure" />
      <biological_entity id="o17296" name="stem" name_original="stem" src="d0_s0" type="structure" />
      <relation from="o17293" id="r2218" name="called" negation="false" src="d0_s0" to="o17294" />
      <relation from="o17293" id="r2219" name="corresponding" negation="false" src="d0_s0" to="o17295" />
      <relation from="o17293" id="r2220" modifier="partly" name="to" negation="false" src="d0_s0" to="o17296" />
    </statement>
    <statement id="d0_s1">
      <text>Roots 0 or 1–21;</text>
      <biological_entity id="o17297" name="root" name_original="roots" src="d0_s1" type="structure">
        <character name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>root hairs absent.</text>
      <biological_entity constraint="root" id="o17298" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems: distinct stems absent.</text>
      <biological_entity id="o17299" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o17300" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cataphylls absent.</text>
      <biological_entity id="o17301" name="cataphyll" name_original="cataphylls" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fronds 1 or 2–20 or more, not differentiated into petiole and blade (thin white stipe or green stalk attaching new fronds to mother frond), coherent at base, flattened or globular, fronds smaller than 1.5 cm, venation from node, outer veins sometimes branching distally from inner ones, or veins absent;</text>
      <biological_entity id="o17302" name="frond" name_original="fronds" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="20" />
        <character constraint="into blade" constraintid="o17304" is_modifier="false" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character constraint="at base" constraintid="o17305" is_modifier="false" name="fusion" notes="" src="d0_s5" value="coherent" value_original="coherent" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s5" value="globular" value_original="globular" />
      </biological_entity>
      <biological_entity id="o17303" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o17304" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o17305" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o17306" name="frond" name_original="fronds" src="d0_s5" type="structure">
        <character modifier="smaller than" name="some_measurement" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o17307" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity constraint="outer" id="o17308" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character constraint="from inner veins" constraintid="o17309" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17309" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o17310" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o17306" id="r2221" name="from" negation="false" src="d0_s5" to="o17307" />
    </statement>
    <statement id="d0_s6">
      <text>new fronds (daughter fronds) arising successively in 1–2 pouches or in cavity at base of mother frond;</text>
      <biological_entity id="o17311" name="frond" name_original="fronds" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="new" value_original="new" />
        <character constraint="in " constraintid="o17313" is_modifier="false" name="orientation" src="d0_s6" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o17312" name="pouch" name_original="pouches" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o17313" name="cavity" name_original="cavity" src="d0_s6" type="structure" />
      <biological_entity id="o17314" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o17315" name="mother" name_original="mother" src="d0_s6" type="structure" />
      <biological_entity id="o17316" name="frond" name_original="frond" src="d0_s6" type="structure" />
      <relation from="o17313" id="r2222" name="at" negation="false" src="d0_s6" to="o17314" />
      <relation from="o17314" id="r2223" name="part_of" negation="false" src="d0_s6" to="o17315" />
      <relation from="o17314" id="r2224" name="part_of" negation="false" src="d0_s6" to="o17316" />
    </statement>
    <statement id="d0_s7">
      <text>turions present in some species.</text>
      <biological_entity id="o17317" name="turion" name_original="turions" src="d0_s7" type="structure" />
      <biological_entity id="o17318" name="species" name_original="species" src="d0_s7" type="taxon_name" />
      <relation from="o17317" id="r2225" name="present in some" negation="false" src="d0_s7" to="o17318" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually solitary (mostly 2 per frond for L. perpusilla).</text>
      <biological_entity id="o17319" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers mostly bisexual, 1 (–2) per frond (rare in many species);</text>
      <biological_entity id="o17320" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="2" />
        <character constraint="per frond" constraintid="o17321" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17321" name="frond" name_original="frond" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals absent;</text>
      <biological_entity id="o17322" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals absent;</text>
      <biological_entity id="o17323" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 1–2;</text>
      <biological_entity id="o17324" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovaries 1, bottle-shaped, 1-locular, tapering into short style;</text>
      <biological_entity id="o17325" name="ovary" name_original="ovaries" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s13" value="bottle--shaped" value_original="bottle--shaped" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="1-locular" value_original="1-locular" />
        <character constraint="into style" constraintid="o17326" is_modifier="false" name="shape" src="d0_s13" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o17326" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma funnel-shaped.</text>
      <biological_entity id="o17327" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits follicles;</text>
      <biological_entity constraint="fruits" id="o17328" name="follicle" name_original="follicles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>pericarp membranous, opening by bursting.</text>
      <biological_entity id="o17329" name="pericarp" name_original="pericarp" src="d0_s16" type="structure">
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds 1–5, nearly as long as fruit.</text>
      <biological_entity id="o17330" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="5" />
      </biological_entity>
      <biological_entity id="o17331" name="fruit" name_original="fruit" src="d0_s17" type="structure" />
      <relation from="o17330" id="r2226" modifier="nearly" name="as long as" negation="false" src="d0_s17" to="o17331" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, very rare in regions with high or very low precipitation; not in Greenland or Aleutian Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="very rare in regions with high or very low precipitation" establishment_means="native" />
        <character name="distribution" value="not in Greenland or Aleutian Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>204</number>
  <other_name type="common_name">Duckweed Family</other_name>
  <discussion>In this treatment the terms upper/lower and above/below are always used in relation to the position of the frond in the water. The positions toward the base and the apex of the frond are called proximal and distal, respectively.</discussion>
  <discussion>The "leaf" in the Lemnaceae does not correspond to the leaf of higher plants. It is supposed to consist of a stem in the proximal part (from base to node) and a leaf in the distal part (from node to apex). In Lemnaceae literature it is called a frond. Some authors hold that the flower of the Lemnaceae corresponds to an inflorescence consisting of 1 to 2 male flowers (anthers) and 1 female flower (ovary).</discussion>
  <discussion>With respect to chromosome number, hundreds of Lemnaceae clones were counted, and three levels of cytological variations were identified: (1) intra-individual variation (aneusomaty and/or mixoploidy, (2) intra-populational variation (aneuploidy or polyploidy), and (3) "racial" differentiation (K. Urbanska-Worytkiewicz 1980). Therefore, many chromosome numbers within a species were counted even if only North American plants are listed.</discussion>
  <discussion>Lemnaceae are the smallest and most reduced flowering plants. Therefore, closely related species have but few distinguishing characteristics that are easy to recognize. In addition, most of these features are strongly modificable and overlap considerably. Occasionally Lemnaceae have small, 1–5-celled papillae on the upper surface of the frond. Some species have turions (compact fronds reduced in size and structure, filled with starch grains, forming under unfavorable conditions). Easily recognized by their darker color, tTurions remain for several days with the mother frond before they sink to the bottom.</discussion>
  <discussion>Because of the rarity of flowering and fruiting, only vegetative characteristics are available in most cases. Lemna gibba, L. aequinoctialis, and L. perpusilla are the only American species fruiting rather frequently. The smallness of the plants requires good binoculars magnification and some technical preparations for identification. To analyze anatomic structures (e.g., number of.veins or extension of air spaces), transparent slides are necessary. Fronds preserved in 70% ethanol become transparent. Dried fronds must be boiled first in 70% ethanol and treated afterwards with 10% NaOCl to clear them. Preserving them in ethanol, however, removes pigments that are sometimes important for determination. Under optimal growth conditions the typical anthocyanin pattern of some species does not develop. Therefore, one should observe the species in another season or cultivate it under different conditions. Another difficulty of determination occurs because several (up to 10) Lemnaceae species very often grow together in nature. Therefore, many collection samples contain more than one species, some of which may not be recognized as different at first glance.</discussion>
  <discussion>Lemnaceae are easily distributed by birds over short distances. In many places, they live only so long as conditions are favorable. Afterwards they disappear. A species of a southern area might suddenly occur farther north and remain there for one or.several years. If.the species is exposed to water fed by a warm spring, it might persist far beyond expectations. The distribution maps of the Lemnaceae show the area where the species once was collected and do not represent the actual distribution area, which might be considerably smaller and change within a few years. Generally, most Lemnaceae species have expanded during the last years because of the warming of the climate and eutrophication of the waters.</discussion>
  <discussion>Lemnaceae have a high productivity (some species can double in number within 24 hours) and a very high percentage of amino acids (up to 45% of dry weight). They are used in many regions as food for poultry, pigs, and cows. Wolffia fronds are eaten as a vegetable in southeastern Asia; Lemna gibba is cultivated in Israel for use as a vegetable and salad. Lemnaceae are also used for waste-water purification and as test and indicator plants.</discussion>
  <discussion>Genera 4, species 37 (4 genera, 19 species in the flora, 2 or possibly 3 of these introduced).</discussion>
  <references>
    <reference>Daubs, E. H. 1965. A monograph of Lemnaceae. Illinois Biol. Monogr. 34.</reference>
    <reference>Hegelmaier, C. F. 1868. Die Lemnaceen. Eine monographische Untersuchung.... Leipzig.</reference>
    <reference>Landolt, E. 1986. The family of Lemnaceae—A monographic study, vol. 1. Ver&amp;ouml;ff. Geobot. Inst. E. T. H. Stiftung R&amp;uuml;bel Z&amp;uuml;rich 71.</reference>
    <reference>Landolt, E. and R. Kandeler. 1987. The family of Lemnaceae—A monographic study, vol. 2. Ver&amp;ouml;ff. Geobot. Inst. E. T. H. Stiftung R&amp;uuml;bel Z&amp;uuml;rich 95.</reference>
    <reference>Urbanska-Worytkiewicz, K. 1980. Cytological variation within the family of Lemnaceae. Ver&amp;ouml;ff. Geobot. Inst. E. T. H. Stiftung R&amp;uuml;bel Z&amp;uuml;rich 70: 30–101.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Roots 1–21 per frond; fronds with 1–21 veins; daughter fronds and flowers from 2 lateral pouches at frond base; flowers surrounded by small utricular, membranous scale; stamens 2, 4-locular; seeds longitudinally ribbed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Roots absent; fronds without veins; daughter fronds from single terminal pouch or cavity</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Roots (1–)2–21 per frond; fronds with (3–)5–16(–21) veins, surrounded at base by small scale covering point of attachment of roots; pigment cells present (visible in dead fronds as brown dots)</description>
      <determination>1 Spirodela</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Roots 1 per frond; fronds with 1–5(–7) veins, without scale at base; pigment cells absent (red pigmentation present in some species)</description>
      <determination>2 Lemna</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fronds flat (linear, ribbon-, sabre-, or tongue-shaped, or ovate), with air spaces; daughter fronds from terminal flat pouch at mother-frond base; flower(s) in cavity at side of median line of upper frond surface</description>
      <determination>3 Wolffiella</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fronds 3-dimensional (globular, ovoid, or boat-shaped), without air spaces; daughter fronds from terminal conic pouch or cavity at mother-frond base; flower in cavity on median line of upper frond surface</description>
      <determination>4 Wolffia</determination>
    </key_statement>
  </key>
</bio:treatment>