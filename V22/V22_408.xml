<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">flabelliformis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.</publication_title>
      <place_in_publication>499. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species flabelliformis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000469</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose, rarely solitary, 7–30 cm.</text>
      <biological_entity id="o13035" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short.</text>
      <biological_entity id="o13036" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in fans, (1–) 2–3 (–10) cm;</text>
      <biological_entity id="o13037" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13038" name="fan" name_original="fans" src="d0_s2" type="structure" />
      <relation from="o13037" id="r1734" name="in" negation="false" src="d0_s2" to="o13038" />
    </statement>
    <statement id="d0_s3">
      <text>sheath base without chestnut-brown patch;</text>
      <biological_entity constraint="sheath" id="o13039" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade strongly maroon-tinged, lance-linear, 1–3 (–4) mm wide, margins papillate or finely tuberculate-scabrous.</text>
      <biological_entity id="o13040" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s4" value="maroon-tinged" value_original="maroon-tinged" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="lance-linear" value_original="lance-linear" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13041" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s4" value="tuberculate-scabrous" value_original="tuberculate-scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths much exceeding leaves;</text>
      <biological_entity id="o13042" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o13043" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o13044" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o13043" id="r1735" modifier="much" name="exceeding" negation="false" src="d0_s5" to="o13044" />
    </statement>
    <statement id="d0_s6">
      <text>scapes filiform, nearly terete, 0.5 mm wide, many ribbed;</text>
      <biological_entity id="o13045" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o13046" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="many" value_original="many" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes mostly ovoid, mostly longer than broad, 4–8 (–10) mm, apex acute;</text>
      <biological_entity id="o13047" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o13048" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="mostly longer than broad" value_original="mostly longer than broad" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13049" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts 3–5 mm, margins entire or erose, apex keeled.</text>
      <biological_entity id="o13050" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o13051" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13052" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o13053" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included, strongly curved, 2–3 mm, keel concolorous, firm, ciliate;</text>
      <biological_entity id="o13054" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o13055" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13056" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="concolorous" value_original="concolorous" />
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding in morning, blade obovate, 2.5–3 mm;</text>
      <biological_entity id="o13057" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o13058" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in morning" constraintid="o13059" is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <biological_entity id="o13059" name="morning" name_original="morning" src="d0_s10" type="structure" />
      <biological_entity id="o13060" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o13061" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o13062" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds translucent, broadly ellipsoid to round, 0.3 mm, finely lined longitudinally.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o13063" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s12" to="round" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" modifier="finely; longitudinally" name="architecture" src="d0_s12" value="lined" value_original="lined" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13064" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–early summer (all year south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="flowering time" char_type="range_value" modifier="south" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid, sandy, peaty flatwoods, clearings, disturbed moist sands, coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" modifier="acid" />
        <character name="habitat" value="peaty flatwoods" />
        <character name="habitat" value="clearings" />
        <character name="habitat" value="disturbed moist sands" />
        <character name="habitat" value="coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  
</bio:treatment>