<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Septati</taxon_name>
    <taxon_name authority="Coville" date="1895" rank="species">validus</taxon_name>
    <place_of_publication>
      <publication_title>Bulletin of the Torrey Botanical Club</publication_title>
      <place_in_publication>22:305. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus septati;species validus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000193</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Buchenau" date="unknown" rank="species">crassifolius</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>12: 326. 1890,</place_in_publication>
      <other_info_on_pub>not Bosc ex Laharpe (1827)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species crassifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, sometimes to nearly cespitose, 4–10 dm.</text>
      <biological_entity id="o5568" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes to nearly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes not tuberous, 2 mm diam.</text>
      <biological_entity id="o5569" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character name="diameter" src="d0_s1" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect, terete, 3–5 mm diam., smooth.</text>
      <biological_entity id="o5570" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 0.</text>
      <biological_entity id="o5571" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 2–3 (–6), cauline 1–4;</text>
      <biological_entity id="o5572" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o5573" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o5574" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 1–3 mm, apex acute, membranaceous, absent on proximal leaves;</text>
      <biological_entity id="o5575" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o5576" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5577" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranaceous" value_original="membranaceous" />
        <character constraint="on proximal leaves" constraintid="o5578" is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5578" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade green, laterally compressed, 9–70 cm × 2–6 mm.</text>
      <biological_entity id="o5579" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o5580" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s6" to="70" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal panicles of 10–30 heads, 10–30 cm, branches spreading;</text>
      <biological_entity id="o5581" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o5582" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s7" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5583" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s7" to="30" />
      </biological_entity>
      <biological_entity id="o5584" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o5582" id="r749" name="consist_of" negation="false" src="d0_s7" to="o5583" />
    </statement>
    <statement id="d0_s8">
      <text>primary bract erect;</text>
      <biological_entity constraint="primary" id="o5585" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>heads 20–30-flowered, spheric, (10–) 12–15 mm diam.</text>
      <biological_entity id="o5586" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="20-30-flowered" value_original="20-30-flowered" />
        <character is_modifier="false" name="shape" src="d0_s9" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s9" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals green to reddish, lance-subulate, 4–5 mm, apex acuminate;</text>
      <biological_entity id="o5587" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5588" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lance-subulate" value_original="lance-subulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5589" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 3, anthers 1/3–1/2 filament length.</text>
      <biological_entity id="o5590" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5591" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o5592" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s11" to="1/2" />
      </biological_entity>
      <biological_entity id="o5593" name="filament" name_original="filament" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Capsules exserted, straw-colored, 1-locular, subulate, 4–5,5 mm, tapering to subulate tip, valves separating or not at dehiscence;</text>
      <biological_entity id="o5594" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="tapering" name="shape" src="d0_s12" to="subulate" />
      </biological_entity>
      <biological_entity id="o5595" name="tip" name_original="tip" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>fertile throughout or only proximal to middle..</text>
      <biological_entity id="o5596" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="separating" value_original="separating" />
        <character name="arrangement" src="d0_s12" value="not" value_original="not" />
        <character is_modifier="false" modifier="throughout; throughout" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="only proximal" name="position" src="d0_s13" to="middle" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds broadly ellipsoid, 0.5–0.6 mm, not tailed;</text>
      <biological_entity id="o5597" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>body clear yellowbrown.</text>
      <biological_entity id="o5598" name="body" name_original="body" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="clear yellowbrown" value_original="clear yellowbrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Kans., La., Miss., Mo., N.C., Okla., S.C., Tenn., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>79</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence 10–30 cm; heads 20–30-flowered; capsule fully separating at apex after dehiscence</description>
      <determination>79a Juncus validus var. validus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescence 2–5 cm; heads 6–15-flowered; capsule remaining united at apex after dehiscence</description>
      <determination>79b Juncus validus var. fascinatus</determination>
    </key_statement>
  </key>
</bio:treatment>