<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="Celarier" date="1956" rank="species">pedicellata</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Laboratory</publication_title>
      <place_in_publication>24:6. 1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species pedicellata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000429</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o2747" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o2748" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o2748" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots: some tuberous, thick.</text>
      <biological_entity id="o2749" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems spreading and diffuse, much branched, mainly from base, pubescent with glandular-hairs, eglandular hairs, or mixture.</text>
      <biological_entity id="o2750" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="density" src="d0_s2" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character constraint="with glandular-hairs" constraintid="o2752" is_modifier="false" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2751" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o2752" name="glandular-hair" name_original="glandular-hairs" src="d0_s2" type="structure" />
      <biological_entity id="o2753" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o2750" id="r363" modifier="mainly" name="from" negation="false" src="d0_s2" to="o2751" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves recurved and somewhat lax;</text>
      <biological_entity id="o2754" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dark green to yellowish green, linear-lanceolate, mostly 20–30 × 0.5–1.0 cm (distal leaf-blades equal to or narrower than sheaths when sheaths opened, flattened), sparsely to densely pubescent.</text>
      <biological_entity id="o2755" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s4" to="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="1.0" to_unit="cm" />
        <character is_modifier="false" modifier="mostly; sparsely to densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, solitary, or more commonly also with lateral, pedunculate inflorescences;</text>
      <biological_entity id="o2756" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o2757" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="true" name="position" src="d0_s5" value="lateral" value_original="lateral" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <relation from="o2756" id="r364" modifier="commonly" name="with" negation="false" src="d0_s5" to="o2757" />
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous, similar to leaves in form, sparsely to densely pubescent.</text>
      <biological_entity id="o2758" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2759" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o2760" name="form" name_original="form" src="d0_s6" type="structure" />
      <relation from="o2758" id="r365" name="to" negation="false" src="d0_s6" to="o2759" />
      <relation from="o2759" id="r366" name="in" negation="false" src="d0_s6" to="o2760" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o2761" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s7" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 2.5–4.5 cm, densely pubescent with medium to long, glandular-hairs;</text>
      <biological_entity id="o2762" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s8" to="4.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="medium" name="size" src="d0_s8" to="long" />
      </biological_entity>
      <biological_entity id="o2763" name="glandular-hair" name_original="glandular-hairs" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>sepals 6–11 mm, densely pubescent with glandular-hairs like those of pedicels, occasionally with a few eglandular hairs;</text>
      <biological_entity id="o2764" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character constraint="with glandular-hairs" constraintid="o2765" is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2765" name="glandular-hair" name_original="glandular-hairs" src="d0_s9" type="structure" />
      <biological_entity id="o2766" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o2767" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o2765" id="r367" modifier="like" name="part_of" negation="false" src="d0_s9" to="o2766" />
      <relation from="o2764" id="r368" modifier="occasionally" name="with" negation="false" src="d0_s9" to="o2767" />
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, pink to dark blue, broadly ovate, not clawed;</text>
      <biological_entity id="o2768" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s10" to="dark blue" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens free;</text>
      <biological_entity id="o2769" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded.</text>
      <biological_entity id="o2770" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 2–4 mm;</text>
      <biological_entity id="o2771" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hilum as long as seed.</text>
      <biological_entity id="o2773" name="seed" name_original="seed" src="d0_s14" type="structure" />
      <relation from="o2772" id="r369" name="as long as" negation="false" src="d0_s14" to="o2773" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 12.</text>
      <biological_entity id="o2772" name="hilum" name_original="hilum" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o2774" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13</number>
  <discussion>Tradescantia pedicellata is a most unsatisfactory species. The species may have arisen as a hybrid between Tradescantia humilis and T. occidentalis and been recognized as a species because of its constant morphology and high pollen fertility (R. P. Celarier 1956). C. Sinclair (1967) concluded, however, that there was no evidence for the species' existence, and I have found it very difficult to recognize specimens that agree with the original description (no type has been located).</discussion>
  <discussion>Tradescantia diffusa Bush, a name overlooked by E. Anderson and R. E. Woodson Jr. (1935), has been considered the correct name for this plant (D. T. MacRoberts 1978). After examining the type of T. diffusa, I concluded that it was conspecific with the type of T. humilis.</discussion>
  
</bio:treatment>