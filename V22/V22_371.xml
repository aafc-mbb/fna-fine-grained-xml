<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Persoon" date="1805" rank="genus">Ottelia</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 400. 1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus Ottelia</taxon_hierarchy>
    <other_info_on_name type="etymology">Malay am ottelambel, apparently from otta, to stick to, in reference to thin leaves that stick to body, and am bel, nymphaea</other_info_on_name>
    <other_info_on_name type="fna_id">123432</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial, of fresh waters.</text>
      <biological_entity id="o12691" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes and stolons absent [present].</text>
      <biological_entity id="o12692" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12693" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Erect stems rooted in substrate, unbranched, short.</text>
      <biological_entity id="o12694" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="in substrate" constraintid="o12695" is_modifier="false" name="architecture" src="d0_s2" value="rooted" value_original="rooted" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o12695" name="substrate" name_original="substrate" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, submersed [floating], petiolate;</text>
      <biological_entity id="o12696" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="location" src="d0_s3" value="submersed" value_original="submersed" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lanceolate to reniform [linear], base cuneate to cordate, apex rounded to acute;</text>
      <biological_entity id="o12697" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="reniform" />
      </biological_entity>
      <biological_entity id="o12698" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="cordate" />
      </biological_entity>
      <biological_entity id="o12699" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>midvein without rows of lacunae along sides, blade uniform in color throughout;</text>
      <biological_entity id="o12700" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o12701" name="row" name_original="rows" src="d0_s5" type="structure" />
      <biological_entity id="o12702" name="lacuna" name_original="lacunae" src="d0_s5" type="structure" />
      <biological_entity id="o12703" name="side" name_original="sides" src="d0_s5" type="structure" />
      <biological_entity id="o12704" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s5" value="uniform" value_original="uniform" />
      </biological_entity>
      <relation from="o12700" id="r1689" name="without" negation="false" src="d0_s5" to="o12701" />
      <relation from="o12701" id="r1690" name="part_of" negation="false" src="d0_s5" to="o12702" />
      <relation from="o12701" id="r1691" name="along" negation="false" src="d0_s5" to="o12703" />
    </statement>
    <statement id="d0_s6">
      <text>abaxial surface without prickles or aerenchyma, smooth;</text>
      <biological_entity constraint="abaxial" id="o12705" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12706" name="prickle" name_original="prickles" src="d0_s6" type="structure" />
      <biological_entity id="o12707" name="aerenchymum" name_original="aerenchyma" src="d0_s6" type="structure" />
      <relation from="o12705" id="r1692" name="without" negation="false" src="d0_s6" to="o12706" />
      <relation from="o12705" id="r1693" name="without" negation="false" src="d0_s6" to="o12707" />
    </statement>
    <statement id="d0_s7">
      <text>intravaginal squamules entire.</text>
      <biological_entity constraint="intravaginal" id="o12708" name="squamule" name_original="squamules" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences scapose, long-pedunculate;</text>
      <biological_entity id="o12709" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="long-pedunculate" value_original="long-pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>spathes winged or ribbed.</text>
      <biological_entity id="o12710" name="spathe" name_original="spathes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers bisexual [unisexual, staminate and pistillate on different plants], sessile [staminate pedicellate], floating or occasionally opening under water;</text>
      <biological_entity id="o12711" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s10" value="floating" value_original="floating" />
        <character name="growth_form_or_location" src="d0_s10" value="occasionally" value_original="occasionally" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white to pink or light violet, often yellow at base;</text>
      <biological_entity id="o12712" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="pink or light violet" />
        <character constraint="at base" constraintid="o12713" is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o12713" name="base" name_original="base" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct;</text>
      <biological_entity id="o12714" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen in monads;</text>
      <biological_entity id="o12715" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
      <biological_entity id="o12716" name="monad" name_original="monads" src="d0_s13" type="structure" />
      <relation from="o12715" id="r1694" name="in" negation="false" src="d0_s13" to="o12716" />
    </statement>
    <statement id="d0_s14">
      <text>ovary 1-locular;</text>
      <biological_entity id="o12717" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3–9, not 2-fid.</text>
      <biological_entity id="o12718" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="9" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits oblong, ridged, dehiscing irregularly.</text>
      <biological_entity id="o12719" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="irregularly" name="dehiscence" src="d0_s16" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds fusiform, covered with hairs.</text>
      <biological_entity id="o12720" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity id="o12721" name="hair" name_original="hairs" src="d0_s17" type="structure" />
      <relation from="o12720" id="r1695" name="covered with" negation="false" src="d0_s17" to="o12721" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, North America, South America, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>Species 21 (1 in flora).</discussion>
  <references>
    <reference>Cook, C. D. K., J.-J. Symoens, and K. Urmi-K&amp;ouml;nig. 1984. A revision of the genus Ottelia (Hydrocharitaceae). I1. Generic considerations. Aquatic Bot. 18: 263–274.</reference>
    <reference>Cook, C. D. K. and K. Urmi-K&amp;ouml;nig. 1984. A revision of the genus Ottelia (Hydrocharitaceae). 2. The species of Eurasia, Australasia, and America. Aquatic Bot. 20: 131–177.</reference>
    <reference>Holmes, W. C. 1978. Range extension for Ottelia alismoides (L.) Pers. (Hydrocharitaceae). Castanea 43: 193–194.</reference>
    <reference>Turner, C. E. 1980. Ottelia alismoides (L.) Pers. (Hydrocharitaceae) — U.S.A., California. Madro&amp;ntilde;o 27: 177.</reference>
  </references>
  
</bio:treatment>