<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">178</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="Rafinesque" date="1814" rank="species">ohiensis</taxon_name>
    <place_of_publication>
      <publication_title>Précis Découv. Somiol.</publication_title>
      <place_in_publication>45. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species ohiensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000425</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">canaliculata</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species canaliculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">foliosa</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species foliosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">incarnata</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species incarnata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">reflexa</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species reflexa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o14997" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o14998" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14998" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 15–115 cm;</text>
      <biological_entity id="o14999" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="115" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous or occasionally pilose, glaucous.</text>
      <biological_entity id="o15000" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves spirally arranged, sessile, forming acute angle with stem, arcuate;</text>
      <biological_entity id="o15001" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="course_or_shape" notes="" src="d0_s3" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity id="o15002" name="angle" name_original="angle" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15003" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o15001" id="r1971" name="forming" negation="false" src="d0_s3" to="o15002" />
      <relation from="o15001" id="r1972" name="with" negation="false" src="d0_s3" to="o15003" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to linear-lanceolate, 5–45 × 0.4–4.5 cm (distal leaf-blades equal to or narrower than sheaths when sheaths opened, flattened), apex acuminate, glaucous, usually glabrous, sometimes pilose near sheath.</text>
      <biological_entity id="o15004" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s4" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15005" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="near sheath" constraintid="o15006" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o15006" name="sheath" name_original="sheath" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and often axillary;</text>
      <biological_entity id="o15007" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous.</text>
      <biological_entity id="o15008" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o15009" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s7" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.7–3 cm, glabrous;</text>
      <biological_entity id="o15010" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals glaucous, 4–15 mm, glabrous or with apical tuft of eglandular hairs;</text>
      <biological_entity id="o15011" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="with apical tuft" value_original="with apical tuft" />
      </biological_entity>
      <biological_entity constraint="apical" id="o15012" name="tuft" name_original="tuft" src="d0_s9" type="structure" />
      <biological_entity id="o15013" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o15011" id="r1973" name="with" negation="false" src="d0_s9" to="o15012" />
      <relation from="o15012" id="r1974" name="part_of" negation="false" src="d0_s9" to="o15013" />
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, deep blue to rose, rarely white, broadly ovate, not clawed, 0.8–2 cm;</text>
      <biological_entity id="o15014" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="deep" value_original="deep" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s10" to="rose" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens free;</text>
      <biological_entity id="o15015" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded.</text>
      <biological_entity id="o15016" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–6 mm.</text>
      <biological_entity id="o15017" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–3 mm. 2n = 12, 24.</text>
      <biological_entity id="o15018" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15019" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late winter–fall (Feb (Fla)–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="Feb_(Fla)" to="fall" from="late winter" />
        <character name="flowering time" char_type="range_value" to="Sep" from="" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, railroad rights-of-way, fields, thickets, less commonly in woods, occasionally along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad rights-of-way" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="woods" modifier="less commonly in" />
        <character name="habitat" value="streams" modifier="occasionally along" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <discussion>Tradescantia ohiensis is the most common and widespread species in the United States. It hybridizes with many of the other species.</discussion>
  <discussion>Tradescantia ohiensis var. foliosa (Small) MacRoberts has been recognized for the forms with pilose leaves and sheaths (D. T. MacRoberts 1977). I have found such plants scattered among populations of glabrous plants, and I do not consider them worthy of formal taxonomic status.</discussion>
  <discussion>The following hybrids are known: Tradescantia ohiensis × T. gigantea, in Louisiana and Texas; T. ohiensis × T. hirsuticaulis, Arkansas; T. ohiensis × T. occidentalis, Arkansas, Louisiana; T. ohiensis × T. ozarkana, Arkansas; T. ohiensis × T. paludosa, Louisiana (reported by MacRoberts, 1980); T. ohiensis × T. roseolens, Alabama, Florida; T. ohiensis × T. subaspera, Alabama, Georgia, Mississippi, North Carolina, South Carolina, Tennessee, Virginia, West Virginia; and T. ohiensis × T. virginiana, Georgia, Kentucky, Maryland, Mississippi, North Carolina, South Carolina, and Virginia.</discussion>
  
</bio:treatment>