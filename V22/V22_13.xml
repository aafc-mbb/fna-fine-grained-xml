<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.</publication_title>
      <place_in_publication>500. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000467</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xyris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">elliottii</taxon_name>
    <taxon_name authority="Malme" date="unknown" rank="variety">stenotera</taxon_name>
    <taxon_hierarchy>genus Xyris;species elliottii;variety stenotera;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, densely cespitose, 40–60 (–70) cm.</text>
      <biological_entity id="o1933" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems compact.</text>
      <biological_entity id="o1934" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in narrow fans to erect, 10–30 (–40) cm;</text>
      <biological_entity id="o1935" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1936" name="fan" name_original="fans" src="d0_s2" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="in narrow fans" name="orientation" src="d0_s2" to="erect" />
      </biological_entity>
      <relation from="o1935" id="r248" name="in" negation="false" src="d0_s2" to="o1936" />
    </statement>
    <statement id="d0_s3">
      <text>sheath base tan to brown, firm;</text>
      <biological_entity constraint="sheath" id="o1937" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s3" to="brown" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade mostly green or tinged with maroon, narrowly linear, flattened, plane or slightly twisted, 1–2 (–2.5) mm wide, smooth, margins pale, narrow, incrassate, smooth or papillate.</text>
      <biological_entity id="o1938" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="tinged with maroon" value_original="tinged with maroon" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1939" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale" value_original="pale" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="size" src="d0_s4" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths exceeded by leaves;</text>
      <biological_entity id="o1940" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o1941" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o1942" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o1941" id="r249" name="exceeded by" negation="false" src="d0_s5" to="o1942" />
    </statement>
    <statement id="d0_s6">
      <text>scapes linear, nearly terete, 0.7–1 mm wide, apically 2-ribbed, ribs smooth or papillate;</text>
      <biological_entity id="o1943" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o1944" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s6" value="2-ribbed" value_original="2-ribbed" />
      </biological_entity>
      <biological_entity id="o1945" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes mostly ovoid or ellipsoid, 6–15 mm, apex acute;</text>
      <biological_entity id="o1946" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o1947" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1948" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts 5–6 mm, margins pale, strongly scarious, lacerate, often squarrose, submarginally often reddish, apex low-keeled.</text>
      <biological_entity id="o1949" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o1950" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1951" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="strongly" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s8" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" modifier="submarginally often" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o1952" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="low-keeled" value_original="low-keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included or slightly exsert, slightly curved, (5.5–) 6–7 mm, keel concolorous, firm, finely lacerate or apically lacero-fimbriate, not papillate or ciliate;</text>
      <biological_entity id="o1953" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o1954" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s9" value="exsert" value_original="exsert" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1955" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="concolorous" value_original="concolorous" />
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s9" value="lacero-fimbriate" value_original="lacero-fimbriate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding in morning, blade obovate, 5 mm;</text>
      <biological_entity id="o1956" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1957" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in morning" constraintid="o1958" is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <biological_entity id="o1958" name="morning" name_original="morning" src="d0_s10" type="structure" />
      <biological_entity id="o1959" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o1960" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1961" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds translucent, ellipsoid, 0.5–0.6 mm, prominently longitudinally lined.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 189.</text>
      <biological_entity id="o1962" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="prominently longitudinally" name="architecture" src="d0_s12" value="lined" value_original="lined" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1963" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="189" value_original="189" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (all year south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" modifier="south" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid sandy flatwoods, sandy shores, swales in pinelands, bog edges, coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid sandy flatwoods" />
        <character name="habitat" value="sandy shores" />
        <character name="habitat" value="swales" constraint="in pinelands , bog edges , coastal plain" />
        <character name="habitat" value="pinelands" />
        <character name="habitat" value="bog edges" />
        <character name="habitat" value="coastal plain" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., S.C.; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <discussion>Xyris elliottii with its densely cespitose habit, its glossy brown or red-brown, chaffy leaf sheath bases, and narrow leaves is a part of a complex including Xyris baldwiniana and X. isoetifolia. Usually it is readily distinguished by its taller habit, thicker scapes, and larger spikes, but particularly by its strongly contrasting pale, incrassate leaf blade borders. In peninsular Florida, however, this leaf border is not consistently present, particularly in the narrower-bladed populations (in these, leaf blades may be less than 1 mm wide). Such plants can be distinguished from X. baldwiniana by the staminodial brush, absent in X. baldwiniana, and from X. isoetifolia by the different spike shape, the ragged (rather than entire) bracts, and by a different seed sculpture. Hybrids between X. elliottii and X. brevifolia occur in southern Florida.</discussion>
  
</bio:treatment>