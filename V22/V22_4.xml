<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kunth" date="unknown" rank="family">mayacaceae</taxon_name>
    <taxon_name authority="Aublet" date="1775" rank="genus">mayaca</taxon_name>
    <taxon_name authority="Aublet" date="1775" rank="species">fluviatilis</taxon_name>
    <place_of_publication>
      <publication_title>Historie des Plantes de la Guiane francoise [sic]</publication_title>
      <place_in_publication>1: 42, plate 15. 1775</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mayacaceae;genus mayaca;species fluviatilis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220008231</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mayaca</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">aubletii</taxon_name>
    <taxon_hierarchy>genus Mayaca;species aubletii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mayaca</taxon_name>
    <taxon_name authority="Gandoger" date="unknown" rank="species">caroliniana</taxon_name>
    <taxon_hierarchy>genus Mayaca;species caroliniana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mayaca</taxon_name>
    <taxon_name authority="Gandoger" date="unknown" rank="species">longipes</taxon_name>
    <taxon_hierarchy>genus Mayaca;species longipes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, submerged or terrestrial.</text>
      <biological_entity id="o725" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="location" src="d0_s0" value="submerged" value_original="submerged" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sometimes much branched and matted, decumbent to erect, to 60 cm, usually much less.</text>
      <biological_entity id="o726" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade narrowly lanceolate to linear, 2–20 (–30) cm × 0.5 mm, apex entire or 2-fid.</text>
      <biological_entity id="o727" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o728" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character name="width" src="d0_s2" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o729" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers to 1 cm wide;</text>
      <biological_entity id="o730" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pedicels 2–12 mm, elongating in fruit to 20 mm;</text>
      <biological_entity id="o731" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character constraint="in fruit" constraintid="o732" is_modifier="false" name="length" src="d0_s4" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o732" name="fruit" name_original="fruit" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals persistent, ovate to lanceolate-elliptic, 2–4.5 × 0.7–1.5 mm;</text>
      <biological_entity id="o733" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate-elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals rose to maroon or lilac, sometimes whitish basally, broadly ovate, 3.5–5 × 3–4.5 mm;</text>
      <biological_entity id="o734" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s6" to="maroon or lilac" />
        <character is_modifier="false" modifier="sometimes; basally" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens 1.5–3 mm;</text>
      <biological_entity id="o735" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 1–2 mm;</text>
      <biological_entity id="o736" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 0.5–1 mm, dehiscing by means of apical, porelike slits;</text>
      <biological_entity id="o737" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
        <character constraint="by-means-of slits" constraintid="o738" is_modifier="false" name="dehiscence" src="d0_s9" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o738" name="slit" name_original="slits" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="apical" value_original="apical" />
        <character is_modifier="true" name="shape" src="d0_s9" value="pore-like" value_original="porelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistils 2–2.3 mm, stigmas 3-lobed to 3-fid.</text>
      <biological_entity id="o739" name="pistil" name_original="pistils" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o740" name="stigma" name_original="stigmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3-lobed" name="shape" src="d0_s10" to="3-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules nearly globose to ellipsoid, often irregular because of abortion, to 4 × 3.4 mm.</text>
      <biological_entity id="o741" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character char_type="range_value" from="nearly globose" name="shape" src="d0_s11" to="ellipsoid" />
        <character constraint="because-of abortion" constraintid="o742" is_modifier="false" modifier="often" name="architecture_or_course" src="d0_s11" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" notes="" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" notes="" src="d0_s11" to="3.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o742" name="abortion" name_original="abortion" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 2–25 per capsule, nearly globose, to 1.3 × 0.9 mm;</text>
      <biological_entity id="o743" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per capsule" constraintid="o744" from="2" name="quantity" src="d0_s12" to="25" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="" src="d0_s12" value="globose" value_original="globose" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s12" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o744" name="capsule" name_original="capsule" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>seed-coat scrobiculate or ridged and pitted.</text>
      <biological_entity id="o745" name="seed-coat" name_original="seed-coat" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="scrobiculate" value_original="scrobiculate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="relief" src="d0_s13" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall ([Mar–]May–Oct[–Nov]).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" ([Mar-]May-Oct[-Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, seepage areas, springy places, and in or at margins of ponds, pools, streams, and on coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="seepage areas" />
        <character name="habitat" value="margins" modifier="springy places and in or at" constraint="of ponds , pools , streams ," />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="pools" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="coastal plain" modifier="and on" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.; Mexico; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Some floras have recognized two species in our area, Mayaca fluviatilis, with leaves mostly 4–20 mm, pedicels shorter than the leaves, and capsules oblong-ellipsoid, nearly twice as long as wide; and M. aubletii, with leaves mostly 3–5 mm, pedicels longer than the leaves, and capsules ovoid to nearly globose, nearly as broad as long. Mayaca fluviatilis appears to be the more aquatic form and M. aubletii the more terrestrial one of a single species, and it is quite possible that all or most of the morphologic variation is because of differences in ecology. Experimental work is needed to test the variability in this species as a function of habitat.</discussion>
  <discussion>The literature report for Virginia could not be confirmed.</discussion>
  
</bio:treatment>