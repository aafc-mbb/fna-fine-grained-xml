<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Plumier ex Linnaeus" date="unknown" rank="genus">commelina</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 41. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus commelina;species erecta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000043</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Commelina</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_hierarchy>genus Commelina;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Commelina</taxon_name>
    <taxon_name authority="Wooton" date="unknown" rank="species">crispa</taxon_name>
    <taxon_hierarchy>genus Commelina;species crispa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Commelina</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">elegans</taxon_name>
    <taxon_hierarchy>genus Commelina;species elegans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial.</text>
      <biological_entity id="o14284" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots fleshy, stout, tufted.</text>
      <biological_entity id="o14285" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems cespitose, usually erect to ascending (rarely decumbent, rooting at nodes).</text>
      <biological_entity id="o14286" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="usually erect" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: leaf-sheath auriculate at apex;</text>
      <biological_entity id="o14287" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14288" name="sheath" name_original="leaf-sheath" src="d0_s3" type="structure">
        <character constraint="at apex" constraintid="o14289" is_modifier="false" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o14289" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade sessile or petiolate, linear to lanceolate (rarely lanceolate-ovate), 5–15 × 0.3–4 cm, apex acuminate (rarely acute).</text>
      <biological_entity id="o14290" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14291" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14292" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: distal cyme vestigial, included;</text>
      <biological_entity id="o14293" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o14294" name="cyme" name_original="cyme" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="position" src="d0_s5" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathes solitary or clustered, green, pedunculate, not at all to strongly falcate, 1–2.5 (–4) × 0.7–1.5 (–2.5) cm, margins longly connate, glabrous except along connate edge, apex acute to acuminate, sometimes purple, usually variously pubescent;</text>
      <biological_entity id="o14295" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o14296" name="spathe" name_original="spathes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s6" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="pedunculate" value_original="pedunculate" />
        <character constraint="except " constraintid="o14298" is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14297" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="strongly" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" is_modifier="true" name="atypical_width" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" is_modifier="true" name="width" src="d0_s6" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="longly" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o14298" name="edge" name_original="edge" src="d0_s6" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o14299" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_density" src="d0_s6" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="usually variously" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o14296" id="r1913" modifier="at" name="to" negation="false" src="d0_s6" to="o14297" />
    </statement>
    <statement id="d0_s7">
      <text>peduncles 0.5–1 (–2) cm.</text>
      <biological_entity id="o14300" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o14301" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual and staminate, 1.5–4 cm wide;</text>
      <biological_entity id="o14302" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>proximal petal minute, white, distal petals blue (rarely lavender or white);</text>
      <biological_entity constraint="proximal" id="o14303" name="petal" name_original="petal" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="minute" value_original="minute" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14304" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="blue" value_original="blue" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>staminodes 3, staminodes and medial stamen entirely yellow;</text>
      <biological_entity id="o14305" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o14306" name="staminode" name_original="staminodes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="entirely" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="medial" id="o14307" name="stamen" name_original="stamen" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="entirely" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>antherodes cruciform.</text>
      <biological_entity id="o14308" name="antherode" name_original="antherodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cruciform" value_original="cruciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules 3-locular, 2-valved (very rarely 3-valved), 3.5–4.5 × 3–5 mm;</text>
      <biological_entity id="o14309" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-valved" value_original="2-valved" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>abaxial locule warty, indehiscent (very rarely smooth and dehiscent);</text>
      <biological_entity constraint="abaxial" id="o14310" name="locule" name_original="locule" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="warty" value_original="warty" />
        <character is_modifier="false" name="dehiscence" src="d0_s13" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>adaxial locules smooth, dehiscent.</text>
      <biological_entity constraint="adaxial" id="o14311" name="locule" name_original="locules" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 3, brown, with soft, whitish tissue at both ends or in a band, 2.4–3.5 × 2.3–2.8 mm, nearly smooth.</text>
      <biological_entity id="o14313" name="band" name_original="band" src="d0_s15" type="structure" />
      <relation from="o14312" id="r1914" name="with soft , whitish tissue at both ends or in" negation="false" src="d0_s15" to="o14313" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 60.</text>
      <biological_entity id="o14312" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" notes="" src="d0_s15" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" notes="" src="d0_s15" to="2.8" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" notes="" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14314" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky woods and hillsides, scrub oak woods, pine woods and barrens, sand dunes, hummocks, shale barrens, roadsides, railroad rights-of-way, fields, and occasionally a weed in cultivated ground</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky woods" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="scrub oak" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="pine woods" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="hummocks" />
        <character name="habitat" value="shale barrens" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad rights-of-way" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="a weed" modifier="and occasionally" constraint="in cultivated ground" />
        <character name="habitat" value="cultivated ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ariz., Colo., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Miss., Mo., Nebr., N.C., N.J., N.Mex., N.Y., Okla., Pa., S.C., Tenn., Tex., Va., W.Va., Wis.; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <discussion>Commelina erecta grows in temperate regions of North and Central America, as well as in tropical regions.</discussion>
  <discussion>This is by far the most variable species of Commelina in the flora. Three freely intergrading varieties may be recognized, although they are of questionable significance: C. erecta var. erecta, with larger leaves lanceolate to lanceolate-ovate, (1.5–)2–4 cm wide, and spathes (2.2–)2.5–3.6 cm, occurs throughout our region; C. erecta var. angustifolia (Michaux) Fernald, with leaves linear to narrowly lanceolate, 0.3–1.5 cm wide, and spathes 1–2 cm, is mainly southern but extends as far north as Virginia; and C. erecta var. deamiana Fernald, with leaves linear to narrowly lanceolate, 0.5–1.7 cm wide, and spathes 2–3.5 cm, occurs in midwestern United States south to Texas.</discussion>
  
</bio:treatment>