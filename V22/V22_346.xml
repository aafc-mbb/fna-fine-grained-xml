<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">zosteraceae</taxon_name>
    <taxon_name authority="Hooker" date="1838" rank="genus">phyllospadix</taxon_name>
    <taxon_name authority="Hooker" date="1838" rank="species">scouleri</taxon_name>
    <place_of_publication>
      <publication_title>Flora Boreali-Americana</publication_title>
      <place_in_publication>2: 171. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zosteraceae;genus phyllospadix;species scouleri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220010385</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs;</text>
      <biological_entity id="o13377" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>nodes with 2 rows of 3–5 roots.</text>
      <biological_entity id="o13378" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o13379" name="row" name_original="rows" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o13380" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s1" to="5" />
      </biological_entity>
      <relation from="o13378" id="r1790" name="with" negation="false" src="d0_s1" to="o13379" />
      <relation from="o13379" id="r1791" name="part_of" negation="false" src="d0_s1" to="o13380" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: sheath 4–35 cm, margins not overlapping;</text>
      <biological_entity id="o13381" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13382" name="sheath" name_original="sheath" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13383" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade to 2 m × 1–4 mm, margins entire, apex obtuse to truncate or rarely slightly notched;</text>
      <biological_entity id="o13384" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o13385" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="length" src="d0_s3" to="2" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13386" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13387" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="truncate or rarely slightly notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>veins 3.</text>
      <biological_entity id="o13388" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13389" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Generative shoots 0.2–11 cm, nodes 1–2, proximal node when present with 1 leaf, sterile, distal node with 1 leaf and 1 (–2) spathes.</text>
      <biological_entity constraint="generative" id="o13390" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13391" name="node" name_original="nodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o13392" name="node" name_original="node" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when present with 1 leaf" name="reproduction" src="d0_s5" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13393" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o13394" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o13395" name="spathe" name_original="spathes" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <relation from="o13393" id="r1792" name="with" negation="false" src="d0_s5" to="o13394" />
      <relation from="o13393" id="r1793" name="with" negation="false" src="d0_s5" to="o13395" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: peduncles 11–60 × 1–2 mm;</text>
      <biological_entity id="o13396" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o13397" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s6" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>staminate bract 4–5.5 × 2–3 mm;</text>
      <biological_entity id="o13398" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o13399" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate bract 4–8 × 1.5–3 mm, base not narrowed, apex truncate to acute.</text>
      <biological_entity id="o13400" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o13401" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13402" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o13403" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits 4–5 × 5.5 mm.</text>
      <biological_entity id="o13404" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="5.5" value_original="5.5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late spring and summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Intertidal and upper part of sublittoral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="intertidal" constraint="of sublittoral" />
        <character name="habitat" value="upper part" constraint="of sublittoral" />
        <character name="habitat" value="sublittoral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-2 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2" to_unit="m" from="2" from_unit="m" constraint="- " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.; Mexico (in Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (in Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Scouler’s surf-grass</other_name>
  <references>
    <reference>Barnabas, A. D. 1994. Anatomical, histochemical and ultrastructural features of the seagrass Phyllospadix scouleri Hook. Aquatic Bot. 49: 167–182.</reference>
  </references>
  
</bio:treatment>