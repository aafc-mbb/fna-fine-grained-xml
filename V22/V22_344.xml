<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Loefling" date="1758" rank="genus">Callisia</taxon_name>
    <place_of_publication>
      <publication_title>Iter Hispanicum</publication_title>
      <place_in_publication>305. 1758</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus Callisia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kallos, beauty, referring to the attractive leaves</other_info_on_name>
    <other_info_on_name type="fna_id">105131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small" date="unknown" rank="genus">Cuthbertia</taxon_name>
    <taxon_hierarchy>genus Cuthbertia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Leiandra</taxon_name>
    <taxon_hierarchy>genus Leiandra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Phyodina</taxon_name>
    <taxon_hierarchy>genus Phyodina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Small" date="unknown" rank="genus">Tradescantella</taxon_name>
    <taxon_hierarchy>genus Tradescantella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial or rarely annual.</text>
      <biological_entity id="o12271" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots thin, rarely tuberous.</text>
      <biological_entity id="o12272" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spirally arranged or 2-ranked;</text>
      <biological_entity id="o12273" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s2" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade sessile.</text>
      <biological_entity id="o12274" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences terminal and/or axillary, cyme pairs (often aggregated into larger spikelike or paniclelike units), cymes sessile, umbellike, contracted, subtended by bracts;</text>
      <biological_entity id="o12275" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o12276" name="cyme" name_original="cyme" src="d0_s4" type="structure" />
      <biological_entity id="o12277" name="cyme" name_original="cymes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="umbel-like" value_original="umbellike" />
        <character is_modifier="false" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o12278" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <relation from="o12277" id="r1643" name="subtended by" negation="false" src="d0_s4" to="o12278" />
    </statement>
    <statement id="d0_s5">
      <text>bracts inconspicuous, less than 1 cm;</text>
      <biological_entity id="o12279" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>spathaceous bracts absent;</text>
      <biological_entity id="o12280" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="spathaceous" value_original="spathaceous" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles persistent.</text>
      <biological_entity id="o12281" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual (bisexual and pistillate in C. repens), radially symmetric;</text>
      <biological_entity id="o12282" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s8" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels very short or well developed;</text>
      <biological_entity id="o12283" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" modifier="well; well" name="development" src="d0_s9" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals distinct, subequal;</text>
      <biological_entity id="o12284" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals distinct, white or pink to rose [rarely blue], equal, not clawed;</text>
      <biological_entity id="o12285" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="rose" />
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 6 or 0–3, all fertile, equal;</text>
      <biological_entity id="o12286" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="6" value_original="6" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="3" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments glabrous or bearded;</text>
      <biological_entity id="o12287" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovary 2–3-locular, ovules [1–] 2 per locule, 1-seriate.</text>
      <biological_entity id="o12288" name="ovary" name_original="ovary" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="2-3-locular" value_original="2-3-locular" />
      </biological_entity>
      <biological_entity id="o12289" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s14" to="2" to_inclusive="false" />
        <character constraint="per locule" constraintid="o12290" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s14" value="1-seriate" value_original="1-seriate" />
      </biological_entity>
      <biological_entity id="o12290" name="locule" name_original="locule" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules 2–3-valved, 2–3-locular.</text>
      <biological_entity id="o12291" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-3-valved" value_original="2-3-valved" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="2-3-locular" value_original="2-3-locular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds [1-] 2 per locule;</text>
      <biological_entity id="o12292" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s16" to="2" to_inclusive="false" />
        <character constraint="per locule" constraintid="o12293" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12293" name="locule" name_original="locule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>hilum punctiform;</text>
      <biological_entity id="o12294" name="hilum" name_original="hilum" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>embryotega abaxial.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 6–8.</text>
      <biological_entity id="o12295" name="embryotegum" name_original="embryotega" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity constraint="x" id="o12296" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s19" to="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, tropical America; major center of distribution in Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="tropical America" establishment_means="native" />
        <character name="distribution" value="major center of distribution in Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <discussion>Species ca. 20 (7 in the flora).</discussion>
  <references>
    <reference>Anderson, E. and R. E. Woodson Jr. 1935. The species of Tradescantia indigenous to the United States. Contr. Arnold Arbor. 9: 1–132.</reference>
    <reference>Lakela, O. 1972. Field observations of Cuthbertia (Commelinaceae) with description of a new form. Sida 5: 26–32.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers white, filaments glabrous; plants creeping, or ascending and stoloniferous.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers pink to rose, filaments bearded; plants erect to ascending, or creeping.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Robust, stoloniferous plants to 1 m; leaves oblong to lanceolate-oblong, 15–30 cm; flowers fragrant</description>
      <determination>5 Callisia fragrans</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Weak, mat-forming plants; leaves lanceolate to ovate, 1–3.5 cm; flowers odorless.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences sessile in distal leaf axils; flowers sessile or subsessile; petals inconspicuous; stamens 0–6; ovary and capsule 2-locular</description>
      <determination>6 Callisia repens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences pedunculate; flowers distinctly pedicellate; petals conspicuous; stamens 6; ovary and capsule 3-locular</description>
      <determination>7 Callisia cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants creeping; leaves oblong-elliptic to lanceolate-oblong, 1–3.5 cm</description>
      <determination>4 Callisia micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Plants erect to ascending; leaves linear, mainly 5–25 cm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Distal leaf blades as wide as opened, flattened sheaths or wider, 0.4–1.5 cm wide</description>
      <determination>1 Callisia rosea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Distal leaf blades much narrower than opened, flattened sheaths, 0.1–0.5 mm wide.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants not cespitose or scarcely so; roots persistently woolly; bracts usually minute, scarious, 1–3(–7) mm</description>
      <determination>2 Callisia ornata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants cespitose; roots glabrous to sparsely puberulent; bracts often elongate, ± herbaceous, 2–14 mm</description>
      <determination>3 Callisia graminea</determination>
    </key_statement>
  </key>
</bio:treatment>