<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">263</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Luzula</taxon_name>
    <taxon_name authority="Blytt in M. N. Blytt and A. G. Blytt" date="1861" rank="species">arctica</taxon_name>
    <place_of_publication>
      <publication_title>in M. N. Blytt and A. G. Blytt,Norges Flora</publication_title>
      <place_in_publication>1: 299. 1861</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus luzula;species arctica;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000219</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Luzula</taxon_name>
    <taxon_name authority="(Laestadius) Beurling Bot. Not." date="unknown" rank="species">nivalis</taxon_name>
    <place_of_publication>
      <place_in_publication>55. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Luzula;species nivalis;</taxon_hierarchy>
  </taxon_identification>
  <number>14</number>
  <other_name type="common_name">Arctic wood rush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms densely cespitose, 5-20 cm.</text>
      <biological_entity id="o969" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: sheaths brown to straw-colored;</text>
      <biological_entity id="o970" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o971" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="straw-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal leaves to 10 cm × 4 mm;</text>
      <biological_entity id="o972" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o973" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="length" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character name="width" src="d0_s2" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline leaves usually 2, reduced.</text>
      <biological_entity id="o974" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o975" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences: glomerules 1-3, sessile;</text>
      <biological_entity id="o976" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o977" name="glomerule" name_original="glomerules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal inflorescence bract inconspicuous, brown, much shorter to ± equaling inflorescence, apex often clear, dentate;</text>
      <biological_entity id="o978" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="inflorescence" id="o979" name="bract" name_original="bract" src="d0_s5" type="structure" constraint_original="proximal inflorescence">
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="much" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o980" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s5" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o981" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="clear" value_original="clear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts deep brown, margins dentate;</text>
      <biological_entity id="o982" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o983" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="depth" src="d0_s6" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o984" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles deep brown, margins dentate.</text>
      <biological_entity id="o985" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o986" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o987" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals deep brown with narrow clear margins and apex, 1.7-2.1 mm;</text>
      <biological_entity id="o988" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o989" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character constraint="with apex" constraintid="o991" is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o990" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity id="o991" name="apex" name_original="apex" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>anthers ± equaling filament length.</text>
      <biological_entity id="o992" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o993" name="anther" name_original="anthers" src="d0_s9" type="structure" />
      <biological_entity id="o994" name="filament" name_original="filament" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsules dark reddish to blackish, shining, spheric, 1.8-2.1 mm, usually exceeding tepals.</text>
      <biological_entity id="o995" name="capsule" name_original="capsules" src="d0_s10" type="structure">
        <character char_type="range_value" from="dark reddish" name="coloration" src="d0_s10" to="blackish" />
        <character is_modifier="false" name="reflectance" src="d0_s10" value="shining" value_original="shining" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o996" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
      <relation from="o995" id="r130" modifier="usually" name="exceeding" negation="false" src="d0_s10" to="o996" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds translucent, clear brown, broadly elliptic, with few entangled hairs, 1-1.2 mm. 2n = 24.</text>
      <biological_entity id="o997" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="clear brown" value_original="clear brown" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o998" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="few" value_original="few" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="entangled" value_original="entangled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o999" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
      <relation from="o997" id="r131" name="with" negation="false" src="d0_s11" to="o998" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, stony places on slopes and in dwarf shrub heaths in alpine and arctic tundra; circumpolar.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony" modifier="wet" />
        <character name="habitat" value="slopes" modifier="places on" />
        <character name="habitat" value="dwarf shrub heaths" modifier="and in" constraint="in alpine and arctic tundra ; circumpolar" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="arctic tundra" />
        <character name="habitat" value="circumpolar" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>