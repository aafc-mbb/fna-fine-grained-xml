<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">277</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="F. Rudolphi" date="unknown" rank="family">sparganiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">sparganium</taxon_name>
    <taxon_name authority="Laestadius ex Beurling" date="1852" rank="species">hyperboreum</taxon_name>
    <place_of_publication>
      <publication_title>Wikstrom’s Arberattelse</publication_title>
      <place_in_publication>1850, Bihang 4. 1853 or 1854 Arsberatt. Bot. Arbet. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sparganiaceae;genus sparganium;species hyperboreum</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000369</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, grasslike, to 0.8 m;</text>
      <biological_entity id="o4360" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="grasslike" value_original="grasslike" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="0.8" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>leaves and inflorescences usually floating.</text>
      <biological_entity id="o4361" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
      <biological_entity id="o4362" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves limp, unkeeled, flat, 0.1–0.4 (–0.8) m × 1–5 mm.</text>
      <biological_entity id="o4363" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="limp" value_original="limp" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="unkeeled" value_original="unkeeled" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="m" name="atypical_length" src="d0_s2" to="0.8" to_unit="m" />
        <character char_type="range_value" from="0.1" from_unit="m" name="length" src="d0_s2" to="0.4" to_unit="m" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: rachis unbranched, flexuous;</text>
      <biological_entity id="o4364" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o4365" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts ascending, lower bracts slightly inflated near base;</text>
      <biological_entity id="o4366" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o4367" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4368" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character constraint="near base" constraintid="o4369" is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o4369" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pistillate heads 1–4, axillary, contiguous, sessile, or most proximal peduncled and supra-axillary, 0.5–1.4 cm diam. in fruit;</text>
      <biological_entity id="o4370" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o4371" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="contiguous" value_original="contiguous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="peduncled" value_original="peduncled" />
        <character is_modifier="false" name="position" src="d0_s5" value="supra-axillary" value_original="supra-axillary" />
        <character char_type="range_value" constraint="in fruit" constraintid="o4372" from="0.5" from_unit="cm" name="diameter" src="d0_s5" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4372" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>staminate heads 1 (–2), terminal, contiguous or not with distalmost pistillate head.</text>
      <biological_entity id="o4373" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o4374" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="contiguous" value_original="contiguous" />
        <character name="arrangement" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o4375" name="head" name_original="head" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o4374" id="r579" name="with" negation="false" src="d0_s6" to="o4375" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: tepals without subapical dark spot, erose;</text>
      <biological_entity id="o4376" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4377" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" notes="" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o4378" name="dark-spot" name_original="dark spot" src="d0_s7" type="structure" />
      <relation from="o4377" id="r580" name="without" negation="false" src="d0_s7" to="o4378" />
    </statement>
    <statement id="d0_s8">
      <text>stigmas 1, ovate.</text>
      <biological_entity id="o4379" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4380" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits brown or yellowish, dull, subsessile, body ellipsoid to obovoid, not faceted, ±more or less constricted at equator, 2–5 × 1.5–2.5 mm;</text>
      <biological_entity id="o4381" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="dull" value_original="dull" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o4382" name="body" name_original="body" src="d0_s9" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s9" to="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="faceted" value_original="faceted" />
        <character constraint="at equator" constraintid="o4383" is_modifier="false" modifier="more or less more or less" name="size" src="d0_s9" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" notes="" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4383" name="equator" name_original="equator" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>beak less than 0.5 mm, or absent;</text>
      <biological_entity id="o4384" name="beak" name_original="beak" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals attached at base, not reaching equator.</text>
      <biological_entity id="o4385" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character constraint="at base" constraintid="o4386" is_modifier="false" name="fixation" src="d0_s11" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o4386" name="base" name_original="base" src="d0_s11" type="structure" />
      <biological_entity id="o4387" name="equator" name_original="equator" src="d0_s11" type="structure" />
      <relation from="o4385" id="r581" name="reaching" negation="true" src="d0_s11" to="o4387" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1.2n = 30.</text>
      <biological_entity id="o4388" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4389" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cold, quiet, shallow, oligo- to mesotrophic arctic-alpine waters</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cold" />
        <character name="habitat" value="quiet" />
        <character name="habitat" value="shallow" />
        <character name="habitat" value="oligo" />
        <character name="habitat" value="mesotrophic arctic-alpine waters" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Yukon; Alaska; circumboreal.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="circumboreal" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Rubanier hyperboréal</other_name>
  <discussion>Sparganium hyperboreum is distinguished from other floating-leaved species by its beakless fruits with sessile stigmas.</discussion>
  <discussion>Putative hybrids between Sparganium hyperboreum and S. natans have been found in Manitoba, Newfoundland, Northwest Territories, and Alaska by V. L. Harms (1973), who discussed variation in both species. The hybrids have wider (2–5 mm) leaves, golden-brown fruits with short (1 mm) beaks, and supra-axillary pistillate heads (V. L. Harms 1973; C. D. K. Cook and M. S. Nicholls 1986).</discussion>
  <references>
    <reference>Beal, E. O. 1977. A Manual of Marsh and Aquatic Vascular Plants of North Carolina, with Habitat Data. Raleigh. [North Carolina Agric. Exp. Sta. Techn. Bull. 247.]</reference>
    <reference>Larson, G. E. 1993. Aquatic and Wetland Vascular Plants of the Northern Great Plains. Fort Collins, Colo. [U.S.D.A. Forest Serv., Gen. Techn. Rep. RM-238.]</reference>
    <reference>L&amp;ouml;ve, &amp;Aacute;. and D. L&amp;ouml;ve. 1981. In: IOPB chromosome number reports LXXII. Taxon 30: 694–708.</reference>
    <reference>Mason, H. L. 1957. A Flora of the Marshes of California. Berkeley.</reference>
    <reference>Brayshaw, T. C. 1985. Pondweeds and Bur-reeds, and Their Relatives: Aquatic Families of Vascular Plants in British Columbia. Victoria. [Brit. Columbia Prov. Mus., Occas. Pap. 26.]</reference>
    <reference>Cook, C. D. K. 1985. Sparganium: Some old names and their types. Bot. Jahrb. Syst. 107: 269–276.</reference>
    <reference>Müller-Doblies, D. 1970. Taylor, R. L. and G. A. Mulligan. 1968. Flora of the Queen Charlotte Islands. Part 2. Cytological Aspects of the Vascular Plants. Ottawa.</reference>
    <reference>Voss, E. G. 1966. Nomenclatural notes on monocots. Rhodora 68: 435–463.</reference>
  </references>
  
</bio:treatment>