<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Kral" date="1978" rank="species">tennesseensis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>80: 444, figs. a–e. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species tennesseensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000483</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, 30–70 cm, base bulbous in maturity.</text>
      <biological_entity id="o5501" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o5502" name="base" name_original="base" src="d0_s0" type="structure">
        <character constraint="in maturity" is_modifier="false" name="architecture" src="d0_s0" value="bulbous" value_original="bulbous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems compact.</text>
      <biological_entity id="o5503" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves ascending, 15–45 cm;</text>
      <biological_entity id="o5504" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths soft, base pink or red;</text>
      <biological_entity id="o5505" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o5506" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade deep green, linear, flattened, somewhat twisted, 5–10 mm wide, smooth, margins somewhat scabrous.</text>
      <biological_entity id="o5507" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s4" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o5508" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths exceeded by leaves;</text>
      <biological_entity id="o5509" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o5510" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o5511" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o5510" id="r740" name="exceeded by" negation="false" src="d0_s5" to="o5511" />
    </statement>
    <statement id="d0_s6">
      <text>scapes linear, terete, distally somewhat compressed, 1 mm wide, 2–5-ribbed, ribs papillate;</text>
      <biological_entity id="o5512" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o5513" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="distally somewhat" name="shape" src="d0_s6" value="compressed" value_original="compressed" />
        <character name="width" src="d0_s6" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-5-ribbed" value_original="2-5-ribbed" />
      </biological_entity>
      <biological_entity id="o5514" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes ellipsoid to broadly ovoid, 10–15 mm;</text>
      <biological_entity id="o5515" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o5516" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="broadly ovoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts (5.5–) 6–6.5 (–7) mm wide, margins entire, apex rounded.</text>
      <biological_entity id="o5517" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o5518" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s8" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="width" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s8" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5519" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5520" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included, slightly curved, (4.5–) 5 mm wide, keel scarious, lacerate;</text>
      <biological_entity id="o5521" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o5522" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o5523" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding midday, blade obovate, 4.5 mm;</text>
      <biological_entity id="o5524" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5525" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
        <character is_modifier="false" name="duration" src="d0_s10" value="midday" value_original="midday" />
      </biological_entity>
      <biological_entity id="o5526" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="4.5" value_original="4.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o5527" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o5528" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds opaque, ellipsoid, 0.5-0.6 mm, finely and irregularly ridged.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o5529" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="finely; irregularly" name="shape" src="d0_s12" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5530" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fens and seeps over calcareous rock, streambanks in calcareous districts</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fens" constraint="over calcareous rock" />
        <character name="habitat" value="seeps" constraint="over calcareous rock" />
        <character name="habitat" value="calcareous rock" />
        <character name="habitat" value="streambanks" constraint="in calcareous districts" />
        <character name="habitat" value="calcareous districts" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–250 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="250" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15</number>
  <discussion>Xyris tennesseensis is unusual in habitat since because its sites are usually basic and its associates are fen or basic soil plants.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>