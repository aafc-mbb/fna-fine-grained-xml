<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Greene" date="1890" rank="species">uncialis</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 105. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species uncialis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000192</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual, cespitose, 0.8–3.5 cm.</text>
      <biological_entity id="o11421" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s0" to="3.5" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 70, more than 0.2 mm diam..</text>
      <biological_entity id="o11422" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="70" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s1" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves to 2.2 cm, 1/2–3/4 height of plant.</text>
      <biological_entity id="o11423" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1/2 height of plant" modifier="to 2.2 cm" name="height" src="d0_s2" to="3/4 height of plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal solitary flowers;</text>
      <biological_entity id="o11424" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity constraint="terminal" id="o11425" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts subtending inflorescence 1, widely truncate, enwrapping culm, inconspicuous, 0.25–0.9 mm, membranous, apex widely truncate, completely sheathing culm..</text>
      <biological_entity id="o11426" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" modifier="widely" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s4" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o11427" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
      <biological_entity id="o11428" name="culm" name_original="culm" src="d0_s4" type="structure" />
      <biological_entity id="o11429" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="widely" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o11430" name="culm" name_original="culm" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="completely" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <relation from="o11426" id="r1504" name="subtending" negation="false" src="d0_s4" to="o11427" />
      <relation from="o11426" id="r1505" name="enwrapping" negation="false" src="d0_s4" to="o11428" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 3-merous,: tepals erect to recurved at maturity, greenish or tinged red, 2–4 × 0.6–0.9 mm;</text>
      <biological_entity id="o11431" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o11432" name="tepal" name_original="tepals" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="at maturity" from="erect" name="orientation" src="d0_s5" to="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="tinged red" value_original="tinged red" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer and inner series nearly equal, apex acutish;</text>
      <biological_entity id="o11433" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity constraint="outer and inner" id="o11434" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11435" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acutish" value_original="acutish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens usually 3, filaments 0.9–1.6 mm, anthers 0.3–0.4 mm, 1/3 length of filaments;</text>
      <biological_entity id="o11436" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o11437" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o11438" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11439" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.4" to_unit="mm" />
        <character name="length" src="d0_s7" value="1/3 length of filaments" value_original="1/3 length of filaments" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>style 0.1–0.3 mm, stigma 0.4–1.3 mm.</text>
      <biological_entity id="o11440" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-merous" value_original="3-merous" />
      </biological_entity>
      <biological_entity id="o11441" name="style" name_original="style" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11442" name="stigma" name_original="stigma" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules reddish to maroon, 3-locular, ovoid to ellipsoid, 1.8–3.2 × 1–2.5 mm, nearly equal or shorter than the tepals.</text>
      <biological_entity id="o11443" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s9" to="maroon" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s9" value="3-locular" value_original="3-locular" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s9" to="ellipsoid" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="than the tepals" constraintid="o11444" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11444" name="tepal" name_original="tepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Seeds ovoid, 0.3–0.4 mm. n = 16.</text>
      <biological_entity id="o11445" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="n" id="o11446" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins of vernal pools and ponds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of vernal pools and ponds" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="ponds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47</number>
  <other_name type="common_name">Inch-high rush</other_name>
  
</bio:treatment>