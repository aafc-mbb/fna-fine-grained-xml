<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Juss." date="unknown" rank="family">araceae</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Peltandra</taxon_name>
    <place_of_publication>
      <publication_title>Journal de Physique, de Chimie, d'Histoire Naturelle et des Arts</publication_title>
      <place_in_publication>89:103. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family araceae;genus Peltandra</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pelte, small shield, and andros, male, referring to the shield-shaped tops of the staminate flowers</other_info_on_name>
    <other_info_on_name type="fna_id">124302</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, wetland.</text>
      <biological_entity id="o3247" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes vertical.</text>
      <biological_entity id="o3248" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves appearing before flowers, several, clustered apically, erect;</text>
      <biological_entity id="o3249" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" modifier="apically" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o3250" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <relation from="o3249" id="r438" name="appearing before" negation="false" src="d0_s2" to="o3250" />
    </statement>
    <statement id="d0_s3">
      <text>petiole equal to or longer than blade;</text>
      <biological_entity id="o3251" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="equal" value_original="equal" />
        <character constraint="than blade" constraintid="o3252" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3252" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade green or glaucous light green, simple, not peltate, lanceolate to widely ovate, base hastate to sagittate, rarely cordate, apex acuminate to rounded or mucronate;</text>
      <biological_entity id="o3253" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="light green" value_original="light green" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s4" value="peltate" value_original="peltate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="widely ovate" />
      </biological_entity>
      <biological_entity id="o3254" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="hastate" name="shape" src="d0_s4" to="sagittate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o3255" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="rounded or mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral-veins parallel.</text>
      <biological_entity id="o3256" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences: peduncle recurving in fruit, half ½ as long as to slightly longer than petiole, apex not swollen;</text>
      <biological_entity id="o3257" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o3258" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o3259" is_modifier="false" name="orientation" src="d0_s6" value="recurving" value_original="recurving" />
      </biological_entity>
      <biological_entity id="o3259" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity id="o3261" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3260" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o3262" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o3258" id="r439" name="as long as" negation="false" src="d0_s6" to="o3261" />
    </statement>
    <statement id="d0_s7">
      <text>spathe tube green, enclosing base of spadix;</text>
      <biological_entity id="o3263" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="spathe" id="o3264" name="tube" name_original="tube" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o3265" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o3266" name="spadix" name_original="spadix" src="d0_s7" type="structure" />
      <relation from="o3264" id="r440" name="enclosing" negation="false" src="d0_s7" to="o3265" />
      <relation from="o3264" id="r441" name="part_of" negation="false" src="d0_s7" to="o3266" />
    </statement>
    <statement id="d0_s8">
      <text>spathe blade green to white, opening slightly to fully at anthesis;</text>
      <biological_entity id="o3267" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity constraint="spathe" id="o3268" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>spadix cylindric.</text>
      <biological_entity id="o3269" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity id="o3270" name="spadix" name_original="spadix" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers unisexual, staminate and pistillate on same plant,, pistillate flowers covering basal portion of spadix,, staminate flowers apical, consisting of 4–5 connate stamens forming flat-topped synandrium;</text>
      <biological_entity id="o3271" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character constraint="on plant" constraintid="o3272" is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o3272" name="plant" name_original="plant" src="d0_s10" type="structure" />
      <biological_entity id="o3273" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o3274" name="portion" name_original="portion" src="d0_s10" type="structure" />
      <biological_entity id="o3275" name="spadix" name_original="spadix" src="d0_s10" type="structure" />
      <biological_entity id="o3276" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="position" src="d0_s10" value="apical" value_original="apical" />
      </biological_entity>
      <biological_entity id="o3277" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o3278" name="synandrium" name_original="synandrium" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="flat-topped" value_original="flat-topped" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character is_modifier="true" name="fusion" src="d0_s10" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o3273" id="r442" name="covering" negation="false" src="d0_s10" to="o3274" />
      <relation from="o3273" id="r443" name="part_of" negation="false" src="d0_s10" to="o3275" />
      <relation from="o3276" id="r444" name="consisting of" negation="false" src="d0_s10" to="o3277" />
      <relation from="o3276" id="r445" name="consisting of" negation="false" src="d0_s10" to="o3278" />
    </statement>
    <statement id="d0_s11">
      <text>sterile flowers proximal to and usually distal to staminate flowers;</text>
      <biological_entity id="o3279" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="usually" name="position_or_shape" src="d0_s11" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o3280" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth absent.</text>
      <biological_entity id="o3281" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits not embedded in spadix, red or green to dark purple-green.</text>
      <biological_entity id="o3282" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s13" to="dark purple-green" />
      </biological_entity>
      <biological_entity id="o3283" name="spadix" name_original="spadix" src="d0_s13" type="structure" />
      <relation from="o3282" id="r446" name="embedded in" negation="true" src="d0_s13" to="o3283" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds 1–2 (–4), mucilage present.</text>
      <biological_entity id="o3284" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>x = 14.</text>
      <biological_entity id="o3285" name="mucilage" name_original="mucilage" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3286" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Eastern North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eastern North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Arrow arum</other_name>
  <discussion>Only one species of Peltandra, P. virginica, (with two subspecies), was recognized by W. H. Blackwell and K. P. Blackwell (1974), a treatment followed by some botanists. They synonymized P. luteospadix with P. sagittifolia, based primarily on their contention that no Peltandra species have red fruits, and called that taxon P. virginica subsp. luteospadix (Fernald) Blackwell &amp; Blackwell. All of the specimens cited in their treatment of the genus are P. virginica.</discussion>
  <discussion>The two species of Peltandra can be distinguished not only on reproductive charactersistics, but also on features of leaf venation. Fossil leaves congeneric with modern Peltandra are known from very late Paleocene through early Eocene deposits of North America. Peltandra is one of two genera of Araceae endemic to the flora area (the other is Orontium).</discussion>
  <discussion>Species 2 (2 in the flora).</discussion>
  <references>
    <reference>Blackwell, W. H., Jr. and K. P. Blackwell. 1974. The taxonomy of Peltandra (Araceae). J. Elisha Mitchell Sci. Soc. 90: 137–140.</reference>
    <reference>Thompson, S. A. 1995. Systematics and biology of the Araceae and Acoraceae of temperate North America. Ph.D. dissertation, University of Illinois.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral leaf veins of ± same thickness; spadix ±about 1/2 as long as spathe; spathe blade white; fruits red</description>
      <determination>1 Peltandra sagittifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lateral leaf veins of 2 different thicknesses; spadix more than 1/2 to almost as long as spathe; spathe blade green to green with white or yellow-green along margins; fruits pea green to mottled green or dark purple-green</description>
      <determination>2 Peltandra virginica</determination>
    </key_statement>
  </key>
</bio:treatment>