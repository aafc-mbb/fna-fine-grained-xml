<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">lemnaceae</taxon_name>
    <taxon_name authority="(Hegelmaier) Hegelmaier" date="1895" rank="genus">wolffiella</taxon_name>
    <taxon_name authority="(Philippi) Hegelmaier" date="1895" rank="species">oblonga</taxon_name>
    <place_of_publication>
      <publication_title>Botanische Jahrbucher fur Systematik, Pflanzengeschichte und Pflanzengeographie</publication_title>
      <place_in_publication>21: 303. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lemnaceae;genus wolffiella;species oblonga</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220014348</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lemna</taxon_name>
    <taxon_name authority="Philippi" date="unknown" rank="species">oblonga</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>29: 45. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lemna;species oblonga;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Fronds 1 or 2–8 coherent together in often starlike groups, narrowly tongue-shaped to ribbonlike, 1.2–7.5 mm, 3–8 times as long as wide, rounded at tip or pointed;</text>
      <biological_entity id="o5231" name="frond" name_original="fronds" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s0" to="8" />
        <character constraint="in groups" constraintid="o5232" is_modifier="false" name="fusion" src="d0_s0" value="coherent" value_original="coherent" />
        <character char_type="range_value" from="narrowly tongue-shaped" name="shape" notes="" src="d0_s0" to="ribbonlike" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s0" to="7.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s0" value="3-8" value_original="3-8" />
        <character constraint="at tip" constraintid="o5233" is_modifier="false" name="shape" src="d0_s0" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pointed" value_original="pointed" />
      </biological_entity>
      <biological_entity id="o5232" name="group" name_original="groups" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="often" name="shape" src="d0_s0" value="starlike" value_original="starlike" />
      </biological_entity>
      <biological_entity id="o5233" name="tip" name_original="tip" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>tract of elongated cells running along or near edge of lower wall of pouch;</text>
      <biological_entity id="o5234" name="tract" name_original="tract" src="d0_s1" type="structure" constraint="cell" constraint_original="cell; cell" />
      <biological_entity id="o5235" name="cell" name_original="cells" src="d0_s1" type="structure">
        <character is_modifier="true" name="length" src="d0_s1" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o5236" name="edge" name_original="edge" src="d0_s1" type="structure" />
      <biological_entity constraint="lower" id="o5237" name="wall" name_original="wall" src="d0_s1" type="structure" />
      <biological_entity id="o5238" name="pouch" name_original="pouch" src="d0_s1" type="structure" />
      <relation from="o5234" id="r694" name="part_of" negation="false" src="d0_s1" to="o5235" />
      <relation from="o5234" id="r695" name="running along" negation="false" src="d0_s1" to="o5236" />
      <relation from="o5236" id="r696" name="part_of" negation="false" src="d0_s1" to="o5237" />
      <relation from="o5236" id="r697" name="part_of" negation="false" src="d0_s1" to="o5238" />
    </statement>
    <statement id="d0_s2">
      <text>area of air spaces within frond much longer than wide, often spreading throughout most of frond;</text>
      <biological_entity id="o5239" name="area" name_original="area" src="d0_s2" type="structure" constraint="air; air">
        <character constraint="of frond" constraintid="o5243" is_modifier="false" modifier="often" name="orientation" notes="" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o5240" name="air" name_original="air" src="d0_s2" type="structure" />
      <biological_entity id="o5241" name="space" name_original="spaces" src="d0_s2" type="structure" />
      <biological_entity id="o5242" name="frond" name_original="frond" src="d0_s2" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s2" value="much longer than wide" value_original="much longer than wide" />
      </biological_entity>
      <biological_entity id="o5243" name="frond" name_original="frond" src="d0_s2" type="structure" />
      <relation from="o5239" id="r698" name="part_of" negation="false" src="d0_s2" to="o5240" />
      <relation from="o5239" id="r699" name="part_of" negation="false" src="d0_s2" to="o5241" />
      <relation from="o5239" id="r700" name="within" negation="false" src="d0_s2" to="o5242" />
    </statement>
    <statement id="d0_s3">
      <text>angle of pouch 45°–90°;</text>
      <biological_entity id="o5244" name="angle" name_original="angle" src="d0_s3" type="structure" constraint="pouch" constraint_original="pouch; pouch">
        <character name="degree" src="d0_s3" value="45°-90°" value_original="45°-90°" />
      </biological_entity>
      <biological_entity id="o5245" name="pouch" name_original="pouch" src="d0_s3" type="structure" />
      <relation from="o5244" id="r701" name="part_of" negation="false" src="d0_s3" to="o5245" />
    </statement>
    <statement id="d0_s4">
      <text>flowering fronds similar to vegetative ones.</text>
      <biological_entity id="o5246" name="frond" name_original="fronds" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o5247" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o5246" id="r702" name="to" negation="false" src="d0_s4" to="o5247" />
    </statement>
    <statement id="d0_s5">
      <text>Fruits 0.3–0.4 mm. 2n = 40, 42.</text>
      <biological_entity id="o5248" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5249" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="40" value_original="40" />
        <character name="quantity" src="d0_s5" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (very rare) throughout year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mesotrophic, quiet waters in warm-temperate to subtropical regions with mild winters</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="quiet waters" modifier="mesotrophic" constraint="in warm-temperate to subtropical regions with mild winters" />
        <character name="habitat" value="warm-temperate" />
        <character name="habitat" value="subtropical regions" constraint="with mild winters" />
        <character name="habitat" value="mild winters" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Fla., La., Miss., Tex.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  
</bio:treatment>