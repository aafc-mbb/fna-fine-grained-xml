<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="F. Rudolphi" date="unknown" rank="family">sparganiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">sparganium</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">angustifolium</taxon_name>
    <place_of_publication>
      <publication_title>Flora Boreali-Americana</publication_title>
      <place_in_publication>2: 189. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sparganiaceae;genus sparganium;species angustifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000364</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sparganium</taxon_name>
    <taxon_name authority="Rehmann" date="unknown" rank="species">angustifolium</taxon_name>
    <taxon_name authority="(Morong) Brayshaw" date="unknown" rank="variety">multipedunculatum</taxon_name>
    <taxon_hierarchy>genus Sparganium;species angustifolium;variety multipedunculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sparganium</taxon_name>
    <taxon_name authority="(Morong) Rydberg" date="unknown" rank="species">emersum</taxon_name>
    <taxon_name authority="(Morong) Reveal" date="unknown" rank="variety">multipedunculatum</taxon_name>
    <taxon_hierarchy>genus Sparganium;species emersum;variety multipedunculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sparganium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">multipedunculatum</taxon_name>
    <place_of_publication>
      <publication_title>S. simplex Hudson var. multipedunculatum Morong</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Sparganium;species multipedunculatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, to more than 2 m long;</text>
      <biological_entity id="o444" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character constraint="to 2+ m" is_modifier="false" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>leaves and inflorescences usually floating.</text>
      <biological_entity id="o445" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
      <biological_entity id="o446" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves limp, unkeeled, flat to planoconvex, 0.2–0.8 (–2.5) m × 2–5 (–10) mm.</text>
      <biological_entity id="o447" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="limp" value_original="limp" />
        <character is_modifier="false" name="shape" src="d0_s2" value="unkeeled" value_original="unkeeled" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="planoconvex" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="m" name="atypical_length" src="d0_s2" to="2.5" to_unit="m" />
        <character char_type="range_value" from="0.2" from_unit="m" name="length" src="d0_s2" to="0.8" to_unit="m" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: rachis unbranched, flexuous, its fertile part usually erect at water surface;</text>
      <biological_entity id="o448" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o449" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o450" name="part" name_original="part" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="fertile" value_original="fertile" />
        <character constraint="at surface" constraintid="o451" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o451" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s3" value="water" value_original="water" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>bracts ascending, lower bracts inflated near base;</text>
      <biological_entity id="o452" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o453" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o454" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character constraint="near base" constraintid="o455" is_modifier="false" name="shape" src="d0_s4" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o455" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>pistillate heads 2–5, at least some supra-axillary, not contiguous, sessile or most proximal peduncled (often prominently so in deeper water), 1–3 cm diam. in fruit;</text>
      <biological_entity id="o456" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o457" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" modifier="at-least" name="position" src="d0_s5" value="supra-axillary" value_original="supra-axillary" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s5" value="contiguous" value_original="contiguous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="peduncled" value_original="peduncled" />
        <character char_type="range_value" constraint="in fruit" constraintid="o458" from="1" from_unit="cm" name="diameter" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o458" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>staminate heads (1–) 2–4, contiguous and appearing as one elongate head, not contiguous with distalmost pistillate head.</text>
      <biological_entity id="o459" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o460" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="contiguous" value_original="contiguous" />
        <character constraint="with distalmost head" constraintid="o462" is_modifier="false" modifier="not" name="arrangement" notes="" src="d0_s6" value="contiguous" value_original="contiguous" />
      </biological_entity>
      <biological_entity id="o461" name="head" name_original="head" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o462" name="head" name_original="head" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o461" id="r63" name="appearing as" negation="false" src="d0_s6" to="o461" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: tepals without subapical dark spot, erose-tipped, stigma 1, lanceovate.</text>
      <biological_entity id="o463" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o464" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="lanceovate" value_original="lanceovate" />
      </biological_entity>
      <biological_entity id="o465" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="subapical" value_original="subapical" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="dark spot" value_original="dark spot" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="erose-tipped" value_original="erose-tipped" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o464" id="r64" name="without" negation="false" src="d0_s7" to="o465" />
    </statement>
    <statement id="d0_s8">
      <text>Fruits reddish to brownish, dull, short-stipitate, ellipsoid to fusiform, not faceted, body constricted at equator, 3–7 mm, tapering to beak;</text>
      <biological_entity id="o466" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s8" to="brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="short-stipitate" value_original="short-stipitate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s8" to="fusiform" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="faceted" value_original="faceted" />
      </biological_entity>
      <biological_entity id="o467" name="body" name_original="body" src="d0_s8" type="structure">
        <character constraint="at equator" constraintid="o468" is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="7" to_unit="mm" />
        <character constraint="to beak" constraintid="o469" is_modifier="false" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o468" name="equator" name_original="equator" src="d0_s8" type="structure" />
      <biological_entity id="o469" name="beak" name_original="beak" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>beak straight, 1.5–2 mm;</text>
      <biological_entity id="o470" name="beak" name_original="beak" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals attached at base, reaching about to equator.</text>
      <biological_entity id="o471" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o472" is_modifier="false" name="fixation" src="d0_s10" value="attached" value_original="attached" />
        <character is_modifier="false" name="position_relational" notes="" src="d0_s10" value="reaching" value_original="reaching" />
      </biological_entity>
      <biological_entity id="o472" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds 1.2n = 30.</text>
      <biological_entity id="o473" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="2n" id="o474" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Jun–Oct southwestward, Jul–Aug northward).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" modifier="southwestward" to="Oct" from="Jun" />
        <character name="flowering time" char_type="range_value" modifier="northward" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid, oligotrophic waters of lakes, ponds, ditches, and streams, usually in shallow waters but to 2.5 m deep</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid" constraint="of lakes , ponds , ditches , and streams , usually in shallow waters but to 2.5" />
        <character name="habitat" value="oligotrophic waters" constraint="of lakes , ponds , ditches , and streams , usually in shallow waters but to 2.5" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="shallow waters" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Idaho, Maine, Mass., Mich., Minn., Mont., Nev., N.H., N.Mex., N.Y., Oreg., Utah, Vt., Wash., Wis., Wyo.; circumboreal.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="circumboreal" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Rubanier à feuilles étroites</other_name>
  <discussion>Sparganium angustifolium is sometimes abundant, its leaves then covering the surface. It is a relatively invariable species that forms fertile hybrids with S. emersum (C. D. K. Cook and M. S. Nicholls 1986), from which it is distinguished by its contiguous staminate heads and flat to plano-convex leaves. See the discussion under S. emersum.</discussion>
  
</bio:treatment>