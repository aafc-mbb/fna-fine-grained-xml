<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">zannichelliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Zannichellia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 969. 1753; Gen. Pl. ed. 5; 416, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family zannichelliaceae;genus Zannichellia</taxon_hierarchy>
    <other_info_on_name type="etymology">for Gian Girolamo Zannichelli, 1662–1729, Venetian apothecary and botanist</other_info_on_name>
    <other_info_on_name type="fna_id">135257</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots single or in pairs at nodes.</text>
      <biological_entity id="o11048" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="in pairs" value_original="in pairs" />
      </biological_entity>
      <biological_entity id="o11049" name="pair" name_original="pairs" src="d0_s0" type="structure" />
      <biological_entity id="o11050" name="node" name_original="nodes" src="d0_s0" type="structure" />
      <relation from="o11048" id="r1458" name="in" negation="false" src="d0_s0" to="o11049" />
      <relation from="o11049" id="r1459" name="at" negation="false" src="d0_s0" to="o11050" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade entire, veins 1 [–3], nearly terete.</text>
      <biological_entity id="o11051" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o11052" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11053" name="vein" name_original="veins" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="3" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescence usually of 2 flowers, 1 staminate, 1 pistillate.</text>
      <biological_entity id="o11054" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="staminate" value_original="staminate" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11055" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <relation from="o11054" id="r1460" name="consist_of" negation="false" src="d0_s2" to="o11055" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers short-pedicellate.</text>
      <biological_entity id="o11056" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-pedicellate" value_original="short-pedicellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers: anthers 4-loculed.</text>
      <biological_entity id="o11057" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o11058" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="4-loculed" value_original="4-loculed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers: pistils surrounded proximally by membranaceous envelope;</text>
      <biological_entity id="o11059" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11060" name="pistil" name_original="pistils" src="d0_s5" type="structure">
        <character constraint="by envelope" constraintid="o11061" is_modifier="false" name="position_relational" src="d0_s5" value="surrounded" value_original="surrounded" />
      </biological_entity>
      <biological_entity id="o11061" name="envelope" name_original="envelope" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="membranaceous" value_original="membranaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stigma asymmetrically funnel-shaped.</text>
      <biological_entity id="o11062" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11063" name="stigma" name_original="stigma" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="asymmetrically" name="shape" src="d0_s6" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Fruits: convex margin minutely dentate;</text>
      <biological_entity id="o11064" name="fruit" name_original="fruits" src="d0_s7" type="structure" />
      <biological_entity id="o11065" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>endocarp often coarsely papillose (visible after decay of outer fruit wall);</text>
      <biological_entity id="o11066" name="fruit" name_original="fruits" src="d0_s8" type="structure" />
      <biological_entity id="o11067" name="endocarp" name_original="endocarp" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often coarsely" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stipe developing into podogyne (stalk supporting pistil).</text>
      <biological_entity id="o11068" name="fruit" name_original="fruits" src="d0_s9" type="structure" />
      <biological_entity id="o11069" name="stipe" name_original="stipe" src="d0_s9" type="structure">
        <character constraint="into podogyne" constraintid="o11070" is_modifier="false" name="development" src="d0_s9" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity id="o11070" name="podogyne" name_original="podogyne" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Horned-pondweed</other_name>
  <discussion>Species ca. 5 (1 in the flora).</discussion>
  <references>
    <reference>Van Vierssen, W. 1982. The ecology of communities dominated by Zannichellia taxa in western Europe. I. Characterization and autecology of the Zannichellia taxa. Aquatic Bot. 12: 103–155.</reference>
  </references>
  
</bio:treatment>