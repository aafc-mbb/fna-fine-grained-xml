<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="Hellquist &amp; R. L. Hilton" date="1983" rank="species">ogdenii</taxon_name>
    <place_of_publication>
      <publication_title>Systematic Botany</publication_title>
      <place_in_publication>8: 88, figs. 1–3, plates 1–2, figs. 1–3. 1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species ogdenii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">222000298</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes absent.</text>
      <biological_entity id="o2615" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems compressed-filiform, without spots, to 50 cm;</text>
      <biological_entity constraint="cauline" id="o2616" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed-filiform" value_original="compressed-filiform" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2617" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o2616" id="r339" name="without" negation="false" src="d0_s1" to="o2617" />
    </statement>
    <statement id="d0_s2">
      <text>glands green, golden brown to dark-brown, 0.2–0.6 mm diam.</text>
      <biological_entity id="o2618" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character char_type="range_value" from="golden brown" name="coloration" src="d0_s2" to="dark-brown" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Turions terminal or lateral, uncommon, 3.7–9.9 × 2.6–6 cm, soft to hard;</text>
      <biological_entity id="o2619" name="turion" name_original="turions" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="uncommon" value_original="uncommon" />
        <character char_type="range_value" from="3.7" from_unit="cm" name="length" src="d0_s3" to="9.9" to_unit="cm" />
        <character char_type="range_value" from="2.6" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="soft" name="texture" src="d0_s3" to="hard" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves flattened with outer and inner leaves in same plane;</text>
      <biological_entity id="o2620" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="with outer inner leaves" constraintid="o2621" is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="outer and inner" id="o2621" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>outer leaves 1–2 per side, base not corrugate, apex apiculate;</text>
      <biological_entity constraint="outer" id="o2622" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o2623" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o2623" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o2624" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
      </biological_entity>
      <biological_entity id="o2625" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner leaves undifferentiated or rolled into hardened, fusiform structure.</text>
      <biological_entity constraint="inner" id="o2626" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="undifferentiated" value_original="undifferentiated" />
        <character constraint="into structure" constraintid="o2627" is_modifier="false" name="shape" src="d0_s6" value="rolled" value_original="rolled" />
      </biological_entity>
      <biological_entity id="o2627" name="structure" name_original="structure" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="hardened" value_original="hardened" />
        <character is_modifier="true" name="shape" src="d0_s6" value="fusiform" value_original="fusiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves submersed, spirally arranged, sessile, rigid;</text>
      <biological_entity id="o2628" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="location" src="d0_s7" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules persistent, inconspicuous, convolute, free from blade, brown or rarely white, not ligulate, 0.9–2.1 cm, slightly fibrous, partially shredding at tip, apex obtuse;</text>
      <biological_entity id="o2629" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="convolute" value_original="convolute" />
        <character constraint="from blade" constraintid="o2630" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="some_measurement" src="d0_s8" to="2.1" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s8" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o2630" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o2631" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o2632" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="partially" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o2629" id="r340" modifier="partially" name="shredding at" negation="false" src="d0_s8" to="o2631" />
    </statement>
    <statement id="d0_s9">
      <text>blade somewhat reddish to olive-green, linear, not arcuate, 1.5–10 cm × 1.2–2.9 mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, cuspidate to bristle-tipped, lacunae present or absent, in 0–3 rows each side of midvein;</text>
      <biological_entity id="o2633" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s9" to="olive-green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="2.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2634" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s9" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2635" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o2636" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o2637" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="hoodlike" value_original="hoodlike" />
        <character char_type="range_value" from="cuspidate" name="architecture" src="d0_s9" to="bristle-tipped" />
      </biological_entity>
      <biological_entity id="o2638" name="lacuna" name_original="lacunae" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2639" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s9" to="3" />
      </biological_entity>
      <biological_entity id="o2640" name="side" name_original="side" src="d0_s9" type="structure" />
      <biological_entity id="o2641" name="midvein" name_original="midvein" src="d0_s9" type="structure" />
      <relation from="o2634" id="r341" name="without" negation="false" src="d0_s9" to="o2635" />
      <relation from="o2638" id="r342" name="in" negation="false" src="d0_s9" to="o2639" />
      <relation from="o2640" id="r343" name="part_of" negation="false" src="d0_s9" to="o2641" />
    </statement>
    <statement id="d0_s10">
      <text>veins 3–9 (–13).</text>
      <biological_entity id="o2642" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="13" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o2643" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s11" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peduncles not dimorphic, terminal or occasionally axillary, erect or rarely recurved, slightly clavate, 1–3 cm;</text>
      <biological_entity id="o2644" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s12" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="occasionally" name="position" src="d0_s12" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>spikes not dimorphic, cylindric, 5–11 mm.</text>
      <biological_entity id="o2645" name="spike" name_original="spikes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits sessile, dark green, orbicular, turgid, abaxial keel obscure, lateral keels obscure or absent, 2.5–3 × 2.2–3 mm, lateral keels if present without points;</text>
      <biological_entity id="o2646" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="shape" src="d0_s14" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="turgid" value_original="turgid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2647" name="keel" name_original="keel" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2648" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2649" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character constraint="without points" constraintid="o2650" is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2650" name="point" name_original="points" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>beak erect, 0.5 mm;</text>
      <biological_entity id="o2651" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o2652" name="side" name_original="sides" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o2653" name="tubercle" name_original="tubercles" src="d0_s16" type="structure" />
      <relation from="o2652" id="r344" name="without" negation="false" src="d0_s16" to="o2653" />
    </statement>
    <statement id="d0_s17">
      <text>embryo with 1 full spiral.</text>
      <biological_entity id="o2654" name="embryo" name_original="embryo" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Chromosome number unknownnot available.</text>
      <biological_entity id="o2655" name="chromosome" name_original="chromosome" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline waters of ponds and lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline waters" constraint="of ponds and lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Mass., N.Y., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <other_name type="common_name">Ogden's pondweed</other_name>
  <other_name type="common_name">potamot d’Ogden</other_name>
  <discussion>Potamogeton ogdenii is an extremely local species, probably known from fewer than a dozen localities. The species is herein reported for the first time from Canada, being known from that country by a single collection made in 1987.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>