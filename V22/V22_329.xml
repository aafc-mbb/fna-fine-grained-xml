<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Poiophylli</taxon_name>
    <taxon_name authority="Coville" date="1895" rank="species">georgianus</taxon_name>
    <place_of_publication>
      <publication_title>Bulletin of the Torrey Botanical Club</publication_title>
      <place_in_publication>22:44. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus poiophylli;species georgianus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000135</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, short-lived perennial, cespitose, to 4 dm.</text>
      <biological_entity id="o16024" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 3–40.</text>
      <biological_entity id="o16025" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s1" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, 2–3;</text>
      <biological_entity id="o16026" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>auricles 0.2–0.3 mm, scarious to membranous;</text>
      <biological_entity id="o16027" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="scarious" name="texture" src="d0_s3" to="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade flat, 5–15 cm × 0.4–0.7 mm, margins entire.</text>
      <biological_entity id="o16028" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s4" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16029" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (3–) 8–30 (–45) -flowered, diffuse, 3–11 cm;</text>
      <biological_entity id="o16030" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(3-)8-30(-45)-flowered" value_original="(3-)8-30(-45)-flowered" />
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary bract rarely surpassing inflorescence.</text>
      <biological_entity constraint="primary" id="o16031" name="bract" name_original="bract" src="d0_s6" type="structure" />
      <biological_entity id="o16032" name="inflorescence" name_original="inflorescence" src="d0_s6" type="structure" />
      <relation from="o16031" id="r2073" modifier="rarely" name="surpassing" negation="false" src="d0_s6" to="o16032" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: bracteoles 2;</text>
      <biological_entity id="o16033" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16034" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>tepals greenish to tan, lanceolate, (3.3–) 3.6–5.1 (–5.7) mm;</text>
      <biological_entity id="o16035" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16036" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s8" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5.7" to_unit="mm" />
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s8" to="5.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer and inner series nearly equal, apex acuminate;</text>
      <biological_entity id="o16037" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer and inner" id="o16038" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o16039" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 6, filaments 0.2–0.4 mm, anthers (0.8–) 1.2–1.5 (–1.7) mm;</text>
      <biological_entity id="o16040" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16041" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o16042" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16043" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.6–0.8 mm.</text>
      <biological_entity id="o16044" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16045" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules tan, 3-locular, ellipsoid to narrowly so, 2.7–4 × 1.2–1.7 mm.</text>
      <biological_entity id="o16046" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="2.7" from_unit="mm" modifier="narrowly" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" modifier="narrowly" name="width" src="d0_s12" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds tan, ellipsoid or widely so, 0.364–0.45 mm, not tailed.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = ca. 80.</text>
      <biological_entity id="o16047" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character name="shape" src="d0_s13" value="widely" value_original="widely" />
        <character char_type="range_value" from="0.364" from_unit="mm" name="some_measurement" src="d0_s13" to="0.45" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16048" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed sites with thin, usually sandy soil over surfacing granite (flatrocks), the soil in these areas may be moist in the spring from seepage or for a short period after rainfalls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="exposed" constraint="with thin , usually sandy soil over surfacing granite" />
        <character name="habitat" value="thin" constraint="over surfacing granite" />
        <character name="habitat" value="sandy soil" modifier="usually" constraint="over surfacing granite" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="the soil" constraint="in these areas" />
        <character name="habitat" value="these areas" />
        <character name="habitat" value="the spring" modifier="may be moist in" />
        <character name="habitat" value="seepage" modifier="from" />
        <character name="habitat" value="a short period" modifier="or for" />
        <character name="habitat" value="rainfalls" modifier="after" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21</number>
  <other_name type="common_name">Georgia rush</other_name>
  
</bio:treatment>