<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Buckley" date="1862" rank="species">filipendulus</taxon_name>
    <place_of_publication>
      <publication_title>Proceedings of the Academy of Natural Sciences of Philadelphia</publication_title>
      <place_in_publication>14: 8. 1862</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species filipendulus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000134</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray ex Engelmann" date="unknown" rank="species">leptocaulis</taxon_name>
    <taxon_hierarchy>genus Juncus;species leptocaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, tufted, 1.5–3.5 dm.</text>
      <biological_entity id="o1187" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="3.5" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes poorly developed.</text>
      <biological_entity id="o1188" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s1" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect, compressed, bases often swollen.</text>
      <biological_entity id="o1189" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o1190" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal 2–4, cauline 1–3;</text>
      <biological_entity id="o1191" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o1192" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1193" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles 0.5–1 mm, apex rounded to nearly acute;</text>
      <biological_entity id="o1194" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o1195" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1196" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="nearly acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat, 3–15 cm × 1–2.5 mm.</text>
      <biological_entity id="o1197" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o1198" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences glomerules, (1–) 2–5 (–10), each with (3–) 6–15 flowers, open;</text>
      <biological_entity constraint="inflorescences" id="o1199" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s6" to="2" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o1200" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s6" to="6" to_inclusive="false" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <relation from="o1199" id="r160" name="with" negation="false" src="d0_s6" to="o1200" />
    </statement>
    <statement id="d0_s7">
      <text>primary bract shorter than inflorescence.</text>
      <biological_entity constraint="primary" id="o1201" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character constraint="than inflorescence" constraintid="o1202" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o1202" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals straw-colored with green midstripe, lanceolate or widely so, 3.5–5 mm, margins sometimes clear;</text>
      <biological_entity id="o1203" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1204" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character constraint="with midstripe" constraintid="o1205" is_modifier="false" name="coloration" src="d0_s8" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character name="shape" src="d0_s8" value="widely" value_original="widely" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1205" name="midstripe" name_original="midstripe" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1206" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="clear" value_original="clear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer and inner series nearly equal;</text>
      <biological_entity id="o1207" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer and inner" id="o1208" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3, filaments 1.5 mm, anthers 0.5 mm;</text>
      <biological_entity id="o1209" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1210" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o1211" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o1212" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.5 mm.</text>
      <biological_entity id="o1213" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1214" name="style" name_original="style" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules tan to reddish-brown, 3-locular, obovoid, 2.6–3.2 mm, shorter than perianth.</text>
      <biological_entity id="o1215" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="reddish-brown" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s12" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s12" to="3.2" to_unit="mm" />
        <character constraint="than perianth" constraintid="o1216" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o1216" name="perianth" name_original="perianth" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds fusiform, 0.5–0.6 mm, not tailed.</text>
      <biological_entity id="o1217" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="spring" />
        <character name="fruiting time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, usually calcareous soils of swales or glades, occasionally in shallow water along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="soils" modifier="usually calcareous" constraint="of swales or glades" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="shallow water" modifier="occasionally in" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="calcareous" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ark., Ky., La., Miss., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37</number>
  
</bio:treatment>