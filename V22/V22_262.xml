<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="Ruprecht" date="1845" rank="species">friesii</taxon_name>
    <place_of_publication>
      <publication_title>Hist. Stirp. Fl. Petrop.</publication_title>
      <place_in_publication>43. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species friesii</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000288</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes absent.</text>
      <biological_entity id="o540" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems compressed, without spots, 10–135 cm;</text>
      <biological_entity constraint="cauline" id="o541" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="135" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o542" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o541" id="r75" name="without" negation="false" src="d0_s1" to="o542" />
    </statement>
    <statement id="d0_s2">
      <text>glands green, greenish brown, or gold, to 0.7 mm diam.</text>
      <biological_entity id="o543" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gold" value_original="gold" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gold" value_original="gold" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Turions terminal or lateral, common, 1.5–5 cm × 1.5–4 mm, soft;</text>
      <biological_entity id="o544" name="turion" name_original="turions" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="common" value_original="common" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves 4-ranked;</text>
      <biological_entity id="o545" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="4-ranked" value_original="4-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>outer leaves 2–3 per side, base corrugate, apex apiculate to acute;</text>
      <biological_entity constraint="outer" id="o546" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o547" from="2" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <biological_entity id="o547" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o548" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
      </biological_entity>
      <biological_entity id="o549" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner leaves reduced, arranged into fan-shaped structure and oriented at 90 angles to outer leaves.</text>
      <biological_entity constraint="inner" id="o550" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character constraint="into structure" constraintid="o551" is_modifier="false" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
        <character constraint="at angles" constraintid="o552" is_modifier="false" name="orientation" notes="" src="d0_s6" value="oriented" value_original="oriented" />
      </biological_entity>
      <biological_entity id="o551" name="structure" name_original="structure" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="fan--shaped" value_original="fan--shaped" />
      </biological_entity>
      <biological_entity id="o552" name="angle" name_original="angles" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="90" value_original="90" />
      </biological_entity>
      <biological_entity constraint="outer" id="o553" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o552" id="r76" name="to" negation="false" src="d0_s6" to="o553" />
    </statement>
    <statement id="d0_s7">
      <text>Leaves submersed, spirally arranged, delicate to rigid, sessile;</text>
      <biological_entity id="o554" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="location" src="d0_s7" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="texture" src="d0_s7" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules not persistent, inconspicuous, convolute, free from blade, white, not ligulate, 0.55–2.1 cm, fibrous, coarse, shredding at tip, apex obtuse;</text>
      <biological_entity id="o555" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="convolute" value_original="convolute" />
        <character constraint="from blade" constraintid="o556" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.55" from_unit="cm" name="some_measurement" src="d0_s8" to="2.1" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="relief" src="d0_s8" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o556" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o557" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o558" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o555" id="r77" name="shredding at" negation="false" src="d0_s8" to="o557" />
    </statement>
    <statement id="d0_s9">
      <text>blade light green, rarely olive-green to.</text>
    </statement>
    <statement id="d0_s10">
      <text>somewhat reddish, linear, not arcuate, 2.3–6.5 cm × 1.2–3.2 mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, acute to apiculate, lacunae absent or 1 narrow row each side of midrib;</text>
      <biological_entity id="o559" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="olive-green" value_original="olive-green" />
        <character is_modifier="false" modifier="somewhat" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s10" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="2.3" from_unit="cm" name="length" src="d0_s10" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o560" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s10" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o561" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o562" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o563" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="hoodlike" value_original="hoodlike" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="apiculate" />
      </biological_entity>
      <biological_entity id="o564" name="lacuna" name_original="lacunae" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o565" name="row" name_original="row" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o566" name="side" name_original="side" src="d0_s10" type="structure" />
      <biological_entity id="o567" name="midrib" name_original="midrib" src="d0_s10" type="structure" />
      <relation from="o560" id="r78" name="without" negation="false" src="d0_s10" to="o561" />
      <relation from="o566" id="r79" name="part_of" negation="false" src="d0_s10" to="o567" />
    </statement>
    <statement id="d0_s11">
      <text>veins 5–7 (–9).</text>
      <biological_entity id="o568" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="9" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o569" name="inflorescence" name_original="inflorescences" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s12" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peduncles not dimorphic, terminal or axillary, erect or rarely recurved, slightly clavate, 1.2–4.1 (–7) cm;</text>
      <biological_entity id="o570" name="peduncle" name_original="peduncles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s13" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s13" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="4.1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s13" to="4.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>spike not dimorphic, cylindric, 7–16 mm.</text>
      <biological_entity id="o571" name="spike" name_original="spike" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits sessile, olive-green to brown, obovoid, turgid, not abaxially or laterally keeled, 1.8–2.5 × 1.2–2 mm;</text>
      <biological_entity id="o572" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="olive-green" name="coloration" src="d0_s15" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="turgid" value_original="turgid" />
        <character is_modifier="false" modifier="not abaxially; abaxially; laterally" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s15" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak erect, 0.3–0.7 mm;</text>
      <biological_entity id="o573" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o574" name="side" name_original="sides" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o575" name="tubercle" name_original="tubercles" src="d0_s17" type="structure" />
      <relation from="o574" id="r80" name="without" negation="false" src="d0_s17" to="o575" />
    </statement>
    <statement id="d0_s18">
      <text>embryo with 1 full spiral.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 26.</text>
      <biological_entity id="o576" name="embryo" name_original="embryo" src="d0_s18" type="structure" />
      <biological_entity constraint="2n" id="o577" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous to brackish waters of lakes and slow-flowing streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous to brackish waters" constraint="of lakes and slow-flowing streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="slow-flowing streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Colo., Conn., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mont., Nebr., N.H., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Utah, Vt., Wash., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Potamot de Fries</other_name>
  <discussion>Potamogeton friesii is a fairly common linear-leaved species, especially of calcareous waters of lakes and streams of the upper Midwest. Whenever turions are present, the species is easily identified, as it is the only one with the outer leaves of the turions having corrugate bases and the inner leaves turned at right angles to the outer leaves.</discussion>
  <discussion>Two hybrids, Potamogeton friesii × P. pusillus (= P. × pusilliformis Fischer [P. × intermedius Fischer]) and P. friesii × P. obtusifolius (= P. × semifructus A. Bennett ex Ascherson &amp; Graebner), have been described.</discussion>
  
</bio:treatment>