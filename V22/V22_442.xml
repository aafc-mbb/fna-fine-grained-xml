<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Luzula</taxon_name>
    <taxon_name authority="Böcher" date="1950" rank="species">groenlandica</taxon_name>
    <place_of_publication>
      <publication_title>Meddelelser om Gronland</publication_title>
      <place_in_publication>147: 18. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus luzula;species groenlandica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000228</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms cespitose, straight, 10–30 cm, stiff.</text>
      <biological_entity id="o4696" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="course" src="d0_s0" value="straight" value_original="straight" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal leaves (with several outer dead ones usually present), to 9 cm × 3 mm, apex callous, sparingly pilose;</text>
      <biological_entity id="o4697" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" notes="" src="d0_s1" to="9" to_unit="cm" />
        <character name="width" notes="" src="d0_s1" unit="mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4698" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4699" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="callous" value_original="callous" />
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves to 5 cm × 2 mm.</text>
      <biological_entity id="o4700" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o4701" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character name="length" src="d0_s2" unit="cm" value="5" value_original="5" />
        <character name="width" src="d0_s2" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: glomerules 1 (–3);</text>
      <biological_entity id="o4702" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o4703" name="glomerule" name_original="glomerules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>peduncles, not exceeding 5 mm;</text>
      <biological_entity id="o4704" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o4705" name="peduncle" name_original="peduncles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal inflorescence bract conspicuous, leaflike, usually conspicuously exceeding inflorescence;</text>
      <biological_entity id="o4706" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="inflorescence" id="o4707" name="bract" name_original="bract" src="d0_s5" type="structure" constraint_original="proximal inflorescence">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o4708" name="inflorescence" name_original="inflorescence" src="d0_s5" type="structure" />
      <relation from="o4707" id="r621" modifier="usually conspicuously" name="exceeding" negation="false" src="d0_s5" to="o4708" />
    </statement>
    <statement id="d0_s6">
      <text>bracts mostly inconspicuous;</text>
      <biological_entity id="o4709" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o4710" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles clear or light to dark-brown, occasionally to 1/2 tepal length.</text>
      <biological_entity id="o4711" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o4712" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s7" to="dark-brown" />
        <character char_type="range_value" from="0" modifier="occasionally" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity id="o4713" name="tepal" name_original="tepal" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals with broad clear margins and apex (apex acute-acuminate), 1.9–2.5 mm, apex acute-acuminate;</text>
      <biological_entity id="o4714" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4715" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4716" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="broad" value_original="broad" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="clear" value_original="clear" />
      </biological_entity>
      <biological_entity id="o4717" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o4718" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute-acuminate" value_original="acute-acuminate" />
      </biological_entity>
      <relation from="o4715" id="r622" name="with" negation="false" src="d0_s8" to="o4716" />
      <relation from="o4715" id="r623" name="with" negation="false" src="d0_s8" to="o4717" />
    </statement>
    <statement id="d0_s9">
      <text>outer whorl ± equaling to exceeding inner whorl;</text>
      <biological_entity id="o4719" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o4720" name="whorl" name_original="whorl" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4721" name="whorl" name_original="whorl" src="d0_s9" type="structure" />
      <relation from="o4720" id="r624" name="exceeding" negation="false" src="d0_s9" to="o4721" />
    </statement>
    <statement id="d0_s10">
      <text>anthers ± equaling filaments.</text>
      <biological_entity id="o4722" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4723" name="anther" name_original="anthers" src="d0_s10" type="structure" />
      <biological_entity id="o4724" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsules dark reddish, shining, ovoid, generally shorter than tepals.</text>
      <biological_entity id="o4725" name="capsule" name_original="capsules" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="reflectance" src="d0_s11" value="shining" value_original="shining" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character constraint="than tepals" constraintid="o4726" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="generally shorter" value_original="generally shorter" />
      </biological_entity>
      <biological_entity id="o4726" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds translucent, brown, ellipsoid, 0.9–1.1 mm;</text>
      <biological_entity id="o4727" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>caruncle barely visible.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 24.</text>
      <biological_entity id="o4728" name="caruncle" name_original="caruncle" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="barely" name="prominence" src="d0_s13" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4729" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy sea shores with herbaceous vegetation to turfy tundra, often by water, to alpine flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy sea shores" constraint="with herbaceous vegetation to turfy tundra , often by water , to alpine flats" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="water" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Man., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12</number>
  <other_name type="common_name">Greenland wood rush</other_name>
  <discussion>The culms of Luzula groenlandica are straight and stiff; basal leaves are sparingly pilose.</discussion>
  
</bio:treatment>