<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="(Britton) Smyth" date="1899" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Transactions of the Kansas Academy of Science</publication_title>
      <place_in_publication>16: 163. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000422</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">virginiana</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="variety">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown, Ill. Fl. N. U.S.</publication_title>
      <place_in_publication>1: 377. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tradescantia;species virginiana;variety occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o1381" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o1382" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o1382" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–90 cm;</text>
      <biological_entity id="o1383" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glaucous, glabrous.</text>
      <biological_entity id="o1384" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves spirally arranged, sessile;</text>
      <biological_entity id="o1385" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear-lanceolate, 5–50 × 0.2–3 cm (distal leaf-blades equal to or narrower than sheaths when sheaths opened, flattened), apex acuminate, glaucous, glabrous.</text>
      <biological_entity id="o1386" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="50" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1387" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, often axillary;</text>
      <biological_entity id="o1388" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous.</text>
      <biological_entity id="o1389" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o1390" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s7" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.8–3 cm, glandular-puberulent, rarely glabrous or glabrescent;</text>
      <biological_entity id="o1391" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4–11 mm, glandular-puberulent, usually with apical tuft of eglandular hairs, occasionally with scattered eglandular hairs among glandular, rarely glabrous or glabrescent;</text>
      <biological_entity id="o1392" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-puberulent" value_original="glandular-puberulent" />
        <character is_modifier="false" modifier="among glandular; rarely" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1393" name="tuft" name_original="tuft" src="d0_s9" type="structure" />
      <biological_entity id="o1394" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o1395" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o1392" id="r178" modifier="usually" name="with" negation="false" src="d0_s9" to="o1393" />
      <relation from="o1393" id="r179" name="part_of" negation="false" src="d0_s9" to="o1394" />
      <relation from="o1392" id="r180" modifier="occasionally" name="with" negation="false" src="d0_s9" to="o1395" />
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, bright blue to rose or magenta, broadly ovate, not clawed, 6–16 mm;</text>
      <biological_entity id="o1396" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="bright blue" name="coloration" src="d0_s10" to="rose or magenta" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens free;</text>
      <biological_entity id="o1397" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded.</text>
      <biological_entity id="o1398" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–7 mm.</text>
      <biological_entity id="o1399" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–4 mm.</text>
      <biological_entity id="o1400" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man.; Ariz., Ark., Colo., Iowa, Kans., La., Minn., Mont., N.Dak., N.Mex., Nebr., Okla., S.Dak., Tex., Utah, Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11</number>
  <discussion>All of the chromosome counts cited by E. Anderson (1954) for this species are attributable to Tradescantia occidentalis var. occidentalis.</discussion>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and pedicels ± uniformly glandular-puberulent, rarely nearly glabrous</description>
      <determination>11a Tradescantia occidentalis var. occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals and pedicels completely glabrous</description>
      <determination>11b Tradescantia occidentalis var. scopulorum</determination>
    </key_statement>
  </key>
</bio:treatment>