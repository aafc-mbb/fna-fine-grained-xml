<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="de Candolle in J. Lamarck and A. P. de Candolle" date="unknown" rank="genus">luzula</taxon_name>
    <taxon_name authority="(Grisebach) Buchenau" date="1890" rank="subgenus">Pterodes</taxon_name>
    <taxon_name authority="Fischer ex E. Meyer" date="1849" rank="species">rufescens</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>22:385. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus luzula;subgenus pterodes;species rufescens;</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000241</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes absent.</text>
      <biological_entity id="o13731" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stolons to 4 cm;</text>
      <biological_entity id="o13732" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scale-leaves present;</text>
      <biological_entity id="o13733" name="scale-leaf" name_original="scale-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>adventitious-roots present.</text>
      <biological_entity id="o13734" name="adventitiou-root" name_original="adventitious-roots" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Culms loosely cespitose, 10–30 cm × 0.5–3.5 mm.</text>
      <biological_entity id="o13735" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s4" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves: sheaths reddish, throats pilose;</text>
      <biological_entity id="o13736" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o13737" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o13738" name="throat" name_original="throats" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade sparsely pubescent;</text>
      <biological_entity id="o13739" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o13740" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal leaves 6–10 cm × 2–5 mm;</text>
      <biological_entity id="o13741" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o13742" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline leaves 2–3, to 4 (–6) cm × 1–2.5 mm.</text>
      <biological_entity id="o13743" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="cauline" id="o13744" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="3" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences simple or rarely branching;</text>
      <biological_entity id="o13745" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s9" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal inflorescence bract to 1/2 length of inflorescence;</text>
      <biological_entity constraint="inflorescence" id="o13746" name="bract" name_original="bract" src="d0_s10" type="structure" constraint_original="proximal inflorescence">
        <character char_type="range_value" from="0 length of inflorescence" name="length" src="d0_s10" to="1/2 length of inflorescence" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts brown, apex truncate, glabrous;</text>
      <biological_entity id="o13747" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o13748" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles ovate, 1/2 length of tepals;</text>
      <biological_entity id="o13749" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character name="length" src="d0_s12" value="1/2 length of tepals" value_original="1/2 length of tepals" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pedicels spreading, 1–4 cm.</text>
      <biological_entity id="o13750" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s13" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers: tepals brown, 1.6–2.5 mm, margins entire, clear;</text>
      <biological_entity id="o13751" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o13752" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13753" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="clear" value_original="clear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 2 times filament length;</text>
      <biological_entity id="o13754" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o13755" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character constraint="filament" constraintid="o13756" is_modifier="false" name="length" src="d0_s15" value="2 times filament length" value_original="2 times filament length" />
      </biological_entity>
      <biological_entity id="o13756" name="filament" name_original="filament" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stigmas 2 times style length.</text>
      <biological_entity id="o13757" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o13758" name="stigma" name_original="stigmas" src="d0_s16" type="structure">
        <character constraint="style" constraintid="o13759" is_modifier="false" name="length" src="d0_s16" value="2 times style length" value_original="2 times style length" />
      </biological_entity>
      <biological_entity id="o13759" name="style" name_original="style" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Capsules straw-colored, sometimes reddish, equal to or exceeding tepals;</text>
      <biological_entity id="o13760" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s17" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o13761" name="tepal" name_original="tepals" src="d0_s17" type="structure" />
      <relation from="o13760" id="r1836" name="exceeding" negation="false" src="d0_s17" to="o13761" />
    </statement>
    <statement id="d0_s18">
      <text>beak a mucro.</text>
      <biological_entity id="o13762" name="beak" name_original="beak" src="d0_s18" type="structure" />
      <biological_entity id="o13763" name="mucro" name_original="mucro" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds dark-brown to black, 1.4 mm;</text>
      <biological_entity id="o13764" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s19" to="black" />
        <character name="some_measurement" src="d0_s19" unit="mm" value="1.4" value_original="1.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>caruncle erect to slightly curved, 2/3 length of seed body.</text>
      <biological_entity constraint="seed" id="o13766" name="body" name_original="body" src="d0_s20" type="structure">
        <character is_modifier="true" name="character" src="d0_s20" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>2n = 52 (Japan).</text>
      <biological_entity id="o13765" name="caruncle" name_original="caruncle" src="d0_s20" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s20" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s20" value="curved" value_original="curved" />
        <character constraint="of seed body" constraintid="o13766" name="quantity" src="d0_s20" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13767" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to mesic open montane forests and forest margins to borders of bogs, marshes, and river bars</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to mesic open montane forests" />
        <character name="habitat" value="forest margins" />
        <character name="habitat" value="to borders" constraint="of bogs , marshes , and river bars" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="river bars" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Rusty wood rush</other_name>
  
</bio:treatment>