<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Septati</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">articulatus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 327. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus septati;species articulatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000096</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Ehrhart ex Hoffmann" date="unknown" rank="species">articulatus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">obtusatus</taxon_name>
    <taxon_hierarchy>genus Juncus;species articulatus;variety obtusatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">articulatus</taxon_name>
    <taxon_name authority="(Wohlleben) House" date="unknown" rank="variety">stolonifer</taxon_name>
    <taxon_hierarchy>genus Juncus;species articulatus;variety stolonifer;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lampocarpus</taxon_name>
    <taxon_hierarchy>genus Juncus;species lampocarpus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous to nearly cespitose, 0.5–6 (–10) dm.</text>
      <biological_entity id="o14889" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="nearly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes 2–3 mm diam., not swollen.</text>
      <biological_entity id="o14890" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect to decumbent (and floating), terete, 1–3 mm diam., smooth.</text>
      <biological_entity id="o14891" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1, maroon to straw-colored, apex acute to obtuse.</text>
      <biological_entity id="o14892" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="maroon" name="coloration" src="d0_s3" to="straw-colored" />
      </biological_entity>
      <biological_entity id="o14893" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 0–2, cauline (1–) 3–6;</text>
      <biological_entity id="o14894" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o14895" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o14896" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 0.5–1 mm, apex rounded, scarious;</text>
      <biological_entity id="o14897" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o14898" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14899" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade green to straw-colored, terete, 3.5–12 cm × 0.5–1.1 mm.</text>
      <biological_entity id="o14900" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o14901" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s6" to="straw-colored" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal panicles of 3–30 (–50) heads, 3.5–8 cm, branches spreading;</text>
      <biological_entity id="o14902" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o14903" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" notes="" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14904" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="50" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="30" />
      </biological_entity>
      <biological_entity id="o14905" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o14903" id="r1963" name="consist_of" negation="false" src="d0_s7" to="o14904" />
    </statement>
    <statement id="d0_s8">
      <text>primary bract erect;</text>
      <biological_entity constraint="primary" id="o14906" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>heads 3–10-flowered, obpyramidal to hemispheric, 6–8 mm diam.</text>
      <biological_entity id="o14907" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-10-flowered" value_original="3-10-flowered" />
        <character char_type="range_value" from="obpyramidal" name="shape" src="d0_s9" to="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals green to straw-colored or dark-brown, ovate to lanceolate, 1.8–3 mm;</text>
      <biological_entity id="o14908" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14909" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="straw-colored or dark-brown" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer tepals with apex acute or acuminate;</text>
      <biological_entity id="o14910" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o14911" name="tepal" name_original="tepals" src="d0_s11" type="structure" />
      <biological_entity id="o14912" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o14911" id="r1964" name="with" negation="false" src="d0_s11" to="o14912" />
    </statement>
    <statement id="d0_s12">
      <text>inner tepals with apex acute-acuminate to obtuse;</text>
      <biological_entity id="o14913" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o14914" name="tepal" name_original="tepals" src="d0_s12" type="structure" />
      <biological_entity id="o14915" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute-acuminate" name="shape" src="d0_s12" to="obtuse" />
      </biological_entity>
      <relation from="o14914" id="r1965" name="with" negation="false" src="d0_s12" to="o14915" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 6, anthers equal to filament length.</text>
      <biological_entity id="o14916" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o14917" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o14918" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="to filament" constraintid="o14919" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o14919" name="filament" name_original="filament" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules exserted ca. 1 mm beyond perianth, chestnut-brown to dark-brown, imperfectly 3-locular, ellipsoid or ovoid, 2.8–4 mm, apex acute proximal to beak, valves separating at dehiscence.</text>
      <biological_entity id="o14920" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character constraint="beyond perianth" constraintid="o14921" name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="chestnut-brown" name="coloration" notes="" src="d0_s14" to="dark-brown" />
        <character is_modifier="false" modifier="imperfectly" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14921" name="perianth" name_original="perianth" src="d0_s14" type="structure" />
      <biological_entity id="o14922" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character constraint="to beak" constraintid="o14923" is_modifier="false" name="position" src="d0_s14" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o14923" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity id="o14924" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds obovoid, 0.5 mm, not tailed.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 80.</text>
      <biological_entity id="o14925" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14926" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet ground in ditches, lake and stream margins, and a variety of other habitats, often a calciphile</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet ground" constraint="in ditches , lake and stream margins , and a variety of other habitats , often a calciphile" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="lake" />
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="a variety" constraint="of other habitats , often a calciphile" />
        <character name="habitat" value="other habitats" />
        <character name="habitat" value="a calciphile" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; B.C., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., Que., P.E.I.; Alaska, Ariz., Colo., Calif., Conn., Idaho, Ind., Ky., Maine, Mass., Mich., Minn., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Oreg., Pa., R.I., S.Dak., Utah, Vt., Va., Wash., W.Va.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>95</number>
  <other_name type="common_name">Jointed rush</other_name>
  <discussion>Juncus articulatus hybridizes with J. brevicaudatus (= J. ×fulvescens Fernald), J. alpinus (= J. ×alpiniformis Fernald), J. nodosus, and J. canadensis.</discussion>
  <discussion>Juncus articulatus var. obtusatus Engelmann appears to be intermediate with J. alpinus. It has spreading inflorescence branches but obtuse inner tepals. This may represent a backcross with J. alpinus. Recent evidence suggests that J. alpinus is a polyploid species with J. articulatus as one of its parents.</discussion>
  
</bio:treatment>