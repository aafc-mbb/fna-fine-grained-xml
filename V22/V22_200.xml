<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Septati</taxon_name>
    <taxon_name authority="Ehrhart in G. F. Hoffmann" date="1791" rank="species">acutiflorus</taxon_name>
    <place_of_publication>
      <publication_title>in G. F. Hoffmann,Deutschl. Fl.</publication_title>
      <place_in_publication>1: 125. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus septati;species acutiflorus;</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000087</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, rhizomatous, to 8 dm.</text>
      <biological_entity id="o12722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes 5–6 mm diam., nodes not swollen.</text>
      <biological_entity id="o12723" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12724" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms erect, terete, 3 mm diam., smooth.</text>
      <biological_entity id="o12725" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character name="diameter" src="d0_s2" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1, straw-colored, apex acute.</text>
      <biological_entity id="o12726" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
      <biological_entity id="o12727" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 1, cauline 2;</text>
      <biological_entity id="o12728" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o12729" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12730" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 0.5 mm, apex rounded, cartilaginous;</text>
      <biological_entity id="o12731" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12732" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o12733" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade 7–45 cm x 1–2 mm, terete, not scabrous.</text>
      <biological_entity id="o12734" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o12735" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="not" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal panicles of 70–120 heads, 6–10 cm, branches ascending;</text>
      <biological_entity id="o12736" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o12737" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" notes="" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12738" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="70" is_modifier="true" name="quantity" src="d0_s7" to="120" />
      </biological_entity>
      <biological_entity id="o12739" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o12737" id="r1696" name="consist_of" negation="false" src="d0_s7" to="o12738" />
    </statement>
    <statement id="d0_s8">
      <text>primary bract erect;</text>
      <biological_entity constraint="primary" id="o12740" name="bract" name_original="bract" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>heads 3–5 (–8) -flowered, obconic, 2–4 mm diam.</text>
      <biological_entity id="o12741" name="head" name_original="heads" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-5(-8)-flowered" value_original="3-5(-8)-flowered" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: tepals dark reddish-brown, lanceolate, apex acuminate to subulate tipte;</text>
      <biological_entity id="o12742" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12743" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o12744" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s10" to="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer tepals 1.9–2.2 mm;</text>
      <biological_entity id="o12745" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o12746" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>inner tepals 2–2.3 mm;</text>
      <biological_entity id="o12747" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o12748" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 6, anthers 2 times filament length.</text>
      <biological_entity id="o12749" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o12750" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o12751" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="filament" constraintid="o12752" is_modifier="false" name="length" src="d0_s13" value="2 times filament length" value_original="2 times filament length" />
      </biological_entity>
      <biological_entity id="o12752" name="filament" name_original="filament" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules inserted with beak exserted, straw-colored, 1-locular, narrowly ovoid, 2.3–2.4 mm, tapering to subulate tip, valves separating at dehiscence.</text>
      <biological_entity id="o12753" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s14" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="tapering" name="shape" src="d0_s14" to="subulate" />
      </biological_entity>
      <biological_entity id="o12754" name="beak" name_original="beak" src="d0_s14" type="structure" />
      <biological_entity id="o12755" name="tip" name_original="tip" src="d0_s14" type="structure" />
      <biological_entity id="o12756" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s14" value="separating" value_original="separating" />
      </biological_entity>
      <relation from="o12753" id="r1697" name="inserted with" negation="false" src="d0_s14" to="o12754" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds narrowly ovoid, 0.5–0.6 mm, not tailed;</text>
      <biological_entity id="o12757" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>body clear yellowbrown.</text>
      <biological_entity id="o12758" name="body" name_original="body" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="clear yellowbrown" value_original="clear yellowbrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Nfld. and Labr. (Nfld.); Europe; Asia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>73</number>
  
</bio:treatment>