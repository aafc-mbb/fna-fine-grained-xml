<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Dumortier" date="unknown" rank="family">potamogetonaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">potamogeton</taxon_name>
    <taxon_name authority="Morong" date="1881" rank="species">hillii</taxon_name>
    <place_of_publication>
      <publication_title>Botanical Gazette</publication_title>
      <place_in_publication>6: 290, fig. 3. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family potamogetonaceae;genus potamogeton;species hillii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potamogeton</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">porteri</taxon_name>
    <taxon_hierarchy>genus Potamogeton;species porteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes absent.</text>
      <biological_entity id="o2819" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline stems slightly compressed, without spots, 30–60 cm;</text>
      <biological_entity constraint="cauline" id="o2820" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2821" name="spot" name_original="spots" src="d0_s1" type="structure" />
      <relation from="o2820" id="r377" name="without" negation="false" src="d0_s1" to="o2821" />
    </statement>
    <statement id="d0_s2">
      <text>glands rare, when present, brown to green, 0.1–0.3 mm diam.</text>
      <biological_entity id="o2822" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="rare" value_original="rare" />
        <character char_type="range_value" from="brown" modifier="when present" name="coloration" src="d0_s2" to="green" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="diameter" src="d0_s2" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Turions terminal, rare, 2.8–3 cm × 1.5–3 mm, soft;</text>
      <biological_entity id="o2823" name="turion" name_original="turions" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="rare" value_original="rare" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaves 2-ranked;</text>
      <biological_entity id="o2824" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>outer leaves 3–4 per side, base not corrugate, apex acute to apiculate;</text>
      <biological_entity constraint="outer" id="o2825" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o2826" from="3" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o2826" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o2827" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s5" value="corrugate" value_original="corrugate" />
      </biological_entity>
      <biological_entity id="o2828" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>inner leaves undifferentiated.</text>
      <biological_entity constraint="inner" id="o2829" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves submersed, spirally arranged, sessile, delicate;</text>
      <biological_entity id="o2830" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="location" src="d0_s7" value="submersed" value_original="submersed" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="delicate" value_original="delicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules persistent, inconspicuous, convolute, free from blade, white to light-brown, not ligulate, 0.7–1.6 cm, slightly fibrous, rarely shredding at tip, apex obtuse;</text>
      <biological_entity id="o2831" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s8" value="convolute" value_original="convolute" />
        <character constraint="from blade" constraintid="o2832" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character char_type="range_value" from="white" name="coloration" notes="" src="d0_s8" to="light-brown" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="1.6" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s8" value="fibrous" value_original="fibrous" />
      </biological_entity>
      <biological_entity id="o2832" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o2833" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity id="o2834" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o2831" id="r378" modifier="rarely" name="shredding at" negation="false" src="d0_s8" to="o2833" />
    </statement>
    <statement id="d0_s9">
      <text>blade pale green to olive-green, linear, not arcuate, 2–6 cm × 0.6–2.5 (–4) mm, base slightly tapering, without basal lobes, not clasping, margins entire, not crispate, apex not hoodlike, apiculate to bristle-tipped or rarely blunt, lacunae in 1–2 rows each side of midrib;</text>
      <biological_entity id="o2835" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s9" to="olive-green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="not" name="course_or_shape" src="d0_s9" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2836" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" notes="" src="d0_s9" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2837" name="lobe" name_original="lobes" src="d0_s9" type="structure" />
      <biological_entity id="o2838" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="crispate" value_original="crispate" />
      </biological_entity>
      <biological_entity id="o2839" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="hoodlike" value_original="hoodlike" />
        <character char_type="range_value" from="apiculate" name="architecture" src="d0_s9" to="bristle-tipped" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o2840" name="lacuna" name_original="lacunae" src="d0_s9" type="structure" />
      <biological_entity id="o2841" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o2842" name="side" name_original="side" src="d0_s9" type="structure" />
      <biological_entity id="o2843" name="midrib" name_original="midrib" src="d0_s9" type="structure" />
      <relation from="o2836" id="r379" name="without" negation="false" src="d0_s9" to="o2837" />
      <relation from="o2840" id="r380" name="in" negation="false" src="d0_s9" to="o2841" />
      <relation from="o2842" id="r381" name="part_of" negation="false" src="d0_s9" to="o2843" />
    </statement>
    <statement id="d0_s10">
      <text>veins 3.</text>
      <biological_entity id="o2844" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences unbranched, emersed;</text>
      <biological_entity id="o2845" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="location" src="d0_s11" value="emersed" value_original="emersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>peduncles not dimorphic, axillary and/or terminal, erect to ascending, rarely recurved, slightly clavate, 6–13.5 mm;</text>
      <biological_entity id="o2846" name="peduncle" name_original="peduncles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="position" src="d0_s12" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position" src="d0_s12" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="13.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>spikes not dimorphic, globose, (2–) 4–7 mm.</text>
      <biological_entity id="o2847" name="spike" name_original="spikes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s13" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits brown to light greenish brown, ovoid to orbicular, turgid, sessile, abaxially and laterally keeled (3-keeled), 2.3–4 × 2–3.2 mm, lateral keels without points;</text>
      <biological_entity id="o2848" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s14" to="light greenish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s14" to="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s14" value="turgid" value_original="turgid" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="abaxially; laterally" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s14" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2849" name="keel" name_original="keels" src="d0_s14" type="structure" />
      <biological_entity id="o2850" name="point" name_original="points" src="d0_s14" type="structure" />
      <relation from="o2849" id="r382" name="without" negation="false" src="d0_s14" to="o2850" />
    </statement>
    <statement id="d0_s15">
      <text>beak erect, 0.3–0.7 mm;</text>
      <biological_entity id="o2851" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sides without basal tubercles;</text>
      <biological_entity id="o2852" name="side" name_original="sides" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o2853" name="tubercle" name_original="tubercles" src="d0_s16" type="structure" />
      <relation from="o2852" id="r383" name="without" negation="false" src="d0_s16" to="o2853" />
    </statement>
    <statement id="d0_s17">
      <text>embryo with 1 full spiral.</text>
      <biological_entity id="o2854" name="embryo" name_original="embryo" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Chromosome number unknownnot available.</text>
      <biological_entity id="o2855" name="chromosome" name_original="chromosome" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline waters of marshes, ponds, lakes, and slow-moving streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline waters" constraint="of marshes , ponds , lakes , and slow-moving streams" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="slow-moving streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Conn., Mass., Mich., N.Y., Ohio, Pa., Vt., Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13</number>
  <discussion>Potamogeton hillii is an easily recognized species either in fruit or when sterile. The leaf blade has a bristle tip and five or fewer veins. Those characters combined with the usual absence of nodal glands will separate this species from all other North American linear-leaved species. Ecologically, it is consistently found in more alkaline waters than any other North American pondweed. A study of 35 localities established the mean to be 124.1 mg/l CaCO3 (C. B. Hellquist 1984).</discussion>
  <references>
    <reference>Hellquist, C. B. 1984. Observations of Potamogeton hillii Morong in North America. Rhodora 86: 101–111.</reference>
  </references>
  
</bio:treatment>