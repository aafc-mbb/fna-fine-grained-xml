<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginiana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 288. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species virginiana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220013658</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">brevicaulis</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species brevicaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o9869" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o9870" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o9870" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots (1.5–) 2–4 mm thick, fleshy.</text>
      <biological_entity id="o9871" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s1" to="4" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 5–35 cm;</text>
      <biological_entity id="o9872" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous or occasionally distal internodes sparsely puberulent.</text>
      <biological_entity id="o9873" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9874" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="occasionally" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves spirally arranged, sessile;</text>
      <biological_entity id="o9875" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-lanceolate, 13–37 × 0.4–2.5 cm (distal leaf-blades equal to or narrower than sheaths when sheaths opened, flattened), apex acuminate, glabrous or occasionally puberulent.</text>
      <biological_entity id="o9876" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="13" from_unit="cm" name="length" src="d0_s5" to="37" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9877" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and (rarely) axillary;</text>
      <biological_entity id="o9878" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts foliaceous, well developed, not saccate, sparsely to densely pilose.</text>
      <biological_entity id="o9879" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s7" value="saccate" value_original="saccate" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o9880" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s8" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1.2–3.5 cm, eglandular-pilose or puberulent;</text>
      <biological_entity id="o9881" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s9" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="eglandular-pilose" value_original="eglandular-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals ± inflated, 7–16 mm, uniformly eglandular-pilose;</text>
      <biological_entity id="o9882" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="uniformly" name="pubescence" src="d0_s10" value="eglandular-pilose" value_original="eglandular-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals distinct, blue to purple, occasionally rose or white, broadly ovate, not clawed, 1.2–2 cm;</text>
      <biological_entity id="o9883" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s11" to="purple occasionally rose or white" />
        <character char_type="range_value" from="blue" name="coloration" src="d0_s11" to="purple occasionally rose or white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens free;</text>
      <biological_entity id="o9884" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments bearded.</text>
      <biological_entity id="o9885" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 4–7 mm.</text>
      <biological_entity id="o9886" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2–3 mm. 2n = 12, 24.</text>
      <biological_entity id="o9887" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9888" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer (Mar–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods, thickets, fields, roadsides and railroad rights-of-way</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad rights-of-way" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">&amp;Eacute;phémère de Virginie</other_name>
  <discussion>The records from the northern parts of the range of Tradescantia virginiana may all represent garden escapes (E. Anderson 1954). The uncertainty about the records from Arkansas and Mississippi reflects the difficulty in identifying some specimens. The specimens in question come from areas in which T. hirsutiflora (but not T. virginiana) has been recorded (E. Anderson and R. E. Woodson Jr. 1935). The exact geographic boundaries between these putatively allopatric species are uncertain. D. T. MacRoberts (1980b) has made a useful contribution toward our knowledge of these species.</discussion>
  
</bio:treatment>