<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Palisot de Beauvois ex Desvaux" date="unknown" rank="family">eriocaulaceae</taxon_name>
    <taxon_name authority="Kunth" date="1841" rank="genus">lachnocaulon</taxon_name>
    <taxon_name authority="(Walter) Morong" date="1891" rank="species">anceps</taxon_name>
    <place_of_publication>
      <publication_title>Bulletin of the Torrey Botanical Club</publication_title>
      <place_in_publication>18:360. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family eriocaulaceae;genus lachnocaulon;species anceps</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000198</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriocaulon</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">anceps</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>82. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriocaulon;species anceps;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriocaulon</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">villosum</taxon_name>
    <taxon_hierarchy>genus Eriocaulon;species villosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lachnocaulon</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">floridanum</taxon_name>
    <taxon_hierarchy>genus Lachnocaulon;species floridanum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lachnocaulon</taxon_name>
    <taxon_name authority="Körnicke" date="unknown" rank="species">glabrum</taxon_name>
    <taxon_hierarchy>genus Lachnocaulon;species glabrum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, forming domes of rosettes, 15–40 cm.</text>
      <biological_entity id="o7340" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o7341" name="dome" name_original="domes" src="d0_s0" type="structure" />
      <biological_entity id="o7342" name="rosette" name_original="rosettes" src="d0_s0" type="structure" />
      <relation from="o7340" id="r974" name="forming" negation="false" src="d0_s0" to="o7341" />
      <relation from="o7340" id="r975" name="part_of" negation="false" src="d0_s0" to="o7342" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves narrowly to broadly linear, gradually attenuate, 2.5–6 (–12) cm.</text>
      <biological_entity id="o7343" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: scape sheaths as long as leaves;</text>
      <biological_entity id="o7344" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity constraint="scape" id="o7345" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <biological_entity id="o7346" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o7345" id="r976" name="as long as" negation="false" src="d0_s2" to="o7346" />
    </statement>
    <statement id="d0_s3">
      <text>scapes filiform, 0.5 mm thick distally, obscurely 4–5-ribbed, pilose from base to apex (s Florida extreme of species glabrous-scaped);</text>
      <biological_entity id="o7347" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o7348" name="scape" name_original="scapes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character modifier="distally" name="thickness" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_shape" src="d0_s3" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character constraint="from base" constraintid="o7349" is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o7349" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o7350" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <relation from="o7349" id="r977" name="to" negation="false" src="d0_s3" to="o7350" />
    </statement>
    <statement id="d0_s4">
      <text>mature heads whitish to pale gray, globose to short-cylindric, 4–7 (–9) mm wide;</text>
      <biological_entity id="o7351" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure" />
      <biological_entity id="o7352" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s4" to="pale gray" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s4" to="short-cylindric" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>receptacle densely pilose;</text>
      <biological_entity id="o7353" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o7354" name="receptacle" name_original="receptacle" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer involucral-bracts becoming reflexed, brownish, oblong to triangular, 1–1.5 mm, apex acute or obtuse, surfaces hairy abaxially and at margins;</text>
      <biological_entity id="o7355" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="outer" id="o7356" name="involucral-bract" name_original="involucral-bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7357" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o7358" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7359" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <relation from="o7358" id="r978" name="at" negation="false" src="d0_s6" to="o7359" />
    </statement>
    <statement id="d0_s7">
      <text>receptacular-bracts dark-brown, mostly spatulate, 1.5–2 mm, apex obtuse, distally with white, club-shaped hairs.</text>
      <biological_entity id="o7360" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o7361" name="receptacular-bract" name_original="receptacular-bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s7" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7362" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o7363" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s7" value="club--shaped" value_original="club--shaped" />
      </biological_entity>
      <relation from="o7362" id="r979" modifier="distally" name="with" negation="false" src="d0_s7" to="o7363" />
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: sepals 3, dark-brown, narrowly spatulate, 1.5–2 mm, apex acute, abaxially with white hairs;</text>
      <biological_entity id="o7364" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7365" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7366" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7367" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <relation from="o7366" id="r980" modifier="abaxially" name="with" negation="false" src="d0_s8" to="o7367" />
    </statement>
    <statement id="d0_s9">
      <text>androphore pale, narrowly club-shaped, 1.5 mm, glabrous;</text>
      <biological_entity id="o7368" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7369" name="androphore" name_original="androphore" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="club--shaped" value_original="club--shaped" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 3, appendages 3.</text>
      <biological_entity id="o7370" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o7371" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o7372" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate flowers: sepals 3, pale, oblong to broadly linear, 2 mm (enlarging to 3 mm), apex acute, distal abaxial surface pubescent, hairs white;</text>
      <biological_entity id="o7373" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7374" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="broadly linear" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7375" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o7376" name="surface" name_original="surface" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7377" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>gynoecium 3-carpellate;</text>
      <biological_entity id="o7378" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7379" name="gynoecium" name_original="gynoecium" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-carpellate" value_original="3-carpellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stylar column pale, dilated apically, appendages 3.</text>
      <biological_entity id="o7380" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="stylar" id="o7381" name="column" name_original="column" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o7382" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds pale to dark-brown, not lustrous, ellipsoid, 0.5–0.55 mm, longitudinal ribs conspicuous, pale, transverse ribs less conspicuous, pattern fine but coarser than in L. beyrichianum.</text>
      <biological_entity id="o7383" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s14" to="dark-brown" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s14" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7384" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o7385" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s14" value="transverse" value_original="transverse" />
        <character is_modifier="false" modifier="less" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="width" src="d0_s14" value="fine" value_original="fine" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sands and peats of shores, pine savanna, bog and seep edges, flatwoods clearings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sands" constraint="of shores , pine savanna ," />
        <character name="habitat" value="peats" constraint="of shores , pine savanna ," />
        <character name="habitat" value="shores" />
        <character name="habitat" value="pine savanna" />
        <character name="habitat" value="bog" />
        <character name="habitat" value="seep edges" />
        <character name="habitat" value="flatwoods clearings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex., Va.; West Indies (Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  
</bio:treatment>