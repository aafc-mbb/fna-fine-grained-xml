<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">najadaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">najas</taxon_name>
    <taxon_name authority="(Willdenow) Rostkovius &amp; W. L. E. Schmidt" date="1824" rank="species">flexilis</taxon_name>
    <place_of_publication>
      <publication_title>Flora Sedinensis</publication_title>
      <place_in_publication>382. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family najadaceae;genus najas;species flexilis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000255</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caulinia</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">flexilis</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Acad. Roy. Sci. Hist. (Berlin)</publication_title>
      <place_in_publication>1798: 89, plate1, fig. 1. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Caulinia;species flexilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Najas</taxon_name>
    <taxon_name authority="(Maguire) Reveal" date="unknown" rank="species">caespitosus</taxon_name>
    <taxon_hierarchy>genus Najas;species caespitosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Najas.</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">canadensis</taxon_name>
    <taxon_hierarchy>genus Najas.;species canadensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems often profusely branched distally, 2.5–5 cm × 0.2–0.6 mm;</text>
      <biological_entity id="o4525" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often profusely; distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s0" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>internodes 0.16–6.8 cm, without prickles.</text>
      <biological_entity id="o4526" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.16" from_unit="cm" name="some_measurement" src="d0_s1" to="6.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4527" name="prickle" name_original="prickles" src="d0_s1" type="structure" />
      <relation from="o4526" id="r600" name="without" negation="false" src="d0_s1" to="o4527" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading to ascending with age, 0.2–0.6 cm, lax in age;</text>
      <biological_entity id="o4528" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="with age" constraintid="o4529" from="spreading" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="0.6" to_unit="cm" />
        <character constraint="in age" constraintid="o4530" is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o4529" name="age" name_original="age" src="d0_s2" type="structure" />
      <biological_entity id="o4530" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>sheath 0.7–1.6 mm wide, apex rounded;</text>
      <biological_entity id="o4531" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s3" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4532" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 0.2–0.6 mm wide, margins minutely serrulate, teeth 35–80 per side, apex acute, with 1–2 teeth, teeth unicellular;</text>
      <biological_entity id="o4533" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4534" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o4535" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o4536" from="35" name="quantity" src="d0_s4" to="80" />
      </biological_entity>
      <biological_entity id="o4536" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o4537" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4538" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity id="o4539" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <relation from="o4537" id="r601" name="with" negation="false" src="d0_s4" to="o4538" />
    </statement>
    <statement id="d0_s5">
      <text>midvein without prickles abaxially.</text>
      <biological_entity id="o4540" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o4541" name="prickle" name_original="prickles" src="d0_s5" type="structure" />
      <relation from="o4540" id="r602" name="without" negation="false" src="d0_s5" to="o4541" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 1 (–2) per axil, staminate and pistillate on same plants.</text>
      <biological_entity id="o4542" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character constraint="per axil" constraintid="o4543" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="staminate" value_original="staminate" />
        <character constraint="on plants" constraintid="o4544" is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o4543" name="axil" name_original="axil" src="d0_s6" type="structure" />
      <biological_entity id="o4544" name="plant" name_original="plants" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers in distal axils, 1.1–2.7 mm;</text>
      <biological_entity id="o4545" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4546" name="axil" name_original="axils" src="d0_s7" type="structure" />
      <relation from="o4545" id="r603" name="in" negation="false" src="d0_s7" to="o4546" />
    </statement>
    <statement id="d0_s8">
      <text>involucral beaks 3-lobed, 0.7–1.2 mm;</text>
      <biological_entity id="o4547" name="beak" name_original="beaks" src="d0_s8" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s8" value="involucral" value_original="involucral" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anther 1-loculed, 1.1–2.7 mm.</text>
      <biological_entity id="o4548" name="anther" name_original="anther" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-loculed" value_original="1-loculed" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers in distal to proximal axils, 2.5–4.7 mm;</text>
      <biological_entity id="o4549" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4550" name="axil" name_original="axils" src="d0_s10" type="structure" />
      <relation from="o4549" id="r604" name="in" negation="false" src="d0_s10" to="o4550" />
    </statement>
    <statement id="d0_s11">
      <text>styles 1.5–1.7 mm;</text>
      <biological_entity id="o4551" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stigmas 3-lobed.</text>
      <biological_entity id="o4552" name="stigma" name_original="stigmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds not recurved, deep brown to yellow, narrowly to broadly obovoid, (1.2–) 2.5–3.7 × 0.2–1.2 mm, apex with style situated at center;</text>
      <biological_entity id="o4553" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="deep" value_original="deep" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="yellow" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_length" src="d0_s13" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4554" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <biological_entity id="o4555" name="style" name_original="style" src="d0_s13" type="structure" />
      <biological_entity id="o4556" name="center" name_original="center" src="d0_s13" type="structure" />
      <relation from="o4554" id="r605" name="with" negation="false" src="d0_s13" to="o4555" />
      <relation from="o4555" id="r606" name="situated at" negation="false" src="d0_s13" to="o4556" />
    </statement>
    <statement id="d0_s14">
      <text>testa glossy, 3 cell-layers thick, smooth;</text>
      <biological_entity id="o4557" name="testa" name_original="testa" src="d0_s14" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s14" value="glossy" value_original="glossy" />
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>aeroleareoles regularly arranged in ca. 50 longitudinal rows, not ladderlike, 3–4-angled, longer than broad, end walls not raised.</text>
      <biological_entity id="o4558" name="cell-layer" name_original="cell-layers" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thick" value_original="thick" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character constraint="in rows" constraintid="o4559" is_modifier="false" modifier="regularly" name="arrangement" src="d0_s15" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o4559" name="row" name_original="rows" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="50" value_original="50" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s15" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="ladderlike" value_original="ladderlike" />
        <character is_modifier="false" name="shape" src="d0_s15" value="3-4-angled" value_original="3-4-angled" />
        <character is_modifier="false" name="length_or_size" src="d0_s15" value="longer than broad" value_original="longer than broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 12, 24.</text>
      <biological_entity constraint="end" id="o4560" name="wall" name_original="walls" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s15" value="raised" value_original="raised" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4561" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lakes and rivers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rivers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Calif., Conn., Del., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., N.H., N.J., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Utah, Vt., Va., Wash., W.Va., Wis.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <discussion>In habit, Najas flexilis is most similar to N. guadalupensis. When seeds are present, N. flexilis can be separated easily from the latter species by the glossy, smooth, yellowish seeds that are widest above the middle. In the northern United States and in Canada, N. flexilis is by far the most common species of Najas, although in the Ohio and surrounding areas, it is disappearing as eutrophication (depletion of oxygen from lakes) continues (W. A. Wentz and R. L. Stuckey 1971).</discussion>
  <references>
    <reference>Posluszny, U. and R. Sattler. 1976. Floral development of Najas flexilis. Canad. J. Bot. 54: 1140–1151.</reference>
  </references>
  
</bio:treatment>