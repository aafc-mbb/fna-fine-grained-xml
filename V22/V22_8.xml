<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Septati</taxon_name>
    <taxon_name authority="Coville in N. L. Britton and A. Brown" date="1913" rank="species">nodatus</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton and A. Brown,Ill. Fl. N. U.S., ed. 2</publication_title>
      <place_in_publication>1: 482. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus septati;species nodatus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000159</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">acuminatus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">robustus</taxon_name>
    <taxon_hierarchy>genus Juncus;species acuminatus;variety robustus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="(Engelmann) Coville" date="unknown" rank="species">robustus</taxon_name>
    <place_of_publication>
      <place_in_publication>1896,</place_in_publication>
      <other_info_on_pub>not S. Watson 1879</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species robustus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, 3–10 dm.</text>
      <biological_entity id="o14773" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots without terminal tubers.</text>
      <biological_entity id="o14774" name="root" name_original="roots" src="d0_s1" type="structure" />
      <biological_entity constraint="terminal" id="o14775" name="tuber" name_original="tubers" src="d0_s1" type="structure" />
      <relation from="o14774" id="r1951" name="without" negation="false" src="d0_s1" to="o14775" />
    </statement>
    <statement id="d0_s2">
      <text>Culms erect, terete, 4–6 mm diam., smooth.</text>
      <biological_entity id="o14776" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cataphylls 1–2, straw-colored, apex acute.</text>
      <biological_entity id="o14777" name="cataphyll" name_original="cataphylls" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="2" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
      <biological_entity id="o14778" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 1–2, cauline 1–2;</text>
      <biological_entity id="o14779" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o14780" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o14781" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles 1.2–1.5 mm, apex rounded, scarious;</text>
      <biological_entity id="o14782" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o14783" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14784" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade straw-colored or green, terete, 20–65 cm × 1.1–3.5 mm, with prominent and conspicuous ringlike bands at position of cross partitions;</text>
      <biological_entity id="o14785" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o14786" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s6" value="terete" value_original="terete" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s6" to="65" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14787" name="band" name_original="bands" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="prominence" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="shape" src="d0_s6" value="ringlike" value_original="ringlike" />
      </biological_entity>
      <biological_entity id="o14788" name="partition" name_original="partitions" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="cross" value_original="cross" />
      </biological_entity>
      <relation from="o14786" id="r1952" name="with" negation="false" src="d0_s6" to="o14787" />
      <relation from="o14787" id="r1953" name="at" negation="false" src="d0_s6" to="o14788" />
    </statement>
    <statement id="d0_s7">
      <text>distal cauline leaves reduced to 2.5 cm.</text>
      <biological_entity id="o14789" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal cauline" id="o14790" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="to 2.5 cm" is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal panicles of 30–250 heads, 8–12 cm, branches spreading;</text>
      <biological_entity id="o14791" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity constraint="terminal" id="o14792" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14793" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s8" to="250" />
      </biological_entity>
      <biological_entity id="o14794" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o14792" id="r1954" name="consist_of" negation="false" src="d0_s8" to="o14793" />
    </statement>
    <statement id="d0_s9">
      <text>primary bract erect to ascending;</text>
      <biological_entity constraint="primary" id="o14795" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>heads 2–10-flowered, broadly obovoid to hemispheric, 0.3–0.5 mm diam.</text>
      <biological_entity id="o14796" name="head" name_original="heads" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-10-flowered" value_original="2-10-flowered" />
        <character char_type="range_value" from="broadly obovoid" name="shape" src="d0_s10" to="hemispheric" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: tepals straw-colored, lance-subulate, apex acuminate;</text>
      <biological_entity id="o14797" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14798" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lance-subulate" value_original="lance-subulate" />
      </biological_entity>
      <biological_entity id="o14799" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>outer tepals 1.9–2.2 mm;</text>
      <biological_entity id="o14800" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="outer" id="o14801" name="tepal" name_original="tepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner tepals 1.7–2.1 mm;</text>
      <biological_entity id="o14802" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="inner" id="o14803" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s13" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 3, anthers equal filament length.</text>
      <biological_entity id="o14804" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o14805" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o14806" name="anther" name_original="anthers" src="d0_s14" type="structure" />
      <biological_entity id="o14807" name="filament" name_original="filament" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules exserted, straw-colored, 1-locular, ovoid, 1.9–2.5 mm, apex acute, valves separating at dehiscence.</text>
      <biological_entity id="o14808" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s15" value="1-locular" value_original="1-locular" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14809" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14810" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds oblong or ellipsoid, 0.5–0.6 mm, not tailed;</text>
      <biological_entity id="o14811" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>body clear yellowbrown.</text>
      <biological_entity id="o14812" name="body" name_original="body" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="clear yellowbrown" value_original="clear yellowbrown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Fruiting late spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="fruiting time" char_type="range_value" to="late summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Commonly in shallow water, marshy shores, sloughs, wet flatwoods, and savannas, bogs, ditches, wet woods, shores, in standing water to 3 ft 1 m deep</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow water" modifier="commonly in" />
        <character name="habitat" value="marshy shores" />
        <character name="habitat" value="sloughs" />
        <character name="habitat" value="wet flatwoods" />
        <character name="habitat" value="savannas" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="ditches" />
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="shores" constraint="in standing water to 3" />
        <character name="habitat" value="water" />
        <character name="habitat" value="1 m" modifier="ft" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ill., Ind., Kans., Ky., La., Mo., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>88</number>
  
</bio:treatment>