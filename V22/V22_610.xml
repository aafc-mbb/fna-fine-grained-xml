<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. de Jussieu" date="unknown" rank="family">hydrocharitaceae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">elodea</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">canadensis</taxon_name>
    <place_of_publication>
      <publication_title>Flora Boreali-Americana</publication_title>
      <place_in_publication>1: 20. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrocharitaceae;genus elodea;species canadensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">220004673</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elodea</taxon_name>
    <taxon_name authority="H. St. John" date="unknown" rank="species">brandegeeae</taxon_name>
    <taxon_hierarchy>genus Elodea;species brandegeeae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves mostly in 3s, spreading to recurved, linear, oblong, to ovate, 5–13 × (1.1–) 2–5 mm, flat.</text>
      <biological_entity id="o14680" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s0" to="recurved" />
        <character is_modifier="false" name="shape" src="d0_s0" value="linear" value_original="linear" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s0" to="ovate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s0" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s0" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="atypical_width" src="d0_s0" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s0" to="5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s0" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o14681" name="3" name_original="3s" src="d0_s0" type="structure" />
      <relation from="o14680" id="r1939" name="in" negation="false" src="d0_s0" to="o14681" />
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: staminate spathes cylindric, 8.2–13.5 mm;</text>
      <biological_entity id="o14682" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o14683" name="spathe" name_original="spathes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8.2" from_unit="mm" name="some_measurement" src="d0_s1" to="13.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>peduncles abscissing just before or during anthesis;</text>
      <biological_entity id="o14684" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o14685" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character is_modifier="false" name="condition" src="d0_s2" value="abscissing" value_original="abscissing" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pistillate spathes cylindric, 8.3–17.5 mm.</text>
      <biological_entity id="o14686" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o14687" name="spathe" name_original="spathes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="8.3" from_unit="mm" name="some_measurement" src="d0_s3" to="17.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers unisexual;</text>
      <biological_entity id="o14688" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="unisexual" value_original="unisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>staminate flowers: stamens 7–9 (–18), pedicels detaching before or during anthesis;</text>
      <biological_entity id="o14689" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14690" name="stamen" name_original="stamens" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="18" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o14691" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>all filaments connate proximally, forming column;</text>
      <biological_entity id="o14692" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14693" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s6" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o14694" name="column" name_original="column" src="d0_s6" type="structure" />
      <relation from="o14693" id="r1940" name="forming" negation="false" src="d0_s6" to="o14694" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 1.7–3 mm;</text>
      <biological_entity id="o14695" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14696" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pollen in tetrads;</text>
      <biological_entity id="o14697" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o14698" name="pollen" name_original="pollen" src="d0_s8" type="structure" />
      <biological_entity id="o14699" name="tetrad" name_original="tetrads" src="d0_s8" type="structure" />
      <relation from="o14698" id="r1941" name="in" negation="false" src="d0_s8" to="o14699" />
    </statement>
    <statement id="d0_s9">
      <text>pistillate flowers: styles 2.6–4 mm.</text>
      <biological_entity id="o14700" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o14701" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds fusiform, 4–5.7 mm, basal hairs absent.</text>
      <biological_entity id="o14702" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 24 (Britain).</text>
      <biological_entity constraint="basal" id="o14703" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14704" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waters, mostly calcareous, of lakes and rivers</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waters" />
        <character name="habitat" value="calcareous" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rivers" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., N.B., N.S., Ont., Que., Sask.; Ala., Ark., Calif., Colo., Conn., Del., Fla., Idaho, Ill., Ind., Iowa., Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Tenn., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  
</bio:treatment>