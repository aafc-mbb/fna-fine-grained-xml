<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="Ker Gawler" date="1814" rank="species">subaspera</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>39: plate 1597. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species subaspera</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000435</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tradescantia</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">pilosa</taxon_name>
    <taxon_hierarchy>genus Tradescantia;species pilosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o14489" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o14490" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14490" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems often flexuous, 30–100 cm;</text>
      <biological_entity id="o14491" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes pilose to glabrescent.</text>
      <biological_entity id="o14492" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s2" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves spirally arranged, at least proximal ones distinctly petiolate;</text>
      <biological_entity id="o14493" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s3" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o14494" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="at-least" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade dark green, lanceolate-oblong to lanceolate-elliptic or lanceolate, 6–30 × (0.4–) 1–6.5 cm (distal leaf-blades wider than sheaths when sheaths opened, flattened), apex acuminate, glabrous to puberulent.</text>
      <biological_entity id="o14495" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="lanceolate-oblong" name="shape" src="d0_s4" to="lanceolate-elliptic or lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_width" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14496" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, usually also axillary at distal nodes, axillary inflorescences sessile or variously pedunculate;</text>
      <biological_entity id="o14497" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character constraint="at distal nodes" constraintid="o14498" is_modifier="false" modifier="usually" name="position" src="d0_s5" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14498" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity constraint="axillary" id="o14499" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="variously" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts foliaceous.</text>
      <biological_entity id="o14500" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o14501" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s7" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 1–1.7 cm, pilose to glabrous;</text>
      <biological_entity id="o14502" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s8" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 4–10 mm, puberulent with glandular, eglandular, or mixture of glandular, eglandular hairs, occasionally glabrous;</text>
      <biological_entity id="o14503" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character constraint="of hairs" constraintid="o14504" is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14504" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals distinct, light to dark blue, rarely white, broadly ovate, not clawed, 10–15 mm;</text>
      <biological_entity id="o14505" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="light" name="coloration" src="d0_s10" to="dark blue" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens free;</text>
      <biological_entity id="o14506" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments bearded.</text>
      <biological_entity id="o14507" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules 4–6 mm.</text>
      <biological_entity id="o14508" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 2–3 mm. 2n = 12, 24.</text>
      <biological_entity id="o14509" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14510" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall (May–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods along streams and on slopes and bluffs, less commonly dry woods, roadsides, fields, or along railroads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" constraint="along streams" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="slopes" modifier="and on" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="dry woods" modifier="less commonly" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="railroads" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., D.C., Fla., Ga., Ill., Ind., Ky., La., Miss., Mo., N.C., Ohio., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Two varieties are recognized by E. Anderson and R. E. Woodson Jr. (1935): Tradescantia subaspera var. subaspera, with the stems more or less conspicuously flexuous distally and the distal lateral inflorescences sessile (western extensions of Appalachian Plateau from western West Virginia, central Kentucky, and Tennessee to Illinois and Missouri); and T. subaspera var. montana (Britton) Anderson &amp; Woodson, with the stems straight or only slightly flexuous distally and all the lateral inflorescences pedunculate (southern Appalachians from southwestern Virginia to northern Alabama and Georgia, also the coastal plain from northern Florida to Louisiana). Many specimens can only be determined by their locale, so I do not find the separation of the two varieties very meaningful. The distribution record for the District of Columbia is based on a specimen believed to be a garden escape; that from southern Florida on a specimen cited by C. Sinclair (1967).</discussion>
  
</bio:treatment>