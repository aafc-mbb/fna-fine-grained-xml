<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Agardh" date="unknown" rank="family">xyridaceae</taxon_name>
    <taxon_name authority="Gronovius in C. Linnaeus" date="1754" rank="genus">xyris</taxon_name>
    <taxon_name authority="Chapman" date="1860" rank="species">stricta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.</publication_title>
      <place_in_publication>500. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family xyridaceae;genus xyris;species stricta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000480</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, cespitose, 20–90 (–100) cm.</text>
      <biological_entity id="o17022" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems compact.</text>
      <biological_entity id="o17023" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves in narrow fans, 10–60 cm;</text>
      <biological_entity id="o17024" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17025" name="fan" name_original="fans" src="d0_s2" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o17024" id="r2192" name="in" negation="false" src="d0_s2" to="o17025" />
    </statement>
    <statement id="d0_s3">
      <text>sheaths smooth to papillate, base maroon;</text>
      <biological_entity id="o17026" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s3" to="papillate" />
      </biological_entity>
      <biological_entity id="o17027" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="maroon" value_original="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade deep green with red or strong maroon tints, linear-tapering, flat, (2–) 4–10 mm wide, smooth or papillate, margins smooth or minutely scabrous, smooth, or papillate.</text>
      <biological_entity id="o17028" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="depth" src="d0_s4" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character constraint="with red or strong maroon tints , linear-tapering , flat , (2-)4-10 mm" is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity id="o17029" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape sheaths much exceeded by leaves;</text>
      <biological_entity id="o17030" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity constraint="scape" id="o17031" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o17032" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o17031" id="r2193" name="exceeded by" negation="false" src="d0_s5" to="o17032" />
    </statement>
    <statement id="d0_s6">
      <text>scapes linear, distally oval to ancipital 2-edged, 1.5–3 mm wide, 2–several-ribbed, ribs papillate or minutely scabrous;</text>
      <biological_entity id="o17033" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o17034" name="scape" name_original="scapes" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s6" value="oval" value_original="oval" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="2-edged" value_original="2-edged" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-several-ribbed" value_original="2-several-ribbed" />
      </biological_entity>
      <biological_entity id="o17035" name="rib" name_original="ribs" src="d0_s6" type="structure">
        <character is_modifier="false" name="relief" src="d0_s6" value="papillate" value_original="papillate" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikes ellipsoid to ovoid or cylindric, (10–) 15–30 mm, apex blunt to acute;</text>
      <biological_entity id="o17036" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o17037" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="ovoid or cylindric" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17038" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s7" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>fertile bracts 5–7 mm, margins entire, apex slightly keeled, convex.</text>
      <biological_entity id="o17039" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o17040" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17041" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o17042" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: lateral sepals included, reddish-brown, curved, 5–7 mm, keel firm, ciliate;</text>
      <biological_entity id="o17043" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o17044" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="included" value_original="included" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17045" name="keel" name_original="keel" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals unfolding in morning, blade obtriangular, 3–4 mm;</text>
      <biological_entity id="o17046" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17047" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character constraint="in morning" constraintid="o17048" is_modifier="false" name="shape" src="d0_s10" value="unfolding" value_original="unfolding" />
      </biological_entity>
      <biological_entity id="o17048" name="morning" name_original="morning" src="d0_s10" type="structure" />
      <biological_entity id="o17049" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtriangular" value_original="obtriangular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes bearded.</text>
      <biological_entity id="o17050" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17051" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds mealy, cylindro-fusiform, 0.6–0.8 mm, lined longitudinally with papillae, less distinctly cross-lined.</text>
      <biological_entity id="o17053" name="papilla" name_original="papillae" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o17052" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="mealy" value_original="mealy" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
        <character constraint="with papillae" constraintid="o17053" is_modifier="false" name="architecture" src="d0_s12" value="lined" value_original="lined" />
        <character is_modifier="false" modifier="less distinctly" name="coloration" notes="" src="d0_s12" value="cross-lined" value_original="cross-lined" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17054" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss., N.C., S.C., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <discussion>Xyris stricta forms a complex with X. ambigua and possibly arose as an independently breeding hybrid of X. ambigua and X. laxifolia within whose ranges it grows. I have labored since 1966 to determine its taxonomic status and at first believed it to be an extreme of X. ambigua with farinous seeds and smaller petals; then later I determined it to be a distinct species (X. obscura Kral, nom. nud.). Since then I have continued to make field observations and collections of the plant and have arrived at a different conclusion, allowing a more accurate and consistent taxonomy. On the one hand, the plant recently described as X. louisianica ranges to drier habitat than is usual for X. stricta and there commonly mingles with X. ambigua. But in character the plant overlaps most strongly with X. stricta, certainly in habit, pigmentation, leaf shape, bract and sepal character, corolla, and seed. The morphometrics are an indication that these plants have evolved in the Gulf Coastal Plain from X. stricta and are backcrossing with it.</discussion>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 50–90(–104) cm high; leaves 20–60 cm; blade 3–8 mm wide, edges smooth to ciliolate or papillate; scapes distally sharply 2-ribbed, these making edges, but with no or few additional low ribs; ribs smooth, scabrous, or cilioltate; spikes lance-cylindric, or cylindric, 2–3 cm; fertile bracts 6–7 mm; sepals averaging 5–6.5 mm</description>
      <determination>7a Xyris stricta var. stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants mostly 40–80 cm high; leaves 20–40 cm; blade (2–)2.5–3(–4) mm wide, edges scabrous or scabro-ciliolate; scapes distally strongly 2-ribbed, these making edges, but with several additional ribs between; all ribs minutely scabrous or papillate; spikes narrowly ovoid, lanceoloid, or ellipsoid, under 2 cm; fertile bracts 5–6.5 mm; sepals averaging 5–6 mm</description>
      <determination>7b Xyris stricta var. obscura</determination>
    </key_statement>
  </key>
</bio:treatment>