<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">bromeliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tillandsia</taxon_name>
    <taxon_name authority="Schultes f. in J. J. Roemer and J. A. Schultes" date="1830" rank="species">balbisiana</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Roemer and J. A. Schultes,Syst. Veg.</publication_title>
      <place_in_publication>7(2):1212. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bromeliaceae;genus tillandsia;species balbisiana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">222000392</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants single or clustering, flowering to 75 cm.</text>
      <biological_entity id="o6234" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustering" value_original="clustering" />
        <character is_modifier="false" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short.</text>
      <biological_entity id="o6235" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 15–30, many-ranked, reflexed, twisted or contorted, gray, often flushed red, to 65 × 0.6–1.4 cm, appressed-grayish-scaly;</text>
      <biological_entity id="o6236" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s2" to="30" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="many-ranked" value_original="many-ranked" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s2" value="flushed red" value_original="flushed red" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="65" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s2" to="1.4" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s2" value="appressed-grayish-scaly" value_original="appressed-grayish-scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath conspicuously rust-colored toward base, ovate to elliptic, conspicuously inflated, forming small pseudobulb, 2–4 cm wide;</text>
      <biological_entity id="o6237" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character constraint="toward base" constraintid="o6238" is_modifier="false" modifier="conspicuously" name="coloration" src="d0_s3" value="rust-colored" value_original="rust-colored" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s3" to="elliptic" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6238" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o6239" name="pseudobulb" name_original="pseudobulb" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
      </biological_entity>
      <relation from="o6237" id="r827" name="forming" negation="false" src="d0_s3" to="o6239" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear-triangular, leathery, channeled to involute, apex attenuate.</text>
      <biological_entity id="o6240" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-triangular" value_original="linear-triangular" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="channeled" name="shape" src="d0_s4" to="involute" />
      </biological_entity>
      <biological_entity id="o6241" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape conspicuous, erect, 8–30 cm, 2–4 mm diam.;</text>
      <biological_entity id="o6242" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o6243" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts densely imbricate, spreading, recurved and twisted like leaves;</text>
      <biological_entity id="o6244" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o6245" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character constraint="like leaves" constraintid="o6246" is_modifier="false" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o6246" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>sheath of bracts narrowing gradually into blade;</text>
      <biological_entity id="o6247" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o6248" name="sheath" name_original="sheath" src="d0_s7" type="structure">
        <character constraint="into blade" constraintid="o6250" is_modifier="false" name="width" src="d0_s7" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o6249" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o6250" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <relation from="o6248" id="r828" name="part_of" negation="false" src="d0_s7" to="o6249" />
    </statement>
    <statement id="d0_s8">
      <text>spikes erect, 2-pinnate, linear, compressed, 2–10 × 1 cm, apex acute;</text>
      <biological_entity id="o6251" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o6252" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="10" to_unit="cm" />
        <character name="width" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6253" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 2–10 (rarely simple).</text>
      <biological_entity id="o6254" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o6255" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Floral bracts imbricate, erect, green to red, broad (covering all or most of rachis, rachis not visible at anthesis), elliptic, keeled, 1.5–2 cm, leathery, base not visible at anthesis, apex acute, surfaces glabrous to inconspicuously scaly near apex only, venation even to slight.</text>
      <biological_entity constraint="floral" id="o6256" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s10" to="red" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s10" to="2" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o6257" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o6258" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6259" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="near apex" constraintid="o6260" from="glabrous" name="pubescence" src="d0_s10" to="inconspicuously scaly" />
        <character is_modifier="false" modifier="even" name="prominence" notes="" src="d0_s10" value="slight" value_original="slight" />
      </biological_entity>
      <biological_entity id="o6260" name="apex" name_original="apex" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5–30, conspicuous;</text>
      <biological_entity id="o6261" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="30" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals with adaxial pair connate, lanceolate, keeled, 1.5–2 cm, leathery, apex acute, surfaces glabrous;</text>
      <biological_entity id="o6262" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s12" to="2" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6263" name="pair" name_original="pair" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o6264" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6265" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o6262" id="r829" name="with" negation="false" src="d0_s12" to="o6263" />
    </statement>
    <statement id="d0_s13">
      <text>corolla tubular;</text>
      <biological_entity id="o6266" name="corolla" name_original="corolla" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals erect, violet, ligulate, to 3.5 cm;</text>
      <biological_entity id="o6267" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="violet" value_original="violet" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s14" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted;</text>
      <biological_entity id="o6268" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma exserted, conduplicate-spiral.</text>
      <biological_entity id="o6269" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s16" value="conduplicate-spiral" value_original="conduplicate-spiral" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits to 4 cm.</text>
      <biological_entity id="o6270" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s17" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic on a variety of hosts in open woods, cypress swamps, coastal forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="a variety" constraint="of hosts in open woods , cypress swamps ," />
        <character name="habitat" value="hosts" constraint="in open woods , cypress swamps ," />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="cypress swamps" />
        <character name="habitat" value="epiphytic on a variety of hosts in open woods" />
        <character name="habitat" value="coastal forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  
</bio:treatment>