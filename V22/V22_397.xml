<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">cannaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">canna</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">glauca</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 1. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cannaceae;genus canna;species glauca</taxon_hierarchy>
    <other_info_on_name type="fna_id">200028485</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes far-creeping, 0.5–1.5 cm diam.</text>
      <biological_entity id="o12659" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="far-creeping" value_original="far-creeping" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="diameter" src="d0_s0" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: sheath and blade glaucous;</text>
      <biological_entity id="o12660" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12661" name="sheath" name_original="sheath" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o12662" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade narrowly ovate, 28–70 × 1.5–14 cm, base cuneate, apex very gradually narrowing to acute.</text>
      <biological_entity id="o12663" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12664" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="28" from_unit="cm" name="length" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12665" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12666" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="very gradually" name="width" src="d0_s2" value="narrowing" value_original="narrowing" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences racemes, simple or occasionally branched, bearing 2-flowered cincinni, more than 10 flowers per inflorescence;</text>
      <biological_entity constraint="inflorescences" id="o12667" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12668" name="cincinnus" name_original="cincinni" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
      <biological_entity id="o12669" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o12670" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <relation from="o12667" id="r1687" name="bearing" negation="false" src="d0_s3" to="o12668" />
      <relation from="o12669" id="r1688" name="per" negation="false" src="d0_s3" to="o12670" />
    </statement>
    <statement id="d0_s4">
      <text>primary bracts 10–30 cm;</text>
      <biological_entity constraint="primary" id="o12671" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>secondary bracts 5–20 cm;</text>
      <biological_entity constraint="secondary" id="o12672" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bracts persistent, (broadly) ovate-triangular, 0.7–2.5 × 0.4–1 cm, apex entire or irregularly lobed, glaucous;</text>
      <biological_entity constraint="floral" id="o12673" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate-triangular" value_original="ovate-triangular" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12674" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles (broadly) ovate-triangular, 0.3–2 cm × 4–8 mm, apex entire or irregularly lobed.</text>
      <biological_entity id="o12675" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-triangular" value_original="ovate-triangular" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12676" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers pure yellow, 7.5–10 cm;</text>
      <biological_entity id="o12677" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels short, to 0.5 cm;</text>
      <biological_entity id="o12678" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals narrowly elliptic-triangular, 1.2–2 × 0.3–0.5 cm;</text>
      <biological_entity id="o12679" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="elliptic-triangular" value_original="elliptic-triangular" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s10" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s10" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals erect, 5–9 cm, tube 1–2 cm, lobes narrowly ovate, 4–7 × 0.7–1.1 cm;</text>
      <biological_entity id="o12680" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s11" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12681" name="tube" name_original="tube" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12682" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s11" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s11" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>staminodes 4, narrowly elliptic to narrowly ovate, 7.5–10 cm, free part 0.5–2.3 cm wide, apex sometimes slightly notched;</text>
      <biological_entity id="o12683" name="staminode" name_original="staminodes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s12" to="narrowly ovate" />
        <character char_type="range_value" from="7.5" from_unit="cm" name="some_measurement" src="d0_s12" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12684" name="part" name_original="part" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="free" value_original="free" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s12" to="2.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12685" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s12" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>labellum strongly reflexed, linear, approximately equal to other staminodes.</text>
      <biological_entity id="o12686" name="labellum" name_original="labellum" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character constraint="to staminodes" constraintid="o12687" is_modifier="false" modifier="approximately" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o12687" name="staminode" name_original="staminodes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsules globose to ellipsoid, 2–5 × 2–4 cm.</text>
      <biological_entity id="o12688" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s14" to="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s14" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s14" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds brown, ovoid, 7–10 × 6–8 mm. 2n = 18.</text>
      <biological_entity id="o12689" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s15" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12690" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer; fruiting summer–fall (Jun–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
        <character name="fruiting time" char_type="range_value" to="fall" from="summer" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins of marshes, swamps, ponds, and wet ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of marshes , swamps , ponds , and wet ditches" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="wet ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., S.C., Tex.; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Louisiana canna</other_name>
  <discussion>Canna glauca is introduced in Florida and probably in South Carolina.</discussion>
  <references>
    <reference>Belling, J. 1931. Chromomeres of liliaceous plants. Univ. Calif. Publ. Bot. 16: 153–170.</reference>
    <reference>Lerman, J. C. and E. M. Cigliano. 1971. New carbon-14 evidence for six hundred years old Canna compacta seed. Nature 232: 568–570.</reference>
    <reference>Yeo, P. F. 1993. Secondary pollen presentation: Form, function and evolution. Pl. Syst. Evol., Suppl. 6: 204–208.</reference>
    <reference>Honing, J. A. 1928. Canna crosses II. Meded. Landbouwhoogeschool 32: 1–14.</reference>
  </references>
  
</bio:treatment>