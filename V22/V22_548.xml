<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">commelinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tradescantia</taxon_name>
    <taxon_name authority="E. S. Anderson &amp; Woodson" date="1935" rank="species">ernestiana</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Arnold Arbor.</publication_title>
      <place_in_publication>9: 58, plate 8, map 4. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family commelinaceae;genus tradescantia;species ernestiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000414</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect or ascending, rarely rooting at nodes.</text>
      <biological_entity id="o14469" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
        <character constraint="at nodes" constraintid="o14470" is_modifier="false" modifier="rarely" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
        <character name="growth_form" value="herb" />
      </biological_entity>
      <biological_entity id="o14470" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots (1–) 1.5–5 mm thick, fleshy.</text>
      <biological_entity id="o14471" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not flexuous, 5–40 cm;</text>
      <biological_entity id="o14472" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s2" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes usually glabrous.</text>
      <biological_entity id="o14473" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves spirally arranged, sessile;</text>
      <biological_entity id="o14474" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade dull green, linear-lanceolate to lanceolate-oblong, 9–27 × 1–4 cm (distal leaf-blades wider than sheaths when sheaths opened, flattened), base cuneate to rounded, apex acuminate, not glaucous, glabrous or sparsely puberulent.</text>
      <biological_entity id="o14475" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="lanceolate-oblong" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s5" to="27" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14476" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="rounded" />
      </biological_entity>
      <biological_entity id="o14477" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal;</text>
      <biological_entity id="o14478" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts foliaceous.</text>
      <biological_entity id="o14479" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers distinctly pedicillate;</text>
      <biological_entity id="o14480" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture" src="d0_s8" value="pedicillate" value_original="pedicillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2–3.2 cm, minutely pilose;</text>
      <biological_entity id="o14481" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3.2" to_unit="cm" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals somewhat inflated, 9–16 mm, uniformly eglandular-pilose;</text>
      <biological_entity id="o14482" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="uniformly" name="pubescence" src="d0_s10" value="eglandular-pilose" value_original="eglandular-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals distinct, deep blue, purple, or rose-red, broadly ovate, not clawed, 1.2–1.5 cm;</text>
      <biological_entity id="o14483" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="blue" value_original="blue" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rose-red" value_original="rose-red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rose-red" value_original="rose-red" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens free;</text>
      <biological_entity id="o14484" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments bearded.</text>
      <biological_entity id="o14485" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 5–7 mm.</text>
      <biological_entity id="o14486" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds 2–3 mm. 2n = 12.</text>
      <biological_entity id="o14487" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14488" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring (Mar–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" constraint="Mar-May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded hillsides, ledges and bluffs, occasionally along streams or in pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hillsides" modifier="wooded" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="streams" modifier="occasionally along" />
        <character name="habitat" value="pastures" modifier="or in" />
        <character name="habitat" value="wooded" modifier="occasionally" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <discussion>Tradescantia ernestiana is sympatric with, and easily confused with, T. virginiana in northern Alabama and perhaps northern Georgia [reported from Georgia by C. Sinclair (1967, p. 87), but no specimens are cited and I have seen none]. At present, the two species can be separated only by the relative width of the blade and sheath of the distal leaves. They are obviously closely related and should be studied in the field in the southern Appalachians where their ranges overlap. The Texas record is taken from C. Sinclair (1967).</discussion>
  <discussion>The hybrid Tradescantia ernestinana × T. ozarkana is known from Arkansas and Missouri.</discussion>
  
</bio:treatment>