<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">bromeliaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">tillandsia</taxon_name>
    <taxon_name authority="H. Luther" date="1985" rank="species">×smalliana</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>57:176. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bromeliaceae;genus tillandsia;species ×smalliana</taxon_hierarchy>
    <other_info_on_name type="fna_id">222000390</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants single or clustering, flowering to 50 cm.</text>
      <biological_entity id="o9042" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustering" value_original="clustering" />
        <character is_modifier="false" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short.</text>
      <biological_entity id="o9043" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 20–40, many-ranked, erect to spreading, gray, 30–45 × 1–2.2 cm, appressed-grayish-scaly;</text>
      <biological_entity id="o9044" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s2" to="40" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="many-ranked" value_original="many-ranked" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="2.2" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s2" value="appressed-grayish-scaly" value_original="appressed-grayish-scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheath dark rust-colored, broadly elliptic, conspicuously inflated, forming small pseudobulb, 3–4 cm wide;</text>
      <biological_entity id="o9045" name="sheath" name_original="sheath" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark rust-colored" value_original="dark rust-colored" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9046" name="pseudobulb" name_original="pseudobulb" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
      </biological_entity>
      <relation from="o9045" id="r1188" name="forming" negation="false" src="d0_s3" to="o9046" />
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly triangular, leathery, channeled to involute, apex attenuate.</text>
      <biological_entity id="o9047" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
        <character char_type="range_value" from="channeled" name="shape" src="d0_s4" to="involute" />
      </biological_entity>
      <biological_entity id="o9048" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences: scape conspicuous, erect, 15–35 cm, 3–6 mm diam.;</text>
      <biological_entity id="o9049" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o9050" name="scape" name_original="scape" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts densely imbricate, erect to spreading, like leaves but gradually smaller;</text>
      <biological_entity id="o9051" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity id="o9052" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
      <biological_entity id="o9053" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o9052" id="r1189" name="like" negation="false" src="d0_s6" to="o9053" />
    </statement>
    <statement id="d0_s7">
      <text>sheath of bracts narrowing gradually into blade;</text>
      <biological_entity id="o9054" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o9055" name="sheath" name_original="sheath" src="d0_s7" type="structure">
        <character constraint="into blade" constraintid="o9057" is_modifier="false" name="width" src="d0_s7" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o9056" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o9057" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <relation from="o9055" id="r1190" name="part_of" negation="false" src="d0_s7" to="o9056" />
    </statement>
    <statement id="d0_s8">
      <text>spikes erect, 2-pinnate, narrowly elliptic, compressed, 2–6 × 1–1.5 cm, apex acute;</text>
      <biological_entity id="o9058" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o9059" name="spike" name_original="spikes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9060" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lateral branches 3–13.</text>
      <biological_entity id="o9061" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o9062" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s9" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Floral bracts imbricate, erect, red, broad (covering all or most of rachis, rachis not visible at anthesis), elliptic, keeled, 2–2.5 cm, leathery, base not visible at anthesis, apex acute, surfaces glabrous, venation even to slight.</text>
      <biological_entity constraint="floral" id="o9063" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s10" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o9064" name="base" name_original="base" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" modifier="not" name="prominence" src="d0_s10" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o9065" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9066" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="even" name="prominence" src="d0_s10" value="slight" value_original="slight" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5–40, conspicuous;</text>
      <biological_entity id="o9067" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="40" />
        <character is_modifier="false" name="prominence" src="d0_s11" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals with adaxial pair connate, lanceolate, keeled, 2–2.4 cm, thin-leathery, veined, apex acute, surfaces slightly scaly;</text>
      <biological_entity id="o9068" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s12" to="2.4" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s12" value="thin-leathery" value_original="thin-leathery" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9069" name="pair" name_original="pair" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o9070" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9071" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture_or_pubescence" src="d0_s12" value="scaly" value_original="scaly" />
      </biological_entity>
      <relation from="o9068" id="r1191" name="with" negation="false" src="d0_s12" to="o9069" />
    </statement>
    <statement id="d0_s13">
      <text>corolla tubular, petals erect, violet, ligulate, to 5 cm;</text>
      <biological_entity id="o9072" name="corolla" name_original="corolla" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o9073" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="violet" value_original="violet" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s13" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens exserted;</text>
      <biological_entity id="o9074" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma exserted, conduplicate-spiral.</text>
      <biological_entity id="o9075" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="arrangement_or_course" src="d0_s15" value="conduplicate-spiral" value_original="conduplicate-spiral" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits to 3.8 cm.</text>
      <biological_entity id="o9076" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s16" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering winter–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic, usually on Taxodium, in swamps and well-lit hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" modifier="epiphytic usually on" />
        <character name="habitat" value="well-lit hammocks" />
        <character name="habitat" value="epiphytic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <discussion>Tillandsia × smalliana has usually been misdetermined as T. polystachia (Linnaeus) Linnaeus, a common Caribbean species not known to occur in Florida.</discussion>
  <discussion>The probable parentage of Tillandsia × smalliana is T. balbisiana Schultes f. × T. fasciculata Swartz.</discussion>
  
</bio:treatment>