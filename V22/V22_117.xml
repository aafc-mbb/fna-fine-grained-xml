<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="A. L. Jussieu" date="unknown" rank="family">juncaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juncus</taxon_name>
    <taxon_name authority="Buchenau" date="1875" rank="subgenus">Graminifolii</taxon_name>
    <taxon_name authority="Coville" date="1893" rank="species">orthophyllus</taxon_name>
    <place_of_publication>
      <publication_title>Contributions from the U. S. National Herbarium</publication_title>
      <place_in_publication>4: 207. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family juncaceae;genus juncus;subgenus graminifolii;species orthophyllus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000162</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">longistylis</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="variety">latifolius</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>2: 496. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species longistylis;variety latifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juncus</taxon_name>
    <taxon_name authority="(Engelmann) Buchenau" date="unknown" rank="species">latifolius</taxon_name>
    <place_of_publication>
      <place_in_publication>1890,</place_in_publication>
      <other_info_on_pub>not Wulfen 1789</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Juncus;species latifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 2–4 dm.</text>
      <biological_entity id="o4783" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes creeping.</text>
      <biological_entity id="o4784" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms compressed.</text>
      <biological_entity id="o4785" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal several, cauline 0–3;</text>
      <biological_entity id="o4786" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o4787" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4788" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles, if present, 0.5–1 mm, apex acutish;</text>
      <biological_entity id="o4789" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" notes="" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4790" name="auricle" name_original="auricles" src="d0_s4" type="structure" />
      <biological_entity id="o4791" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acutish" value_original="acutish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade flat, basal 10–40 cm × 1–6 mm, cauline blade reduced.</text>
      <biological_entity id="o4792" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4793" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4794" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4795" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences glomerules, usually 3–12, each with 5–10 flowers, open;</text>
      <biological_entity constraint="inflorescences" id="o4796" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" modifier="usually" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o4797" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <relation from="o4796" id="r631" name="with" negation="false" src="d0_s6" to="o4797" />
    </statement>
    <statement id="d0_s7">
      <text>primary bract much shorter than inflorescence.</text>
      <biological_entity constraint="primary" id="o4798" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character constraint="than inflorescence" constraintid="o4799" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o4799" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: tepals brown, lanceolate, 5–6 mm, apex obtuse;</text>
      <biological_entity id="o4800" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4801" name="tepal" name_original="tepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4802" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer series shorter, margins scarious, apex acute, minutely papillose;</text>
      <biological_entity id="o4803" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="outer" id="o4804" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4805" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o4806" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s9" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>inner series with margins clear;</text>
      <biological_entity id="o4807" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="inner" id="o4808" name="series" name_original="series" src="d0_s10" type="structure" />
      <biological_entity id="o4809" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="clear" value_original="clear" />
      </biological_entity>
      <relation from="o4808" id="r632" name="with" negation="false" src="d0_s10" to="o4809" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 6, filaments 0.5–1 mm, anthers 1.6–2.4 (–3) mm;</text>
      <biological_entity id="o4810" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4811" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o4812" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4813" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style 0.5–2 mm.</text>
      <biological_entity id="o4814" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o4815" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules tan, 3-locular, obovoid, 3–5 mm, shorter than perianth.</text>
      <biological_entity id="o4816" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="tan" value_original="tan" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s13" value="3-locular" value_original="3-locular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character constraint="than perianth" constraintid="o4817" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4817" name="perianth" name_original="perianth" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Seeds ovoid, 0.6 mm, not tailed.</text>
      <biological_entity id="o4818" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="tailed" value_original="tailed" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting late spring to summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="fruiting time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist ground in mountain meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ground" modifier="moist" constraint="in mountain meadows" />
        <character name="habitat" value="mountain meadows" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31</number>
  <other_name type="common_name">Straight-leaf rush</other_name>
  
</bio:treatment>