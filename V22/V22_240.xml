<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/25 13:26:20</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">22</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ventenat" date="unknown" rank="family">alismataceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">sagittaria</taxon_name>
    <taxon_name authority="Engelmann" date="1883" rank="species">cristata</taxon_name>
    <place_of_publication>
      <publication_title>Proceedings of the Davenport Academy of Natural Sciences</publication_title>
      <place_in_publication>4: 29. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family alismataceae;genus sagittaria;species cristata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">222000330</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sagittaria</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">graminea</taxon_name>
    <taxon_name authority="(Engelmann) Bogin" date="unknown" rank="variety">cristata</taxon_name>
    <taxon_hierarchy>genus Sagittaria;species graminea;variety cristata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial, to 75 cm;</text>
      <biological_entity id="o5124" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes absent;</text>
      <biological_entity id="o5125" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stolons present;</text>
      <biological_entity id="o5126" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>corms present.</text>
      <biological_entity id="o5127" name="corm" name_original="corms" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves emersed, sessile, phyllodial, linear to lanceolate, flattened, 15–25 (–40) × 1.5–4 cm, or petiole triangular, 15–50 cm, blade linear to elliptic-lanceolate, 4–10 × 0.3–2 cm.</text>
      <biological_entity id="o5128" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="location" src="d0_s4" value="emersed" value_original="emersed" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5129" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s4" to="50" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5130" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="elliptic-lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences racemes, of 3–6 whorls, 1.5 × 4 cm;</text>
      <biological_entity constraint="inflorescences" id="o5131" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character name="length" notes="" src="d0_s5" unit="cm" value="1.5" value_original="1.5" />
        <character name="width" notes="" src="d0_s5" unit="cm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o5132" name="whorl" name_original="whorls" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o5131" id="r678" name="consist_of" negation="false" src="d0_s5" to="o5132" />
    </statement>
    <statement id="d0_s6">
      <text>peduncle 20–60 cm;</text>
      <biological_entity id="o5133" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s6" to="60" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts connate greater than or equal to ¼ total length, ovate, 4–10 mm, nearly scarious, not papillose;</text>
    </statement>
    <statement id="d0_s8">
      <text>fruiting pedicels spreading, cylindric, 0.8–3 cm.</text>
      <biological_entity id="o5134" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="connate" value_original="connate" />
        <character is_modifier="false" name="size" src="d0_s7" value="greater than" value_original="greater than" />
        <character is_modifier="false" name="size" src="d0_s7" value="equal to ¼ total length" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5135" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o5134" id="r679" name="fruiting" negation="false" src="d0_s8" to="o5135" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers to 0.8 cm diam.;</text>
      <biological_entity id="o5136" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s9" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals recurved to spreading, not enclosing flower;</text>
      <biological_entity id="o5137" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="recurved" name="orientation" src="d0_s10" to="spreading" />
      </biological_entity>
      <biological_entity id="o5138" name="flower" name_original="flower" src="d0_s10" type="structure" />
      <relation from="o5137" id="r680" name="enclosing" negation="true" src="d0_s10" to="o5138" />
    </statement>
    <statement id="d0_s11">
      <text>filaments dilated, exceeding anthers in length, pubescent;</text>
      <biological_entity id="o5140" name="anther" name_original="anthers" src="d0_s11" type="structure" />
      <relation from="o5139" id="r681" name="exceeding" negation="false" src="d0_s11" to="o5140" />
    </statement>
    <statement id="d0_s12">
      <text>pistillate pedicellate, without ring of sterile stamens.</text>
      <biological_entity id="o5141" name="ring" name_original="ring" src="d0_s12" type="structure" constraint="stamen" constraint_original="stamen; stamen">
        <character is_modifier="false" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o5142" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o5141" id="r682" name="part_of" negation="false" src="d0_s12" to="o5142" />
    </statement>
    <statement id="d0_s13">
      <text>Fruiting heads 1.2–2 cm diam.;</text>
      <biological_entity id="o5139" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="dilated" value_original="dilated" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="diameter" src="d0_s13" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5143" name="head" name_original="heads" src="d0_s13" type="structure" />
      <relation from="o5139" id="r683" name="fruiting" negation="false" src="d0_s13" to="o5143" />
    </statement>
    <statement id="d0_s14">
      <text>achenes cuneate-obovoid, abaxially keeled, 2.5–3 × 1.4–1.8 mm, beaked;</text>
      <biological_entity id="o5144" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cuneate-obovoid" value_original="cuneate-obovoid" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s14" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>faces not tuberculate, wings 1, entire, glands absent;</text>
      <biological_entity id="o5145" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s15" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o5146" name="wing" name_original="wings" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5147" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>beak ascending to horizontal, 0.4–0.7 mm.</text>
      <biological_entity id="o5148" name="beak" name_original="beak" src="d0_s16" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s16" to="horizontal" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jul–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jul-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy margins and bottoms of lakes, ponds, and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy margins" constraint="of lakes , ponds , and swamps" />
        <character name="habitat" value="bottoms" constraint="of lakes , ponds , and swamps" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ill., Iowa, Mich., Minn., Mo., Nebr., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17</number>
  
</bio:treatment>