<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="section">Systylae</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Pl. Hort. Monsp.,</publication_title>
      <place_in_publication>137. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;section Systylae</taxon_hierarchy>
    <other_info_on_name type="fna_id">318050</other_info_on_name>
  </taxon_identification>
  <number>7b.1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, sometimes forming thickets;</text>
      <biological_entity id="o21633" name="thicket" name_original="thickets" src="d0_s0" type="structure" />
      <relation from="o21632" id="r1409" modifier="sometimes" name="forming" negation="false" src="d0_s0" to="o21633" />
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous.</text>
      <biological_entity id="o21632" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to procumbent, climbing, vinelike, (10–) 15–30 (–100) dm;</text>
      <biological_entity id="o21634" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="climbing" value_original="climbing" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="vinelike" value_original="vinelike" />
        <character char_type="range_value" from="10" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="15" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="100" to_unit="dm" />
        <character char_type="range_value" from="15" from_unit="dm" name="some_measurement" src="d0_s2" to="30" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches glabrous, eglandular;</text>
      <biological_entity constraint="distal" id="o21635" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>prickles infrastipular and/or internodal, single or paired, usually curved or declined, sometimes erect, flattened, ± stout, sometimes mixed with aciculi, rarely absent.</text>
      <biological_entity id="o21636" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="internodal" value_original="internodal" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="declined" value_original="declined" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character constraint="with aciculi" constraintid="o21637" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o21637" name="aciculus" name_original="aciculi" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" notes="" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous or semipersistent, 5–12 cm, membranous or leathery;</text>
      <biological_entity id="o21638" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s5" value="semipersistent" value_original="semipersistent" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules persistent, adnate to petiole, auricles flared or erect, margins lacinulose, sometimes entire, eglandular or stipitate-glandular;</text>
      <biological_entity id="o21639" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o21640" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o21640" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o21641" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flared" value_original="flared" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o21642" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 3–9 (–11), terminal: petiolule 5–16 mm, blade elliptic, ovate, elliptic-ovate, or obovate, 10–48 (–70) × 8–27 (–40) mm, abaxial surfaces glabrous or pubescent to tomentose, eglandular, sometimes glandular, adaxial dull or lustrous.</text>
      <biological_entity id="o21643" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="11" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21644" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21645" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="48" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="48" to_unit="mm" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21646" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s7" to="tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21647" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences panicles, 1–6 (–30+) -flowered.</text>
      <biological_entity constraint="inflorescences" id="o21648" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-6(-30+)-flowered" value_original="1-6(-30+)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, slender, 5–25 mm, glabrous or pubescent, eglandular or stipitate-glandular;</text>
      <biological_entity id="o21649" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts persistent, 1–3, margins lacinulose, stipitate-glandular.</text>
      <biological_entity id="o21650" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="3" />
      </biological_entity>
      <biological_entity id="o21651" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1.5–5 cm diam.;</text>
      <biological_entity id="o21652" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s11" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium ovoid, oblong, or urceolate, glabrous, eglandular or stipitate-glandular;</text>
      <biological_entity id="o21653" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals persistent, reflexed, ovate-acuminate or lanceolate, 6–18 × 1–4 mm, margins entire or (outer) pinnatifid, abaxial surfaces glabrous or pubescent, eglandular or stipitate-glandular;</text>
      <biological_entity id="o21654" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s13" to="18" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21655" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s13" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21656" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single or double, pink, rose-purple, or white;</text>
      <biological_entity id="o21657" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character name="quantity" src="d0_s14" value="double" value_original="double" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="rose-purple" value_original="rose-purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="rose-purple" value_original="rose-purple" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 6–25, styles connate in columns, exsert 3–6 mm, sometimes ± free with age, glabrous or pilose, stylar orifice 0.5–2 mm diam., hypanthial disc conic, 2–4 mm diam.</text>
      <biological_entity id="o21658" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s15" to="25" />
      </biological_entity>
      <biological_entity id="o21659" name="style" name_original="styles" src="d0_s15" type="structure">
        <character constraint="in columns" constraintid="o21660" is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" notes="" src="d0_s15" value="exsert" value_original="exsert" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character constraint="with age" constraintid="o21661" is_modifier="false" modifier="sometimes more or less" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21660" name="column" name_original="columns" src="d0_s15" type="structure" />
      <biological_entity id="o21661" name="age" name_original="age" src="d0_s15" type="structure" />
      <biological_entity constraint="stylar" id="o21662" name="orifice" name_original="orifice" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="hypanthial" id="o21663" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Hips red or orange-red, usually globose, sometimes ovoid or subglobose, (4–) 5–10 × 5–9 mm, glabrous, eglandular or sparsely stipitate-glandular;</text>
      <biological_entity id="o21664" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_length" src="d0_s16" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s16" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals late deciduous, reflexed.</text>
      <biological_entity id="o21665" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basiparietal.</text>
      <biological_entity id="o21666" name="achene" name_original="achenes" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia (primarily China), n Africa; introduced in Mexico, West Indies, Central America, South America, Pacific Islands (New Zealand, Philippines).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (primarily China)" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Philippines)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Synstylae</other_name>
  <discussion>Species ca. 35 (3 in the flora).</discussion>
  <discussion>Other species of sect. Systylae, hybrids, and cultivars that spread by stoloniferous shoots and are mostly sterile have escaped from cultivation and are occasionally found. One is Rosa sempervirens Linnaeus, evergreen rose, native to southern Europe, Asiatic Turkey, and northern Africa and found in California (B. Ertter and W. H. Lewis 2008); introduced in 1827 in France, it may also be the basis of a collection from Massachusetts (B. N. Gates 15713, 8 Nov. 1942, CONN), noted as doubtless an escape and spreading by rooting freely at tips of canes, and in Oklahoma, where a scrambling vine was collected (P. Kirtley 68, 6 Oct. 1935, MO). Such occasional findings of R. sempervirens can be expected in mostly warmer areas of the continent where it is readily recognized by its sprawling stems to 60 dm, and usually persistent, leathery, and lustrous leaflets with terminal leaflets distinctly longer than laterals. The species is introduced sporadically in the West Indies, Central America, and South America.</discussion>
  <discussion>Rosa ×moschata Herrmann, musk rose, an escape from mostly southern latitude gardens, is found in Alabama, Illinois (not confirmed), and Louisiana. It is identified by long climbing or prostrate stoloniferous stems reaching 90 dm with erect or slightly curved prickles, leaflets ovate to lanceolate-elliptic, 5–50(–70) mm, mostly glabrous, finely serrate, and adaxially not lustrous, flowers to 5 cm diam. in clusters having a musky fragrance, petals white, or less commonly, pink, often double, sepals narrowly lanceolate and long-acuminate to 20 mm, strongly reflexed, dark red or purple-brown hips.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 3–5, terminal: petiolules 10–16 mm, blade teeth (18–)35(–42) per side; stipule margins entire, sometimes fimbriate; flowers 3–5 cm diam.; sepal margins entire.</description>
      <determination>3 Rosa setigera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets (5–)7–9(–11), terminal: petiolules 5–13 mm, blade teeth 12–20 per side; stipule margins fimbriate; flowers 1.5–2.5 cm diam.; sepal margins pinnatifid</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels 5–12 mm, stipitate-glandular or eglandular, usually pubescent; sepals 6–10 × 1.5–2 mm, abaxial surfaces usually stipitate-glandular; flowers 1.5–2.5 cm diam.; petals single or double; leaflet adaxial surfaces dull, terminal blade membranous.</description>
      <determination>4 Rosa multiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pedicels 18–25 mm, eglandular, glabrous; sepals 6–8 × 1–1.5 mm, abaxial surfaces eglandular; flowers 2–2.5 cm diam.; petals usually double; leaflet adaxial surfaces lustrous, terminal blade leathery.</description>
      <determination>5 Rosa lucieae</determination>
    </key_statement>
  </key>
</bio:treatment>