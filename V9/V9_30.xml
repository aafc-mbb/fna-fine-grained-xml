<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">ulmarieae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">filipendula</taxon_name>
    <taxon_name authority="(S. Watson) Howell" date="1898" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>185. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe ulmarieae;genus filipendula;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100221</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>18: 192. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species occidentalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="Pallas" date="unknown" rank="species">camtschatica</taxon_name>
    <taxon_name authority="(S. Watson) Wenzig" date="unknown" rank="variety">occidentalis</taxon_name>
    <taxon_hierarchy>genus S.;species camtschatica;variety occidentalis</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Queen of the forest</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants weakly rhizomatous, 7–15 dm.</text>
      <biological_entity id="o23022" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="7" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes horizontal, stout, 10–15 mm wide, internodes 1–4 cm;</text>
      <biological_entity id="o23023" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23024" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>root-tubers absent.</text>
      <biological_entity id="o23025" name="root-tuber" name_original="root-tubers" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems finely puberulent distally.</text>
      <biological_entity id="o23026" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="finely; distally" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 1 or 2, deciduous by flowering;</text>
      <biological_entity id="o23027" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o23028" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" unit="or" value="2" value_original="2" />
        <character constraint="by flowering" is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules round, 1–1.5 cm diam., base not auriculate;</text>
      <biological_entity id="o23029" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o23030" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="round" value_original="round" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s5" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23031" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets in 1–3 pairs or lacking, remote, ovate to elliptic, to 1 cm, margins serrate;</text>
      <biological_entity id="o23032" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o23033" name="leaflet" name_original="leaflets" src="d0_s6" type="structure" />
      <biological_entity id="o23034" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o23035" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="remote" value_original="remote" />
        <character char_type="range_value" constraint="in " constraintid="o23037" from="ovate" name="shape" src="d0_s6" to="elliptic" />
      </biological_entity>
      <biological_entity id="o23036" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o23037" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="remote" value_original="remote" />
        <character char_type="range_value" constraint="in " constraintid="o23039" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23038" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o23039" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="remote" value_original="remote" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <relation from="o23033" id="r1497" name="in" negation="false" src="d0_s6" to="o23034" />
      <relation from="o23033" id="r1498" name="in" negation="false" src="d0_s6" to="o23035" />
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflets round, 6–16 cm diam., palmately 5–7-lobed, lobes ovate to oblong-lanceolate, margins doubly serrate, apex acute to acuminate, surfaces short-appressed hairy at least on veins.</text>
      <biological_entity id="o23040" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o23041" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="round" value_original="round" />
        <character char_type="range_value" from="6" from_unit="cm" name="diameter" src="d0_s7" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s7" value="5-7-lobed" value_original="5-7-lobed" />
      </biological_entity>
      <biological_entity id="o23042" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o23043" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o23044" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
      </biological_entity>
      <biological_entity id="o23045" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="short-appressed" value_original="short-appressed" />
        <character constraint="on veins" constraintid="o23046" is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23046" name="vein" name_original="veins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 100+-flowered;</text>
      <biological_entity id="o23047" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="100+-flowered" value_original="100+-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches and pedicels densely short-appressed hairy, hairs straight.</text>
      <biological_entity id="o23048" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="fixation_or_orientation" src="d0_s9" value="short-appressed" value_original="short-appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23049" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="fixation_or_orientation" src="d0_s9" value="short-appressed" value_original="short-appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o23050" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: hypanthium nearly flat, saucer-shaped;</text>
      <biological_entity id="o23051" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o23052" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s10" value="saucer--shaped" value_original="saucer--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (4–) 5 (–6), green, narrowly triangular, 4–6 mm, margins often serrate, usually with midrib, abaxially puberulent, adaxially glabrous;</text>
      <biological_entity id="o23053" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o23054" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23055" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s11" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" notes="" src="d0_s11" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23056" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <relation from="o23055" id="r1499" modifier="usually" name="with" negation="false" src="d0_s11" to="o23056" />
    </statement>
    <statement id="d0_s12">
      <text>petals (4–) 5 (–6), white, in buds sometimes marginally pink, oblanceolate, 10–15 mm, not clawed, base broad, margins entire;</text>
      <biological_entity id="o23057" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o23058" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s12" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="6" />
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o23059" name="bud" name_original="buds" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes marginally" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o23060" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o23061" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o23058" id="r1500" name="in" negation="false" src="d0_s12" to="o23059" />
    </statement>
    <statement id="d0_s13">
      <text>stamens white, about equal to petals.</text>
      <biological_entity id="o23062" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o23063" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character constraint="to petals" constraintid="o23064" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o23064" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Achenes 7–12, flattened, lanceolate, straight, 6–7.5 mm, sutures densely ciliate, faces sparsely hairy;</text>
      <biological_entity id="o23065" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s14" to="12" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23066" name="suture" name_original="sutures" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o23067" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stipes 1–2 mm;</text>
      <biological_entity id="o23068" name="stipe" name_original="stipes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 1.5–2 mm.</text>
      <biological_entity id="o23069" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, mossy rock along forest streams, at or slightly above water level, wet rock on mountain slopes, riverbanks, rocky summits</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" constraint="along forest streams" />
        <character name="habitat" value="mossy rock" constraint="along forest streams" />
        <character name="habitat" value="forest streams" />
        <character name="habitat" value="water level" />
        <character name="habitat" value="wet rock" constraint="on mountain slopes , riverbanks , rocky summits" />
        <character name="habitat" value="mountain slopes" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="rocky summits" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Filipendula occidentalis is known only from the valleys of several small rivers in the coast ranges of northwestern Oregon and southwestern Washington. The species is threatened by logging and associated forest-management activities. The species is most probably related to F. camtschatica (Pallas) Maximowicz from the Pacific coast of Asia.</discussion>
  
</bio:treatment>