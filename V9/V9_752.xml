<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">446</other_info_on_meta>
    <other_info_on_meta type="mention_page">438</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="illustration_page">440</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="unknown" rank="genus">aronia</taxon_name>
    <taxon_name authority="(Michaux) Elliott" date="1821" rank="species">melanocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>1: 557. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus aronia;species melanocarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416098</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mespilus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">arbutifolia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="variety">melanocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 292. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mespilus;species arbutifolia;variety melanocarpa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aronia</taxon_name>
    <taxon_name authority="(Willdenow) Koehne" date="unknown" rank="species">nigra</taxon_name>
    <taxon_hierarchy>genus Aronia;species nigra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Photinia</taxon_name>
    <taxon_name authority="(Michaux) K. R. Robertson &amp; J. B. Phipps" date="unknown" rank="species">melanocarpa</taxon_name>
    <taxon_hierarchy>genus Photinia;species melanocarpa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrus</taxon_name>
    <taxon_name authority="(Linnaeus) Linnaeus f." date="unknown" rank="species">arbutifolia</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="variety">nigra</taxon_name>
    <taxon_hierarchy>genus Pyrus;species arbutifolia;variety nigra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="(Michaux) Willdenow" date="unknown" rank="species">melanocarpa</taxon_name>
    <taxon_hierarchy>genus P.;species melanocarpa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sorbus</taxon_name>
    <taxon_name authority="(Michaux) Hey nhold" date="unknown" rank="species">melanocarpa</taxon_name>
    <taxon_hierarchy>genus Sorbus;species melanocarpa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Black chokeberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous or glabrescent.</text>
      <biological_entity id="o14131" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves pale green abaxially, dark green and shiny adaxially, becoming scarlet;</text>
      <biological_entity id="o14132" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s1" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s1" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="scarlet" value_original="scarlet" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 2.5–7 × 2.5–3.5 cm, apex subacute to acuminate or apiculate, surfaces glabrous or glabrescent, adaxial midrib stipitate-glandular.</text>
      <biological_entity id="o14133" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14134" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s2" to="acuminate or apiculate" />
      </biological_entity>
      <biological_entity id="o14135" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14136" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers sweet-scented;</text>
      <biological_entity id="o14137" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="false" name="odor" src="d0_s3" value="sweet-scented" value_original="sweet-scented" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o14138" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepal margins glabrous;</text>
      <biological_entity constraint="sepal" id="o14139" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers yellow to purplish red.</text>
      <biological_entity id="o14140" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="purplish red" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pomes black, glabrous, taste acid and bitter.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 34, 68.</text>
      <biological_entity id="o14141" name="pome" name_original="pomes" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="black" value_original="black" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="taste" src="d0_s7" value="bitter" value_original="bitter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14142" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="34" value_original="34" />
        <character name="quantity" src="d0_s8" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, bogs, wet thickets, margins of ponds and lakes, beaver ponds, woods, moist high-elevation forests, rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="margins" constraint="of ponds and lakes , beaver ponds , woods , moist high-elevation forests , rock outcrops" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="beaver ponds" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="moist high-elevation forests" />
        <character name="habitat" value="rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>