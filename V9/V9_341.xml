<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="(S. Watson) Rydberg in N. L. Britton et al." date="1908" rank="species">setosa</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 290. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species setosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100278</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">baileyi</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">setosa</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>91. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ivesia;species baileyi;variety setosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">baileyi</taxon_name>
    <taxon_name authority="(S. Watson) J. T. Howell" date="unknown" rank="variety">setosa</taxon_name>
    <taxon_hierarchy>genus Potentilla;species baileyi;variety setosa</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Bristly ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green, tufted to ± densely matted.</text>
      <biological_entity id="o18639" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="more or less densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± ascending to nearly erect, 0.7–2.5 (–2.8) dm.</text>
      <biological_entity id="o18640" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="less ascending" name="orientation" src="d0_s1" to="nearly erect" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2.8" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves weakly planar to loosely cylindric, 5–9 (–12) cm;</text>
      <biological_entity constraint="basal" id="o18641" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="weakly planar" name="shape" src="d0_s2" to="loosely cylindric" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheathing base ± strigose abaxially;</text>
      <biological_entity id="o18642" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="more or less; abaxially" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–8 cm;</text>
      <biological_entity id="o18643" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral leaflets 5–10 per side, separate to overlapping distally, ± flabellate, 2–8 mm, incised ± 3/4 to base, sometimes nearly to base, into (3–) 7–11 ovate teeth to narrowly obovate lobes, apex usually ± setose, surfaces ± sparsely hirsute, conspicuously glandular;</text>
      <biological_entity constraint="lateral" id="o18644" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o18645" from="5" name="quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="separate" modifier="distally" name="arrangement" notes="" src="d0_s5" to="overlapping" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="flabellate" value_original="flabellate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character constraint="to base" constraintid="o18646" name="quantity" src="d0_s5" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o18645" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o18646" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18647" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18648" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s5" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s5" to="11" />
        <character is_modifier="true" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o18649" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o18650" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s5" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o18651" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less sparsely" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o18644" id="r1210" modifier="sometimes nearly; nearly" name="to" negation="false" src="d0_s5" to="o18647" />
      <relation from="o18644" id="r1211" name="into" negation="false" src="d0_s5" to="o18648" />
      <relation from="o18648" id="r1212" name="to" negation="false" src="d0_s5" to="o18649" />
    </statement>
    <statement id="d0_s6">
      <text>terminal leaflets ± indistinct.</text>
      <biological_entity constraint="terminal" id="o18652" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s6" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (0–) 1;</text>
      <biological_entity constraint="cauline" id="o18653" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade vestigial.</text>
      <biological_entity id="o18654" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="vestigial" value_original="vestigial" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (1–) 5–15 (–30) -flowered, ± open, 1–6 (–12) cm diam.</text>
      <biological_entity id="o18655" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(1-)5-15(-30)-flowered" value_original="(1-)5-15(-30)-flowered" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s9" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s9" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 5–15 (–20) mm.</text>
      <biological_entity id="o18656" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 7–10 mm diam.;</text>
      <biological_entity id="o18657" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets 5, lanceolate, 1.3–2.5 (–3) mm;</text>
      <biological_entity constraint="epicalyx" id="o18658" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium patelliform, (1–) 1.5–2 × 2–3.5 (–4) mm;</text>
      <biological_entity id="o18659" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="patelliform" value_original="patelliform" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s13" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals (1.5–) 2–3.5 mm, ± acute;</text>
      <biological_entity id="o18660" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals yellow, oblanceolate to narrowly spatulate, 1.5–2.5 mm;</text>
      <biological_entity id="o18661" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="narrowly spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5, filaments 0.8–1.5 mm, anthers yellow, oblong, 0.4–0.7 mm;</text>
      <biological_entity id="o18662" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18663" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18664" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels 2–8, styles 1.2–2 mm.</text>
      <biological_entity id="o18665" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="8" />
      </biological_entity>
      <biological_entity id="o18666" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes greenish white to light tan, 1.7–2 mm, smooth, ± carunculate.</text>
      <biological_entity id="o18667" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="greenish white" name="coloration" src="d0_s18" to="light tan" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s18" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s18" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky talus slopes, boulders and outcrops, most often but not always of calcareous origin, occasionally away from immediate outcrops, sagebrush communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky talus slopes" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="calcareous origin" modifier="most often but not always of" />
        <character name="habitat" value="immediate outcrops" modifier="occasionally away from" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2600(–3100) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3100" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ivesia setosa occurs mostly to the east and south of I. baileyi, from southeastern Humboldt and Churchill counties to Elko, White Pine, and northern Nye counties, Nevada, and in the Deep Creek Range of western Utah. It represents a transition between the planar-leaved, chasmophytic members of sect. Setosae and the cylindric-leaved, matted species of flatter sites. Stems of I. setosa are more generally erect than in other species in the section, and the usually calcareous substrate is noteworthy. The deeply incised (usually not quite to base) leaflets are somewhat intermediate between the toothed leaflets of I. baileyi and the leaflets of I. shockleyi that are incised to the base into separate lobes. The individual leaflets of both I. baileyi and I. setosa are more or less flat and distichously paired; in I. shockleyi, groups of leaflet lobes are folded over onto each other, giving a verticillate appearance to the leaflet arrangement.</discussion>
  
</bio:treatment>