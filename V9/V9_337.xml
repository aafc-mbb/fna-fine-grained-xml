<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="(Eastwood ex J. T. Howell) Ertter" date="1989" rank="species">arizonica</taxon_name>
    <taxon_name authority="(Brandegee) Ertter" date="1989" rank="variety">saxosa</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>14: 233. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species arizonica;variety saxosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100668</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Purpusia</taxon_name>
    <taxon_name authority="Brandegee" date="unknown" rank="species">saxosa</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>27: 447. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Purpusia;species saxosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(A. Nelson) J. T. Howell" date="unknown" rank="species">osterhoutii</taxon_name>
    <taxon_name authority="(Brandegee) J. T. Howell" date="unknown" rank="variety">saxosa</taxon_name>
    <taxon_hierarchy>genus Potentilla;species osterhoutii;variety saxosa</taxon_hierarchy>
  </taxon_identification>
  <number>5b.</number>
  <other_name type="common_name">Rock purpusia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Inflorescences (1–) 5–30 (–150) -flowered, (0.5–) 2–14 cm diam.</text>
      <biological_entity id="o3915" name="inflorescence" name_original="inflorescences" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="(1-)5-30(-150)-flowered" value_original="(1-)5-30(-150)-flowered" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="diameter" src="d0_s0" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s0" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Flowers: hypanthium turbinate, ± 2 times as deep as wide;</text>
      <biological_entity id="o3916" name="flower" name_original="flowers" src="d0_s1" type="structure" />
      <biological_entity id="o3917" name="hypanthium" name_original="hypanthium" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="width" src="d0_s1" value="2 times as deep as wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petals white;</text>
      <biological_entity id="o3918" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o3919" name="petal" name_original="petals" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>anthers 1–1.5 mm.</text>
      <biological_entity id="o3920" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o3921" name="anther" name_original="anthers" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, rocky outcrops of mainly volcanic origin, usually in crevices of more or less vertical protected cliffs or boulders, in sagebrush communities, pinyon-juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky outcrops" modifier="dry" constraint="of mainly volcanic origin , usually" />
        <character name="habitat" value="volcanic origin" modifier="mainly" />
        <character name="habitat" value="crevices" modifier="usually" constraint="of more or less vertical protected cliffs or boulders , in sagebrush communities , pinyon-juniper woodlands" />
        <character name="habitat" value="vertical protected cliffs" modifier="less" />
        <character name="habitat" value="boulders" constraint="in sagebrush communities , pinyon-juniper woodlands" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="pinyon-juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety saxosa is known from scattered locations in the North and South Pahroc ranges, Lincoln County, and on Pahute Mesa, Nye County, Nevada. Reports of this variety in the Sheep Range of Clark County, Nevada (N. H. Holmgren 1997b; T. L. Ackerman et al. 2003), are probably based on var. arizonica.</discussion>
  
</bio:treatment>