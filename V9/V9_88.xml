<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="(R. Brown) Seringe in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="species">rossii</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>2: 553. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species rossii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100227</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sieversia</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="species">rossii</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>18, plate C. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sieversia;species rossii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Acomastylis</taxon_name>
    <taxon_name authority="(R. Brown) Greene" date="unknown" rank="species">rossii</taxon_name>
    <taxon_hierarchy>genus Acomastylis;species rossii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Geum</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">rossii</taxon_name>
    <taxon_name authority="(Greene) C. L. Hitchcock" date="unknown" rank="variety">depressum</taxon_name>
    <taxon_hierarchy>genus Geum;species rossii;variety depressum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="(Piper) Greene" date="unknown" rank="species">rossii</taxon_name>
    <taxon_name authority="(Rydberg) C. L. Hitchcock" date="unknown" rank="variety">turbinatum</taxon_name>
    <taxon_hierarchy>genus G.;species rossii;variety turbinatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">turbinatum</taxon_name>
    <taxon_hierarchy>genus G.;species turbinatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracilipes</taxon_name>
    <taxon_hierarchy>genus S.;species gracilipes</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Ross’s avens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subscapose.</text>
      <biological_entity id="o21271" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 4–28 cm, glabrous or downy, hairs to 1 mm, sometimes septate-glandular.</text>
      <biological_entity id="o21272" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="28" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
      </biological_entity>
      <biological_entity id="o21273" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s1" value="septate-glandular" value_original="septate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 3–13 cm, blade pinnate to interruptedly pinnate, major leaflets 13–26, alternating with 0–14 minor ones, terminal leaflet slightly larger than major laterals;</text>
      <biological_entity id="o21274" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o21275" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21276" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="pinnate" name="architecture_or_shape" src="d0_s2" to="interruptedly pinnate" />
      </biological_entity>
      <biological_entity id="o21277" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s2" to="26" />
        <character constraint="with minor" constraintid="o21278" is_modifier="false" name="arrangement" src="d0_s2" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o21278" name="minor" name_original="minor" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s2" to="14" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o21279" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than major laterals" constraintid="o21280" is_modifier="false" name="size" src="d0_s2" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity id="o21280" name="lateral" name_original="laterals" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 0.7–2 cm, stipules adnate to leaf, indistinguishable from pair of lobes, blade bractlike, not resembling basal, alternate, simple, pinnatifid to 3-fid.</text>
      <biological_entity id="o21281" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o21282" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o21283" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character constraint="to leaf" constraintid="o21284" is_modifier="false" name="fusion" src="d0_s3" value="adnate" value_original="adnate" />
        <character constraint="from pair" constraintid="o21285" is_modifier="false" name="prominence" notes="" src="d0_s3" value="indistinguishable" value_original="indistinguishable" />
      </biological_entity>
      <biological_entity id="o21284" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o21285" name="pair" name_original="pair" src="d0_s3" type="structure" />
      <biological_entity id="o21286" name="lobe" name_original="lobes" src="d0_s3" type="structure" />
      <biological_entity id="o21287" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s3" to="3-fid" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21288" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o21285" id="r1382" name="part_of" negation="false" src="d0_s3" to="o21286" />
      <relation from="o21287" id="r1383" name="resembling" negation="true" src="d0_s3" to="o21288" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–3 (–4) -flowered.</text>
      <biological_entity id="o21289" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-3(-4)-flowered" value_original="1-3(-4)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels woolly, sometimes glandular.</text>
      <biological_entity id="o21290" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o21291" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 1.5–7 mm;</text>
      <biological_entity constraint="epicalyx" id="o21292" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green, slightly purple-tinged to strongly purple;</text>
      <biological_entity id="o21293" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="slightly purple-tinged" name="coloration" src="d0_s8" to="strongly purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect to erect-spreading, 3–10 mm;</text>
      <biological_entity id="o21294" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s9" to="erect-spreading" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, yellow, obovate to nearly orbiculate, 5–12 (–17) mm, longer than sepals, apex broadly rounded to irregularly emarginate.</text>
      <biological_entity id="o21295" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="nearly orbiculate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="17" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character constraint="than sepals" constraintid="o21296" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o21296" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile, glabrous.</text>
      <biological_entity id="o21298" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <relation from="o21297" id="r1384" name="fruiting" negation="false" src="d0_s11" to="o21298" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles wholly persistent, not geniculate-jointed, 2–5 (–10) mm, apex not hooked, glabrous throughout or pilose only at base.</text>
      <biological_entity id="o21297" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s10" to="irregularly emarginate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21299" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o21300" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="wholly" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21302" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o21297" id="r1385" name="fruiting" negation="false" src="d0_s12" to="o21299" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 56.</text>
      <biological_entity id="o21301" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o21302" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21303" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine and arctic tundra, rocky slopes, often in gravelly or peaty soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine" />
        <character name="habitat" value="arctic tundra" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="peaty soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T., Nunavut, Yukon; Alaska, Ariz., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The variability accommodated here in Geum rossii was distributed by earlier monographers such as P. A. Rydberg (1913b) and F. Bolle (1933) among a half dozen species. W. Gajewski (1957) reduced them to two species, G. rossii and G. turbinatum; most recent taxonomists have recognized the two taxa as subspecies or varieties of a single species. The large geographic discontinuity between the Rocky Mountain and arctic ranges makes it easy for those wishing to follow this tradition. No one morphologic character or combination of characters neatly separates the arctic plants from those of the Rockies.</discussion>
  <discussion>Where their ranges overlap in Alaska, Geum rossii hybridizes with G. calthifolium to form sterile plants known as G. ×macranthum (Kearney ex Rydberg) B. Boivin; see discussion under 4. G. schofieldii.</discussion>
  
</bio:treatment>