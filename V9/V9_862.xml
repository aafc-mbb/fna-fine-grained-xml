<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="mention_page">511</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">599</other_info_on_meta>
    <other_info_on_meta type="illustration_page">510</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="unknown" rank="section">Douglasia</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Douglasianae</taxon_name>
    <taxon_name authority="Lindley" date="1835" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Edwards's Bot. Reg.</publication_title>
      <place_in_publication>21: plate 1810. 1835</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section douglasia;series douglasianae;species douglasii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100087</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">columbiana</taxon_name>
    <taxon_hierarchy>genus Crataegus;species columbiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">tennowana</taxon_name>
    <taxon_hierarchy>genus C.;species tennowana</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Douglas or black hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 40–80 dm.</text>
      <biological_entity id="o12665" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: 1-year old twigs deep tan to dark mahogany, ± shiny young, older gray to dark gray, often showing brown or alternatively tan-brown eastward;</text>
      <biological_entity id="o12666" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o12667" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="deep" value_original="deep" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s1" to="dark mahogany" />
        <character is_modifier="false" modifier="more or less" name="reflectance" src="d0_s1" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="older" value_original="older" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s1" to="dark gray" />
      </biological_entity>
      <biological_entity id="o12668" name="eastward" name_original="eastward" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="often" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="true" modifier="alternatively" name="coloration" src="d0_s1" value="tan-brown" value_original="tan-brown" />
      </biological_entity>
      <relation from="o12667" id="r794" modifier="often" name="showing" negation="false" src="d0_s1" to="o12668" />
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs straight or slightly recurved, deep brown young, (1.5–) 2–3.5 cm.</text>
      <biological_entity id="o12669" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o12670" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="depth" src="d0_s2" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12671" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o12670" id="r795" name="on" negation="false" src="d0_s2" to="o12671" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 0.7–1.5 cm, pubescent young, slightly glandular;</text>
      <biological_entity id="o12672" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12673" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="young" value_original="young" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually elliptic to broadly elliptic or subrhombic (elliptic-obovate when lobes very small), 4–7 cm, lobes 2–4 per side, sinuses shallow, LII 10–20%, lobe apex subobtuse to acute, margins coarsely, sharply serrate, teeth small, gland-tipped young, venation craspedodromous, veins 4 or 5 per side, apex acute to obtuse, abaxial surface usually glabrous except on veins, adaxial densely short-appressed-pubescent.</text>
      <biological_entity id="o12674" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12675" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subrhombic" value_original="subrhombic" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12676" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o12677" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o12677" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o12678" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o12679" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="subobtuse" modifier="10 20%" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o12680" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o12681" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="small" value_original="small" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity id="o12682" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="4" value_original="4" />
        <character name="quantity" src="d0_s4" unit="or per" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o12683" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o12684" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12685" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="except " constraintid="o12686" is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12686" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o12687" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="short-appressed-pubescent" value_original="short-appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 10–25-flowered;</text>
      <biological_entity id="o12688" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="10-25-flowered" value_original="10-25-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous;</text>
      <biological_entity id="o12689" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteole margins stipitate or sessile-glandular.</text>
      <biological_entity constraint="bracteole" id="o12690" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 10–15 mm diam.;</text>
      <biological_entity id="o12691" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o12692" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals broadly triangular, 3–4 mm, margins sparsely glandular, adaxially glabrous;</text>
      <biological_entity id="o12693" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12694" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 10, anthers pink;</text>
      <biological_entity id="o12695" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o12696" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3 or 4.</text>
      <biological_entity id="o12697" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s12" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes vinous and usually waxy glaucous young, dull black mature, usually ellipsoid, 6–8 mm diam., glabrous;</text>
      <biological_entity id="o12698" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s13" value="ceraceous" value_original="waxy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="life_cycle" src="d0_s13" value="young" value_original="young" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepal remnants reflexed, apex obtuse;</text>
      <biological_entity constraint="sepal" id="o12699" name="remnant" name_original="remnants" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o12700" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 3 or 4, sides excavated, sometimes only shallowly.</text>
      <biological_entity id="o12701" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s15" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 68.</text>
      <biological_entity id="o12702" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="excavated" value_original="excavated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12703" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brush, old fields, fencerows, woodland edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brush" />
        <character name="habitat" value="old fields" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="woodland edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont., Sask.; Alaska, Calif., Idaho, Mich., Minn., Mont., Oreg., Wash., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus douglasii occurs from southern and western British Columbia to the panhandle of Alaska and ranges to northern California and the Rocky Mountains of Idaho, Montana, and southwestern Alberta. Disjunct populations occur farther east in the Cypress Hills (Alberta and Saskatchewan), the Montana montane islands, and around the northern Great Lakes. In western North America, C. douglasii is found mainly near water in drier areas; in more mesic regions, it may occur in open woodlands. In the Great Lakes area, it mainly occurs in old pastures and on fencerows.</discussion>
  <discussion>Crataegus douglasii is distinguished from C. gaylussacia by having ten stamens and is rather variable in leaf shape and size. The species is distinguished from C. okennonii by the latter usually being taller and typically more erect as well as having a usually straight, single trunk, shorter thorns, ampulliform-orbicular fruit, and often purplish crimson fall foliage. Crataegus castlegarensis is the most similar species, and beyond possessing relatively few to many multiple thorns, it is distinguished from C. douglasii by its hairy hypanthia, pedicels, and, often, fruit, which is orbicular, plump, crimson to purple in late August, ripening to deep purple (sometimes black) at a time when sympatric C. douglasii is fully black.</discussion>
  
</bio:treatment>