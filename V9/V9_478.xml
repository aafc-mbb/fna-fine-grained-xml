<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="illustration_page">285</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="Rydberg" date="1898" rank="species">cuneifolia</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. N. Amer. Potentilleae,</publication_title>
      <place_in_publication>204, plate 111 [as cuneata]. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species cuneifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100211</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Munz" date="unknown" rank="species">peirsonii</taxon_name>
    <taxon_hierarchy>genus Potentilla;species peirsonii</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches elongate.</text>
      <biological_entity constraint="caudex" id="o18459" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems openly tufted to loosely spaced, 0.5–4.5 dm;</text>
      <biological_entity id="o18460" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="openly tufted" name="arrangement" src="d0_s1" to="loosely spaced" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="4.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>base 1–2 mm diam., sparsely septate-glandular.</text>
      <biological_entity id="o18461" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s2" value="septate-glandular" value_original="septate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sparsely to moderately hairy;</text>
      <biological_entity id="o18462" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal 2–15 cm, leaflet pairs (2–) 3–5;</text>
      <biological_entity constraint="basal" id="o18463" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18464" name="leaflet" name_original="leaflet" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>terminal leaflet broadly cuneate to nearly round, 0.6–2 (–3.5) × 0.5–2 cm, teeth irregularly single, 2–4 per side, apex rounded to truncate;</text>
      <biological_entity constraint="terminal" id="o18465" name="leaflet" name_original="leaflet" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly cuneate" name="shape" src="d0_s5" to="nearly round" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18466" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character char_type="range_value" constraint="per side" constraintid="o18467" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o18467" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline 0–1 (–2), reduced, leaflet pairs 1–2.</text>
      <biological_entity id="o18468" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="truncate" />
        <character is_modifier="false" name="position" src="d0_s6" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o18469" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="1" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o18470" name="leaflet" name_original="leaflet" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 3–10 (–15) -flowered, not leafy, open, 1/4–3/4 of stem, wide, branch angles 20–75°.</text>
      <biological_entity id="o18471" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-10(-15)-flowered" value_original="3-10(-15)-flowered" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character char_type="range_value" constraint="of stem" constraintid="o18472" from="1/4" name="quantity" src="d0_s7" to="3/4" />
        <character is_modifier="false" name="width" notes="" src="d0_s7" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity id="o18472" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <biological_entity constraint="branch" id="o18473" name="angle" name_original="angles" src="d0_s7" type="structure">
        <character name="degree" src="d0_s7" value="20-75°" value_original="20-75°" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–15 (proximal to 30) mm, sparsely to moderately short-hairy, septate-glandular.</text>
      <biological_entity id="o18474" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s8" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="septate-glandular" value_original="septate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers opening narrowly;</text>
      <biological_entity id="o18475" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>epicalyx bractlets linear to narrowly elliptic or lanceolate, 1–2 × 0.5 mm;</text>
      <biological_entity constraint="epicalyx" id="o18476" name="bractlet" name_original="bractlets" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals erect, 2–5 mm, apex broadly obtuse;</text>
      <biological_entity id="o18477" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18478" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals scarcely overlapping, erect, yellow, narrowly obovate, 2–4 × 1.5–2.5 mm, ± equal to or slightly longer than sepals;</text>
      <biological_entity id="o18479" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s12" value="equal" value_original="equal" />
        <character constraint="than sepals" constraintid="o18480" is_modifier="false" name="length_or_size" src="d0_s12" value="more or less equal to or slightly longer" />
      </biological_entity>
      <biological_entity id="o18480" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.5–2.5 mm, anthers 0.5–0.8 mm;</text>
      <biological_entity id="o18481" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18482" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles slender, 1.5–2.5 mm.</text>
      <biological_entity id="o18483" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="slender" value_original="slender" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, 0.8–1.2 mm.</text>
      <biological_entity id="o18484" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Drymocallis cuneifolia has a complex nomenclatural history, complicated by the extreme rarity of the typical variety. Rydberg described the species on the basis of a single collection from the San Bernardino Mountains of San Bernardino County, probably near Green Lead Mines. When comparable (though significantly smaller) plants were found in the San Gabriel Mountains of Los Angeles County, P. A. Munz and I. M. Johnston (1925) adopted Potentilla cuneifolia (Rydberg) Th. Wolf for both extremes; this name was later replaced with P. peirsonii because of the earlier P. cuneifolia Bertoloni. In the absence of comparable new collections from the San Bernardino Mountains, D. D. Keck (in J. Clausen et al. 1940) concluded that the type of D. cuneifolia was merely an immature specimen of D. lactea var. lactea and accordingly described P. glandulosa subsp. ewanii to accommodate populations in the San Gabriel Mountains.</discussion>
  <discussion>The 2004 discovery of a small population of plants comparable to the type of Drymocallis cuneifolia from near the type locality (Elvin 3555, IRVC, UCR) confirms that D. cuneifolia is a valid taxon that shares most of the diagnostic features of populations in the San Gabriel Mountains. Plants in the San Bernardino Mountains tend to be larger than those in the San Gabriel Mountains, with fewer and somewhat differently shaped leaflets; they are accordingly treated here as varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 2–4.5 dm; basal leaves 5–15 cm; San Bernardino Mountains.</description>
      <determination>15a Drymocallis cuneifolia var. cuneifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 0.5–2(–2.5) dm; basal leaves 2–10 cm; San Gabriel Mountains.</description>
      <determination>15b Drymocallis cuneifolia var. ewanii</determination>
    </key_statement>
  </key>
</bio:treatment>