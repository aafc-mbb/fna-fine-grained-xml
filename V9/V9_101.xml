<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 500. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species virginianum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416595</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Geum</taxon_name>
    <taxon_name authority="(Porter) E. P. Bicknell" date="unknown" rank="species">flavum</taxon_name>
    <taxon_hierarchy>genus Geum;species flavum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">G.</taxon_name>
    <taxon_name authority="Muhlenberg ex Link" date="unknown" rank="species">hirsutum</taxon_name>
    <taxon_hierarchy>genus G.;species hirsutum</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Cream or Virginia or pale avens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants leafy-stemmed.</text>
      <biological_entity id="o33712" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="leafy-stemmed" value_original="leafy-stemmed" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 25–110 cm, puberulent and hirsute to densely hirsute, some hairs 2–2.5 mm.</text>
      <biological_entity id="o33713" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="110" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="hirsute" name="pubescence" src="d0_s1" to="densely hirsute" />
      </biological_entity>
      <biological_entity id="o33714" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 12–25 cm, blade simple or pinnate, major leaflets 3–5, plus 0–4 minor basal ones, terminal leaflet slightly larger than major laterals;</text>
      <biological_entity id="o33715" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o33716" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33717" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o33718" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity constraint="minor basal" id="o33719" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="terminal" id="o33720" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than major laterals" constraintid="o33721" is_modifier="false" name="size" src="d0_s2" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity id="o33721" name="lateral" name_original="laterals" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 4–15 (–23) cm, stipules ± free, 11–48 × 6–35 mm, blade 3-foliolate or simple and 3-lobed to unlobed.</text>
      <biological_entity id="o33722" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o33723" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="23" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33724" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s3" to="48" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33725" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="3-lobed" name="shape" src="d0_s3" to="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–14-flowered.</text>
      <biological_entity id="o33726" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-14-flowered" value_original="3-14-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels puberulent, sometimes with scattered hairs, eglandular.</text>
      <biological_entity id="o33727" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o33728" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o33727" id="r2218" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o33728" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o33729" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 1–1.5 mm;</text>
      <biological_entity constraint="epicalyx" id="o33730" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green;</text>
      <biological_entity id="o33731" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals spreading but soon reflexed, 3–6 mm;</text>
      <biological_entity id="o33732" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="soon" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, cream, oblong to elliptic, (1.5–) 2–3.5 mm, shorter than sepals, apex rounded.</text>
      <biological_entity id="o33733" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="cream" value_original="cream" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o33734" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o33734" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile, densely bristly, hairs 1–2.3 mm.</text>
      <biological_entity id="o33735" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o33736" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <biological_entity id="o33737" name="whole-organism" name_original="" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="bristly" value_original="bristly" />
      </biological_entity>
      <relation from="o33735" id="r2219" name="fruiting" negation="false" src="d0_s11" to="o33736" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles geniculate-jointed, proximal segment persistent, (3–) 4.5–7 mm, apex hooked, glabrate, distal segment deciduous, 1–2 mm, pilose in basal 1/2, hairs much longer than diam. of style.</text>
      <biological_entity id="o33738" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33739" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o33740" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o33741" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33742" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o33743" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character constraint="in basal 1/2" constraintid="o33744" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o33744" name="1/2" name_original="1/2" src="d0_s12" type="structure" />
      <biological_entity id="o33745" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character constraint="than diam of style" constraintid="o33746" is_modifier="false" name="length_or_size" src="d0_s12" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o33746" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="diam" value_original="diam" />
      </biological_entity>
      <relation from="o33738" id="r2220" name="fruiting" negation="false" src="d0_s12" to="o33739" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly forests, river bottoms to dry uplands, rocky slopes, oak-hickory woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" modifier="mostly" />
        <character name="habitat" value="river bottoms" />
        <character name="habitat" value="dry uplands" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="oak-hickory woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Ky., Md., Mass., Mich., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Geum virginianum has been confused nomenclaturally with G. laciniatum. B. L. Robinson and M. L. Fernald (1908) and J. K. Small (1933) misapplied the name G. virginianum to the species correctly named G. laciniatum, and they used the names G. flavum (Robinson and Fernald) and G. hirsutum (Small) for what is correctly named G. virginianum. Much of what is named G. virginianum in older herbarium collections is actually G. laciniatum.</discussion>
  <discussion>In habit and leaf form, Geum virginianum is similar to G. canadense. Geum virginianum differs in having cream petals shorter than the sepals (versus white and usually equal to or longer), the largest stipules of the cauline leaves 20–48 × 10–35 mm (versus smaller), and the stems hirsute to densely hirsute (versus glabrate to downy with only scattered longer hairs). The variability within G. canadense is so great that for some specimens it is difficult to determine whether they belong to G. canadense or G. virginianum.</discussion>
  <discussion>L. A. Raynor (1952) believed Geum virginianum to be an F1 hybrid between G. aleppicum and G. canadense. K. R. Robertson (1974) acknowledged that some herbarium specimens assignable to G. virginianum from the area where G. aleppicum and G. canadense are sympatric have mostly aborted pollen and may represent natural hybrids. He pointed out that farther south, where G. aleppicum is absent, specimens of G. virginianum appear fully fertile and seem to represent a valid species.</discussion>
  
</bio:treatment>