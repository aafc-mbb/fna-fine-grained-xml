<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="illustration_page">52</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="(Linnaeus) Kuntze" date="1891" rank="species">repens</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>1: 223. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species repens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100445</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dalibarda</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">repens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 491. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dalibarda;species repens</taxon_hierarchy>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Robin runaway</other_name>
  <other_name type="common_name">false violet</other_name>
  <other_name type="common_name">dalibarde rampante</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, 0.5–1 dm, unarmed.</text>
      <biological_entity id="o15068" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="1" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, moderately appressed-hairy, eglandular or sparsely to moderately stipitate-glandular, not pruinose.</text>
      <biological_entity id="o15069" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s1" value="appressed-hairy" value_original="appressed-hairy" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, simple;</text>
      <biological_entity id="o15070" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate-laciniate, (2–) 3–5 (–8) mm;</text>
      <biological_entity id="o15071" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate-laciniate" value_original="lanceolate-laciniate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade ovate-orbiculate, 1.5–2.7 (–3) × 1.5–3.5 (–4) cm, base deeply cordate, unlobed, margins crenate to broadly dentate, apex rounded, abaxial surfaces sparsely to moderately hairy, sparsely to moderately short-stipitate-glandular.</text>
      <biological_entity id="o15072" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate-orbiculate" value_original="ovate-orbiculate" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15073" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o15074" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenate" name="shape" src="d0_s4" to="broadly dentate" />
      </biological_entity>
      <biological_entity id="o15075" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15076" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="hairy" modifier="moderately" name="pubescence" src="d0_s4" to="sparsely moderately short-stipitate-glandular" />
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s4" to="sparsely moderately short-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1-flowered.</text>
      <biological_entity id="o15077" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels moderately and retrorsely long-hairy, eglandular.</text>
      <biological_entity id="o15078" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence" src="d0_s6" value="long-hairy" value_original="long-hairy" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o15079" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals absent in fertile flowers, present in sterile flowers, white, narrowly elliptic to oblanceolate, 5–8 mm;</text>
      <biological_entity id="o15080" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character constraint="in flowers" constraintid="o15081" is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15081" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character constraint="in flowers" constraintid="o15082" is_modifier="false" name="presence" notes="" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15082" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments filiform;</text>
      <biological_entity id="o15083" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovaries densely hairy, styles glabrous.</text>
      <biological_entity id="o15084" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15085" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits whitish, 0.3–0.5 cm, dry;</text>
      <biological_entity id="o15086" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s11" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s11" value="dry" value_original="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>drupelets 5–10 (–15), not coherent, separating from torus, enclosed by converging sepals.</text>
      <biological_entity id="o15088" name="torus" name_original="torus" src="d0_s12" type="structure" />
      <biological_entity id="o15089" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="converging" value_original="converging" />
      </biological_entity>
      <relation from="o15087" id="r969" name="enclosed by" negation="false" src="d0_s12" to="o15089" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o15087" name="drupelet" name_original="drupelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="10" />
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s12" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o15088" is_modifier="false" name="arrangement" src="d0_s12" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15090" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist woods, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Conn., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus repens is recognized by its creeping, unarmed stems, simple, ovate-orbiculate leaves, long petioles with spreading hairs, sterile petaliferous flowers on long pedicels and fertile apetalous flowers on short pedicels, and essentially dry fruits. Phylogenetic analysis of DNA sequence data supports inclusion of D. repens in Rubus (L. A. Alice and C. S. Campbell 1999), among other basal species to R. lasiococcus (K. V. Ambrose 2006).</discussion>
  <discussion>The Iroquois use a decoction of powdered plants of Rubus repens as a blood purifier and for venereal disease (J. W. Herrick 1977).</discussion>
  
</bio:treatment>