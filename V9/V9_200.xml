<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">139</other_info_on_meta>
    <other_info_on_meta type="illustration_page">129</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="Poeverlein in P. F. A. Ascherson et al." date="1904" rank="section">Rivales</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">supina</taxon_name>
    <taxon_name authority="(Nuttall) Soják" date="1969" rank="subspecies">paradoxa</taxon_name>
    <place_of_publication>
      <publication_title>Folia Geobot. Phytotax.</publication_title>
      <place_in_publication>4: 207. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section rivales;species supina;subspecies paradoxa;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242341175</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">paradoxa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 437. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species paradoxa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="(S. Watson) E. Sheldon" date="unknown" rank="species">nicolletii</taxon_name>
    <taxon_hierarchy>genus P.;species nicolletii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">supina</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">nicolletii</taxon_name>
    <taxon_hierarchy>genus P.;species supina;variety nicolletii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">supina</taxon_name>
    <taxon_name authority="(Nuttall) Th. Wolf" date="unknown" rank="variety">paradoxa</taxon_name>
    <taxon_hierarchy>genus P.;species supina;variety paradoxa</taxon_hierarchy>
  </taxon_identification>
  <number>10a.</number>
  <other_name type="common_name">Bushy cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems prostrate to ascending, sometimes erect, (1–) 2–4 (–6) dm, hairs at base not particularly stiff, not tubercle-based, glands absent or sparse, inconspicuous.</text>
      <biological_entity id="o19227" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s0" to="ascending" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o19228" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s0" value="tubercle-based" value_original="tubercle-based" />
      </biological_entity>
      <biological_entity id="o19229" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not particularly" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o19230" name="gland" name_original="glands" src="d0_s0" type="structure">
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s0" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o19228" id="r1246" name="at" negation="false" src="d0_s0" to="o19229" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves pinnate (with distal leaflets ± confluent), 5–12 (–15) cm;</text>
      <biological_entity id="o19231" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–6 (–8) cm, long hairs sparse to common, ± ascending, sometimes erect or appressed, 0.5–1 (–1.5) mm, usually ± weak, crisped hairs absent or sparse to common, glands sparse to common, inconspicuous;</text>
      <biological_entity id="o19232" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19233" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="common" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="usually more or less" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o19234" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="common" />
      </biological_entity>
      <biological_entity id="o19235" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="common" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 5–9, on distal (1/4–) 1/2–2/3 of leaf axis, ± separate, largest ones oblanceolate to elliptic or obovate, 1–2 (–4) × 0.6–1.2 (–2.5) cm, distal 2/3 to ± whole margin ± evenly incised 1/4–1/2 to midvein, teeth (2–) 4–6 per side, surfaces glabrate or sparsely hairy, glands mostly absent.</text>
      <biological_entity id="o19236" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="9" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s3" value="separate" value_original="separate" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="elliptic or obovate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s3" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s3" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o19237" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="of leaf axis" constraintid="o19238" from="1/4" name="quantity" src="d0_s3" to="1/2-2/3" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o19238" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o19239" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less; more or less evenly" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o19240" from="1/4" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity id="o19240" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o19241" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s3" to="4" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o19242" from="4" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o19242" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o19243" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19244" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o19236" id="r1247" name="on" negation="false" src="d0_s3" to="o19237" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences (10–) 20–100+-flowered.</text>
      <biological_entity id="o19245" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="(10-)20-100+-flowered" value_original="(10-)20-100+-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0.5–2 (–2.5) cm.</text>
      <biological_entity id="o19246" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: epicalyx bractlets ± elliptic to narrowly ovate, 3–5 (–7) × 1–2 (–3) mm;</text>
      <biological_entity id="o19247" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="epicalyx" id="o19248" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="less elliptic" name="shape" src="d0_s6" to="narrowly ovate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium 3–5 mm diam.;</text>
      <biological_entity id="o19249" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o19250" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 3–5 (–6) mm, apex acute to apiculate;</text>
      <biological_entity id="o19251" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o19252" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19253" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals yellow, obovate, 2.5–5 × 2–4.5 mm;</text>
      <biological_entity id="o19254" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o19255" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 20 (–25), filaments 0.7–1.8 (–2) mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o19256" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o19257" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="25" />
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o19258" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19259" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>carpels ca. 100, styles 0.5–0.6 mm.</text>
      <biological_entity id="o19260" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o19261" name="carpel" name_original="carpels" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="100" value_original="100" />
      </biological_entity>
      <biological_entity id="o19262" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes brown, 0.7–1.3 mm, usually strongly rugose, developing a smooth, corky protuberance on ventral suture, often as large as body of achene.</text>
      <biological_entity id="o19263" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
        <character is_modifier="false" modifier="usually strongly" name="relief" src="d0_s12" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="development" src="d0_s12" value="developing" value_original="developing" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="ventral" id="o19265" name="suture" name_original="suture" src="d0_s12" type="structure" />
      <biological_entity id="o19266" name="body" name_original="body" src="d0_s12" type="structure" />
      <biological_entity id="o19267" name="achene" name_original="achene" src="d0_s12" type="structure" />
      <relation from="o19264" id="r1248" name="on" negation="false" src="d0_s12" to="o19265" />
      <relation from="o19264" id="r1249" modifier="often" name="as large as" negation="false" src="d0_s12" to="o19266" />
      <relation from="o19266" id="r1250" name="part_of" negation="false" src="d0_s12" to="o19267" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 28.</text>
      <biological_entity id="o19264" name="protuberance" name_original="protuberance" src="d0_s12" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s12" value="corky" value_original="corky" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19268" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy shorelines of lakes, reservoirs, and streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy shorelines" constraint="of lakes , reservoirs , and streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="reservoirs" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask.; Colo., Idaho, Ill., Iowa, Kans., La., Mich., Minn., Mo., Mont., Nebr., N.Mex., N.Y., N.Dak., Ohio, Okla., Oreg., Pa., S.Dak., Tex., Utah, Wash., Wyo.; n Mexico; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Although subsp. paradoxa is relatively common and widespread in central North America, occurrences are more sporadic and possibly ephemeral in the far western states. The diagnostic corky protuberance on the achene is well developed only on fully mature achenes, appearing merely as a flap of membranous tissue in earlier stages.</discussion>
  
</bio:treatment>