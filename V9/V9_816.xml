<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul M. Catling,Gisèle Mitrow</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">488</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Lindley" date="1821" rank="genus">CHAENOMELES</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>13: 97. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus CHAENOMELES</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek chaino, open, and melon, apple, alluding to mistakenly presumed splitting of fruit</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">106309</other_info_on_name>
  </taxon_identification>
  <number>60.</number>
  <other_name type="common_name">Flowering quince</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs [or trees], (0.2–) 10–20 dm.</text>
      <biological_entity id="o35045" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems few-to-many, erect or spreading;</text>
      <biological_entity id="o35046" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s1" to="many" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark purplish brown, blackish brown, purplish black, or purple, with scattered pale-brown lenticels;</text>
      <biological_entity id="o35047" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="purplish brown" value_original="purplish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purplish black" value_original="purplish black" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o35048" name="lenticel" name_original="lenticels" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
      <relation from="o35047" id="r2317" name="with" negation="false" src="d0_s2" to="o35048" />
    </statement>
    <statement id="d0_s3">
      <text>long and short-shoots present;</text>
      <biological_entity id="o35049" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="true" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>thorns present;</text>
    </statement>
    <statement id="d0_s5">
      <text>glabrous or hairy young, smooth older;</text>
      <biological_entity id="o35050" name="thorn" name_original="thorns" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>buds triangular-ovoid, apex obtuse or acute, scale margins glabrous or hairy.</text>
      <biological_entity id="o35051" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular-ovoid" value_original="triangular-ovoid" />
      </biological_entity>
      <biological_entity id="o35052" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="scale" id="o35053" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves deciduous or semipersistent, cauline, simple;</text>
      <biological_entity id="o35054" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s7" value="semipersistent" value_original="semipersistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o35055" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules persistent, free, reniform or suborbiculate, rarely ovate, leaflike, margins serrate or crenate-serrate;</text>
      <biological_entity id="o35056" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s8" value="reniform" value_original="reniform" />
        <character is_modifier="false" name="shape" src="d0_s8" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
      </biological_entity>
      <biological_entity id="o35057" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole present;</text>
      <biological_entity id="o35058" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade spatulate, obovate, elliptic, or ovate, 3–9 cm, firm or leathery, margins flat, serrate or crenate-serrate, venation pinnate, surfaces glabrous, sometimes midvein abaxially.</text>
      <biological_entity id="o35059" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="9" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="firm" value_original="firm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o35060" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="crenate-serrate" value_original="crenate-serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o35061" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o35062" name="midvein" name_original="midvein" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences terminal on short branches, appearing lateral on branch as a whole, [2 or] 3–5 [–10] -flowered, fascicles, glabrous or hairy;</text>
      <biological_entity id="o35063" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character constraint="on branches" constraintid="o35064" is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="3-5[-10]-flowered" value_original="3-5[-10]-flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o35064" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o35065" name="branch" name_original="branch" src="d0_s11" type="structure">
        <character is_modifier="true" name="position" src="d0_s11" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o35066" name="whole" name_original="whole" src="d0_s11" type="structure" />
      <relation from="o35063" id="r2318" name="appearing" negation="false" src="d0_s11" to="o35065" />
      <relation from="o35063" id="r2319" name="as" negation="false" src="d0_s11" to="o35066" />
    </statement>
    <statement id="d0_s12">
      <text>bracts present or absent;</text>
      <biological_entity id="o35067" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>bracteoles present or absent.</text>
      <biological_entity id="o35068" name="bracteole" name_original="bracteoles" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels present, short, or absent.</text>
      <biological_entity id="o35069" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Flowers opening before or with leaves, perianth and androecium epigynous, 25–50 mm diam.;</text>
      <biological_entity id="o35070" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o35071" name="leaf" name_original="leaves" src="d0_s15" type="structure" />
      <biological_entity id="o35072" name="perianth" name_original="perianth" src="d0_s15" type="structure" />
      <biological_entity id="o35073" name="androecium" name_original="androecium" src="d0_s15" type="structure">
        <character is_modifier="true" name="position" src="d0_s15" value="epigynous" value_original="epigynous" />
      </biological_entity>
      <relation from="o35070" id="r2320" name="opening before or with" negation="false" src="d0_s15" to="o35071" />
      <relation from="o35070" id="r2321" name="opening before or with" negation="false" src="d0_s15" to="o35072" />
      <relation from="o35070" id="r2322" name="opening before or with" negation="false" src="d0_s15" to="o35073" />
    </statement>
    <statement id="d0_s16">
      <text>hypanthium campanulate, ± constricted at mouth, 4–7 mm diam., exterior glabrous;</text>
      <biological_entity id="o35074" name="hypanthium" name_original="hypanthium" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="campanulate" value_original="campanulate" />
        <character constraint="at mouth" constraintid="o35075" is_modifier="false" modifier="more or less" name="size" src="d0_s16" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" notes="" src="d0_s16" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35075" name="mouth" name_original="mouth" src="d0_s16" type="structure" />
      <biological_entity id="o35076" name="exterior" name_original="exterior" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals 5, reflexed or ascending, suborbiculate or ovate, abaxial surface glabrous, adaxial hairy;</text>
      <biological_entity id="o35077" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s17" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o35078" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o35079" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 5, white, pink, or red, obovate or ovate to suborbiculate, base short-clawed, apex rounded;</text>
      <biological_entity id="o35080" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="red" value_original="red" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s18" to="suborbiculate" />
      </biological_entity>
      <biological_entity id="o35081" name="base" name_original="base" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="short-clawed" value_original="short-clawed" />
      </biological_entity>
      <biological_entity id="o35082" name="apex" name_original="apex" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens 40–60, equal to or 1/2 length petals;</text>
      <biological_entity id="o35083" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s19" to="60" />
        <character constraint="to petals" constraintid="o35084" is_modifier="false" name="variability" src="d0_s19" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o35084" name="petal" name_original="petals" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="1/2" value_original="1/2" />
        <character is_modifier="true" name="character" src="d0_s19" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>carpels 5, connate, adnate to hypanthium, indumentum not recorded, styles 2–5, terminal, basally connate 1/3 of length, nearly equal to stamens;</text>
      <biological_entity id="o35085" name="carpel" name_original="carpels" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character constraint="to hypanthium" constraintid="o35086" is_modifier="false" name="fusion" src="d0_s20" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o35086" name="hypanthium" name_original="hypanthium" src="d0_s20" type="structure" />
      <biological_entity id="o35087" name="indumentum" name_original="indumentum" src="d0_s20" type="structure" />
      <biological_entity id="o35088" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s20" to="5" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s20" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character name="length" src="d0_s20" value="1/3" value_original="1/3" />
        <character constraint="to stamens" constraintid="o35089" is_modifier="false" modifier="nearly" name="variability" src="d0_s20" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o35089" name="stamen" name_original="stamens" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>ovules 2.</text>
      <biological_entity id="o35090" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits pomes, sessile, yellow or yellowish green, globose, subglobose, or ovoid, 23–60 mm diam., 5-locular, glabrous;</text>
    </statement>
    <statement id="d0_s23">
      <text>fleshy;</text>
      <biological_entity constraint="fruits" id="o35091" name="pome" name_original="pomes" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="shape" src="d0_s22" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s22" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="23" from_unit="mm" name="diameter" src="d0_s22" to="60" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s22" value="5-locular" value_original="5-locular" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="texture" src="d0_s23" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>hypanthium persistent;</text>
      <biological_entity id="o35092" name="hypanthium" name_original="hypanthium" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>sepals deciduous;</text>
      <biological_entity id="o35093" name="sepal" name_original="sepals" src="d0_s25" type="structure">
        <character is_modifier="false" name="duration" src="d0_s25" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>carpels cartilaginous;</text>
      <biological_entity id="o35094" name="carpel" name_original="carpels" src="d0_s26" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s26" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>styles deciduous.</text>
      <biological_entity id="o35095" name="style" name_original="styles" src="d0_s27" type="structure">
        <character is_modifier="false" name="duration" src="d0_s27" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>Seeds 10 per locule.</text>
      <biological_entity id="o35097" name="locule" name_original="locule" src="d0_s28" type="structure" />
    </statement>
    <statement id="d0_s29">
      <text>x = 17.</text>
      <biological_entity id="o35096" name="seed" name_original="seeds" src="d0_s28" type="structure">
        <character constraint="per locule" constraintid="o35097" name="quantity" src="d0_s28" value="10" value_original="10" />
      </biological_entity>
      <biological_entity constraint="x" id="o35098" name="chromosome" name_original="" src="d0_s29" type="structure">
        <character name="quantity" src="d0_s29" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia (China, Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Japan)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Choenomeles</other_name>
  <other_name type="past_name">name and orthography conserved</other_name>
  <discussion>Species 4 (2 in the flora).</discussion>
  <discussion>The flowering quinces are widely cultivated as ornamental shrubs for their attractive and abundant pink, red, or white flowers. Other species differ from those in the flora area in their entire leaf margins and tomentose leaves.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branches smooth (not verrucose with age); leaf margins serrate; pomes 40–60 mm diam.</description>
      <determination>1 Chaenomeles speciosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branches scabrous (verrucose with age); leaf margins crenate-serrate; pomes 23–40 mm diam.</description>
      <determination>2 Chaenomeles japonica</determination>
    </key_statement>
  </key>
</bio:treatment>