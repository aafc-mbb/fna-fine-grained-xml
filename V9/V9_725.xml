<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Corrêa ex Bonpland in A. von Humboldt and A. J. Bonpland" date="unknown" rank="genus">vauquelinia</taxon_name>
    <taxon_name authority="(Torrey) Sargent" date="1889" rank="species">californica</taxon_name>
    <taxon_name authority="W. J. Hess &amp; Henrickson" date="1987" rank="subspecies">sonorensis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>12: 130, figs. 11a–c. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus vauquelinia;species californica;subspecies sonorensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100526</other_info_on_name>
  </taxon_identification>
  <number>1b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Young stems densely white-tomentulose, becoming canescent.</text>
      <biological_entity id="o2956" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" value_original="young" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="white-tomentulose" value_original="white-tomentulose" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s0" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (4–) 6–16 (–22) mm;</text>
      <biological_entity id="o2957" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2958" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="22" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade bicolor, abaxially white, adaxially green and nonlustrous, linear to linear-lanceolate, (2.5–) 5–11 (–15) × (0.6–) 0.7–1.2 (–1.4) cm, surfaces villous-tomentulose, soon or tardily glabrescent except for hairy midveins.</text>
      <biological_entity id="o2959" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2960" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="nonlustrous" value_original="nonlustrous" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="linear-lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="11" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_width" src="d0_s2" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s2" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2961" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous-tomentulose" value_original="villous-tomentulose" />
        <character constraint="except-for midveins" constraintid="o2962" is_modifier="false" modifier="soon; tardily" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o2962" name="midvein" name_original="midveins" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Corymbs 1.5–4.5 × 2–6.5 cm, villous to tomentulose.</text>
      <biological_entity id="o2963" name="corymb" name_original="corymbs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s3" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s3" to="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium 2–2.5 × 3–3.5 mm, exterior white villous-tomentulose, interior glabrous except at base;</text>
      <biological_entity id="o2964" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2965" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2966" name="exterior" name_original="exterior" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous-tomentulose" value_original="villous-tomentulose" />
        <character is_modifier="false" name="position" src="d0_s4" value="interior" value_original="interior" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2967" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o2966" id="r178" name="except at" negation="false" src="d0_s4" to="o2967" />
    </statement>
    <statement id="d0_s5">
      <text>sepals 1.3–1.8 × 1.6–2 mm, villous-tomentulose;</text>
      <biological_entity id="o2968" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2969" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s5" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous-tomentulose" value_original="villous-tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 4–5 × 2.4–3 mm;</text>
      <biological_entity id="o2970" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2971" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 3–5 mm.</text>
      <biological_entity id="o2972" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2973" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules 5–6 × 3.5–4 mm.</text>
      <biological_entity id="o2974" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 3.5–4 × 0.9–1.2 mm.</text>
      <biological_entity id="o2975" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Canyon margins and hillsides of the Sonoran Desert</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="canyon margins" constraint="of the sonoran desert" />
        <character name="habitat" value="hillsides" constraint="of the sonoran desert" />
        <character name="habitat" value="desert" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies sonorensis is known from the Ajo Mountains in Pima County. Some plants from the Baboquivari Mountains of Pima County have somewhat similar narrow leaves but lack the dense vestiture on stems, inflorescences, and abaxial leaf surfaces.</discussion>
  
</bio:treatment>