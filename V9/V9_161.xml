<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="illustration_page">111</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="A. Gray" date="1872" rank="species">pisocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 382. 1872</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species pisocarpa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100423</other_info_on_name>
  </taxon_identification>
  <number>26.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, loosely clustered or in dense thickets.</text>
      <biological_entity id="o25370" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="in dense thickets" value_original="in dense thickets" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o25371" name="thicket" name_original="thickets" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o25370" id="r1662" name="in" negation="false" src="d0_s0" to="o25371" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (2–) 4–20 (–25) dm, openly branched;</text>
      <biological_entity id="o25372" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark ± glaucous when young, dark reddish-brown or dull red with age outer layer may exfoliate as thin ash gray peel, glabrous;</text>
      <biological_entity id="o25373" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dull" value_original="dull" />
        <character constraint="with age" constraintid="o25374" is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o25374" name="age" name_original="age" src="d0_s2" type="structure" />
      <biological_entity id="o25376" name="peel" name_original="peel" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="ash gray" value_original="ash gray" />
      </biological_entity>
      <relation from="o25375" id="r1663" name="exfoliate as" negation="false" src="d0_s2" to="o25376" />
    </statement>
    <statement id="d0_s3">
      <text>infrastipular prickles usually paired, erect, rarely curved, usually subulate, 2–10 × 2–4 mm, base glabrous, internodal prickles rare or absent.</text>
      <biological_entity constraint="outer" id="o25375" name="layer" name_original="layer" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o25377" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25378" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o25379" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="rare" value_original="rare" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 5–10 (–13) cm;</text>
      <biological_entity id="o25380" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules 8–22 × 2–5 mm, auricles flared, 2–5 mm, margins usually entire, sometimes erose or lobed, finely ciliolate to ciliate, eglandular, surfaces glabrous or sparsely pubescent, sparsely stipitate-glandular or eglandular;</text>
      <biological_entity id="o25381" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25382" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flared" value_original="flared" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25383" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate to ciliate" value_original="ciliolate to ciliate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o25384" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and rachis sometimes with pricklets, glabrous or hairy hairs to 1 mm, sometimes stipitate-glandular;</text>
      <biological_entity id="o25385" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25386" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25387" name="pricklet" name_original="pricklets" src="d0_s6" type="structure" />
      <biological_entity id="o25388" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o25385" id="r1664" name="with" negation="false" src="d0_s6" to="o25387" />
      <relation from="o25386" id="r1665" name="with" negation="false" src="d0_s6" to="o25387" />
    </statement>
    <statement id="d0_s7">
      <text>leaflets 5–7 (–9), terminal: petiolule 8–12 mm, blade elliptic-ovate, (15–) 20–45 (–60) × 9–16 (–20) mm, widest at or below middle, membranous, base cuneate to obtuse, margins 1 (–2) -serrate, teeth 12–22 per side, on distal 3/4–4/5 of margin, acute, eglandular, apex acute, sometimes obtuse, abaxial surfaces pale green, usually sparsely pubescent, eglandular, adaxial green, dull, glabrous, rarely puberulent.</text>
      <biological_entity id="o25389" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o25390" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25391" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s7" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s7" to="45" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
        <character constraint="at or below middle leaflets" constraintid="o25392" is_modifier="false" name="width" src="d0_s7" value="widest" value_original="widest" />
        <character is_modifier="false" name="texture" notes="" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity constraint="middle" id="o25392" name="leaflet" name_original="leaflets" src="d0_s7" type="structure" />
      <biological_entity id="o25393" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
      <biological_entity id="o25394" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="1(-2)-serrate" value_original="1(-2)-serrate" />
      </biological_entity>
      <biological_entity id="o25395" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o25396" from="12" name="quantity" src="d0_s7" to="22" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o25396" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o25397" name="3/4-4/5" name_original="3/4-4/5" src="d0_s7" type="structure" />
      <biological_entity id="o25398" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o25399" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25400" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o25401" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o25395" id="r1666" name="on" negation="false" src="d0_s7" to="o25397" />
      <relation from="o25397" id="r1667" name="part_of" negation="false" src="d0_s7" to="o25398" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences corymbs, sometimes panicles or solitary flowers, 1–12-flowered.</text>
      <biological_entity constraint="inflorescences" id="o25402" name="corymb" name_original="corymbs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="1-12-flowered" value_original="1-12-flowered" />
      </biological_entity>
      <biological_entity id="o25403" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
      <biological_entity id="o25404" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, sometimes recurved, slender, 10–22 mm, usually glabrous, sometimes finely puberulent, eglandular, rarely stipulate-glandular;</text>
      <biological_entity id="o25405" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="22" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes finely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_function_or_pubescence" src="d0_s9" value="stipulate-glandular" value_original="stipulate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 2–3, lanceolate, 8–14 × 3–6 mm, margins entire, sometimes serrate, irregularly stipitate-glandular and/or erose, surfaces glabrous or pubescent, eglandular.</text>
      <biological_entity id="o25406" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25407" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="irregularly" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o25408" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 2.4–3.8 cm diam.;</text>
      <biological_entity id="o25409" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_unit="cm" name="diameter" src="d0_s11" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium ovoid-urceolate, 3–5 × 2.5–3.5 mm, glabrous, rarely setose, eglandular, neck (0–) 0.5–1 × 2 mm;</text>
      <biological_entity id="o25410" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid-urceolate" value_original="ovoid-urceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o25411" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s12" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s12" to="1" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals spreading, ovatelanceolate, 10–17 × 1.5–3 mm, tip 3–7 (–10) × 1.5–2.5 mm, margins entire, abaxial surfaces sometimes puberulent, densely or sparsely stipitate-glandular, rarely eglandular;</text>
      <biological_entity id="o25412" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s13" to="17" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25413" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25414" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25415" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="densely; sparsely" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, pink to deep pink, 12–18 × 10–18 mm;</text>
      <biological_entity id="o25416" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="deep pink" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s14" to="18" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s14" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 75;</text>
      <biological_entity id="o25417" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="75" value_original="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>carpels 22–35, styles exsert 1–2 mm beyond stylar orifice (1 mm diam.) of hypanthial disc (3.5 mm diam.).</text>
      <biological_entity id="o25418" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s16" to="35" />
      </biological_entity>
      <biological_entity id="o25419" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exsert" value_original="exsert" />
        <character char_type="range_value" constraint="beyond stylar, orifice" constraintid="o25420, o25421" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25420" name="stylar" name_original="stylar" src="d0_s16" type="structure" />
      <biological_entity id="o25421" name="orifice" name_original="orifice" src="d0_s16" type="structure" />
      <biological_entity id="o25422" name="hypanthial" name_original="hypanthial" src="d0_s16" type="structure" />
      <biological_entity id="o25423" name="disc" name_original="disc" src="d0_s16" type="structure" />
      <relation from="o25420" id="r1668" name="part_of" negation="false" src="d0_s16" to="o25422" />
      <relation from="o25420" id="r1669" name="part_of" negation="false" src="d0_s16" to="o25423" />
      <relation from="o25421" id="r1670" name="part_of" negation="false" src="d0_s16" to="o25422" />
      <relation from="o25421" id="r1671" name="part_of" negation="false" src="d0_s16" to="o25423" />
    </statement>
    <statement id="d0_s17">
      <text>Hips scarlet, globose, sometimes subglobose or ovoid, 7–15 × 7–13 mm, fleshy, glabrous, eglandular, rarely setose or stipitate-glandular, neck (0–) 1–1.5 × 1.5–3.5 mm;</text>
      <biological_entity id="o25424" name="hip" name_original="hips" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s17" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s17" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s17" to="13" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s17" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o25425" name="neck" name_original="neck" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s17" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s17" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals persistent, erect.</text>
      <biological_entity id="o25426" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Achenes basiparietal, 5–35, tan, 3–4 × 1.5–2.5 mm. 2n = 14, 28.</text>
      <biological_entity id="o25427" name="achene" name_original="achenes" src="d0_s19" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s19" to="35" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s19" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s19" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25428" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="14" value_original="14" />
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences (1–)3–12-flowered; sepal abaxial surfaces usually stipitate-glandular, tips to 10 mm; infrastipular prickles (1–)2; hips usually globose, rarely subglobose, abruptly narrowed to necks 1.5–3 mm diam.; leaflets most commonly 7, terminal blade 15–35 mm.</description>
      <determination>26a Rosa pisocarpa subsp. pisocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences 1–3(–10+)-flowered; sepal abaxial surfaces usually eglandular, rarely stipitate-glandular, tips to 7 mm; infrastipular prickles 0–1(–2); hips subglobose to ovoid, gradually to abruptly narrowed to necks 2.5–3.5 mm diam.; leaflets most commonly 5, terminal blade 20–45(–60) mm.</description>
      <determination>26b Rosa pisocarpa subsp. ahartii</determination>
    </key_statement>
  </key>
</bio:treatment>