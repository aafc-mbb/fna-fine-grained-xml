<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Multijugae</taxon_name>
    <taxon_name authority="Tiehm &amp; Ertter" date="1984" rank="species">basaltica</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>36: 228, fig. 1. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section multijugae;species basaltica;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100305</other_info_on_name>
  </taxon_identification>
  <number>45.</number>
  <other_name type="common_name">Soldier Meadow or Black Rock cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± rosetted;</text>
      <biological_entity id="o33396" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots fleshy-thickened.</text>
      <biological_entity id="o33397" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ± prostrate, sometimes ascending in supporting vegetation, 1.5–5 dm, lengths 2–3 times basal leaves.</text>
      <biological_entity id="o33398" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character constraint="in vegetation" constraintid="o33399" is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="5" to_unit="dm" />
        <character constraint="leaf" constraintid="o33400" is_modifier="false" name="length" src="d0_s2" value="2-3 times basal leaves" value_original="2-3 times basal leaves" />
      </biological_entity>
      <biological_entity id="o33399" name="vegetation" name_original="vegetation" src="d0_s2" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s2" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity constraint="basal" id="o33400" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves pinnate with distal leaflets ± confluent, 5–12 (–18) × 1–1.5 (–2) cm;</text>
      <biological_entity constraint="basal" id="o33401" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with distal leaflets" constraintid="o33402" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s3" to="18" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o33402" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–1 (–1.5) cm, straight hairs absent, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o33403" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33404" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o33405" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o33406" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lateral leaflets (5–) 10–15 (–20) per side, on nearly whole leaf axis, ± overlapping, largest ones (or leaflet lobes) elliptic, 0.4–1 (–2) × 0.2–0.5 (–1) cm, simple and entire or 2 (–3) -fid to base (sometimes shallowly toothed as well), segments 1–2 (–3), narrowly to broadly elliptic, 4–10 (–20) × (1–) 2–3 mm, apical tufts absent or less than 0.5 mm, surfaces green, glaucous, straight hairs absent or sparse to common, tightly appressed, 0.5 mm or less, stiff, cottony hairs absent, glands absent or sparse.</text>
      <biological_entity constraint="primary lateral" id="o33407" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s5" to="10" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="20" />
        <character char_type="range_value" constraint="per side" constraintid="o33408" from="10" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s5" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="to base" constraintid="o33410" is_modifier="false" name="shape" src="d0_s5" value="2(-3)-fid" value_original="2(-3)-fid" />
      </biological_entity>
      <biological_entity id="o33408" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o33409" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o33410" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o33411" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o33412" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33413" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o33414" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character name="some_measurement" src="d0_s5" unit="mm" value="0.5" value_original="0.5" />
        <character name="some_measurement" src="d0_s5" value="less" value_original="less" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o33415" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o33416" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o33407" id="r2203" name="on" negation="false" src="d0_s5" to="o33409" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 2–4.</text>
      <biological_entity constraint="cauline" id="o33417" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences (5–) 10–20-flowered, loosely cymose.</text>
      <biological_entity id="o33418" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="(5-)10-20-flowered" value_original="(5-)10-20-flowered" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.8–3 (–4) cm, straight to ± recurved in fruit.</text>
      <biological_entity id="o33419" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character constraint="in fruit" constraintid="o33420" is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o33420" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets lanceolate-elliptic to ovate-elliptic, 1.5–2.5 (–3) × 1 mm;</text>
      <biological_entity id="o33421" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o33422" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate-elliptic" name="shape" src="d0_s9" to="ovate-elliptic" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 2–5 mm diam.;</text>
      <biological_entity id="o33423" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o33424" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (3–) 4–5 (–6) mm, apex acute to acuminate;</text>
      <biological_entity id="o33425" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o33426" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33427" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (3.5–) 4.5–6.5 × 2.5–4 mm;</text>
      <biological_entity id="o33428" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o33429" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s12" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s12" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.5–3 mm, anthers 0.6–0.8 mm;</text>
      <biological_entity id="o33430" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o33431" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33432" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 3–10, styles 2–2.5 mm.</text>
      <biological_entity id="o33433" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o33434" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="10" />
      </biological_entity>
      <biological_entity id="o33435" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.8–2.2 mm, smooth to ± rugose, not carunculate.</text>
      <biological_entity id="o33436" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s15" to="more or less rugose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, subalkaline meadows in shrub steppe</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" constraint="in shrub steppe" />
        <character name="habitat" value="subalkaline meadows" constraint="in shrub steppe" />
        <character name="habitat" value="shrub steppe" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla basaltica is very distinctive in its glaucous, highly divided cylindric leaves with leaflets arranged in four ranks. The leaves strongly resemble those of some species of Ivesia, especially I. kingii S. Watson, but molecular analysis (B. Ertter et al. 1998) confirms a relationship with other members of sect. Multijugae.</discussion>
  <discussion>Potentilla basaltica is known from only two localities, one in Lassen County, California, and the other in Humboldt County, Nevada. Both populations are highly localized, and P. basaltica is a candidate for federal listing as well as of conservation concern in both states.</discussion>
  <discussion>The epithet basaltica was chosen as a reference to the Black Rock Desert, not habitat.</discussion>
  
</bio:treatment>