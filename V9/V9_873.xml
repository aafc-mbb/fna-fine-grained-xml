<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">517</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="mention_page">520</other_info_on_meta>
    <other_info_on_meta type="mention_page">523</other_info_on_meta>
    <other_info_on_meta type="mention_page">641</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Macracanthae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Macracanthae</taxon_name>
    <taxon_name authority="Schrader ex Link" date="1831" rank="species">succulenta</taxon_name>
    <place_of_publication>
      <publication_title>Handbuch</publication_title>
      <place_in_publication>2: 78. 1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section macracanthae;series macracanthae;species succulenta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416356</other_info_on_name>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Succulent hawthorn</other_name>
  <other_name type="common_name">aubépine succulente</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 40–80 dm.</text>
      <biological_entity id="o33239" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: older trunks usually bearing compound thorns;</text>
      <biological_entity id="o33241" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o33242" name="trunk" name_original="trunks" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="older" value_original="older" />
      </biological_entity>
      <biological_entity id="o33243" name="thorn" name_original="thorns" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="compound" value_original="compound" />
      </biological_entity>
      <relation from="o33242" id="r2196" name="bearing" negation="false" src="d0_s1" to="o33243" />
    </statement>
    <statement id="d0_s2">
      <text>twigs: new growth reddish green, glabrous, 1-year old dark, shiny redbrown, 2–3-years old becoming dark gray, older ± paler gray;</text>
      <biological_entity id="o33244" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <biological_entity id="o33245" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark" value_original="dark" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="becoming" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" modifier="more or less" name="life_cycle" src="d0_s2" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="paler gray" value_original="paler gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs numerous, usually recurved, shiny, 1-year old dark blackish brown, stout, 3–6 (–8) cm.</text>
      <biological_entity id="o33246" name="twig" name_original="twigs" src="d0_s3" type="structure" />
      <biological_entity id="o33247" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark blackish" value_original="dark blackish" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o33248" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o33247" id="r2197" name="on" negation="false" src="d0_s3" to="o33248" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 1–2 cm, narrowly winged distally, glabrous, eglandular;</text>
      <biological_entity id="o33249" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o33250" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="narrowly; distally" name="architecture" src="d0_s4" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade rhombic-elliptic to broadly rhombic-ovate or elliptic, 4–7 cm widest near middle, subcoriaceous mature (then often blue-green), base cuneate (constricted), lobes 3–5 per side, obscure to well-marked, sinuses shallow, lobe apex usually subacute to obtuse, margins serrate except proximally, veins 6–8 per side, impressed, apex acute to subacute, rarely obtuse, abaxial surface glabrous, adaxial scabrate-pubescent young.</text>
      <biological_entity id="o33251" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o33252" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="rhombic-elliptic" name="shape" src="d0_s5" to="broadly rhombic-ovate or elliptic" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
        <character constraint="near middle leaves" constraintid="o33253" is_modifier="false" name="width" src="d0_s5" value="widest" value_original="widest" />
        <character is_modifier="false" name="texture" notes="" src="d0_s5" value="subcoriaceous" value_original="subcoriaceous" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity constraint="middle" id="o33253" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o33254" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o33255" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o33256" from="3" name="quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="obscure" name="prominence" notes="" src="d0_s5" to="well-marked" />
      </biological_entity>
      <biological_entity id="o33256" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o33257" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character is_modifier="false" name="depth" src="d0_s5" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o33258" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually subacute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity id="o33259" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character char_type="range_value" constraint="per side" constraintid="o33261" from="6" name="quantity" src="d0_s5" to="8" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s5" value="impressed" value_original="impressed" />
      </biological_entity>
      <biological_entity id="o33260" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o33261" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o33262" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="subacute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33263" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33264" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrate-pubescent" value_original="scabrate-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="young" value_original="young" />
      </biological_entity>
      <relation from="o33259" id="r2198" name="except" negation="false" src="d0_s5" to="o33260" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 15–30-flowered;</text>
      <biological_entity id="o33265" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="15-30-flowered" value_original="15-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches pubescent or glabrous;</text>
      <biological_entity id="o33266" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles linear, 1.7 cm, margins glandular.</text>
      <biological_entity id="o33267" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s8" unit="cm" value="1.7" value_original="1.7" />
      </biological_entity>
      <biological_entity id="o33268" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 12–17 mm diam.;</text>
      <biological_entity id="o33269" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium glabrous or pubescent;</text>
      <biological_entity id="o33270" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals narrowly triangular, 4–6 mm, margins glandular-serrate to glandular-laciniate, abaxially glabrous, adaxial pubescence not recorded;</text>
      <biological_entity id="o33271" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o33272" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="glandular-serrate" name="shape" src="d0_s11" to="glandular-laciniate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33273" name="margin" name_original="margins" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 20, anthers usually red or pink, rarely white, 0.5–0.7 mm;</text>
      <biological_entity id="o33274" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o33275" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 2 or 3.</text>
      <biological_entity id="o33276" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s13" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pomes bright or deep red, lustrous, suborbicular, (4–) 7–10 (–14) mm diam., glabrous, rarely pubescent;</text>
      <biological_entity id="o33277" name="pome" name_original="pomes" src="d0_s14" type="structure">
        <character is_modifier="true" name="reflectance" src="d0_s14" value="bright" value_original="bright" />
        <character is_modifier="true" name="depth" src="d0_s14" value="deep" value_original="deep" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s14" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s14" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s14" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>flesh mealy or succulent mature;</text>
      <biological_entity id="o33278" name="flesh" name_original="flesh" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="mealy" value_original="mealy" />
        <character is_modifier="false" name="texture" src="d0_s15" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="life_cycle" src="d0_s15" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals spreading-reflexed;</text>
      <biological_entity id="o33279" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading-reflexed" value_original="spreading-reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pyrenes 2 or 3, sides pitted.</text>
      <biological_entity id="o33280" name="pyrene" name_original="pyrenes" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s17" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 51.</text>
      <biological_entity id="o33281" name="side" name_original="sides" src="d0_s17" type="structure">
        <character is_modifier="false" name="relief" src="d0_s17" value="pitted" value_original="pitted" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33282" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., Ont., Que.; Conn., Ill., Ind., Iowa, Maine, Mass., Md., Mich., Minn., Mo., N.C., N.H., N.Y., Ohio, Pa., R.I., Tenn., Va., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>Crataegus succulenta ranges through the southern Great Lakes area to the middle St. Lawrence and southern New England, to Minnesota, to Iowa, Missouri (very rare), and Ohio, the Appalachians to North Carolina. An outlier has recently been recognized in Manitoba.</discussion>
  <discussion>The dark twig colors of Crataegus succulenta are dramatic in winter and the coral red expanding bud scales are conspicuous in spring, more so than in most other species of hawthorn except C. macracantha. In summer, its commonly bluish green leaves, eglandular petioles, and impressed venation combine with thorn and twig characteristics to make this and C. macracantha usually instantly recognizable.</discussion>
  <discussion>Crataegus succulenta often forms suckering thickets in the north.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades proportionately narrow (1.6–2:1), elliptic to rhombic-elliptic or narrowly ovate</description>
      <determination>25e. Crataegus succulenta var. neofluvialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades proportionately wide (1.3–1.6:1), ovate to broadly rhombic-ovate or broadly elliptic</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pomes 4–6 mm diam.</description>
      <determination>25d Crataegus succulenta var. pisifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pomes 7–14 mm diam</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pomes 12–14 mm diam.</description>
      <determination>25c Crataegus succulenta var. gemmosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pomes 7–10 mm diam</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers red or pink.</description>
      <determination>25a Crataegus succulenta var. succulenta</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers white.</description>
      <determination>25b Crataegus succulenta var. michiganensis</determination>
    </key_statement>
  </key>
</bio:treatment>