<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">194</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Juzepczuk in V. L. Komarov et al." date="1941" rank="section">Aureae</taxon_name>
    <taxon_name authority="(Lehmann) Oakes ex Rydberg" date="1896" rank="species">robbinsiana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 304. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section aureae;species robbinsiana;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100357</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Haller f." date="unknown" rank="species">minima</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="variety">robbinsiana</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Potentill.,</publication_title>
      <place_in_publication>159. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species minima;variety robbinsiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="Malte" date="unknown" rank="species">hyparctica</taxon_name>
    <taxon_name authority="(Lehmann) Á. Löve &amp; D. Löve" date="unknown" rank="subspecies">robbinsiana</taxon_name>
    <taxon_hierarchy>genus P.;species hyparctica;subspecies robbinsiana</taxon_hierarchy>
  </taxon_identification>
  <number>70.</number>
  <other_name type="common_name">White Mountains cinquefoil</other_name>
  <other_name type="common_name">Robbins’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted to densely matted;</text>
      <biological_entity id="o6886" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branches short, ± slender, often embedded in old leaf-bases.</text>
      <biological_entity constraint="caudex" id="o6887" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o6888" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o6887" id="r419" modifier="often" name="embedded in" negation="false" src="d0_s1" to="o6888" />
    </statement>
    <statement id="d0_s2">
      <text>Stems spreading to erect, 0.1–0.4 dm, lengths 1–2 times basal leaves.</text>
      <biological_entity id="o6889" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s2" to="0.4" to_unit="dm" />
        <character constraint="leaf" constraintid="o6890" is_modifier="false" name="length" src="d0_s2" value="1-2 times basal leaves" value_original="1-2 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6890" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not in ranks, ternate, 1–2 cm;</text>
      <biological_entity constraint="basal" id="o6891" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="ternate" value_original="ternate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6892" name="rank" name_original="ranks" src="d0_s3" type="structure" />
      <relation from="o6891" id="r420" name="in" negation="false" src="d0_s3" to="o6892" />
    </statement>
    <statement id="d0_s4">
      <text>stipules: apex broadly acute;</text>
      <biological_entity id="o6893" name="stipule" name_original="stipules" src="d0_s4" type="structure" />
      <biological_entity id="o6894" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.7–3 cm, long hairs sparse to abundant, spreading to subappresed, 0.2–2 mm, weak, glands sparse to common;</text>
      <biological_entity id="o6895" name="stipule" name_original="stipules" src="d0_s5" type="structure" />
      <biological_entity id="o6896" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6897" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="abundant" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fixation" src="d0_s5" value="subappresed" value_original="subappresed" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o6898" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 3, central obovate, 0.5–1.3 × 0.2–1 cm, petiolule 0–1 mm, margins flat to ± revolute, not lobed, distal ± 2/3 evenly incised ± 1/2 to midvein, teeth 2–4 (–5) per side, surfaces similar, green, hairs ± abundant (or nearly absent adaxially), 0.5–1 mm, glands sparse to common.</text>
      <biological_entity id="o6899" name="stipule" name_original="stipules" src="d0_s6" type="structure" />
      <biological_entity id="o6900" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="central" id="o6901" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6902" name="petiolule" name_original="petiolule" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6903" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="more or less revolute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="more or less" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s6" value="2/3" value_original="2/3" />
        <character is_modifier="false" modifier="evenly; more or less" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character constraint="to midvein" constraintid="o6904" name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o6904" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o6905" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" />
        <character char_type="range_value" constraint="per side" constraintid="o6906" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o6906" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o6907" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o6908" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s6" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6909" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s6" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1 (–2) -flowered.</text>
      <biological_entity id="o6910" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1(-2)-flowered" value_original="1(-2)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels straight, 0.5–3 cm, not much longer in fruit than in flower.</text>
      <biological_entity id="o6911" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="not much" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets oblong to elliptic, (1.8–) 2–2.5 (–3) × 0.7–1.2 (–1.4) mm, ± equal to sepals, margins flat to ± revolute;</text>
      <biological_entity id="o6912" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o6913" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="elliptic" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_length" src="d0_s9" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
        <character constraint="to sepals" constraintid="o6914" is_modifier="false" modifier="more or less" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o6914" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o6915" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="more or less revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium (4–) 5–7 mm diam.;</text>
      <biological_entity id="o6916" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6917" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (1.8–) 2–2.5 (–3) mm, apex broadly acute;</text>
      <biological_entity id="o6918" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6919" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6920" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals yellow, 2–3 × 2–3 mm;</text>
      <biological_entity id="o6921" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6922" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.5–1 mm, anthers 0.4–0.6 mm;</text>
      <biological_entity id="o6923" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6924" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6925" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 20–30, styles columnar-tapered, sometimes ± papillate-swollen in proximal 1/3–1/2, 0.8–1 mm.</text>
      <biological_entity id="o6926" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o6927" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="30" />
      </biological_entity>
      <biological_entity id="o6928" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="columnar-tapered" value_original="columnar-tapered" />
        <character constraint="in proximal 1/3-1/2 , 0.8-1 mm" is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1–1.3 mm. 2n = 49.</text>
      <biological_entity id="o6929" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6930" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="49" value_original="49" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist rocky slopes and flats, in montane tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist rocky slopes" constraint="in montane tundra" />
        <character name="habitat" value="flats" constraint="in montane tundra" />
        <character name="habitat" value="montane tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.H., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla robbinsiana is known only from two sites in the White Mountains, Monroe County, New Hampshire, and is reported in Vermont. Additional populations have been established nearby as a result of transplant efforts. Listed in 1980 as a federally endangered species, an intense recovery program resulted in the species being delisted in 2002.</discussion>
  <discussion>Some early specimens were distributed as Potentilla frigida Villars, a similar European species.</discussion>
  <references>
    <reference>Cogbill, C. V. 1968. The interplay of botanists and Potentilla robbinsiana: Discovery, systematics, collection, and stewardship of a rare species. Rhodora 95: 52–75.</reference>
    <reference>Graber, R. E. 1980. The life history and ecology of Potentilla robbinsiana. Rhodora 82: 131–140.</reference>
    <reference>Graber, R. E. and L. G. Brewer. 1985. Changes in the population of the rare and endangered plant Potentilla robbinsiana Oakes during the period 1973 to 1983. Rhodora 87: 449–457.</reference>
    <reference>Steele, F. L. 1964. Potentilla robbinsiana in the White Mountains of New Hampshire. Rhodora 66: 408–411.</reference>
  </references>
  
</bio:treatment>