<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="Crépin" date="1889" rank="section">Gallicae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">gallica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 492. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section gallicae;species gallica;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011253</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">French or Gallic rose</other_name>
  <other_name type="common_name">rose of Provence</other_name>
  <other_name type="common_name">rosier de France</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: distal branches green to dull red;</text>
      <biological_entity id="o31062" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity constraint="distal" id="o31063" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dull red" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>prickles internodal, curved, sometimes erect, rarely hooked, declined, 3–7 × 2–5 mm.</text>
      <biological_entity id="o31064" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o31065" name="prickle" name_original="prickles" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="internodal" value_original="internodal" />
        <character is_modifier="false" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s1" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="declined" value_original="declined" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules subulate, 14–24 × 3–5 mm, auricles 4–10 mm, surfaces pubescent, eglandular;</text>
      <biological_entity id="o31066" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o31067" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s2" to="24" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31068" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31069" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole and rachis with sparse pricklets, puberulent, densely stipitate-glandular;</text>
      <biological_entity id="o31070" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o31071" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o31072" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o31073" name="pricklet" name_original="pricklets" src="d0_s3" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o31071" id="r2073" name="with" negation="false" src="d0_s3" to="o31073" />
      <relation from="o31072" id="r2074" name="with" negation="false" src="d0_s3" to="o31073" />
    </statement>
    <statement id="d0_s4">
      <text>leaflets 5 (–7), terminal blade slightly rugose, base obtuse to subcordate, margins shallowly 1 (–2) -dentate-crenate, teeth 14–23 per side, apex acute to acuminate, abaxial surfaces pale, gray green, sessile-glandular particularly on midveins, adaxial bluish green or dark green.</text>
      <biological_entity id="o31074" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31075" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o31076" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s4" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o31077" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o31078" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s4" value="1(-2)-dentate-crenate" value_original="1(-2)-dentate-crenate" />
      </biological_entity>
      <biological_entity id="o31079" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o31080" from="14" name="quantity" src="d0_s4" to="23" />
      </biological_entity>
      <biological_entity id="o31080" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o31081" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31082" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray green" value_original="gray green" />
        <character constraint="on midveins" constraintid="o31083" is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o31083" name="midvein" name_original="midveins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o31084" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3 (–8) -flowered.</text>
      <biological_entity id="o31085" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3(-8)-flowered" value_original="1-3(-8)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: bracts 1 or 2, caducous, lanceolate, 8–14 × 2–4 mm, margins ciliate, surfaces glabrous or pubescent, eglandular or stipitate-glandular.</text>
      <biological_entity id="o31086" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o31087" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s6" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31088" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o31089" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: hypanthium 5–7 × 3–5 (–7) mm, neck (0–) 1 × 3 mm;</text>
      <biological_entity id="o31090" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o31091" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31092" name="neck" name_original="neck" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s7" to="1" to_inclusive="false" to_unit="mm" />
        <character name="length" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character name="width" src="d0_s7" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepal tip 7 × 2 mm, erect or spreading;</text>
      <biological_entity id="o31093" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="sepal" id="o31094" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="mm" value="7" value_original="7" />
        <character name="width" src="d0_s8" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 27–35 × 20–30 mm [or larger];</text>
      <biological_entity id="o31095" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o31096" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="27" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles exsert 2–4 mm beyond hypanthium orifice.</text>
      <biological_entity id="o31097" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o31098" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exsert" value_original="exsert" />
        <character char_type="range_value" constraint="beyond hypanthium orifice" constraintid="o31099" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o31099" name="orifice" name_original="orifice" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Hips leathery.</text>
      <biological_entity id="o31100" name="hip" name_original="hips" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Achenes 3, 5 × 4–5 mm. 2n = 28.</text>
      <biological_entity id="o31101" name="achene" name_original="achenes" src="d0_s12" type="structure">
        <character name="area" src="d0_s12" unit=",5×4-5 mm" value="5×4-5" value_original="5×4-5" />
        <character name="area" src="d0_s12" unit=",5×4-5 mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31102" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste areas, roadside thickets, railways, former house sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="roadside thickets" />
        <character name="habitat" value="railways" />
        <character name="habitat" value="former house sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.S., Ont., Que.; Conn., Ill., Ind., Ky., La., Maine, Mass., Mich., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Tex., Vt., Va., Wis.; c, s Europe; w Asia (Caucasus, Iraq); introduced also in Mexico (Oaxaca), Central America, South America, w Europe (Guernsey), s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="w Asia (Caucasus)" establishment_means="native" />
        <character name="distribution" value="w Asia (Iraq)" establishment_means="native" />
        <character name="distribution" value="also in Mexico (Oaxaca)" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="w Europe (Guernsey)" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rosa ×centifolia Linnaeus (cabbage rose), long cultivated with about 500 ornamental cultivars known, is a probable hybrid of R. gallica × R. ×moschata Herrmann; it is not known to produce seed (P. V. Heath 1992). A. Bruneau et al. (2007) found that plastid DNA markers of R. gallica and R. ×centifolia are identical, indicating that R. gallica is the maternal parent. Rosa damascena Miller (Damask rose) also is close to R. ×centifolia and is thought to have the same parentage; if this proves true, then R. ×centifolia is the earlier name.</discussion>
  <discussion>Rosa gallica is used as a tonic, mild astringent, and eye wash, and to treat bowel complaints and excessive mucous discharges. Petals of the closely related R. ×centifolia are collected for the distillation of ‘rose water,’ a laxative used also to treat infantile diseases (J. Lindley 1838).</discussion>
  
</bio:treatment>