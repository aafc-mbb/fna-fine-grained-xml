<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">160</other_info_on_meta>
    <other_info_on_meta type="mention_page">126</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">158</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">164</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Leucophyllae</taxon_name>
    <place_of_publication>
      <publication_title>in J. M. Coulter and A. Nelson, New Man. Bot. Rocky Mt.,</publication_title>
      <place_in_publication>255. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Leucophyllae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318030</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Leucophyllae</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. N. Amer. Potentilleae,</publication_title>
      <place_in_publication>31. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;unranked Leucophyllae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Hippianae</taxon_name>
    <taxon_hierarchy>genus Potentilla;unranked Hippianae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Ertter &amp; Reveal" date="unknown" rank="section">Hippianae</taxon_name>
    <taxon_hierarchy>genus Potentilla;section Hippianae</taxon_hierarchy>
  </taxon_identification>
  <number>8l.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, ± tufted, not stoloniferous;</text>
      <biological_entity id="o23321" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened;</text>
      <biological_entity id="o23322" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vestiture primarily of long and cottony or sometimes crisped-cottony hairs, glands usually absent or sparse, sometimes common, not red.</text>
      <biological_entity id="o23323" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o23324" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="true" modifier="sometimes" name="pubescence" src="d0_s2" value="crisped-cottony" value_original="crisped-cottony" />
      </biological_entity>
      <biological_entity id="o23325" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s2" value="common" value_original="common" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <relation from="o23323" id="r1512" name="part_of" negation="false" src="d0_s2" to="o23324" />
    </statement>
    <statement id="d0_s3">
      <text>Stems ascending to erect, not flagelliform, not rooting at nodes, lateral to persistent basal rosettes, (0.3–) 1.5–7 (–8) dm, lengths (1–) 1.5–4 (–5) times basal leaves.</text>
      <biological_entity id="o23326" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o23327" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="8" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="7" to_unit="dm" />
        <character constraint="leaf" constraintid="o23329" is_modifier="false" name="length" src="d0_s3" value="(1-)1.5-4(-5) times basal leaves" value_original="(1-)1.5-4(-5) times basal leaves" />
      </biological_entity>
      <biological_entity id="o23327" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o23328" name="rosette" name_original="rosettes" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o23329" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not 2-ranked;</text>
      <biological_entity id="o23330" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o23331" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="2-ranked" value_original="2-ranked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline (0–) 1–6+;</text>
      <biological_entity id="o23332" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o23333" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves pinnate to subpinnate (with distal leaflets often confluent), 3–45 (–50) cm;</text>
      <biological_entity id="o23334" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o23335" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="pinnate" name="architecture" src="d0_s6" to="subpinnate" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs usually ± to tightly appressed, sometimes ascending, usually stiff, sometimes weak, glands absent or sparse or obscured;</text>
      <biological_entity id="o23336" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o23337" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o23338" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="tightly" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="true" modifier="sometimes" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="true" modifier="usually" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character is_modifier="true" modifier="sometimes" name="fragility" src="d0_s7" value="weak" value_original="weak" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o23337" id="r1513" name="to" negation="false" src="d0_s7" to="o23338" />
    </statement>
    <statement id="d0_s8">
      <text>leaflets (5–) 7–15, on distal (1/6–) 1/5–2/3 of leaf axis, separate to ± overlapping distally, oblanceolate to narrowly obovate, oblong, or cuneate, margins flat, distal 1/4 to whole length, rarely less, usually ± evenly incised 1/4–1/2 or less to midvein, sometimes entire, teeth (0–) 2–18 per side, surfaces similar to strongly dissimilar, abaxial white to ± green, cottony hairs absent or sparse to dense, adaxial green to white, not glaucous, long hairs mostly weak, sometimes stiff.</text>
      <biological_entity id="o23339" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o23340" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="15" />
        <character char_type="range_value" from="separate" modifier="distally" name="arrangement" notes="" src="d0_s8" to="more or less overlapping" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="narrowly obovate oblong or cuneate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="narrowly obovate oblong or cuneate" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="narrowly obovate oblong or cuneate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23341" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="of leaf axis" constraintid="o23342" from="1/6" name="quantity" src="d0_s8" to="1/5-2/3" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o23342" name="axis" name_original="axis" src="d0_s8" type="structure" />
      <biological_entity id="o23343" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character name="length" src="d0_s8" value="1/4" value_original="1/4" />
        <character is_modifier="false" modifier="rarely less; less; usually more or less evenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s8" to="1/2" />
        <character name="quantity" src="d0_s8" value="less" value_original="less" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23344" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o23345" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s8" to="2" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o23346" from="2" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
      <biological_entity id="o23346" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o23347" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o23348" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" modifier="strongly; strongly" name="coloration" src="d0_s8" to="more or less green" />
      </biological_entity>
      <biological_entity id="o23349" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" value="sparse to dense" value_original="sparse to dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23350" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s8" to="white" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o23351" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" modifier="mostly" name="fragility" src="d0_s8" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o23340" id="r1514" name="on" negation="false" src="d0_s8" to="o23341" />
      <relation from="o23343" id="r1515" name="to" negation="false" src="d0_s8" to="o23344" />
      <relation from="o23347" id="r1516" name="to" negation="false" src="d0_s8" to="o23348" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (5–) 7–60-flowered, cymose, open.</text>
      <biological_entity id="o23352" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(5-)7-60-flowered" value_original="(5-)7-60-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels straight in fruit, 0.3–3 (–6) cm, proximal usually much longer than distal.</text>
      <biological_entity id="o23353" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o23354" is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23354" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o23355" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o23356" is_modifier="false" name="length_or_size" src="d0_s10" value="usually much longer" value_original="usually much longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23356" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o23357" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 2–7 mm diam.;</text>
      <biological_entity id="o23358" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals yellow, ± obcordate, (3–) 4–10 mm, usually longer than sepals, retuse;</text>
      <biological_entity id="o23359" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="than sepals" constraintid="o23360" is_modifier="false" name="length_or_size" src="d0_s13" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity id="o23360" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens 20–25;</text>
      <biological_entity id="o23361" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, tapered-filiform, papillate-swollen in proximal 1/5 or less, (1–) 1.5–3 mm.</text>
      <biological_entity id="o23362" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="tapered-filiform" value_original="tapered-filiform" />
        <character constraint="in proximal 1/5" constraintid="o23363" is_modifier="false" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="less" name="atypical_some_measurement" src="d0_s15" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="less" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23363" name="1/5" name_original="1/5" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth to slightly rugose.</text>
      <biological_entity id="o23364" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s16" to="slightly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w, c North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w" establishment_means="native" />
        <character name="distribution" value="c North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>Section Leucophyllae is an apparent radiation of four species in monsoonal regions of the American Southwest, ranging from the Colorado Plateau, Rocky Mountains, and western Great Plains to the prairies of Canada. The plants share features with sect. Graciles but have consistently pinnate leaves, distal leaflets often decurrent, and petioles commonly densely strigose. Plants are often locally abundant in relatively dry forest openings and open grasslands, tending to be more xeric-tolerant than plants of sect. Graciles.</discussion>
  <discussion>Descriptions and keys focus on the most distinctive expressions, which are blurred by a propensity for the species to hybridize wherever they grow sympatrically. This is counteracted by at least some level of habitat partitioning; for example, Potentilla hippiana tends to occur on deeper soils and P. crinita on rockier slopes than other species in this section.</discussion>
  <discussion>As a further complication, key diagnostic characters sometimes sort independently in different parts of the relatively large ranges of the species. The prairie expression described as Potentilla argyrea is particularly problematic, undermining the distinction between P. effusa and P. hippiana in the northern part of their ranges. In the southern Rocky Mountains, the two species are relatively distinct on the basis of correlated vestiture and leaflet toothing. In contrast, the prairie expression combines vestiture closer to P. hippiana with leaflet features of P. effusa. Pending further analysis, B. Boivin (1952) and B. C. Johnston (1980) are followed in retaining this variant in P. hippiana, though without formal taxonomic recognition. Inclusion in P. effusa might prove more justified, as done by J. Soják (2006).</discussion>
  <discussion>Since Potentilla subjuga (sect. Subjugae) and P. ovina (sect. Multijugae) are sometimes identified as members of sect. Leucophyllae, they are included herein and key out in the third and fifth couplets, respectively.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets: abaxial surfaces white to gray, crisped or cottony hairs abundant to dense</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets: abaxial surfaces ± green to grayish or silvery, crisped or cottony hairs usually absent or sparse</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Epicalyx bractlets: abaxial vestiture often much sparser than that of sepals, often glabrate or glabrescent distally, straight hairs absent or sparse, cottony hairs usually abundant (at least proximally); lateral leaflets evenly to unevenly paired, teeth (1–)4–9 per side on distal (1/2–)2/3–3/4 (rarely more) of margin, surfaces similar, cottony hairs abundant to dense abaxially, abundant adaxially; Alberta to Manitoba, to Colorado, possibly disjunct in nc New Mexico and ne Utah.</description>
      <determination>31 Potentilla effusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Epicalyx bractlets: abaxial vestiture similar to or ± sparser than that of sepals, usually not glabrescent, straight hairs ± abundant, crisped or sometimes ± cottony hairs absent or sparse to abundant; lateral leaflets evenly (to unevenly in argyrea phase) paired, teeth (5–)7–12(–18) per side on distal (2/3–)3/4 to whole margin, surfaces ± to strongly dissimilar, crisped-cottony hairs abundant to dense abaxially, absent or sparse to common adaxially; c Canada to Nevada, Arizona, and New Mexico, introduced farther east</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflets (2–)3–6(–7) per side, distal pairs ± decurrent, often confluent with terminal leaflet, surfaces ± to strongly dissimilar; 500–3400 m.</description>
      <determination>30 Potentilla hippiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflets 2(–3) per side, distal pairs not decurrent or confluent with terminal leaflet, surfaces strongly dissimilar; 3400–4000 m.</description>
      <determination>34 Potentilla subjuga (sect. Subjugae)</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lateral leaflets often unevenly paired, distal pairs usually not decurrent or confluent with terminal leaflet, surfaces ± green; epicalyx bractlets with cottony hairs sometimes abundant (at least proximally)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Lateral leaflets evenly paired, distal pairs ± decurrent, often confluent with terminal leaflet, surfaces greenish to silvery or grayish; epicalyx bractlets with cottony hairs usually absent</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems ascending to erect; basal leaves: leaflets 2–3 per side, incised ± 1/2 to midvein, teeth 2–5 per side; cauline leaves 2–6+; cottony hairs often present (at least on epicalyx bractlets); nc Colorado.</description>
      <determination>31 Potentilla effusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems prostrate to ascending; basal leaves: leaflets 3–5 per side, incised 1/2–2/3(–3/4) to midvein, teeth (0–)1–2(–3) per side; cauline leaves 1–2; cottony hairs absent; Uinta and middle Rocky mountains, to Nevada and Colorado.</description>
      <determination>39 Potentilla ovina (sect. Multijugae)</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaves 15–45(–50) cm; larger leaflets (2–)3–7 cm, not conduplicate, teeth 6–18 per side on distal 3/4+ of margin; se Wyoming, e slope Rocky Mountains of Colorado to Sacramento Mountains, New Mexico.</description>
      <determination>32 Potentilla ambigens</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaves 3–15(–20) cm; larger leaflets 1–3(–4) cm, often conduplicate, teeth (0–)1–5(–9) per side on distal 1/4–1/2(–2/3) or less of margin; s Nevada to sw Colorado, n Arizona, and nw New Mexico.</description>
      <determination>33 Potentilla crinita</determination>
    </key_statement>
  </key>
</bio:treatment>