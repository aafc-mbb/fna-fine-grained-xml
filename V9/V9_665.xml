<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">sorbarieae</taxon_name>
    <taxon_name authority="(Seringe) A. Braun in P. F. A. Ascherson" date="unknown" rank="genus">sorbaria</taxon_name>
    <taxon_name authority="(Regel) Maximowicz" date="unknown" rank="species">kirilowii</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Petersburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 225. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe sorbarieae;genus sorbaria;species kirilowii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011659</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Regel" date="unknown" rank="species">kirilowii</taxon_name>
    <place_of_publication>
      <publication_title>in E. Regel and H. S. T. Tiling, Fl. Ajan.,</publication_title>
      <place_in_publication>81. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species kirilowii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sorbaria</taxon_name>
    <taxon_name authority="C. K. Schneider" date="unknown" rank="species">arborea</taxon_name>
    <taxon_hierarchy>genus Sorbaria;species arborea</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Giant false spiraea</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–70 dm.</text>
      <biological_entity id="o12333" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="dm" name="some_measurement" src="d0_s0" to="70" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade 14–35 × 8–17 cm;</text>
      <biological_entity id="o12334" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12335" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s1" to="35" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s1" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets 13–21, oblong-ovate, (30–) 60–90 (–130) × (10–) 13–30 (–40) mm, abaxial surface with simple hairs to 0.8 mm in vein-axils, sometimes also stipitate-stellate, adaxial glabrous.</text>
      <biological_entity id="o12336" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o12337" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s2" to="21" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s2" to="60" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="130" to_unit="mm" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s2" to="90" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s2" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="40" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s2" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12338" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" notes="" src="d0_s2" value="stipitate-stellate" value_original="stipitate-stellate" />
      </biological_entity>
      <biological_entity id="o12339" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" constraint="in vein-axils" constraintid="o12340" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12340" name="vein-axil" name_original="vein-axils" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o12341" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12338" id="r768" name="with" negation="false" src="d0_s2" to="o12339" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences (7–) 20–30 (–42) × (5–) 10–17 (–33) cm.</text>
      <biological_entity id="o12342" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_length" src="d0_s3" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="42" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_width" src="d0_s3" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="33" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s3" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels (and axes) usually glabrous.</text>
      <biological_entity id="o12343" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 5–9 mm diam.</text>
    </statement>
    <statement id="d0_s6">
      <text>(anther tip to tip);</text>
      <biological_entity id="o12344" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o12345" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals ovate-rounded, margins entire;</text>
      <biological_entity id="o12346" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate-rounded" value_original="ovate-rounded" />
      </biological_entity>
      <biological_entity id="o12347" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals orbicular, 1.9–3.5 mm diam.;</text>
      <biological_entity id="o12348" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="diameter" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 20–25, those opposite petals 1–1.5 mm, others 1.9–2.5 (–4.5) mm;</text>
      <biological_entity id="o12349" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="25" />
      </biological_entity>
      <biological_entity id="o12350" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="opposite" value_original="opposite" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12351" name="other" name_original="others" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries glabrous or nearly so, styles 1–1.2 mm.</text>
      <biological_entity id="o12352" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s11" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o12353" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Follicles 3–5.3 mm, glabrous or nearly so.</text>
      <biological_entity id="o12354" name="follicle" name_original="follicles" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s12" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Wash.; Asia (China, Japan, Korea, Tibet); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Tibet)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">kirilowi</other_name>
  <discussion>Sorbaria kirilowii is cultivated in the United States and Europe, persisting and becoming naturalized in northwestern Washington.</discussion>
  
</bio:treatment>