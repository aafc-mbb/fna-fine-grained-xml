<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">168</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Multijugae</taxon_name>
    <taxon_name authority="B. C. Johnston &amp; Ertter" date="2010" rank="species">uliginosa</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>4: 14, fig. 1. 2010</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section multijugae;species uliginosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100373</other_info_on_name>
  </taxon_identification>
  <number>43.</number>
  <other_name type="common_name">Cunningham Marsh cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rosetted to tufted;</text>
      <biological_entity id="o934" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="rosetted" name="arrangement" src="d0_s0" to="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened.</text>
      <biological_entity id="o935" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems probably prostrate to decumbent, or ± ascending in supporting vegetation, 2.5–5.5 dm, lengths 1–2 times basal leaves.</text>
      <biological_entity id="o936" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="probably" name="growth_form_or_orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="in vegetation" constraintid="o937" is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="5.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o938" is_modifier="false" name="length" src="d0_s2" value="1-2 times basal leaves" value_original="1-2 times basal leaves" />
      </biological_entity>
      <biological_entity id="o937" name="vegetation" name_original="vegetation" src="d0_s2" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s2" value="supporting" value_original="supporting" />
      </biological_entity>
      <biological_entity constraint="basal" id="o938" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves pinnate with distal leaflets ± confluent, 15–32 × 2–4 cm;</text>
      <biological_entity constraint="basal" id="o939" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with distal leaflets" constraintid="o940" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" notes="" src="d0_s3" to="32" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o940" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–10 (–15) cm, straight hairs absent or sparse to common, appressed, 0.5–1.5 mm, stiff, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o941" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o942" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o943" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o944" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>primary lateral leaflets 6–10 (–12) per side (irregularly paired), on distal ± 1/2 of leaf axis, loosely overlapping to proximally separate, largest ones cuneate to flabellate, 1.2–2.2 × 1–3 cm, ± whole margin unevenly incised 3/4 to nearly to midvein, ultimate segments (3–) 5–10 (–15), linear to narrowly oblanceolate, 5–17 × 1–2.5 mm, apical tufts less than 0.5 mm, surfaces green, not glaucous, straight hairs sparse (often glabrate adaxially), appressed, 0.5–1 (–1.5) mm, stiff, cottony hairs absent, glands absent or sparse.</text>
      <biological_entity constraint="primary lateral" id="o945" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="12" />
        <character char_type="range_value" constraint="per side" constraintid="o946" from="6" name="quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="loosely overlapping" name="arrangement" notes="" src="d0_s5" to="proximally separate" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="flabellate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s5" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o946" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o947" name="distal" name_original="distal" src="d0_s5" type="structure">
        <character constraint="of leaf axis" constraintid="o948" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o948" name="axis" name_original="axis" src="d0_s5" type="structure" />
      <biological_entity id="o949" name="margin" name_original="margin" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less; unevenly" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character constraint="nearly to midvein" constraintid="o950" name="quantity" src="d0_s5" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o950" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity constraint="ultimate" id="o951" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="15" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="10" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="17" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o952" name="tuft" name_original="tufts" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o953" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o954" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o955" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s5" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o956" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o945" id="r56" name="on" negation="false" src="d0_s5" to="o947" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 2.</text>
      <biological_entity constraint="cauline" id="o957" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 6–10-flowered, very openly cymose.</text>
      <biological_entity id="o958" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="6-10-flowered" value_original="6-10-flowered" />
        <character is_modifier="false" modifier="very openly" name="architecture" src="d0_s7" value="cymose" value_original="cymose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–6 cm, ± recurved in fruit.</text>
      <biological_entity id="o959" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character constraint="in fruit" constraintid="o960" is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o960" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets narrowly elliptic to oblong-ovate, 4–6 × 1–2.5 mm;</text>
      <biological_entity id="o961" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o962" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s9" to="oblong-ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 5–6 mm diam.;</text>
      <biological_entity id="o963" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o964" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–7 mm, apex acute;</text>
      <biological_entity id="o965" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o966" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o967" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 6–10 × 5–8 mm;</text>
      <biological_entity id="o968" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o969" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments (1.5–) 2–3 mm, anthers 0.7–1.2 mm;</text>
      <biological_entity id="o970" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o971" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o972" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels ca. 10, styles 2.5–3.5 mm.</text>
      <biological_entity id="o973" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o974" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o975" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 2–2.6 mm, smooth, ± carunculate.</text>
      <biological_entity id="o976" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Permanent oligotrophic wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="permanent oligotrophic wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–40 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="40" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla uliginosa is known only from a handful of historical collections from Cunningham Marsh, one of three wetlands in southern Sonoma County that harbor a suite of disjunct and regionally rare species (B. C. Johnston and B. Ertter 2010). Although previously included in P. hickmanii, P. uliginosa differs in having woodier caudices, longer stems and leaves, and more deeply incised leaflets occupying less of the leaf axis. Potentilla uliginosa is presumed extinct; however, a morphologically comparable collection of the P. millefolia complex from Josephine County, Oregon (Deer Creek, near Selma, 14 June 1929, Applegate 5735, UC), opens the possibility of additional undiscovered populations in the mountains of northwestern California.</discussion>
  
</bio:treatment>