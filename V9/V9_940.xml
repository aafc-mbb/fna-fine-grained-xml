<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">555</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="(Sargent) Rehder" date="1940" rank="series">Molles</taxon_name>
    <taxon_name authority="Buckley" date="1862" rank="species">texana</taxon_name>
    <taxon_name authority="(Sargent) J. B. Phipps" date="2012" rank="variety">dasyphylla</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012–78: 5. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series molles;species texana;variety dasyphylla;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100607</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Sargent" date="unknown" rank="species">dasyphylla</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Missouri Bot. Gard.</publication_title>
      <place_in_publication>22: 80. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;species dasyphylla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="species">induta</taxon_name>
    <taxon_hierarchy>genus C.;species induta</taxon_hierarchy>
  </taxon_identification>
  <number>49b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades: lobes 1 or 2 (or 3) per side, sinuses shallow, lobe apex ± obtuse to subacute.</text>
      <biological_entity id="o2752" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure" />
      <biological_entity id="o2753" name="lobe" name_original="lobes" src="d0_s0" type="structure">
        <character name="quantity" src="d0_s0" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s0" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o2754" name="side" name_original="side" src="d0_s0" type="structure" />
      <biological_entity id="o2755" name="sinuse" name_original="sinuses" src="d0_s0" type="structure">
        <character is_modifier="false" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o2756" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character char_type="range_value" from="less obtuse" name="shape" src="d0_s0" to="subacute" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Brush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="brush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Mo., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus induta is a small-anthered and particularly large-fruited variant, locally known as turkey haw. Crataegus brachyphylla Sargent is very similar to var. dasyphylla, but with only three styles and pyrenes; it is from dry hills in southwestern Arkansas.</discussion>
  
</bio:treatment>