<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">402</other_info_on_meta>
    <other_info_on_meta type="mention_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">407</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">spiraea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">salicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 489. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus spiraea;species salicifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011799</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">amena</taxon_name>
    <taxon_hierarchy>genus Spiraea;species amena</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Bridewort</other_name>
  <other_name type="common_name">willowleaf meadowsweet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 dm, thicket forming.</text>
      <biological_entity id="o19642" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending or spreading, rarely branched.</text>
      <biological_entity id="o19643" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending or spreading" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 2–6 mm;</text>
      <biological_entity id="o19644" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19645" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly rhombic to rhombic or lanceolate to narrowly elliptic, usually widest at middle, 3–7 × 1–3 cm, length 3–5 times width, chartaceous, base acute, margins sharply serrate to serrulate nearly to base, number of primary and secondary serrations 1 times number of secondary-veins (excluding inter-secondary veins), venation pinnate craspedodromous, secondary-veins not prominent, regularly terminating in primary teeth, inter-secondary veins usually 1–4 per leaf, apex acute, abaxial surface mostly puberulent, adaxial glabrous.</text>
      <biological_entity id="o19646" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19647" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly rhombic" name="shape" src="d0_s3" to="rhombic or lanceolate" />
        <character constraint="at middle leaves" constraintid="o19648" is_modifier="false" modifier="usually" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" notes="" src="d0_s3" to="7" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-5" value_original="3-5" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity constraint="middle" id="o19648" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19649" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o19650" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="to base" constraintid="o19651" from="sharply serrate" name="architecture_or_shape" src="d0_s3" to="serrulate" />
        <character constraint="secondary-vein" constraintid="o19653" is_modifier="false" name="size_or_quantity" src="d0_s3" value="1 times number of secondary-veins" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity id="o19651" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity constraint="primary and secondary" id="o19652" name="serration" name_original="serrations" src="d0_s3" type="structure" />
      <biological_entity id="o19653" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure" />
      <biological_entity id="o19654" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="primary" id="o19655" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity constraint="inter-secondary" id="o19656" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per leaf" constraintid="o19657" from="1" modifier="regularly" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o19657" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o19658" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19659" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19660" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o19650" id="r1282" name="part_of" negation="false" src="d0_s3" to="o19652" />
      <relation from="o19654" id="r1283" name="terminating in" negation="false" src="d0_s3" to="o19655" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences mostly terminal, cylindric to obconic panicles, 5–10 × 2–4 cm height 2–5 times diam.;</text>
      <biological_entity id="o19661" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s4" to="obconic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" notes="" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" notes="" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s4" value="2-5 times diam" value_original="2-5 times diam" />
      </biological_entity>
      <biological_entity id="o19662" name="panicle" name_original="panicles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>branches sometimes in axils of leaves, puberulent or glabrous.</text>
      <biological_entity id="o19663" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19664" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity id="o19665" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o19663" id="r1284" name="in" negation="false" src="d0_s5" to="o19664" />
      <relation from="o19664" id="r1285" name="part_of" negation="false" src="d0_s5" to="o19665" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 3–6 mm, puberulent or glabrous.</text>
      <biological_entity id="o19666" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 4–7 (–10) mm diam.;</text>
      <biological_entity id="o19667" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthia hemispheric, 0.8–1 mm, abaxial surface glabrous or sparsely puberulent, adaxial glabrous;</text>
      <biological_entity id="o19668" name="hypanthium" name_original="hypanthia" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19669" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19670" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals ovate, 0.8–1 mm;</text>
      <biological_entity id="o19671" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals pink, elliptic to widely obovate, 1.8–2 mm;</text>
      <biological_entity id="o19672" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="widely obovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes 0;</text>
      <biological_entity id="o19673" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 28–32, 2 times petal length.</text>
      <biological_entity id="o19674" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s12" to="32" />
        <character constraint="petal" constraintid="o19675" is_modifier="false" name="length" src="d0_s12" value="2 times petal length" value_original="2 times petal length" />
      </biological_entity>
      <biological_entity id="o19675" name="petal" name_original="petal" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Follicles oblanceoloid, 4 mm, glabrous, adaxial suture glabrous or ciliate.</text>
      <biological_entity id="o19676" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceoloid" value_original="oblanceoloid" />
        <character name="some_measurement" src="d0_s13" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19677" name="suture" name_original="suture" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug; fruiting Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old homesteads in moist areas, flood plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old homesteads" constraint="in moist areas , flood plains" />
        <character name="habitat" value="moist areas" />
        <character name="habitat" value="flood plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Ga., Ky., Mich., Miss., N.C., Vt., Va.; e Europe; e Asia; introduced also in w, c Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="e Europe" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="also in w" establishment_means="introduced" />
        <character name="distribution" value="c Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spiraea salicifolia is escaped from cultivation in northeastern North America. K. Sax (1936) reported a specimen of S. salicifolia as tetraploid; it is possible that it was not a European specimen, so this count may be unreliable.</discussion>
  <discussion>The name Spiraea salicifolia is often misapplied to individuals of S. alba var. alba or S. alba var. latifolia (see discussion above). In addition, being a cultivated species that has been popular in gardens and likely introduced and escaped in various hybridized forms, the intermediates and potential hybrids with native taxa can make identification difficult. For problematic specimens that appear to be hybrids, see A. J. Silverside (1988, 1990).</discussion>
  
</bio:treatment>