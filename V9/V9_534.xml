<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Joshua C. Springer,Bruce D. Parfitt†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">131</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">331</other_info_on_meta>
    <other_info_on_meta type="mention_page">343</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">Dryas</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 501. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 220. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus Dryas</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek Dryas, name for oak nymphs</other_info_on_name>
    <other_info_on_name type="fna_id">110971</other_info_on_name>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Mountain-avens</other_name>
  <other_name type="common_name">dryade</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, tufted or matted, scapose, 0.1–2.3 dm;</text>
      <biological_entity id="o32165" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s0" to="2.3" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices relatively long, woody.</text>
      <biological_entity id="o32166" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 3+, branched, rooting freely;</text>
      <biological_entity id="o32167" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bark green in 1st year, reddish to brown in 2d year, often appearing blackish, peeling;</text>
      <biological_entity id="o32169" name="year" name_original="year" src="d0_s3" type="structure" />
      <biological_entity id="o32170" name="year" name_original="year" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>short and long-shoots absent;</text>
      <biological_entity id="o32168" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character constraint="in year" constraintid="o32169" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character char_type="range_value" constraint="in year" constraintid="o32170" from="reddish" name="coloration" notes="" src="d0_s3" to="brown" />
        <character is_modifier="false" modifier="often" name="coloration" notes="" src="d0_s3" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o32171" name="long-shoot" name_original="long-shoots" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stoloniferous branches horizontal, relatively short, glabrous.</text>
      <biological_entity id="o32172" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves persistent, simple or (in D. drummondii) basal leaflets 1 (–2);</text>
      <biological_entity id="o32173" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o32174" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stipules persistent, narrowly lanceolate, margins usually entire, sometimes 1–2-toothed;</text>
      <biological_entity id="o32175" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o32176" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="1-2-toothed" value_original="1-2-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole present;</text>
      <biological_entity id="o32177" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade elliptic, lanceolate, linear, oblong, oblongelliptic, obovate, oval, or ovate, 0.2–3.9 (–4.6) cm, smooth to rugose or plicate, margins usually flat, sometimes revolute, crenate, dentate, serrate, or entire, abaxial surface tomentose or woolly, adaxial usually glabrous, sometimes tomentose or sparsely hairy on proximal portion of midvein.</text>
      <biological_entity id="o32178" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.9" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="4.6" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s9" to="3.9" to_unit="cm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s9" to="rugose" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s9" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o32179" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o32180" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o32181" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
        <character constraint="on proximal portion" constraintid="o32182" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o32182" name="portion" name_original="portion" src="d0_s9" type="structure" />
      <biological_entity id="o32183" name="midvein" name_original="midvein" src="d0_s9" type="structure" />
      <relation from="o32182" id="r2135" name="part_of" negation="false" src="d0_s9" to="o32183" />
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal, flowers solitary;</text>
      <biological_entity id="o32184" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o32185" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts absent;</text>
      <biological_entity id="o32186" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles absent.</text>
      <biological_entity id="o32187" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicels present.</text>
      <biological_entity id="o32188" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers 13–29 mm diam.;</text>
      <biological_entity id="o32189" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="diameter" src="d0_s14" to="29" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>hypanthium saucer-shaped, 3–15 mm, exterior villous, stipitate-glandular;</text>
      <biological_entity id="o32190" name="hypanthium" name_original="hypanthium" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="saucer--shaped" value_original="saucer--shaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32191" name="exterior" name_original="exterior" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals 8–10, ascending to spreading, linear-oblong or linear-lanceolate to lanceolate, elliptic, or ovate;</text>
      <biological_entity id="o32192" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s16" to="10" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s16" to="spreading" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s16" to="lanceolate elliptic or ovate" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s16" to="lanceolate elliptic or ovate" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s16" to="lanceolate elliptic or ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 8–10 (–12), deciduous or persistent, usually white or cream, sometimes yellow, ovate to obovate, oblanceolate, or ovoid;</text>
      <biological_entity id="o32193" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="12" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s17" to="10" />
        <character is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s17" to="obovate oblanceolate or ovoid" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s17" to="obovate oblanceolate or ovoid" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s17" to="obovate oblanceolate or ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stamens 50–130, shorter than petals;</text>
      <biological_entity id="o32194" name="stamen" name_original="stamens" src="d0_s18" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s18" to="130" />
        <character constraint="than petals" constraintid="o32195" is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o32195" name="petal" name_original="petals" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>torus hemispheric;</text>
      <biological_entity id="o32196" name="torus" name_original="torus" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>carpels 60–150, sessile, stigmas lateral.</text>
      <biological_entity id="o32197" name="carpel" name_original="carpels" src="d0_s20" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s20" to="150" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o32198" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="position" src="d0_s20" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits aggregated achenes, 20–40, linear to lanceolate, 0.8–3.5 mm, smooth with many short white hairs;</text>
      <biological_entity id="o32199" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s21" to="lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s21" to="3.5" to_unit="mm" />
        <character constraint="with hairs" constraintid="o32201" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s21" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o32200" name="achene" name_original="achenes" src="d0_s21" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s21" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s21" to="40" />
      </biological_entity>
      <biological_entity id="o32201" name="hair" name_original="hairs" src="d0_s21" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s21" value="many" value_original="many" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s21" value="short" value_original="short" />
        <character is_modifier="true" name="coloration" src="d0_s21" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hypanthium persistent;</text>
      <biological_entity id="o32202" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, erect-ascending.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 9.</text>
      <biological_entity id="o32203" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s23" value="erect-ascending" value_original="erect-ascending" />
      </biological_entity>
      <biological_entity constraint="x" id="o32204" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia; arctic, boreal, and alpine regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="arctic" establishment_means="native" />
        <character name="distribution" value="boreal" establishment_means="native" />
        <character name="distribution" value="and alpine regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 15 (7 in the flora).</discussion>
  <discussion>Hybridization is common in Dryas (E. Hultén 1959b), often rendering specimen identification difficult when based on morphological features. Molecular evidence indicates that there are multiple morphologically similar species within the broad concept of D. octopetala (R. Elven et al., unpubl.). A study of amplified fragment length polymorphism (AFLP) markers found D. drummondii to be sister to all other species of the genus (I. Skrede et al. 2006). The taxonomy in the present treatment is based on that in the Pan-arctic Flora (R. Elven et al., http://nhm2.uio.no/paf/) and reflects the findings by Skrede et al. that AFLP markers support various proposals of B. A. Jurtzev (1984) and refute some of those by Hultén.</discussion>
  <discussion>Species of Dryas are among the important components of arctic and alpine tundra in terms of biomass (I. Skrede et al. 2006) and cover. Dryas has been placed in Dryadeae, which comprises four (Frankia association) actinorhizal genera, also including Cercocarpus, Chamaebatia, and Purshia (D. Potter et al. 2007). Dryas octopetala in the broad sense is characteristically a pioneer (T. T. Elkington 1971); it forms ectomycorrhizal associations with Hebeloma alpinum (J. Favre) Bruchet (Cortinariaceae, Agaricales), Cenococcum geophilum Fries (Ascomycetae, familial relationship unknown), and other fungi (M. Gardes and A. Dahlberg 1996). Nitrogen fixation occurs in root nodules of D. drummondii, D. integrifolia, and D. octopetala (in the broad sense) (D. B. Lawrence et al. 1967). The authors have observed root nodules on D. ajanensis subsp. beringensis. Soils in primary successional environments with D. drummondii have higher nitrogen and phosphorus, which facilitates growth of spruce seedlings (F. S. Chapin III et al. 1994). In late successional stages, D. drummondii is inhibited by alders by shading and from (possible) allelopathic effects (Chapin et al.).</discussion>
  <discussion>In the key and descriptions below, feathery hairs, referred to by some authors as “octopetala scales,” consist of relatively thick brown axes bearing delicate white hairs, appearing featherlike (E. Hultén 1959b). The brown 'axes' of Hultén are analogous to a feather rachis. Such hairs may be absent from rare individuals in species that normally have them. Normally glandular species may include rare eglandular individuals.</discussion>
  <references>
    <reference>Hultén, E. 1959b. Studies in the genus Dryas. Svensk Bot. Tidskr. 53: 507–542.</reference>
    <reference>Porsild, A. E. 1947. The genus Dryas in North America. Canad. Field-Naturalist 61: 175–192.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals yellow, erect; leaf blades: bases usually cuneate (sometimes truncate or cordate); basal leaflets 1(–2); leaf surfaces tomentose; flowers nodding at flowering; filaments hairy.</description>
      <determination>1 Dryas drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually white or cream, sometimes yellow, spreading; leaf blades: bases usually truncate or cordate, sometimes cuneate or rounded; basal leaflets 0; leaf abaxial surfaces tomentose to woolly, adaxial glabrous or glabrate, sometimes tomentose; flowers erect at flowering; filaments glabrous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: margins usually entire or dentate to crenate, sometimes serrate in proximal 1/2; (plants eglandular).</description>
      <determination>7 Dryas integrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: margins serrate to crenate or dentate (sometimes almost pinnatifid); (plants usually glandular, sometimes eglandular)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Feathery hairs usually present, especially abaxially on midveins</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Feathery hairs usually absent, rarely 1+ feathery hairs abaxially on midveins in D. hookeriana and D. incisa</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Feathery hairs and stipitate glands abundant along midveins abaxially; nw North America.</description>
      <determination>5 Dryas ajanensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Feathery hairs and stipitate glands scattered to rare along midveins abaxially; e Greenland.</description>
      <determination>6 Dryas octopetala</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades: sinuses of margins 45–60% to midvein, apices rounded to obtuse, surfaces smooth to rugose, plicate (midvein and lateral veins adaxially sunken into folds).</description>
      <determination>3 Dryas alaskensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades: sinuses of margins 5–30% to midvein, apices acute to slightly obtuse, surfaces smooth to rugulose or slightly plicate, (margins strongly revolute to flat, only midvein ± obscured adaxially within fold)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Midveins and petioles not stipitate-glandular abaxially.</description>
      <determination>2 Dryas incisa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Midveins and petioles stipitate-glandular abaxially.</description>
      <determination>4 Dryas hookeriana</determination>
    </key_statement>
  </key>
</bio:treatment>