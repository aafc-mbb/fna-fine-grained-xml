<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan S. Weakley</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">POTERIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 994. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 430. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus POTERIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek poterion, goblet or cup, presumably alluding to shape of hypanthium</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">126639</other_info_on_name>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Salad burnet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial [unarmed or thorny shrubs], 1–8 [–40] dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous [taprooted].</text>
      <biological_entity id="o3324" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, ascending to erect, glabrescent to loosely villous [glabrous].</text>
      <biological_entity id="o3325" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s2" to="loosely villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous [persistent], basal and/or cauline;</text>
      <biological_entity id="o3326" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="basal cauline" id="o3328" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>stipules persistent, adnate to petiole or free, lanceolate to ovate, margins entire or serrate;</text>
      <biological_entity id="o3329" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="to " constraintid="o3331" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o3330" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o3331" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character char_type="range_value" constraint="to " constraintid="o3333" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
      </biological_entity>
      <biological_entity id="o3332" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o3333" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present;</text>
      <biological_entity id="o3334" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to obovate [or elliptic], 1–15 [–35] cm, herbaceous [subcoriaceous], leaflets 2–12 pairs, elliptic, orbiculate, or obovate, margins flat, crenate, serrate, or incised, surfaces glabrous or hairy hairs unicellular to multicellular, acicular and/or gland-tipped.</text>
      <biological_entity id="o3335" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="35" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3336" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o3337" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o3338" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o3339" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="unicellular" name="architecture" src="d0_s6" to="multicellular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal [or axillary] to distal leaves, [15–] 20–100+-flowered, headlike spikes, glabrous or sparsely [to densely] pubescent;</text>
      <biological_entity id="o3340" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="[15-]20-100+-flowered" value_original="[15-]20-100+-flowered" />
      </biological_entity>
      <biological_entity id="o3341" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o3342" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="headlike" value_original="headlike" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles present, sometimes absent;</text>
      <biological_entity id="o3343" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts absent;</text>
      <biological_entity id="o3344" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles present.</text>
      <biological_entity id="o3345" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels absent.</text>
      <biological_entity id="o3346" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers bisexual or pistillate (plants gynomonoecious [dioecious]), 2–4 [–5] mm diam.;</text>
      <biological_entity id="o3347" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium dry [fleshy, spongy], top-shaped, ellipsoid, or nearly orbicular, 2–9 mm, angled, winged, and/or ornamented with irregular flanges and papillae, tan to brown, hairy or glabrous;</text>
      <biological_entity id="o3348" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="condition_or_texture" src="d0_s13" value="dry" value_original="dry" />
        <character is_modifier="false" name="shape" src="d0_s13" value="top--shaped" value_original="top--shaped" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s13" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s13" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="angled" value_original="angled" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character constraint="with papillae" constraintid="o3350" is_modifier="false" name="relief" src="d0_s13" value="ornamented" value_original="ornamented" />
        <character char_type="range_value" from="tan" name="coloration" notes="" src="d0_s13" to="brown" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3349" name="flange" name_original="flanges" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s13" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity id="o3350" name="papilla" name_original="papillae" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>sepals 4, distinct [basally connate], usually ascending or spreading, rarely slightly reflexed, ovate to elliptic;</text>
      <biological_entity id="o3351" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="rarely slightly" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s14" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals 0;</text>
      <biological_entity id="o3352" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 0–30 (–50), shorter or longer than sepals;</text>
      <biological_entity id="o3353" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="50" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s16" to="30" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character constraint="than sepals" constraintid="o3354" is_modifier="false" name="length_or_size" src="d0_s16" value="shorter or longer" value_original="shorter or longer" />
      </biological_entity>
      <biological_entity id="o3354" name="sepal" name_original="sepals" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>carpels [1 or] 2 (or 3), glabrous, styles repeatedly branched, brushlike.</text>
      <biological_entity id="o3355" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3356" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="repeatedly" name="architecture" src="d0_s17" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="brushlike" value_original="brushlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits achenes, 1 or 2 [or 3], ellipsoid, 1.5–2 mm diam., glabrous (though reticulate and muricate);</text>
      <biological_entity constraint="fruits" id="o3357" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s18" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s18" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>hypanthium persistent, enclosing achenes;</text>
      <biological_entity id="o3358" name="hypanthium" name_original="hypanthium" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o3359" name="achene" name_original="achenes" src="d0_s19" type="structure" />
      <relation from="o3358" id="r203" name="enclosing" negation="false" src="d0_s19" to="o3359" />
    </statement>
    <statement id="d0_s20">
      <text>sepals persistent, spreading to slightly ascending.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 7.</text>
      <biological_entity id="o3360" name="sepal" name_original="sepals" src="d0_s20" type="structure">
        <character is_modifier="false" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s20" to="slightly ascending" />
      </biological_entity>
      <biological_entity constraint="x" id="o3361" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe (especially Mediterranean region), w Asia, n Africa, n Atlantic Islands (Macaronesia); introduced also in s South America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe (especially Mediterranean region)" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="n Atlantic Islands (Macaronesia)" establishment_means="introduced" />
        <character name="distribution" value="also in s South America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 13 (1 in the flora).</discussion>
  <discussion>Poterium is here circumscribed to include Bencomia Webb &amp; Berthelot, Dendriopoterium Sventenius (all of Macaronesia), Marcetella Sventenius, and Sarcopoterium Spach (of the eastern Mediterranean) based on molecular phylogenetic analyses summarized by D. Potter et al. (2007). The inclusion of these woody Mediterranean and Macaronesian endemics significantly increases the morphological diversity in the genus; see also 25. Sanguisorba.</discussion>
  <references>
    <reference>Nordborg, G. 1967. The genus Sanguisorba section Poterium: Experimental studies and taxonomy. Opera Bot. 16: 1–166.</reference>
  </references>
  
</bio:treatment>