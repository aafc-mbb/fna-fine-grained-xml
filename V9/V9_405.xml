<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">HORKELIA</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="1827" rank="species">californica</taxon_name>
    <taxon_name authority="(Greene) Ertter &amp; Reveal" date="2007" rank="variety">elata</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>17: 318. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section horkelia;species californica;variety elata;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100644</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) Greene" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="Greene" date="1891" rank="variety">elata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>1: 66. 1891,</place_in_publication>
      <other_info_on_pub>based on P. elata Greene, Pittonia 1: 100. 1887, not Salisbury 1796</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species californica;variety elata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="(Greene) Rydberg" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(Crum) Ertter" date="unknown" rank="subspecies">dissita</taxon_name>
    <taxon_hierarchy>genus Horkelia;species californica;subspecies dissita</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">elata</taxon_name>
    <taxon_hierarchy>genus H.;species elata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">elata</taxon_name>
    <taxon_name authority="Crum" date="unknown" rank="variety">dissita</taxon_name>
    <taxon_hierarchy>genus P.;species elata;variety dissita</taxon_hierarchy>
  </taxon_identification>
  <number>12b.</number>
  <other_name type="common_name">Tall horkelia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves (5–) 10–25 cm;</text>
      <biological_entity constraint="basal" id="o5215" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheathing bases usually glabrous, sometimes sparsely strigose;</text>
      <biological_entity id="o5216" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lateral leaflets (5–) 7–9 per side, ovate to round, 5–30 mm, cleft 1/2–3/4+ to midrib, ultimate teeth 10–30;</text>
      <biological_entity constraint="lateral" id="o5217" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s2" to="7" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o5218" from="7" name="quantity" src="d0_s2" to="9" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s2" to="round" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o5219" from="1/2" name="quantity" src="d0_s2" to="3/4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o5218" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o5219" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o5220" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet 10–40 mm.</text>
      <biological_entity constraint="terminal" id="o5221" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: epicalyx bractlets usually entire;</text>
      <biological_entity id="o5222" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="epicalyx" id="o5223" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hypanthium interior glabrous;</text>
      <biological_entity id="o5224" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5225" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="interior" value_original="interior" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals not red-mottled adaxially;</text>
      <biological_entity id="o5226" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o5227" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not; adaxially" name="coloration" src="d0_s6" value="red-mottled" value_original="red-mottled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 0.5–1.5 × 0.2–0.5 mm;</text>
      <biological_entity id="o5228" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o5229" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>carpels 50–100;</text>
      <biological_entity id="o5230" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o5231" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s8" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 2–3 mm. 2n = 28.</text>
      <biological_entity id="o5232" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o5233" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5234" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadow edges, along streams, in chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadow edges" constraint="along streams , in chaparral" />
        <character name="habitat" value="streams" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety elata occurs away from the immediate coast in the northern coastal ranges (Contra Costa to Humboldt counties), western foothills of the Sierra Nevada (Butte to Tulare counties), and disjunctly in the San Bernardino Mountains. At its most distinctive, the variety is represented by relatively delicate plants with finely divided leaflets at higher elevations in the North Coast ranges. Populations in the Sierran foothills, including the type of Potentilla elata var. dissita, tend to be relatively coarse and densely villous. Intermediates with var. californica exist in interior mid elevation sites, including Mount Diablo in Contra Costa County and the type locality of Horkelia glandulosa Eastwood in Mendocino County.</discussion>
  
</bio:treatment>