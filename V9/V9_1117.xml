<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">651</other_info_on_meta>
    <other_info_on_meta type="mention_page">442</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="mention_page">649</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">amelanchier</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall ex M. Roemer" date="1847" rank="species">alnifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Nat. Syn. Monogr.</publication_title>
      <place_in_publication>3: 147. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus amelanchier;species alnifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100018</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aronia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">alnifolia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 306. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aronia;species alnifolia</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Alder-leaved shadbush</other_name>
  <other_name type="common_name">saskatoon</other_name>
  <other_name type="common_name">saskatoon berry</other_name>
  <other_name type="common_name">amélanchier à feuilles d'aulne</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–12 m.</text>
      <biological_entity id="o1580" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="12" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–50, solitary or colonial.</text>
      <biological_entity id="o1581" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="50" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="colonial" value_original="colonial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly unfolded;</text>
      <biological_entity id="o1582" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (3–) 6.8–19.1 (–28) mm;</text>
      <biological_entity id="o1583" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="19.1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="28" to_unit="mm" />
        <character char_type="range_value" from="6.8" from_unit="mm" name="some_measurement" src="d0_s3" to="19.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually elliptic to oval to suborbiculate, sometimes quadrangular, (14–) 24–47 (–67) × (7–) 17–36 (–55) mm, base usually subcordate to truncate, sometimes ± tapering or ± cuneate, each margin with 0–3 (–9) teeth on proximal 1/2 and (0–) 3–5 (–8) teeth in distalmost cm, largest teeth more than 1 mm, apex rounded to truncate or occasionally acute or mucronate, abaxial surface sparsely to densely hairy (or glabrous) by flowering, sparsely to moderately hairy (or glabrous) later, adaxial glabrous or sparsely (moderately) hairy later.</text>
      <biological_entity id="o1584" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually elliptic" name="shape" src="d0_s4" to="oval" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="quadrangular" value_original="quadrangular" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_length" src="d0_s4" to="24" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="47" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="67" to_unit="mm" />
        <character char_type="range_value" from="24" from_unit="mm" name="length" src="d0_s4" to="47" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s4" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="36" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="55" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s4" to="36" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1585" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually subcordate" name="shape" src="d0_s4" to="truncate" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o1586" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o1587" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1588" name="1/2" name_original="1/2" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o1589" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="distalmost" id="o1590" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="largest" id="o1591" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1592" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate or occasionally acute or mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1593" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="condition" src="d0_s4" value="later" value_original="later" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1594" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="condition" src="d0_s4" value="later" value_original="later" />
      </biological_entity>
      <relation from="o1586" id="r106" name="with" negation="false" src="d0_s4" to="o1587" />
      <relation from="o1587" id="r107" name="on" negation="false" src="d0_s4" to="o1588" />
      <relation from="o1589" id="r108" name="in" negation="false" src="d0_s4" to="o1590" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (4–) 6–11 (–16) -flowered, (8–) 14–43 (–62) mm.</text>
      <biological_entity id="o1595" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(4-)6-11(-16)-flowered" value_original="(4-)6-11(-16)-flowered" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="14" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="43" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="62" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s5" to="43" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: (0 or) 1 or 2 (or 3) subtended by a leaf, proximalmost (2–) 3–20 (–29) mm.</text>
      <biological_entity id="o1596" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or subtended" value="1" value_original="1" />
        <character constraint="by leaf" constraintid="o1597" name="quantity" src="d0_s6" unit="or subtended" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o1597" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity constraint="proximalmost" id="o1598" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="29" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals erect to recurved after flowering, (1.4–) 2.2–4 (–4.9) mm;</text>
      <biological_entity id="o1599" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o1600" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="after flowering" from="erect" name="orientation" src="d0_s7" to="recurved" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4.9" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblanceolate to oval or obovate to elliptic, (5.7–) 9.5–14 (–18.8) × (2.2–) 3.3–5.2 (–6.6) mm;</text>
      <biological_entity id="o1601" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o1602" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="oval or obovate" />
        <character char_type="range_value" from="5.7" from_unit="mm" name="atypical_length" src="d0_s8" to="9.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="18.8" to_unit="mm" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="atypical_width" src="d0_s8" to="3.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="6.6" to_unit="mm" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="width" src="d0_s8" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens (10–) 15–21 (–22);</text>
      <biological_entity id="o1603" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1604" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s9" to="15" to_inclusive="false" />
        <character char_type="range_value" from="21" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="22" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles (3 or) 4 or 5 (or 6), (1.3–) 2–2.9 (–3.9) mm;</text>
      <biological_entity id="o1605" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1606" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s10" unit="or" value="5" value_original="5" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3.9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary apex moderately to densely hairy (or glabrous).</text>
      <biological_entity id="o1607" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="ovary" id="o1608" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pomes black or purple, 8–15 mm diam.</text>
      <biological_entity id="o1609" name="pome" name_original="pomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Iowa, Kans., Minn., Mont., N.Dak., N.Mex., Nebr., Nev., Oreg., S.Dak., Tex., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Amelanchier alnifolia is widespread and polymorphic, and its taxonomic and geographic limits have been viewed differently. L. Cinq-Mars (1971) considered this species to range eastward to the Gaspé Peninsula. Disagreements about the boundary between A. alnifolia and A. humilis are evident in herbarium specimen annotations. The distinctness of the varieties of A. alnifolia has also been questioned. G. N. Jones (1946) treated the three varieties recognized here as distinct species and noted that the leaves of var. alnifolia and var. semiintegrifolia (A. florida) are virtually indistinguishable, and, although petal lengths of the two do not overlap, occasional larger-flowered var. alnifolia and occasional smaller-flowered var. semiintegrifolia occur, so the petal length distinction is not an absolute one. Geographically these two varieties are largely separate, with var. alnifolia occurring in the Great Plains and Rocky Mountains and var. semiintegrifolia on the Pacific slopes of mountains from Alaska to northern California.</discussion>
  <discussion>Amelanchier alnifolia is thought to hybridize with Sorbus scopulina (x\Amelasorbus jackii Rehder).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary apices glabrous (or sparsely hairy); shrubs 1–2(–4) m.</description>
      <determination>4c Amelanchier alnifolia var. pumila</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovary apices moderately to densely hairy (or glabrous); shrubs or trees, 1–12 m</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences (8–)26–43(–62) mm; proximalmost pedicels (5–)8–16(–29) mm.</description>
      <determination>4a Amelanchier alnifolia var. alnifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences (8–)14–26(–35) mm; proximalmost pedicels (2 or)3–8(–13) mm.</description>
      <determination>4b Amelanchier alnifolia var. semiintegrifolia</determination>
    </key_statement>
  </key>
</bio:treatment>