<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="A. Gray" date="1872" rank="species">pisocarpa</taxon_name>
    <taxon_name authority="Ertter &amp; W. H. Lewis" date="2008" rank="subspecies">ahartii</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>55: 171, fig. 1. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species pisocarpa;subspecies ahartii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100508</other_info_on_name>
  </taxon_identification>
  <number>26b.</number>
  <other_name type="common_name">Ahart's rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rarely forming thickets.</text>
      <biological_entity id="o33904" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o33905" name="thicket" name_original="thickets" src="d0_s0" type="structure" />
      <relation from="o33904" id="r2241" name="forming" negation="false" src="d0_s0" to="o33905" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually solitary or loosely clustered, (2–) 4–14 (–18) dm;</text>
    </statement>
    <statement id="d0_s2">
      <text>infrastipular prickles 0 or 1 (or 2), 2–5 mm.</text>
      <biological_entity id="o33906" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="loosely; loosely" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="18" to_unit="dm" />
        <character char_type="range_value" from="4" from_unit="dm" name="some_measurement" src="d0_s1" to="14" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o33907" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character name="presence" src="d0_s2" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s2" unit="or" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (5–) 6–11 (–13) cm;</text>
      <biological_entity id="o33908" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 5–7, most commonly 5, terminal blade 20–45 (–60) mm.</text>
      <biological_entity id="o33909" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="7" />
        <character modifier="commonly" name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o33910" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–3 (–10+) -flowered.</text>
      <biological_entity id="o33911" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-3(-10+)-flowered" value_original="1-3(-10+)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepal tip to 7 mm, abaxial surfaces usually eglandular, rarely stipitate-glandular;</text>
      <biological_entity id="o33912" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="sepal" id="o33913" name="tip" name_original="tip" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o33914" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>carpels 32–34.</text>
      <biological_entity id="o33915" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o33916" name="carpel" name_original="carpels" src="d0_s7" type="structure">
        <character char_type="range_value" from="32" name="quantity" src="d0_s7" to="34" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Hips subglobose to ovoid, 8–13 mm diam., gradually to abruptly narrowed to neck 2.5–3.5 mm diam.</text>
      <biological_entity id="o33917" name="hip" name_original="hips" src="d0_s8" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s8" to="ovoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s8" to="13" to_unit="mm" />
        <character constraint="to neck" constraintid="o33918" is_modifier="false" modifier="gradually to abruptly" name="shape" src="d0_s8" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o33918" name="neck" name_original="neck" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 5–20.2n = 28.</text>
      <biological_entity id="o33919" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
      <biological_entity constraint="2n" id="o33920" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streamsides, meadow margins, roadsides in woodlands, seasonally moist areas in openings of midmontane forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="meadow margins" />
        <character name="habitat" value="roadsides" constraint="in woodlands , seasonally moist areas in openings of midmontane forests" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="moist areas" modifier="seasonally" constraint="in openings of midmontane forests" />
        <character name="habitat" value="openings" constraint="of midmontane forests" />
        <character name="habitat" value="midmontane forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies ahartii comprises populations from the northern Sierra Nevada and southern Cascade Range (B. Ertter and W. H. Lewis 2008). Stems tend to have relatively few (or no) prickles, relatively large ovate leaves, and relatively few flowers.</discussion>
  
</bio:treatment>