<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Engelmann ex A. Gray" date="1850" rank="species">minutiflora</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 185. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species minutiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100398</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Texas almond</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, suckering, much branched, 10–20 dm, weakly thorny.</text>
      <biological_entity id="o11967" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with axillary end buds, canescent.</text>
      <biological_entity id="o11968" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="end" id="o11969" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="axillary end" />
      <relation from="o11968" id="r744" name="with" negation="false" src="d0_s1" to="o11969" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o11970" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–2 (–6) mm, glabrous, eglandular;</text>
      <biological_entity id="o11971" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic or obovate, 0.5–1.6 (–3.5) × 0.3–0.8 (–2.1) cm, base cuneate, margins usually entire, sometimes irregularly serrulate (sometimes dentate on long-shoots), teeth sharp to blunt, eglandular, some callus-tipped, apex usually obtuse to rounded, sometimes apiculate, surfaces glabrous.</text>
      <biological_entity id="o11972" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="2.1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="0.8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11973" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o11974" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes irregularly" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o11975" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="sharp" name="shape" src="d0_s4" to="blunt" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="callus-tipped" value_original="callus-tipped" />
      </biological_entity>
      <biological_entity id="o11976" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually obtuse" name="shape" src="d0_s4" to="rounded" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o11977" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers.</text>
      <biological_entity id="o11978" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o11979" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0–2 mm, puberulent.</text>
      <biological_entity id="o11980" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, plants dioecious, blooming at leaf emergence;</text>
      <biological_entity id="o11981" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o11982" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioecious" value_original="dioecious" />
        <character constraint="at leaf" constraintid="o11983" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11983" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium campanulate, 2–3 mm, glabrous externally;</text>
      <biological_entity id="o11984" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals spreading, triangular, 0.7–1.5 mm, margins entire, surfaces glabrous;</text>
      <biological_entity id="o11985" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11986" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11987" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, obovate, 2–3.5 mm;</text>
      <biological_entity id="o11988" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries hairy.</text>
      <biological_entity id="o11989" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes reddish-brown, globose to ovoid, 9–12 mm, puberulent;</text>
      <biological_entity id="o11990" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s12" to="ovoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium tardily deciduous;</text>
      <biological_entity id="o11991" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mesocarps leathery to dry (slightly splitting);</text>
      <biological_entity id="o11992" name="mesocarp" name_original="mesocarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="leathery" name="texture" src="d0_s14" to="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stones ovoid to subglobose, not flattened.</text>
      <biological_entity id="o11993" name="stone" name_original="stones" src="d0_s15" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s15" to="subglobose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Mar; fruiting May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky streambeds and uplands, limestone hills, ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry rocky streambeds" />
        <character name="habitat" value="uplands" />
        <character name="habitat" value="limestone hills" />
        <character name="habitat" value="ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Prunus minutiflora is a rare species limited to central Texas around the Edwards Plateau.</discussion>
  
</bio:treatment>