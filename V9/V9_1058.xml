<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">616</other_info_on_meta>
    <other_info_on_meta type="mention_page">612</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="2007" rank="series">Apricae</taxon_name>
    <taxon_name authority="Beadle" date="1900" rank="species">alleghaniensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>30: 337. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series apricae;species alleghaniensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100058</other_info_on_name>
  </taxon_identification>
  <number>123.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 20–40 (–50) dm.</text>
      <biological_entity id="o34052" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs ± flexuous, new growth reddish, glabrous, 1-year old reddish gray, older gray;</text>
      <biological_entity id="o34053" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o34054" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s1" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity id="o34055" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish gray" value_original="reddish gray" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs straight or slightly recurved, 2-years old deep chestnut-brown to blackish, fine, 1.5–4 cm.</text>
      <biological_entity id="o34056" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o34057" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="deep" value_original="deep" />
        <character char_type="range_value" from="chestnut-brown" name="coloration" src="d0_s2" to="blackish" />
        <character is_modifier="false" name="width" src="d0_s2" value="fine" value_original="fine" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o34058" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o34057" id="r2243" name="on" negation="false" src="d0_s2" to="o34058" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole length 25–30% blade, glabrous, densely sessile-glandular;</text>
      <biological_entity id="o34059" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o34060" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="25-30%" name="length" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s3" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o34061" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade elliptic-ovate to rhombic-ovate, (2–) 3–5 cm, widest towards base, thin, base broadly cuneate, lobes 3 per side, sinuses short to moderately deep, lobe apex acute to acuminate, margins serrulate, teeth with small glands, veins 5 per side, apex acute, abaxial surface glabrous, adaxial pilose young, soon glabrescent.</text>
      <biological_entity id="o34062" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o34063" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s4" to="rhombic-ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character constraint="towards base" constraintid="o34064" is_modifier="false" name="width" src="d0_s4" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o34064" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s4" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o34065" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o34066" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character constraint="per side" constraintid="o34067" name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o34067" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o34068" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" modifier="moderately" name="depth" src="d0_s4" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o34069" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity id="o34070" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o34071" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o34072" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o34073" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character constraint="per side" constraintid="o34074" name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o34074" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o34075" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o34076" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34077" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <relation from="o34071" id="r2244" name="with" negation="false" src="d0_s4" to="o34072" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–4-flowered;</text>
      <biological_entity id="o34078" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-4-flowered" value_original="2-4-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous;</text>
      <biological_entity id="o34079" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles caducous, linear, margins glandular.</text>
      <biological_entity id="o34080" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o34081" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 20 mm diam.;</text>
      <biological_entity id="o34082" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character name="diameter" src="d0_s8" unit="mm" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium glabrous;</text>
      <biological_entity id="o34083" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals narrowly triangular, 5 mm, margins glandular-serrate, abaxially glabrous;</text>
      <biological_entity id="o34084" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o34085" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="glandular-serrate" value_original="glandular-serrate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 10, anthers pink;</text>
      <biological_entity id="o34086" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o34087" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 4.</text>
      <biological_entity id="o34088" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes red, orbicular-pyriform, 8–12 mm diam., glabrous;</text>
      <biological_entity id="o34089" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="orbicular-pyriform" value_original="orbicular-pyriform" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals spreading;</text>
      <biological_entity id="o34090" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 2–5.</text>
      <biological_entity id="o34091" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hills, brush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hills" />
        <character name="habitat" value="brush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus alleghaniensis is known from a few locations in northeastern Alabama, Georgia, and Tennessee, and is apparently scarce.</discussion>
  
</bio:treatment>