<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">459</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="(Franchet) G. Klotz" date="1957" rank="species">cochleatus</taxon_name>
    <place_of_publication>
      <publication_title>Wiss. Z. Martin-Luther-Univ. Halle-Wittenberg, Math.-Naturwiss. Reihe</publication_title>
      <place_in_publication>6: 952. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species cochleatus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242315051</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotoneaster</taxon_name>
    <taxon_name authority="Wallich ex Lindley forma cochleatus Franchet" date="unknown" rank="species">buxifolius</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Delavay.,</publication_title>
      <place_in_publication>224. 1890 (as buxifolia forma cochleata)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotoneaster;species buxifolius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Wallich ex Lindley" date="unknown" rank="species">microphyllus</taxon_name>
    <taxon_name authority="(Franchet) Rehder &amp; E. H. Wilson" date="unknown" rank="variety">cochleatus</taxon_name>
    <taxon_hierarchy>genus C.;species microphyllus;variety cochleatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Baker" date="unknown" rank="species">thymifolius</taxon_name>
    <taxon_name authority="(Franchet) Franchet" date="unknown" rank="variety">cochleatus</taxon_name>
    <taxon_hierarchy>genus C.;species thymifolius;variety cochleatus</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Yunnan cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 0.4 m.</text>
      <biological_entity id="o6709" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="0.4" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate or nearly so, carpeting, rooting;</text>
      <biological_entity id="o6710" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character name="growth_form_or_orientation" src="d0_s1" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches spiraled and distichous, dense, red to purple-black, slender, initially yellow-green strigose.</text>
      <biological_entity id="o6711" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s2" to="purple-black" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="initially" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent;</text>
      <biological_entity id="o6712" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–4 mm, strigose;</text>
      <biological_entity id="o6713" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obovate to broadly obovate, rarely suborbiculate, 5–14 × 3–9 mm, coriaceous, base obtuse or broadly cuneate, margins slightly revolute, veins 2 or 3, superficial, apex obtuse, sometimes emarginate, abaxial surfaces grayish, reticulate, initially densely strigose-villous, adaxial dark green, shiny, not glaucous, sometimes lightly rugose, glabrescent.</text>
      <biological_entity id="o6714" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="broadly obovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="9" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o6715" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6716" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o6717" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o6718" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6719" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s5" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="initially densely" name="pubescence" src="d0_s5" value="strigose-villous" value_original="strigose-villous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6720" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="sometimes lightly" name="relief" src="d0_s5" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences on fertile shoots 8–15 mm, usually with 4 leaves, 1 (–3) -flowered.</text>
      <biological_entity id="o6721" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="1(-3)-flowered" value_original="1(-3)-flowered" />
      </biological_entity>
      <biological_entity id="o6722" name="shoot" name_original="shoots" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6723" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o6721" id="r412" name="on" negation="false" src="d0_s6" to="o6722" />
      <relation from="o6721" id="r413" modifier="usually" name="with" negation="false" src="d0_s6" to="o6723" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 3–5 mm, strigose.</text>
      <biological_entity id="o6724" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 7–10 mm diam.;</text>
      <biological_entity id="o6725" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>buds pinkish;</text>
      <biological_entity id="o6726" name="bud" name_original="buds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium cupulate, strigose or pilose-strigose;</text>
      <biological_entity id="o6727" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins villous, apex obtuse or acute, surfaces initially sparsely pilose-strigose;</text>
      <biological_entity id="o6728" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o6729" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6730" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o6731" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="initially sparsely" name="pubescence" src="d0_s11" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals spreading, white, glabrous;</text>
      <biological_entity id="o6732" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o6733" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens (15–) 20, filaments white, anthers dark purple;</text>
      <biological_entity id="o6734" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o6735" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s13" to="20" to_inclusive="false" />
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o6736" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o6737" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2 (or 3).</text>
      <biological_entity id="o6738" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o6739" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes bright red to crimson, subglobose, 7–9 × 8–10 mm, slightly shiny, not glaucous, sparsely pilose;</text>
      <biological_entity id="o6740" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character char_type="range_value" from="bright red" name="coloration" src="d0_s15" to="crimson" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s15" to="9" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s15" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals suberect, sparsely strigose;</text>
    </statement>
    <statement id="d0_s17">
      <text>navel slightly open;</text>
      <biological_entity id="o6741" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants at apex on small projection.</text>
      <biological_entity constraint="style" id="o6742" name="remnant" name_original="remnants" src="d0_s18" type="structure" />
      <biological_entity id="o6743" name="apex" name_original="apex" src="d0_s18" type="structure" />
      <biological_entity id="o6744" name="projection" name_original="projection" src="d0_s18" type="structure">
        <character is_modifier="true" name="size" src="d0_s18" value="small" value_original="small" />
      </biological_entity>
      <relation from="o6742" id="r414" name="at" negation="false" src="d0_s18" to="o6743" />
      <relation from="o6743" id="r415" name="on" negation="false" src="d0_s18" to="o6744" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes 2 (or 3).</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 68 (Germany).</text>
      <biological_entity id="o6745" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6746" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forest edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.Y.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cotoneaster cochleatus was treated as a variety of C. microphyllus by L. Lingdi and A. R. Brach (2003); here the two are distinguished at species rank, following H. Nybom et al. (2005). Plants of C. microphyllus have a suberect habit, usually elliptic leaves (rarely broadly obovate) with acute apices, and pomes 6 mm wide; C. cochleatus is always prostrate and has usually obovate leaves (rarely suborbiculate) with blunt apices, and pomes 8–10 mm wide.</discussion>
  
</bio:treatment>