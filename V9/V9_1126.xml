<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">655</other_info_on_meta>
    <other_info_on_meta type="mention_page">649</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="mention_page">661</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">amelanchier</taxon_name>
    <taxon_name authority="E. L. Nielsen" date="1939" rank="species">interior</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>22: 185, plate 13. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus amelanchier;species interior</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100025</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amelanchier</taxon_name>
    <taxon_name authority="E. L. Nielsen" date="unknown" rank="species">wiegandii</taxon_name>
    <taxon_hierarchy>genus Amelanchier;species wiegandii</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Wiegand’s shadbush</other_name>
  <other_name type="common_name">amélanchier de l'intérieur</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1–10 m.</text>
      <biological_entity id="o25339" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10, forming colonies or solitary.</text>
      <biological_entity id="o25341" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves not fully unfolded;</text>
      <biological_entity id="o25342" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 10–30 mm;</text>
      <biological_entity id="o25343" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate to elliptic, 30–70 × 20–50 mm, base rounded to subcordate, each margin with 3–15 teeth on proximal 1/2 and (2–) 4 or 5 (–7) teeth in distalmost cm, largest teeth less than 1 mm, apex short acuminate to apiculate, abaxial surface glabrous or sparsely hairy by flowering, surfaces glabrous later.</text>
      <biological_entity id="o25344" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25345" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o25346" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o25347" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o25348" name="1/2" name_original="1/2" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" unit="or teeth" />
        <character name="quantity" src="d0_s4" unit="or teeth" value="4" value_original="4 " />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7" unit="or teeth" />
        <character constraint="in distalmost teeth" constraintid="o25349" name="quantity" src="d0_s4" unit="or teeth" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o25349" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="largest" id="o25350" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character modifier="less than" name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25351" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s4" to="apiculate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25352" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="by flowering" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o25353" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="condition" src="d0_s4" value="later" value_original="later" />
      </biological_entity>
      <relation from="o25346" id="r1660" name="with" negation="false" src="d0_s4" to="o25347" />
      <relation from="o25347" id="r1661" name="on" negation="false" src="d0_s4" to="o25348" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 4–12-flowered, 30–75 mm.</text>
      <biological_entity id="o25354" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-12-flowered" value_original="4-12-flowered" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s5" to="75" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: 1 or 2 subtended by a leaf, proximalmost 10–45 mm.</text>
      <biological_entity id="o25355" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or subtended" value="1" value_original="1" />
        <character constraint="by leaf" constraintid="o25356" name="quantity" src="d0_s6" unit="or subtended" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25356" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity constraint="proximalmost" id="o25357" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals recurved after flowering, 2–5 mm;</text>
      <biological_entity id="o25358" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o25359" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="after flowering" is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals obovate, 6–15 × 4–5 mm;</text>
      <biological_entity id="o25360" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o25361" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 20;</text>
      <biological_entity id="o25362" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o25363" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 5, 3–5 mm;</text>
      <biological_entity id="o25364" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o25365" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary apex densely (moderately) hairy (or glabrous).</text>
      <biological_entity id="o25366" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="ovary" id="o25367" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pomes purple-black, 6–8 mm diam. 2n = 4x.</text>
      <biological_entity id="o25368" name="pome" name_original="pomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple-black" value_original="purple-black" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25369" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="x" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry woods, bluffs rocky areas and slopes, stream banks, fields, thickets, and sandy areas, sometimes wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry woods" />
        <character name="habitat" value="bluffs rocky areas" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="sandy areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ill., Iowa, Mich., Minn., Ohio, S.Dak., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Amelanchier interior is distinguished by its capacity to grow into a tree to ten meters and by having sparsely hairy young leaves that are often reddish and densely hairy ovary apices. E. L. Nielsen (1939) differentiated A. interior and A. wiegandii on the basis of leaves being carinate in A. wiegandii as opposed to flat in A. interior, and leaf sinuses rounded versus acute. G. N. Jones (1946) considered those differences to be slight, and he included A. wiegandii in A. interior. The authors follow Jones, but retain the common name Wiegand’s shadbush to commemorate Wiegand’s early insights about the taxonomy of Amelanchier. M. L. Fernald (1950) considered A. wiegandii as suggesting a small-leaved A. laevis but with shorter buds, fewer teeth, fewer veins, and summit of ovary heavily tomentose. P. Landry (1975) thought A. wiegandii to be the hybrid of A. arborea and A. sanguinea, and E. G. Voss (1972–1996, vol. 2) reasoned that A. interior is a hybrid swarm involving A. laevis (or sometimes A. arborea) and plants of the A. spicata and/or A. sanguinea complex. A hybrid origin of A. interior from A. laevis and A. sanguinea is reasonable given that stem height, the number of leaf teeth, and petal length are more or less intermediate between those two species. DNA sequences from ITS region indicate that A. interior (A. wiegandii) is a possible later-generation hybrid involving a member of the western North American ITS clade (which also includes A. humilis and A. sanguinea of eastern North America) and some eastern North American taxon (C. S. Campbell et al. 1997). Multiple hybrid origins, possibly from different species, may explain the variability of A. interior. Amelanchier interior has unusually large ranges of lengths for proximal pedicels, sepals, and petals. The leaves of A. wiegandii were described by Nielsen as bronze at flowering; Jones described the leaves of A. interior as green when young. The authors assume that A. interior as they interpret it is polymorphic for the color of young leaves.</discussion>
  
</bio:treatment>