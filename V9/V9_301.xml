<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Niveae</taxon_name>
    <taxon_name authority="(Turczaninow) Juzepczuk in V. L. Komarov et al." date="1941" rank="species">arenosa</taxon_name>
    <place_of_publication>
      <publication_title>in V. L. Komarov et al., Fl. URSS</publication_title>
      <place_in_publication>10: 137. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section niveae;species arenosa;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100303</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nivea</taxon_name>
    <taxon_name authority="Turczaninow" date="unknown" rank="variety">arenosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>16: 607. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species nivea;variety arenosa</taxon_hierarchy>
  </taxon_identification>
  <number>75.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scarcely to ± tufted.</text>
      <biological_entity id="o12786" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="scarcely to more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudex branches thick, not columnar, not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o12787" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s1" value="columnar" value_original="columnar" />
        <character constraint="with leaves" constraintid="o12788" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o12788" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, (0.3–) 0.8–2.5 (–4.5) dm, lengths (2–) 3–5 times basal leaves.</text>
      <biological_entity id="o12789" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.8" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s2" to="2.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o12790" is_modifier="false" name="length" src="d0_s2" value="(2-)3-5 times basal leaves" value_original="(2-)3-5 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12790" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 1.5–12 (–20) cm;</text>
      <biological_entity constraint="basal" id="o12791" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–7 (–15) cm, long hairs sparse to abundant, spreading to ± ascending, rarely loosely appressed, 1–2 (–2.5) mm, usually stiff, sometimes weak (subsp. chamissonis), verrucose, short and/or crisped hairs absent or sparse to abundant, cottony hairs absent, glands absent or sparse;</text>
      <biological_entity id="o12792" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12793" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="abundant" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="more or less ascending" />
        <character is_modifier="false" modifier="rarely loosely" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="sometimes" name="fragility" src="d0_s4" value="weak" value_original="weak" />
        <character is_modifier="false" name="relief" src="d0_s4" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o12794" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
      <biological_entity id="o12795" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12796" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets separate to ± overlapping, central obovate, 1–3.5 (–4.5) × 0.5–2 (–3) cm, usually petiolulate, petiolule to 5 mm, base cuneate, margins slightly revolute, distal ± 3/4 incised ± 1/2 to midvein, teeth (2–) 3–4 (–6) per side, ± approximate to distant, surfaces dissimilar, often strongly so, abaxial white to gray, long hairs 0.5–1.8 mm, cottony-crisped hairs ± dense, adaxial green, sometimes grayish green, long hairs sparse to abundant, short-crisped hairs sparse to abundant.</text>
      <biological_entity id="o12797" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="separate" name="arrangement" src="d0_s5" to="more or less overlapping" />
      </biological_entity>
      <biological_entity constraint="central" id="o12798" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o12799" name="petiolule" name_original="petiolule" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12800" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o12801" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="more or less" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s5" value="3/4" value_original="3/4" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character constraint="to midvein" constraintid="o12802" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o12802" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o12803" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" constraint="per side" constraintid="o12804" from="3" name="quantity" src="d0_s5" to="4" />
        <character char_type="range_value" from="less approximate" name="arrangement" notes="" src="d0_s5" to="distant" />
      </biological_entity>
      <biological_entity id="o12804" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o12805" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o12806" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" modifier="often strongly; strongly" name="coloration" src="d0_s5" to="gray" />
      </biological_entity>
      <biological_entity id="o12807" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12808" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12809" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
      </biological_entity>
      <biological_entity id="o12810" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="abundant" />
      </biological_entity>
      <biological_entity id="o12811" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="short-crisped" value_original="short-crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (0–) 1–2.</text>
      <biological_entity constraint="cauline" id="o12812" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence 1–7 (–15) -flowered.</text>
      <biological_entity id="o12813" name="inflorescence" name_original="inflorescence" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-7(-15)-flowered" value_original="1-7(-15)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1.5–5 cm in flower, to 6 (–10) cm in fruit.</text>
      <biological_entity id="o12814" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in flower" constraintid="o12815" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s8" to="10" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o12816" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12815" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o12816" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets linear-lanceolate to lanceolate-elliptic, 2–5 (–7) × 0.4–1.2 (–1.5) mm, 1/4–1/2 as wide as sepals, margins usually flat, red-glands absent or sparse and inconspicuous;</text>
      <biological_entity id="o12817" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o12818" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s9" to="lanceolate-elliptic" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="1.2" to_unit="mm" />
        <character char_type="range_value" constraint="as-wide-as sepals" constraintid="o12819" from="1/4" name="quantity" src="d0_s9" to="1/2" />
      </biological_entity>
      <biological_entity id="o12819" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o12820" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o12821" name="red-gland" name_original="red-glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 3–5 mm diam.;</text>
      <biological_entity id="o12822" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12823" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 3–6 (–8) mm, apex acute;</text>
      <biological_entity id="o12824" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12825" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12826" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 4–7 (–10) × 4–7 (–9) mm, ± longer than sepals;</text>
      <biological_entity id="o12827" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12828" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character constraint="than sepals" constraintid="o12829" is_modifier="false" name="length_or_size" src="d0_s12" value="more or less longer" value_original="more or less longer" />
      </biological_entity>
      <biological_entity id="o12829" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.8–1 mm, anthers 0.4 mm;</text>
      <biological_entity id="o12830" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o12831" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12832" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 28–40, apical hairs absent, styles conic-columnar, strongly papillate-swollen in proximal 1/5–1/3, 1–1.5 mm.</text>
      <biological_entity id="o12833" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o12834" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s14" to="40" />
      </biological_entity>
      <biological_entity constraint="apical" id="o12835" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12836" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic-columnar" value_original="conic-columnar" />
        <character constraint="in proximal 1/5-1/3 , 1-1.5 mm" is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.1 mm.</text>
      <biological_entity id="o12837" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.1" value_original="1.1" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Que., Sask., Yukon; Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>The name Potentilla arenosa is now used for most arctic and subarctic plants previously treated as P. hookeriana or P. nivea subsp. hookeriana (Lehmann) Hiitonen. As noted by J. Soják (1986), the type of P. hookeriana has quinate leaves; that name is now restricted to a Rocky Mountain species in sect. Rubricaules. The arctic and subarctic material was briefly (1989–1999) called P. nivea, as discussed under that species. Because the type of P. nivea var. arenosa and other northern Asian specimens correspond closely to the North American plants, the name P. arenosa is assigned here.</discussion>
  <discussion>The two subspecies differ only in one character, the petiole hairs, but are largely allopatric. Subspecies arenosa occurs in western and northern Greenland, northern North America (very northern in the east), and northern Asia (and perhaps northeasternmost European Russia); subsp. chamissonis occurs in southern Greenland, northeastern North America (more southern than subsp. arenosa), and northern Europe at least east to the Urals.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles with common to abundant short and/or stiff crisped hairs in addition to long verrucose hairs.</description>
      <determination>75a Potentilla arenosa subsp. arenosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles with sparse or no short and/or soft crisped hairs in addition to long verrucose hairs.</description>
      <determination>75b Potentilla arenosa subsp. chamissonis</determination>
    </key_statement>
  </key>
</bio:treatment>