<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">608</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Bracteatae</taxon_name>
    <taxon_name authority="Beadle" date="unknown" rank="species">harbisonii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>28: 413. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series bracteatae;species harbisonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100107</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Lance" date="unknown" rank="species">SELECTEDREFERENCE</taxon_name>
    <place_of_publication>
      <publication_title>R. and J. B. Phipps.</publication_title>
      <place_in_publication>2000. Crataegus harbisonii rediscovered and amplified. Castanea 85: 291–296.</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>species SELECTEDREFERENCE</taxon_hierarchy>
  </taxon_identification>
  <number>112.</number>
  <other_name type="common_name">Harbison hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 50–80 dm.</text>
      <biological_entity id="o16337" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="50" from_unit="dm" name="some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth sparsely pilose, 1-year old chestnut-brown, becoming smooth;</text>
      <biological_entity id="o16339" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16340" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o16341" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="chestnut-brown" value_original="chestnut-brown" />
        <character is_modifier="false" modifier="becoming" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs frequent, 2-years old glossy, dark-brown or nearly black, stout, 3–4 cm.</text>
      <biological_entity id="o16342" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o16343" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="reflectance" src="d0_s2" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s2" value="black" value_original="black" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16344" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <relation from="o16343" id="r1052" name="on" negation="false" src="d0_s2" to="o16344" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole length 15–20% blade, sparsely pilose, gland-dotted;</text>
      <biological_entity id="o16345" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16346" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="15-20%; sparsely" name="length" notes="" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
      <biological_entity id="o16347" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade shiny green, broadly elliptic to ovate, 4–7 cm, base cuneate, lobes 0 or 1–4 per side, sometimes apiculi, sinuses very shallow, margins doubly serrate, 5 teeth per cm, teeth 2 mm, gland-tipped early, veins 6 or 7 per side, apex acute, abaxial surface pubescent on veins mature, adaxial appressed-pubescent young.</text>
      <biological_entity id="o16348" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16349" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16350" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16351" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="per side" constraintid="o16352" from="1" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o16352" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o16353" name="apiculus" name_original="apiculi" src="d0_s4" type="structure" />
      <biological_entity id="o16354" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity id="o16355" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o16356" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o16357" name="cm" name_original="cm" src="d0_s4" type="structure" />
      <biological_entity id="o16358" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o16359" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="6" value_original="6" />
        <character name="quantity" src="d0_s4" unit="or per" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o16360" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o16361" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16362" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="on veins" constraintid="o16363" is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16363" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="mature" value_original="mature" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16364" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <relation from="o16356" id="r1053" name="per" negation="false" src="d0_s4" to="o16357" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 5–12-flowered, arising subterminally from perennial short-shoots, also frequently on leafy side shoots of season lateral to extension shoots;</text>
      <biological_entity id="o16365" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-12-flowered" value_original="5-12-flowered" />
        <character constraint="from short-shoots" constraintid="o16366" is_modifier="false" name="orientation" src="d0_s5" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o16366" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="side" id="o16367" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="leafy" value_original="leafy" />
        <character constraint="to extension shoots" constraintid="o16369" is_modifier="false" name="position" src="d0_s5" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o16368" name="season" name_original="season" src="d0_s5" type="structure" />
      <biological_entity constraint="extension" id="o16369" name="shoot" name_original="shoots" src="d0_s5" type="structure" />
      <relation from="o16365" id="r1054" modifier="frequently" name="on" negation="false" src="d0_s5" to="o16367" />
      <relation from="o16367" id="r1055" name="part_of" negation="false" src="d0_s5" to="o16368" />
    </statement>
    <statement id="d0_s6">
      <text>branches pilose;</text>
      <biological_entity id="o16370" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles semipersistent, ligulate, 7–18 mm, subherbaceous, margins glandular-pectinate to glandular-serrate.</text>
      <biological_entity id="o16371" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="semipersistent" value_original="semipersistent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="ligulate" value_original="ligulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="18" to_unit="mm" />
        <character is_modifier="false" name="growth_form" src="d0_s7" value="subherbaceous" value_original="subherbaceous" />
      </biological_entity>
      <biological_entity id="o16372" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="glandular-pectinate" name="shape" src="d0_s7" to="glandular-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 20–25 mm diam.;</text>
      <biological_entity id="o16373" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium pubescent;</text>
      <biological_entity id="o16374" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals lanceolate, margins glandular-laciniate;</text>
      <biological_entity id="o16375" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o16376" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="glandular-laciniate" value_original="glandular-laciniate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20, anthers cream to light yellow;</text>
      <biological_entity id="o16377" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o16378" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s11" to="light yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3–5.</text>
      <biological_entity id="o16379" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes orange-red to red, 12–22 mm diam., pubescent;</text>
      <biological_entity id="o16380" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character char_type="range_value" from="orange-red" name="coloration" src="d0_s13" to="red" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s13" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals patent-reflexed;</text>
      <biological_entity id="o16381" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="patent-reflexed" value_original="patent-reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 3–5.2n = 68.</text>
      <biological_entity id="o16382" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16383" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early May; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early May" from="early May" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wooded hills, creekside brush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wooded hills" />
        <character name="habitat" value="creekside brush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">harbisoni</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Crataegus harbisonii is rare and is similar both to C. ashei and C. triflora and could perhaps be thought of as a particularly robust form of C. ashei; however, it is perhaps more likely of hybrid origin between these species. The most significant similarity to C. triflora (ser. Triflorae) is an ability to produce inflorescences on shoots of the season lateral to extension shoots. Nevertheless, it is easily distinguished from C. triflora by characteristics of growth habit, by its smaller flowers (except in the Louisiana and some Mississippi forms of C. triflora) with fewer stamens, by the more coriaceous leaves with larger and more distant teeth, by having among the most persistent bracteoles in the genus, a feature that led to the creation of ser. Bracteatae.</discussion>
  
</bio:treatment>