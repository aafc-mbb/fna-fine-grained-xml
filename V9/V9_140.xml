<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="de Candolle ex Seringe" date="1818" rank="section">Pimpinellifoliae</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">spinosissima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 491. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section pimpinellifoliae;species spinosissima;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011325</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rosa</taxon_name>
    <taxon_name authority="Baker" date="unknown" rank="species">illinoiensis</taxon_name>
    <taxon_hierarchy>genus Rosa;species illinoiensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">lutescens</taxon_name>
    <taxon_hierarchy>genus R.;species lutescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pimpinellifolia</taxon_name>
    <taxon_hierarchy>genus R.;species pimpinellifolia</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Burnet or Scots rose</other_name>
  <other_name type="common_name">rosier pimprenelle</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Prickles internodal, dense on main-stems, sparser on some flowering branches, paired or single, erect or ± curved, terete, 5–6 × 1–3 mm, mixed with shorter prickles and usually aciculi.</text>
      <biological_entity id="o8620" name="prickle" name_original="prickles" src="d0_s0" type="structure">
        <character is_modifier="false" name="position" src="d0_s0" value="internodal" value_original="internodal" />
        <character constraint="on main-stems" constraintid="o8621" is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character constraint="on branches" constraintid="o8622" is_modifier="false" name="arrangement_or_density" notes="" src="d0_s0" value="sparser" value_original="sparser" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s0" value="paired" value_original="paired" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s0" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s0" value="terete" value_original="terete" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s0" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character constraint="with shorter prickles" constraintid="o8623" is_modifier="false" name="arrangement" src="d0_s0" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o8621" name="main-stem" name_original="main-stems" src="d0_s0" type="structure" />
      <biological_entity id="o8622" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity constraint="shorter" id="o8623" name="prickle" name_original="prickles" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: stipules 9–14 × 2–4 mm, auricles 2.5–5 mm, margins entire or serrate-glandular, surfaces glabrous;</text>
      <biological_entity id="o8624" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o8625" name="stipule" name_original="stipules" src="d0_s1" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s1" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8626" name="auricle" name_original="auricles" src="d0_s1" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8627" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="serrate-glandular" value_original="serrate-glandular" />
      </biological_entity>
      <biological_entity id="o8628" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole and rachis with pricklets, pubescent, glandular;</text>
      <biological_entity id="o8629" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8630" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o8631" name="rachis" name_original="rachis" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o8632" name="pricklet" name_original="pricklets" src="d0_s2" type="structure" />
      <relation from="o8630" id="r554" name="with" negation="false" src="d0_s2" to="o8632" />
      <relation from="o8631" id="r555" name="with" negation="false" src="d0_s2" to="o8632" />
    </statement>
    <statement id="d0_s3">
      <text>leaflets: blade oblong-ovate or suborbiculate, (5–) 7–9 (–11) × 5–8 mm, or broadly elliptic, 5–22 × 5–12 mm, base obtuse or rounded to broadly cuneate, margins 1-serrate, sometimes multi-serrate, teeth 8–12 per side, gland-tipped, apex obtuse to acute.</text>
      <biological_entity id="o8633" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o8634" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s3" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8635" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="broadly cuneate" />
      </biological_entity>
      <biological_entity id="o8636" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="multi-serrate" value_original="multi-serrate" />
      </biological_entity>
      <biological_entity id="o8637" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o8638" from="8" name="quantity" src="d0_s3" to="12" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o8638" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o8639" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels: bracts absent.</text>
      <biological_entity id="o8640" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o8641" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: hypanthium 3–5 × 3–4 mm;</text>
      <biological_entity id="o8642" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o8643" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepal tip 2–3 mm, apex acuminate or caudate-acuminate;</text>
      <biological_entity id="o8644" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="sepal" id="o8645" name="tip" name_original="tip" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8646" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="caudate-acuminate" value_original="caudate-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, 15–25 × 14–23 mm;</text>
      <biological_entity id="o8647" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o8648" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s7" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>styles exsert 2 mm beyond stylar orifice of hypanthial disc.</text>
      <biological_entity id="o8649" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o8650" name="style" name_original="styles" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="exsert" value_original="exsert" />
        <character constraint="beyond stylar, orifice" constraintid="o8651, o8652" name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8651" name="stylar" name_original="stylar" src="d0_s8" type="structure" />
      <biological_entity id="o8652" name="orifice" name_original="orifice" src="d0_s8" type="structure" />
      <biological_entity id="o8653" name="hypanthial" name_original="hypanthial" src="d0_s8" type="structure" />
      <biological_entity id="o8654" name="disc" name_original="disc" src="d0_s8" type="structure" />
      <relation from="o8651" id="r556" name="part_of" negation="false" src="d0_s8" to="o8653" />
      <relation from="o8651" id="r557" name="part_of" negation="false" src="d0_s8" to="o8654" />
      <relation from="o8652" id="r558" name="part_of" negation="false" src="d0_s8" to="o8653" />
      <relation from="o8652" id="r559" name="part_of" negation="false" src="d0_s8" to="o8654" />
    </statement>
    <statement id="d0_s9">
      <text>Achenes 8–12, dark tan, 4 × 2.5 mm. 2n = 28.</text>
      <biological_entity id="o8655" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s9" to="12" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="dark tan" value_original="dark tan" />
        <character name="length" src="d0_s9" unit="mm" value="4" value_original="4" />
        <character name="width" src="d0_s9" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8656" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., Ont., Que.; Conn., Ill., Ind., Kans., Maine, Mass., Mo., N.H., N.J., N.Y., Ohio, Tenn., Vt., Va., Wis.; Europe; Asia (China, Japan); introduced also in Atlantic Islands (Iceland), Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="also in Atlantic Islands (Iceland)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rosa spinosissima hybridizes with other species of the genus. Some garden cultivars are more robust and glandular than wild types.</discussion>
  <discussion>Rosa spinosissima is an erect, relatively low subshrub characterized by erect, relatively short stems with relatively long rhizomes forming dense patches. Stems extend to 10 dm with dense, intermixed internodal prickles and aciculi and lacking infrastipular prickles. Leaflets are relatively small and mostly 9–11; flowers are solitary and petals are white; inflorescences lack bracts; and hips are lustrous, blackish purple with erect, persistent sepals.</discussion>
  <discussion>Using Rosa spinosissima as one parent, George Harison crossed R. ×foetida (Austrian brier rose) in his New York City garden in about 1830 to produce Harison's yellow rose, R. ×harisonii Rivers. This unique yellow flowering rose was planted widely as pioneers moved west across the plains where even today it has been found in Arizona, Colorado, Idaho, Iowa, Nebraska, Nevada, Texas, Utah, and other states.</discussion>
  <discussion>Of a group of medicinal plants tested for antioxidant properties, Rosa spinosissima, with the largest amounts of phenolic compounds, proved to have the highest radical scavenging activity and provided the highest peroxidation inhibition (A. Mavi et al. 2004).</discussion>
  
</bio:treatment>