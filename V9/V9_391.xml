<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">252</other_info_on_meta>
    <other_info_on_meta type="mention_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">HORKELIA</taxon_name>
    <taxon_name authority="Elmer" date="1905" rank="species">rydbergii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>39: 50. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section horkelia;species rydbergii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100246</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">bernardina</taxon_name>
    <taxon_hierarchy>genus Horkelia;species bernardina</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_name authority="(S. Watson) D. D. Keck" date="unknown" rank="subspecies">parryi</taxon_name>
    <taxon_hierarchy>genus H.;species bolanderi;subspecies parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">H.</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">parryi</taxon_name>
    <taxon_hierarchy>genus H.;species bolanderi;variety parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_name authority="(S. Watson) Munz &amp; I. M. Johnston" date="unknown" rank="variety">parryi</taxon_name>
    <taxon_hierarchy>genus Potentilla;species bolanderi;variety parryi</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Rydberg’s horkelia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted to ± matted, usually grayish to grayish green.</text>
      <biological_entity id="o6623" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character char_type="range_value" from="usually grayish" name="coloration" src="d0_s0" to="grayish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (1–) 2–7 dm, hairs ascending to appressed.</text>
      <biological_entity id="o6624" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o6625" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves ± planar, (4–) 8–20 (–30) × 0.8–2 (–3) cm;</text>
      <biological_entity constraint="basal" id="o6626" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="planar" value_original="planar" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s2" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules entire;</text>
      <biological_entity id="o6627" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 7–14 per side, separate to ± overlapping at least distally, cuneate to flabellate, (3–) 5–15 (–22) × 3–10 (–15) mm, ± 1/2 to as wide as long, divided ± 1/3 to midrib into (3–) 5–10 acute to obtuse teeth, densely (to sparsely) strigose or pilose.</text>
      <biological_entity id="o6628" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o6629" from="7" name="quantity" src="d0_s4" to="14" />
        <character char_type="range_value" from="separate" modifier="at-least distally; distally" name="arrangement" notes="" src="d0_s4" to="more or less overlapping" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="flabellate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
        <character modifier="more or less" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character constraint="to midrib" constraintid="o6630" name="quantity" src="d0_s4" value="1/3" value_original="1/3" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o6629" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o6630" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <biological_entity id="o6631" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="acute" is_modifier="true" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
      <relation from="o6630" id="r404" name="into" negation="false" src="d0_s4" to="o6631" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (2 or) 3–6 (–8).</text>
      <biological_entity constraint="cauline" id="o6632" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="8" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences ± open to congested, flowers arranged individually or in glomerules.</text>
      <biological_entity id="o6633" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open to congested" value_original="open to congested" />
      </biological_entity>
      <biological_entity id="o6634" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="individually" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in glomerules" value_original="in glomerules" />
      </biological_entity>
      <biological_entity id="o6635" name="glomerule" name_original="glomerules" src="d0_s6" type="structure" />
      <relation from="o6634" id="r405" name="in" negation="false" src="d0_s6" to="o6635" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels (1–) 2–8 mm.</text>
      <biological_entity id="o6636" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 10–15 mm diam.;</text>
      <biological_entity id="o6637" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlets lanceolate to narrowly elliptic, 2–3.5 (–4) × 0.5–1 mm, ± 3/4 length of sepals, entire;</text>
      <biological_entity constraint="epicalyx" id="o6638" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="narrowly elliptic" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character modifier="more or less" name="length" src="d0_s9" value="3/4 length of sepals" value_original="3/4 length of sepals" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 1–1.5 (–2) × 2.5–4.5 (–6) mm, less than 1/2 as deep as wide, interior pilose;</text>
      <biological_entity id="o6639" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" constraint="as-deep-as interior" constraintid="o6640" from="0" name="quantity" src="d0_s10" to="1/2" />
      </biological_entity>
      <biological_entity id="o6640" name="interior" name_original="interior" src="d0_s10" type="structure">
        <character is_modifier="true" name="width" src="d0_s10" value="wide" value_original="wide" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals spreading to ± reflexed, lanceolate, (2.5–) 3–5 (–5.2) mm;</text>
      <biological_entity id="o6641" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s11" to="more or less reflexed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals oblong to oblanceolate, 4–5.5 × 1.2–2.5 mm, apex rounded to truncate or slightly emarginate;</text>
      <biological_entity id="o6642" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6643" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s12" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.5–2 × 0.5–1 mm, anthers 0.6–1 mm;</text>
      <biological_entity id="o6644" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6645" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 20–50 (–120);</text>
      <biological_entity id="o6646" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="120" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2–4 mm.</text>
      <biological_entity id="o6647" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light to dark-brown, 1–1.5 mm, smooth or roughened.</text>
      <biological_entity id="o6648" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s16" to="dark-brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s16" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry to moist meadows and stream banks, in conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry to moist meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="conifer woodlands" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Horkelia rydbergii occurs in the Transverse Ranges of Kern, Los Angeles, San Bernardino, and Ventura counties. Reports from farther north (as H. bolanderi var. parryi) are referable to H. marinensis, H. yadonii, or, possibly, H. cuneata. Although there has been occasional nomenclatural confusion between this taxon (as H. bolanderi var. parryi) and H. parryi Greene, the two are taxonomically and geographically distinct.</discussion>
  <discussion>Plants in the Mount Pinos–Lockwood Valley area of Kern and Ventura counties, and on Frazier Mountain, are distinctly gray with an abundance of tightly curled hairs. The leaves tend to be relatively short (to 8 cm) with relatively small leaflets (3–6 mm) on the basal leaves. Stems are relatively short (to 3 dm), and the fruits are dark brown, distinctly rugose, and 1.3–1.5 mm. This phase (including the type of Horkelia rydbergii) abruptly gives way to a grayish green phase with a slightly less dense, pilose indument in the San Bernardino and San Gabriel mountains. Here the plants tend to have larger basal leaves (to 30 cm) and leaflets (5–12 mm), longer stems (to 7 dm), and fruits that are a lighter brown, mostly smooth, and 1–1.2 mm. This phase in turn grades into even less densely hairy plants near Bear Lake, where the type of H. bolanderi var. parryi was obtained.</discussion>
  
</bio:treatment>