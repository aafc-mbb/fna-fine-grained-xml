<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Rubricaules</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="species">modesta</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 331. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section rubricaules;species modesta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100341</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Richardson" date="unknown" rank="species">concinna</taxon_name>
    <taxon_name authority="(Rydberg) S. L. Welsh &amp; B. C. Johnston" date="unknown" rank="variety">modesta</taxon_name>
    <taxon_hierarchy>genus Potentilla;species concinna;variety modesta</taxon_hierarchy>
  </taxon_identification>
  <number>86.</number>
  <other_name type="common_name">Modest cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches usually not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o8744" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="with leaves" constraintid="o8745" is_modifier="false" modifier="usually not" name="architecture" src="d0_s0" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o8745" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to nearly erect, (0.3–) 0.5–1.5 (–2.5) dm.</text>
      <biological_entity id="o8746" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="nearly erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves usually palmate, rarely also ternate on same plant or subpalmate, 2–6 (–8) cm;</text>
      <biological_entity constraint="basal" id="o8747" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character constraint="on plant" constraintid="o8748" is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="ternate" value_original="ternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subpalmate" value_original="subpalmate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8748" name="plant" name_original="plant" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.5–3.5 (–5) cm, long hairs common to abundant, ± ascending to almost spreading, 1–2 mm, weak to ± stiff, scarcely to ± verrucose, short and/or crisped hairs common to abundant, cottony hairs absent, glands sparse to common;</text>
      <biological_entity id="o8749" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8750" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s3" to="abundant" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending to almost" value_original="ascending to almost" />
        <character is_modifier="false" modifier="almost" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="more or less stiff" />
        <character is_modifier="false" modifier="scarcely to more or less" name="relief" src="d0_s3" value="verrucose" value_original="verrucose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o8751" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s3" to="abundant" />
      </biological_entity>
      <biological_entity id="o8752" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8753" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets (3–) 5, proximalmost separated by 0–1 mm, central obovate to oblanceolate-elliptic, 0.5–2 (–2.5) × 0.5–1 (–1.5) cm, petiolules 1 mm, distal 3/4 to nearly whole margin incised 1/2–3/4 to midvein, teeth 2–5 per side, (1–) 2–5 mm, apical tufts 0.5–1 mm, abaxial surfaces white, long hairs common to abundant, cottony-crisped hairs dense, short hairs and glands absent or obscured, adaxial grayish green, long hairs common to abundant, 0.5–1.5 mm, ± stiff, rarely soft, short and/or crisped hairs sparse to common, cottony hairs absent, glands sparse to common.</text>
      <biological_entity id="o8754" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o8755" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character constraint="by central leaflets" constraintid="o8756" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" notes="" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_width" notes="" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" notes="" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o8756" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="oblanceolate-elliptic" />
      </biological_entity>
      <biological_entity id="o8757" name="petiolule" name_original="petiolules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character constraint="to margin" constraintid="o8758" name="quantity" src="d0_s4" value="3/4" value_original="3/4" />
        <character char_type="range_value" constraint="to midvein" constraintid="o8759" from="1/2" name="quantity" src="d0_s4" to="3/4" />
      </biological_entity>
      <biological_entity id="o8758" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o8759" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o8760" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o8761" from="2" name="quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s4" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8761" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="apical" id="o8762" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8763" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o8764" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
      <biological_entity id="o8765" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o8766" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o8767" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8768" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="grayish green" value_original="grayish green" />
      </biological_entity>
      <biological_entity id="o8769" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="rarely" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o8770" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
      </biological_entity>
      <biological_entity id="o8771" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o8772" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 1–2.</text>
      <biological_entity constraint="cauline" id="o8773" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (1–) 3–10-flowered, congested or ± elongating in fruit, branch angle 20–35°.</text>
      <biological_entity id="o8774" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="(1-)3-10-flowered" value_original="(1-)3-10-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
        <character constraint="in fruit" constraintid="o8775" is_modifier="false" modifier="more or less" name="length" src="d0_s6" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o8775" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
      <biological_entity constraint="branch" id="o8776" name="angle" name_original="angle" src="d0_s6" type="structure">
        <character name="degree" src="d0_s6" value="20-35°" value_original="20-35°" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 0.2–0.5 cm, proximal to 1.2 cm.</text>
      <biological_entity id="o8777" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s7" to="0.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8778" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: epicalyx bractlets narrowly elliptic, 1.5–3 × 0.5–1 mm;</text>
      <biological_entity id="o8779" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="epicalyx" id="o8780" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 3–4 mm diam.;</text>
      <biological_entity id="o8781" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o8782" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 2.5–4 mm, apex ± acute, glands abundant, usually not obscured;</text>
      <biological_entity id="o8783" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8784" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8785" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8786" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="usually not" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals yellow, ± overlapping, 3.5–5 × 3–4 mm, slightly longer than sepals;</text>
      <biological_entity id="o8787" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8788" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s11" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
        <character constraint="than sepals" constraintid="o8789" is_modifier="false" name="length_or_size" src="d0_s11" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity id="o8789" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 1 mm, anthers 0.3–0.7 mm;</text>
      <biological_entity id="o8790" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8791" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8792" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>carpels 20–40, styles 1 mm.</text>
      <biological_entity id="o8793" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8794" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="40" />
      </biological_entity>
      <biological_entity id="o8795" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 1 mm.</text>
      <biological_entity id="o8796" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine tundra, fellfields, talus slopes, cliffs, usually on limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine tundra" />
        <character name="habitat" value="fellfields" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="limestone" modifier="cliffs usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500–3900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3900" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla modesta is the dominant component of what has previously been called P. rubricaulis in the Intermountain Region (for example, N. H. Holmgren 1997b, including illustration). This and other species of alpine Potentilla often grow in mixed populations, which contributes to confusion and mistaken identifications. In general, P. modesta has a more compact inflorescence and more consistently palmate basal leaves than sympatric members of the section. Intergrades with P. jepsonii (sect. Pensylvanicae) can also be problematic, though leaves of the latter are generally subpinnate and abaxially grayish rather than white.</discussion>
  <discussion>The epithet modesta is misapplied in the combination Potentilla concinna var. modesta, which S. L. Welsh et al. (1993) used for plants mostly placed here in P. concinna var. divisa (sect. Concinnae).</discussion>
  
</bio:treatment>