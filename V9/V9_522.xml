<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Spach" date="1846" rank="genus">poteridium</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg in N. L. Britton et al." date="1908" rank="species">occidentale</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 388. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus poteridium;species occidentale</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100383</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sanguisorba</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 429. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sanguisorba;species occidentalis</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Western burnet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annual, glabrous;</text>
      <biological_entity id="o21700" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="winter-annual" value_original="winter-annual" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots 3–10 dm.</text>
      <biological_entity id="o21701" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stamens 2 (or 4).</text>
      <biological_entity id="o21702" name="stamen" name_original="stamens" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruits: hypanthia ridges rounded, thickened;</text>
      <biological_entity id="o21703" name="fruit" name_original="fruits" src="d0_s3" type="structure" />
      <biological_entity constraint="hypanthia" id="o21704" name="ridge" name_original="ridges" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sepals not thickened proximally.</text>
      <biological_entity id="o21705" name="fruit" name_original="fruits" src="d0_s4" type="structure" />
      <biological_entity id="o21706" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not; proximally" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul; fruiting Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, sandy open ground, sagebrush flats, vernal pools, drawdown shorelines of streams and lakes, grassy clearings, roadsides, particularly with surficial or subterranean moisture</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sandy open ground" />
        <character name="habitat" value="sagebrush flats" />
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="drawdown shorelines" constraint="of streams and lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="grassy clearings" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="surficial" />
        <character name="habitat" value="subterranean moisture" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Idaho, Mont., Nev., N.Mex., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The taxonomic status of Poteridium occidentale has been controversial. Most floristicians of the twentieth century accepted it as distinct from the more eastern P. annuum (usually treating the two in Sanguisorba); but in recent decades a trend developed to merge the two taxa. While superficially similar, they seem to represent independent evolutionary lineages, as indicated by morphologic distinctions and allopatric distributions.</discussion>
  
</bio:treatment>