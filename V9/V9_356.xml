<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">IVESIA</taxon_name>
    <taxon_name authority="S. Watson" date="1882" rank="species">utahensis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 371. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section ivesia;species utahensis;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100282</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">utahensis</taxon_name>
    <taxon_hierarchy>genus Potentilla;species utahensis</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Utah ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green, rosetted to tufted;</text>
      <biological_entity id="o25701" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="rosetted" name="arrangement" src="d0_s0" to="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot proximally enlarged, not fleshy.</text>
      <biological_entity id="o25702" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s1" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems prostrate to ascending, 0.5–1.5 (–2) dm.</text>
      <biological_entity id="o25703" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves ± tightly cylindric, 2–7 (–9) cm;</text>
      <biological_entity constraint="basal" id="o25704" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less tightly" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheathing base glabrous abaxially;</text>
      <biological_entity id="o25705" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–2 cm, hairs 0.5–1 mm;</text>
      <biological_entity id="o25706" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o25707" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 15–20 per side, 2–4 mm, glabrate or sparsely short-hirsute, sparsely to densely glandular, lobes (2–) 3–8, narrowly oblanceolate to obovate, apex not setose.</text>
      <biological_entity id="o25708" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o25709" from="15" name="quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="sparsely short-hirsute" name="pubescence" src="d0_s6" to="sparsely densely glandular" />
        <character char_type="range_value" from="sparsely short-hirsute" name="pubescence" src="d0_s6" to="sparsely densely glandular" />
      </biological_entity>
      <biological_entity id="o25709" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o25710" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="8" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s6" to="obovate" />
      </biological_entity>
      <biological_entity id="o25711" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (0–) 1, not paired.</text>
      <biological_entity constraint="cauline" id="o25712" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (5–) 10–30-flowered, 1–2.5 (–5) cm diam.;</text>
      <biological_entity id="o25713" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="(5-)10-30-flowered" value_original="(5-)10-30-flowered" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glomerules 1–few.</text>
      <biological_entity id="o25714" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="false" name="quantity" src="d0_s9" to="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1–7 mm.</text>
      <biological_entity id="o25715" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 7–9 mm diam.;</text>
      <biological_entity id="o25716" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets narrowly oblong to oval, 0.8–2 mm;</text>
      <biological_entity constraint="epicalyx" id="o25717" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s12" to="oval" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium shallowly cupulate, (1–) 1.5–2 × 2.5–3.5 mm;</text>
      <biological_entity id="o25718" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s13" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals (1.8–) 2–3 mm, acute;</text>
      <biological_entity id="o25719" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, sometimes pink-tinged, oblanceolate to spatulate, 1.8–3 mm;</text>
      <biological_entity id="o25720" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s15" value="pink-tinged" value_original="pink-tinged" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s15" to="spatulate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5, filaments 1.3–1.8 mm, anthers orangish to reddish-brown, 0.4–0.6 mm;</text>
      <biological_entity id="o25721" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o25722" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25723" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="orangish" name="coloration" src="d0_s16" to="reddish-brown" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels (1–) 2–4, styles 1.5–2 mm.</text>
      <biological_entity id="o25724" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s17" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="4" />
      </biological_entity>
      <biological_entity id="o25725" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes yellowish green to light tan or gray-brown, 1.7–1.9 mm.</text>
      <biological_entity id="o25726" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s18" to="light tan or gray-brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s18" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus slopes, bare ridges, in high-elevation sagebrush communities, subalpine to alpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="bare ridges" constraint="in high-elevation sagebrush communities" />
        <character name="habitat" value="high-elevation sagebrush communities" />
        <character name="habitat" value="subalpine to alpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3200–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="3200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia utahensis is known from the Wasatch and adjacent Uinta Mountains in northern Utah. The species stands as the white-petaled counterpart to the yellow-petaled Sierran I. lycopodioides, possibly indicating a common ancestral stock that was once more continuous across the Great Basin.</discussion>
  
</bio:treatment>