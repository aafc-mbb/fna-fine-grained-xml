<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">349</other_info_on_meta>
    <other_info_on_meta type="mention_page">348</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">neillieae</taxon_name>
    <taxon_name authority="(Cambessèdes) Rafinesque" date="unknown" rank="genus">physocarpus</taxon_name>
    <taxon_name authority="(Pursh) Kuntze" date="1891" rank="species">capitatus</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Gen. Pl.</publication_title>
      <place_in_publication>1: 219. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe neillieae;genus physocarpus;species capitatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100294</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">capitata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 342. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species capitata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Physocarpus</taxon_name>
    <taxon_name authority="(Linnaeus) Maximowicz" date="unknown" rank="species">opulifolius</taxon_name>
    <taxon_name authority="(Seringe) B. Boivin" date="unknown" rank="variety">tomentellus</taxon_name>
    <taxon_hierarchy>genus Physocarpus;species opulifolius;variety tomentellus</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Pacific nine-bark</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 45 (–60) dm.</text>
      <biological_entity id="o7433" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="dm" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="45" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sometimes suckering, angled, glabrous or finely stellate-hairy.</text>
      <biological_entity id="o7434" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="suckering" value_original="suckering" />
        <character is_modifier="false" name="shape" src="d0_s1" value="angled" value_original="angled" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s1" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules linear to narrowly elliptic, 4 × 0.5–2 mm;</text>
      <biological_entity id="o7435" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o7436" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s2" to="narrowly elliptic" />
        <character name="length" src="d0_s2" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–2 (–3) cm;</text>
      <biological_entity id="o7437" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7438" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly ovate to obovate, (3–) 4–8 cm, usually as wide as long, base rounded to truncate or slightly cordate, 3-lobed or 5-lobed, margins irregularly crenate to doubly serrate, apex obtuse to acute, abaxial surface paler, glabrous or more densely stellate-hairy, adaxial glabrous or sparsely stellate-hairy.</text>
      <biological_entity id="o7439" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o7440" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s4" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o7441" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate or slightly cordate 3-lobed or 5-lobed" />
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="truncate or slightly cordate 3-lobed or 5-lobed" />
      </biological_entity>
      <biological_entity id="o7442" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="irregularly crenate" name="shape" src="d0_s4" to="doubly serrate" />
      </biological_entity>
      <biological_entity id="o7443" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7444" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="paler" value_original="paler" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7445" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 30–50-flowered, dense, hemispheric racemes, 3 cm diam., sometimes compound with some proximal pedicels becoming secondary peduncles;</text>
      <biological_entity id="o7446" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="30-50-flowered" value_original="30-50-flowered" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o7447" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character name="diameter" src="d0_s5" unit="cm" value="3" value_original="3" />
        <character constraint="with proximal pedicels" constraintid="o7448" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7448" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <biological_entity constraint="secondary" id="o7449" name="peduncle" name_original="peduncles" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>bracts narrowly elliptic to spatulate, 4 × 2 mm, apex acute or erose-dentate, faces glandular.</text>
      <biological_entity id="o7450" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s6" to="spatulate" />
        <character name="length" src="d0_s6" unit="mm" value="4" value_original="4" />
        <character name="width" src="d0_s6" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7451" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="erose-dentate" value_original="erose-dentate" />
      </biological_entity>
      <biological_entity id="o7452" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–1.5 cm, densely stellate-hairy.</text>
      <biological_entity id="o7453" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 5–8 mm diam.;</text>
      <biological_entity id="o7454" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium cupshaped, 2 mm, densely stellate-hairy;</text>
      <biological_entity id="o7455" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cup-shaped" value_original="cup-shaped" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals pale green to white, darker in center, triangular, 2–3 mm, apex gland-tipped, surfaces densely stellate-hairy;</text>
      <biological_entity id="o7456" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s10" to="white" />
        <character constraint="in center" constraintid="o7457" is_modifier="false" name="coloration" src="d0_s10" value="darker" value_original="darker" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7457" name="center" name_original="center" src="d0_s10" type="structure" />
      <biological_entity id="o7458" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o7459" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, broadly elliptic to orbiculate, 3–4 × 3–4 mm;</text>
      <biological_entity id="o7460" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s11" to="orbiculate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens equal to or exceeding petals;</text>
      <biological_entity id="o7461" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o7462" name="petal" name_original="petals" src="d0_s12" type="structure" />
      <relation from="o7461" id="r464" name="exceeding" negation="false" src="d0_s12" to="o7462" />
    </statement>
    <statement id="d0_s13">
      <text>carpels 3–5, connate basally, mostly glabrous, sometimes hairy (on ventral suture).</text>
      <biological_entity id="o7463" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Follicles 3–5, connate basally, shiny brown, ovoid, 5–7 mm (lengths slightly exceeding sepals), glabrous;</text>
      <biological_entity id="o7464" name="follicle" name_original="follicles" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2.5–3 mm.</text>
      <biological_entity id="o7465" name="style" name_original="styles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 2 (–5), pyriform, 2.3–2.8 mm. 2n = 18.</text>
      <biological_entity id="o7466" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="5" />
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s16" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s16" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7467" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sunny slopes on clay (higher elevations), stream and swamp banks, lake margins in moist woods (lower elevations)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sunny slopes" modifier="open" constraint="on clay ( higher elevations ) , stream" />
        <character name="habitat" value="clay" modifier="on" />
        <character name="habitat" value="higher elevations" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="banks" modifier="and swamp" />
        <character name="habitat" value="lake margins" constraint="in moist woods ( lower elevations )" />
        <character name="habitat" value="moist woods" />
        <character name="habitat" value="lower elevations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Physocarpus capitatus is commonly cultivated.</discussion>
  
</bio:treatment>