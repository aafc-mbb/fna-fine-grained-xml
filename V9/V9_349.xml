<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="T. W. Nelson &amp; J. P. Nelson" date="1981" rank="species">paniculata</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>33: 165, fig. 1. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species paniculata;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100269</other_info_on_name>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Ash Creek ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants grayish, ± matted.</text>
      <biological_entity id="o18787" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="more or less" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± prostrate, 0.4–1.5 (–2) dm.</text>
      <biological_entity id="o18788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves tightly cylindric, (1.5–) 2–5 (–7) cm;</text>
      <biological_entity constraint="basal" id="o18789" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="tightly" name="shape" src="d0_s2" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheathing base densely hairy abaxially;</text>
      <biological_entity id="o18790" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–4 cm;</text>
      <biological_entity id="o18791" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lateral leaflets (5–) 8–15 per side, overlapping at least distally, ± flabellate, 0.5–2 mm, incised to base or nearly so into (0–) 3–8 (–15) elliptic to narrowly obovate lobes, apex not or obscurely setose, surfaces densely hirsute, cryptically glandular;</text>
      <biological_entity constraint="lateral" id="o18792" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s5" to="8" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o18793" from="8" name="quantity" src="d0_s5" to="15" />
        <character is_modifier="false" modifier="at-least distally; distally" name="arrangement" notes="" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="flabellate" value_original="flabellate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character constraint="to " constraintid="o18795" is_modifier="false" name="shape" src="d0_s5" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o18793" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o18794" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18795" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o18796" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18797" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o18798" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s5" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o18799" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="cryptically" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o18795" id="r1221" name="to" negation="false" src="d0_s5" to="o18796" />
      <relation from="o18795" id="r1222" name="to" negation="false" src="d0_s5" to="o18797" />
    </statement>
    <statement id="d0_s6">
      <text>terminal leaflets indistinct.</text>
      <biological_entity constraint="terminal" id="o18800" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (0–) 1;</text>
      <biological_entity constraint="cauline" id="o18801" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade reduced.</text>
      <biological_entity id="o18802" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 20–200-flowered, congested, (1–) 1.5–6 (–10) cm diam.</text>
      <biological_entity id="o18803" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="20-200-flowered" value_original="20-200-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="congested" value_original="congested" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s9" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s9" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1.5–6 mm.</text>
      <biological_entity id="o18804" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 4–6 mm diam.;</text>
      <biological_entity id="o18805" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets 5, lanceolate to elliptic, 0.6–1.5 (–2) mm;</text>
      <biological_entity constraint="epicalyx" id="o18806" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="elliptic" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium shallowly cupulate, 1 × 2–3 mm;</text>
      <biological_entity id="o18807" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character name="length" src="d0_s13" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals (1–) 1.5–2.5 (–3) mm, acute;</text>
      <biological_entity id="o18808" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white to pale yellowish, linear, 1 mm;</text>
      <biological_entity id="o18809" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s15" to="pale yellowish" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5, filaments 0.3–1 mm, anthers yellow with maroon margins, ovate, 0.3–0.5 mm;</text>
      <biological_entity id="o18810" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18811" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18812" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character constraint="with margins" constraintid="o18813" is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18813" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="maroon" value_original="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels 1–2 (–3), styles 0.7–1.8 mm.</text>
      <biological_entity id="o18814" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="2" />
      </biological_entity>
      <biological_entity id="o18815" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 0.8–1.5 mm, smooth, prominently carunculate.</text>
      <biological_entity id="o18816" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s18" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry shallow volcanic ash and cinders atop volcanic bedrock, open sagebrush communities, adjacent conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cinders" constraint="atop volcanic bedrock , open sagebrush communities , adjacent conifer" />
        <character name="habitat" value="volcanic bedrock" />
        <character name="habitat" value="open sagebrush communities" />
        <character name="habitat" value="adjacent conifer" />
        <character name="habitat" value="dry shallow volcanic ash" />
        <character name="habitat" value="cinders atop volcanic bedrock" />
        <character name="habitat" value="adjacent conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia paniculata is known only from the Ash Valley area of Lassen County. The distinctions between I. paniculata and I. rhypara are perhaps on the same scale as variation among disjunct population clusters of I. rhypara, but no taxonomic adjustments are proposed at this time.</discussion>
  
</bio:treatment>