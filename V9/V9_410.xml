<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">CAPITATAE</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22(7): 7. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section CAPITATAE</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318013</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Capitatae</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. N. Amer. Potentilleae,</publication_title>
      <place_in_publication>122. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Horkelia;unranked Capitatae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Crum" date="unknown" rank="section">Capitatae</taxon_name>
    <taxon_hierarchy>genus Potentilla;section Capitatae</taxon_hierarchy>
  </taxon_identification>
  <number>10d.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming tufts or open mats, green or reddish, rarely grayish, conspicuously glandular, resinously aromatic.</text>
      <biological_entity id="o6417" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="resinously" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6418" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o6419" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o6417" id="r386" name="forming" negation="false" src="d0_s0" to="o6418" />
      <relation from="o6417" id="r387" name="forming" negation="false" src="d0_s0" to="o6419" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (0.6–) 1–6 (–9) dm.</text>
      <biological_entity id="o6420" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="9" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves planar;</text>
      <biological_entity constraint="basal" id="o6421" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="planar" value_original="planar" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules entire;</text>
      <biological_entity id="o6422" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3–8 (–15) per side, separate, sometimes ± overlapping, divided 1/5–3/4+ to midrib into (3–) 5–15 teeth or lobes not restricted to apex.</text>
      <biological_entity id="o6423" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="15" />
        <character char_type="range_value" constraint="per side" constraintid="o6424" from="3" name="quantity" src="d0_s4" to="8" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s4" value="separate" value_original="separate" />
        <character is_modifier="false" modifier="sometimes more or less" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character char_type="range_value" constraint="to midrib" constraintid="o6425" from="1/5" name="quantity" src="d0_s4" to="3/4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6424" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o6425" name="midrib" name_original="midrib" src="d0_s4" type="structure" />
      <biological_entity id="o6426" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o6427" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity id="o6428" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o6425" id="r388" name="into" negation="false" src="d0_s4" to="o6426" />
      <relation from="o6425" id="r389" name="into" negation="false" src="d0_s4" to="o6427" />
      <relation from="o6426" id="r390" name="restricted to" negation="false" src="d0_s4" to="o6428" />
      <relation from="o6427" id="r391" name="restricted to" negation="false" src="d0_s4" to="o6428" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences open to ± congested, flowers usually arranged in ± capitate glomerules, arranged individually in H. fusca var. filicoides.</text>
      <biological_entity id="o6429" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open to more" value_original="open to more" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity id="o6430" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character constraint="in glomerules" constraintid="o6431" is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character constraint="in h" constraintid="o6432" is_modifier="false" name="arrangement" notes="" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o6431" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o6432" name="h" name_original="h" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels remaining ± straight, outermost sometimes ± reflexed in congested inflorescences, 1–3 (–10) mm.</text>
      <biological_entity id="o6433" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="position" src="d0_s6" value="outermost" value_original="outermost" />
        <character constraint="in inflorescences" constraintid="o6434" is_modifier="false" modifier="sometimes more or less" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6434" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets linear, 0.2–0.3 (–0.5) mm wide, entire;</text>
      <biological_entity id="o6435" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o6436" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium interior glabrous;</text>
      <biological_entity id="o6437" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o6438" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="interior" value_original="interior" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals acute to acuminate;</text>
      <biological_entity id="o6439" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6440" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white to pale-pink, often veined with pink to rose, ± oblanceolate to cuneate, apex emarginate to truncate or rounded;</text>
      <biological_entity id="o6441" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6442" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pale-pink" />
        <character constraint="with apex" constraintid="o6443" is_modifier="false" modifier="often" name="architecture" src="d0_s10" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o6443" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" is_modifier="true" name="coloration" src="d0_s10" to="rose" />
        <character char_type="range_value" from="less oblanceolate" is_modifier="true" name="shape" src="d0_s10" to="cuneate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments white, glabrous, anthers longer than wide;</text>
      <biological_entity id="o6444" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6445" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6446" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="longer than wide" value_original="longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 10–25.</text>
      <biological_entity id="o6447" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6448" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1–1.8 mm, smooth.</text>
      <biological_entity id="o6449" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Rydberg recognized seven species in his group Capitatae, treated here as intergrading variation within a single species. Plants of the section share the characteristic Horkelia odor, glandularity, and planar leaves of sect. Horkelia, but differ in the combination of relatively small, short-pedicelled flowers that (except for var. filicoides) are most commonly aggregated into one or more capitate, purple-suffused glomerules, with linear epicalyx bractlets, oblanceolate-cuneate petals that are often pink-tinged, and relatively short, broad filaments.</discussion>
  
</bio:treatment>