<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Smith in A. Rees" date="1810" rank="species">calthifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>16: Geum no. 13. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species calthifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100222</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Caltha-leaved avens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subscapose.</text>
      <biological_entity id="o28587" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 8–40 cm, sparsely hirsute proximally, puberulent throughout.</text>
      <biological_entity id="o28588" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 5–25 cm, blade strongly lyrate-pinnate, sometimes simple, major leaflet 1, minor leaflets 1–7, terminal leaflet much larger than minor laterals;</text>
      <biological_entity id="o28589" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o28590" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28591" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s2" value="lyrate-pinnate" value_original="lyrate-pinnate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o28592" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="minor" id="o28593" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o28594" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than minor laterals" constraintid="o28595" is_modifier="false" name="size" src="d0_s2" value="much larger" value_original="much larger" />
      </biological_entity>
      <biological_entity constraint="minor" id="o28595" name="lateral" name_original="laterals" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 1.5–4.5 cm, stipules not evident, blade bractlike, not resembling basal, simple.</text>
      <biological_entity id="o28596" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o28597" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s3" to="4.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28598" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o28599" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28600" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o28599" id="r1867" name="resembling" negation="true" src="d0_s3" to="o28600" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–2 (–4) -flowered.</text>
      <biological_entity id="o28601" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2(-4)-flowered" value_original="1-2(-4)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels downy, hirsute, eglandular.</text>
      <biological_entity id="o28602" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="downy" value_original="downy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o28603" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 2–6 mm;</text>
      <biological_entity constraint="epicalyx" id="o28604" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green or green with slight maroon tinge;</text>
      <biological_entity id="o28605" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character constraint="with slight" is_modifier="false" name="coloration" src="d0_s8" value="maroon tinge" value_original="maroon tinge" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect-spreading, 6–11 mm;</text>
      <biological_entity id="o28606" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, yellow, obcordate-obdeltate to nearly orbiculate, 9–13 mm, longer than sepals, apex broadly and shallowly emarginate.</text>
      <biological_entity id="o28607" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obcordate-obdeltate" name="shape" src="d0_s10" to="nearly orbiculate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
        <character constraint="than sepals" constraintid="o28608" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o28608" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile, glabrous.</text>
      <biological_entity id="o28610" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <relation from="o28609" id="r1868" name="fruiting" negation="false" src="d0_s11" to="o28610" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles wholly persistent, not geniculate-jointed, 9–14 mm, apex not hooked, pilose in basal 3/4, eglandular.</text>
      <biological_entity id="o28609" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28611" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o28612" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="wholly" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o28614" name="3/4" name_original="3/4" src="d0_s12" type="structure" />
      <relation from="o28609" id="r1869" name="fruiting" negation="false" src="d0_s12" to="o28611" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 42.</text>
      <biological_entity id="o28613" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character constraint="in basal 3/4" constraintid="o28614" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28615" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Muskegs, moist meadows, heathlands, moist sites on rocky slopes, ledges, and cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="muskegs" />
        <character name="habitat" value="moist meadows" />
        <character name="habitat" value="heathlands" />
        <character name="habitat" value="moist sites" constraint="on rocky slopes , ledges , and cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Geum calthifolium hybridizes with G. rossii [= G. ×macranthum (Kearney ex Rydberg) B. Boivin]; see discussion under 4. G. schofieldii.</discussion>
  
</bio:treatment>