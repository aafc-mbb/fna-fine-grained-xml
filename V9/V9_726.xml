<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Corrêa ex Bonpland in A. von Humboldt and A. J. Bonpland" date="unknown" rank="genus">vauquelinia</taxon_name>
    <taxon_name authority="(Torrey) Sargent" date="1889" rank="species">californica</taxon_name>
    <taxon_name authority="(Standley) W. J. Hess &amp; Henrickson" date="1987" rank="subspecies">pauciflora</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>12: 135. 1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus vauquelinia;species californica;subspecies pauciflora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100525</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vauquelinia</taxon_name>
    <taxon_name authority="Standley" date="unknown" rank="species">pauciflora</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>31: 132. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vauquelinia;species pauciflora</taxon_hierarchy>
  </taxon_identification>
  <number>1c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Young stems loosely tomentulose, tardily glabrescent.</text>
      <biological_entity id="o2102" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" value_original="young" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s0" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" modifier="tardily" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole (1.5–) 4–16 (–22) mm;</text>
      <biological_entity id="o2103" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2104" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade green or yellow-green, lanceolate or oblong-lanceolate to elliptic or oblongelliptic, sometimes oblong-ovate, (2.2–) 3–7.5 (–9) × (0.6–) 0.8–1.4 (–2) cm, surfaces glabrate or puberulent along midveins.</text>
      <biological_entity id="o2105" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2106" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="elliptic or oblongelliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="2.2" from_unit="cm" name="atypical_length" src="d0_s2" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="9" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_width" src="d0_s2" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s2" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2107" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character constraint="along midveins" constraintid="o2108" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o2108" name="midvein" name_original="midveins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Corymbs 1.5–5 × 1.7–7 cm, puberulent to glabrate.</text>
      <biological_entity id="o2109" name="corymb" name_original="corymbs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium 1.5–2.5 × 2.5–3.3 mm, exterior puberulent, interior glabrate;</text>
      <biological_entity id="o2110" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2111" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s4" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2112" name="exterior" name_original="exterior" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="position" src="d0_s4" value="interior" value_original="interior" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals 1.1–2.2 × 1.4–2 mm, abaxially puberulent to glabrate;</text>
      <biological_entity id="o2113" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2114" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s5" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="abaxially puberulent" name="pubescence" src="d0_s5" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 3.4–5.4 × 2.4–3.4 mm;</text>
      <biological_entity id="o2115" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2116" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="length" src="d0_s6" to="5.4" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="width" src="d0_s6" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments (2.5–) 3–6 mm.</text>
      <biological_entity id="o2117" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2118" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules (4.5–) 5–6 × 3.5–4 (–4.5) mm.</text>
      <biological_entity id="o2119" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_length" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds 3.8–5 × 1.1–1.4 mm.</text>
      <biological_entity id="o2120" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone substrates in arid chaparral-desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="substrates" modifier="limestone" constraint="in arid chaparral-desert scrub" />
        <character name="habitat" value="arid chaparral-desert scrub" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua, Coahuila, Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies pauciflora is known from Cochise County, Arizona, and Hidalgo County, New Mexico. These plants have relatively small (to 5.5 mm) and thickened leaves with short petioles (to 7 mm), and small inflorescences.</discussion>
  
</bio:treatment>