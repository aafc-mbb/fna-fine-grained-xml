<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="J. Fryer &amp; B. Hylmö" date="1998" rank="species">magnificus</taxon_name>
    <place_of_publication>
      <publication_title>New Plantsman</publication_title>
      <place_in_publication>5: 138, fig. p. 140 [lower left]. 1998</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species magnificus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100047</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Magnificent cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 3–5 m.</text>
      <biological_entity id="o27733" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, spreading, arching;</text>
      <biological_entity id="o27734" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches spiraled and distichous, maroon, initially tomentose-pilose.</text>
      <biological_entity id="o27735" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="maroon" value_original="maroon" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s2" value="tomentose-pilose" value_original="tomentose-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o27736" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–8 mm, pilose-villous;</text>
      <biological_entity id="o27737" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-villous" value_original="pilose-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade suborbiculate, broadly elliptic, ovate, or rhombic, 34–40 × 20–32 mm, subcoriaceous, base obtuse or cuneate, margins flat, veins 5 or 6, superficial, apex acute [acuminate or apiculate], abaxial surfaces pale green, initially densely pilose, soon glabrous, adaxial brownish or coppery green, soon light green, slightly shiny, not glaucous, flat between lateral-veins, glabrescent;</text>
      <biological_entity id="o27738" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="34" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s5" to="32" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o27739" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o27740" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o27741" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="5" value_original="5" />
        <character name="quantity" src="d0_s5" unit="or" value="6" value_original="6" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
      </biological_entity>
      <biological_entity id="o27742" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27743" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="initially densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27745" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves lacking notable color.</text>
      <biological_entity constraint="adaxial" id="o27744" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="coppery green" value_original="coppery green" />
        <character is_modifier="false" modifier="soon" name="coloration" src="d0_s5" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character constraint="between lateral-veins" constraintid="o27745" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o27746" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o27744" id="r1808" name="fall" negation="false" src="d0_s6" to="o27746" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 25–40 mm with 3 or 4 leaves, 5–12-flowered, lax.</text>
      <biological_entity id="o27747" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="5-12-flowered" value_original="5-12-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o27748" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with 3 or 4leaves" from="25" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
      <relation from="o27747" id="r1809" name="on" negation="false" src="d0_s7" to="o27748" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 3–12 mm, glabrate.</text>
      <biological_entity id="o27749" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 12–14 mm diam.;</text>
      <biological_entity id="o27750" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>buds white;</text>
      <biological_entity id="o27751" name="bud" name_original="buds" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium cupulate, glabrous;</text>
      <biological_entity id="o27752" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals: margins sparsely villous, apex acute or obtuse, surfaces glabrous;</text>
      <biological_entity id="o27753" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o27754" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27755" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o27756" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals spreading, white, with hair-tuft;</text>
      <biological_entity id="o27757" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o27758" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o27759" name="hair-tuft" name_original="hair-tuft" src="d0_s13" type="structure" />
      <relation from="o27758" id="r1810" name="with" negation="false" src="d0_s13" to="o27759" />
    </statement>
    <statement id="d0_s14">
      <text>stamens (16–) 20, filaments white, anthers white;</text>
      <biological_entity id="o27760" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o27761" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="16" name="atypical_quantity" src="d0_s14" to="20" to_inclusive="false" />
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o27762" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o27763" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles (1 or) 2.</text>
      <biological_entity id="o27764" name="sepal" name_original="sepals" src="d0_s15" type="structure" />
      <biological_entity id="o27765" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pomes red, dark red, red-purple, crimson, or rich red with cherry, globose or depressed-globose, 10–13 × 12–13 mm, slightly shiny, not glaucous, glabrous;</text>
      <biological_entity id="o27766" name="pome" name_original="pomes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="crimson" value_original="crimson" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="rich" value_original="rich" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="red with cherry" value_original="red with cherry" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="crimson" value_original="crimson" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="rich" value_original="rich" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="red with cherry" value_original="red with cherry" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s16" to="13" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s16" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s16" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s16" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals suberect, indumentum not recorded;</text>
      <biological_entity id="o27767" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="suberect" value_original="suberect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>navel semiopen;</text>
      <biological_entity id="o27768" name="indumentum" name_original="indumentum" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style remnants at or near apex.</text>
      <biological_entity constraint="style" id="o27769" name="remnant" name_original="remnants" src="d0_s19" type="structure" />
      <biological_entity id="o27770" name="apex" name_original="apex" src="d0_s19" type="structure" />
      <relation from="o27769" id="r1811" name="at or near" negation="false" src="d0_s19" to="o27770" />
    </statement>
    <statement id="d0_s20">
      <text>Pyrenes (1 or) 2.2n = 68.</text>
      <biological_entity id="o27771" name="pyrene" name_original="pyrenes" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27772" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, hedgerows, edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="hedgerows" />
        <character name="habitat" value="edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ill., Iowa; Asia (China).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cotoneaster magnificus was synonymized with C. multiflorus Bunge by L. Lingdi and A. R. Brach (2003). Flowers of the latter are 9–10 mm wide in clusters of 10–20, the flowering pedicels are to 7 mm, and the leaves are smaller, thinner, and tend to be ovate. Plants of C. magnificus have flowers 12–14 mm wide in clusters of 5–12, flowering pedicels are to 12 mm, and the leaves are larger, thicker, and mostly elliptic. Reports of wild C. multiflorus from Illinois (F. Swink and G. S. Wilhelm 1994; J. T. Kartesz and C. A. Meacham 2003) and Iowa (W. R. Norris et al. 2001; Kartesz and Meacham) are referred to C. magnificus.</discussion>
  
</bio:treatment>