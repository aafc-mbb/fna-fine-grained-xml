<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">IVESIA</taxon_name>
    <taxon_name authority="A. Gray" date="1874" rank="species">webberi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 71. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section ivesia;species webberi;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100283</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">webberi</taxon_name>
    <taxon_hierarchy>genus Potentilla;species webberi</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Webber’s or wire ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± green, ± rosetted;</text>
      <biological_entity id="o20637" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender to ± stout, not fleshy.</text>
      <biological_entity id="o20638" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s1" to="more or less stout" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending, 0.5–1.5 (–1.8) dm.</text>
      <biological_entity id="o20639" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves loosely ± cylindric, 3–7 (–10) cm;</text>
      <biological_entity constraint="basal" id="o20640" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely more or less" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheathing base ± strigose abaxially;</text>
      <biological_entity id="o20641" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="more or less; abaxially" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–5 (–6) cm, hairs 2–4 mm;</text>
      <biological_entity id="o20642" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20643" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 4–8 (–10) per side, (0.5–) 3–8 (–10) mm, loosely long-strigose or villous and short-hirsute, ± glandular, lobes 2–5 (–12), linear to lanceolate, apex not setose.</text>
      <biological_entity id="o20644" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" constraint="per side" constraintid="o20645" from="4" name="quantity" src="d0_s6" to="8" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s6" value="long-strigose" value_original="long-strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="short-hirsute" value_original="short-hirsute" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o20645" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o20646" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="12" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o20647" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 2, paired.</text>
      <biological_entity constraint="cauline" id="o20648" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 5–15 (–25) -flowered, 1.5–3 (–6) cm diam.;</text>
      <biological_entity id="o20649" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-15(-25)-flowered" value_original="5-15(-25)-flowered" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="diameter" src="d0_s8" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glomerules 1.</text>
      <biological_entity id="o20650" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels (0.5–) 1–8 (–13) mm.</text>
      <biological_entity id="o20651" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 9–12 mm diam.;</text>
      <biological_entity id="o20652" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s11" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets linear, 1.2–3 mm;</text>
      <biological_entity constraint="epicalyx" id="o20653" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium cupulate, 1–2 (–2.5) × 2.5–5 mm;</text>
      <biological_entity id="o20654" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="2" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals 2.5–4.5 (–5.5) mm, acute;</text>
      <biological_entity id="o20655" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals yellow, narrowly oblanceolate, 2–3 (–4) mm;</text>
      <biological_entity id="o20656" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5, filaments 1.8–2.5 (–3) mm, anthers yellow, (0.8–) 1–1.6 mm;</text>
      <biological_entity id="o20657" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o20658" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20659" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels 3–8, styles 1.8–2.2 mm.</text>
      <biological_entity id="o20660" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s17" to="8" />
      </biological_entity>
      <biological_entity id="o20661" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s17" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes light-brown, often mottled darker brown, 1.9–2.5 mm.</text>
      <biological_entity id="o20662" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="mottled darker" value_original="mottled darker" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry flats and slopes, in sagebrush communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry flats" constraint="in sagebrush communities , conifer woodlands" />
        <character name="habitat" value="slopes" constraint="in sagebrush communities , conifer woodlands" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(1300–)1500–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1500" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia webberi is known only from the eastern foothills of the northern Sierra Nevada and scattered ranges to the east in California and adjacent Nevada. It is among the more distinctive species in the genus and is only tentatively placed in sect. Ivesia. The leaflets are loosely incised into slender, sparsely villous segments, and the two cauline leaves are paired with dissected stipules. Previous reports of the stems and inflorescence branches being glandular-puberulent are due to a misinterpretation of the minute pustulose bases associated with the villous indumentum as being enlarged glands.</discussion>
  
</bio:treatment>