<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="Ertter &amp; Reveal" date="1977" rank="species">rhypara</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rhypara</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species rhypara;variety rhypara;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250100680</other_info_on_name>
  </taxon_identification>
  <number>10a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (4–) 8–30 cm diam.</text>
      <biological_entity id="o19379" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s0" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="diameter" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (0.3–) 0.4–1.5 (–2) dm, usually exceeding leaves by more than 2 cm.</text>
      <biological_entity id="o19380" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o19381" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o19380" id="r1261" modifier="usually" name="exceeding" negation="false" src="d0_s1" to="o19381" />
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 10–60 (–100) -flowered, (1–) 2–5 (–7) cm diam.</text>
      <biological_entity id="o19382" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="10-60(-100)-flowered" value_original="10-60(-100)-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s2" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s2" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Scarcely or cryptically petrophytic in ash tuff bedrock overlain by dry soil and pulverized rubble, in sagebrush communities, sometimes juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ash tuff bedrock" modifier="scarcely or cryptically petrophytic in" />
        <character name="habitat" value="dry soil" />
        <character name="habitat" value="pulverized rubble" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="juniper woodlands" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety rhypara is known from widely scattered locations in central Malheur and southeastern Lake counties, Oregon, and in northern Washoe, Humboldt, and Elko counties, Nevada. Although not overtly petrophytic, plants are generally associated with crevices in the underlying bedrock (E. M. Clark and W. H. Clark 2003). They might thereby benefit from a more favorable water relationship, allowing them to be in full bloom when most associated species are summer-dormant.</discussion>
  
</bio:treatment>