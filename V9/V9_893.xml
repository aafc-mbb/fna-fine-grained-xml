<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="illustration_page">533</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Virides</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">viridis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">viridis</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series virides;species viridis;variety viridis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100614</other_info_on_name>
  </taxon_identification>
  <number>32a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: trunk bark light gray, ± exfoliating;</text>
      <biological_entity id="o8596" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity constraint="trunk" id="o8597" name="bark" name_original="bark" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="light gray" value_original="light gray" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s0" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>1-year old twigs gray-brown.</text>
      <biological_entity id="o8598" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o8599" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-brown" value_original="gray-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade broadly lanceolate to narrowly rhombic, 4–7 cm, thin, base usually broadly cuneate, lobes 2 or 3 per side, distinct, sinuses shallow to moderately deep, max LII 15–40%, lobe apex acute to subacute, margins crenate-serrate, teeth 1 mm, venation craspedodromous, veins 4 or 5 per side, apex acute, surfaces glabrous young except abaxially with tufts of hair in vein-axils, glabrescent.</text>
      <biological_entity id="o8600" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8601" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s2" to="narrowly rhombic" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o8602" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o8603" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" unit="or per" value="2" value_original="2" />
        <character name="quantity" src="d0_s2" unit="or per" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s2" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o8604" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o8605" name="sinuse" name_original="sinuses" src="d0_s2" type="structure">
        <character char_type="range_value" from="shallow" name="depth" src="d0_s2" to="moderately deep" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o8606" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" modifier="15 40%" name="shape" src="d0_s2" to="subacute" />
      </biological_entity>
      <biological_entity id="o8607" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o8608" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character name="some_measurement" src="d0_s2" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity id="o8609" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" unit="or per" value="4" value_original="4" />
        <character name="quantity" src="d0_s2" unit="or per" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o8610" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o8611" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o8612" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o8613" name="tuft" name_original="tufts" src="d0_s2" type="structure" />
      <biological_entity id="o8614" name="hair" name_original="hair" src="d0_s2" type="structure" />
      <biological_entity id="o8615" name="vein-axil" name_original="vein-axils" src="d0_s2" type="structure" />
      <relation from="o8612" id="r551" name="except" negation="false" src="d0_s2" to="o8613" />
      <relation from="o8612" id="r552" name="part_of" negation="false" src="d0_s2" to="o8614" />
      <relation from="o8612" id="r553" name="in" negation="false" src="d0_s2" to="o8615" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences: branches glabrous.</text>
      <biological_entity id="o8616" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o8617" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: hypanthium glabrous.</text>
      <biological_entity id="o8618" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o8619" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, fertile, alluvial woodlands, agricultural derivatives of these</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fertile" modifier="moist" />
        <character name="habitat" value="alluvial woodlands" />
        <character name="habitat" value="agricultural derivatives" constraint="of these" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Ky., La., Md., Miss., Mo., N.C., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety viridis is widespread and sympatric with the other varieties except var. glabriuscula, with which it shares a diffuse boundary. Reports from Kansas, Oklahoma, and Texas need confirmation. Variety viridis grades into var. glabriuscula, var. lanceolata, var. ovata, and Crataegus nitida.</discussion>
  
</bio:treatment>