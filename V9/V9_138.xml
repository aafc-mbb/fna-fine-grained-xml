<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">92</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="de Candolle ex Seringe" date="1818" rank="section">Caninae</taxon_name>
    <taxon_name authority="Smith in J. E. Smith et al." date="1812" rank="species">mollis</taxon_name>
    <place_of_publication>
      <publication_title>in J. E. Smith et al., Engl. Bot.</publication_title>
      <place_in_publication>35: plate 2459. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section caninae;species mollis;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100419</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Soft downy-rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, forming large thickets;</text>
      <biological_entity id="o21175" name="thicket" name_original="thickets" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
      </biological_entity>
      <relation from="o21174" id="r1373" name="forming" negation="false" src="d0_s0" to="o21175" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o21174" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually spreading and arching, erect, 8–15 (–20) dm;</text>
      <biological_entity id="o21176" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="20" to_unit="dm" />
        <character char_type="range_value" from="8" from_unit="dm" name="some_measurement" src="d0_s2" to="15" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches erect or spreading, bark reddish when exposed, pruinose when young or shaded;</text>
      <biological_entity constraint="distal" id="o21177" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>infrastipular prickles paired, erect, 5–6 × 3–4 mm, lengths ± uniform, internodal prickles similar or smaller, mixed with aciculi.</text>
      <biological_entity id="o21178" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when exposed" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="when young or shaded" name="coating" src="d0_s3" value="pruinose" value_original="pruinose" />
      </biological_entity>
      <biological_entity id="o21179" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="lengths" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="lengths" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s4" value="uniform" value_original="uniform" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o21180" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="smaller" value_original="smaller" />
        <character constraint="with aciculi" constraintid="o21181" is_modifier="false" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o21181" name="aciculus" name_original="aciculi" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, 8–11 (–14) cm;</text>
      <biological_entity id="o21182" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules 15–20 × 5–10 mm, auricles 5–8 mm, margins glabrous or ciliate, surfaces usually puberulent to tomentulose, sometimes glabrous, eglandular;</text>
      <biological_entity id="o21183" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21184" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21185" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21186" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually puberulent" name="pubescence" src="d0_s6" to="tomentulose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole and rachis without pricklets, pubescent, eglandular;</text>
      <biological_entity id="o21187" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o21188" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o21189" name="pricklet" name_original="pricklets" src="d0_s7" type="structure" />
      <relation from="o21187" id="r1374" name="without" negation="false" src="d0_s7" to="o21189" />
      <relation from="o21188" id="r1375" name="without" negation="false" src="d0_s7" to="o21189" />
    </statement>
    <statement id="d0_s8">
      <text>leaflets (5–) 7, glands resin-scented when crushed, terminal: petiolule 10–17 mm, blade elliptic to ovate, 12–35 × 10–18 mm, base cuneate, sometimes rounded, margins 1–2-serrate, teeth 14–18 per side, apex acute, sometimes rounded, abaxial surfaces tomentose, resinous-glandular, sometimes eglandular, adaxial light green, dull, glabrous or mostly tomentulose.</text>
      <biological_entity id="o21190" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s8" to="7" to_inclusive="false" />
        <character name="quantity" src="d0_s8" value="7" value_original="7" />
      </biological_entity>
      <biological_entity id="o21191" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when crushed" name="odor" src="d0_s8" value="resin-scented" value_original="resin-scented" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21192" name="petiolule" name_original="petiolule" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21193" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21194" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o21195" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="1-2-serrate" value_original="1-2-serrate" />
      </biological_entity>
      <biological_entity id="o21196" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o21197" from="14" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
      <biological_entity id="o21197" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o21198" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21199" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="resinous-glandular" value_original="resinous-glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21200" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="light green" value_original="light green" />
        <character is_modifier="false" name="reflectance" src="d0_s8" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s8" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences panicles, 1 or 2 (–5) -flowered.</text>
      <biological_entity constraint="inflorescences" id="o21201" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="2(-5)-flowered" value_original="2(-5)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels erect or slightly reflexed, (5–) 15–35 mm, sparingly stipitate-glandular or eglandular;</text>
      <biological_entity id="o21202" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s10" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts (1–) 2, ovatelanceolate, 10–12 × 4–5 mm, margins irregularly glandular-serrate, surfaces glabrous or pubescent, eglandular.</text>
      <biological_entity id="o21203" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s11" to="2" to_inclusive="false" />
        <character name="quantity" src="d0_s11" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21204" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s11" value="glandular-serrate" value_original="glandular-serrate" />
      </biological_entity>
      <biological_entity id="o21205" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 3–4.5 cm diam.;</text>
      <biological_entity id="o21206" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s12" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium globose, 5–7 × 5–7 mm, stipitate-glandular or setose, neck absent;</text>
      <biological_entity id="o21207" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o21208" name="neck" name_original="neck" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals spreading, ovatelanceolate, 20–25 × (4–) 5 mm, margins entire, tip 8–10 × 0.5–1 mm, abaxially densely glandular or stipitate-glandular;</text>
      <biological_entity id="o21209" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s14" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o21210" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21211" name="tip" name_original="tip" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s14" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s14" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s14" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals deep pink, rarely white, 9–16 × 10–16 mm;</text>
      <biological_entity id="o21212" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="depth" src="d0_s15" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s15" to="16" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s15" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>carpels 50–65, styles lanate, exsert 1.5 mm beyond stylar orifice (3.5 mm diam.) of hypanthial disc (5 mm diam.).</text>
      <biological_entity id="o21213" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s16" to="65" />
      </biological_entity>
      <biological_entity id="o21214" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="lanate" value_original="lanate" />
        <character is_modifier="false" name="position" src="d0_s16" value="exsert" value_original="exsert" />
        <character constraint="beyond stylar, orifice" constraintid="o21215, o21216" name="some_measurement" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity id="o21215" name="stylar" name_original="stylar" src="d0_s16" type="structure" />
      <biological_entity id="o21216" name="orifice" name_original="orifice" src="d0_s16" type="structure" />
      <biological_entity id="o21217" name="hypanthial" name_original="hypanthial" src="d0_s16" type="structure" />
      <biological_entity id="o21218" name="disc" name_original="disc" src="d0_s16" type="structure" />
      <relation from="o21215" id="r1376" name="part_of" negation="false" src="d0_s16" to="o21217" />
      <relation from="o21215" id="r1377" name="part_of" negation="false" src="d0_s16" to="o21218" />
      <relation from="o21216" id="r1378" name="part_of" negation="false" src="d0_s16" to="o21217" />
      <relation from="o21216" id="r1379" name="part_of" negation="false" src="d0_s16" to="o21218" />
    </statement>
    <statement id="d0_s17">
      <text>Hips red to purplish, globose to depressed-globose, 11–16 × 12–15 (–20) mm, glabrous, sometimes setose, stipitate-glandular;</text>
      <biological_entity id="o21219" name="hip" name_original="hips" src="d0_s17" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s17" to="purplish" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s17" to="depressed-globose" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s17" to="16" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s17" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s17" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals persistent, erect.</text>
      <biological_entity id="o21220" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Achenes 60, dark tan to black, 4–5 × 1.5–2 mm. 2n = 28, 35, 42.</text>
      <biological_entity id="o21221" name="achene" name_original="achenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="60" value_original="60" />
        <character char_type="range_value" from="dark tan" name="coloration" src="d0_s19" to="black" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s19" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s19" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21222" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
        <character name="quantity" src="d0_s19" value="35" value_original="35" />
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, stream banks, roadsides, overgrown pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="overgrown pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Vt.; n, c Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rosa mollis is introduced from Scotland.</discussion>
  <discussion>Rosa mollis is closely allied to R. villosa Linnaeus; the two were treated as distinct by W. J. Bean (1970–1988) and A. V. Gilman (2012).</discussion>
  
</bio:treatment>