<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Döll) Grenier in J. C. M. Grenier and D. A. Godron" date="unknown" rank="section">Terminales</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. M. Grenier and D. A. Godron, Fl. France</publication_title>
      <place_in_publication>1: 522, 532. 1848–1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Terminales</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">318042</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Döll" date="unknown" rank="unranked">Terminales</taxon_name>
    <place_of_publication>
      <publication_title>Rhein. Fl.,</publication_title>
      <place_in_publication>772. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;unranked Terminales</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Argenteae</taxon_name>
    <taxon_hierarchy>genus Potentilla;unranked Argenteae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Juzepczuk" date="unknown" rank="section">Argenteae</taxon_name>
    <taxon_hierarchy>genus Potentilla;section Argenteae</taxon_hierarchy>
  </taxon_identification>
  <number>8h.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, rosetted or tufted, not stoloniferous;</text>
      <biological_entity id="o2976" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened;</text>
      <biological_entity id="o2977" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vestiture of long, short-crisped, and cottony or crisped-cottony hairs, glands absent or sparse to common, not red.</text>
      <biological_entity id="o2978" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="common" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o2979" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s2" value="short-crisped" value_original="short-crisped" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="crisped-cottony" value_original="crisped-cottony" />
      </biological_entity>
      <biological_entity id="o2980" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s2" value="short-crisped" value_original="short-crisped" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="crisped-cottony" value_original="crisped-cottony" />
      </biological_entity>
      <relation from="o2978" id="r179" name="part_of" negation="false" src="d0_s2" to="o2979" />
      <relation from="o2978" id="r180" name="part_of" negation="false" src="d0_s2" to="o2980" />
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent to erect, not flagelliform, not rooting at nodes, from centers of ephemeral basal rosettes, 1–6 dm, lengths (2–) 3–5 (–10) times basal or proximal cauline leaves.</text>
      <biological_entity id="o2981" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o2982" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="6" to_unit="dm" />
        <character constraint="leaf" constraintid="o2985" is_modifier="false" name="length" src="d0_s3" value="(2-)3-5(-10) times basal or proximal cauline leaves" />
      </biological_entity>
      <biological_entity id="o2982" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o2983" name="center" name_original="centers" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o2984" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o2985" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o2981" id="r181" name="from" negation="false" src="d0_s3" to="o2983" />
      <relation from="o2983" id="r182" name="part_of" negation="false" src="d0_s3" to="o2984" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not in ranks;</text>
      <biological_entity id="o2986" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o2987" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2988" name="rank" name_original="ranks" src="d0_s4" type="structure" />
      <relation from="o2987" id="r183" name="in" negation="false" src="d0_s4" to="o2988" />
    </statement>
    <statement id="d0_s5">
      <text>cauline 2–9;</text>
      <biological_entity id="o2989" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o2990" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary leaves usually palmate, sometimes ternate, proximal ones 2–14 cm;</text>
      <biological_entity id="o2991" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o2992" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="palmate" value_original="palmate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="ternate" value_original="ternate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2993" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs loosely appressed to spreading, soft to weak, glands absent or sparse to common;</text>
      <biological_entity id="o2994" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o2995" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character char_type="range_value" from="loosely appressed" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s7" value="soft" value_original="soft" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o2996" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s7" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets 5–7, at tip of leaf axis, ± overlapping or not, oblanceolate to obovate, margins flat or revolute, distal 1/2–3/4+ evenly to unevenly incised 1/3–3/4+ to midvein, teeth 2–10 per side, surfaces similar to strongly dissimilar, abaxial green to white, cottony and/or crisped hairs absent or sparse to dense, adaxial green, not glaucous, long hairs weak to stiff.</text>
      <biological_entity id="o2997" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o2998" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="7" />
        <character is_modifier="false" modifier="more or less" name="arrangement" notes="" src="d0_s8" value="overlapping" value_original="overlapping" />
        <character name="arrangement" src="d0_s8" value="not" value_original="not" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="obovate" />
      </biological_entity>
      <biological_entity id="o2999" name="tip" name_original="tip" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o3000" name="axis" name_original="axis" src="d0_s8" type="structure" />
      <biological_entity id="o3001" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s8" to="3/4" upper_restricted="false" />
        <character is_modifier="false" modifier="evenly to unevenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o3002" from="1/3" name="quantity" src="d0_s8" to="3/4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3002" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o3003" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o3004" from="2" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <biological_entity id="o3004" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o3005" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="cottony" value_original="cottony" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3006" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character char_type="range_value" from="green" modifier="strongly; strongly" name="coloration" src="d0_s8" to="white" />
      </biological_entity>
      <biological_entity id="o3007" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" value="sparse to dense" value_original="sparse to dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3008" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o3009" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s8" to="stiff" />
      </biological_entity>
      <relation from="o2998" id="r184" name="at" negation="false" src="d0_s8" to="o2999" />
      <relation from="o2999" id="r185" name="part_of" negation="false" src="d0_s8" to="o3000" />
      <relation from="o3005" id="r186" name="to" negation="false" src="d0_s8" to="o3006" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 10–100+-flowered, cymose, ± open.</text>
      <biological_entity id="o3010" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-100+-flowered" value_original="10-100+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels usually straight in fruit, 0.3–1.5 (–3) cm, proximal ± longer than distal.</text>
      <biological_entity id="o3011" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="in fruit" constraintid="o3012" is_modifier="false" modifier="usually" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3012" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o3013" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o3014" is_modifier="false" name="length_or_size" src="d0_s10" value="more or less longer" value_original="more or less longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3014" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o3015" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium 2–5 mm diam.;</text>
      <biological_entity id="o3016" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals yellow, obovate to cuneate-obcordate, (2–) 2.5–7 (–8) mm, slightly shorter to ± longer than sepals, apex rounded to truncate or retuse;</text>
      <biological_entity id="o3017" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s13" to="cuneate-obcordate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="size_or_length" src="d0_s13" value="shorter to more" value_original="shorter to more" />
        <character constraint="than sepals" constraintid="o3018" is_modifier="false" name="length_or_size" src="d0_s13" value="shorter to more more or less longer" />
      </biological_entity>
      <biological_entity id="o3018" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o3019" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="truncate or retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens ca. 20;</text>
      <biological_entity id="o3020" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, columnar-tapered, scarcely to strongly papillate-swollen in proximal 1/5–1/2, 0.6–1.2 mm.</text>
      <biological_entity id="o3021" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="columnar-tapered" value_original="columnar-tapered" />
        <character constraint="in proximal 1/5-1/2 , 0.6-1.2 mm" is_modifier="false" modifier="scarcely to strongly" name="shape" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth to rugose.</text>
      <biological_entity id="o3022" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s16" to="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia; also introduced in Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="also  in Pacific Islands (New Zealand)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 20–30 (3 in the flora).</discussion>
  <discussion>As summarized by A. Kurtto et al. (in J. Jalas et al. 1972+, vol. 13), the species comprising sect. Terminales (including sect. Argenteae) consist of both sexual and apomictic populations of various ploidy levels that can be subdivided into more or less consistent species. The three species adventive in North America are relatively distinct, representing only a subset of European variation. Collections of Potentilla inclinata and P. intermedia are sometimes confused; the former often has petals smaller than the European average, and anthers are often intermediate in size. The distribution of the two species in North America may need adjusting from what is presented here.</discussion>
  <discussion>Another species complex, the Potentilla collina Wibel group (as addressed by A. Kurtto et al. in J. Jalas et al. 1972+, vol. 13), may be present in North America, at least as an occasional waif. The specimens underlying the citation of this species by P. A. Rydberg (1898, 1908d) are here identified as P. argentea (New York) and P. inclinata (Minnesota); however, variation of traits distinguishing members of sect. Terminales can be subtle and difficult to interpret out of their European context.</discussion>
  <discussion>Potentilla intermedia is considered to be of hybrid origin involving P. argentea and P. norvegica; it appears to reproduce by both sexual and apomictic means. Some authors consider P. inclinata to be the hybrid derivative of P. argentea and P. recta (A. Kurtto et al. in J. Jalas et al. 1972+, vol. 13). The P. collina group is likewise thought to have a hybrid origin, involving members of sections Aureae and Terminales. Placement of these species in sect. Terminales is made on the basis of key morphologic characters.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet surfaces strongly dissimilar, abaxial white, cottony hairs dense, distal 1/2–2/3 of margin incised 1/2–3/4+ to midvein, teeth 2–3 per side (more if lobed or secondarily toothed).</description>
      <determination>15 Potentilla argentea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet surfaces similar or ± dissimilar, abaxial green, grayish, or gray-green, cottony hairs absent, distal (1/2–)3/4+ of margin incised 1/3–2/3 to midvein, teeth 4–10 per side</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Anthers (0.5–)0.8–1.2 mm; petals 4–7(–8) mm; leaflets grayish to gray-green abaxially, short or crisped hairs usually ± abundant, sometimes sparse, margins usually evenly incised; epicalyx bractlets: lengths ± 1 times sepals.</description>
      <determination>16 Potentilla inclinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Anthers 0.3–0.5 mm; petals 3–5 mm; leaflets green to grayish green abaxially, short or crisped hairs ± sparse, margins usually unevenly, sometimes evenly, incised (often with 1–2 incisions nearly to midvein); epicalyx bractlets: lengths usually 2/3, sometimes 1, times sepals.</description>
      <determination>17 Potentilla intermedia</determination>
    </key_statement>
  </key>
</bio:treatment>