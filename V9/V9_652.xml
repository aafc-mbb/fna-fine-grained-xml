<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James Henrickson,Bruce D. Parfitt†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Focke in H. G. A. Engler and K. Prantl" date="1888" rank="tribe">kerrieae</taxon_name>
    <taxon_name authority="Torrey" date="1851" rank="genus">COLEOGYNE</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Assoc. Advancem. Sci.</publication_title>
      <place_in_publication>4: 192. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe kerrieae;genus COLEOGYNE</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek koleos, sheath, and gyne, female, alluding to thin staminal tubelike sheath surrounding ovary and style</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">107634</other_info_on_name>
  </taxon_identification>
  <number>40.</number>
  <other_name type="common_name">Blackbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2–6 (–10) dm, herbage strigose, with appressed medifixed hairs.</text>
      <biological_entity id="o11587" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o11588" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o11589" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s0" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="fixation" src="d0_s0" value="medifixed" value_original="medifixed" />
      </biological_entity>
      <relation from="o11588" id="r723" name="with" negation="false" src="d0_s0" to="o11589" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 15–150, stiffly erect;</text>
      <biological_entity id="o11590" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s1" to="150" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark dark russet, becoming gray, striate;</text>
      <biological_entity id="o11591" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark russet" value_original="dark russet" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s2" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>long-shoots divaricately and oppositely branched, short-shoots present;</text>
      <biological_entity id="o11592" name="long-shoot" name_original="long-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="oppositely" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>unarmed (stiff dead branches resembling thorns).</text>
      <biological_entity id="o11593" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unarmed" value_original="unarmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent (drought deciduous), cauline, opposite and clustered on short-shoots;</text>
      <biological_entity id="o11594" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o11595" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character constraint="on short-shoots" constraintid="o11596" is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o11596" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stipules persistent on distal leaf-base margins, deltate-lanceolate, margins entire;</text>
      <biological_entity id="o11597" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character constraint="on distal leaf-base margins" constraintid="o11598" is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="deltate-lanceolate" value_original="deltate-lanceolate" />
      </biological_entity>
      <biological_entity constraint="leaf-base" id="o11598" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="distal leaf-base" />
      <biological_entity id="o11599" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole obscure;</text>
      <biological_entity id="o11600" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate-spatulate, 0.6–1.2 cm, thickened, leathery, margins thick, rounded (not revolute), entire.</text>
      <biological_entity id="o11601" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate-spatulate" value_original="oblanceolate-spatulate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s8" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="texture" src="d0_s8" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o11602" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal on long and short-shoot spurs, flowers solitary;</text>
      <biological_entity id="o11603" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character constraint="on short-shoot spurs" constraintid="o11604" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="short-shoot" id="o11604" name="spur" name_original="spurs" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o11605" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts present, reduced, leaflike.</text>
      <biological_entity id="o11606" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s10" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="leaflike" value_original="leaflike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels reduced.</text>
      <biological_entity id="o11607" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 11–14 mm diam.;</text>
      <biological_entity id="o11608" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="diameter" src="d0_s12" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>epicalyx bractlets 0;</text>
      <biological_entity constraint="epicalyx" id="o11609" name="bractlet" name_original="bractlets" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium campanulate, 1–2 mm;</text>
      <biological_entity id="o11610" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 4 (rarely 5), weakly spreading, abaxially yellow, outer narrowly ovate, inner broadly ovate, margins thin;</text>
      <biological_entity id="o11611" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="4" value_original="4" />
        <character is_modifier="false" modifier="weakly" name="orientation" src="d0_s15" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="position" src="d0_s15" value="outer" value_original="outer" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="position" src="d0_s15" value="inner" value_original="inner" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o11612" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals usually 0, infrequently 1–4 (or 5) on scattered flowers, caducous, translucent yellow, spatulate;</text>
      <biological_entity id="o11613" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="on flowers" constraintid="o11614" from="1" modifier="infrequently" name="quantity" src="d0_s16" to="4" />
        <character is_modifier="false" name="duration" notes="" src="d0_s16" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="translucent yellow" value_original="translucent yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o11614" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s16" value="scattered" value_original="scattered" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens (16–) 20–25 (–30), equal to sepals;</text>
      <biological_entity id="o11615" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="16" name="atypical_quantity" src="d0_s17" to="20" to_inclusive="false" />
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="30" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s17" to="25" />
        <character constraint="to sepals" constraintid="o11616" is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11616" name="sepal" name_original="sepals" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>torus extending into tubelike sheath surrounding ovary and proximal style (interior sericeous, exterior bearing some stamens);</text>
      <biological_entity id="o11617" name="torus" name_original="torus" src="d0_s18" type="structure" />
      <biological_entity id="o11618" name="sheath" name_original="sheath" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="tubelike" value_original="tubelike" />
      </biological_entity>
      <biological_entity id="o11619" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s18" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o11620" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s18" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <relation from="o11617" id="r724" name="extending into" negation="false" src="d0_s18" to="o11618" />
    </statement>
    <statement id="d0_s19">
      <text>carpels 1 (rarely 2), glabrous except at style base, styles lateral, exserted beyond sheath, sericeous most of length;</text>
      <biological_entity id="o11621" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character constraint="except at base" constraintid="o11622" is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11622" name="base" name_original="base" src="d0_s19" type="structure" constraint="style" />
      <biological_entity id="o11623" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="length" src="d0_s19" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o11624" name="sheath" name_original="sheath" src="d0_s19" type="structure" />
      <relation from="o11623" id="r725" name="exserted beyond" negation="false" src="d0_s19" to="o11624" />
    </statement>
    <statement id="d0_s20">
      <text>ovule 1, lateral.</text>
      <biological_entity id="o11625" name="ovule" name_original="ovule" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s20" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits achenes, 1 (–2), brown, ovoid-reniform, distally narrower, 5.3–5.8 mm, shiny, glabrous;</text>
      <biological_entity constraint="fruits" id="o11626" name="achene" name_original="achenes" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="2" />
        <character name="quantity" src="d0_s21" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid-reniform" value_original="ovoid-reniform" />
        <character is_modifier="false" modifier="distally" name="width" src="d0_s21" value="narrower" value_original="narrower" />
        <character char_type="range_value" from="5.3" from_unit="mm" name="some_measurement" src="d0_s21" to="5.8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s21" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hypanthium persistent;</text>
      <biological_entity id="o11627" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, ascending.</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 8.</text>
      <biological_entity id="o11628" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s23" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="x" id="o11629" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Coleogyne is a xeromorphic desert shrub with floral features unusual within Rosaceae: a peculiar sheathlike torus surrounds the carpel and most of the style that bears some of the stamens, four sepals, and petals commonly absent but the inner sepal surface strongly yellow; vegetatively, Coleogyne is further distinguished by its opposite leaves and uniformly oriented medifixed trichomes. The single-ovuled, solitary ovary has a lateral style, suggesting relationship with multi-ovuled ancestors. Molecular data support relationships in the Kerrieae in subfam. Amygdaloideae (D. Potter et al. 2007).</discussion>
  
</bio:treatment>