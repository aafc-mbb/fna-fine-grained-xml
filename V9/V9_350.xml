<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">230</other_info_on_meta>
    <other_info_on_meta type="mention_page">221</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">IVESIA</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section IVESIA</taxon_hierarchy>
    <other_info_on_name type="fna_id">318019</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Lycopodioides</taxon_name>
    <taxon_hierarchy>genus Horkelia;unranked Lycopodioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="unranked">Lycopodioides</taxon_name>
    <taxon_hierarchy>genus Ivesia;unranked Lycopodioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens" date="unknown" rank="section">Lycopodioides</taxon_name>
    <taxon_hierarchy>genus Ivesia;section Lycopodioides</taxon_hierarchy>
  </taxon_identification>
  <number>9b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually rosetted or tufted, rarely ± matted (I. pygmaea), not forming hanging clumps (not in vertical rock crevices except I. longibracteata), ± aromatic;</text>
      <biological_entity id="o28818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s0" value="rosetted" value_original="rosetted" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="rarely more or less" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character is_modifier="false" modifier="not; more or less" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o28819" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="hanging" value_original="hanging" />
      </biological_entity>
      <relation from="o28818" id="r1884" name="forming" negation="true" src="d0_s0" to="o28819" />
    </statement>
    <statement id="d0_s1">
      <text>taproot stout to fusiform and fleshy.</text>
      <biological_entity id="o28820" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (0.2–) 0.3–2 (–4) dm.</text>
      <biological_entity id="o28821" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves usually loosely to very tightly cylindric (mousetail-like in I. muirii), rarely weakly planar (I. longibracteata);</text>
      <biological_entity constraint="basal" id="o28822" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely to very tightly" name="shape" src="d0_s3" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="rarely weakly" name="shape" src="d0_s3" value="planar" value_original="planar" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules present;</text>
      <biological_entity id="o28823" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets overlapping, individually distinguishable or not, lobed to base, sparsely to densely hairy, sometimes glabrous or glabrate;</text>
      <biological_entity id="o28824" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="individually" name="prominence" src="d0_s5" value="distinguishable" value_original="distinguishable" />
        <character name="prominence" src="d0_s5" value="not" value_original="not" />
        <character constraint="to base" constraintid="o28825" is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" notes="" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o28825" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>terminal leaflets indistinct.</text>
      <biological_entity constraint="terminal" id="o28826" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (0–) 1–3, sometimes paired;</text>
      <biological_entity constraint="cauline" id="o28827" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade usually ± reduced to vestigial, rarely well developed.</text>
      <biological_entity id="o28828" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" modifier="rarely well" name="development" src="d0_s8" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences usually ± congested, sometimes becoming open in fruit, flowers mostly arranged in 1–few (–several in I. gordonii var. wasatchensis) loose to capitate glomerules.</text>
      <biological_entity id="o28829" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="architecture_or_arrangement" src="d0_s9" value="congested" value_original="congested" />
        <character constraint="in fruit" constraintid="o28830" is_modifier="false" modifier="sometimes becoming" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o28830" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o28831" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character constraint="in glomerules" constraintid="o28832" is_modifier="false" modifier="mostly" name="arrangement" src="d0_s9" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o28832" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="1-few" value_original="1-few" />
        <character char_type="range_value" from="loose" is_modifier="true" name="architecture" src="d0_s9" to="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels remaining straight.</text>
      <biological_entity id="o28833" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers: hypanthium shallowly cupulate or campanulate, sometimes turbinate (I. gordonii);</text>
      <biological_entity id="o28834" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28835" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s11" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="turbinate" value_original="turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals not medially reflexed, golden to pale-yellow, sometimes white (I. utahensis) and then sometimes pink-tinged, not or scarcely clawed, apex acute to truncate, rounded, or emarginate;</text>
      <biological_entity id="o28836" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o28837" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not medially" name="orientation" src="d0_s12" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="golden" name="coloration" src="d0_s12" to="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="white and then sometimes pink-tinged" />
        <character is_modifier="false" modifier="sometimes; not; scarcely" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o28838" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="truncate rounded or emarginate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="truncate rounded or emarginate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="truncate rounded or emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 5 (10 in I. pygmaea), anthers longer than wide, laterally dehiscent;</text>
      <biological_entity id="o28839" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o28840" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o28841" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="longer than wide" value_original="longer than wide" />
        <character is_modifier="false" modifier="laterally" name="dehiscence" src="d0_s13" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels (1–) 2–15 (–30 in I. pygmaea).</text>
      <biological_entity id="o28842" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o28843" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s14" to="2" to_inclusive="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s14" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes vertical, smooth or nearly so, not carunculate.</text>
      <biological_entity id="o28844" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s15" value="nearly" value_original="nearly" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="carunculate" value_original="carunculate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 8 (8 in the flora).</discussion>
  <discussion>Species in sect. Ivesia tend to form compact rosettes or tufts in open montane to alpine areas, often where rocky but generally not growing in rock crevices (except Ivesia longibracteata). General characteristics of the section are an overall evident glandularity, loosely to tightly cylindric leaves (mousetail-like in I. muirii), relatively short ascending to erect stems, straight pedicels, and flowers that are typically congested into 1–few capitate clusters that sometimes become more open in fruit. The taproots of some species, especially I. lycopodioides, are markedly enlarged, an adaptation to alpine growing conditions. The inclusion of two anomalous species in the section, I. longibracteata and I. webberi, is tentative; it is also possible that I. cryptocaulis belongs here rather than in sect. Setosae.</discussion>
  <discussion>Section Ivesia has its center of species diversity in the high Sierra Nevada of California, where Ivesia pygmaea, with its ten stamens and more open habit, is possibly transitional between sects. Ivesia and Setosae. Ivesia gordonii (the most widespread species), I. tweedyi, and I. utahensis form an arc around the northern Great Basin, as outliers from the core Sierran distribution of the section.</discussion>
  <discussion>Some early floras treated Ivesia pygmaea and I. lycopodioides as varieties of I. gordonii (for example, W. H. Brewer et al. 1876–1880, vol. 1) or Potentilla gordonii (Hooker) Greene (W. L. Jepson [1923–1925], 1909–1943, vol. 2). Recent treatments follow D. D. Keck (1938) in recognizing all three as distinct species; some annotations and references are nevertheless carried over from the earlier expanded circumscription of I. gordonii.</discussion>
  <discussion>Since Ivesia cryptocaulis is sometimes identified as a member of sect. Ivesia, it is included herein and keys out in the seventh couplet.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Epicalyx bractlets longer than sepals, 2.5–5 mm; leaves weakly planar to loosely ± cylindric; leaflets 5–6 per side; Castle Crags, n California.</description>
      <determination>18 Ivesia longibracteata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Epicalyx bractlets shorter than sepals, 0.5–3(–4 in I. gordonii) mm; leaves loosely to tightly cylindric; leaflets mostly 10–40 per side (4–10 in I. webberi); w United States</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants silvery; leaves very tightly cylindric (mousetail-like, with leaflets scarcely distinguishable); leaflets 0.4–1 mm.</description>
      <determination>17 Ivesia muirii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants ± green; leaves loosely to tightly cylindric; leaflets distinguishable, (0.5–)2–13(–18) mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cauline leaves 2, paired; basal leaves: leaflets 4–8(–10) per side; petiole hairs 2–4 mm.</description>
      <determination>19 Ivesia webberi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cauline leaves 0–2(–3), not paired; basal leaves: leaflets (6–)10–35 per side; petiole hairs 0.2–1.5 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypanthia turbinate to campanulate, (1.5–)2–4(–4.5) mm (± as deep as wide); petals narrowly oblanceolate to narrowly spatulate.</description>
      <determination>15 Ivesia gordonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypanthia shallowly cupulate or shallowly campanulate, 1–2 mm (wider than deep); petals oblanceolate or broadly elliptic to spatulate or broadly obovate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Carpels (1–)2–6(–9); Idaho, Utah, Washington</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Carpels (5–)8–30; e California, Nevada</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals white, sometimes pink-tinged; stems prostrate to ascending; leaflets 2–4 mm; Utah.</description>
      <determination>14 Ivesia utahensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petals golden yellow; stems ascending to erect; leaflets 4–7(–10) mm; n Idaho, Washington.</description>
      <determination>16 Ivesia tweedyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants diffusely matted; Spring Mountains, s Nevada.</description>
      <determination>9 Ivesia cryptocaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants rosetted to tufted or ± tightly matted; Sierra Nevada, White Mountains, and Sweetwater Mountains of e California and adjacent w Nevada</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stamens 10; sheathing bases usually ± strigose abaxially; taproots stout, not fleshy.</description>
      <determination>12 Ivesia pygmaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stamens 5; sheathing bases glabrous abaxially; taproots fusiform, fleshy.</description>
      <determination>13 Ivesia lycopodioides</determination>
    </key_statement>
  </key>
</bio:treatment>