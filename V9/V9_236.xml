<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">163</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Leucophyllae</taxon_name>
    <taxon_name authority="Douglas ex Lehmann" date="1830" rank="species">effusa</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Stirp. Pug.</publication_title>
      <place_in_publication>2: 8. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section leucophyllae;species effusa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100321</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Lehmann" date="unknown" rank="species">hippiana</taxon_name>
    <taxon_name authority="(Douglas ex Lehmann) Dorn" date="unknown" rank="subspecies">effusa</taxon_name>
    <taxon_hierarchy>genus Potentilla;species hippiana;subspecies effusa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hippiana</taxon_name>
    <taxon_name authority="(Douglas ex Lehmann) Dorn" date="unknown" rank="variety">effusa</taxon_name>
    <taxon_hierarchy>genus P.;species hippiana;variety effusa</taxon_hierarchy>
  </taxon_identification>
  <number>31.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (0.5–) 1.5–4 (–5.5) dm, lengths 2–3 (–4) times basal leaves.</text>
      <biological_entity id="o1968" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5.5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character constraint="leaf" constraintid="o1969" is_modifier="false" name="length" src="d0_s0" value="2-3(-4) times basal leaves" value_original="2-3(-4) times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1969" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves pinnate, (3–) 5–15 (–25) cm;</text>
      <biological_entity constraint="basal" id="o1970" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–5 (–8) cm, long hairs absent to abundant, ± appressed, 1–3 mm, stiff to weak, short and crisped hairs absent or obscured, cottony hairs abundant (at least on first-formed leaves), glands sparse or obscured;</text>
      <biological_entity id="o1971" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1972" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character char_type="range_value" from="absent" name="quantity" src="d0_s2" to="abundant" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="stiff" name="fragility" src="d0_s2" to="weak" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o1973" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o1974" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o1975" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets not conduplicate, lateral ones evenly to unevenly paired, 2–5 (–7) per side (secondary leaflets sometimes interspersed) on distal 1/3–1/2 of leaf axis, distal pairs usually not decurrent or confluent with terminal leaflet, larger leaflets oblanceolate to narrowly obovate, 1–2.5 (–3) × 0.3–1.3 (–1.8) cm, distal (1/2–) 2/3–3/4 (rarely more) of margin incised ± 1/2 to midvein, teeth (1–) 2–9 per side, 1–4 mm, surfaces similar, gray to white or ± green, abaxial: long hairs sparse to common (at least on veins), 0.5–1.5 mm, weak to stiff, short or crisped hairs absent or sparse, sometimes obscured, cottony hairs absent or sparse to dense, glands absent, sparse, or obscured, adaxial: long hairs absent or sparse to common, short or crisped hairs absent or sparse, cottony hairs absent or sparse to abundant, glands absent or sparse.</text>
      <biological_entity id="o1976" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="evenly to unevenly" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="7" />
        <character char_type="range_value" constraint="per side" constraintid="o1977" from="2" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o1977" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o1978" name="1/3-1/2" name_original="1/3-1/2" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o1979" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o1980" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character constraint="with terminal leaflet" constraintid="o1981" is_modifier="false" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o1981" name="leaflet" name_original="leaflet" src="d0_s3" type="structure" />
      <biological_entity constraint="larger" id="o1982" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly obovate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1983" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="of margin" constraintid="o1984" from="1/2" name="quantity" src="d0_s3" to="2/3-3/4" />
        <character constraint="to midvein" constraintid="o1985" name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1984" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o1985" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o1986" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s3" to="2" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o1987" from="2" name="quantity" src="d0_s3" to="9" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1987" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o1988" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s3" to="white or more or less green" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1989" name="petiole" name_original="petioles" src="d0_s3" type="structure" />
      <biological_entity id="o1990" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="stiff" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o1991" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o1992" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s3" value="sparse to dense" value_original="sparse to dense" />
      </biological_entity>
      <biological_entity id="o1993" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1994" name="petiole" name_original="petioles" src="d0_s3" type="structure" />
      <biological_entity id="o1995" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o1996" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o1997" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="abundant" />
      </biological_entity>
      <biological_entity id="o1998" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o1977" id="r126" name="on" negation="false" src="d0_s3" to="o1978" />
      <relation from="o1978" id="r127" name="part_of" negation="false" src="d0_s3" to="o1979" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 2–6+.</text>
      <biological_entity constraint="cauline" id="o1999" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 7–30-flowered.</text>
      <biological_entity id="o2000" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="7-30-flowered" value_original="7-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.3–3 cm.</text>
      <biological_entity id="o2001" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets linear to narrowly lanceolate, 1–2.5 (–3) × 0.3–1 mm, 1/2–2/3 as long as sepals, abaxial vestiture often much sparser than sepals, often glabrate or glabrescent distally, straight hairs absent or sparse, cottony hairs usually abundant (at least proximally), sometimes absent or nearly so (var. rupincola);</text>
      <biological_entity id="o2002" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o2003" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="narrowly lanceolate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s7" to="2/3" />
      </biological_entity>
      <biological_entity id="o2004" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o2005" name="vestiture" name_original="vestiture" src="d0_s7" type="structure">
        <character constraint="than sepals" constraintid="o2006" is_modifier="false" name="arrangement_or_density" src="d0_s7" value="often much sparser" value_original="often much sparser" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s7" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o2006" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o2007" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o2008" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s7" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 2–6 mm diam.;</text>
      <biological_entity id="o2009" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2010" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 3–6 (–7) mm, apex long acuminate;</text>
      <biological_entity id="o2011" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2012" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2013" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals 4–6.5 × 4–6 mm;</text>
      <biological_entity id="o2014" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2015" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–3.5 mm, anthers 0.5–1 mm;</text>
      <biological_entity id="o2016" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2017" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2018" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 3–15, styles 1.5–2.1 mm.</text>
      <biological_entity id="o2019" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2020" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s12" to="15" />
      </biological_entity>
      <biological_entity id="o2021" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1.8–2.1 mm, smooth.</text>
      <biological_entity id="o2022" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Colo., Idaho, Minn., Mont., N.Dak., N.Mex., Nebr., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>In the southern Rocky Mountains, Potentilla effusa is relatively distinct from P. hippiana both morphologically and geographically, with P. effusa mostly east of the Continental Divide and P. hippiana mostly west. In general, P. hippiana differs from P. effusa in having leaflets more evenly paired, more tightly serrate with teeth often occurring on the whole margin, and more bicolored with abaxial vestiture of crisped rather than cottony hairs. Stems of P. effusa have more cauline leaves on average than P. hippiana, and vestiture of epicalyx bractlets of P. effusa (as here circumscribed) is never sericeous.</discussion>
  <discussion>The distinction between the two species is marred by intermediate populations throughout their shared range in the northern Rocky Mountains and Great Plains. Most regional floras focused on these areas have accordingly included Potentilla effusa within P. hippiana, sometimes without infraspecific distinction. An alternate solution has been the recognition of P. hippiana subsp. effusa, with the subspecies further subdivided into the two varieties recognized here. The varieties themselves intergrade; P. effusa var. ×coloradensis (Rydberg) Th. Wolf is available for the hybrid.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets gray to white, surfaces moderately long-hairy and moderately to densely cottony.</description>
      <determination>31a Potentilla effusa var. effusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets ± green, surfaces glabrous or sparsely to moderately long-hairy (at least on veins) and not or sparsely cottony.</description>
      <determination>31b Potentilla effusa var. rupincola</determination>
    </key_statement>
  </key>
</bio:treatment>