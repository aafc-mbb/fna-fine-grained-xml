<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="Thory" date="1820" rank="section">Laevigatae</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Monogr. Rosier,</publication_title>
      <place_in_publication>37. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section Laevigatae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">318047</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rosa</taxon_name>
    <taxon_name authority="Crépin" date="unknown" rank="section">Sinicae</taxon_name>
    <taxon_hierarchy>genus Rosa;section Sinicae</taxon_hierarchy>
  </taxon_identification>
  <number>7b.3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen, forming dense thickets;</text>
      <biological_entity id="o31714" name="thicket" name_original="thickets" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o31713" id="r2115" name="forming" negation="false" src="d0_s0" to="o31714" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes relatively long.</text>
      <biological_entity id="o31713" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o31715" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems climbing, 20–75 (–100) dm;</text>
      <biological_entity id="o31716" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s2" value="climbing" value_original="climbing" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="100" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s2" to="75" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches glabrous, eglandular;</text>
    </statement>
    <statement id="d0_s4">
      <text>infrastipular and internodal prickles paired or single, curved or hooked, ± flattened, stout.</text>
      <biological_entity constraint="distal" id="o31717" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o31718" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="false" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s4" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent, 4–8 cm, leathery;</text>
      <biological_entity id="o31719" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules caducous, ± free from petiole, margins fimbriate, some teeth gland-tipped;</text>
      <biological_entity id="o31720" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character constraint="from petiole" constraintid="o31721" is_modifier="false" modifier="more or less" name="fusion" src="d0_s6" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o31721" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o31722" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o31723" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 3 (–5), terminal: petiolule 9–13 mm, blade ovate-elliptic or lanceolate, 40–65 × 30–40 mm, abaxial surfaces glabrous, eglandular, adaxial lustrous.</text>
      <biological_entity id="o31724" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="5" />
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o31725" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31726" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s7" to="65" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31727" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o31728" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s7" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 1 (or 2) -flowered.</text>
      <biological_entity id="o31729" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-flowered" value_original="1-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, stout, 12–30 mm, glabrous, eglandular, setae apical or absent;</text>
      <biological_entity id="o31730" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o31731" name="seta" name_original="setae" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="apical" value_original="apical" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts caducous, 0 or 1 (or 2), margins serrate, most teeth gland-tipped.</text>
      <biological_entity id="o31732" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
        <character name="presence" src="d0_s10" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o31733" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o31734" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5.5–8 (–10) cm diam.;</text>
      <biological_entity id="o31735" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="diameter" src="d0_s11" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium cupulate, densely setose, eglandular;</text>
      <biological_entity id="o31736" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals ± persistent, erect, lanceolate or ovatelanceolate, 18–35 × 4 mm, margins entire, abaxial surfaces glabrous or sparsely pubescent, stipitate-glandular;</text>
      <biological_entity id="o31737" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s13" to="35" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o31738" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31739" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, usually white, rarely rose;</text>
      <biological_entity id="o31740" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s14" value="rose" value_original="rose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 45–50, styles free, glabrous or pilose, hypanthium orifice 1.5–2 mm diam., hypanthial disc conic, 6–8 mm diam.</text>
      <biological_entity id="o31741" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s15" to="50" />
      </biological_entity>
      <biological_entity id="o31742" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o31743" name="orifice" name_original="orifice" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="hypanthial" id="o31744" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Hips dark red, ovoid to oblong, 20–35 × 9–20 mm, glabrous, densely setose (with age only setae remnants), eglandular;</text>
      <biological_entity id="o31745" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark red" value_original="dark red" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s16" to="oblong" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s16" to="35" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s16" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals tardily deciduous, spreading to reflexed.</text>
      <biological_entity id="o31746" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s17" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basiparietal.</text>
      <biological_entity id="o31747" name="achene" name_original="achenes" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia; introduced also in West Indies, s Africa, Atlantic Islands, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="also in West Indies" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>