<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="illustration_page">330</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="de Candolle ex Poiret in J. Lamarck et al." date="unknown" rank="genus">purshia</taxon_name>
    <taxon_name authority="(Pursh) de Candolle" date="1818" rank="species">tridentata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London</publication_title>
      <place_in_publication>12: 158. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus purshia;species tridentata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220011274</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tigarea</taxon_name>
    <taxon_name authority="Pursh" date="unknown" rank="species">tridentata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 333, plate 15. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tigarea;species tridentata</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Bitterbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, evergreen or partially summer–winter-deciduous, 2–25 (–40) dm.</text>
      <biological_entity id="o10179" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="partially" name="duration" src="d0_s0" value="summer-winter-deciduous" value_original="summer-winter-deciduous" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: young-stem internodes 4–12 mm, sparsely to densely arachnoid-villous, ± hirtellous, or glabrous, stipitate-glandular or not;</text>
      <biological_entity id="o10180" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="young-stem" id="o10181" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s1" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s1" value="arachnoid-villous" value_original="arachnoid-villous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character name="pubescence" src="d0_s1" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>short-shoot spurs simple or branched, 5–20 (–60) × 1.5–2 mm.</text>
      <biological_entity id="o10182" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity constraint="short-shoot" id="o10183" name="spur" name_original="spurs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: blade of 1 or 2 types: (1) persistent, dark green adaxially, obovate to spatulate, 4–9 × (1–) 3–6 mm, irregularly lobed in distal 1/3, lobes 3 (–5), lobes entire, ± terete, central lobe deflexed, often apiculate, 2 lateral ascending, margins strongly revolute, abaxial surface arachnoid-villous, midvein glabrous, glabrate, or hirtellous, adaxial glabrous, sometimes arachnoid-villous or hirtellous, often glandular-punctate along margins;</text>
      <biological_entity id="o10184" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10185" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character modifier="of 1 or 2types" name="atypical_quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="dark green" value_original="dark green" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
        <character constraint="in distal 1/3" constraintid="o10186" is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10186" name="1/3" name_original="1/3" src="d0_s3" type="structure" />
      <biological_entity id="o10187" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="5" />
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o10188" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity constraint="central" id="o10189" name="lobe" name_original="lobe" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s3" value="apiculate" value_original="apiculate" />
        <character name="quantity" src="d0_s3" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o10190" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10191" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="arachnoid-villous" value_original="arachnoid-villous" />
      </biological_entity>
      <biological_entity id="o10192" name="midvein" name_original="midvein" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10193" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="arachnoid-villous" value_original="arachnoid-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirtellous" value_original="hirtellous" />
        <character constraint="along margins" constraintid="o10194" is_modifier="false" modifier="often" name="coloration_or_relief" src="d0_s3" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
      <biological_entity id="o10194" name="margin" name_original="margins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>(2) winter deciduous, green-gray adaxially, flabelliform, 6–15+ × 3–7+ mm, margins weakly revolute, usually 3-toothed, sometimes 3-lobed, distally, abaxial surface arachnoid-villous to arachnoid-tomentose, veins ± glabrate, adaxial hirtellous and ± arachnoid villous, sometimes sessile or stipitate-glandular along margins.</text>
      <biological_entity id="o10195" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character name="atypical_quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="season" src="d0_s4" value="winter" value_original="winter" />
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s4" value="green-gray" value_original="green-gray" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flabelliform" value_original="flabelliform" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o10196" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="3-toothed" value_original="3-toothed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10197" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character char_type="range_value" from="arachnoid-villous" modifier="distally" name="pubescence" src="d0_s4" to="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity id="o10198" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10199" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="arachnoid" value_original="arachnoid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character constraint="along margins" constraintid="o10200" is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o10200" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Pedicels 0.2–2.5 mm.</text>
      <biological_entity id="o10201" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers: hypanthium obconic, 2.5–5 × 1.5–3 mm, 3.5–6 mm diam. in fruit, pubescent-sericeous, sessile or stipitate-glandular;</text>
      <biological_entity id="o10202" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o10203" name="hypanthium" name_original="hypanthium" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obconic" value_original="obconic" />
        <character name="diameter" src="d0_s6" unit="mm" value="2.5-5×1.5-3" value_original="2.5-5×1.5-3" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10204" from="3.5" from_unit="mm" name="diameter" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="pubescent-sericeous" value_original="pubescent-sericeous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o10204" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>petals usually lemon to cream-yellow, sometimes nearly white, obovate-spatulate, 4–7+ mm;</text>
      <biological_entity id="o10205" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o10206" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually lemon" name="coloration" src="d0_s7" to="cream-yellow" />
        <character is_modifier="false" modifier="sometimes nearly" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stamens 17–35;</text>
      <biological_entity id="o10207" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10208" name="stamen" name_original="stamens" src="d0_s8" type="structure">
        <character char_type="range_value" from="17" name="quantity" src="d0_s8" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>carpels 1 or 2.</text>
      <biological_entity id="o10209" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10210" name="carpel" name_original="carpels" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s9" unit="or" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Achenes broadly obovoid-fusiform (circular in cross-section or flattened on common side when paired), 7–12 × 3.5–5.2 mm, 15–22-veined, persistent style 5–7 (–10) mm, densely hirtellous.</text>
      <biological_entity id="o10211" name="achene" name_original="achenes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovoid-fusiform" value_original="obovoid-fusiform" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="5.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="15-22-veined" value_original="15-22-veined" />
      </biological_entity>
      <biological_entity id="o10212" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="true" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., N.Mex., Nebr., Nev., Oreg., Utah, Wash., Wyo.; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves dimorphic, blades flabelliform, thin, margins weakly revolute, usually 3-toothed, sometimes 3-lobed, apically, adaxial surfaces persistently arachnoid-villous and hirtellous, margins rarely with sessile or stipitate glands, these not strongly resinous, (at least proximalmost short-shoot leaves, smaller, thick, 3-lobed distally, margins with or without resinous glands); stems: young long shoots arachnoid-villous to pubescent, stipitate-glandular or not.</description>
      <determination>1a Purshia tridentata var. tridentata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves monomorphic, blades obovate, coriaceous-thickened, 3(–5)-lobed, lobes oblong-linear, central lobes deflexed, 2 lateral ascending, margins strongly revolute, adaxial surfaces glabrous, sometimes weakly arachnoid-villous or hirtellous, margins often with large, resiniferous, punctate glands; stems: young long shoots glabrous or sparsely hirtellous, often stipitate-glandular.</description>
      <determination>1b Purshia tridentata var. glandulosa</determination>
    </key_statement>
  </key>
</bio:treatment>