<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="illustration_page">245</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="genus">horkelia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">HORKELIA</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="1827" rank="species">californica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus horkelia;section horkelia;species californica;variety californica;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100643</other_info_on_name>
  </taxon_identification>
  <number>12a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves 8–40 cm;</text>
      <biological_entity constraint="basal" id="o3536" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sheathing bases ± strigose;</text>
      <biological_entity id="o3537" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lateral leaflets 4–7 (–9) per side, ovate to round, 10–40 mm, cleft ± 1/2 (–3/4) to midrib, ultimate teeth 10–50;</text>
      <biological_entity constraint="lateral" id="o3538" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="9" />
        <character char_type="range_value" constraint="per side" constraintid="o3539" from="4" name="quantity" src="d0_s2" to="7" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s2" to="round" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
        <character char_type="range_value" constraint="to midrib" constraintid="o3540" from="1/2" name="quantity" src="d0_s2" to="3/4" />
      </biological_entity>
      <biological_entity id="o3539" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o3540" name="midrib" name_original="midrib" src="d0_s2" type="structure" />
      <biological_entity constraint="ultimate" id="o3541" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s2" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet 10–40 mm.</text>
      <biological_entity constraint="terminal" id="o3542" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: epicalyx bractlets often toothed;</text>
      <biological_entity id="o3543" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="epicalyx" id="o3544" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>hypanthium interior ± pilose;</text>
      <biological_entity id="o3545" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o3546" name="hypanthium" name_original="hypanthium" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="interior" value_original="interior" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals often red-mottled adaxially;</text>
      <biological_entity id="o3547" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o3548" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often; adaxially" name="coloration" src="d0_s6" value="red-mottled" value_original="red-mottled" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 1.5–3 × 0.5–1 mm;</text>
      <biological_entity id="o3549" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3550" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>carpels 80–200;</text>
      <biological_entity id="o3551" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3552" name="carpel" name_original="carpels" src="d0_s8" type="structure">
        <character char_type="range_value" from="80" name="quantity" src="d0_s8" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 3–4 mm. 2n = 56.</text>
      <biological_entity id="o3553" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3554" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3555" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Margins of coastal scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="margins" constraint="of coastal scrub" />
        <character name="habitat" value="coastal scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety californica is known from the immediate coast from Humboldt to San Luis Obispo counties.</discussion>
  
</bio:treatment>