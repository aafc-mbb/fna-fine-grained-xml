<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">335</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1824" rank="genus">cercocarpus</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">betuloides</taxon_name>
    <taxon_name authority="(Eastwood) Dunkle" date="1940" rank="variety">traskiae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. S. Calif. Acad. Sci.</publication_title>
      <place_in_publication>39: 2. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus cercocarpus;species betuloides;variety traskiae</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100542</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cercocarpus</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">traskiae</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>3, 1: 136, plate 11, fig. 7. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cercocarpus;species traskiae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">betuloides</taxon_name>
    <taxon_name authority="(Eastwood) A. E. Murray" date="unknown" rank="subspecies">traskiae</taxon_name>
    <taxon_hierarchy>genus C.;species betuloides;subspecies traskiae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">montanus</taxon_name>
    <taxon_name authority="(Eastwood) F. L. Martin" date="unknown" rank="variety">traskiae</taxon_name>
    <taxon_hierarchy>genus C.;species montanus;variety traskiae</taxon_hierarchy>
  </taxon_identification>
  <number>1c.</number>
  <other_name type="common_name">Santa Catalina mountain mahogany</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 10–30 (–80) dm.</text>
      <biological_entity id="o32523" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Young stems woolly with tightly coiled hairs.</text>
      <biological_entity id="o32525" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="young" value_original="young" />
        <character constraint="with hairs" constraintid="o32526" is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o32526" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="tightly" name="architecture" src="d0_s1" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules 5–10 mm;</text>
      <biological_entity id="o32527" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o32528" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to broadly ovate, obovate, suborbiculate, or oblongelliptic, (15–) 25–50 (–62) × (11–) 17–35 (–45) mm, strongly coriaceous, margins crenate, often strongly revolute, dentate-serrate in distal 1/2–3/4, abaxial veins densely and ± persistently white villous to woolly with loosely curved, 1/2 coiled at base, or coiled hairs 0.5–2 mm, areoles canescent, adaxial surface shiny, moderately hairy or glabrate.</text>
      <biological_entity id="o32529" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o32530" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate obovate suborbiculate or oblongelliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate obovate suborbiculate or oblongelliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate obovate suborbiculate or oblongelliptic" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovate obovate suborbiculate or oblongelliptic" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s3" to="25" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="62" to_unit="mm" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_width" src="d0_s3" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="45" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s3" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o32531" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="often strongly" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character constraint="in distal 1/2-3/4" constraintid="o32532" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate-serrate" value_original="dentate-serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o32532" name="1/2-3/4" name_original="1/2-3/4" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o32533" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less persistently" name="pubescence" src="d0_s3" value="white" value_original="white" />
        <character char_type="range_value" constraint="with loosely curved" from="villous" name="pubescence" src="d0_s3" to="woolly" />
        <character name="quantity" src="d0_s3" value="1/2" value_original="1/2" />
        <character constraint="at base" constraintid="o32534" is_modifier="false" name="architecture" src="d0_s3" value="coiled" value_original="coiled" />
      </biological_entity>
      <biological_entity id="o32534" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o32535" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="coiled" value_original="coiled" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32536" name="areole" name_original="areoles" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o32537" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers 3–8 (–16) per short-shoot;</text>
      <biological_entity id="o32538" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="16" />
        <character char_type="range_value" constraint="per short-shoot" constraintid="o32539" from="3" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
      <biological_entity id="o32539" name="short-shoot" name_original="short-shoot" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>hypanthial tubes woolly with tightly coiled hairs.</text>
      <biological_entity constraint="hypanthial" id="o32540" name="tube" name_original="tubes" src="d0_s5" type="structure">
        <character constraint="with hairs" constraintid="o32541" is_modifier="false" name="pubescence" src="d0_s5" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o32541" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="tightly" name="architecture" src="d0_s5" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Achenes 7–11 × 1.8–2.3 mm;</text>
    </statement>
    <statement id="d0_s7">
      <text>fruiting pedicels 6–10 mm;</text>
      <biological_entity id="o32542" name="achene" name_original="achenes" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s6" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32543" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o32542" id="r2151" name="fruiting" negation="false" src="d0_s7" to="o32543" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthial tubes 9–11.5 mm;</text>
      <biological_entity constraint="hypanthial" id="o32544" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="11.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicel/tube ratio 52–69%;</text>
      <biological_entity id="o32545" name="tube" name_original="pedicel/tube" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>fruit awns 5–6 cm.</text>
      <biological_entity constraint="fruit" id="o32546" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Metamorphosed volcanic soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="volcanic soil" modifier="metamorphosed" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety traskiae is known from a south-facing, steep canyon on Santa Catalina Island, where it grows on a highly mineralized metamorphic igneous-derived saussurite gabbro substrate, one not exposed elsewhere in southern California. The population now consists of 11 adult plants, down from 40–50 plants in 1898, the reduction attributed to herbivory by introduced mammals. In molecular studies summarized by L. H. Rieseberg and D. Gerber (1995), only six plants were considered pure traskiae; five were hybrids or introgressants with the insular phase of var. blancheae. The areas around several var. traskiae trees have been fenced to allow for recruitment.</discussion>
  <discussion>Variety traskiae differs from var. blancheae in thicker, more coriaceous leaves with revolute margins that often obscure the marginal teeth, and thick, woolly tomentum of mostly coiled hairs on the abaxial leaf surface, pedicels, and hypanthial tubes. The thick vestiture can weather away over time.</discussion>
  <discussion>Plants grown in gardens from cuttings of pure traskiae often lose the thick leaves, thick cuticles, and revolute margins that characterize the taxon in its native habitat, indicating that the diagnostic morphological traits in part may be a phenotypic response to its arid environment and perhaps its peculiar soil type.</discussion>
  
</bio:treatment>