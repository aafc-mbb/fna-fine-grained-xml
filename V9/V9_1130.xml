<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">657</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="mention_page">649</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="mention_page">660</other_info_on_meta>
    <other_info_on_meta type="mention_page">661</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">amelanchier</taxon_name>
    <taxon_name authority="(Tausch) M. Roemer" date="1847" rank="species">bartramiana</taxon_name>
    <place_of_publication>
      <publication_title>Fam. Nat. Syn. Monogr.</publication_title>
      <place_in_publication>3: 145. 1847</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus amelanchier;species bartramiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100020</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrus</taxon_name>
    <taxon_name authority="Tausch" date="unknown" rank="species">bartramiana</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>21: 715. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pyrus;species bartramiana</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Mountain shadbush or serviceberry</other_name>
  <other_name type="common_name">amélanchier de Bartram</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.3–2.5 (–5) m.</text>
      <biological_entity id="o903" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–50, fastigiate, suckering and forming ± dense colonies.</text>
      <biological_entity id="o904" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="50" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="fastigiate" value_original="fastigiate" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves half-unfolded;</text>
      <biological_entity id="o905" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (2–) 4.5–10.5 (–25) mm;</text>
      <biological_entity id="o906" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s3" to="10.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly elliptic-oval to oblong to broadly oval, (26–) 37–51 (–74) × (12–) 20–29 (–48) mm, base usually cuneate, each margin (2–) 8–14 (–27) teeth on proximal 1/2 and (2–) 7–12 (–21) teeth in distalmost cm, largest teeth less than 1 mm, apex acute to rounded, abaxial surface sparsely (moderately) hairy (or glabrous) by flowering, glabrous or sparsely hairy later, adaxial glabrous (or sparsely hairy) later.</text>
      <biological_entity id="o907" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly elliptic-oval" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="26" from_unit="mm" name="atypical_length" src="d0_s4" to="37" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="51" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="74" to_unit="mm" />
        <character char_type="range_value" from="37" from_unit="mm" name="length" src="d0_s4" to="51" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="29" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="48" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="29" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o908" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o909" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="27" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" to="14" />
      </biological_entity>
      <biological_entity id="o910" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o911" name="1/2" name_original="1/2" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="7" to_inclusive="false" />
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="21" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
      <biological_entity id="o912" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="distalmost" id="o913" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="largest" id="o914" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o915" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o916" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="by adaxial teeth" constraintid="o917" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="condition" src="d0_s4" value="later" value_original="later" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o917" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="true" name="condition" src="d0_s4" value="later" value_original="later" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o910" id="r53" name="on" negation="false" src="d0_s4" to="o911" />
      <relation from="o912" id="r54" name="in" negation="false" src="d0_s4" to="o913" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (1 or) 2 or 3 (or 4) -flowered, (6–) 13–25 (–38) mm.</text>
      <biological_entity id="o918" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-flowered" value_original="3-flowered" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="38" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: (0 or) 1 (–3) subtended by a leaf, proximalmost (4–) 11–21 (–35) mm.</text>
      <biological_entity id="o919" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o920" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity constraint="proximalmost" id="o921" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="11" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="21" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s6" to="21" to_unit="mm" />
      </biological_entity>
      <relation from="o919" id="r55" name="subtended by" negation="false" src="d0_s6" to="o920" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals ascending to recurved after flowering, (1.7–) 2.7–3.9 (–6) mm;</text>
      <biological_entity id="o922" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o923" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="after flowering" from="ascending" name="orientation" src="d0_s7" to="recurved" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="2.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s7" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblong-oval to broadly elliptic, (5.5–) 7.1–8.7 (–16.9) × (2.6–) 3.9–5.3 (–7) mm;</text>
      <biological_entity id="o924" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o925" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong-oval" name="shape" src="d0_s8" to="broadly elliptic" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_length" src="d0_s8" to="7.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="16.9" to_unit="mm" />
        <character char_type="range_value" from="7.1" from_unit="mm" name="length" src="d0_s8" to="8.7" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="atypical_width" src="d0_s8" to="3.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="width" src="d0_s8" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens (8–) 18–21 (–25);</text>
      <biological_entity id="o926" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o927" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s9" to="18" to_inclusive="false" />
        <character char_type="range_value" from="21" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="25" />
        <character char_type="range_value" from="18" name="quantity" src="d0_s9" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles (3 or) 4 or 5, (2.7–) 3.8–5.2 (–6.1) mm;</text>
      <biological_entity id="o928" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o929" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="4" value_original="4" />
        <character name="quantity" src="d0_s10" unit="or" value="5" value_original="5" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6.1" to_unit="mm" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s10" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary apex densely hairy (or glabrous).</text>
      <biological_entity id="o930" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="ovary" id="o931" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pomes dark purple, pear-shaped, 10–15 mm diam. 2n = 2x, 3x, 4x.</text>
      <biological_entity id="o932" name="pome" name_original="pomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="pear--shaped" value_original="pear--shaped" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o933" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="x,x,x" value="2" value_original="2" />
        <character name="quantity" src="d0_s12" unit="x,x,x" value="3" value_original="3" />
        <character name="quantity" src="d0_s12" unit="x,x,x" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug; fruiting Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cool woods, mountain slopes, summits, bogs, poor fens, conifer swamps, acidic soil, sandy lake shores, stream banks, rocky ridges, roadside thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cool woods" />
        <character name="habitat" value="mountain slopes" />
        <character name="habitat" value="summits" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="poor fens" />
        <character name="habitat" value="conifer swamps" />
        <character name="habitat" value="acidic soil" />
        <character name="habitat" value="sandy lake shores" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="roadside thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Maine, Mass., Mich., Minn., N.H., N.Y., Pa., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Amelanchier bartramiana is the only North American Amelanchier with leaves that are imbricate in bud, usually fewer than four flowers per inflorescence, and conic ovary apices (and hence fruits that are more or less pear-shaped rather than globose as in other members of the genus). Because of these differences, P. Landry (1975) placed A. bartramiana in its own subgenus, and all other members of the genus in another. W. H. Blanchard (1907), W. A. Robinson and C. R. Partanen (1980), and Robinson (1982) also recognized the distinctness of this species. Amelanchier bartramiana grows farther north than any other shadbush in eastern North America, and more than other shadbushes, it occupies relatively undisturbed habitats, such as peatlands and natural breaks in mature forests. Some plants in this species produce seed sexually (C. S. Campbell et al. 1987). Sexual plants of A. bartramiana are self-incompatible diploids; a tetraploid individual has been reported (A. C. Dibble et al. 1998); it had relatively large petals and might have been an autotetraploid.</discussion>
  <discussion>Amelanchier bartramiana usually flowers with A. laevis, and it frequently hybridizes with other members of the genus. M. L. Fernald (1950) and L. Cinq-Mars (1971) reported hybrids with A. arborea (A. ×quinti-martii Louis-Marie), A. canadensis, A. fernaldii, A. gaspensis, A. humilis, A. intermedia, A. laevis, A. sanguinea, A. spicata, and A. interior. The hybrid with A. laevis can usually be found when these two species grow together (J. E. Weber and C. S. Campbell 1989). The authors have documented a hybrid between A. arborea and A. bartramiana in eastern Pennsylvania (M. B. Burgess et al., unpubl.).</discussion>
  
</bio:treatment>