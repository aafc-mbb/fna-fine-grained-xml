<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="mention_page">183</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">Subviscosae</taxon_name>
    <taxon_name authority="S. Watson" date="1876" rank="species">wheeleri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 148. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section subviscosae;species wheeleri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100381</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wheeleri</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">paupercula</taxon_name>
    <taxon_hierarchy>genus Potentilla;species wheeleri;variety paupercula</taxon_hierarchy>
  </taxon_identification>
  <number>55.</number>
  <other_name type="common_name">Wheeler’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Short hairs not well differentiated from long hairs, absent or sparse throughout.</text>
      <biological_entity id="o16622" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character constraint="from hairs" constraintid="o16623" is_modifier="false" modifier="not well" name="variability" src="d0_s0" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="throughout" name="quantity" src="d0_s0" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o16623" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2–2.5 dm.</text>
      <biological_entity id="o16624" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s1" to="2.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves palmate, 1.5–9.5 cm;</text>
      <biological_entity constraint="basal" id="o16625" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s2" to="9.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–7 cm, long hairs abundant to dense, spreading to appressed, 1–2 mm, weak to stiff, glands ± abundant;</text>
      <biological_entity id="o16626" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16627" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s3" to="stiff" />
      </biological_entity>
      <biological_entity id="o16628" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 5, central cuneate-elliptic to obovate, 0.5–2.5 × 0.3–1.5 cm, scarcely to distinctly petiolulate, distal 1/3 of margins evenly incised ± 1/4 to midvein, teeth 2–4 (–5) per side, surfaces gray-green, long hairs abundant to dense, 1–1.5 mm, glands ± abundant, sometimes obscured.</text>
      <biological_entity id="o16629" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="central" id="o16630" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate-elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="scarcely to distinctly" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character constraint="of margins" constraintid="o16631" name="quantity" src="d0_s4" value="1/3" value_original="1/3" />
        <character constraint="to midvein" constraintid="o16632" name="quantity" src="d0_s4" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o16631" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="evenly; more or less" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o16632" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o16633" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="5" />
        <character char_type="range_value" constraint="per side" constraintid="o16634" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <biological_entity id="o16634" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o16635" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="gray-green" value_original="gray-green" />
      </biological_entity>
      <biological_entity id="o16636" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16637" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 1–20-flowered.</text>
      <biological_entity id="o16638" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-20-flowered" value_original="1-20-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.5–1.5 cm.</text>
      <biological_entity id="o16639" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets lanceolate to ovate-elliptic, 1–3.5 × 1–1.5 mm;</text>
      <biological_entity id="o16640" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o16641" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="ovate-elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 2–4 mm diam.;</text>
      <biological_entity id="o16642" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16643" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 2–5 mm, apex ± acute;</text>
      <biological_entity id="o16644" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o16645" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16646" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals ± paler abaxially, bright-yellow adaxially, ± obcordate, 3–6 × 2–5 mm;</text>
      <biological_entity id="o16647" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16648" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less; abaxially" name="coloration" src="d0_s10" value="paler" value_original="paler" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s10" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="obcordate" value_original="obcordate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–2 mm, anthers 0.5 mm;</text>
      <biological_entity id="o16649" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16650" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16651" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels ca. 20, styles (1.2–) 1.5–2 mm.</text>
      <biological_entity id="o16652" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16653" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o16654" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1–1.5 mm, lightly rugose.</text>
      <biological_entity id="o16655" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="lightly" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy flats, streamsides, lake margins, open conifer woodlands, alpine fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy flats" />
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="open conifer woodlands" />
        <character name="habitat" value="alpine fellfields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla wheeleri is found in the southern Sierra Nevada and San Bernardino Mountains of southern California. Compact plants on the summit of Mount San Gorgonio, described by Jepson as var. paupercula, show no consistent difference to justify their taxonomic segregation. Variety viscidula Rydberg has been misapplied to Arizona populations now called P. rhyolitica. Plants identified as P. wheeleri (excluding P. rimicola) from the Sierra San Pedro Mártir (Baja California) stand as a distinct species, P. luteosericea Rydberg (= P. pinetorum Wiggins).</discussion>
  
</bio:treatment>