<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fragaria</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">vesca</taxon_name>
    <taxon_name authority="(Chamisso &amp; Schlechtendal) Staudt" date="1962" rank="subspecies">californica</taxon_name>
    <place_of_publication>
      <publication_title>Canad. J. Bot.</publication_title>
      <place_in_publication>40: 872. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus fragaria;species vesca;subspecies californica</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100484</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fragaria</taxon_name>
    <taxon_name authority="Chamisso &amp; Schlechtendal" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>2: 20. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fragaria;species californica</taxon_hierarchy>
  </taxon_identification>
  <number>1d</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves green to dark green;</text>
      <biological_entity id="o2686" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="dark green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>terminal leaflet blade ± round or slightly ovate-rhombic, length/width 0.9–1.3, teeth: relative number 0.4–0.9, relative size 1–2.3, terminal tooth equal to or shorter than adjacent teeth.</text>
      <biological_entity constraint="leaflet" id="o2687" name="blade" name_original="blade" src="d0_s1" type="structure" constraint_original="terminal leaflet">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="round" value_original="round" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s1" value="ovate-rhombic" value_original="ovate-rhombic" />
        <character char_type="range_value" from="0.9" name="length/width" src="d0_s1" to="1.3" />
      </biological_entity>
      <biological_entity id="o2688" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" name="quantity" src="d0_s1" to="0.9" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="2.3" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2689" name="tooth" name_original="tooth" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="equal" value_original="equal" />
        <character constraint="than adjacent teeth" constraintid="o2690" is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o2690" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers bisexual, 17–22 mm diam.;</text>
      <biological_entity id="o2691" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s2" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="17" from_unit="mm" name="diameter" src="d0_s2" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hypanthium 11–18.1 mm diam.;</text>
      <biological_entity id="o2692" name="hypanthium" name_original="hypanthium" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="diameter" src="d0_s3" to="18.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals white, widely obovate to nearly orbiculate, margins distinct, sometimes crenate.</text>
      <biological_entity id="o2693" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character char_type="range_value" from="widely obovate" name="shape" src="d0_s4" to="nearly orbiculate" />
      </biological_entity>
      <biological_entity id="o2694" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Achenes superficial or in shallow pits;</text>
      <biological_entity id="o2695" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
        <character is_modifier="false" name="position" src="d0_s5" value="in shallow pits" value_original="in shallow pits" />
      </biological_entity>
      <biological_entity id="o2696" name="pit" name_original="pits" src="d0_s5" type="structure">
        <character is_modifier="true" name="depth" src="d0_s5" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o2695" id="r162" name="in" negation="false" src="d0_s5" to="o2696" />
    </statement>
    <statement id="d0_s6">
      <text>bractlets and sepals spreading or reflexed;</text>
      <biological_entity id="o2697" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o2698" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>torus carmine or red, shiny, globose to ± oblong, easily separating from hypanthium.</text>
      <biological_entity id="o2700" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 14.</text>
      <biological_entity id="o2699" name="torus" name_original="torus" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="carmine" value_original="carmine" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="red" value_original="red" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s7" to="more or less oblong" />
        <character constraint="from hypanthium" constraintid="o2700" is_modifier="false" modifier="easily" name="arrangement" src="d0_s7" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2701" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal strand, grassy coastal areas, coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal strand" />
        <character name="habitat" value="grassy coastal areas" />
        <character name="habitat" value="forests" modifier="coniferous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.; Mexico (Baja California, Baja California Sur).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California Sur)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Whether subsp. bracteata has introgressed into subsp. californica not only in northern California and Oregon, but also in southern California, should be investigated.</discussion>
  
</bio:treatment>