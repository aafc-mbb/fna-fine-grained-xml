<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="illustration_page">245</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) Ertter &amp; Reveal" date="2007" rank="section">COMARELLA</taxon_name>
    <taxon_name authority="(M. E. Jones) D. D. Keck" date="1939" rank="species">sabulosa</taxon_name>
    <place_of_publication>
      <publication_title>Lloydia</publication_title>
      <place_in_publication>1: 124. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section comarella;species sabulosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100275</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">sabulosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 680. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species sabulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Comarella</taxon_name>
    <taxon_name authority="(M. E. Jones) Rydberg" date="unknown" rank="species">sabulosa</taxon_name>
    <taxon_hierarchy>genus Comarella;species sabulosa</taxon_hierarchy>
  </taxon_identification>
  <number>29.</number>
  <other_name type="common_name">Sevier ivesia</other_name>
  <other_name type="common_name">yellow comarella</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± grayish green.</text>
      <biological_entity id="o22760" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish green" value_original="grayish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (1.8–) 2–6 (–6.5) dm.</text>
      <biological_entity id="o22761" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="1.8" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="6.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves 7–25 (–30) cm;</text>
      <biological_entity constraint="basal" id="o22762" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheathing base usually sparsely strigose abaxially;</text>
      <biological_entity id="o22763" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="usually sparsely; abaxially" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–4 (–5) cm;</text>
      <biological_entity id="o22764" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 15–40 per side, ± flabellate, 3–14 mm, usually incised to base into 2–3 oblanceolate lobes, ± densely short-hirsute to villous.</text>
      <biological_entity id="o22765" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o22766" from="15" name="quantity" src="d0_s5" to="40" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s5" value="flabellate" value_original="flabellate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="14" to_unit="mm" />
        <character constraint="to base" constraintid="o22767" is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character char_type="range_value" from="less densely short-hirsute" name="pubescence" notes="" src="d0_s5" to="villous" />
      </biological_entity>
      <biological_entity id="o22766" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o22767" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o22768" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="true" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <relation from="o22767" id="r1478" name="into" negation="false" src="d0_s5" to="o22768" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 10–60-flowered, 4–15 cm diam.</text>
      <biological_entity id="o22769" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="10-60-flowered" value_original="10-60-flowered" />
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels (1–) 5–20 mm.</text>
      <biological_entity id="o22770" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 9–14 mm diam.;</text>
      <biological_entity id="o22771" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlets lanceolate, 1–3 (–3.3) mm;</text>
      <biological_entity constraint="epicalyx" id="o22772" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium interior golden, 1–2 × 3–5 mm;</text>
      <biological_entity id="o22773" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="interior" value_original="interior" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="golden" value_original="golden" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (2.5–) 3.5–6 mm, base golden adaxially, apex acute to acuminate;</text>
      <biological_entity id="o22774" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22775" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s11" value="golden" value_original="golden" />
      </biological_entity>
      <biological_entity id="o22776" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals yellow, linear to narrowly oblanceolate, 2–4 mm;</text>
      <biological_entity id="o22777" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="narrowly oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 5, filaments 2–4 mm, anthers yellow, sometimes red-rimmed, oblong, 0.6–1.2 mm;</text>
      <biological_entity id="o22778" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o22779" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22780" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s13" value="red-rimmed" value_original="red-rimmed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 1–5, styles 2–3 mm.</text>
      <biological_entity id="o22781" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s14" to="5" />
      </biological_entity>
      <biological_entity id="o22782" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, 1.7–2.2 mm.</text>
      <biological_entity id="o22783" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry flats and slopes, on gravelly volcanic or limestone soil, in sagebrush and other desert shrub communities, montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry flats" constraint="on gravelly volcanic or limestone soil" />
        <character name="habitat" value="slopes" constraint="on gravelly volcanic or limestone soil" />
        <character name="habitat" value="volcanic" modifier="gravelly" />
        <character name="habitat" value="limestone soil" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="other desert shrub communities" />
        <character name="habitat" value="conifer woodlands" modifier="montane" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia sabulosa occurs from central Nevada and southwestern Utah south to Arizona north of the Grand Canyon.</discussion>
  
</bio:treatment>