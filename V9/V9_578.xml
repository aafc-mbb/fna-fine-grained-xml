<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan S. Weakley,Robert A. S. Wright</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="mention_page">348</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">neillieae</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="genus">NEILLIA</taxon_name>
    <place_of_publication>
      <publication_title>Prodr. Fl. Nepal.,</publication_title>
      <place_in_publication>228. 1825</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe neillieae;genus NEILLIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Patrick Neill, 1776 – 1851 Scottish printer, naturalist, and secretary of the Caledonian Horticultural Society</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">121784</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Siebold &amp; Zuccarini" date="unknown" rank="genus">Stephanandra</taxon_name>
    <taxon_hierarchy>genus Stephanandra</taxon_hierarchy>
  </taxon_identification>
  <number>33.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 2.5–25 [–40] dm.</text>
      <biological_entity id="o16438" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s0" to="25" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–20+, arching, sometimes rooting at tips;</text>
      <biological_entity id="o16439" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character constraint="at tips" constraintid="o16440" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o16440" name="tip" name_original="tips" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>bark redbrown [to greenish];</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous or slightly hairy [to densely yellowbrown pubescent when young].</text>
      <biological_entity id="o16441" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, cauline;</text>
      <biological_entity id="o16442" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16443" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>stipules persistent [deciduous], ovatelanceolate to elliptic, margins remotely to strongly serrate;</text>
      <biological_entity id="o16444" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s5" to="elliptic" />
      </biological_entity>
      <biological_entity id="o16445" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="remotely to strongly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole present;</text>
      <biological_entity id="o16446" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade ovate, 2–6 [–12] cm, herbaceous, margins flat, incised or lobed, lobes [3–] 7–11, poorly or well developed, singly or doubly serrate, venation palmate, abaxial surface glabrous or sparsely hairy (especially on main veins), adaxial glabrous or sparsely hairy.</text>
      <biological_entity id="o16447" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o16448" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="incised" value_original="incised" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o16449" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="11" />
        <character is_modifier="false" modifier="poorly; well" name="development" src="d0_s7" value="developed" value_original="developed" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="palmate" value_original="palmate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16450" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16451" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, sometimes also axillary, 3–70+-flowered, racemes or panicles to corymbs;</text>
      <biological_entity id="o16452" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o16453" name="raceme" name_original="racemes" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="3-70+-flowered" value_original="3-70+-flowered" />
      </biological_entity>
      <biological_entity id="o16454" name="panicle" name_original="panicles" src="d0_s8" type="structure" constraint="terminal">
        <character is_modifier="true" name="position" src="d0_s8" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="3-70+-flowered" value_original="3-70+-flowered" />
      </biological_entity>
      <biological_entity id="o16455" name="corymb" name_original="corymbs" src="d0_s8" type="structure" />
      <relation from="o16452" id="r1059" name="to" negation="false" src="d0_s8" to="o16455" />
      <relation from="o16453" id="r1060" name="to" negation="false" src="d0_s8" to="o16455" />
      <relation from="o16454" id="r1061" name="to" negation="false" src="d0_s8" to="o16455" />
    </statement>
    <statement id="d0_s9">
      <text>bracts present deciduous;</text>
      <biological_entity id="o16456" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles absent;</text>
      <biological_entity id="o16457" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peduncles present.</text>
      <biological_entity id="o16458" name="peduncle" name_original="peduncles" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels present.</text>
      <biological_entity id="o16459" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers [3–] 4–5 [–10] mm diam.;</text>
      <biological_entity id="o16460" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s13" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium cupshaped [obconic, campanulate], [2–] 3–5 [–12] mm, glabrous or sparsely hairy [glandular-pubescent, densely hairy];</text>
      <biological_entity id="o16461" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cup-shaped" value_original="cup-shaped" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, ascending, triangular to oblong [elliptic];</text>
      <biological_entity id="o16462" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s15" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, white [pink or reddish], obovate;</text>
      <biological_entity id="o16463" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens [5–] 10 [–30] in 1 [–2] whorl (s), shorter than [approximately equal to] petals;</text>
      <biological_entity id="o16464" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s17" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="30" />
        <character constraint="in whorl" constraintid="o16465" name="quantity" src="d0_s17" value="10" value_original="10" />
        <character constraint="than petals" constraintid="o16466" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o16465" name="whorl" name_original="whorl" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s17" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16466" name="petal" name_original="petals" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>carpels 1 [–5];</text>
      <biological_entity id="o16467" name="carpel" name_original="carpels" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="5" />
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 2 [–10].</text>
      <biological_entity id="o16468" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="10" />
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits follicles [aggregated], 1, obliquely subglobose [ellipsoid or cylindric], 2–3 [–5] mm diam., dehiscent from base, enclosed in or protruding from hypanthium, glabrous or sparsely to moderately pilose [glandular-pubescent, densely hairy];</text>
      <biological_entity constraint="fruits" id="o16469" name="follicle" name_original="follicles" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="1" value_original="1" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s20" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s20" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s20" to="3" to_unit="mm" />
        <character constraint="from base" constraintid="o16470" is_modifier="false" name="dehiscence" src="d0_s20" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="position" notes="" src="d0_s20" value="enclosed" value_original="enclosed" />
        <character constraint="from hypanthium" constraintid="o16471" is_modifier="false" name="prominence" src="d0_s20" value="protruding" value_original="protruding" />
        <character char_type="range_value" from="glabrous or" name="pubescence" notes="" src="d0_s20" to="sparsely moderately pilose" />
      </biological_entity>
      <biological_entity id="o16470" name="base" name_original="base" src="d0_s20" type="structure" />
      <biological_entity id="o16471" name="hypanthium" name_original="hypanthium" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>hypanthium persistent;</text>
      <biological_entity id="o16472" name="hypanthium" name_original="hypanthium" src="d0_s21" type="structure">
        <character is_modifier="false" name="duration" src="d0_s21" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>sepals persistent, ascending to erect.</text>
      <biological_entity id="o16473" name="sepal" name_original="sepals" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s22" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds 1 [–4].</text>
    </statement>
    <statement id="d0_s24">
      <text>x = 9.</text>
      <biological_entity id="o16474" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s23" to="4" />
        <character name="quantity" src="d0_s23" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="x" id="o16475" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Va.; Asia; introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 15–20 (1 in the flora).</discussion>
  <discussion>Neillia has traditionally been considered to be one of three genera of Neillieae, the others being Physocarpus and Stephanandra. Few, if any, consistent morphologic differences exist between Neillia and Stephanandra, and a molecular phylogenetic analysis by Oh S. H. and D. Potter (2005) strongly suggested that Stephanandra originated from within Neillia, perhaps by hybridization of the two main lineages. The authors follow Oh (2006) in treating Neillia more broadly to include Stephanandra. Various members of Neillia are cultivated horticulturally in North America. Only N. incisa appears to have naturalized (and apparently only sparingly); but other species have the potential to do so.</discussion>
  <references>
    <reference>Oh, S. H. 2006. Neillia includes Stephanandra. Novon 16: 91–95.</reference>
  </references>
  
</bio:treatment>