<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">489</other_info_on_meta>
    <other_info_on_meta type="mention_page">490</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="genus">photinia</taxon_name>
    <taxon_name authority="(Thunberg) Franchet &amp; Savatier" date="1873" rank="species">glabra</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl. Jap.</publication_title>
      <place_in_publication>1: 141. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus photinia;species glabra</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200010984</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Thunberg" date="unknown" rank="species">glabra</taxon_name>
    <place_of_publication>
      <publication_title>in J. A. Murray, Syst. Veg. ed.</publication_title>
      <place_in_publication>14, 465. 1784</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;species glabra</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Jap." date="unknown" rank="species">Fl.</taxon_name>
    <place_of_publication>
      <place_in_publication>205. 1784</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>species Fl.</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Japanese photinia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–50 dm.</text>
      <biological_entity id="o444" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves persistent;</text>
      <biological_entity id="o445" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 10–30 mm, glabrous;</text>
      <biological_entity id="o446" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade elliptic to elliptic-obovate or elliptic-oblong, 5–9 × 2.5–5 cm, coriaceous, base cuneate, margins crenate-serrulate, lateral-veins 10–15 (–18) pairs, apex abruptly acuminate, surfaces glabrous.</text>
      <biological_entity id="o447" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="elliptic-obovate or elliptic-oblong" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o448" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o449" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="crenate-serrulate" value_original="crenate-serrulate" />
      </biological_entity>
      <biological_entity id="o450" name="lateral-vein" name_original="lateral-veins" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="18" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o451" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o452" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 4–10 cm diam.</text>
      <biological_entity id="o453" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels without lenticels.</text>
      <biological_entity id="o454" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <biological_entity id="o455" name="lenticel" name_original="lenticels" src="d0_s5" type="structure" />
      <relation from="o454" id="r26" name="without" negation="false" src="d0_s5" to="o455" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers 7–8 mm diam.;</text>
      <biological_entity id="o456" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals obovate, 2–3 mm, adaxially white-tomentose proximally.</text>
      <biological_entity id="o457" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="adaxially; proximally" name="pubescence" src="d0_s7" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fencerows, thickets, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, La.; Asia (China, Japan, Myanmar, Thailand); Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Myanmar)" establishment_means="native" />
        <character name="distribution" value="Asia (Thailand)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As noted by C. Kalkman (1973), Lindley cited Crataegus glabra as a synonym of his proposed new species Photinia serrulata. The concept of P. serrulata by Lindley was not the same as that of C. glabra by Thunberg, and P. serrulata Lindley has persisted as a name in use for P. serratifolia. In any case, the publication of P. serrulata was superfluous and illegitimate.</discussion>
  <discussion>Photinia ×fraseri Dress, or 'red tip,' is a popular hybrid cultivar between P. glabra and P. serrulata, valued especially for its brilliant burgundy red young leaves, which mature to green over weeks through shades of copper red. Shrubs are 3–4(–4.5) m; leaf blades elliptic to oblong-obovate or elliptic-obovate, 7–12 × 3–4(–4.5) cm, basally cuneate, with petioles 1–1.5(–2.3) cm. The leaves are smaller than those of P. serratifolia but tend to be longer and slightly narrower, with sharper teeth, than those of P. glabra. Apparently, fruit set is very low and the plants have not been recorded as escaping cultivation. In Texas, P. ×fraseri produces an abundant flush of new leaves in late October through November, when neither of the parents is doing so.</discussion>
  
</bio:treatment>