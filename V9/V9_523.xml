<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan S. Weakley</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">321</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="mention_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">SANGUISORBA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 116. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 53. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus SANGUISORBA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin sanguis, blood, and sorbeo, to absorb, apparently alluding to traditional European and Asian uses to stop external or internal bleeding, suggested by dark red flowers of some species and medieval doctrine of signatures</other_info_on_name>
    <other_info_on_name type="fna_id">129129</other_info_on_name>
  </taxon_identification>
  <number>25.</number>
  <other_name type="common_name">Burnet</other_name>
  <other_name type="common_name">sanguisorbe</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 2–20 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o27540" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, ascending to erect, glabrous or glabrescent.</text>
      <biological_entity id="o27541" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous, basal and cauline;</text>
      <biological_entity id="o27542" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules persistent, adnate to petiole, free portion usually ovate, obovate, or suborbiculate, margins dentate;</text>
      <biological_entity id="o27543" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o27544" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o27544" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o27545" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
      </biological_entity>
      <biological_entity id="o27546" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present;</text>
      <biological_entity id="o27547" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblongelliptic, 5–55 cm (basal blades largest, cauline reduced distally), herbaceous, leaflets (3–) 7–21, orbiculate, ovate, or oblong, leafy stipels sometimes present on vigorous leaves, margins flat, crenate or serrate, surfaces glabrous or sparsely hairy.</text>
      <biological_entity id="o27548" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="55" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o27549" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s6" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="21" />
        <character is_modifier="false" name="shape" src="d0_s6" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o27550" name="stipel" name_original="stipels" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o27551" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s6" value="vigorous" value_original="vigorous" />
      </biological_entity>
      <biological_entity id="o27552" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o27553" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o27550" id="r1796" modifier="sometimes" name="present on" negation="false" src="d0_s6" to="o27551" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal or axillary to distal leaves, 50–500+-flowered, spikes, ellipsoid to cylindric, glabrous or variously pubescent;</text>
      <biological_entity id="o27554" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="50-500+-flowered" value_original="50-500+-flowered" />
      </biological_entity>
      <biological_entity id="o27555" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o27556" name="spike" name_original="spikes" src="d0_s7" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="cylindric" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="variously" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles present;</text>
      <biological_entity id="o27557" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts absent;</text>
      <biological_entity id="o27558" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles present.</text>
      <biological_entity id="o27559" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels absent.</text>
      <biological_entity id="o27560" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 2–5 mm diam.;</text>
      <biological_entity id="o27561" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium urceolate, 1–6 mm, smooth, muricate, or winged, glabrous;</text>
      <biological_entity id="o27562" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals 4, basally connate, spreading, petaloid, elliptic to ovate;</text>
      <biological_entity id="o27563" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="petaloid" value_original="petaloid" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s14" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals 0;</text>
      <biological_entity id="o27564" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens [2–] 4 [–12], shorter or longer than sepals;</text>
      <biological_entity id="o27565" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s16" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="12" />
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character constraint="than sepals" constraintid="o27566" is_modifier="false" name="length_or_size" src="d0_s16" value="shorter or longer" value_original="shorter or longer" />
      </biological_entity>
      <biological_entity id="o27566" name="sepal" name_original="sepals" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>carpels 1 [or 2], glabrous, styles repeatedly branched, brushlike;</text>
      <biological_entity id="o27567" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27568" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="repeatedly" name="architecture" src="d0_s17" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="brushlike" value_original="brushlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules pendulous.</text>
      <biological_entity id="o27569" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s18" value="pendulous" value_original="pendulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits achenes, 1, globose, 0.5–2 mm, glabrous;</text>
      <biological_entity constraint="fruits" id="o27570" name="achene" name_original="achenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>hypanthium persistent, enclosing achenes, dry, hardened, top-shaped to ellipsoid, 2–5 mm, 4-angled or winged, faces smooth between angles;</text>
      <biological_entity id="o27571" name="hypanthium" name_original="hypanthium" src="d0_s20" type="structure">
        <character is_modifier="false" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="condition_or_texture" src="d0_s20" value="dry" value_original="dry" />
        <character is_modifier="false" name="texture" src="d0_s20" value="hardened" value_original="hardened" />
        <character char_type="range_value" from="top-shaped" name="shape" src="d0_s20" to="ellipsoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s20" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o27572" name="achene" name_original="achenes" src="d0_s20" type="structure" />
      <biological_entity id="o27573" name="face" name_original="faces" src="d0_s20" type="structure">
        <character constraint="between angles" constraintid="o27574" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o27574" name="angle" name_original="angles" src="d0_s20" type="structure" />
      <relation from="o27571" id="r1797" name="enclosing" negation="false" src="d0_s20" to="o27572" />
    </statement>
    <statement id="d0_s21">
      <text>sepals persistent, spreading to ascending.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 14.</text>
      <biological_entity id="o27575" name="sepal" name_original="sepals" src="d0_s21" type="structure">
        <character is_modifier="false" name="duration" src="d0_s21" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s21" to="ascending" />
      </biological_entity>
      <biological_entity constraint="x" id="o27576" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 15 (4 in the flora).</discussion>
  <discussion>Sanguisorba is here circumscribed more narrowly than it has been traditionally (for example, H. A. Gleason and A. Cronquist 1991) (though exactly as it was circumscribed by P. A. Rydberg 1908–1918), based on recent molecular phylogenetic analyses summarized in D. Potter et al. (2007), following and refining initial findings by M. S. Kerr (2004). Poteridium and Poterium are in a clade separate from Sanguisorba and basal in Sanguisorbeae, with Sanguisorba then basal to the remainder of Sanguisorbeae (Potter et al.).</discussion>
  <discussion>Acropetal flowering refers to flowers opening sequentially from the base toward the apex of the inflorescence. Basipetal flowering refers to flowers opening sequentially from the apex toward the base of the inflorescence.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikes flowering acropetal, 5–25 cm; calyx lobes white to greenish white, rarely cream to pinkish or slightly purple</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spikes flowering basipetal, 1.5–7 cm; calyx lobes purple or dark purple</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets oblong-ovate to lance-oblong, lengths ca. 2–4 times widths, bases broadly cuneate, truncate, or rounded to slightly cordate; calyx lobe midveins thickened distally; plants 9–20 dm; c, e North America.</description>
      <determination>1 Sanguisorba canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets ovate-oblong to ovate or suborbiculate, lengths ca. 1.2–2 times widths, bases usually deeply cordate, rarely truncate; calyx lobe midveins not thickened distally; plants 2–12 dm; nw North America.</description>
      <determination>2 Sanguisorba stipulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamen lengths 1.5–2.5 times calyx lobes, filaments flattened and dilated in distal 1/2; calyx lobes purple; leaflet lengths 1–2 times widths; spikes cylindric, 1.5–7 cm.</description>
      <determination>3 Sanguisorba menziesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamen lengths ± equal to calyx lobes, filaments filiform throughout; calyx lobes dark purple; leaflet lengths 1.5–2.5 times widths; spikes ellipsoid or ovoid, 1.5–3 cm.</description>
      <determination>4 Sanguisorba officinalis</determination>
    </key_statement>
  </key>
</bio:treatment>