<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">47</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">odoratus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 494. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species odoratus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100440</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rubacer</taxon_name>
    <taxon_name authority="(Linnaeus) Rydberg" date="unknown" rank="species">odoratum</taxon_name>
    <taxon_hierarchy>genus Rubacer;species odoratum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rubus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">odoratus</taxon_name>
    <taxon_name authority="L. H. Bailey" date="unknown" rank="variety">albidus</taxon_name>
    <taxon_hierarchy>genus Rubus;species odoratus;variety albidus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">odoratus</taxon_name>
    <taxon_name authority="Millspaugh" date="unknown" rank="variety">columbianus</taxon_name>
    <taxon_hierarchy>genus R.;species odoratus;variety columbianus</taxon_hierarchy>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Purple flowering raspberry</other_name>
  <other_name type="common_name">ronce odorante</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 dm, unarmed.</text>
      <biological_entity id="o1739" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, sparsely to moderately hairy, moderately to densely stipitate-glandular, glands dark purple, not pruinose.</text>
      <biological_entity id="o1740" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1741" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, simple;</text>
      <biological_entity id="o1742" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules lanceolate to ovate, 5–15 mm;</text>
      <biological_entity id="o1743" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade subrotund to reniform, 9–20 (–30) × (10–) 15–25 (–30) cm, base cordate, palmately, ± deeply, (3–) 5-lobed, margins finely, irregularly serrate to doubly serrate, apex acute to acuminate, abaxial surfaces sparsely to moderately hairy, sparsely to densely stipitate-glandular, glands dark purple.</text>
      <biological_entity id="o1744" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="subrotund" name="shape" src="d0_s4" to="reniform" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_width" src="d0_s4" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s4" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1745" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="palmately; more or less deeply; deeply" name="shape" src="d0_s4" value="(3-)5-lobed" value_original="(3-)5-lobed" />
      </biological_entity>
      <biological_entity id="o1746" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="serrate to doubly" value_original="serrate to doubly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o1747" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1748" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="hairy" modifier="moderately" name="pubescence" src="d0_s4" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="hairy" name="pubescence" src="d0_s4" to="sparsely densely stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1749" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal and axillary, 4–7 (–22) -flowered, cymiform to thyrsiform.</text>
      <biological_entity id="o1750" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-7(-22)-flowered" value_original="4-7(-22)-flowered" />
        <character char_type="range_value" from="cymiform" name="architecture" src="d0_s5" to="thyrsiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels pubescent, moderately to densely stipitate-glandular, glands dark purple.</text>
      <biological_entity id="o1751" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o1752" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers bisexual;</text>
      <biological_entity id="o1753" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals usually magenta, rarely white, broadly obovate to suborbiculate, (12–) 17–25 (–30) mm;</text>
      <biological_entity id="o1754" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="magenta" value_original="magenta" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s8" to="suborbiculate" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="17" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments filiform;</text>
      <biological_entity id="o1755" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovaries distally densely hairy, styles clavate, glabrous.</text>
      <biological_entity id="o1756" name="ovary" name_original="ovaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally densely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o1757" name="style" name_original="styles" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits pale to dark red, hemispheric, 0.7–1.5 cm;</text>
      <biological_entity id="o1758" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s11" to="dark red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s11" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>drupelets 30–60, coherent, separating from torus.</text>
      <biological_entity id="o1760" name="torus" name_original="torus" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o1759" name="drupelet" name_original="drupelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o1760" is_modifier="false" name="arrangement" src="d0_s12" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1761" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist shady sites in deciduous forests, margins of woods, rocky slopes, wooded talus, stream banks, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist shady sites" constraint="in deciduous forests , margins of woods , rocky slopes ," />
        <character name="habitat" value="deciduous forests" />
        <character name="habitat" value="margins" constraint="of woods , rocky slopes ," />
        <character name="habitat" value="woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="talus" modifier="wooded" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="wooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Ala., Conn., Del., D.C., Ga., Ill., Ind., Ky., Maine, Md., Mass., Mich., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis.; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus odoratus is introduced in Washington State. The species is distinguished from other flowering raspberries by its erect, unarmed stems, simple leaves, large flowers, magenta petals, glabrous, clavate styles, and purple stipitate glands densely covering most plant parts. A hybrid with R. nutkanus (R. ×fraseri Rehder) is thought to occur in areas of overlap in northern Michigan (E. G. Voss 1972–1996, vol. 2).</discussion>
  <discussion>The Cherokee used leaf infusions for labor pains and the Iroquois used plant infusions for miscarriage (P. Bergner 1997). Rubus odoratus is grown as an ornamental for its relatively large flowers and magenta petals and its edible fruits that are somewhat dry and insipid.</discussion>
  
</bio:treatment>