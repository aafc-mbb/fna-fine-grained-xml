<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">545</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="(Loudon) Rehder" date="1940" rank="series">Punctatae</taxon_name>
    <taxon_name authority="J. B. Phipps" date="2006" rank="species">spes-aestatum</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>16: 382, fig. 1. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series punctatae;species spes-aestatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100173</other_info_on_name>
  </taxon_identification>
  <number>41.</number>
  <other_name type="common_name">Hope-of-summers</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 30–60 dm.</text>
      <biological_entity id="o18875" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="60" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="60" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth canescent, 1-year old reddish-brown, older ± pale gray;</text>
      <biological_entity id="o18877" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o18878" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o18879" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="more or less" name="life_cycle" src="d0_s1" value="older" value_original="older" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="pale gray" value_original="pale gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs absent or few, straight or slightly curved, 1-year old silver-black, slender, 2.5–6 cm.</text>
      <biological_entity id="o18880" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18881" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="silver-black" value_original="silver-black" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18882" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <relation from="o18881" id="r1225" name="on" negation="false" src="d0_s2" to="o18882" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 0.7–1.5 cm, length 18–25% blade, densely pubescent young, glabrescent, sparsely sessile-glandular;</text>
      <biological_entity id="o18883" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18884" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="18-25%; densely" name="length" notes="" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_function_or_pubescence" src="d0_s3" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o18885" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade rhombic-elliptic, 4–6 cm (3–4 cm at anthesis), 1.5–2 times as long as wide, base cuneate, lobes 3 or 4 per side, lobe apex acute, margins serrate, teeth gland-tipped, veins 4 or 5 per side, apex acuminate, abaxial surface sparsely pubescent, densely pubescent on veins young, adaxial scabrous-pubescent young.</text>
      <biological_entity id="o18886" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18887" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="rhombic-elliptic" value_original="rhombic-elliptic" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s4" value="1.5-2" value_original="1.5-2" />
      </biological_entity>
      <biological_entity id="o18888" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o18889" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="3" value_original="3" />
        <character name="quantity" src="d0_s4" unit="or per" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o18890" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="lobe" id="o18891" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o18892" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o18893" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o18894" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="4" value_original="4" />
        <character name="quantity" src="d0_s4" unit="or per" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18895" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o18896" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18897" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character constraint="on veins" constraintid="o18898" is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18898" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18899" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous-pubescent" value_original="scabrous-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 6–12-flowered;</text>
      <biological_entity id="o18900" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="6-12-flowered" value_original="6-12-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches densely canescent;</text>
      <biological_entity id="o18901" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles membranous or ± herbaceous, margins glandular.</text>
      <biological_entity id="o18902" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s7" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o18903" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 20–22 mm diam.;</text>
      <biological_entity id="o18904" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium canescent;</text>
      <biological_entity id="o18905" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepal margins glandular-serrate, abaxially sparsely pubescent;</text>
      <biological_entity constraint="sepal" id="o18906" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="glandular-serrate" value_original="glandular-serrate" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20, anthers yellow or cream;</text>
      <biological_entity id="o18907" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o18908" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.</text>
      <biological_entity id="o18909" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes red, suborbicular, 10 mm diam.</text>
    </statement>
    <statement id="d0_s14">
      <text>(dry), glabrous, short-pubescent at base and apex;</text>
      <biological_entity id="o18910" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular" value_original="suborbicular" />
        <character name="diameter" src="d0_s13" unit="mm" value="10" value_original="10" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="at apex" constraintid="o18912" is_modifier="false" name="pubescence" src="d0_s14" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
      <biological_entity id="o18911" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity id="o18912" name="apex" name_original="apex" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>sepals ± eroded (in specimen seen);</text>
      <biological_entity id="o18913" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_relief" src="d0_s15" value="eroded" value_original="eroded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pyrenes 3–5.</text>
      <biological_entity id="o18914" name="pyrene" name_original="pyrenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s16" to="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, hills, dry open woods, alluvial streambanks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="hills" />
        <character name="habitat" value="dry open woods" />
        <character name="habitat" value="alluvial streambanks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill., Mo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Crataegus spes-aestatum is known only from southern Illinois and counties bordering the Mississippi River in southern Missouri. It is similar to C. collina and has not been seen with certainty since the 1930s.</discussion>
  
</bio:treatment>