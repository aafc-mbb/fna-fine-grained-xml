<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">192</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">190</other_info_on_meta>
    <other_info_on_meta type="mention_page">201</other_info_on_meta>
    <other_info_on_meta type="mention_page">212</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="illustration_page">193</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Juzepczuk in V. L. Komarov et al." date="1941" rank="section">Aureae</taxon_name>
    <taxon_name authority="Malte" date="1934" rank="species">hyparctica</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>36: 177. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section aureae;species hyparctica;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100334</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Lehmann) Oakes ex Rydberg" date="unknown" rank="species">robbinsiana</taxon_name>
    <taxon_name authority="(Malte) D. Löve" date="unknown" rank="subspecies">hyparctica</taxon_name>
    <taxon_hierarchy>genus Potentilla;species robbinsiana;subspecies hyparctica</taxon_hierarchy>
  </taxon_identification>
  <number>69.</number>
  <other_name type="common_name">Arctic cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cushion-forming;</text>
      <biological_entity id="o1337" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cushion-forming" value_original="cushion-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branches short-to-elongate, slender, often embedded in old leaf-bases.</text>
      <biological_entity constraint="caudex" id="o1338" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="short" name="size" src="d0_s1" to="elongate" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o1339" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o1338" id="r86" modifier="often" name="embedded in" negation="false" src="d0_s1" to="o1339" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 0.2–2 (–2.5) dm, lengths 1–3 (–4) times basal leaves.</text>
      <biological_entity id="o1340" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character constraint="leaf" constraintid="o1341" is_modifier="false" name="length" src="d0_s2" value="1-3(-4) times basal leaves" value_original="1-3(-4) times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1341" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not in ranks, ternate, (1–) 2–8 (–10) cm;</text>
      <biological_entity constraint="basal" id="o1342" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="ternate" value_original="ternate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1343" name="rank" name_original="ranks" src="d0_s3" type="structure" />
      <relation from="o1342" id="r87" name="in" negation="false" src="d0_s3" to="o1343" />
    </statement>
    <statement id="d0_s4">
      <text>stipules: apex ± acute;</text>
      <biological_entity id="o1344" name="stipule" name_original="stipules" src="d0_s4" type="structure" />
      <biological_entity id="o1345" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–6.5 (–8.5) cm, long hairs sparse to common, spreading to subappressed, 0.8–2 mm, weak to ± stiff, glands sparse to common;</text>
      <biological_entity id="o1346" name="stipule" name_original="stipules" src="d0_s5" type="structure" />
      <biological_entity id="o1347" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1348" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="subappressed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s5" to="more or less stiff" />
      </biological_entity>
      <biological_entity id="o1349" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 3, central obovate to broadly obovate, 0.5–2.5 (–2.8) × 0.3–2 (–2.2) cm, petiolule 0–3 mm, margins flat or slightly revolute, not lobed, distal 1/2–2/3 (–3/4) evenly incised ± 1/2 to midvein, teeth (2–) 3–5 per side, surfaces ± similar, abaxial usually pale green, sometimes grayish, hairs sparse to abundant, 0.5–1.7 mm, adaxial darker green, hairs and glands sparse.</text>
      <biological_entity id="o1350" name="stipule" name_original="stipules" src="d0_s6" type="structure" />
      <biological_entity id="o1351" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="central" id="o1352" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="broadly obovate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2.8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1353" name="petiolule" name_original="petiolule" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1354" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s6" to="2/3-3/4" />
        <character is_modifier="false" modifier="evenly; more or less" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character constraint="to midvein" constraintid="o1355" name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1355" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o1356" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s6" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o1357" from="3" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o1357" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o1358" name="surface" name_original="surfaces" src="d0_s6" type="structure" />
      <biological_entity constraint="abaxial" id="o1359" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale green" value_original="pale green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity id="o1360" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s6" to="abundant" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1361" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1362" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="darker green" value_original="darker green" />
        <character is_modifier="false" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1363" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="darker green" value_original="darker green" />
        <character is_modifier="false" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1–3 (–5) -flowered.</text>
      <biological_entity id="o1364" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-3(-5)-flowered" value_original="1-3(-5)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels straight, 0.3–3 cm in flower, to 9 cm in fruit.</text>
      <biological_entity id="o1365" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" constraint="in flower" constraintid="o1366" from="0.3" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o1367" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1366" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o1367" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets narrowly to broadly oblong or ovate, 3.5–7 × 1.5–5 mm, margins flat;</text>
      <biological_entity id="o1368" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o1369" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1370" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 4–7 mm diam.;</text>
      <biological_entity id="o1371" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1372" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–8 mm, apex subacute to rounded;</text>
      <biological_entity id="o1373" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1374" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1375" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s11" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals pale or bright-yellow, 4–9 × 4–6 mm;</text>
      <biological_entity id="o1376" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1377" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale" value_original="pale" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.5–1.1 mm, anthers 0.2–0.4 mm;</text>
      <biological_entity id="o1378" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1379" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1380" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 50–80, styles ± columnar, not or scarcely papillate-swollen proximally, 0.6–0.9 mm.</text>
      <biological_entity id="o1381" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1382" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s14" to="80" />
      </biological_entity>
      <biological_entity id="o1383" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="columnar" value_original="columnar" />
        <character is_modifier="false" modifier="not; scarcely; proximally" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1.1–1.3 mm.</text>
      <biological_entity id="o1384" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Nfld. and Labr., Nunavut, Que., Yukon; Alaska, Mont., Wash., Wyo.; n Eurasia; circumpolar.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
        <character name="distribution" value="circumpolar" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 3 (2 in the flora).</discussion>
  <discussion>Southern arctic and subarctic plants differ from the northern arctic ones in presumably independent features. Where the two forms are sympatric (western Greenland, northern Quebec, Baffin Island), they appear to remain distinct even in mixed populations and are accordingly treated as subspecies.</discussion>
  <discussion>Although some features of scattered plants in Alaska and Yukon approach subsp. nivicola Jurtzev &amp; V. V. Petrovsky (described from northeastern Asia), such plants do not otherwise correspond fully with this taxon. Possible hybrids between Potentilla hyparctica and P. pulchella (sect. Pensylvanicae) are addressed in the discussion of sect. Pensylvanicae. Presumed hybrids with P. villosa from the Aleutian Islands, characterized by the combination of basally thickened styles and lack of cottony hairs on leaflet abaxial surfaces, have been named P. ×aleutica Soják.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Epicalyx bractlets narrowly to ± oblong or ovate, 1.5–2 mm wide; central leaflets: petiolules 0–2 mm, bases cuneate.</description>
      <determination>69a Potentilla hyparctica subsp. hyparctica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Epicalyx bractlets broadly oblong or ovate, 2–5 mm wide; central leaflets: petiolules (0–)2–3(–5) mm, bases broadly cuneate to rounded.</description>
      <determination>69b Potentilla hyparctica subsp. elatior</determination>
    </key_statement>
  </key>
</bio:treatment>