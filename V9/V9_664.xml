<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">sorbarieae</taxon_name>
    <taxon_name authority="(Seringe) A. Braun in P. F. A. Ascherson" date="unknown" rank="genus">sorbaria</taxon_name>
    <taxon_name authority="(Linnaeus) A. Braun in P. F. A. Ascherson" date="1860" rank="species">sorbifolia</taxon_name>
    <place_of_publication>
      <publication_title>in P. F. A. Ascherson, Fl. Brandenburg</publication_title>
      <place_in_publication>1: 177. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe sorbarieae;genus sorbaria;species sorbifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200011660</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">sorbifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 490. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species sorbifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sorbaria</taxon_name>
    <taxon_name authority="(Maximowicz) C. K. Schneider" date="unknown" rank="species">sorbifolia</taxon_name>
    <taxon_name authority="Maximowicz" date="unknown" rank="variety">stellipila</taxon_name>
    <taxon_hierarchy>genus Sorbaria;species sorbifolia;variety stellipila</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stellipila</taxon_name>
    <taxon_hierarchy>genus S.;species stellipila</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Sorbaire à feuilles de sorbier</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 dm.</text>
      <biological_entity id="o6274" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blade 14–30 × 5–17 cm;</text>
      <biological_entity id="o6275" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o6276" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s1" to="17" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflets (9–) 11–21 (–29), oblong-ovate to elliptic, (25–) 35–75 (–95) × (8–) 12–20 (–25) mm, abaxial surface glabrous or ± sparsely stipitate-stellate, adaxial with some simple hairs near margins, otherwise glabrous.</text>
      <biological_entity id="o6277" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6278" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s2" to="11" to_inclusive="false" />
        <character char_type="range_value" from="21" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="29" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s2" to="21" />
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_length" src="d0_s2" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="95" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s2" to="75" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s2" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="25" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6279" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less sparsely" name="arrangement_or_shape" src="d0_s2" value="stipitate-stellate" value_original="stipitate-stellate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6280" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="otherwise" name="pubescence" notes="" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6281" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o6282" name="margin" name_original="margins" src="d0_s2" type="structure" />
      <relation from="o6280" id="r378" name="with" negation="false" src="d0_s2" to="o6281" />
      <relation from="o6281" id="r379" name="near" negation="false" src="d0_s2" to="o6282" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences (7–) 10–15 (–34) × (3–) 4–7 (–14) cm.</text>
      <biological_entity id="o6283" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="34" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_width" src="d0_s3" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="14" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="width" src="d0_s3" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels (and axes) usually puberulent, stipitate-glandular, less often stipitate-stellate.</text>
      <biological_entity id="o6284" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="less often" name="arrangement_or_shape" src="d0_s4" value="stipitate-stellate" value_original="stipitate-stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–14 mm diam.</text>
    </statement>
    <statement id="d0_s6">
      <text>(anther tip to tip);</text>
      <biological_entity id="o6285" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium puberulent, hirtellous, stellate, or glabrous;</text>
      <biological_entity id="o6286" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals ovate to oblong-ovate, margins often glandular-serrate;</text>
      <biological_entity id="o6287" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong-ovate" />
      </biological_entity>
      <biological_entity id="o6288" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s8" value="glandular-serrate" value_original="glandular-serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals ovate to orbiculate, 2.7–4.3 × 2.1–3.4 mm;</text>
      <biological_entity id="o6289" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="orbiculate" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s9" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="2.1" from_unit="mm" name="width" src="d0_s9" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens 20–35 [–50], 2–6.5 mm (of variable length);</text>
      <biological_entity id="o6290" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="50" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="35" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries sericeous, styles 1.3–3.5 mm.</text>
      <biological_entity id="o6291" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o6292" name="style" name_original="styles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Follicles 4.5–6 mm, sericeous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 36.</text>
      <biological_entity id="o6293" name="follicle" name_original="follicles" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6294" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, old fields, waste areas, overgrown forest margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old fields" modifier="roadsides" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="overgrown forest margins" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask., Yukon; Conn., Ind., Iowa, Maine, Mass., Mich., Minn., N.H., N.J., N.Y., Ohio, Pa., R.I., Tenn., Vt., Va., Wash., W.Va., Wis.; Asia (n China, Japan, Korea, Manchuria, e Siberia); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Asia (n China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Manchuria)" establishment_means="native" />
        <character name="distribution" value="Asia (e Siberia)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sorbaria sorbifolia is cultivated in North America and Europe. The plants are always colonial and are capable of becoming adventive.</discussion>
  
</bio:treatment>