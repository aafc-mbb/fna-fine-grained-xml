<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="mention_page">594</other_info_on_meta>
    <other_info_on_meta type="mention_page">605</other_info_on_meta>
    <other_info_on_meta type="mention_page">640</other_info_on_meta>
    <other_info_on_meta type="mention_page">643</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">macracanthae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Macracanthae</taxon_name>
    <place_of_publication>
      <publication_title>Man. Cult. Trees ed.</publication_title>
      <place_in_publication>2, 368. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section macracanthae;series Macracanthae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318081</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Loudon" date="unknown" rank="section">Macracanthae</taxon_name>
    <place_of_publication>
      <publication_title>Arbor. Frutic. Brit.</publication_title>
      <place_in_publication>2: 819. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;section Macracanthae</taxon_hierarchy>
  </taxon_identification>
  <number>64e.1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 20–70 (–80) dm.</text>
      <biological_entity id="o16142" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="70" to_unit="dm" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="dm" />
        <character char_type="range_value" from="20" from_unit="dm" name="some_measurement" src="d0_s0" to="70" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: 1–2-year old twigs usually very dark;</text>
      <biological_entity id="o16144" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16145" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" modifier="usually very" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs ± stout.</text>
      <biological_entity id="o16146" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o16147" name="thorn" name_original="thorns" src="d0_s2" type="structure" />
      <biological_entity id="o16148" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
      <relation from="o16147" id="r1039" name="on" negation="false" src="d0_s2" to="o16148" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole glandular or eglandular;</text>
      <biological_entity id="o16149" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16150" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade usually rhombic to rhombic-elliptic or broadly elliptic to ovate, lobes 0 or 3–5 (–8) per side, lobe apex acute to obtuse, venation craspedodromous, veins (3–) 6–8 per side, adaxial surface scabrous-pubescent young, glabrescent.</text>
      <biological_entity id="o16151" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16152" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic to rhombic-elliptic" value_original="rhombic to rhombic-elliptic" />
        <character name="shape" src="d0_s4" value="broadly elliptic to ovate" value_original="broadly elliptic to ovate" />
      </biological_entity>
      <biological_entity id="o16153" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="8" />
        <character char_type="range_value" constraint="per side" constraintid="o16154" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o16154" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="lobe" id="o16155" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity id="o16156" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="6" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o16157" from="6" name="quantity" src="d0_s4" to="8" />
      </biological_entity>
      <biological_entity id="o16157" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o16158" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous-pubescent" value_original="scabrous-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (5–) 12–30-flowered;</text>
      <biological_entity id="o16159" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(5-)12-30-flowered" value_original="(5-)12-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracteoles caducous, membranous, margins sessile or stipitate-glandular.</text>
      <biological_entity id="o16160" name="bracteole" name_original="bracteoles" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o16161" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers (12–) 13–19 (–22) mm diam.;</text>
      <biological_entity id="o16162" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="19" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="22" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="diameter" src="d0_s7" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepal margins glandular-denticulate to glandular-laciniate, rarely nearly entire;</text>
      <biological_entity constraint="sepal" id="o16163" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="glandular-denticulate" name="shape" src="d0_s8" to="glandular-laciniate" />
        <character is_modifier="false" modifier="rarely nearly" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens 7–10 or 20;</text>
      <biological_entity id="o16164" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s9" to="10" />
        <character name="quantity" src="d0_s9" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles 2 or 3 (–5).</text>
      <biological_entity id="o16165" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="5" />
        <character name="quantity" src="d0_s10" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pomes orange to red, broadly ellipsoid to suborbicular, glabrous or hairy;</text>
      <biological_entity id="o16166" name="pome" name_original="pomes" src="d0_s11" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s11" to="red" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s11" to="suborbicular" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pyrenes 2 or 3 (–5), sides eroded.</text>
      <biological_entity id="o16167" name="pyrene" name_original="pyrenes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="5" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o16168" name="side" name_original="sides" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s12" value="eroded" value_original="eroded" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (5 in the flora).</discussion>
  <discussion>Three common core species comprise ser. Macracanthae: the similar Crataegus macracantha and C. succulenta and the somewhat dissimilar C. calpodendron. These are notable for their mid late to very late anthesis compared to sympatric hawthorns, stout thorns, and eglandular petioles. Crataegus sheridana and C. rubribracteolata are placed here for convenience, may not be closely related, and perhaps would better sit with ser. Anomalae. Series Macracanthae is one of only two series of red-fruited hawthorns in North America that has eroded pyrenes.</discussion>
  <discussion>Members of ser. Macracanthae form putative hybrids with members of ser. Crus-galli (Crataegus persimilis), ser. Parvifoliae (C. ×vailiae), ser. Pruinosae (C. chadsfordiana Sargent), ser. Punctatae (C. florifera), and ser. Rotundifoliae (C. laurentiana), at least. Some are treated here as interserial hybrid species numbers 154–169; C. laurentiana, C. persimilis, and C. ×vailiae key out in the eighth, first, and second couplets, respectively.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: adaxial surfaces glabrous or glabrate, glossy, lobes 0, or 3 or 4 per side.</description>
      <determination>166 Crataegus persimilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: adaxial surfaces pubescent young, matte, lobes 0 or (1–)3–5(–8) per side</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences 2–8(–20)-flowered; sepals ± equal to petals, margins glandular-laciniate.</description>
      <determination>169 Crataegus ×vailiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences (5–)12–30-flowered; sepals shorter than petals, margins usually glandular-denticulate to glandular-laciniate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petioles eglandular, rarely glandular on distal wing</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petioles ± sessile-glandular</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">New growth pubescent; thorns on twigs absent or numerous; leaves (often yellowish green, without impressed venation), abaxial surfaces persistently appressed-pubescent; sepal margins nearly entire or shallowly glandular-serrate; (anthesis 1–3 weeks after sympatric species of this series); stamens 20.</description>
      <determination>24 Crataegus calpodendron</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">New growth glabrous; thorns on twigs usually numerous; leaves (often blue-green, with impressed venation), abaxial surfaces glabrous (except the pertomentosa form of C. macrantha); sepal margins glandular-serrate to glandular-laciniate; (anthesis 1–3 weeks before C. calpodendron in same locality); stamens 10 or 20</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stamens 20; anthers 0.5–0.7 mm.</description>
      <determination>25 Crataegus succulenta</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stamens 10; anthers 0.9–1.2 mm.</description>
      <determination>26 Crataegus macracantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Pyrene sides pitted or ± deeply concave</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Pyrene sides ± roughened with irregular pitting or diagonal scarring</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade sinuses obscure to well-marked, LII 5–15%, lobe apices usually obtuse; pyrene sides pitted; (anthesis late among sympatric hawthorns); bracteoles in young inflorescences ± pallid; anthers white or pink; nw Montana.</description>
      <determination>26 Crataegus macracantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade sinuses moderately deep, max LII 25–33%, lobe apices acute to acuminate; pyrene sides ± deeply concave; (anthesis one of two earliest sympatric hawthorns); bracteoles in young inflorescences deep orange-red or fading; anthers ivory or cream; Cypress Hills, Alberta, Saskatchewan.</description>
      <determination>27 Crataegus rubribracteolata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade lobe apices acute and sinuses shallow, LII 5–15%, or lobe apices subacute, sinuses deeper, LII 15–30%; anthers ivory; Great Plains.</description>
      <determination>28 Crataegus sheridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade lobe apices acute and sinuses moderately deep, LII 20–33%; anthers cream or pale pink; Minnesota, Maine, Michigan, Wisconsin, to Quebec and Newfoundland.</description>
      <determination>164 Crataegus laurentiana</determination>
    </key_statement>
  </key>
</bio:treatment>