<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard J. Pankhurst†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">445</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="mention_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">489</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="unknown" rank="genus">ARONIA</taxon_name>
    <place_of_publication>
      <publication_title>Philos. Bot.</publication_title>
      <place_in_publication>1: 140, 155. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus ARONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek Aria, name for whitebeam (formerly a species of Sorbus), alluding to resemblance to chokeberry fruit</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">102649</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(de Candolle) Nieuwland" date="unknown" rank="genus">Adenorachis</taxon_name>
    <taxon_hierarchy>genus Adenorachis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Pyrus</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="section">Adenorachis</taxon_name>
    <taxon_hierarchy>genus Pyrus;section Adenorachis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Sorbus</taxon_name>
    <taxon_name authority="(Medikus) C. K. Schneider" date="unknown" rank="section">Aronia</taxon_name>
    <taxon_hierarchy>genus Sorbus;section Aronia</taxon_hierarchy>
  </taxon_identification>
  <number>54.</number>
  <other_name type="common_name">Chokeberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 8–20 dm;</text>
    </statement>
    <statement id="d0_s1">
      <text>suckering.</text>
      <biological_entity id="o4564" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="suckering" value_original="suckering" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, erect;</text>
      <biological_entity id="o4565" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bark gray or brown, smooth;</text>
      <biological_entity id="o4566" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>short-shoots absent;</text>
    </statement>
    <statement id="d0_s5">
      <text>unarmed;</text>
    </statement>
    <statement id="d0_s6">
      <text>appressed-pilose, glabrous, or glabrescent.</text>
      <biological_entity id="o4567" name="short-shoot" name_original="short-shoots" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="unarmed" value_original="unarmed" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="appressed-pilose" value_original="appressed-pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves deciduous, cauline, simple;</text>
      <biological_entity id="o4568" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4569" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stipules persistent, adnate to petiole, narrowly triangular, margins glandular;</text>
      <biological_entity id="o4570" name="stipule" name_original="stipules" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o4571" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s8" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o4571" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o4572" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole present;</text>
      <biological_entity id="o4573" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blade elliptic to obovate, 2.5–7.5 (–18) cm, membranous, margins flat, glandular serrulate-dentate, venation pinnate, surfaces glabrous or glabrescent to pilose (or villous).</text>
      <biological_entity id="o4574" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="18" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s10" to="7.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o4575" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrulate-dentate" value_original="serrulate-dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o4576" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s10" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Inflorescences lateral and apparently terminal, 5–12 (–20) -flowered, corymbose, appressed pilose;</text>
      <biological_entity id="o4577" name="inflorescence" name_original="inflorescences" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="apparently" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-12(-20)-flowered" value_original="5-12(-20)-flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="corymbose" value_original="corymbose" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracts reduced to glands;</text>
      <biological_entity id="o4578" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character constraint="to glands" constraintid="o4579" is_modifier="false" name="size" src="d0_s12" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4579" name="gland" name_original="glands" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>bracteoles reduced to glands.</text>
      <biological_entity id="o4580" name="bracteole" name_original="bracteoles" src="d0_s13" type="structure">
        <character constraint="to glands" constraintid="o4581" is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4581" name="gland" name_original="glands" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Pedicels present.</text>
      <biological_entity id="o4582" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Flowers: perianth and androecium epigynous, 12–20 mm diam.;</text>
      <biological_entity id="o4583" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o4584" name="perianth" name_original="perianth" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="epigynous" value_original="epigynous" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s15" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4585" name="androecium" name_original="androecium" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="epigynous" value_original="epigynous" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s15" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hypanthium campanulate, 1–2 mm, glabrous or villous;</text>
      <biological_entity id="o4586" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o4587" name="hypanthium" name_original="hypanthium" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals 5, erect, triangular;</text>
      <biological_entity id="o4588" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o4589" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s17" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 5, white to pale-pink, elliptic to orbiculate, base clawed;</text>
      <biological_entity id="o4590" name="flower" name_original="flowers" src="d0_s18" type="structure" />
      <biological_entity id="o4591" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s18" to="pale-pink" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s18" to="orbiculate" />
      </biological_entity>
      <biological_entity id="o4592" name="base" name_original="base" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens 16–22, equal to petals;</text>
      <biological_entity id="o4593" name="flower" name_original="flowers" src="d0_s19" type="structure" />
      <biological_entity id="o4594" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s19" to="22" />
        <character constraint="to petals" constraintid="o4595" is_modifier="false" name="variability" src="d0_s19" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o4595" name="petal" name_original="petals" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>carpels 5, connate proximally, adnate to hypanthium, hairy, styles terminal, distinct;</text>
      <biological_entity id="o4596" name="flower" name_original="flowers" src="d0_s20" type="structure" />
      <biological_entity id="o4597" name="carpel" name_original="carpels" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s20" value="connate" value_original="connate" />
        <character constraint="to hypanthium" constraintid="o4598" is_modifier="false" name="fusion" src="d0_s20" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s20" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o4598" name="hypanthium" name_original="hypanthium" src="d0_s20" type="structure" />
      <biological_entity id="o4599" name="style" name_original="styles" src="d0_s20" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s20" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 2.</text>
      <biological_entity id="o4600" name="flower" name_original="flowers" src="d0_s21" type="structure" />
      <biological_entity id="o4601" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits pomes, red or black, obovoid or subglobose, 6–9 (–11) mm, glabrous or pilose;</text>
      <biological_entity constraint="fruits" id="o4602" name="pome" name_original="pomes" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s22" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s22" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s22" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s22" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>hypanthium persistent;</text>
      <biological_entity id="o4603" name="hypanthium" name_original="hypanthium" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>sepals persistent, ± appressed;</text>
      <biological_entity id="o4604" name="sepal" name_original="sepals" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="more or less" name="fixation_or_orientation" src="d0_s24" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>carpels cartilaginous;</text>
      <biological_entity id="o4605" name="carpel" name_original="carpels" src="d0_s25" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s25" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>styles and often filaments persistent.</text>
      <biological_entity id="o4606" name="style" name_original="styles" src="d0_s26" type="structure">
        <character is_modifier="false" name="duration" src="d0_s26" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o4607" name="filament" name_original="filaments" src="d0_s26" type="structure">
        <character is_modifier="false" name="duration" src="d0_s26" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>Seeds 1–8 per pome, 2–3 mm. x = 17.</text>
      <biological_entity id="o4608" name="seed" name_original="seeds" src="d0_s27" type="structure">
        <character char_type="range_value" constraint="per pome" constraintid="o4609" from="1" name="quantity" src="d0_s27" to="8" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s27" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4609" name="pome" name_original="pome" src="d0_s27" type="structure" />
      <biological_entity constraint="x" id="o4610" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <discussion>Aronia has been included in Photinia (K. R. Robertson et al. 1991) on morphologic evidence, but C. Kalkman (2004) doubted this conclusion; a phylogenetic analysis by C. S. Campbell et al. (2007), using chloroplast and nuclear DNA sequence data, did not find a close relationship between A. arbutifolia and P. villosa. Historically, species of Aronia have been assigned variously to Adenorachis, Crataegus, Halmia M. Roemer, Malus, Mespilus, Pyrus, and Sorbus. Aronia latifolia Riddell from Kentucky appears to be a form of Amelanchier canadensis. Aronia is cultivated for food (juice, wine, and jam, and as a soft drink flavoring) and as an ornamental for its leaf color, for example, in the former Soviet Union (as A. mitschurinii A. K. Skvortsov &amp; Maitulina), Sweden (H. A. Persson Hovmalm et al. 2004), and in North America.</discussion>
  <discussion>Experiments by J. W. Hardin (1973) suggested that species of Aronia are variously outbreeding, self-compatible, or apomictic. They can also hybridize with Sorbus, forming the intergeneric hybrid ×Sorbaronia C. K. Schneider (see 53. Sorbus). The primary pollinators are thought to be small bees.</discussion>
  <discussion>Varieties have been described for each species, but they are not recognized here as they appear to represent merely extremes of variation.</discussion>
  <discussion>Aronia ×prunifolia (Marshall) Rehder [Mespilus prunifolia Marshall; Adenorachis atropurpurea (Britton) Nieuwland; Aronia atropurpurea Britton; A. floribunda (Lindley) Sweet; Photinia floribunda (Lindley) K. R. Robertson &amp; J. B. Phipps; Pyrus floribunda Lindley], the purple chokeberry, is intermediate between the two species in indumentum but has purple pomes. It is found in St. Pierre and Miquelon, eastern Canada (New Brunswick, Newfoundland, Nova Scotia, Ontario, Prince Edward Island, and Quebec), and the eastern United States (Connecticut, Delaware, District of Columbia, Florida, Illinois, Indiana, Maine, Maryland, Massachusetts, Michigan, Minnesota, New Hampshire, New Jersey, New York, North Carolina, Ohio, Pennsylvania, Rhode Island, South Carolina, Tennessee, Vermont, Virginia, West Virginia, and Wisconsin).</discussion>
  <discussion>J. W. Hardin (1973) concluded that the two species are fairly distinct but that Aronia ×prunifolia tends to obscure the boundary between them, making meaningful identification difficult. The fact that the putative hybrid tends to make apparently normal fruit could be the result of apomixis. It could also explain why it has been able to spread beyond the range limits of at least one of its putative parents.</discussion>
  <references>
    <reference>Hardin, J. W. 1973. The enigmatic chokeberries. Bull. Torrey Bot. Club 100: 178–184.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves shiny adaxially, glabrous or glabrescent; hypanthia glabrous; pomes black.</description>
      <determination>1 Aronia melanocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves dull adaxially, abaxially pilose (except for glabrous forms); hypanthia villous, especially proximally; pomes red.</description>
      <determination>2 Aronia arbutifolia</determination>
    </key_statement>
  </key>
</bio:treatment>