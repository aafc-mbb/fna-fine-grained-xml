<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard Lis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">417</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="(K. Koch) Maximowicz" date="unknown" rank="genus">Holodiscus</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Peterburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 253. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus Holodiscus</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek holos, whole, and diskos, disc, alluding to entire floral disc</other_info_on_name>
    <other_info_on_name type="fna_id">115616</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="K. Koch" date="unknown" rank="unranked">Holodiscus</taxon_name>
    <place_of_publication>
      <publication_title>Dendrologie</publication_title>
      <place_in_publication>1: 309. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;unranked Holodiscus</taxon_hierarchy>
  </taxon_identification>
  <number>47.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, spreading to densely compact, (2–) 3–30 (–60) dm.</text>
      <biological_entity id="o10805" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="densely" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, erect to arching;</text>
      <biological_entity id="o10806" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark gray, exfoliating, periderm reddish to nearly black;</text>
      <biological_entity id="o10807" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" name="relief" src="d0_s2" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o10808" name="periderm" name_original="periderm" src="d0_s2" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s2" to="nearly black" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>shoots 3 types: long strongly angled or pleated 1st year, short, and water-sprout;</text>
      <biological_entity id="o10809" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pleated" value_original="pleated" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o10810" name="water-sprout" name_original="water-sprout" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>glabrous, glabrate, downy, canescent, or pilose-tomentose.</text>
      <biological_entity id="o10811" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="downy" value_original="downy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-tomentose" value_original="pilose-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="canescent" value_original="canescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-tomentose" value_original="pilose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, cauline, alternate, dimorphic with shoot type, those of short-shoots in fascicles of 3–8, simple;</text>
      <biological_entity id="o10812" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10813" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character constraint="with shoot" constraintid="o10814" is_modifier="false" name="growth_form" src="d0_s5" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="of 3-8" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o10814" name="shoot" name_original="shoot" src="d0_s5" type="structure" />
      <biological_entity id="o10815" name="short-shoot" name_original="short-shoots" src="d0_s5" type="structure" />
      <relation from="o10813" id="r662" name="part_of" negation="false" src="d0_s5" to="o10815" />
    </statement>
    <statement id="d0_s6">
      <text>petiole absent or present, distinct, sometimes obscured by decurrent leaf base;</text>
      <biological_entity id="o10816" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
        <character constraint="by leaf base" constraintid="o10817" is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o10817" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade ± ovate, rhombic, trullate to broadly trullate, broadly obovate, or obtrullate, 1–10 cm, membranaceous to chartaceous, sometimes coriaceous, margins flat, usually serrate, sometimes doubly serrate, venation pinnate, simple-craspedodromous, abaxial surface glabrous, glabrate, pilose to villous, tomentose, or sericeous, sometimes sessile or stipitate-glandular, granular deposits present or absent, adaxial glabrous, glabrate, puberulent, pubescent, short-hirsute, velutinous, villous, sessile or stipitate-glandular or eglandular.</text>
      <biological_entity id="o10818" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="trullate" name="shape" src="d0_s7" to="broadly trullate broadly obovate or obtrullate" />
        <character char_type="range_value" from="trullate" name="shape" src="d0_s7" to="broadly trullate broadly obovate or obtrullate" />
        <character char_type="range_value" from="trullate" name="shape" src="d0_s7" to="broadly trullate broadly obovate or obtrullate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="membranaceous" name="texture" src="d0_s7" to="chartaceous" />
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s7" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o10819" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes doubly" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple-craspedodromous" value_original="simple-craspedodromous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10820" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s7" to="villous tomentose or sericeous" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s7" to="villous tomentose or sericeous" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s7" to="villous tomentose or sericeous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o10821" name="deposit" name_original="deposits" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence_or_relief_or_texture" src="d0_s7" value="granular" value_original="granular" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10822" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="short-hirsute" value_original="short-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="velutinous" value_original="velutinous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal on long-shoots, erect or pendulous, 10–100+-flowered, panicles, glabrate, puberulent, villous, or tomentose hairs golden, tan, or white, stipitate or sessile-glandular or absent;</text>
      <biological_entity id="o10823" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character constraint="on long-shoots" constraintid="o10824" is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-100+-flowered" value_original="10-100+-flowered" />
      </biological_entity>
      <biological_entity id="o10824" name="long-shoot" name_original="long-shoots" src="d0_s8" type="structure" />
      <biological_entity id="o10825" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o10826" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="golden" value_original="golden" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts absent or subtending inflorescence branches;</text>
      <biological_entity id="o10827" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="inflorescence" id="o10828" name="branch" name_original="branches" src="d0_s9" type="structure" constraint_original="subtending inflorescence" />
    </statement>
    <statement id="d0_s10">
      <text>bracteoles present.</text>
      <biological_entity id="o10829" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels present.</text>
      <biological_entity id="o10830" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers appearing after leaves, 2–6 mm diam.;</text>
      <biological_entity id="o10831" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10832" name="leaf" name_original="leaves" src="d0_s12" type="structure" />
      <relation from="o10831" id="r663" name="appearing after" negation="false" src="d0_s12" to="o10832" />
    </statement>
    <statement id="d0_s13">
      <text>epicalyx bractlets 0;</text>
      <biological_entity constraint="epicalyx" id="o10833" name="bractlet" name_original="bractlets" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium patelliform to shallowly crateriform, 1.5–5 mm diam.;</text>
      <biological_entity id="o10834" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character char_type="range_value" from="patelliform" name="shape" src="d0_s14" to="shallowly crateriform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, erect to spreading or slightly reflexed, triangular to elliptic-ovate or deltate;</text>
      <biological_entity id="o10835" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s15" to="spreading or slightly reflexed" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s15" to="elliptic-ovate or deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, usually white, sometimes pink-tinged, rarely pink, ovate to elliptic;</text>
      <biological_entity id="o10836" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="pink-tinged" value_original="pink-tinged" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s16" value="pink" value_original="pink" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s16" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 15–20, equal to or longer than petals, with prominent, raised nectar disc proximal to inner rim, exterior puberulent to tomentose, sometimes sessile-glandular;</text>
      <biological_entity id="o10837" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s17" to="20" />
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character constraint="than petals" constraintid="o10838" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10838" name="petal" name_original="petals" src="d0_s17" type="structure" />
      <biological_entity constraint="nectar" id="o10839" name="disc" name_original="disc" src="d0_s17" type="structure" />
      <biological_entity id="o10840" name="rim" name_original="rim" src="d0_s17" type="structure" />
      <biological_entity id="o10841" name="exterior" name_original="exterior" src="d0_s17" type="structure">
        <character char_type="range_value" from="puberulent" modifier="with prominent" name="pubescence" src="d0_s17" to="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s17" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <relation from="o10837" id="r664" modifier="with prominent" name="raised" negation="false" src="d0_s17" to="o10839" />
    </statement>
    <statement id="d0_s18">
      <text>torus inconspicuous;</text>
      <biological_entity id="o10842" name="torus" name_original="torus" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>carpels 5, adnate to hypanthium base, hirsute and hispid, styles terminal, stigmas 2-lobed;</text>
      <biological_entity id="o10843" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
        <character constraint="to hypanthium base" constraintid="o10844" is_modifier="false" name="fusion" src="d0_s19" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s19" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o10844" name="base" name_original="base" src="d0_s19" type="structure" />
      <biological_entity id="o10845" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s19" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o10846" name="stigma" name_original="stigmas" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 2 (1 aborted).</text>
      <biological_entity id="o10847" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits aggregated achenes, 5, laterally flattened, inverted dolabriform, with abaxial edge strongly convex, adaxial straight, 1–1.5 mm, hispid, hirsute, and often sessile or stipitate-glandular;</text>
      <biological_entity id="o10848" name="fruit" name_original="fruits" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" notes="" src="d0_s21" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="orientation" src="d0_s21" value="inverted" value_original="inverted" />
        <character is_modifier="false" name="shape" src="d0_s21" value="dolabriform" value_original="dolabriform" />
      </biological_entity>
      <biological_entity id="o10849" name="achene" name_original="achenes" src="d0_s21" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s21" value="aggregated" value_original="aggregated" />
        <character name="quantity" src="d0_s21" value="5" value_original="5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10850" name="edge" name_original="edge" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s21" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10851" name="edge" name_original="edge" src="d0_s21" type="structure">
        <character is_modifier="false" name="course" src="d0_s21" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s21" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o10848" id="r665" name="with" negation="false" src="d0_s21" to="o10850" />
    </statement>
    <statement id="d0_s22">
      <text>hypanthium persistent;</text>
      <biological_entity id="o10852" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals persistent, erect;</text>
      <biological_entity id="o10853" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s23" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>styles deciduous or persistent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 9.</text>
      <biological_entity id="o10854" name="style" name_original="styles" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o10855" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Costa Rica, Guatemala, Honduras), South America (Colombia, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Costa Rica)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (2 in the flora).</discussion>
  <discussion>Holodiscus contains two species complexes: the argenteus and the discolor complexes (R. A. Lis 1990). The discolor complex comprises H. discolor and H. microphyllus and ranges from British Columbia and Montana through the western United States and south through Mexico along the Sierra Madre Occidental and Oriental, with the southern terminus near the Valley of Mexico. The argenteus complex comprises H. argenteus (Linnaeus f.) Maximowicz, H. fissus (Lindley) C. K. Schneider, and H. orizabae F. A. Ley and ranges from the Trans-Mexican Volcanic Belt south to Colombia and Venezuela.</discussion>
  <discussion>A number of species and varieties have been described from the western United States that appear locally unique, and many names have been applied at the species and varietal levels. The leaves have provided the primary characters for circumscribing taxa in the genus, as the flowers and inflorescences show a narrow range of variation. Holodiscus can exhibit phenotypic plasticity in leaf morphology, particularly in response to changes in relative amounts of moisture and light.</discussion>
  <discussion>Each plant has three shoot types that bear leaves of morphologies slightly to widely divergent among types: short shoots, long shoots, and water-sprouts. The short shoots bear leaves in fascicles of 3–8, the most diagnostic leaves for the taxa. Multiple seasons of leaf development on a short shoot result in a short woody stem with leaf scars in close succession in a phyllotactic spiral.</discussion>
  <discussion>The long shoots develop in the spring, grow rapidly, and may attain a length of 1–2 m in a month or two; they often have a strongly angled or pleated appearance; leaves are alternate along the stem. The long-shoot leaves have rapid growth and expansion and differ from short-shoot leaves in characteristics such as petiole condition, blade size, shape, serration, and base and apex shapes. Long-shoot leaves are quite responsive to local environmental conditions and the season’s water regime under which they developed; consequently, they are more variable morphologically than short-shoot leaves. Long shoots are often terminated by an inflorescence and are more frequently observed and collected. Long shoots may arise from anywhere on the plant, and in some plants, populations, or taxa, they may be relatively short (sometimes as short as 2–6 cm), but maintain the particular morphology of the long shoot.</discussion>
  <discussion>Water-sprout shoots typically arise from the crown of the plant and appear to occur for two reasons: trauma to the plant by severe clipping, mowing, or intense grazing; or unusually high moisture during the spring and early summer growth phase. Water-sprout leaves are the least diagnostic because they have a similar morphology across taxa of the discolor complex. They are quite striking when found and can be dramatically different from short-shoot leaves. Water-sprout leaves are typically thin and delicate, strongly serrate with primary and secondary teeth, and glabrous or glabrescent to very sparsely pubescent, hence appearing very green.</discussion>
  <discussion>Holodiscus has been segregated from the Spiraeeae, with a follicle fruit type, and placed in its own tribe (Holodisceae Focke) based upon the presence of achenes (W. O. Focke 1888–1891; P. A. Rydberg 1908–1918; C. Kalkman 1988; A. L. Takhtajan 1997). Recent molecular work by D. R. Morgan et al. (1994) and D. A. Potter et al. (2007, 2007b) confirms that Holodiscus belongs in Spiraeeae. The investigation into floral ontogeny and morphology in Amygdaloideae by R. C. Evans and T. A. Dickinson (1999b) found that Aruncus, Holodiscus, and Spiraea share three character states of ovule morphology, further supporting the inclusion of Holodiscus in Spiraeeae.</discussion>
  <discussion>Two other generic names have been used for taxa in Holodiscus: Schizonotus Rafinesque, an illegitimate name, and Sericotheca Rafinesque, a rejected name.</discussion>
  <discussion>Holodiscus was of particular horticultural interest in the nineteenth century, which led to the description of many species and varieties. The panicle of flowers can be attractive, and a shrub arched and sagging with relatively many panicles is eye-catching.</discussion>
  <references>
    <reference>Ley, F. A. 1943. A taxonomic revision of the genus Holodiscus (Rosaceae). Bull. Torrey Bot. Club 70: 275–288.</reference>
    <reference>Lis, R. A. 1990. A Taxonomic Revision of Holodiscus Maxim. (Spiraeoideae: Rosaceae) Based upon Numerical Analyses of Leaf Morphometric Variation. Ph.D. dissertation. University of California, Berkeley.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: long shoots usually predominant, blades ± ovate, rhombic, or ± trullate, 1–10 cm, margins frequently with secondary teeth; short shoots usually not predominant, blades (0.5–)2–8 cm, ± ovate, bases truncate, apices obtuse, or slightly ovate to obovate or rhombic, bases acute, decurrent, apices acute; blade maximal width usually proximal to middle.</description>
      <determination>1 Holodiscus discolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: long shoots usually not predominant, blades obtrullate or broadly obovate to ± ovate, 1–3 cm, margins infrequently with secondary teeth; short shoots usually predominant, blades 0.3–1.5(–3) cm, obtrullate, bases acute, decurrent, cuneate, apices obtuse; blade maximal width usually from middle distally.</description>
      <determination>2 Holodiscus microphyllus</determination>
    </key_statement>
  </key>
</bio:treatment>