<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">591</other_info_on_meta>
    <other_info_on_meta type="mention_page">586</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="unknown" date="1940" rank="series">Intricatae</taxon_name>
    <taxon_name authority="Sargent" date="1903" rank="species">stonei</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>5: 62. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series intricatae;species stonei;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100175</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Beadle" date="unknown" rank="species">biltmoreana</taxon_name>
    <taxon_name authority="(Sargent) Kruschke" date="unknown" rank="variety">stonei</taxon_name>
    <taxon_hierarchy>genus Crataegus;species biltmoreana;variety stonei</taxon_hierarchy>
  </taxon_identification>
  <number>97.</number>
  <other_name type="common_name">Stone’s hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 dm.</text>
      <biological_entity id="o28377" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth reddish green, glabrous or villous, 1-year old dull reddish-brown, eventually gray;</text>
      <biological_entity id="o28378" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28379" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o28380" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish green" value_original="reddish green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="reflectance" src="d0_s1" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="eventually" name="coloration" src="d0_s1" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>thorns on twigs ± straight, 2-years old blackish, ultimately gray, ± slender, 4–6 cm.</text>
      <biological_entity id="o28381" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o28382" name="thorn" name_original="thorns" src="d0_s2" type="structure">
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="blackish" value_original="blackish" />
        <character is_modifier="false" modifier="ultimately" name="coloration" src="d0_s2" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="more or less" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28383" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s2" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o28382" id="r1851" name="on" negation="false" src="d0_s2" to="o28383" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole length 30–40% blade, winged distally, villous, glandular;</text>
      <biological_entity id="o28384" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28385" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="30-40%; distally" name="length" notes="" src="d0_s3" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o28386" name="blade" name_original="blade" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade narrowly ovate, 7–8 cm, base cuneate, lobes 0 or 1–3 per side, sinuses shallow, lobe apex acute, margins serrate, teeth gland-tipped, veins 4 or 5 per side, apex acuminate, abaxial surface sparsely villous along veins, adaxial sparsely villous.</text>
      <biological_entity id="o28387" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28388" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o28389" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28390" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character char_type="range_value" constraint="per side" constraintid="o28391" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o28391" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o28392" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="false" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o28393" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28394" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o28395" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o28396" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" unit="or per" value="4" value_original="4" />
        <character name="quantity" src="d0_s4" unit="or per" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o28397" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o28398" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28399" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="along veins" constraintid="o28400" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o28400" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o28401" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 4–6-flowered;</text>
      <biological_entity id="o28402" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="4-6-flowered" value_original="4-6-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches villous;</text>
      <biological_entity id="o28403" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles not seen.</text>
      <biological_entity id="o28404" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers 18–20 mm diam.;</text>
      <biological_entity id="o28405" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="diameter" src="d0_s8" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium densely pubescent;</text>
      <biological_entity id="o28406" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepal length not recorded, margins coarsely glandular-serrate, acuminate, abaxially pubescent;</text>
      <biological_entity id="o28407" name="sepal" name_original="sepal" src="d0_s10" type="structure" />
      <biological_entity id="o28408" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="coarsely" name="length" src="d0_s10" value="glandular-serrate" value_original="glandular-serrate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 5–10, anthers pink;</text>
      <biological_entity id="o28409" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o28410" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 3 or 4.</text>
      <biological_entity id="o28411" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s12" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes dull yellow to russet, broadly ellipsoid, 12–14 mm diam., hairy;</text>
      <biological_entity id="o28412" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dull" value_original="dull" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="russet" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s13" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals on collar, ± reflexed;</text>
      <biological_entity id="o28413" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o28414" name="collar" name_original="collar" src="d0_s14" type="structure" />
      <relation from="o28413" id="r1852" name="on" negation="false" src="d0_s14" to="o28414" />
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 3 or 4.</text>
      <biological_entity id="o28415" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s15" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May–early Jun; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="late May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods, openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mass., N.Y., Pa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Crataegus stonei is another species related to C. biltmoreana, from which it differs mainly in its pink anthers, proportionately narrower and more elongate leaves, and lower stature.</discussion>
  
</bio:treatment>