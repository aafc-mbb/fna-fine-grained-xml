<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">ulmarieae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">filipendula</taxon_name>
    <taxon_name authority="Moench" date="1794" rank="species">vulgaris</taxon_name>
    <place_of_publication>
      <publication_title>Methodus,</publication_title>
      <place_in_publication>663. 1794</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe ulmarieae;genus filipendula;species vulgaris</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">220005285</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">filipendula</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 490. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species filipendula</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Filipendule vulgaire</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, 5–10 dm.</text>
      <biological_entity id="o7098" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rhizomes horizontal to nearly vertical, relatively stout, 5–10 mm wide, internodes 0.5–1 cm;</text>
      <biological_entity id="o7099" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s1" to="nearly vertical" />
        <character is_modifier="false" modifier="relatively" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7100" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>root-tubers round to elongate, 1–3 cm.</text>
      <biological_entity id="o7101" name="root-tuber" name_original="root-tubers" src="d0_s2" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s2" to="elongate" />
        <character char_type="range_value" from="1" from_unit="cm" name="distance" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems glabrous.</text>
      <biological_entity id="o7102" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal 3–6 (–7), persistent to flowering;</text>
      <biological_entity id="o7103" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o7104" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules ovate, 0.8–1.2 cm, apiculate, base auriculate;</text>
      <biological_entity id="o7105" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7106" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o7107" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets in 7–17 pairs, close, lanceolate, to 3 cm, margins deeply dentate;</text>
      <biological_entity id="o7108" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="lateral" id="o7109" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="close" value_original="close" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7110" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s6" to="17" />
      </biological_entity>
      <biological_entity id="o7111" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
      <relation from="o7109" id="r438" name="in" negation="false" src="d0_s6" to="o7110" />
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflets lanceolate, to 3 cm, 3-lobed, lobes lanceolate, margins deeply dentate, apex acute, abaxial surface glabrous or hairy on nerves, hairs appressed, straight to curly.</text>
      <biological_entity id="o7112" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="terminal" id="o7113" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o7114" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o7115" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o7116" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7117" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character constraint="on nerves" constraintid="o7118" is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o7118" name="nerve" name_original="nerves" src="d0_s7" type="structure" />
      <biological_entity id="o7119" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s7" value="curly" value_original="curly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences usually less than 100-flowered;</text>
      <biological_entity id="o7120" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="0-100-flowered" value_original="0-100-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches and pedicels glabrous.</text>
      <biological_entity id="o7121" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7122" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: hypanthium concave, becoming convex in fruit;</text>
      <biological_entity id="o7123" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o7124" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="concave" value_original="concave" />
        <character constraint="in fruit" constraintid="o7125" is_modifier="false" modifier="becoming" name="shape" src="d0_s10" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o7125" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>sepals (5–) 6–7 (–9), green, spatulate to triangular, 2–3 mm, margins without midrib, glabrous;</text>
      <biological_entity id="o7126" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7127" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="9" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="7" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s11" to="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7128" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7129" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <relation from="o7128" id="r439" name="without" negation="false" src="d0_s11" to="o7129" />
    </statement>
    <statement id="d0_s12">
      <text>petals (5–) 6–7 (–9), white to cream, obovate, 5–8 mm, clawed, base narrow, margins entire;</text>
      <biological_entity id="o7130" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7131" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s12" to="6" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="9" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s12" to="7" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="cream" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o7132" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o7133" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens white, about equal to petals.</text>
      <biological_entity id="o7134" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7135" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character constraint="to petals" constraintid="o7136" is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o7136" name="petal" name_original="petals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Achenes 10–18, slightly flattened, ovate, straight, 3–5 mm, densely appressed-hairy throughout, hairs straight, short;</text>
      <biological_entity id="o7137" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="18" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s14" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sessile (attached to enlarged globose torus);</text>
      <biological_entity id="o7138" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 0.5–1 mm. 2n = 14, 14+2B (Europe, w Asia).</text>
      <biological_entity id="o7139" name="style" name_original="styles" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7140" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit=",+b" value="14" value_original="14" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s16" unit=",+b" upper_restricted="false" />
        <character name="quantity" src="d0_s16" unit=",+b" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer (Jun–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" constraint="Jun-Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Abandoned gardens and cemeteries, well-drained short grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="abandoned gardens" />
        <character name="habitat" value="cemeteries" />
        <character name="habitat" value="well-drained short grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Nfld. and Labr. (Nfld.), N.S., Ont.; Calif., Conn., Maine, Mass., N.Y., Vt.; Eurasia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Filipendula vulgaris has been cultivated as an ornamental in North America and probably escapes. The native range stretches from Atlantic Europe to southwestern Siberia (Altai Mountains), and from southern Scandinavia to northern Africa. Despite strong morphologic differences from the other species of Filipendula, it is probably related to F. ulmaria (I. A. Schanzer 1994). Two additional names have been associated with this species: Filipendula hexapetala Gilibert is a rejected name, and F. hexapetala Gilibert ex Maximowicz is illegitimate.</discussion>
  
</bio:treatment>