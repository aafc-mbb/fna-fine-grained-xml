<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">246</other_info_on_meta>
    <other_info_on_meta type="illustration_page">245</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Baillon) Ertter &amp; Reveal" date="2007" rank="section">STELLARIOPSIS</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">santolinoides</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 531. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section stellariopsis;species santolinoides;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220006881</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">santolinoides</taxon_name>
    <taxon_hierarchy>genus Potentilla;species santolinoides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stellariopsis</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">santolinoides</taxon_name>
    <taxon_hierarchy>genus Stellariopsis;species santolinoides</taxon_hierarchy>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Silver mousetail</other_name>
  <other_name type="common_name">stellariopsis</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants ± grayish to silvery.</text>
      <biological_entity id="o15433" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="less grayish" name="coloration" src="d0_s0" to="silvery" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, (1–) 1.5–4 dm.</text>
      <biological_entity id="o15434" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves mousetail-like (individual leaflets scarcely distinguishable), 3–10 cm;</text>
      <biological_entity constraint="basal" id="o15435" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="mousetail-like" value_original="mousetail-like" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheathing base densely strigose abaxially;</text>
      <biological_entity id="o15436" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–1.5 cm;</text>
      <biological_entity id="o15437" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets 60–80 per side, 0.6–1.5 mm, lobes (0–) 3–5, obovate to oval, densely villous.</text>
      <biological_entity id="o15438" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o15439" from="60" name="quantity" src="d0_s5" to="80" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15439" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o15440" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="5" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oval" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences (10–) 30–200-flowered, 3–8 (–30) cm diam.</text>
      <biological_entity id="o15441" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="(10-)30-200-flowered" value_original="(10-)30-200-flowered" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="diameter" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 5–30 mm.</text>
      <biological_entity id="o15442" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers 5–8 mm diam.;</text>
      <biological_entity id="o15443" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>epicalyx bractlets oblong to broadly ovate or orbiculate, 0.2–0.5 mm;</text>
      <biological_entity constraint="epicalyx" id="o15444" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="broadly ovate or orbiculate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 0.5–1.5 × 2–3 mm;</text>
      <biological_entity id="o15445" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 1–2 mm, apex obtuse to acute;</text>
      <biological_entity id="o15446" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15447" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, broadly obovate to orbiculate, 2–2.5 mm;</text>
      <biological_entity id="o15448" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s12" to="orbiculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 15, filaments 1.2–1.8 mm, anthers purple, broadly obcordate to pouch-shaped, 0.3–0.4 mm;</text>
      <biological_entity id="o15449" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="15" value_original="15" />
      </biological_entity>
      <biological_entity id="o15450" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15451" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
        <character char_type="range_value" from="broadly obcordate" name="shape" src="d0_s13" to="pouch-shaped" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 1, styles 2–3 mm.</text>
      <biological_entity id="o15452" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15453" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes mottled grayish brown, 1.7–2 mm. 2n = 28.</text>
      <biological_entity id="o15454" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="mottled grayish" value_original="mottled grayish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15455" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy granitic soil, decomposed granite accumulations, ledges and outcrops, in montane and subalpine conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandy granitic soil" />
        <character name="habitat" value="decomposed granite accumulations" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="subalpine conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ivesia santolinoides is found on loose granitic substrates in the Sierra Nevada, Transverse Ranges, and San Jacinto Mountains from El Dorado to Riverside counties. The species is easily recognized by its silvery mousetail-like leaves and erect, diffuse inflorescences with small, plumlike flowers.</discussion>
  
</bio:treatment>