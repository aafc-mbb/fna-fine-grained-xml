<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">654</other_info_on_meta>
    <other_info_on_meta type="mention_page">650</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">amelanchier</taxon_name>
    <taxon_name authority="(Wiegand) Fernald &amp; Weatherby" date="1931" rank="species">gaspensis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>33: 235. 1931</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus amelanchier;species gaspensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100024</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amelanchier</taxon_name>
    <taxon_name authority="(Pursh) de Candolle" date="unknown" rank="species">sanguinea</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="variety">gaspensis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>14: 139. 1912</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Amelanchier;species sanguinea;variety gaspensis</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Gaspé shadbush</other_name>
  <other_name type="common_name">amélanchier de Gaspésie</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–6 m.</text>
      <biological_entity id="o34112" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="6" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–40, suckering and densely colonial.</text>
      <biological_entity id="o34113" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="40" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="colonial" value_original="colonial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly unfolded;</text>
      <biological_entity id="o34114" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole (8–) 12.8–20.7 (–25) mm;</text>
      <biological_entity id="o34115" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="12.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="12.8" from_unit="mm" name="some_measurement" src="d0_s3" to="20.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly oblong to suborbiculate, (32–) 41–57 (–63) × (26–) 30–40 (–45) mm, base cordate or rounded, each margin with (0–) 3–9 (–12) teeth on proximal 1/2 and 3–5 teeth in distalmost cm, largest teeth more than 1 mm, apex rounded to subtruncate or short-pointed, abaxial surface moderately (sparsely or densely) hairy by flowering, surfaces sparsely hairy (or glabrous) later.</text>
      <biological_entity id="o34116" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s4" to="suborbiculate" />
        <character char_type="range_value" from="32" from_unit="mm" name="atypical_length" src="d0_s4" to="41" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="57" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="63" to_unit="mm" />
        <character char_type="range_value" from="41" from_unit="mm" name="length" src="d0_s4" to="57" to_unit="mm" />
        <character char_type="range_value" from="26" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34117" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o34118" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o34119" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="12" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="9" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o34120" name="1/2" name_original="1/2" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity id="o34121" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="distalmost" id="o34122" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity constraint="largest" id="o34123" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o34124" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subtruncate or short-pointed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o34125" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o34126" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="condition" src="d0_s4" value="later" value_original="later" />
      </biological_entity>
      <relation from="o34118" id="r2245" name="with" negation="false" src="d0_s4" to="o34119" />
      <relation from="o34119" id="r2246" name="on" negation="false" src="d0_s4" to="o34120" />
      <relation from="o34121" id="r2247" name="in" negation="false" src="d0_s4" to="o34122" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (3–) 5–10 (–12) -flowered, (20–) 35–50 (–55) mm.</text>
      <biological_entity id="o34127" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(3-)5-10(-12)-flowered" value_original="(3-)5-10(-12)-flowered" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="55" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s5" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels: 1 or 2 subtended by a leaf, proximalmost (9–) 12–19 (–26) mm.</text>
      <biological_entity id="o34128" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" unit="or subtended" value="1" value_original="1" />
        <character constraint="by leaf" constraintid="o34129" name="quantity" src="d0_s6" unit="or subtended" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o34129" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity constraint="proximalmost" id="o34130" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="19" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="26" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="19" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals recurved after flowering, (2.6–) 3–4.5 (–5.9) mm;</text>
      <biological_entity id="o34131" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o34132" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character constraint="after flowering" is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5.9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals oblanceolate, (8–) 9.7–13.5 (–15) × (3–) 4.1–6.2 (–7) mm;</text>
      <biological_entity id="o34133" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o34134" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_length" src="d0_s8" to="9.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="15" to_unit="mm" />
        <character char_type="range_value" from="9.7" from_unit="mm" name="length" src="d0_s8" to="13.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s8" to="4.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="width" src="d0_s8" to="6.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>stamens (17–) 18–20 (–21);</text>
      <biological_entity id="o34135" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o34136" name="stamen" name_original="stamens" src="d0_s9" type="structure">
        <character char_type="range_value" from="17" name="atypical_quantity" src="d0_s9" to="18" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="21" />
        <character char_type="range_value" from="18" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>styles (4 or) 5, (2.3–) 2.6–3.5 (–4.1) mm;</text>
      <biological_entity id="o34137" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34138" name="style" name_original="styles" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4.1" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary apex densely hairy.</text>
      <biological_entity id="o34139" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="ovary" id="o34140" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pomes blackish purple, 10 mm diam. 2n = 4x.</text>
      <biological_entity id="o34141" name="pome" name_original="pomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="blackish purple" value_original="blackish purple" />
        <character name="diameter" src="d0_s12" unit="mm" value="10" value_original="10" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34142" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" unit="x" value="4" value_original="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous shores, gravel beaches, cliffs, ledges, alluvial woods, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous shores" />
        <character name="habitat" value="gravel beaches" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="alluvial woods" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>K. M. Wiegand (1912) considered Amelanchier gaspensis to be perplexing because it varies much in stature and habit, as well as in leaf outline and dentition. The leaves suggest an intermediate condition between A. sanguinea and A. humilis, especially in the venation.</discussion>
  <discussion>G. N. Jones (1946) considered the range of Amelanchier gaspensis to extend off the Gaspé Peninsula in Quebec and into Ontario and Michigan; he listed 13 specimens away from the Gaspé, and uncertainty exists about the identity of some of these specimens. For some specimens, Jones first annotated them as A. spicata and later changed his annotation to A. gaspensis. Wiegand annotated one of the specimens as “A. humilis x?,” another specimen as “A. humilis,” and another as “A. florida” (A. alnifolia var. semiintegrifolia). E. G. Voss (1972–1996, vol. 2) included A. gaspensis in A. sanguinea. M. L. Fernald (1950) reported A. gaspensis from northern Maine; specimens from there assigned to A. gaspensis do not match it in all respects. The occurrence of A. gaspensis away from the Gaspé remains questionable.</discussion>
  
</bio:treatment>