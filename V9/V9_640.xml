<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Arthur Haines</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Schulze-Menz ex Reveal" date="2010" rank="tribe">exochordeae</taxon_name>
    <taxon_name authority="Lindley" date="1858" rank="genus">Exochorda</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Chron.</publication_title>
      <place_in_publication>1858: 925. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe exochordeae;genus Exochorda</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek exo -, outside, and chorde, string, alluding to free placentary cords external to carpels</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">112553</other_info_on_name>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Pearlbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 30 (–50) dm.</text>
      <biological_entity id="o9123" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–20+, spreading to erect;</text>
      <biological_entity id="o9124" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="20" upper_restricted="false" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark brown to gray-brown on branches, not exfoliating;</text>
      <biological_entity id="o9125" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on branches" constraintid="o9126" from="brown" name="coloration" src="d0_s2" to="gray-brown" />
        <character is_modifier="false" modifier="not" name="relief" notes="" src="d0_s2" value="exfoliating" value_original="exfoliating" />
      </biological_entity>
      <biological_entity id="o9126" name="branch" name_original="branches" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>short-shoots absent;</text>
    </statement>
    <statement id="d0_s4">
      <text>glabrous or sparsely hairy in early season.</text>
      <biological_entity id="o9127" name="short-shoot" name_original="short-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="in season" constraintid="o9128" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9128" name="season" name_original="season" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, cauline, alternate;</text>
      <biological_entity id="o9129" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9130" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules usually absent;</text>
      <biological_entity id="o9131" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole present, sometimes relatively short;</text>
      <biological_entity id="o9132" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes relatively" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade elliptic or oblong to oblong-obovate, 1.5–4.4 (–6) [–9] cm, membranous, margins flat, entire or serrate in distal 1/2, surfaces glabrous or hairy.</text>
      <biological_entity id="o9133" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="oblong-obovate" />
        <character char_type="range_value" from="4.4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="4.4" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o9134" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character constraint="in distal 1/2" constraintid="o9135" is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9135" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <biological_entity id="o9136" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, 4–10-flowered, racemes, axes glabrous or sparsely hairy prior to expansion of flowers;</text>
      <biological_entity id="o9137" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="4-10-flowered" value_original="4-10-flowered" />
      </biological_entity>
      <biological_entity id="o9138" name="raceme" name_original="racemes" src="d0_s9" type="structure" />
      <biological_entity id="o9139" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9140" name="prior-to-expansion" name_original="prior-to-expansion" src="d0_s9" type="structure" />
      <biological_entity id="o9141" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <relation from="o9140" id="r573" name="part_of" negation="false" src="d0_s9" to="o9141" />
    </statement>
    <statement id="d0_s10">
      <text>bracts present;</text>
      <biological_entity id="o9142" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracteoles present.</text>
      <biological_entity id="o9143" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels present.</text>
      <biological_entity id="o9144" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers bisexual (plants synoecious), appearing with leaves, 25–50 mm diam.;</text>
      <biological_entity id="o9145" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="25" from_unit="mm" name="diameter" src="d0_s13" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9146" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <relation from="o9145" id="r574" name="appearing with" negation="false" src="d0_s13" to="o9146" />
    </statement>
    <statement id="d0_s14">
      <text>hypanthium shallowly campanulate to obturbinate, 3.5–4.2 mm, glabrous;</text>
      <biological_entity id="o9147" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character char_type="range_value" from="shallowly campanulate" name="shape" src="d0_s14" to="obturbinate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, ascending, triangular to broad-triangular or semiorbiculate;</text>
      <biological_entity id="o9148" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="triangular" name="shape" src="d0_s15" to="broad-triangular or semiorbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, white, [oblong] obovate to suborbiculate, base clawed;</text>
      <biological_entity id="o9149" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s16" to="suborbiculate" />
      </biological_entity>
      <biological_entity id="o9150" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 15–20 [–30], shorter than petals;</text>
      <biological_entity id="o9151" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s17" to="30" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s17" to="20" />
        <character constraint="than petals" constraintid="o9152" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9152" name="petal" name_original="petals" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>torus absent;</text>
      <biological_entity id="o9153" name="torus" name_original="torus" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>carpels 5, connate, glabrous, styles 5;</text>
      <biological_entity id="o9154" name="carpel" name_original="carpels" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="connate" value_original="connate" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9155" name="style" name_original="styles" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 1 or 2.</text>
      <biological_entity id="o9156" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s20" unit="or" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Fruits capsules, semicircular in outline, shorter than wide or as tall as wide in side view, 6–10 [–15] mm, glabrous;</text>
      <biological_entity constraint="fruits" id="o9157" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character constraint="in outline" constraintid="o9158" is_modifier="false" name="arrangement_or_shape" src="d0_s21" value="semicircular" value_original="semicircular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s21" value="as-tall-as" value_original="as-tall-as" />
        <character constraint="as-tall-as" is_modifier="false" modifier="in side view" name="width" src="d0_s21" value="wide" value_original="wide" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s21" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s21" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9158" name="outline" name_original="outline" src="d0_s21" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s21" value="shorter than wide" value_original="shorter than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hypanthium not persistent;</text>
      <biological_entity id="o9159" name="hypanthium" name_original="hypanthium" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>sepals not persistent.</text>
      <biological_entity id="o9160" name="sepal" name_original="sepals" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds 1 or 2 per capsule, winged.</text>
      <biological_entity id="o9162" name="capsule" name_original="capsule" src="d0_s24" type="structure" />
    </statement>
    <statement id="d0_s25">
      <text>x = 8.</text>
      <biological_entity id="o9161" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s24" unit="or per" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s24" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="x" id="o9163" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia; introduced also in Europe, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (1 in the flora).</discussion>
  <discussion>Species of Exochorda are attractive, spring-flowering shrubs with racemes of showy, white flowers, and are commonly planted.</discussion>
  
</bio:treatment>