<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="illustration_page">285</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="(Lindley) Rydberg" date="1898" rank="species">glandulosa</taxon_name>
    <taxon_name authority="(Parish) Ertter" date="2007" rank="variety">viscida</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>1: 43. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species glandulosa;variety viscida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100622</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drymocallis</taxon_name>
    <taxon_name authority="Parish" date="unknown" rank="species">viscida</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>38: 460. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Drymocallis;species viscida</taxon_hierarchy>
  </taxon_identification>
  <number>12d.</number>
  <other_name type="common_name">Parish’s drymocallis or wood beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (1.5–) 2–6.5 dm, base 1–4 mm diam.</text>
      <biological_entity id="o18119" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="6.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o18120" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal (3–) 6–20 cm, leaflet pairs 2–3 (–4), terminal leaflet obovate to rhombic-elliptic, (1–) 2–4 × 1–2.5 cm, teeth single or double, 4–9 per side, apex rounded to obtuse;</text>
      <biological_entity id="o18121" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18122" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18123" name="leaflet" name_original="leaflet" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o18124" name="leaflet" name_original="leaflet" src="d0_s1" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s1" to="rhombic-elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s1" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18125" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character name="quantity" src="d0_s1" value="double" value_original="double" />
        <character char_type="range_value" constraint="per side" constraintid="o18126" from="4" name="quantity" src="d0_s1" to="9" />
      </biological_entity>
      <biological_entity id="o18126" name="side" name_original="side" src="d0_s1" type="structure" />
      <biological_entity id="o18127" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline moderately developed, leaflet pairs 2–3.</text>
      <biological_entity id="o18128" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o18129" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o18130" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences (2–) 5–30-flowered, not leafy, narrow, branch angles 15–30 (–40) °.</text>
      <biological_entity id="o18131" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="(2-)5-30-flowered" value_original="(2-)5-30-flowered" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="branch" id="o18132" name="angle" name_original="angles" src="d0_s3" type="structure">
        <character name="degree" src="d0_s3" value="15-30(-40)°" value_original="15-30(-40)°" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 2–10 (proximal to 20) mm.</text>
      <biological_entity id="o18133" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: epicalyx bractlets lanceolate-elliptic, (1.5–) 2–3 (–4.5) × 0.7–1.1 (–1.5) mm;</text>
      <biological_entity id="o18134" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity constraint="epicalyx" id="o18135" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="lanceolate-elliptic" value_original="lanceolate-elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>sepals reflexed, 4–6 (–8) mm, apex obtuse to acute;</text>
      <biological_entity id="o18136" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o18137" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18138" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals reflexed, yellow, narrowly obovate-elliptic, 2–4 (–5) × 1.5–2.5 mm;</text>
      <biological_entity id="o18139" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o18140" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 0.5–1.5 (–2) mm.</text>
      <biological_entity id="o18141" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o18142" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Achenes 0.9–1 mm.</text>
      <biological_entity id="o18143" name="achene" name_original="achenes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist openings, montane forest</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist openings" />
        <character name="habitat" value="montane forest" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety viscida is provisionally resurrected to accommodate plants in the mountains of southern California that combine the flowers and vestiture of var. reflexa with the narrow inflorescences and frequently single-toothed leaflets of Drymocallis lactea var. lactea.</discussion>
  
</bio:treatment>