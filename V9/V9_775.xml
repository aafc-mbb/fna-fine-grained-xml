<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">461</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="illustration_page">448</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="Flinck &amp; B. Hylmö" date="1991" rank="species">atropurpureus</taxon_name>
    <place_of_publication>
      <publication_title>Watsonia</publication_title>
      <place_in_publication>18: 311. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species atropurpureus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100041</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Prostrate or purple-flowered cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–1 m.</text>
      <biological_entity id="o15963" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, spreading horizontally, arching;</text>
      <biological_entity id="o15964" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches divaricate, sometimes distichous, red-purple, initially densely yellowish strigose-villous.</text>
      <biological_entity id="o15965" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s2" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" modifier="initially densely" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose-villous" value_original="strigose-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o15966" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 1–2 mm, yellow-villous;</text>
      <biological_entity id="o15967" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="yellow-villous" value_original="yellow-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, on vigorous shoots obovate-orbiculate, 9–14 × 8–12 mm, chartaceous, base obtuse or acute, margins flat or slightly undulate, veins 2 or 3, superficial or slightly sunken, apex obtuse or truncate, mucronulate, abaxial surfaces shiny, sparsely golden yellow-pilose, adaxial green to dark green, shiny, not glaucous, flat or scarcely bulging between lateral-veins, glabrous;</text>
      <biological_entity id="o15968" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" notes="" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" notes="" src="d0_s5" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o15969" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="vigorous" value_original="vigorous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate-orbiculate" value_original="obovate-orbiculate" />
      </biological_entity>
      <biological_entity id="o15970" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15971" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o15972" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s5" value="sunken" value_original="sunken" />
      </biological_entity>
      <biological_entity id="o15973" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15974" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="sparsely" name="coloration" src="d0_s5" value="golden yellow-pilose" value_original="golden yellow-pilose" />
      </biological_entity>
      <biological_entity id="o15976" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
      <relation from="o15968" id="r1025" name="on" negation="false" src="d0_s5" to="o15969" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves ruby red.</text>
      <biological_entity constraint="adaxial" id="o15975" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s5" to="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character constraint="between lateral-veins" constraintid="o15976" is_modifier="false" modifier="scarcely" name="pubescence_or_shape" src="d0_s5" value="bulging" value_original="bulging" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="ruby red" value_original="ruby red" />
      </biological_entity>
      <biological_entity id="o15977" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o15975" id="r1026" name="fall" negation="false" src="d0_s6" to="o15977" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 9–11 mm with 3 leaves, (1 or) 2 or 3-flowered, compact.</text>
      <biological_entity id="o15978" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="3-flowered" value_original="3-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o15979" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with leaves" constraintid="o15980" from="9" from_unit="mm" name="some_measurement" src="d0_s7" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15980" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <relation from="o15978" id="r1027" name="on" negation="false" src="d0_s7" to="o15979" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.2–1 mm, pilose.</text>
      <biological_entity id="o15981" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 6 mm, closed or nearly so;</text>
      <biological_entity id="o15982" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="6" value_original="6" />
        <character is_modifier="false" name="condition" src="d0_s9" value="closed" value_original="closed" />
        <character name="condition" src="d0_s9" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium funnelform, sparsely yellow-pilose-strigose;</text>
      <biological_entity id="o15983" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="yellow-pilose-strigose" value_original="yellow-pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins yellow-villous, apex green and red-purple, acuminate or acute, surfaces sparsely pilose;</text>
      <biological_entity id="o15984" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o15985" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="yellow-villous" value_original="yellow-villous" />
      </biological_entity>
      <biological_entity id="o15986" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green and red-purple" value_original="green and red-purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o15987" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals erect-incurved, dark red, base purple-black, margins narrowly white;</text>
      <biological_entity id="o15988" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o15989" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect-incurved" value_original="erect-incurved" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark red" value_original="dark red" />
      </biological_entity>
      <biological_entity id="o15990" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple-black" value_original="purple-black" />
      </biological_entity>
      <biological_entity id="o15991" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10–14, filaments dark red to dark purple-red, white distally, anthers white [pink-tinged];</text>
      <biological_entity id="o15992" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o15993" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="14" />
      </biological_entity>
      <biological_entity id="o15994" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s13" to="dark purple-red" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o15995" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2 or 3 (or 4).</text>
      <biological_entity id="o15996" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o15997" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s14" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes bright red, usually broadly obovoid, sometimes obovoid, 7–8 × 7–8 mm, shiny, not glaucous, glabrate;</text>
      <biological_entity id="o15998" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="bright red" value_original="bright red" />
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals suberect, margins villous, sparsely pilose;</text>
      <biological_entity id="o15999" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="suberect" value_original="suberect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>navel open;</text>
      <biological_entity id="o16000" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants 3/4 from base.</text>
      <biological_entity constraint="style" id="o16001" name="remnant" name_original="remnants" src="d0_s18" type="structure">
        <character constraint="from base" constraintid="o16002" name="quantity" src="d0_s18" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o16002" name="base" name_original="base" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes 2 or 3 (or 4).</text>
      <biological_entity id="o16003" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s19" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May; fruiting Sep–Jan.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="fruiting time" char_type="range_value" to="Jan" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, rock roadcuts, paths, edges of disturbed forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="rock roadcuts" />
        <character name="habitat" value="paths" />
        <character name="habitat" value="edges" constraint="of disturbed forests" />
        <character name="habitat" value="disturbed forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.Y., Wash.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Although treated as synonymous with Cotoneaster horizontalis by L. Lingdi and A. R. Brach (2003), C. atropurpureus is separated by having somewhat thinner leaves on vigorous shoots that tend to be obovate instead of broadly elliptic, with obtuse to truncate apices and sometimes slightly wavy margins. It also tends towards less regular branching and somewhat darker petals and filaments.</discussion>
  
</bio:treatment>