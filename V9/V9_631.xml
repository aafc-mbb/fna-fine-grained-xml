<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="mention_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">nigra</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Kew.</publication_title>
      <place_in_publication>2: 165. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species nigra</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242341592</other_info_on_name>
  </taxon_identification>
  <number>37.</number>
  <other_name type="common_name">Canada plum</other_name>
  <other_name type="common_name">prunier noir</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, sometimes suckering, 30–90 dm, moderately thorny.</text>
      <biological_entity id="o458" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="90" to_unit="dm" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="90" to_unit="dm" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with axillary end buds, usually glabrous, rarely hairy.</text>
      <biological_entity id="o460" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="end" id="o461" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="axillary end" />
      <relation from="o460" id="r27" name="with" negation="false" src="d0_s1" to="o461" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o462" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 8–22 mm, glabrate with hairs adaxially, sometimes glandular distally, glands 1–3, discoid;</text>
      <biological_entity id="o463" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="22" to_unit="mm" />
        <character constraint="with hairs" constraintid="o464" is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_function_or_pubescence" notes="" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o464" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o465" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="discoid" value_original="discoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade broadly elliptic to obovate, (5–) 7–11 × 3–6.5 cm, base obtuse to rounded or subcordate, margins doubly crenate-serrate, teeth blunt, glandular, apex abruptly acuminate, abaxial surface hairy along midribs and major veins, adaxial glabrous.</text>
      <biological_entity id="o466" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s4" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="11" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o467" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="rounded or subcordate" />
      </biological_entity>
      <biological_entity id="o468" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="crenate-serrate" value_original="crenate-serrate" />
      </biological_entity>
      <biological_entity id="o469" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o470" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o471" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="along veins" constraintid="o473" is_modifier="false" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o472" name="midrib" name_original="midribs" src="d0_s4" type="structure" />
      <biological_entity id="o473" name="vein" name_original="veins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o474" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 2–4-flowered, umbellate fascicles.</text>
      <biological_entity id="o475" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-4-flowered" value_original="2-4-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="umbellate" value_original="umbellate" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 7–20 mm, usually glabrous, rarely hairy.</text>
      <biological_entity id="o476" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers blooming before or at leaf emergence;</text>
      <biological_entity id="o477" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character constraint="before leaf" constraintid="o478" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o478" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium red-tinged, obconic, 3–4 (–5) mm, usually glabrous, rarely hairy externally;</text>
      <biological_entity id="o479" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="red-tinged" value_original="red-tinged" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely; externally" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals broadly spreading to reflexed, ovate, 2–4 (–5) mm, margins glandular-toothed, abaxial surface usually glabrous, rarely sparsely hairy, adaxial glabrous or hairy;</text>
      <biological_entity id="o480" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="broadly spreading" name="orientation" src="d0_s9" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o481" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="glandular-toothed" value_original="glandular-toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o482" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o483" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, often fading to pink, suborbiculate to oblong-obovate, 8–13 mm;</text>
      <biological_entity id="o484" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="fading" modifier="often" name="coloration" src="d0_s10" to="pink" />
        <character char_type="range_value" from="suborbiculate" name="shape" src="d0_s10" to="oblong-obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries glabrous.</text>
      <biological_entity id="o485" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes red, orange, or yellowish, barely glaucous, globose to ellipsoid, 15–30 mm, glabrous;</text>
      <biological_entity id="o486" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="barely" name="pubescence" src="d0_s12" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s12" to="ellipsoid" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>mesocarps fleshy;</text>
      <biological_entity id="o487" name="mesocarp" name_original="mesocarps" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stones ovoid-ellipsoid, strongly flattened.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 16.</text>
      <biological_entity id="o488" name="stone" name_original="stones" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o489" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun; fruiting Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Borders of deciduous woods, bottomland forests, roadside thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="borders" constraint="of deciduous woods , bottomland forests ," />
        <character name="habitat" value="deciduous woods" />
        <character name="habitat" value="bottomland forests" />
        <character name="habitat" value="thickets" modifier="roadside" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., Que.; Conn., Ill., Ind., Iowa, Maine, Mass., Mich., Minn., N.H., N.Y., N.Dak., Ohio, Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the northeastern United States and adjacent Canada, Prunus nigra co-occurs with, and is sometimes confused with, P. americana, despite being distinct in both flower and leaf. The red-tinged hypanthia and sepals of P. nigra give the entire inflorescence a pinkish coloration even when the petals are pure white, and the sepals bear obvious glandular teeth along their margins; in P. americana the hypanthia and sepals are green and the sepals bear relatively few glandular teeth or are eglandular. The leaf marginal teeth of P. nigra are rounded and bear glands at their tips or have callus scars where the glands fell off; those of P. americana are acute and eglandular; some may have a callused tip.</discussion>
  
</bio:treatment>