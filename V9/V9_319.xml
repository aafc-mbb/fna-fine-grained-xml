<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">211</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="mention_page">210</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Rubricaules</taxon_name>
    <taxon_name authority="Jurtzev" date="1988" rank="species">uschakovii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>73: 1613. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section rubricaules;species uschakovii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100374</other_info_on_name>
  </taxon_identification>
  <number>90.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches often sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o20095" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="with leaves" constraintid="o20096" is_modifier="false" modifier="often" name="architecture" src="d0_s0" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o20096" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="condition" src="d0_s0" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to nearly erect, 0.3–1.5 dm.</text>
      <biological_entity id="o20097" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="nearly erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s1" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves often both ternate and palmate or subpalmate on same plant, 1–5 cm;</text>
      <biological_entity constraint="basal" id="o20098" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="ternate" value_original="ternate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="palmate" value_original="palmate" />
        <character constraint="on plant" constraintid="o20099" is_modifier="false" name="architecture" src="d0_s2" value="subpalmate" value_original="subpalmate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20099" name="plant" name_original="plant" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.5–3 cm, long hairs sparse to dense, ± appressed to ascending, (1–) 1.5–2.5 mm, soft to ± weak, smooth, short hairs absent or sparse, crisped (/cottony) hairs sparse to common, glands absent or sparse;</text>
      <biological_entity id="o20100" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20101" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="density" src="d0_s3" to="dense" />
        <character char_type="range_value" from="less appressed" name="orientation" src="d0_s3" to="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="weak" value_original="weak" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20102" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o20103" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
      <biological_entity id="o20104" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3–5, proximalmost separated by 0–2 mm, central broadly elliptic to obovate, 0.5–2 × 0.3–1.4 cm, petiolules ± 1 mm, distal 2/3 to nearly whole margin incised 2/3–3/5+ to midvein, teeth (2–) 3–4 per side, 3–5 mm, apical tufts 1–1.5 mm, abaxial surfaces grayish white to white, long hairs common to abundant, cottony (/crisped) hairs dense, short hairs and glands absent or obscured, adaxial grayish green to gray, long hairs abundant, 1–2 mm, soft (grading to cottony), short/crisped/cottony hairs common to abundant, glands sparse to common.</text>
      <biological_entity id="o20105" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
      </biological_entity>
      <biological_entity constraint="proximalmost" id="o20106" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character constraint="by central leaflets" constraintid="o20107" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" notes="" src="d0_s4" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="central" id="o20107" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s4" to="obovate" />
      </biological_entity>
      <biological_entity id="o20108" name="petiolule" name_original="petiolules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="position_or_shape" src="d0_s4" value="distal" value_original="distal" />
        <character constraint="to margin" constraintid="o20109" name="quantity" src="d0_s4" value="2/3" value_original="2/3" />
        <character char_type="range_value" constraint="to midvein" constraintid="o20110" from="2/3" name="quantity" src="d0_s4" to="3/5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o20109" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o20110" name="midvein" name_original="midvein" src="d0_s4" type="structure" />
      <biological_entity id="o20111" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="3" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o20112" from="3" name="quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20112" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="apical" id="o20113" name="tuft" name_original="tufts" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20114" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character char_type="range_value" from="grayish white" name="coloration" src="d0_s4" to="white" />
      </biological_entity>
      <biological_entity id="o20115" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
      <biological_entity id="o20116" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s4" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o20117" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o20118" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20119" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character char_type="range_value" from="grayish green" name="coloration" src="d0_s4" to="gray" />
      </biological_entity>
      <biological_entity id="o20120" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o20121" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="common" name="quantity" src="d0_s4" to="abundant" />
      </biological_entity>
      <biological_entity id="o20122" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 0–1.</text>
      <biological_entity constraint="cauline" id="o20123" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1–3 (–4) -flowered, open, branch angle 20–40°.</text>
      <biological_entity id="o20124" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-3(-4)-flowered" value_original="1-3(-4)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="branch" id="o20125" name="angle" name_original="angle" src="d0_s6" type="structure">
        <character name="degree" src="d0_s6" value="20-40°" value_original="20-40°" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–1.5 cm, proximal to 3 cm.</text>
      <biological_entity id="o20126" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20127" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: epicalyx bractlets lanceolate to ovate, 3–4 × 1–2 mm;</text>
      <biological_entity id="o20128" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity constraint="epicalyx" id="o20129" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium 2.5–3.5 mm diam.;</text>
      <biological_entity id="o20130" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20131" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals 3.5–5 mm, apex obtuse to subacute, glands sparse to common, often ± obscured;</text>
      <biological_entity id="o20132" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20133" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20134" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="subacute" />
      </biological_entity>
      <biological_entity id="o20135" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s10" to="common" />
        <character is_modifier="false" modifier="often more or less" name="prominence" src="d0_s10" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pale-yellow, often overlapping, 5–7 × 5–8 (–9) mm, distinctly longer than sepals;</text>
      <biological_entity id="o20136" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20137" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s11" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="8" to_unit="mm" />
        <character constraint="than sepals" constraintid="o20138" is_modifier="false" name="length_or_size" src="d0_s11" value="distinctly longer" value_original="distinctly longer" />
      </biological_entity>
      <biological_entity id="o20138" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments 0.5–1.5 mm, anthers ± 0.5 mm;</text>
      <biological_entity id="o20139" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o20140" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20141" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>carpels 30–60, styles 0.8–0.9 mm.</text>
      <biological_entity id="o20142" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20143" name="carpel" name_original="carpels" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
      <biological_entity id="o20144" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes 1–1.3 mm.</text>
      <biological_entity id="o20145" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry tundra meadows, gravel and loam ridges, loam flats, sandy bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry tundra meadows" />
        <character name="habitat" value="gravel" />
        <character name="habitat" value="loam ridges" />
        <character name="habitat" value="loam flats" />
        <character name="habitat" value="sandy bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>For discussion of probable parentage and resultant diagnostic characters, see discussion of 89. Potentilla pedersenii.</discussion>
  
</bio:treatment>