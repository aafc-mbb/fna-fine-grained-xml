<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">idaeus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">idaeus</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species idaeus;subspecies idaeus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100522</other_info_on_name>
  </taxon_identification>
  <number>13a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems to 1 cm diam., eglandular;</text>
      <biological_entity id="o24423" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="diameter" src="d0_s0" to="1" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>bark usually not peeling with age.</text>
      <biological_entity id="o24424" name="bark" name_original="bark" src="d0_s1" type="structure" />
      <biological_entity id="o24425" name="age" name_original="age" src="d0_s1" type="structure" />
      <relation from="o24424" id="r1586" name="peeling with" negation="false" src="d0_s1" to="o24425" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole eglandular;</text>
      <biological_entity id="o24426" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24427" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3–5 (–7), terminal broadly ovate to oblong-ovate, base cordate, unlobed or lobed in larger leaflets, apex acute to acuminate, laterals acute to attenuate, abaxial surfaces eglandular.</text>
      <biological_entity id="o24428" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24429" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="oblong-ovate" />
      </biological_entity>
      <biological_entity id="o24430" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="unlobed" value_original="unlobed" />
        <character constraint="in larger leaflets" constraintid="o24431" is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="larger" id="o24431" name="leaflet" name_original="leaflets" src="d0_s3" type="structure" />
      <biological_entity id="o24432" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
      <biological_entity id="o24433" name="lateral" name_original="laterals" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24434" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals eglandular.</text>
      <biological_entity id="o24435" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o24436" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits: drupelets 20–60, coherent, separating without torus attached.</text>
      <biological_entity id="o24437" name="fruit" name_original="fruits" src="d0_s5" type="structure" />
      <biological_entity id="o24439" name="torus" name_original="torus" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation" src="d0_s5" value="attached" value_original="attached" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 14.</text>
      <biological_entity id="o24438" name="drupelet" name_original="drupelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s5" to="60" />
        <character is_modifier="false" name="fusion" src="d0_s5" value="coherent" value_original="coherent" />
        <character constraint="without torus" constraintid="o24439" is_modifier="false" name="arrangement" src="d0_s5" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24440" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, roadsides, disturbed areas, dry soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="dry soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B., N.S., Ont., P.E.I., Que.; Alaska, Ark., Conn., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mont., N.J., N.Y., N.C., N.Dak., Ohio, R.I., S.Dak., Tenn., Utah, Vt., Va., Wash., Wyo.; Eurasia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora area, subsp. idaeus primarily is an escape from cultivation. M. L. Fernald (1919b) reported a thornless form (forma inermis Kaufmann) as apparently native to the Magdalen Islands, Quebec, and Minnesota. L. H. Bailey (1941–1945) concluded that typical R. idaeus might be native to the northern regions of North America. Subspecies idaeus usually has narrow prickles but rarely can have some bristles.</discussion>
  
</bio:treatment>