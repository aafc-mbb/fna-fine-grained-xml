<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Barbara Ertter,James L. Reveal</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">124</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">191</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="Poeverlein in P. F. A. Ascherson et al." date="1904" rank="section">Rivales</taxon_name>
    <place_of_publication>
      <publication_title>in P. F. A. Ascherson et al., Syn. Mitteleur. Fl.</publication_title>
      <place_in_publication>6(1): 669. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section Rivales</taxon_hierarchy>
    <other_info_on_name type="fna_id">318037</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Lehmann) A. Nelson" date="unknown" rank="section">Supinae</taxon_name>
    <taxon_hierarchy>genus Potentilla;section Supinae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Supinae Lehmann" date="unknown" rank="species">ser.</taxon_name>
    <taxon_hierarchy>genus Potentilla;species ser.</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Necker ex Greene" date="unknown" rank="section">Tridophyllum</taxon_name>
    <taxon_hierarchy>section Tridophyllum</taxon_hierarchy>
  </taxon_identification>
  <number>8f.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or short-lived perennials, 1-stemmed to ± tufted, not stoloniferous;</text>
      <biological_entity id="o14458" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="1-stemmed" value_original="1-stemmed" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots not fleshy-thickened;</text>
      <biological_entity id="o14461" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="size_or_width" src="d0_s1" value="fleshy-thickened" value_original="fleshy-thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>vestiture of long hairs, sometimes ± crisped hairs present, glands absent or sparse to abundant, not red (sometimes red-septate).</text>
      <biological_entity id="o14462" name="vestiture" name_original="vestiture" src="d0_s2" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o14463" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o14464" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="sometimes more or less" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14465" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s2" to="abundant" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="red" value_original="red" />
      </biological_entity>
      <relation from="o14462" id="r935" name="part_of" negation="false" src="d0_s2" to="o14463" />
    </statement>
    <statement id="d0_s3">
      <text>Stems decumbent to erect, sometimes prostrate, not flagelliform, not rooting at nodes, from centers of ephemeral basal rosettes, (1–) 2–6 (–9) dm, lengths (1–) 2–5 (–12) times basal leaves.</text>
      <biological_entity id="o14466" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s3" to="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s3" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="flagelliform" value_original="flagelliform" />
        <character constraint="at nodes" constraintid="o14467" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s3" to="9" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" notes="" src="d0_s3" to="6" to_unit="dm" />
        <character constraint="leaf" constraintid="o14470" is_modifier="false" name="length" src="d0_s3" value="(1-)2-5(-12) times basal leaves" value_original="(1-)2-5(-12) times basal leaves" />
      </biological_entity>
      <biological_entity id="o14467" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o14468" name="center" name_original="centers" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o14469" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="duration" src="d0_s3" value="ephemeral" value_original="ephemeral" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14470" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o14466" id="r936" name="from" negation="false" src="d0_s3" to="o14468" />
      <relation from="o14468" id="r937" name="part_of" negation="false" src="d0_s3" to="o14469" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal not in ranks;</text>
      <biological_entity id="o14471" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o14472" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14473" name="rank" name_original="ranks" src="d0_s4" type="structure" />
      <relation from="o14472" id="r938" name="in" negation="false" src="d0_s4" to="o14473" />
    </statement>
    <statement id="d0_s5">
      <text>cauline (0–) 1–9 (–14);</text>
      <biological_entity id="o14474" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o14475" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s5" to="1" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="14" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal and proximal cauline ternate or palmate to pinnate (with distal leaflets ± confluent), (2–) 3–15 (–25) cm;</text>
      <biological_entity id="o14476" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="basal and proximal cauline" id="o14477" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="palmate" name="architecture" src="d0_s6" to="pinnate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole: long hairs ascending to spreading, rarely appressed, weak to stiff, glands absent or sparse to abundant;</text>
      <biological_entity id="o14478" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o14479" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" modifier="rarely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s7" to="stiff" />
      </biological_entity>
      <biological_entity id="o14480" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s7" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>leaflets 3–9, at tip to distal 2/3 of leaf axis, separate to overlapping, oblanceolate or elliptic to obovate, sometimes oval or nearly round, margins flat, distal 1/2 to ± whole length evenly to unevenly incised 1/4–1/2 to midvein, teeth (2–) 3–8 (–15) per side, surfaces ± similar, green (abaxial often paler), not glaucous, long hairs weak to stiff, cottony hairs absent.</text>
      <biological_entity id="o14481" name="petiole" name_original="petiole" src="d0_s8" type="structure" />
      <biological_entity id="o14482" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="9" />
        <character constraint="of leaf axis, margins" constraintid="o14484, o14485" name="quantity" src="d0_s8" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s8" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="more or less" name="character" src="d0_s8" value="length" value_original="length" />
        <character is_modifier="false" modifier="evenly to unevenly" name="shape" src="d0_s8" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o14486" from="1/4" name="quantity" src="d0_s8" to="1/2" />
      </biological_entity>
      <biological_entity id="o14483" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o14484" name="axis" name_original="axis" src="d0_s8" type="structure">
        <character char_type="range_value" from="separate" name="arrangement" src="d0_s8" to="overlapping" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oval" value_original="oval" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o14485" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="separate" name="arrangement" src="d0_s8" to="overlapping" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oval" value_original="oval" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s8" value="round" value_original="round" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o14486" name="midvein" name_original="midvein" src="d0_s8" type="structure" />
      <biological_entity id="o14487" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="15" />
        <character char_type="range_value" constraint="per side" constraintid="o14488" from="3" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o14488" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o14489" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s8" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o14490" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character char_type="range_value" from="weak" name="fragility" src="d0_s8" to="stiff" />
      </biological_entity>
      <biological_entity id="o14491" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s8" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o14482" id="r939" name="at" negation="false" src="d0_s8" to="o14483" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences (5–) 20–100+-flowered, ± cymose, sometimes racemiform, compact to open.</text>
      <biological_entity id="o14492" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="(5-)20-100+-flowered" value_original="(5-)20-100+-flowered" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="cymose" value_original="cymose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s9" value="racemiform" value_original="racemiform" />
        <character char_type="range_value" from="compact" name="architecture" src="d0_s9" to="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels straight to ± recurved in fruit, 0.2–2 (–3) cm, proximal often ± longer than distal.</text>
      <biological_entity id="o14493" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character constraint="in fruit" constraintid="o14494" is_modifier="false" modifier="more or less" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s10" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14494" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal" id="o14495" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than distal pedicels" constraintid="o14496" is_modifier="false" name="length_or_size" src="d0_s10" value="often more or less longer" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14496" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers 5-merous;</text>
      <biological_entity id="o14497" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-merous" value_original="5-merous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium (2–) 3–6 (–7) mm diam.;</text>
      <biological_entity id="o14498" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s12" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals pale-yellow to yellow, broadly oblanceolate to oblong or broadly obovate, (1–) 1.5–5 mm, usually shorter than sepals, apex rounded to ± retuse;</text>
      <biological_entity id="o14499" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s13" to="yellow" />
        <character char_type="range_value" from="broadly oblanceolate" name="shape" src="d0_s13" to="oblong or broadly obovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o14500" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o14500" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o14501" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s13" to="more or less retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens (5–) 10–20 (–25);</text>
      <biological_entity id="o14502" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s14" to="10" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="25" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles subapical, conic-tapered, slightly to strongly papillate-swollen ± whole length, 0.5–0.8 mm.</text>
      <biological_entity id="o14503" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="subapical" value_original="subapical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="conic-tapered" value_original="conic-tapered" />
        <character is_modifier="false" modifier="slightly to strongly; more or less" name="length" src="d0_s15" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes smooth to strongly rugose.</text>
      <biological_entity id="o14504" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s16" to="strongly rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Eurasia, Africa; introduced in Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="in Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 18–21 (4 in the flora).</discussion>
  <discussion>The North American representatives of sect. Rivales are morphologically distinctive in being annuals, biennials, or short-lived perennials (especially in cold regions) with very short, conic-tapered styles. Plants grow primarily in wet habitats, especially where periodically inundated. These distinctions led E. L. Greene (1906c) to adopt Tridophyllum. However, the gross morphological cohesiveness is not matched by molecular monophyly. Instead, chloroplast data scatter Potentilla norvegica, P. rivalis, and P. supina among the core Potentilla, while P. biennis is sister species to the monophyletic Ivesia-Horkelia-Horkeliella clade (C. Dobeš and J. Paule 2010; M. H. Töpel et al. 2011). Nuclear markers, on the other hand, place P. norvegica as sister to the Ivesia-Horkelia-Horkeliella clade (Töpel et al.), suggesting a possible hybrid origin and/or multiple origins of the biennial habit.</discussion>
  <discussion>Existing herbarium annotations of Potentilla biennis, P. norvegica, and P. rivalis are not reliable, but the three species can be readily distinguished by vestiture of proximal petioles and stems. Only P. biennis has prominently septate gland-tipped trichomes; P. norvegica is characterized by relatively stiff, spreading, tubercle-based hairs to 3 mm, resembling those of P. recta. In addition, the tiny, smooth, pale achenes of P. biennis and P. rivalis contrast sharply with the larger, darker, strongly ridged ones of P. norvegica.</discussion>
  <discussion>Basal leaves usually wither by anthesis, and cauline leaves (that is, those proximal to the first flowering and/or branching node) often wither by mid-anthesis as well. Unless otherwise indicated, leaves include both basal and cauline leaves. If leaves are ephemeral, and/or where stems branch at the proximal nodes, proximal inflorescence bracts can be used as leaf-equivalents in the keys and descriptions.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves pinnate to subpalmate, leaflets 5–9</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ternate or palmate, leaflets 3–5</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 5–9, on distal (1/4–)1/2–2/3 of leaf axis; achenes usually strongly rugose, developing a smooth, corky protuberance often as large as body of achene.</description>
      <determination>10 Potentilla supina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 5(–7), on less than distal 1/5 (basal leaves) or 1/2 (cauline leaves) of leaf axis; achenes ± smooth, without a corky protuberance.</description>
      <determination>11 Potentilla rivalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petiole and stem base hairs usually ± stiff, 1–2.5(–3) mm, glands absent or sparse, inconspicuous; achenes usually strongly rugose, tan to brown, 0.8–1.3 mm; hypanthia 4–7 mm diam.; stamens 15 or 20, anthers 0.3–0.5 mm.</description>
      <determination>13 Potentilla norvegica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petiole and stem base hairs usually ± weak, 0.5–1.5 mm (very weak if longer), glands absent or sparse to abundant, sometimes conspicuous; achenes ± smooth, white to yellowish, 0.5–0.9 mm; hypanthia (2–)3–4(–5.5) mm diam.; stamens (5–)10 or 15, anthers 0.2–0.3 mm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petiole and stem base glands absent or sparse, inconspicuous; leaflets oblanceolate-elliptic to obovate, separate to ± overlapping; stems decumbent to erect, sometimes prostrate.</description>
      <determination>11 Potentilla rivalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petiole and stem base glands sparse to abundant, conspicuous (to 1 mm, septate); leaflets mostly obovate or oval to nearly round, usually overlapping; stems ascending to erect.</description>
      <determination>12 Potentilla biennis</determination>
    </key_statement>
  </key>
</bio:treatment>