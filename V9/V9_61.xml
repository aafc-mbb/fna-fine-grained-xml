<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">31</other_info_on_meta>
    <other_info_on_meta type="mention_page">32</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Douglas in W. J. Hooker" date="1832" rank="species">nivalis</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker, Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 181. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species nivalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100439</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Snow raspberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, to 1.5 dm, armed.</text>
      <biological_entity id="o11833" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems perennial, creeping, sparsely hairy, glabrescent, eglandular, not pruinose;</text>
      <biological_entity id="o11834" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>prickles sparse, strongly retrorse, stout, to 1 mm, broad-based.</text>
      <biological_entity id="o11835" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s2" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="broad-based" value_original="broad-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves evergreen, simple or ternate;</text>
      <biological_entity id="o11836" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules adnate to petioles, broadly elliptic to ovate, (6–) 8–10 mm;</text>
      <biological_entity id="o11837" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="to petioles" constraintid="o11838" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
        <character char_type="range_value" from="broadly elliptic" name="shape" notes="" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11838" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade ovate to cordate, (2.5–) 3–5 (–8) cm, lobe or leaflet base cordate, shallowly 3-lobed, margins coarsely, singly or doubly dentate, apex acute to obtuse, abaxial surfaces with prickles along midveins, glabrous or sparsely hairy, eglandular, both surfaces lustrous fresh.</text>
      <biological_entity id="o11839" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="cordate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11840" name="lobe" name_original="lobe" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity constraint="leaflet" id="o11841" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o11842" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o11843" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11844" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o11845" name="prickle" name_original="prickles" src="d0_s5" type="structure" />
      <biological_entity id="o11846" name="midvein" name_original="midveins" src="d0_s5" type="structure" />
      <biological_entity id="o11847" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="condition" src="d0_s5" value="fresh" value_original="fresh" />
      </biological_entity>
      <relation from="o11844" id="r736" name="with" negation="false" src="d0_s5" to="o11845" />
      <relation from="o11845" id="r737" name="along" negation="false" src="d0_s5" to="o11846" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 1–2-flowered.</text>
      <biological_entity id="o11848" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-2-flowered" value_original="1-2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels: prickles scattered, retrorse, moderately to densely hairy, eglandular.</text>
      <biological_entity id="o11849" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o11850" name="prickle" name_original="prickles" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual;</text>
      <biological_entity id="o11851" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals magenta to pink, elliptic to oblanceolate or spatulate, (5–) 8–10 mm;</text>
      <biological_entity id="o11852" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="magenta" name="coloration" src="d0_s9" to="pink" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="oblanceolate or spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments filiform;</text>
      <biological_entity id="o11853" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries moderately hairy, styles glabrous.</text>
      <biological_entity id="o11854" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11855" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits red, hemispheric, 0.4–1 cm;</text>
      <biological_entity id="o11856" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s12" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s12" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>drupelets 3–10, not coherent, separating from torus.</text>
      <biological_entity id="o11858" name="torus" name_original="torus" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o11857" name="drupelet" name_original="drupelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="10" />
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s13" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o11858" is_modifier="false" name="arrangement" src="d0_s13" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11859" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, semishaded forests, glades, moist soil, logged areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="semishaded forests" />
        <character name="habitat" value="glades" />
        <character name="habitat" value="moist soil" />
        <character name="habitat" value="areas" modifier="logged" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus nivalis is recognized by its creeping, prickly stems, simple to ternate, evergreen leaves, broadly elliptic to ovate stipules, two leaflets, relatively small flowers, and magenta to pink petals. Its closest relative is likely the Mexican R. pumilus Focke. Asian species previously classified in subg. Chamaebatus (Focke) Focke are hexaploid (M. M. Thompson 1997) and not phylogenetically close; R. nivalis appears to be sister to all blackberries of subg. Rubus (L. A. Alice and C. S. Campbell 1999; Alice et al. 2008).</discussion>
  <discussion>The fruits of Rubus nivalis are eaten fresh, stewed, and canned by the Hoh and Quileute Indians (A. B. Reagan 1936).</discussion>
  <discussion>The only known specimen of Rubus nivalis from California was collected in 1961 from Del Norte County at 1250 m near the Oregon border.</discussion>
  
</bio:treatment>