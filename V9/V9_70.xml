<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">51</other_info_on_meta>
    <other_info_on_meta type="mention_page">33</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Dumortier" date="1829" rank="tribe">rubeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rubus</taxon_name>
    <taxon_name authority="Maximowicz" date="1872" rank="species">phoenicolasius</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Acad. Imp. Sci. Saint-Pétersbourg</publication_title>
      <place_in_publication>17: 160. 1872</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe rubeae;genus rubus;species phoenicolasius</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">200011527</other_info_on_name>
  </taxon_identification>
  <number>28.</number>
  <other_name type="common_name">Wineberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 (–30) dm, armed.</text>
      <biological_entity id="o15794" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="armed" value_original="armed" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems biennial, arching, sparsely to moderately hairy, long-stipitate-glandular, not pruinose;</text>
      <biological_entity id="o15795" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>prickles sparse to dense, erect to hooked, slender, 2–8 (–10) mm, slender-based;</text>
      <biological_entity id="o15796" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character char_type="range_value" from="sparse" name="density" src="d0_s2" to="dense" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bristles dense, 2–6 mm, usually gland-tipped and reddish purple, glands ellipsoid to narrowly cupshaped.</text>
      <biological_entity id="o15797" name="bristle" name_original="bristles" src="d0_s3" type="structure">
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish purple" value_original="reddish purple" />
      </biological_entity>
      <biological_entity id="o15798" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s3" to="narrowly cupshaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deciduous, ternate;</text>
      <biological_entity id="o15799" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="ternate" value_original="ternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules filiform, 5–12 mm;</text>
      <biological_entity id="o15800" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>terminal leaflets broadly deltate or ovate to suborbiculate, 4–15 × 3.5–14 cm, base truncate, rounded, or shallowly cordate, unlobed or dentate to shallowly, sharply or bluntly 3-lobed, margins coarsely serrate or doubly serrate, apex acute to acuminate, abaxial surfaces with scattered, erect to curved prickles on moderate to large-sized veins, densely white-hairy, moderately to densely long-stipitate-glandular along veins.</text>
      <biological_entity constraint="terminal" id="o15801" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="suborbiculate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="width" src="d0_s6" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15802" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="unlobed" value_original="unlobed" />
        <character constraint="to margins" constraintid="o15803" is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="doubly; doubly" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15803" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="shallowly; sharply; bluntly" name="shape" src="d0_s6" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15804" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15805" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character char_type="range_value" from="densely white-hairy" name="pubescence" notes="" src="d0_s6" to="moderately densely long-stipitate-glandular" />
        <character char_type="range_value" constraint="along veins" constraintid="o15808" from="densely white-hairy" name="pubescence" src="d0_s6" to="moderately densely long-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o15806" name="prickle" name_original="prickles" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="true" name="course" src="d0_s6" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o15807" name="vein" name_original="veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="moderate" is_modifier="true" name="size" src="d0_s6" to="large-sized" />
      </biological_entity>
      <biological_entity id="o15808" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <relation from="o15805" id="r1009" name="with" negation="false" src="d0_s6" to="o15806" />
      <relation from="o15806" id="r1010" name="on" negation="false" src="d0_s6" to="o15807" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal and axillary, 5–30-flowered, cymiform to racemiform.</text>
      <biological_entity id="o15809" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-30-flowered" value_original="5-30-flowered" />
        <character char_type="range_value" from="cymiform" name="architecture" src="d0_s7" to="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels usually unarmed or prickles sparse, erect, moderately to densely hairy, densely long-stipitate-glandular.</text>
      <biological_entity id="o15810" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unarmed" value_original="unarmed" />
      </biological_entity>
      <biological_entity id="o15811" name="prickle" name_original="prickles" src="d0_s8" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s8" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers bisexual;</text>
      <biological_entity id="o15812" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white to pink, ovate to suborbiculate, 4–6 mm;</text>
      <biological_entity id="o15813" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="pink" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="suborbiculate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments laminar;</text>
      <biological_entity id="o15814" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="laminar" value_original="laminar" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries glabrous.</text>
      <biological_entity id="o15815" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits red to maroon, globose, 1–1.5 cm;</text>
      <biological_entity id="o15816" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s13" to="maroon" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s13" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>drupelets 15–40, strongly coherent, separating from torus.</text>
      <biological_entity id="o15818" name="torus" name_original="torus" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 14.</text>
      <biological_entity id="o15817" name="drupelet" name_original="drupelets" src="d0_s14" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s14" to="40" />
        <character is_modifier="false" modifier="strongly" name="fusion" src="d0_s14" value="coherent" value_original="coherent" />
        <character constraint="from torus" constraintid="o15818" is_modifier="false" name="arrangement" src="d0_s14" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15819" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woodlands, roadsides, disturbed open areas, moist soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed open areas" />
        <character name="habitat" value="soil" modifier="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Ky., Md., Mass., Mo., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.; e Asia (China, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="e Asia (China)" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="e Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rubus phoenicolasius was introduced to North America for edible fruit, breeding stock, and for ornament. Attractive for its typically reddish purple glandular hairs, R. phoenicolasius nonetheless can be invasive.</discussion>
  
</bio:treatment>