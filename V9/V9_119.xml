<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">81</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="(Cockerell) Cockerell ex Rehder" date="1940" rank="subgenus">hesperhodos</taxon_name>
    <taxon_name authority="Crépin" date="1889" rank="section">Minutifoliae</taxon_name>
    <taxon_name authority="Wooton" date="1898" rank="species">stellata</taxon_name>
    <taxon_name authority="(Greene) W. H. Lewis" date="1965" rank="subspecies">mirifica</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>52: 111. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus hesperhodos;section minutifoliae;species stellata;subspecies mirifica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100511</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rosa</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">mirifica</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>2: 62. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rosa;species mirifica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hesperhodos</taxon_name>
    <taxon_name authority="(Greene) Cockerell" date="unknown" rank="species">mirificus</taxon_name>
    <taxon_hierarchy>genus Hesperhodos;species mirificus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">stellata</taxon_name>
    <taxon_name authority="(Greene) Cockerell" date="unknown" rank="variety">mirifica</taxon_name>
    <taxon_hierarchy>genus R.;species stellata;variety mirifica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vernonii</taxon_name>
    <taxon_hierarchy>genus R.;species vernonii</taxon_hierarchy>
  </taxon_identification>
  <number>2c.</number>
  <other_name type="common_name">Wonderful rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, 10–15 dm;</text>
      <biological_entity id="o32442" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="15" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>distal branches usually glabrous, rarely with stellate hairs, usually stipitate-glandular;</text>
      <biological_entity id="o32444" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o32443" id="r2149" modifier="rarely" name="with" negation="false" src="d0_s1" to="o32444" />
    </statement>
    <statement id="d0_s2">
      <text>infrastipular prickles paired, 10–12 × 3–4 mm, internodal prickles and aciculi mixed with sessile and stipitate-glands.</text>
      <biological_entity constraint="distal" id="o32443" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o32445" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o32446" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character constraint="with stipitate-glands" constraintid="o32447" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o32447" name="stipitate-gland" name_original="stipitate-glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules 8 × 2 mm, margins entire, sessile-glandular, glands sparse, auricles not notably foliaceous;</text>
      <biological_entity id="o32448" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o32449" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character name="length" src="d0_s3" unit="mm" value="8" value_original="8" />
        <character name="width" src="d0_s3" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o32450" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o32451" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o32452" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not notably" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole and rachis glabrous or puberulent;</text>
      <biological_entity id="o32453" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o32454" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o32455" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets (3–) 5, terminal: petiolule 1.5–2 mm, blade deltate, margins 1-serrate, teeth 4–6 per side, abaxial surfaces glabrous or tomentulose on midveins and elsewhere, adaxial dull.</text>
      <biological_entity id="o32456" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character name="quantity" src="d0_s5" value="5" value_original="5" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o32457" name="petiolule" name_original="petiolule" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32458" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
      </biological_entity>
      <biological_entity id="o32459" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-serrate" value_original="1-serrate" />
      </biological_entity>
      <biological_entity id="o32460" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o32461" from="4" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o32461" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o32462" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="on midveins" constraintid="o32463" is_modifier="false" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
      <biological_entity id="o32463" name="midvein" name_original="midveins" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o32464" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="elsewhere" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 4 cm diam.;</text>
      <biological_entity id="o32465" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character name="diameter" src="d0_s6" unit="cm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium setose, setae 1.5–3 mm;</text>
      <biological_entity id="o32466" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o32467" name="seta" name_original="setae" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 16–20 × 5 mm;</text>
      <biological_entity id="o32468" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s8" to="20" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pink.</text>
      <biological_entity id="o32469" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Hips broadly subglobose, 9–15 mm diam., densely setose.</text>
      <biological_entity id="o32470" name="hip" name_original="hips" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="9" from_unit="mm" name="diameter" src="d0_s10" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Igneous or limestone cliffs, arroyos, edges of pinyon pine woods, along streams, roads, openings in pine-juniper woods, rocky north slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cliffs" modifier="igneous or" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="edges" constraint="of pinyon pine woods" />
        <character name="habitat" value="pinyon pine woods" />
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="roads" />
        <character name="habitat" value="openings" constraint="in pine-juniper woods" />
        <character name="habitat" value="pine-juniper woods" />
        <character name="habitat" value="rocky north slopes" />
        <character name="habitat" value="igneous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies mirifica occurs to the east of subsp. stellata in the Sacramento and White mountains of Otero County, New Mexico, to Culberson and Hudspeth counties in the Guadalupe and Eagle mountains of western Texas, where it is found on Permian limestone.</discussion>
  <discussion>Within subsp. mirifica is var. erlansoniae W. H. Lewis (Erlanson rose) having floral branches flexuous at stem nodes, devoid of internodal prickles or nearly so, sepals entire rather than pinnatifid as in var. mirifica, leaflets and stems glabrous, and pedicels eglandular and glabrous; var. erlansoniae is known only from the Guadalupe Mountains of western Texas (W. H. Lewis 1965).</discussion>
  
</bio:treatment>