<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="section">Systylae</taxon_name>
    <taxon_name authority="Franchet &amp; Rochebrune ex Crépin" date="unknown" rank="species">lucieae</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Roy. Bot. Belgique</publication_title>
      <place_in_publication>10: 324. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section systylae;species lucieae;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100417</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rosa</taxon_name>
    <taxon_name authority="Crépin" date="unknown" rank="species">wichuraiana</taxon_name>
    <taxon_hierarchy>genus Rosa;species wichuraiana</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Lucie or memorial rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems procumbent or climbing, 10+ dm, rooting at nodes;</text>
      <biological_entity id="o31603" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="climbing" value_original="climbing" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" upper_restricted="false" />
        <character constraint="at nodes" constraintid="o31604" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o31604" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>bark of canes green or brown;</text>
      <biological_entity id="o31605" name="bark" name_original="bark" src="d0_s1" type="structure" constraint="cane" constraint_original="cane; cane">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o31606" name="cane" name_original="canes" src="d0_s1" type="structure" />
      <relation from="o31605" id="r2105" name="part_of" negation="false" src="d0_s1" to="o31606" />
    </statement>
    <statement id="d0_s2">
      <text>prickles infrastipular and/or internodal, single or paired, curved or declined, ± stout, 4–5 × 2 mm, aciculi absent.</text>
      <biological_entity id="o31607" name="prickle" name_original="prickles" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="internodal" value_original="internodal" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="paired" value_original="paired" />
        <character is_modifier="false" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="declined" value_original="declined" />
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous (north) or semipersistent (south), 8–10 cm;</text>
      <biological_entity id="o31608" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s3" value="semipersistent" value_original="semipersistent" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>stipules narrowly lanceolate, 10–12 × 2–3 mm, auricles erect, sometimes flared, 2–4 mm, margins fimbriate, stipitate-glandular, surfaces glabrous, eglandular;</text>
      <biological_entity id="o31609" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31610" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="flared" value_original="flared" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31611" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="fimbriate" value_original="fimbriate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o31612" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole and rachis with pricklets, glabrous, stipitate or, sometimes, sessile-glandular;</text>
      <biological_entity id="o31613" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="stipitate" value_original="stipitate" />
        <character name="architecture" src="d0_s5" value="," value_original="," />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o31614" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o31615" name="pricklet" name_original="pricklets" src="d0_s5" type="structure" />
      <relation from="o31613" id="r2106" name="with" negation="false" src="d0_s5" to="o31615" />
      <relation from="o31614" id="r2107" name="with" negation="false" src="d0_s5" to="o31615" />
    </statement>
    <statement id="d0_s6">
      <text>leaflets 7–9, terminal: petiolule 5–9 mm, blade broadly ovate to obovate, 15–30 × 12–20 mm, leathery, base cuneate, margins 1 (–2) -serrate, teeth 12–16 per side, acute, gland-tipped, apex acute to acuminate, abaxial surfaces glabrous, eglandular except for glands on midveins, adaxial green, lustrous, glabrous.</text>
      <biological_entity id="o31616" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="9" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o31617" name="petiolule" name_original="petiolule" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31618" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="30" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o31619" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o31620" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="1(-2)-serrate" value_original="1(-2)-serrate" />
      </biological_entity>
      <biological_entity id="o31621" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o31622" from="12" name="quantity" src="d0_s6" to="16" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o31622" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o31623" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31624" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o31625" name="gland" name_original="glands" src="d0_s6" type="structure" />
      <biological_entity id="o31626" name="midvein" name_original="midveins" src="d0_s6" type="structure" />
      <biological_entity constraint="adaxial" id="o31627" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o31625" id="r2108" name="on" negation="false" src="d0_s6" to="o31626" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles (1–) 5–20+-flowered.</text>
      <biological_entity id="o31628" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="(1-)5-20+-flowered" value_original="(1-)5-20+-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 18–25 mm, glabrous, eglandular;</text>
      <biological_entity id="o31629" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 1–3, lanceolate, 9–16 × 2.5–5 mm, margins sparsely stipitate-glandular, surfaces glabrous, eglandular.</text>
      <biological_entity id="o31630" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="3" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="16" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31631" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o31632" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers scent of apple or clover, 2–2.5 cm diam.;</text>
      <biological_entity id="o31633" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s10" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium urceolate, 4–6.5 × 2–3 mm, eglandular, neck (0–) 1 × 4–5 mm;</text>
      <biological_entity id="o31634" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="urceolate" value_original="urceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o31635" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_length" src="d0_s11" to="1" to_inclusive="false" to_unit="mm" />
        <character name="length" src="d0_s11" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals ovate-acuminate, 6–8 × 1–1.5 mm, margins pinnatifid, tip 2 × 0.5 mm, abaxial surfaces glabrous, eglandular;</text>
      <biological_entity id="o31636" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31637" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o31638" name="tip" name_original="tip" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="2" value_original="2" />
        <character name="width" src="d0_s12" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o31639" name="surface" name_original="surfaces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals double, sometimes single, white, pink to rose distally, 13–15 × 11–15 mm;</text>
      <biological_entity id="o31640" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="pink" modifier="distally" name="coloration" src="d0_s13" to="rose" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s13" to="15" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s13" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 12–21, styles pilose, exsert 3.5–5 mm beyond stylar orifice (1.5–2 mm diam.), hypanthial disc appearing flattened with age, 3–4 mm diam.</text>
      <biological_entity id="o31641" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s14" to="21" />
      </biological_entity>
      <biological_entity id="o31642" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="position" src="d0_s14" value="exsert" value_original="exsert" />
        <character char_type="range_value" constraint="beyond stylar, orifice" constraintid="o31643, o31644" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31643" name="stylar" name_original="stylar" src="d0_s14" type="structure" />
      <biological_entity id="o31644" name="orifice" name_original="orifice" src="d0_s14" type="structure" />
      <biological_entity constraint="hypanthial" id="o31645" name="disc" name_original="disc" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31646" name="age" name_original="age" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o31645" id="r2109" name="appearing" negation="false" src="d0_s14" to="o31646" />
    </statement>
    <statement id="d0_s15">
      <text>Hips red, globose, 5–10 × 5–9 mm, eglandular.</text>
      <biological_entity id="o31647" name="hip" name_original="hips" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s15" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s15" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes 1–11, dark tan, 4–4.5 × 2–2.5 mm. 2n = 14 (28).</text>
      <biological_entity id="o31648" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s16" to="11" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark tan" value_original="dark tan" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31649" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
        <character name="atypical_quantity" src="d0_s16" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas, roadsides, flood plains, old homesteads, dry woods, highway verges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="old homesteads" />
        <character name="habitat" value="dry woods" />
        <character name="habitat" value="highway verges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ala., Ark., Calif., Conn., Fla., Ga., Ill., Iowa, Ky., La., Md., Mass., Miss., Mo., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; Asia (China, Japan, Korea); introduced also in Mexico, Pacific Islands (New Zealand, Philippines).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Philippines)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">luciae</other_name>
  <discussion>Introductions of Rosa lucieae in North America are largely from cultivars. The species is most common in southern and mid-latitudes of eastern and south-central United States. With spreading stems to 60 dm, R. lucieae is a typical stoloniferous plant forming carpets of colorful groundcover.</discussion>
  <discussion>Rosa lucieae cultivars and hybrids have been widely used in breeding rambler and climbing roses. Its creeping stems extend to 60 dm and root readily, forming a white- or pink-petaled ground cover for gardens and cemeteries, and along highways where it also serves to stabilize verges. The most commonly found cultivar in North America is 'Dorothy Perkins'.</discussion>
  
</bio:treatment>