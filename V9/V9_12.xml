<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">11</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">crossosomataceae</taxon_name>
    <taxon_name authority="C. T. Mason" date="unknown" rank="genus">apacheria</taxon_name>
    <taxon_name authority="C. T. Mason" date="1975" rank="species">chiricahuensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>23: 105, figs. 1, 2a. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family crossosomataceae;genus apacheria;species chiricahuensis</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220000925</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Chiricahua rockflower</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 7–50 cm (often wider than tall).</text>
      <biological_entity id="o18602" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 3.5–7.5 × 1–2.3 mm.</text>
      <biological_entity id="o18603" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s1" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sepals 3–3.5 mm, usually 2 opposing sepals slightly narrower and longer than other pair.</text>
      <biological_entity id="o18604" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="3.5" to_unit="mm" />
        <character modifier="usually" name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s2" value="narrower" value_original="narrower" />
        <character constraint="than other pair" constraintid="" is_modifier="false" name="length_or_size" src="d0_s2" value="slightly narrower and longer" value_original="slightly narrower and longer" />
      </biological_entity>
      <biological_entity id="o18605" name="sepal" name_original="sepals" src="d0_s2" type="structure" />
      <relation from="o18604" id="r1203" name="opposing" negation="false" src="d0_s2" to="o18605" />
    </statement>
    <statement id="d0_s3">
      <text>Follicles 4–5 mm.</text>
      <biological_entity id="o18606" name="follicle" name_original="follicles" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Seeds 1–1.5 × 0.8–1 mm.</text>
      <biological_entity id="o18607" name="seed" name_original="seeds" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices, cliff faces, on igneous or volcanic substrates (mostly rhyolite)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="cliff" />
        <character name="habitat" value="igneous" />
        <character name="habitat" value="volcanic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Apacheria chiricahuensis has not been documented to occur outside the flora area; its presence in poorly botanized portions of adjacent northwestern Mexico is likely.</discussion>
  
</bio:treatment>