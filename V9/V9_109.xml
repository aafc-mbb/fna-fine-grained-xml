<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James Henrickson,Bruce D. Parfitt†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Endlicher" date="unknown" rank="genus">FALLUGIA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Pl.</publication_title>
      <place_in_publication>16: 1246. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus FALLUGIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Virgilio Fallugi, 1627–1707, Italian abbot</other_info_on_name>
    <other_info_on_name type="fna_id">112641</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Apache plume</other_name>
  <other_name type="common_name">yerba del pasmo</other_name>
  <other_name type="common_name">barba de chivo</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 (–35) dm, polygamodioecious;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous in sandy soil.</text>
      <biological_entity id="o5840" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="polygamodioecious" value_original="polygamodioecious" />
        <character constraint="in soil" constraintid="o5841" is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o5841" name="soil" name_original="soil" src="d0_s1" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s1" value="sandy" value_original="sandy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20+, spreading-ascending;</text>
      <biological_entity id="o5842" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading-ascending" value_original="spreading-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>bark brown, with age separating in papery sheets;</text>
      <biological_entity id="o5843" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character constraint="in sheets" constraintid="o5844" is_modifier="false" name="arrangement" src="d0_s3" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o5844" name="sheet" name_original="sheets" src="d0_s3" type="structure">
        <character is_modifier="true" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>long and short-shoots present;</text>
      <biological_entity id="o5845" name="short-shoot" name_original="short-shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="true" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>young stems whitish, hirtellous to villous, also stellate-lepidote with white to rust orange trichomes.</text>
      <biological_entity id="o5846" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s5" to="villous" />
        <character constraint="with trichomes" constraintid="o5847" is_modifier="false" name="pubescence" src="d0_s5" value="stellate-lepidote" value_original="stellate-lepidote" />
      </biological_entity>
      <biological_entity id="o5847" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="white" is_modifier="true" name="coloration" src="d0_s5" to="rust orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves deciduous, cauline, abscising distal to persistent, clasping bases, crowded on short-shoots, simple;</text>
      <biological_entity id="o5848" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="position" src="d0_s6" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="life_cycle" src="d0_s6" value="abscising" value_original="abscising" />
        <character is_modifier="false" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o5849" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character constraint="on short-shoots" constraintid="o5850" is_modifier="false" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o5850" name="short-shoot" name_original="short-shoots" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>stipules adnate, acicular, reduced on short-shoot leaves, margins entire;</text>
      <biological_entity id="o5851" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acicular" value_original="acicular" />
        <character constraint="on short-shoot leaves" constraintid="o5852" is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="short-shoot" id="o5852" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o5853" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole present;</text>
      <biological_entity id="o5854" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade obovate to oblanceolate or linear, 1–2-pinnately lobed, 0.4–3 cm, leathery, margins revolute, entire, venation pinnate, surfaces hirtellous, villous, orange-lepidote throughout, abaxially densely so, glabrescent.</text>
      <biological_entity id="o5855" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s9" to="oblanceolate or linear" />
        <character is_modifier="false" modifier="1-2-pinnately" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o5856" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o5857" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s9" value="orange-lepidote" value_original="orange-lepidote" />
        <character is_modifier="false" modifier="abaxially densely; densely" name="pubescence" src="d0_s9" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal on long-shoots of season, 1–7-flowered, loose corymbose racemes;</text>
      <biological_entity id="o5858" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character constraint="on long-shoots" constraintid="o5859" is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o5859" name="long-shoot" name_original="long-shoots" src="d0_s10" type="structure" />
      <biological_entity id="o5860" name="season" name_original="season" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-7-flowered" value_original="1-7-flowered" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s10" value="loose" value_original="loose" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="corymbose" value_original="corymbose" />
      </biological_entity>
      <biological_entity id="o5861" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-7-flowered" value_original="1-7-flowered" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s10" value="loose" value_original="loose" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="corymbose" value_original="corymbose" />
      </biological_entity>
      <relation from="o5859" id="r355" name="part_of" negation="false" src="d0_s10" to="o5860" />
      <relation from="o5859" id="r356" name="part_of" negation="false" src="d0_s10" to="o5861" />
    </statement>
    <statement id="d0_s11">
      <text>bracts present, reduced;</text>
      <biological_entity id="o5862" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s11" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracteoles present often linear-acicular or with reduced, paired, basal lobes borne at pedicel base.</text>
      <biological_entity id="o5863" name="bracteole" name_original="bracteoles" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="linear-acicular" value_original="linear-acicular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="with reduced , paired , basal lobes" />
      </biological_entity>
      <biological_entity constraint="basal" id="o5864" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="true" name="size" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="arrangement" src="d0_s12" value="paired" value_original="paired" />
      </biological_entity>
      <biological_entity constraint="pedicel" id="o5865" name="base" name_original="base" src="d0_s12" type="structure" />
      <relation from="o5863" id="r357" name="with" negation="false" src="d0_s12" to="o5864" />
      <relation from="o5864" id="r358" name="borne at" negation="false" src="d0_s12" to="o5865" />
    </statement>
    <statement id="d0_s13">
      <text>Pedicels present.</text>
      <biological_entity id="o5866" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers all pistillate, all staminate, or staminate with terminal one bisexual, 20–35 (–42) mm diam.;</text>
      <biological_entity id="o5867" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character constraint="with bisexual" constraintid="o5868" is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="diameter" notes="" src="d0_s14" to="42" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" notes="" src="d0_s14" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5868" name="bisexual" name_original="bisexual" src="d0_s14" type="structure" constraint="terminal" />
    </statement>
    <statement id="d0_s15">
      <text>epicalyx bractlets 5, entire or 3-toothed;</text>
      <biological_entity constraint="epicalyx" id="o5869" name="bractlet" name_original="bractlets" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>hypanthium broadly funnelform, 2.5–3.5 mm, exterior rusty lepidote-stellate and villose-pilose, interior densely hirsute;</text>
      <biological_entity id="o5870" name="hypanthium" name_original="hypanthium" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5871" name="exterior" name_original="exterior" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="rusty" value_original="rusty" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s16" value="lepidote-stellate" value_original="lepidote-stellate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="villose-pilose" value_original="villose-pilose" />
        <character is_modifier="false" name="position" src="d0_s16" value="interior" value_original="interior" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals 5, ascending, broadly ovate to suborbiculate;</text>
      <biological_entity id="o5872" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s17" to="suborbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>petals 5, white (to pinkish), oblong-obovate to suborbiculate;</text>
      <biological_entity id="o5873" name="petal" name_original="petals" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character char_type="range_value" from="oblong-obovate" name="shape" src="d0_s18" to="suborbiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens (24–) 50–95 (–120), shorter than petals, anthers fertile in staminate flowers, sterile in pistillate flowers;</text>
      <biological_entity id="o5874" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character char_type="range_value" from="24" name="atypical_quantity" src="d0_s19" to="50" to_inclusive="false" />
        <character char_type="range_value" from="95" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="120" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s19" to="95" />
        <character constraint="than petals" constraintid="o5875" is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5875" name="petal" name_original="petals" src="d0_s19" type="structure" />
      <biological_entity id="o5876" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character constraint="in flowers" constraintid="o5877" is_modifier="false" name="reproduction" src="d0_s19" value="fertile" value_original="fertile" />
        <character constraint="in flowers" constraintid="o5878" is_modifier="false" name="reproduction" notes="" src="d0_s19" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o5877" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o5878" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>torus ovoid-cylindric;</text>
      <biological_entity id="o5879" name="torus" name_original="torus" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid-cylindric" value_original="ovoid-cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>carpels (24–) 50–95 (–120), reduced in staminate flowers, sericeous;</text>
      <biological_entity id="o5880" name="carpel" name_original="carpels" src="d0_s21" type="structure">
        <character char_type="range_value" from="24" name="atypical_quantity" src="d0_s21" to="50" to_inclusive="false" />
        <character char_type="range_value" from="95" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="120" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s21" to="95" />
        <character constraint="in flowers" constraintid="o5881" is_modifier="false" name="size" src="d0_s21" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s21" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o5881" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovules 2.</text>
      <biological_entity id="o5882" name="ovule" name_original="ovules" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Fruits aggregated achenes, (24–) 50–95 (–120), compressed, fusiform, 1–2.5 mm, sericeous;</text>
      <biological_entity id="o5883" name="fruit" name_original="fruits" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s23" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s23" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s23" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s23" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o5884" name="achene" name_original="achenes" src="d0_s23" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s23" value="aggregated" value_original="aggregated" />
        <character char_type="range_value" from="24" name="atypical_quantity" src="d0_s23" to="50" to_inclusive="false" />
        <character char_type="range_value" from="95" from_inclusive="false" name="atypical_quantity" src="d0_s23" to="120" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s23" to="95" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>hypanthium persistent;</text>
      <biological_entity id="o5885" name="hypanthium" name_original="hypanthium" src="d0_s24" type="structure">
        <character is_modifier="false" name="duration" src="d0_s24" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>sepals persistent, spreading;</text>
      <biological_entity id="o5886" name="sepal" name_original="sepals" src="d0_s25" type="structure">
        <character is_modifier="false" name="duration" src="d0_s25" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s25" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>torus elongating;</text>
      <biological_entity id="o5887" name="torus" name_original="torus" src="d0_s26" type="structure">
        <character is_modifier="false" name="length" src="d0_s26" value="elongating" value_original="elongating" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>styles persistent, greatly elongating, filiform, plumose.</text>
    </statement>
    <statement id="d0_s28">
      <text>x = 14.</text>
      <biological_entity id="o5888" name="style" name_original="styles" src="d0_s27" type="structure">
        <character is_modifier="false" name="duration" src="d0_s27" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="greatly" name="length" src="d0_s27" value="elongating" value_original="elongating" />
        <character is_modifier="false" name="shape" src="d0_s27" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s27" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="x" id="o5889" name="chromosome" name_original="" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Fallugia has long been considered closely related to Cercocarpus, Cowania, and Dryas. Cytologic (E. D. McArthur et al. 1983), morphologic (J. Henrickson 2001), and molecular (D. R. Morgan et al. 1994; D. Potter et al. 2007) data substantiate its close relationship with Geum in subfam. Rosoideae, tribe Colurieae.</discussion>
  <references>
    <reference>Henrickson, J. 2001. Systematics and relationships of Fallugia (Rosoideae–Rosaceae). Aliso 20: 1–15.</reference>
  </references>
  
</bio:treatment>