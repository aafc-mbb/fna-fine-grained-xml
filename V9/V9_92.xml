<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">peckii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 352. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species peckii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100225</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Mountain avens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants subscapose.</text>
      <biological_entity id="o2448" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="subscapose" value_original="subscapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 7–40 cm, glabrate, sparsely downy, or pilose proximally, downy or pilose distally.</text>
      <biological_entity id="o2449" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="," value_original="," />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 5–15 cm, blade strongly lyrate-pinnate, major leaflet 1, minor leaflets 4–10, terminal leaflet much larger than minor laterals;</text>
      <biological_entity id="o2450" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o2451" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2452" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s2" value="lyrate-pinnate" value_original="lyrate-pinnate" />
      </biological_entity>
      <biological_entity id="o2453" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="minor" id="o2454" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="10" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o2455" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than minor laterals" constraintid="o2456" is_modifier="false" name="size" src="d0_s2" value="much larger" value_original="much larger" />
      </biological_entity>
      <biological_entity constraint="minor" id="o2456" name="lateral" name_original="laterals" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>cauline 0.8–1.7 (–2.5) cm, stipules not evident, blade bractlike, not resembling basal, simple, 3-fid.</text>
      <biological_entity id="o2457" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o2458" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s3" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2459" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o2460" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-fid" value_original="3-fid" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2461" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o2460" id="r150" name="resembling" negation="true" src="d0_s3" to="o2461" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 1–2 (–4) -flowered.</text>
      <biological_entity id="o2462" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-2(-4)-flowered" value_original="1-2(-4)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels densely downy, usually eglandular.</text>
      <biological_entity id="o2463" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="downy" value_original="downy" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o2464" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 2–3 mm;</text>
      <biological_entity constraint="epicalyx" id="o2465" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green;</text>
      <biological_entity id="o2466" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals spreading, 4–7 mm;</text>
      <biological_entity id="o2467" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, yellow, obcordate, nearly orbiculate, or broadly ovate, 9–13 mm, nearly 2 times sepals, apex broadly rounded, emarginate, or irregularly erose.</text>
      <biological_entity id="o2468" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obcordate" value_original="obcordate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="orbiculate" value_original="orbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
        <character constraint="sepal" constraintid="o2469" is_modifier="false" name="size_or_quantity" src="d0_s10" value="2 times sepals" value_original="2 times sepals" />
      </biological_entity>
      <biological_entity id="o2469" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile, glabrous.</text>
      <biological_entity id="o2471" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <relation from="o2470" id="r151" name="fruiting" negation="false" src="d0_s11" to="o2471" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles wholly persistent, not geniculate-jointed, 6–9 mm, apex not hooked, pilose in basal 1/3, eglandular.</text>
      <biological_entity id="o2470" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2472" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o2473" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="wholly" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o2475" name="1/3" name_original="1/3" src="d0_s12" type="structure" />
      <relation from="o2470" id="r152" name="fruiting" negation="false" src="d0_s12" to="o2472" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 42.</text>
      <biological_entity id="o2474" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character constraint="in basal 1/3" constraintid="o2475" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2476" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, wet spots on rocky cliffs and slopes, montane streamsides, coastal bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="wet spots" constraint="on rocky cliffs and slopes" />
        <character name="habitat" value="rocky cliffs" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="streamsides" modifier="montane" />
        <character name="habitat" value="coastal bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.S.; N.H.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Geum peckii occurs at 0–10 m in Nova Scotia and at 1500–1900 m in New Hampshire.</discussion>
  
</bio:treatment>