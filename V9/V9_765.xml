<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">452</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="(W. W. Smith) Flinck &amp; B. Hylmö" date="1966" rank="species">monopyrenus</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>119: 459. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species monopyrenus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100049</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cotoneaster</taxon_name>
    <taxon_name authority="Diels" date="unknown" rank="species">hebephyllus</taxon_name>
    <taxon_name authority="W. W. Smith" date="unknown" rank="variety">monopyrenus</taxon_name>
    <place_of_publication>
      <publication_title>Notes Roy. Bot. Gard. Edinburgh</publication_title>
      <place_in_publication>10: 23. 1917 (as hebephylla var. monopyrena)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cotoneaster;species hebephyllus;variety monopyrenus</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">One-stoned cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 3–5 m.</text>
      <biological_entity id="o30841" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="5" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems loosely erect, arching, spreading;</text>
      <biological_entity id="o30843" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches spiraled, maroon, lenticellate, initially pilose-strigose.</text>
      <biological_entity id="o30844" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="spiraled" value_original="spiraled" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="maroon" value_original="maroon" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s2" value="lenticellate" value_original="lenticellate" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s2" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o30845" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 5–9 mm, pilose-strigose;</text>
      <biological_entity id="o30846" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade elliptic, broadly elliptic, or broadly obovate, sometimes suborbiculate, 25–58 × 15–40 mm, subcoriaceous, base obtuse or cuneate, margins flat, veins 4–6, slightly sunken, apex obtuse or truncate, seldom acute, abaxial surfaces color not recorded, reticulate, tomentose-pilose, later thinning, adaxial dark green, dull, coating not recorded, flat between lateral-veins, glabrescent;</text>
      <biological_entity id="o30847" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s5" to="58" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s5" to="40" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o30848" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o30849" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o30850" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="6" />
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s5" value="sunken" value_original="sunken" />
      </biological_entity>
      <biological_entity id="o30851" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="seldom" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30852" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose-pilose" value_original="tomentose-pilose" />
        <character is_modifier="false" name="condition" src="d0_s5" value="later" value_original="later" />
      </biological_entity>
      <biological_entity id="o30854" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves pale yellowish green.</text>
      <biological_entity constraint="adaxial" id="o30853" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character constraint="between lateral-veins" constraintid="o30854" is_modifier="false" modifier="not" name="coating" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale yellowish" value_original="pale yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o30855" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o30853" id="r2052" name="fall" negation="false" src="d0_s6" to="o30855" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 30–50 mm with 3–4 leaves, 7–20-flowered, lax.</text>
      <biological_entity id="o30856" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="7-20-flowered" value_original="7-20-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o30857" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with leaves" constraintid="o30858" from="30" from_unit="mm" name="some_measurement" src="d0_s7" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30858" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o30856" id="r2053" name="on" negation="false" src="d0_s7" to="o30857" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 2–5 mm, pilose-strigose.</text>
      <biological_entity id="o30859" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 10–12 mm diam., buds white, hypanthium campanulate to cupulate, sparsely pilose-strigose;</text>
      <biological_entity id="o30860" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30861" name="bud" name_original="buds" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o30862" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s9" to="cupulate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals: margins reddish, villous, borders maroon-tipped, membranous, apex acute, acuminate, or obtuse, surfaces sparsely pilose-strigose;</text>
      <biological_entity id="o30863" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
      <biological_entity id="o30864" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o30865" name="border" name_original="borders" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="maroon-tipped" value_original="maroon-tipped" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o30866" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o30867" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals spreading, white, sometimes with hair-tuft;</text>
      <biological_entity id="o30868" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o30869" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o30870" name="hair-tuft" name_original="hair-tuft" src="d0_s11" type="structure" />
      <relation from="o30869" id="r2054" modifier="sometimes" name="with" negation="false" src="d0_s11" to="o30870" />
    </statement>
    <statement id="d0_s12">
      <text>stamens (15–) 20, filaments white;</text>
      <biological_entity id="o30871" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o30872" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s12" to="20" to_inclusive="false" />
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o30873" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers purple to blackish purple;</text>
      <biological_entity id="o30874" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o30875" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="purple" name="coloration_or_density" src="d0_s13" to="blackish purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 1 (or 2).</text>
      <biological_entity id="o30876" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o30877" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes dark red or ruby to maroon, maturing dark purple or purple-black, obovoid, rarely globose or ellipsoid, 9–12.5 × 8.5–11.5 mm, dull, glaucous, glabrous;</text>
      <biological_entity id="o30878" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character char_type="range_value" from="ruby" name="coloration" src="d0_s15" to="maroon" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark purple" value_original="dark purple" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple-black" value_original="purple-black" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s15" to="12.5" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="width" src="d0_s15" to="11.5" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals flat, margins sparsely hairy, sparsely hairy to glabrate;</text>
      <biological_entity id="o30879" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>navel open;</text>
      <biological_entity id="o30880" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
        <character char_type="range_value" from="sparsely hairy" name="pubescence" src="d0_s16" to="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants at apex.</text>
      <biological_entity constraint="style" id="o30881" name="remnant" name_original="remnants" src="d0_s18" type="structure" />
      <biological_entity id="o30882" name="apex" name_original="apex" src="d0_s18" type="structure" />
      <relation from="o30881" id="r2055" name="at" negation="false" src="d0_s18" to="o30882" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes 1 (or 2).</text>
      <biological_entity id="o30883" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Jul–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Wash.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>L. Lingdi and A. R. Brach (2003) synonymized Cotoneaster monopyrenus with C. hebephyllus (as C. hebephyllus var. hebephyllus).</discussion>
  
</bio:treatment>