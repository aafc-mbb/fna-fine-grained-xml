<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Luc Brouillet</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">Gillenieae</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Pterburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 164, 222. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe Gillenieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20931</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>c8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>unarmed.</text>
      <biological_entity id="o26503" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unarmed" value_original="unarmed" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate, pinnately compound (ternate);</text>
      <biological_entity id="o26504" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s2" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>stipules persistent or deciduous, free;</text>
    </statement>
    <statement id="d0_s4">
      <text>venation pinnate.</text>
      <biological_entity id="o26505" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: perianth and androecium perigynous;</text>
      <biological_entity id="o26506" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o26507" name="perianth" name_original="perianth" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o26508" name="androecium" name_original="androecium" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>epicalyx bractlets absent;</text>
      <biological_entity id="o26509" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity constraint="epicalyx" id="o26510" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>hypanthium tubular to campanulate;</text>
      <biological_entity id="o26511" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o26512" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s7" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>torus absent;</text>
      <biological_entity id="o26513" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26514" name="torus" name_original="torus" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>carpels 5, basally connate (becoming distinct at maturity), adnate to hypanthium, styles terminal, distinct;</text>
      <biological_entity id="o26515" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o26516" name="carpel" name_original="carpels" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="5" value_original="5" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s9" value="connate" value_original="connate" />
        <character constraint="to hypanthium" constraintid="o26517" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o26517" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure" />
      <biological_entity id="o26518" name="style" name_original="styles" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovules 2–4, basal, collateral (obturator present).</text>
      <biological_entity id="o26519" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26520" name="ovule" name_original="ovules" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="4" />
        <character is_modifier="false" name="position" src="d0_s10" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s10" value="collateral" value_original="collateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits aggregated follicles;</text>
      <biological_entity id="o26521" name="fruit" name_original="fruits" src="d0_s11" type="structure" />
      <biological_entity id="o26522" name="follicle" name_original="follicles" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="aggregated" value_original="aggregated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles tardily deciduous, elongate.</text>
      <biological_entity id="o26523" name="style" name_original="styles" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genus 1, species 2 (2 in the flora).</discussion>
  <discussion>The base chromosome number for Gillenieae is x = 9. Gillenia is host to Gymnosporangium and Phragmidium rusts.</discussion>
  
</bio:treatment>