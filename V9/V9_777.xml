<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">462</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Medikus" date="1789" rank="genus">cotoneaster</taxon_name>
    <taxon_name authority="Flinck &amp; B. Hylmö" date="1991" rank="species">hjelmqvistii</taxon_name>
    <place_of_publication>
      <publication_title>Watsonia</publication_title>
      <place_in_publication>18: 312. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus cotoneaster;species hjelmqvistii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100044</other_info_on_name>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Hjelmqvist’s cotoneaster</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 0.5–1 m.</text>
      <biological_entity id="o13465" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="1" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, spreading horizontally, arching;</text>
      <biological_entity id="o13466" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="horizontally" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches usually distichous, brownish red, initially densely strigose.</text>
      <biological_entity id="o13467" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="distichous" value_original="distichous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" modifier="initially densely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves deciduous;</text>
      <biological_entity id="o13468" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–4 mm, pilose;</text>
      <biological_entity id="o13469" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade broadly elliptic, on vigorous shoots orbiculate, sometimes broadly obovate, concave, 10–18 (–25) x 9–15 (–25) mm, chartaceous, base obtuse, margins flat, veins 3 or 4 (or 5), superficial or slightly sunken, apex obtuse, mucronulate, abaxial surfaces pale green, yellowish pilose-strigose, adaxial vivid light green, shiny, not glaucous, flat between lateral-veins, glabrous;</text>
      <biological_entity id="o13470" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="sometimes broadly" name="shape" notes="" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="18" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o13471" name="shoot" name_original="shoots" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="vigorous" value_original="vigorous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbiculate" value_original="orbiculate" />
      </biological_entity>
      <biological_entity id="o13472" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o13473" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o13474" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" unit="or" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s5" value="superficial" value_original="superficial" />
        <character is_modifier="false" modifier="slightly" name="prominence" src="d0_s5" value="sunken" value_original="sunken" />
      </biological_entity>
      <biological_entity id="o13475" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13476" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose-strigose" value_original="pilose-strigose" />
      </biological_entity>
      <biological_entity id="o13478" name="lateral-vein" name_original="lateral-veins" src="d0_s5" type="structure" />
      <relation from="o13470" id="r856" name="on" negation="false" src="d0_s5" to="o13471" />
    </statement>
    <statement id="d0_s6">
      <text>fall leaves red to intense red-purple.</text>
      <biological_entity constraint="adaxial" id="o13477" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="vivid light" value_original="vivid light" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character constraint="between lateral-veins" constraintid="o13478" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s6" to="intense red-purple" />
      </biological_entity>
      <biological_entity id="o13479" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o13477" id="r857" name="fall" negation="false" src="d0_s6" to="o13479" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences on fertile shoots 5–25 mm with 3 or 4 leaves, 1–3 (or 4) -flowered, compact.</text>
      <biological_entity id="o13480" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="1-3-flowered" value_original="1-3-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o13481" name="shoot" name_original="shoots" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character char_type="range_value" constraint="with 3 or 4leaves" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <relation from="o13480" id="r858" name="on" negation="false" src="d0_s7" to="o13481" />
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.5–2 (–3) mm, glabrescent.</text>
      <biological_entity id="o13482" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 5–6 mm, open;</text>
      <biological_entity id="o13483" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium cupulate, glabrescent;</text>
      <biological_entity id="o13484" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals: margins villous, apex green and reddish purple, acuminate or acute, surfaces glabrous;</text>
      <biological_entity id="o13485" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o13486" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o13487" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green and reddish purple" value_original="green and reddish purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13488" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals erect-incurved, dark-pink, base red, margins pink;</text>
      <biological_entity id="o13489" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <biological_entity id="o13490" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect-incurved" value_original="erect-incurved" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-pink" value_original="dark-pink" />
      </biological_entity>
      <biological_entity id="o13491" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o13492" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 10–12 (–16), filaments red and pale crimson, anthers white;</text>
      <biological_entity id="o13493" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
      <biological_entity id="o13494" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="16" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
      <biological_entity id="o13495" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="red and pale crimson" value_original="red and pale crimson" />
      </biological_entity>
      <biological_entity id="o13496" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 2 or 3.</text>
      <biological_entity id="o13497" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
      <biological_entity id="o13498" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s14" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pomes orange-red, usually broadly obovoid, sometimes subglobose or obovoid, 5.7–6.8 [–8] x 4.9–7.2 [–8] mm, shiny, not glaucous, glabrous;</text>
      <biological_entity id="o13499" name="pome" name_original="pomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s15" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="6.8" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="8" />
        <character char_type="range_value" from="5.7" name="quantity" src="d0_s15" to="6.8" />
        <character char_type="range_value" from="7.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="some_measurement" src="d0_s15" to="7.2" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s15" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals suberect, margins villous, glabrous;</text>
      <biological_entity id="o13500" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s16" value="suberect" value_original="suberect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>navel open;</text>
      <biological_entity id="o13501" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style remnants 4/5 from base.</text>
      <biological_entity constraint="style" id="o13502" name="remnant" name_original="remnants" src="d0_s18" type="structure">
        <character constraint="from base" constraintid="o13503" name="quantity" src="d0_s18" value="4/5" value_original="4/5" />
      </biological_entity>
      <biological_entity id="o13503" name="base" name_original="base" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Pyrenes 2 or 3, distinct.</text>
      <biological_entity id="o13504" name="pyrene" name_original="pyrenes" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s19" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May; fruiting Oct–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="fruiting time" char_type="range_value" to="Nov" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thickets, wet prairie remnants</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="thickets" />
        <character name="habitat" value="wet prairie remnants" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Oreg.; Asia (China); introduced also in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="also in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cotoneaster hjelmqvistii was overlooked in the Flora of China (L. Lingdi and A. R. Brach 2003).</discussion>
  
</bio:treatment>