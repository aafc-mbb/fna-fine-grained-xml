<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="species">macrophyllum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.,</publication_title>
      <place_in_publication>557. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species macrophyllum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100224</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Large-leaved avens</other_name>
  <other_name type="common_name">benoîte à grandes feuilles</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants leafy-stemmed.</text>
      <biological_entity id="o31422" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="leafy-stemmed" value_original="leafy-stemmed" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 30–110 cm, puberulent and hirsute or sparsely hirsute.</text>
      <biological_entity id="o31423" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="110" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 10–45 cm, blade interruptedly lyrate-pinnate, major leaflets 5–9, alternating with 4–15 minor ones, terminal leaflet usually much larger than major laterals;</text>
      <biological_entity id="o31424" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o31425" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31426" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="interruptedly" name="architecture_or_shape" src="d0_s2" value="lyrate-pinnate" value_original="lyrate-pinnate" />
      </biological_entity>
      <biological_entity id="o31427" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="9" />
        <character constraint="with minor" constraintid="o31428" is_modifier="false" name="arrangement" src="d0_s2" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity id="o31428" name="minor" name_original="minor" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s2" to="15" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o31429" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than major laterals" constraintid="o31430" is_modifier="false" name="size" src="d0_s2" value="usually much larger" value_original="usually much larger" />
      </biological_entity>
      <biological_entity id="o31430" name="lateral" name_original="laterals" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–12 cm, stipules ± free, 7–23 × 3–12 mm, blade lyrate-pinnate, pinnate, 3-foliolate, or simple and 3-lobed.</text>
      <biological_entity id="o31431" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o31432" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o31433" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="23" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31434" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="lyrate-pinnate" value_original="lyrate-pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 3–16-flowered.</text>
      <biological_entity id="o31435" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-16-flowered" value_original="3-16-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels densely puberulent, sometimes with scattered longer hairs, sometimes stipitate-glandular.</text>
      <biological_entity id="o31436" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" notes="" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="longer" id="o31437" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o31436" id="r2094" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o31437" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers erect;</text>
      <biological_entity id="o31438" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets often absent, 0.5–2 mm;</text>
      <biological_entity constraint="epicalyx" id="o31439" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium green;</text>
      <biological_entity id="o31440" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect-spreading but soon reflexed, 2.5–5.5 mm;</text>
      <biological_entity id="o31441" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="soon" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals spreading, yellow, obovate, broadly elliptic, or suborbiculate, 3.5–7 mm, longer than sepals, apex rounded.</text>
      <biological_entity id="o31442" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character constraint="than sepals" constraintid="o31443" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o31443" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori sessile or on less than 1 mm stipes, puberulent.</text>
      <biological_entity id="o31445" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <biological_entity id="o31446" name="stipe" name_original="stipes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="0" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o31444" id="r2095" name="fruiting" negation="false" src="d0_s11" to="o31445" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles geniculate-jointed, proximal segment persistent, 2.5–6 mm, apex hooked, sparsely to densely stipitate-glandular, distal segment deciduous, 1–2 mm, pilose in basal 1/3, hairs much longer than diam. of style.</text>
      <biological_entity id="o31444" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o31447" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o31448" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o31449" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31450" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o31451" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character constraint="in basal 1/3" constraintid="o31452" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31452" name="1/3" name_original="1/3" src="d0_s12" type="structure" />
      <biological_entity id="o31453" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character constraint="than diam of style" constraintid="o31454" is_modifier="false" name="length_or_size" src="d0_s12" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o31454" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="diam" value_original="diam" />
      </biological_entity>
      <relation from="o31444" id="r2096" name="fruiting" negation="false" src="d0_s12" to="o31447" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Maine, Mass., Mich., Minn., Mont., N.Dak., N.H., N.Mex., N.Y., Nebr., Nev., Oreg., S.Dak., Utah, Vt., Wash., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Characters useful in recognizing specimens of Geum macrophyllum are yellow petals, epicalyx bractlets often absent, proximal style segment sparsely to densely stipitate-glandular, and fruiting receptacles puberulent. Across its broad North American range from Alaska to California and Nova Scotia, G. macrophyllum exhibits considerable variation. Based largely on the shape of the terminal leaflet of the basal leaves and the degree of dissection and shape of the divisions of the distal cauline leaves, P. A. Rydberg (1913b) distinguished three species within the range of variation treated here as one species. Basal leaves of G. macrophyllum in the strict sense (var. macrophyllum in this treatment) have relatively large reniform to rounded terminal leaflets and the distal cauline leaves are three-cleft into rhombic or cuneate lobes. The basal-leaf terminal leaflets of G. perincisum (= var. perincisum) are only slightly larger than the laterals and are deeply lobed into rhombic-obovate segments; the distal cauline leaves are dissected into oblanceolate divisions. Both basal and cauline leaves of G. oregonense (= var. perincisum) are intermediate between those of G. macrophyllum and G. perincisum. W. Gajewski (1955) crossed all three taxa under discussion and examined leaf morphology and cytology of the F1 and F2 hybrids. He concluded that they were distinct but not yet completely separated species. Fairly well correlated with the more dissected leaves of G. oregonense and G. perincisum is the presence of minute stalked glands on the pedicels. The treatment here follows H. M. Raup (1931) in recognizing two varieties of G. macrophyllum based more on pedicel glandularity than leaf morphology.</discussion>
  <references>
    <reference>Gajewski, W. 1955. Cytogenetic relations of Geum macrophyllum Willd. with G. perincisum Rydb. and G. oregonense Rydb. Acta Soc. Bot. Poloniae 24: 311–334.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels not glandular-puberulent; cauline leaves with distal simple and 3-lobed (divided less than 3/4 to base), lobes rhombic-oblong.</description>
      <determination>10a Geum macrophyllum var. macrophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels glandular-puberulent; cauline leaves with distal 3-foliolate or simple and 3-lobed (divided almost to base), lobes oblanceolate to obovate.</description>
      <determination>10b Geum macrophyllum var. perincisum</determination>
    </key_statement>
  </key>
</bio:treatment>