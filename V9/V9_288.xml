<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">191</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">189</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) Juzepczuk in V. L. Komarov et al." date="1941" rank="section">Aureae</taxon_name>
    <taxon_name authority="Ferlatte &amp; Strother" date="1990" rank="species">cristae</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>37: 190, fig. 1. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section aureae;species cristae;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100318</other_info_on_name>
  </taxon_identification>
  <number>66.</number>
  <other_name type="common_name">Crested cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted to densely matted;</text>
      <biological_entity id="o6217" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branches usually short, stout, sometimes embedded in old leaf-bases.</text>
      <biological_entity constraint="caudex" id="o6218" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o6219" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o6218" id="r376" modifier="sometimes" name="embedded in" negation="false" src="d0_s1" to="o6219" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 0.3–2 dm, lengths 2–3 times basal leaves.</text>
      <biological_entity id="o6220" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character constraint="leaf" constraintid="o6221" is_modifier="false" name="length" src="d0_s2" value="2-3 times basal leaves" value_original="2-3 times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6221" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not in ranks, ternate, 1–5 (–9) cm;</text>
      <biological_entity constraint="basal" id="o6222" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="ternate" value_original="ternate" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6223" name="rank" name_original="ranks" src="d0_s3" type="structure" />
      <relation from="o6222" id="r377" name="in" negation="false" src="d0_s3" to="o6223" />
    </statement>
    <statement id="d0_s4">
      <text>stipules: apex acute to obtuse;</text>
      <biological_entity id="o6224" name="stipule" name_original="stipules" src="d0_s4" type="structure" />
      <biological_entity id="o6225" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1–4 cm, long hairs absent or sparse to common, spreading to ascending, 0.5–2 mm, ± weak, glands sparse to abundant;</text>
      <biological_entity id="o6226" name="stipule" name_original="stipules" src="d0_s5" type="structure" />
      <biological_entity id="o6227" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6228" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="common" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="ascending" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s5" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o6229" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s5" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 3, central flabellate, 0.4–2 × 0.5–1.5 cm, petiolule 1–3 mm, margins flat, distal 3/4+ deeply 3–5-lobed (sinuses extending 1/2 to nearly to midvein), lobes unevenly incised 1/4–1/2 to midvein, teeth 2–5 per lobe, surfaces similar, dark green, hairs ± sparse, 0.5–1.5 mm, glands sparse to abundant.</text>
      <biological_entity id="o6230" name="stipule" name_original="stipules" src="d0_s6" type="structure" />
      <biological_entity id="o6231" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="central" id="o6232" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flabellate" value_original="flabellate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6233" name="petiolule" name_original="petiolule" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6234" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
        <character char_type="range_value" from="3/4" name="quantity" src="d0_s6" upper_restricted="false" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s6" value="3-5-lobed" value_original="3-5-lobed" />
      </biological_entity>
      <biological_entity id="o6235" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="unevenly" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o6236" from="1/4" name="quantity" src="d0_s6" to="1/2" />
      </biological_entity>
      <biological_entity id="o6236" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o6237" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per lobe" constraintid="o6238" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o6238" name="lobe" name_original="lobe" src="d0_s6" type="structure" />
      <biological_entity id="o6239" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark green" value_original="dark green" />
      </biological_entity>
      <biological_entity id="o6240" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="count_or_density" src="d0_s6" value="sparse" value_original="sparse" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6241" name="gland" name_original="glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s6" to="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 1–7-flowered.</text>
      <biological_entity id="o6242" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-7-flowered" value_original="1-7-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels straight, 0.2–1.2 (–2) cm, not much longer in fruit than in flower.</text>
      <biological_entity id="o6243" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s8" to="1.2" to_unit="cm" />
        <character is_modifier="false" modifier="not much" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets broadly ovate, 2–4 × 2 mm, margins flat;</text>
      <biological_entity id="o6244" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o6245" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6246" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium 3.5–6 mm diam.;</text>
      <biological_entity id="o6247" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6248" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 3–5 mm, apex broadly acute to obtuse;</text>
      <biological_entity id="o6249" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6250" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6251" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s11" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals yellow, 3–5.5 × 4–5 mm;</text>
      <biological_entity id="o6252" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6253" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments 0.7–1.7 mm, anthers 0.5–0.6 mm;</text>
      <biological_entity id="o6254" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6255" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6256" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 25–40, styles tapered-filiform, ± papillate-swollen in proximal 1/5–/1/3, 1.3–2 mm.</text>
      <biological_entity id="o6257" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o6258" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s14" to="40" />
      </biological_entity>
      <biological_entity id="o6259" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="tapered-filiform" value_original="tapered-filiform" />
        <character constraint="in proximal 1/5-/1/3 , 1.3-2 mm" is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes 1–1.5 mm, dorsally crested.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 42.</text>
      <biological_entity id="o6260" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s15" value="crested" value_original="crested" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6261" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, open, serpentine slopes, in conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" modifier="rocky" constraint="in conifer woodlands" />
        <character name="habitat" value="serpentine slopes" constraint="in conifer woodlands" />
        <character name="habitat" value="conifer woodlands" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Potentilla cristae is known only from Cory Peak, Mount Eddy, and the Marble Mountains in northwestern California. In addition to the characteristics in the key, the species is distinctive in having a low dorsal crest on the achenes, hence the specific epithet.</discussion>
  
</bio:treatment>