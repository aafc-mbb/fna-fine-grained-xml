<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="illustration_page">285</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="1898" rank="species">fissa</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. N. Amer. Potentilleae,</publication_title>
      <place_in_publication>197. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species fissa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100213</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">fissa</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 446. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species fissa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Leafy drymocallis or wood beauty</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Caudex branches short-to-elongate.</text>
      <biological_entity constraint="caudex" id="o17009" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="short" name="size" src="d0_s0" to="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems tufted to loosely spaced, (1.2–) 1.5–3.5 (–4.5) dm;</text>
      <biological_entity id="o17010" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="tufted" name="arrangement" src="d0_s1" to="loosely spaced" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="4.5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="3.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>base (1.5–) 2–3 mm diam., ± densely septate-glandular.</text>
      <biological_entity id="o17011" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s2" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="more or less densely" name="architecture_or_function_or_pubescence" src="d0_s2" value="septate-glandular" value_original="septate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sparsely to moderately hairy;</text>
      <biological_entity id="o17012" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal (3–) 7–19 cm, leaflet pairs (4–) 5–6 (–10; additional reduced leaflets sometimes interspersed);</text>
      <biological_entity constraint="basal" id="o17013" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="19" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17014" name="leaflet" name_original="leaflet" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>terminal leaflet usually broadly obovate-cuneate, sometimes elliptic, (1–) 1.5–3.5 (–5) × (1–) 1.5–3 (–3.5) cm, teeth single or double, 5–13 per side, apex rounded to obtuse;</text>
      <biological_entity constraint="terminal" id="o17015" name="leaflet" name_original="leaflet" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually broadly" name="shape" src="d0_s5" value="obovate-cuneate" value_original="obovate-cuneate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_width" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17016" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character name="quantity" src="d0_s5" value="double" value_original="double" />
        <character char_type="range_value" constraint="per side" constraintid="o17017" from="5" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o17017" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>cauline 1–3, well developed, leaflet pairs 4–6 (–10).</text>
      <biological_entity id="o17018" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="position" src="d0_s6" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o17019" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s6" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o17020" name="leaflet" name_original="leaflet" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences 5–15-flowered, leafy, congested to ± open, 1/6–1/2 of stem, narrow to wide, branch angles 15–30 (–40) °.</text>
      <biological_entity id="o17021" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-15-flowered" value_original="5-15-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="congested" name="architecture" src="d0_s7" to="more or less open" />
        <character char_type="range_value" constraint="of stem" constraintid="o17022" from="1/6" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity id="o17022" name="stem" name_original="stem" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrow" name="width" notes="" src="d0_s7" to="wide" />
      </biological_entity>
      <biological_entity constraint="branch" id="o17023" name="angle" name_original="angles" src="d0_s7" type="structure">
        <character name="degree" src="d0_s7" value="15-30(-40)°" value_original="15-30(-40)°" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 1–12 mm, short-hairy, septate-glandular.</text>
      <biological_entity id="o17024" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="short-hairy" value_original="short-hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="septate-glandular" value_original="septate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers opening widely;</text>
      <biological_entity id="o17025" name="flower" name_original="flowers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>epicalyx bractlets linear-oblanceolate, 3–7 × 1–2 mm;</text>
      <biological_entity constraint="epicalyx" id="o17026" name="bractlet" name_original="bractlets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals spreading, 6–10 mm, apex acute to acuminate;</text>
      <biological_entity id="o17027" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17028" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals overlapping, spreading, yellow, broadly obovate, 7–11 × 5–11 mm, equal to or exceeding sepals;</text>
      <biological_entity id="o17029" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17030" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
      <relation from="o17029" id="r1089" name="exceeding" negation="false" src="d0_s12" to="o17030" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.5–4.5 mm, anthers (0.7–) 1–1.4 mm;</text>
      <biological_entity id="o17031" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17032" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles thickened, 1 mm.</text>
      <biological_entity id="o17033" name="style" name_original="styles" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="thickened" value_original="thickened" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes light-brown, 1 mm.</text>
      <biological_entity id="o17034" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="light-brown" value_original="light-brown" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush slopes, open forests, stream banks, often in rocky or moderately disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush slopes" />
        <character name="habitat" value="open forests" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="disturbed sites" modifier="moderately" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Drymocallis fissa is distinctive in its relatively numerous leaflets (often with additional smaller ones), large flowers, and large, elongate anthers. It is most abundant in the Colorado Front Range, extending into the Medicine Bow Mountains of Wyoming. Outlying populations occur at least as far north as the Bighorn Mountains of northern Wyoming and the Black Hills of South Dakota. Tentatively included here are large-anthered populations from the eastern Uintah Mountains of Utah, though these often have fewer leaflets and smaller flowers of unknown color; they may represent a unique taxon worthy of separate recognition. Possible collections of D. fissa from New Mexico, including the type of Potentilla fissa var. major Torrey &amp; A. Gray, are of uncertain placement in that they combine features of D. arguta, D. arizonica, and D. fissa. Reports from other states, including Montana, are probably all based on misidentified specimens.</discussion>
  
</bio:treatment>