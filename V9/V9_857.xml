<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">509</other_info_on_meta>
    <other_info_on_meta type="mention_page">500</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="mention_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="unknown" rank="section">Douglasia</taxon_name>
    <taxon_name authority="unknown" date="1999" rank="series">Cerrones</taxon_name>
    <taxon_name authority="Greene" date="1896" rank="species">saligna</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 99. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section douglasia;series cerrones;species saligna;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100162</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crataegus</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">douglasii</taxon_name>
    <taxon_name authority="S. L. Welsh" date="unknown" rank="variety">duchesnensis</taxon_name>
    <taxon_hierarchy>genus Crataegus;species douglasii;variety duchesnensis</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <other_name type="common_name">Willow hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–50 dm, often clonal.</text>
      <biological_entity id="o4104" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="50" to_unit="dm" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: twigs: new growth green, glabrous, 1-year old reddish tan, older deep gray;</text>
      <biological_entity id="o4105" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o4106" name="twig" name_original="twigs" src="d0_s1" type="structure" />
      <biological_entity id="o4107" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="new" value_original="new" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish tan" value_original="reddish tan" />
        <character is_modifier="false" name="life_cycle" src="d0_s1" value="older" value_original="older" />
        <character is_modifier="false" name="depth" src="d0_s1" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark on younger 2–5 cm thick branches mainly dark (with horizontal lenticels);</text>
      <biological_entity id="o4108" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o4109" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character constraint="on" is_modifier="false" name="life_cycle" src="d0_s2" value="younger" value_original="younger" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4110" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="mainly" name="coloration" src="d0_s2" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs straight (often needlelike) or recurved, 2-years old black, shiny, ± fine, 1–3 cm (sometimes bigger and stouter).</text>
      <biological_entity id="o4111" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o4112" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="black" value_original="black" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="more or less" name="width" src="d0_s3" value="fine" value_original="fine" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4113" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o4112" id="r254" name="on" negation="false" src="d0_s3" to="o4113" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: petiole 0.8–1.8 cm;</text>
      <biological_entity id="o4114" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4115" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s4" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly elliptic to bluntly elliptic-lanceolate, 3–5 cm, ± coriaceous, base narrowly cuneate, lobes 0, margins crenate, teeth numerous, small, obtuse, venation semicamptodromous, veins 6–9 (–12) per side, apex usually obtuse, sometimes acute, abaxial surface glabrous, adaxial appressed-pubescent young, glabrescent.</text>
      <biological_entity id="o4116" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4117" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s5" to="bluntly elliptic-lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o4118" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4119" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4120" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o4121" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4122" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="12" />
        <character char_type="range_value" constraint="per side" constraintid="o4123" from="6" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o4123" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o4124" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4125" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4126" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences 12-flowered;</text>
      <biological_entity id="o4127" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="12-flowered" value_original="12-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches not glandular-punctate;</text>
      <biological_entity id="o4128" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="coloration_or_relief" src="d0_s7" value="glandular-punctate" value_original="glandular-punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles early caducous, few, margins eglandular or sessile-glandular.</text>
      <biological_entity id="o4129" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o4130" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 10–13 mm diam.;</text>
      <biological_entity id="o4131" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals proximally broadly triangular with short, narrower limbs, 1.5–2 mm, margins not recorded, apex acute, pubescence not recorded;</text>
      <biological_entity id="o4132" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character constraint="with narrower limbs" constraintid="o4133" is_modifier="false" modifier="proximally broadly" name="shape" src="d0_s10" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="narrower" id="o4133" name="limb" name_original="limbs" src="d0_s10" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o4134" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o4135" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 20, anthers cream;</text>
      <biological_entity id="o4136" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o4137" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>styles 5.</text>
      <biological_entity id="o4138" name="style" name_original="styles" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pomes black mature, orbicular, 8 mm diam.;</text>
      <biological_entity id="o4139" name="pome" name_original="pomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="black" value_original="black" />
        <character is_modifier="false" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
        <character is_modifier="false" name="shape" src="d0_s13" value="orbicular" value_original="orbicular" />
        <character name="diameter" src="d0_s13" unit="mm" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals suberect, 1.5–2 mm, tips reflexed;</text>
      <biological_entity id="o4140" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="suberect" value_original="suberect" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4141" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pyrenes 3–5, sides irregular.</text>
      <biological_entity id="o4142" name="pyrene" name_original="pyrenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s15" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 34, 51.</text>
      <biological_entity id="o4143" name="side" name_original="sides" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s15" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4144" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="34" value_original="34" />
        <character name="quantity" src="d0_s16" value="51" value_original="51" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun; fruiting Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Along streams, flood plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="flood plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Crataegus saligna is restricted to intermontane areas of northeastern Utah and adjacent Colorado; it may be locally plentiful and the plants flower early. The species may be confused with C. rivularis; it differs by its smaller flowers, fruit, and foliage, 20 stamens, cream anthers, and leaves relatively narrower, more venous, and with smaller, more obtuse teeth. It is more similar in flower and foliage to C. brachyacantha but has very different thorns and bark color, as well as irregular lateral faces of pyrenes.</discussion>
  <discussion>Crataegus saligna is a striking plant with its suckering habit, its glowing copper-tan bark, long arching twigs and branches, dark green foliage, narrow and more or less lanceolate leaves, often beautifully reddish bronze foliage in fall, and black fruit.</discussion>
  
</bio:treatment>