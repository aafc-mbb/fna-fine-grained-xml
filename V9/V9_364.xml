<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">220</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">IVESIA</taxon_name>
    <taxon_name authority="Ertter" date="1989" rank="species">longibracteata</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>14: 233, fig. 1. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section ivesia;species longibracteata;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100265</other_info_on_name>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Castle Crags ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green, ± tufted;</text>
      <biological_entity id="o2221" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot ± stout, not fleshy.</text>
      <biological_entity id="o2222" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 0.3–1.2 dm.</text>
      <biological_entity id="o2223" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="1.2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves weakly planar to loosely ± cylindric, (0.5–) 2–4 (–6) cm;</text>
      <biological_entity constraint="basal" id="o2224" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="weakly planar" name="shape" src="d0_s3" to="loosely more or less cylindric" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheathing base glandular abaxially, otherwise glabrous;</text>
      <biological_entity id="o2225" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="abaxially" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–2 cm, hairs 0.2–1 mm;</text>
      <biological_entity id="o2226" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2227" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 5–6 per side, 2–6 mm, ± short-hirsute, glandular-puberulent, lobes 2–7, oblanceolate to spatulate or obovate, apex not setose.</text>
      <biological_entity id="o2228" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o2229" from="5" name="quantity" src="d0_s6" to="6" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s6" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="short-hirsute" value_original="short-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o2229" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o2230" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="7" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="spatulate or obovate" />
      </biological_entity>
      <biological_entity id="o2231" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="setose" value_original="setose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 1–3, not paired.</text>
      <biological_entity constraint="cauline" id="o2232" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 3–14-flowered, 1–2.5 cm diam.;</text>
      <biological_entity id="o2233" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-14-flowered" value_original="3-14-flowered" />
        <character char_type="range_value" from="1" from_unit="cm" name="diameter" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>glomerules 1.</text>
      <biological_entity id="o2234" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1.5–6 mm.</text>
      <biological_entity id="o2235" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 8–10 mm diam.;</text>
      <biological_entity id="o2236" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets linear to narrowly lanceolate or elliptic, 2.5–5 mm, longer than sepals;</text>
      <biological_entity constraint="epicalyx" id="o2237" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="narrowly lanceolate or elliptic" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character constraint="than sepals" constraintid="o2238" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2238" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>hypanthium shallowly cupulate, 0.5–1 × 2–3 mm;</text>
      <biological_entity id="o2239" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals 1.5–2.5 mm, ± acute;</text>
      <biological_entity id="o2240" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals pale-yellow, linear to narrowly oblanceolate, 1.5–2.5 mm;</text>
      <biological_entity id="o2241" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s15" to="narrowly oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 5, filaments 0.7–1.3 mm, anthers yellow, 0.5–0.8 mm;</text>
      <biological_entity id="o2242" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o2243" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2244" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels 6–11, styles 1–1.5 mm.</text>
      <biological_entity id="o2245" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s17" to="11" />
      </biological_entity>
      <biological_entity id="o2246" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes cream to light tan, 1.2–1.5 mm.</text>
      <biological_entity id="o2247" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="cream" name="coloration" src="d0_s18" to="light tan" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry crevices of granodioritic igneous rock, in mixed oak-conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry crevices" constraint="of granodioritic igneous rock" />
        <character name="habitat" value="granodioritic igneous rock" />
        <character name="habitat" value="mixed oak-conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia longibracteata is known only from the Castle Crags area of Shasta County. The epithet alludes to a diagnostic characteristic unique in the genus: the epicalyx bractlets are longer than the sepals. The plants grow on vertical rock faces, a habitat more characteristic of sect. Setosae; however, the stems are ascending to erect and do not form hanging clumps, and the pedicels are not curved in fruit.</discussion>
  
</bio:treatment>