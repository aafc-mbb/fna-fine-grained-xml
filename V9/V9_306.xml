<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">197</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Niveae</taxon_name>
    <taxon_name authority="Jurtzev in A. I. Tolmatchew" date="1984" rank="species">villosula</taxon_name>
    <place_of_publication>
      <publication_title>in A. I. Tolmatchew, Fl. Arct. URSS</publication_title>
      <place_in_publication>9(1): 319. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section niveae;species villosula;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100379</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">villosula</taxon_name>
    <taxon_name authority="Jurtzev" date="unknown" rank="subspecies">congesta</taxon_name>
    <taxon_hierarchy>genus Potentilla;species villosula;subspecies congesta</taxon_hierarchy>
  </taxon_identification>
  <number>78.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely tufted.</text>
      <biological_entity id="o1818" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudex branches stout, sometimes ± columnar, not sheathed with marcescent whole leaves.</text>
      <biological_entity constraint="caudex" id="o1819" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s1" value="columnar" value_original="columnar" />
        <character constraint="with leaves" constraintid="o1820" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="sheathed" value_original="sheathed" />
      </biological_entity>
      <biological_entity id="o1820" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to erect, 0.2–1.5 (–2) dm, lengths 1.5–3 (–4) times basal leaves.</text>
      <biological_entity id="o1821" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s2" to="1.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o1822" is_modifier="false" name="length" src="d0_s2" value="1.5-3(-4) times basal leaves" value_original="1.5-3(-4) times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1822" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 1–5.5 (–7) cm;</text>
      <biological_entity constraint="basal" id="o1823" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–3.5 (–5) cm, long hairs common to dense, ascending to spreading, loosely appressed, sometimes retrorse, 1–2 (–3) mm, soft, smooth, crisped/short-cottony hairs usually sparse, sometimes common, glands absent or sparse to common or obscured;</text>
      <biological_entity id="o1824" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1825" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s4" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="common" value_original="common" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="spreading" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="retrorse" value_original="retrorse" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1826" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="count_or_density" src="d0_s4" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s4" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o1827" name="gland" name_original="glands" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="common" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaflets overlapping, central broadly obovate to obtriangular, 0.8–2.5 × 0.6–2 cm, sessile to subsessile, base broadly cuneate, margins revolute, distal 1/2–2/3 (–3/4) incised ± 1/2 to midvein, teeth 2–3 (–4) per side, ± approximate to ± distant, surfaces ± dissimilar, abaxial grayish to white or yellowish white, long hairs 1.5–2.5 mm, cottony-crisped hairs ± dense, adaxial grayish green, long hairs abundant to dense, crisped hairs absent, sparse, or obscured.</text>
      <biological_entity id="o1828" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="central" id="o1829" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s5" to="obtriangular" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s5" to="subsessile" />
      </biological_entity>
      <biological_entity id="o1830" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o1831" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s5" to="2/3-3/4" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="incised" value_original="incised" />
        <character constraint="to midvein" constraintid="o1832" name="quantity" src="d0_s5" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1832" name="midvein" name_original="midvein" src="d0_s5" type="structure" />
      <biological_entity id="o1833" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="4" />
        <character char_type="range_value" constraint="per side" constraintid="o1834" from="2" name="quantity" src="d0_s5" to="3" />
        <character char_type="range_value" from="less approximate" name="arrangement" notes="" src="d0_s5" to="more or less distant" />
      </biological_entity>
      <biological_entity id="o1834" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o1835" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="abaxial" id="o1836" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character char_type="range_value" from="grayish" name="coloration" src="d0_s5" to="white or yellowish white" />
      </biological_entity>
      <biological_entity id="o1837" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1838" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="cottony-crisped" value_original="cottony-crisped" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1839" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish green" value_original="grayish green" />
      </biological_entity>
      <biological_entity id="o1840" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="density" src="d0_s5" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o1841" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (0–) 1–2 (–3).</text>
      <biological_entity constraint="cauline" id="o1842" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences (1–) 2–3 (–4) -flowered.</text>
      <biological_entity id="o1843" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="(1-)2-3(-4)-flowered" value_original="(1-)2-3(-4)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.5–3 (–5) cm in flower, to 4 (–6) cm in fruit.</text>
      <biological_entity id="o1844" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" constraint="in flower" constraintid="o1845" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o1846" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1845" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o1846" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: epicalyx bractlets broadly lanceolate to narrowly ovate, 3–7 (–8) × 1.5–3 (–3.5) mm, 2/3 to as wide as sepals, margins ± revolute, red-glands absent;</text>
      <biological_entity id="o1847" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity constraint="epicalyx" id="o1848" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s9" to="narrowly ovate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
        <character constraint="to sepals" constraintid="o1849" name="quantity" src="d0_s9" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o1849" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o1850" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s9" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o1851" name="red-gland" name_original="red-glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>hypanthium (3–) 4–6 mm diam.;</text>
      <biological_entity id="o1852" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1853" name="hypanthium" name_original="hypanthium" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals 4–7 (–8) mm, apex acute or rarely acuminate;</text>
      <biological_entity id="o1854" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1855" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1856" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals 5–10 × 6–12 mm, significantly longer than sepals;</text>
      <biological_entity id="o1857" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1858" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s12" to="12" to_unit="mm" />
        <character constraint="than sepals" constraintid="o1859" is_modifier="false" name="length_or_size" src="d0_s12" value="significantly longer" value_original="significantly longer" />
      </biological_entity>
      <biological_entity id="o1859" name="sepal" name_original="sepals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 1.1–1.4 mm, anthers 0.5–0.8 mm;</text>
      <biological_entity id="o1860" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1861" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1862" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>carpels 40–70, apical hairs absent or sparse (straight), styles narrowly columnar to conic-tapered, papillate-swollen in proximal 1/5 or less, 1–1.2 mm.</text>
      <biological_entity id="o1863" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o1864" name="carpel" name_original="carpels" src="d0_s14" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s14" to="70" />
      </biological_entity>
      <biological_entity constraint="apical" id="o1865" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s14" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o1866" name="style" name_original="styles" src="d0_s14" type="structure">
        <character char_type="range_value" from="narrowly columnar" name="shape" src="d0_s14" to="conic-tapered" />
        <character constraint="in proximal 1/5" constraintid="o1867" is_modifier="false" name="shape" src="d0_s14" value="papillate-swollen" value_original="papillate-swollen" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="less" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1867" name="1/5" name_original="1/5" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Achenes 0.9–2 mm. 2n = 28 (Russian Far East).</text>
      <biological_entity id="o1868" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1869" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring to summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky alpine heaths, outcrops, scree and talus, gravel outwash plains, dry tundra, coastal bluffs, stabilized sand dunes, mostly on acidic bedrock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky alpine heaths" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="scree" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="gravel outwash plains" />
        <character name="habitat" value="dry tundra" />
        <character name="habitat" value="coastal bluffs" />
        <character name="habitat" value="sand dunes" modifier="stabilized" />
        <character name="habitat" value="acidic bedrock" modifier="mostly on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Yukon; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla villosula was included previously in P. villosa; the two overlap in the southern and western Alaskan coasts, where a transition is suspected to be associated with differing ploidy levels. The plants from this area with silky hairs and multiflowered inflorescences are assigned to P. villosa; P. villosula has thicker, stiffer, and less silky hairs, fewer teeth per leaflet, narrower bracts, fewer and smaller flowers with narrower epicalyx bractlets and sepals, and fewer achenes (sometimes with single apical hairs). While P. villosa is restricted to relatively rocky sites near the coast from western Alaska to Oregon, P. villosula often occurs on coastal sand dunes, farther inland, and/or farther to the north.</discussion>
  <discussion>As circumscribed, Potentilla villosula is a major plant of Alaska, especially of the Bering Sea region. The species extends from Alaska and the Yukon to at least central British Columbia. Most of the range given by E. Hultén (1968) for P. villosa belongs to P. villosula. W. J. Cody (1996) accepted the name P. villosula for a common south-central Yukon plant, but the majority of the plants mapped by Cody probably belong to P. subgorodkovii (although the distinction between the two species needs more resolution). Provisionally included here are plants from southern and central Yukon south through the Canadian Rockies, possibly including the type of P. nivea subsp. fallax A. E. Porsild. Such plants tend to be significantly smaller overall than Alaskan P. villosula; at least some have straight hairs on carpel apices.</discussion>
  <discussion>J. Soják (2004) believed that Potentilla villosula evolved from crosses between P. villosa and P. vulcanicola. The affinity with P. villosa is seen in the sericeous vestiture, number of leaflet teeth, and flower number and size, and with P. vulcanicola in the frequent occurrence of straight hairs on carpel apices in both species. Plants with columnar caudex branches, representing a significant part of the Alaskan material, have been called subsp. congesta. This morphology results from the persistence of marcescent whole leaves for several years (in typical P. villosula only sheaths and petioles are retained). This character suggests that P. subvahliana, with which the form called subsp. congesta is sympatric, may be part of its parentage. Potentilla villosula is accepted here in a fairly collective sense, possibly including several hybrid lineages, but with P. villosa a part of them all.</discussion>
  
</bio:treatment>