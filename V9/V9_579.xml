<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">351</other_info_on_meta>
    <other_info_on_meta type="illustration_page">352</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">neillieae</taxon_name>
    <taxon_name authority="D. Don" date="unknown" rank="genus">neillia</taxon_name>
    <taxon_name authority="(Thunberg) S. H. Oh" date="2006" rank="species">incisa</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>16: 92. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe neillieae;genus neillia;species incisa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100287</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Thunberg" date="unknown" rank="species">incisa</taxon_name>
    <place_of_publication>
      <publication_title>in J. A. Murray, Syst. Veg. ed.</publication_title>
      <place_in_publication>14, 472. 1784</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species incisa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stephanandra</taxon_name>
    <taxon_name authority="(Thunberg) Siebold &amp; Zuccarini ex Zabel" date="unknown" rank="species">incisa</taxon_name>
    <taxon_hierarchy>genus Stephanandra;species incisa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Lace shrub</other_name>
  <other_name type="common_name">cutleaf stephanandra</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: stipules prominent.</text>
      <biological_entity id="o22750" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o22751" name="stipule" name_original="stipules" src="d0_s0" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s0" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Inflorescences: peduncles glabrous to densely pubescent.</text>
      <biological_entity id="o22752" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <biological_entity id="o22753" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s1" to="densely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pedicels 5–8 mm, pubescent.</text>
      <biological_entity id="o22754" name="pedicel" name_original="pedicels" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals distinct or basally connate.</text>
      <biological_entity id="o22755" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o22756" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Follicles pilose.</text>
      <biological_entity id="o22757" name="follicle" name_original="follicles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seeds ovoid, shiny.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 18 (Asia).</text>
      <biological_entity id="o22758" name="seed" name_original="seeds" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22759" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May; fruiting Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="May" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist forests, forest edges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist forests" />
        <character name="habitat" value="forest edges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Va.; Asia (Japan, Korea, Taiwan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Taiwan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Neillia incisa is commonly cultivated and is promoted in the nursery trade as an attractive, deer-resistant, and easy-to-grow ornamental shrub. The establishment and spread of N. incisa in Richmond, Virginia, shows its ability to naturalize and occupy shaded sites in moist temperate forests of eastern North America. Field observations indicate local spread by proliferous tip sprouting. Despite the paucity of documented occurrences of its naturalization, it can be expected to naturalize more broadly in eastern North America and may prove to be locally invasive. 'Crispa' is one of the more popular cultivars and is shorter in stature (less than 1 m) than the rest of the species.</discussion>
  
</bio:treatment>