<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">114</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="illustration_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="Lindley" date="1820" rank="species">acicularis</taxon_name>
    <place_of_publication>
      <publication_title>Ros. Monogr.,</publication_title>
      <place_in_publication>44, plate 8. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species acicularis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200011211</other_info_on_name>
  </taxon_identification>
  <number>29.</number>
  <other_name type="common_name">Acicular rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, forming dense thickets.</text>
      <biological_entity id="o15913" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o15914" name="thicket" name_original="thickets" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o15913" id="r1018" name="forming" negation="false" src="d0_s0" to="o15914" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, stout, (3–) 10–20 (–25) dm, sparsely or densely branched distally;</text>
      <biological_entity id="o15915" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character char_type="range_value" from="3" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="25" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s1" to="20" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely; densely; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark pale-brown with tips dull red, glabrous;</text>
      <biological_entity id="o15917" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s2" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>infrastipular prickles absent, internodal prickles dense, erect, subulate, terete, ± flattened, 9 × 4 mm, sparsely stipitate-glandular, mixed with dense aciculi.</text>
      <biological_entity id="o15916" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character constraint="with tips" constraintid="o15917" is_modifier="false" name="coloration" src="d0_s2" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="red" value_original="red" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15918" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o15919" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character name="length" src="d0_s3" unit="mm" value="9" value_original="9" />
        <character name="width" src="d0_s3" unit="mm" value="4" value_original="4" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="with aciculi" constraintid="o15920" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o15920" name="aciculus" name_original="aciculi" src="d0_s3" type="structure">
        <character is_modifier="true" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 5.5–15 cm;</text>
      <biological_entity id="o15921" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules 20–25 × 3–8 mm, auricles flared or erect, 5–7 mm, margins usually entire, stipitate-glandular, surfaces glabrous, sometimes puberulent, sparsely sessile-glandular or eglandular;</text>
      <biological_entity id="o15922" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15923" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flared" value_original="flared" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15924" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o15925" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and rachis usually (rarely) with pricklets, glabrous or puberulent to pubescent, sessile-glandular;</text>
      <biological_entity id="o15926" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" notes="" src="d0_s6" to="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o15927" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" notes="" src="d0_s6" to="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o15928" name="pricklet" name_original="pricklets" src="d0_s6" type="structure" />
      <relation from="o15926" id="r1019" name="with" negation="false" src="d0_s6" to="o15928" />
      <relation from="o15927" id="r1020" name="with" negation="false" src="d0_s6" to="o15928" />
    </statement>
    <statement id="d0_s7">
      <text>leaflets 5–7 (on annual shoots), terminal: petiolule 6–20 mm, blade elliptic, ovoid, or ovatelanceolate, 20–60 × 13–32 mm, firm, margins 1 (–2+) -dentate-serrate, teeth 11–25 per side acute or obtuse, gland-tipped or eglandular, apex acute or obtuse, abaxial surfaces light green, glabrous or puberulent (on main veins), eglandular or sparsely to densely sessile-glandular, adaxial green, dull, glabrous or sparsely hairy.</text>
      <biological_entity id="o15929" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o15930" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15931" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s7" to="60" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s7" to="32" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o15932" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="1(-2+)-dentate-serrate" value_original="1(-2+)-dentate-serrate" />
      </biological_entity>
      <biological_entity id="o15933" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o15934" from="11" name="quantity" src="d0_s7" to="25" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o15934" name="side" name_original="side" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o15935" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15936" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="light green" value_original="light green" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="eglandular or" name="architecture" src="d0_s7" to="sparsely densely sessile-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15937" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences corymbs, 1 or 2 (or 3) -flowered.</text>
      <biological_entity constraint="inflorescences" id="o15938" name="corymb" name_original="corymbs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-flowered" value_original="2-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels reflexed as hips mature, slender, (13–) 20–28 (–35) mm, glabrous, stipitate-glandular or eglandular;</text>
      <biological_entity id="o15939" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character constraint="as hips" constraintid="o15940" is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="28" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="28" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o15940" name="hip" name_original="hips" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 1–3, ovoid, 18–22 × 4–14 mm, margins entire, sessile or short-stipitate-glandular, surfaces glabrous, sometimes hairy, eglandular.</text>
      <biological_entity id="o15941" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15942" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o15943" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 3–6 cm diam.;</text>
      <biological_entity id="o15944" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="diameter" src="d0_s11" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium ovoid to oblong, 5–8 × 4–6 mm, glabrous, eglandular, neck 1 × 2.5–3 mm;</text>
      <biological_entity id="o15945" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s12" to="oblong" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o15946" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals spreading, often beak-capped, lanceolate, 20–33 × (2.5–) 3–3.5 mm, tip 7–12 × 0.5–0.8 mm, margins entire, sometimes pinnatifid, abaxial surfaces glabrous, sometimes pubescent, eglandular or stipitate-glandular (north);</text>
      <biological_entity id="o15947" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s13" value="beak-capped" value_original="beak-capped" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s13" to="33" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s13" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15948" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s13" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15949" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s13" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15950" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, rose-pink to pale-pink, (13–) 22–25 × (11–) 20–25 mm;</text>
      <biological_entity id="o15951" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character char_type="range_value" from="rose-pink" name="coloration" src="d0_s14" to="pale-pink" />
        <character char_type="range_value" from="13" from_unit="mm" name="atypical_length" src="d0_s14" to="22" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_unit="mm" name="length" src="d0_s14" to="25" to_unit="mm" />
        <character char_type="range_value" from="11" from_unit="mm" name="atypical_width" src="d0_s14" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s14" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens 75–100;</text>
      <biological_entity id="o15952" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s15" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>carpels 18–33, styles exsert 1 mm beyond stylar orifice (2 mm diam.) of ± flat hypanthial disc (3.5–4.5 mm diam.).</text>
      <biological_entity id="o15953" name="carpel" name_original="carpels" src="d0_s16" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s16" to="33" />
      </biological_entity>
      <biological_entity id="o15954" name="style" name_original="styles" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="exsert" value_original="exsert" />
        <character constraint="beyond stylar, orifice" constraintid="o15955, o15956" name="some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o15955" name="stylar" name_original="stylar" src="d0_s16" type="structure" />
      <biological_entity id="o15956" name="orifice" name_original="orifice" src="d0_s16" type="structure" />
      <biological_entity id="o15957" name="hypanthial" name_original="hypanthial" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o15958" name="disc" name_original="disc" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence_or_shape" src="d0_s16" value="flat" value_original="flat" />
      </biological_entity>
      <relation from="o15955" id="r1021" name="part_of" negation="false" src="d0_s16" to="o15957" />
      <relation from="o15955" id="r1022" name="part_of" negation="false" src="d0_s16" to="o15958" />
      <relation from="o15956" id="r1023" name="part_of" negation="false" src="d0_s16" to="o15957" />
      <relation from="o15956" id="r1024" name="part_of" negation="false" src="d0_s16" to="o15958" />
    </statement>
    <statement id="d0_s17">
      <text>Hips orange-red to bright red or blue-purple, globose to ellipsoid or urceolate, 10–23 × 9–11 mm, fleshy, glabrous, eglandular, neck 1–2 × 2–2.5 mm;</text>
      <biological_entity id="o15959" name="hip" name_original="hips" src="d0_s17" type="structure">
        <character char_type="range_value" from="orange-red" name="coloration" src="d0_s17" to="bright red or blue-purple" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s17" to="ellipsoid or urceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s17" to="23" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s17" to="11" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o15960" name="neck" name_original="neck" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s17" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals persistent, erect, often beak-capped.</text>
      <biological_entity id="o15961" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s18" value="beak-capped" value_original="beak-capped" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Achenes basiparietal, 14–25, tan, 4 × 2–2.5 mm.</text>
      <biological_entity id="o15962" name="achene" name_original="achenes" src="d0_s19" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s19" to="25" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="tan" value_original="tan" />
        <character name="length" src="d0_s19" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s19" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Colo., Iowa, Maine, Mass., Mich., Minn., Mont., N.Dak., N.H., N.Y., S.Dak., Vt., W.Va., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Rosa acicularis is circumpolar in forests of Eurasia and North America. Octoploid subsp. acicularis occurs from Sweden across northern Russia to Outer Mongolia, northern China, Korea, and Japan; hexaploid subsp. sayi is found throughout northern North America. The two subspecies meet in an intergradation zone from eastern Siberia to Alaska, and perhaps Yukon (W. H. Lewis 1958), where ploidy level is required for subspecies or hybrid confirmation.</discussion>
  <references>
    <reference>Lewis, W. H. 1958. A monograph of the genus Rosa in North America. I. R. accicularis. Brittonia 11: 1–24.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels densely stipitate-glandular; leaflets 5 (or 7), margins 1(–2+)-dentate-serrate; sepal abaxial surfaces usually stipitate-glandular.</description>
      <determination>29a Rosa acicularis subsp. acicularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pedicels usually eglandular, if stipitate-glandular, not to apex or stipitate glands mostly sparse; leaflets 5–7, margins 1–2-dentate-serrate; sepal abaxial surfaces usually eglandular.</description>
      <determination>29b Rosa acicularis subsp. sayi</determination>
    </key_statement>
  </key>
</bio:treatment>