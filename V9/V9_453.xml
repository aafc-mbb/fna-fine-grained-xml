<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">280</other_info_on_meta>
    <other_info_on_meta type="illustration_page">273</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Bunge in C. F. von Ledebour" date="1829" rank="genus">chamaerhodos</taxon_name>
    <taxon_name authority="(Linnaeus) Bunge in C. F. von Ledebour" date="1829" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. von Ledebour, Fl. Altaica</publication_title>
      <place_in_publication>1: 430. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus chamaerhodos;species erecta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200010702</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sibbaldia</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">erecta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 284. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sibbaldia;species erecta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamaerhodos</taxon_name>
    <taxon_name authority="Pickering ex Rydberg" date="unknown" rank="species">erecta</taxon_name>
    <taxon_name authority="(Pickering ex Rydberg) Hultén" date="unknown" rank="subspecies">nuttallii</taxon_name>
    <taxon_hierarchy>genus Chamaerhodos;species erecta;subspecies nuttallii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">erecta</taxon_name>
    <taxon_name authority="(Nuttall) C. L. Hitchcock" date="unknown" rank="variety">parviflora</taxon_name>
    <taxon_hierarchy>genus C.;species erecta;variety parviflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">C.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_hierarchy>genus C.;species nuttallii</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal petiole to 30 mm;</text>
      <biological_entity id="o4091" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o4092" name="petiole" name_original="petiole" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>lobes of cauline leaves 5–18 × 0.2–1 mm;</text>
      <biological_entity id="o4093" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4094" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s1" to="18" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4095" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o4094" id="r253" name="part_of" negation="false" src="d0_s1" to="o4095" />
    </statement>
    <statement id="d0_s2">
      <text>surfaces white-hairy, stipitate-glandular, hairs rarely antrorsely barbed.</text>
      <biological_entity id="o4096" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4097" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="white-hairy" value_original="white-hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4098" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely antrorsely" name="architecture_or_shape" src="d0_s2" value="barbed" value_original="barbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 1.8–2.5 mm;</text>
      <biological_entity id="o4099" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o4100" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 1.8–2.5 × 0.5–1.1 mm. 2n = 14.</text>
      <biological_entity id="o4101" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4102" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4103" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug; fruiting Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="fruiting time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, dry hilltops, knolls, outcrops, buttes, rocky and sandy soil or gravel, exposed and disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="dry hilltops" />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="buttes" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy soil" />
        <character name="habitat" value="gravel" />
        <character name="habitat" value="exposed" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Sask., Yukon; Alaska, Colo., Mich., Minn., Mont., N.Dak., S.Dak., Utah., Wyo.; Asia (Mongolia, Russian Far East, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Asia (Mongolia)" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>No clear distinction could be made among infraspecific taxa of Chamaerhodos erecta. Purportedly distinguishing characters, for example petal length relative to sepal length, vary throughout the range of the species. Stems may be in tufts, single or multiple (one North Dakota specimen exhibited leafy tufts with ten branches from the base). Most often, specimens are single-stemmed, branching distal to the middle, and producing flat-topped inflorescences.</discussion>
  
</bio:treatment>