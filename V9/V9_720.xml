<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="illustration_page">426</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Moench" date="1802" rank="genus">gillenia</taxon_name>
    <taxon_name authority="(Muhlenberg ex Willdenow) Nuttall in W. P. C. Barton" date="unknown" rank="species">stipulata</taxon_name>
    <place_of_publication>
      <publication_title>in W. P. C. Barton, Veg. Mater. Med. U.S.</publication_title>
      <place_in_publication>1: 71. 1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus gillenia;species stipulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416596</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Muhlenberge \× Willdenow" date="unknown" rank="species">stipulata</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.,</publication_title>
      <place_in_publication>542. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;species stipulata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Porteranthus</taxon_name>
    <taxon_name authority="(Muhlenberge \× Willdenow) Britton" date="unknown" rank="species">stipulatus</taxon_name>
    <taxon_hierarchy>genus Porteranthus;species stipulatus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">American ipecac</other_name>
  <other_name type="common_name">Indian physic</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: stipules persistent, depressed-ovate, 10–20 (–25) × 10–20 mm (foliaceous), margins serrate to doubly serrate;</text>
      <biological_entity id="o2511" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o2512" name="stipule" name_original="stipules" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s0" value="depressed-ovate" value_original="depressed-ovate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s0" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s0" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2513" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s0" to="doubly serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>leaflet blade ovate-oblong, those of proximal leaves laciniately lobed or dissected, abaxial surface densely sessile or stipitate-glandular, otherwise hirsute to hirsutulous or rarely glabrous, adaxial hirsutulous, eglandular or sparsely sessile-glandular.</text>
      <biological_entity id="o2514" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="leaflet" id="o2515" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" modifier="laciniately" name="shape" src="d0_s1" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o2516" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="abaxial" id="o2517" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character char_type="range_value" from="hirsute" modifier="otherwise" name="pubescence" src="d0_s1" to="hirsutulous or rarely glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2518" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <relation from="o2515" id="r155" name="part_of" negation="false" src="d0_s1" to="o2516" />
    </statement>
    <statement id="d0_s2">
      <text>Hypanthia 4.5–5.5 mm.</text>
      <biological_entity id="o2519" name="hypanthium" name_original="hypanthia" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petals 10–13 mm.</text>
      <biological_entity id="o2520" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Follicles long-beaked, slightly to prominently rugulose, glabrous or glabrate.</text>
    </statement>
    <statement id="d0_s5">
      <text>2n = 18.</text>
      <biological_entity id="o2521" name="follicle" name_original="follicles" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="long-beaked" value_original="long-beaked" />
        <character is_modifier="false" modifier="slightly to prominently" name="relief" src="d0_s4" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2522" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich woods, post oak woodlands, moist places in 2d-growth pine hills, low woods, ravines, stream valleys, prairie thickets, dry, rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich woods" />
        <character name="habitat" value="post oak woodlands" />
        <character name="habitat" value="moist places" constraint="in 2d-growth pine hills , low woods , ravines , stream valleys , prairie thickets , dry , rocky slopes" />
        <character name="habitat" value="2d-growth pine hills" />
        <character name="habitat" value="low woods" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="stream valleys" />
        <character name="habitat" value="prairie thickets" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ill., Ind., Kans., Ky., La., Md., Mich., Miss., Mo., N.Y., N.C., Ohio, Okla., Pa., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">stipulacea</other_name>
  
</bio:treatment>