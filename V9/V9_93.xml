<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">colurieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">geum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">rivale</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 501. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe colurieae;genus geum;species rivale</taxon_hierarchy>
    <other_info_on_name type="fna_id">200010867</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Purple or water avens</other_name>
  <other_name type="common_name">chocolate-root</other_name>
  <other_name type="common_name">benoîte des ruisseaux</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants leafy-stemmed.</text>
      <biological_entity id="o14763" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="leafy-stemmed" value_original="leafy-stemmed" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 30–85 cm, sparsely downy to scattered-hirsute proximally, downy distally.</text>
      <biological_entity id="o14764" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="85" to_unit="cm" />
        <character char_type="range_value" from="sparsely downy" name="pubescence" src="d0_s1" to="scattered-hirsute proximally" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="downy" value_original="downy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 6–40 cm, blade interruptedly pinnate, major leaflets 5–7, mixed with 7–14 minor ones, terminal leaflet slightly larger than major laterals;</text>
      <biological_entity id="o14765" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o14766" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14767" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="interruptedly" name="architecture_or_shape" src="d0_s2" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o14768" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="7" />
        <character constraint="with minor" constraintid="o14769" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o14769" name="minor" name_original="minor" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s2" to="14" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o14770" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character constraint="than major laterals" constraintid="o14771" is_modifier="false" name="size" src="d0_s2" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity id="o14771" name="lateral" name_original="laterals" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="major" value_original="major" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline 2–10 cm, stipules ± free, 7–18 × 5–9 mm, blade pinnate to 3-foliolate.</text>
      <biological_entity id="o14772" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o14773" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14774" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s3" value="free" value_original="free" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14775" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="pinnate" name="architecture" src="d0_s3" to="3-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences 2–8-flowered.</text>
      <biological_entity id="o14776" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-8-flowered" value_original="2-8-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels densely downy, some hairs glandular.</text>
      <biological_entity id="o14777" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="downy" value_original="downy" />
      </biological_entity>
      <biological_entity id="o14778" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers nodding;</text>
      <biological_entity id="o14779" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets 2–4 mm;</text>
      <biological_entity constraint="epicalyx" id="o14780" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium greenish maroon to maroon;</text>
      <biological_entity id="o14781" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="greenish maroon" name="coloration" src="d0_s8" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect, 7–10 mm;</text>
      <biological_entity id="o14782" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals erect, yellow, suffused with purple and purple-veined, spatulate-obovate, 8–10 mm, ± equal to sepals, apex rounded, truncate, or shallowly emarginate.</text>
      <biological_entity id="o14783" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="suffused with purple" value_original="suffused with purple" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="purple-veined" value_original="purple-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate-obovate" value_original="spatulate-obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character constraint="to sepals" constraintid="o14784" is_modifier="false" modifier="more or less" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o14784" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruiting tori on 4–10 mm stipes, densely bristly.</text>
      <biological_entity id="o14786" name="torus" name_original="tori" src="d0_s11" type="structure" />
      <biological_entity id="o14787" name="stipe" name_original="stipes" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="bristly" value_original="bristly" />
      </biological_entity>
      <relation from="o14785" id="r954" name="fruiting" negation="false" src="d0_s11" to="o14786" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting styles geniculate-jointed, proximal segment persistent, 5–9 mm, apex hooked, stipitate-glandular, bristles on basal 1/2, distal segment deciduous, 3–4.5 mm, pilose except apical 1 mm. 2n = 42.</text>
      <biological_entity id="o14785" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity id="o14788" name="style" name_original="styles" src="d0_s12" type="structure" />
      <biological_entity id="o14789" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="geniculate-jointed" value_original="geniculate-jointed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14790" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14791" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o14792" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <biological_entity constraint="basal" id="o14793" name="1/2" name_original="1/2" src="d0_s12" type="structure" />
      <biological_entity constraint="distal" id="o14794" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character constraint="except apical 1 mm" is_modifier="false" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14795" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="42" value_original="42" />
      </biological_entity>
      <relation from="o14785" id="r955" name="fruiting" negation="false" src="d0_s12" to="o14788" />
      <relation from="o14792" id="r956" name="on" negation="false" src="d0_s12" to="o14793" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="mid spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swamps, fens, bogs, wet meadows, along streams and lakes, moist rich woods, in circumneutral to alkaline soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swamps" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet meadows" constraint="along streams and lakes" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="rich woods" modifier="moist" constraint="in circumneutral" />
        <character name="habitat" value="circumneutral" modifier="in" />
        <character name="habitat" value="to alkaline soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Nunavut, Ont., P.E.I., Que., Sask.; Colo., Conn., Idaho, Ill., Ind., Maine, Md., Mass., Mich., Minn., Mont., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Pa., R.I., S.Dak., Vt., Wash., W.Va., Wis., Wyo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Geum rivale forms hybrids with three other species of the genus in North America: G. ×aurantiacum Fries ex Scheutz [G. aleppicum × G. rivale] is reported from Alberta, Ontario, Montana, and New York; G. ×pervale B. Boivin [G. macrophyllum var. perincisum × G. rivale] is known from Alberta, Ontario, Quebec, and Saskatchewan; and G. ×pulchrum Fernald [G. macrophyllum var. macrophyllum × G. rivale] is reported from Ontario, Quebec, Idaho, New Hampshire, and Vermont. The hybrids generally have the habit and foliage of G. rivale but exhibit shallower hypanthia, spreading sepals, and pure yellow, clawed, obovate to suborbiculate petals. The glandular hairs on the pedicels usually lack the dark tips seen in G. rivale. Given that the morphologic differences among the three named hybrids are vague, suspected hybrid specimens are best determined while in the field where collectors can note the proximity of the hybrids to their supposed parental species.</discussion>
  
</bio:treatment>