<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1840" rank="species">gymnocarpa</taxon_name>
    <taxon_name authority="Ertter &amp; W. H. Lewis" date="2008" rank="variety">serpentina</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>55: 174, fig. 2. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species gymnocarpa;variety serpentina;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100722</other_info_on_name>
  </taxon_identification>
  <number>30b.</number>
  <other_name type="common_name">Gasquet rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (1–) 3–6 (–13) dm.</text>
      <biological_entity id="o25073" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="13" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 2–6 (–8) × 1.5–3 (–4) cm;</text>
      <biological_entity id="o25074" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipules 5–11 mm;</text>
      <biological_entity id="o25075" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 5 (–7), often bluish green or red-tinged, ± leathery, terminal: petiolule 2–10 mm, blade broadly elliptic to obovate or ovate to nearly orbiculate, 4–20 × 4–20 mm, apex usually broadly obtuse to rounded, sometimes nearly truncate.</text>
      <biological_entity id="o25076" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="7" />
        <character name="quantity" src="d0_s3" value="5" value_original="5" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red-tinged" value_original="red-tinged" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o25077" name="petiolule" name_original="petiolule" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25078" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s3" to="obovate or ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25079" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually broadly obtuse" name="shape" src="d0_s3" to="rounded" />
        <character is_modifier="false" modifier="sometimes nearly" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 10–15 mm, eglandular or stipitate-glandular (in same inflorescence).</text>
      <biological_entity id="o25080" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 2 cm diam.;</text>
      <biological_entity id="o25081" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character name="diameter" src="d0_s5" unit="cm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 8–10 × 6–10 mm.</text>
      <biological_entity id="o25082" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Hips irregularly ovoid to elongate-ellipsoid.</text>
      <biological_entity id="o25083" name="hip" name_original="hips" src="d0_s7" type="structure">
        <character char_type="range_value" from="irregularly ovoid" name="shape" src="d0_s7" to="elongate-ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 1–4.</text>
      <biological_entity id="o25084" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Full sun in roadsides, ridges, and other openings in chaparral and stunted forests on ultramafic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="full sun" constraint="in roadsides , ridges , and other openings in chaparral and stunted forests on ultramafic substrates" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="other openings" constraint="in chaparral and stunted forests on ultramafic substrates" />
        <character name="habitat" value="chaparral" constraint="on ultramafic substrates" />
        <character name="habitat" value="stunted forests" constraint="on ultramafic substrates" />
        <character name="habitat" value="ultramafic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1500(–2300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2300" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety serpentina is the form that usually occurs in full sun on ultramafic substrates in the Siskiyou Mountains of northwestern California and southwestern Oregon.</discussion>
  
</bio:treatment>