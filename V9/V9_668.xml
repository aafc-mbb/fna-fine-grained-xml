<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="illustration_page">397</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">spiraea</taxon_name>
    <taxon_name authority="Du Roi" date="unknown" rank="species">alba</taxon_name>
    <place_of_publication>
      <publication_title>Harbk. Baumz.</publication_title>
      <place_in_publication>2: 430. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus spiraea;species alba</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242417310</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Narrow-leaf</other_name>
  <other_name type="common_name">eastern</other_name>
  <other_name type="common_name">or white meadowsweet</other_name>
  <other_name type="common_name">spirée blanche</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 10–20 dm.</text>
      <biological_entity id="o29668" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, unbranched.</text>
      <biological_entity id="o29669" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 2–8 mm, puberulent or sparsely hairy;</text>
      <biological_entity id="o29670" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o29671" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly lanceolate to oblanceolate or broadly oblanceolate to obovate, 2–9 × 0.5–3 cm, length 3–5 times width, chartaceous or membranous, base cuneate to rounded, margins finely to coarsely, sharply serrate to serrulate (sometimes doubly so on long-shoot leaves), number of primary and secondary serrations 0.5–1.1 times number of secondary-veins (excluding inter-secondary veins), venation pinnate craspedodromous, secondary-veins not prominent, irregularly terminating in primary teeth, inter-secondary veins usually 8–12+ per leaf, apex acute to obtuse, abaxial surface mostly glabrous, adaxial glabrous.</text>
      <biological_entity id="o29672" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o29673" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s3" to="oblanceolate or broadly oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-5" value_original="3-5" />
        <character is_modifier="false" name="texture" src="d0_s3" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o29674" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="rounded" />
      </biological_entity>
      <biological_entity id="o29675" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="sharply serrate" modifier="coarsely" name="architecture_or_shape" src="d0_s3" to="serrulate" />
        <character constraint="secondary-vein" constraintid="o29677" is_modifier="false" name="size_or_quantity" src="d0_s3" value="0.5-1.1 times number of secondary-veins" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="craspedodromous" value_original="craspedodromous" />
      </biological_entity>
      <biological_entity constraint="primary and secondary" id="o29676" name="serration" name_original="serrations" src="d0_s3" type="structure" />
      <biological_entity id="o29677" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure" />
      <biological_entity id="o29678" name="secondary-vein" name_original="secondary-veins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s3" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="primary" id="o29679" name="tooth" name_original="teeth" src="d0_s3" type="structure" />
      <biological_entity constraint="inter-secondary" id="o29680" name="vein" name_original="veins" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per leaf" constraintid="o29681" from="8" modifier="irregularly" name="quantity" src="d0_s3" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o29681" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o29682" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29683" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29684" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o29675" id="r1943" name="part_of" negation="false" src="d0_s3" to="o29676" />
      <relation from="o29678" id="r1944" name="terminating in" negation="false" src="d0_s3" to="o29679" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences mostly terminal, narrowly conic to open, pyramidal panicles, 5–20 × 3–10 cm height 1.4–3.5 times diam.;</text>
      <biological_entity id="o29685" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s4" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o29686" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="pyramidal" value_original="pyramidal" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" name="height" src="d0_s4" value="1.4-3.5 times diam" value_original="1.4-3.5 times diam" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches usually in axils of leaves, puberulent to pubescent.</text>
      <biological_entity id="o29687" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" notes="" src="d0_s5" to="pubescent" />
      </biological_entity>
      <biological_entity id="o29688" name="axil" name_original="axils" src="d0_s5" type="structure" />
      <biological_entity id="o29689" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o29687" id="r1945" name="in" negation="false" src="d0_s5" to="o29688" />
      <relation from="o29688" id="r1946" name="part_of" negation="false" src="d0_s5" to="o29689" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 1–2 (–3) mm, glabrous or glabrate.</text>
      <biological_entity id="o29690" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers 3–8 mm diam.;</text>
      <biological_entity id="o29691" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthia hemispheric, 0.6–0.8 mm, abaxial surface usually puberulent to sparsely strigose, sometimes glabrate or glabrous, adaxial glabrous;</text>
      <biological_entity id="o29692" name="hypanthium" name_original="hypanthia" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29693" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character char_type="range_value" from="usually puberulent" name="pubescence" src="d0_s8" to="sparsely strigose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29694" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals triangular, 0.8–1.5 mm;</text>
      <biological_entity id="o29695" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually white, sometimes pink-tinged (in bud), suborbiculate, 1.3–2 (–3) mm;</text>
      <biological_entity id="o29696" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="pink-tinged" value_original="pink-tinged" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>staminodes 0–4;</text>
      <biological_entity id="o29697" name="staminode" name_original="staminodes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 30–50, 1–2 times petal length.</text>
      <biological_entity id="o29698" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="50" />
        <character constraint="petal" constraintid="o29699" is_modifier="false" name="length" src="d0_s12" value="1-2 times petal length" value_original="1-2 times petal length" />
      </biological_entity>
      <biological_entity id="o29699" name="petal" name_original="petal" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Follicles oblanceoloid, 3–4 mm, shiny, glabrous.</text>
      <biological_entity id="o29700" name="follicle" name_original="follicles" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceoloid" value_original="oblanceoloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Conn., Del., Ill., Ind., Iowa, Ky., Maine, Mass., Md., Mich., Minn., Mo., N.C., N.Dak., N.H., N.J., N.Y., Ohio, Pa., R.I., S.Dak., Tenn., Va., Vt., W.Va., Wis.; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Varieties alba and latifolia have regions of hybridization, primarily around the Great Lakes, that produce intermediate forms that may be difficult to key. Recognition of these two taxa as either species or varieties has been problematic because the taxa are quite distinct at the extremes of their range. A. R. Kugel (1958) recognized them as distinct species and her work illustrates the regional zone of hybridization. H. A. Gleason (1952) and Gleason and A. Cronquist (1963) also recognized the two taxa as separate species; later Gleason and Cronquist (1991) recognized them as varieties that frequently intergrade.</discussion>
  <discussion>The European Spiraea salicifolia has a leaf morphology that is similar to that of var. alba and the two taxa have been treated as conspecific. With the interest in Spiraea as an ornamental, S. salicifolia was imported to North America, resulting in this species or hybrids of it becoming naturalized. Specimens that are difficult to identify may be S. salicifolia, as escapes from homesteads and gardens that became established or may have hybridized with native taxa.</discussion>
  <discussion>Spiraea alba has become locally naturalized in western and central Europe; in the British Isles, its hybrids are commonly naturalized as S. ×rosalba Dippel (S. alba × S. salicifolia), or as S. ×billardii Hortus ex K. Koch (S. alba × S. douglasii) (A. J. Silverside 1990).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: lengths 3–4 times widths, margins finely serrate to serrulate; inflorescences narrowly conic.</description>
      <determination>1a Spiraea alba var. alba</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: lengths 2–3 times widths, margins coarsely serrate; inflorescences open, pyramidal.</description>
      <determination>1b Spiraea alba var. latifolia</determination>
    </key_statement>
  </key>
</bio:treatment>