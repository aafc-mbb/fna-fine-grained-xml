<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">Subviscosae</taxon_name>
    <taxon_name authority="Greene" date="1881" rank="species">subviscosa</taxon_name>
    <taxon_name authority="(Rydberg) Kearney &amp; Peebles" date="1939" rank="variety">ramulosa</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>29: 481. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section subviscosae;species subviscosa;variety ramulosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100701</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">ramulosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 430, plate 276. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Potentilla;species ramulosa</taxon_hierarchy>
  </taxon_identification>
  <number>54b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (0.2–) 0.4–1.5 (–2) dm.</text>
      <biological_entity id="o5639" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="0.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s0" to="1.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves not notably seasonally dimorphic, 2–7 (–15 in late-season) cm;</text>
      <biological_entity constraint="basal" id="o5640" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not notably seasonally" name="growth_form" src="d0_s1" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–5 (–11 in late-season) cm, long hairs 2–3 (–4) mm (abundant all seasons);</text>
      <biological_entity id="o5641" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5642" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central leaflets obovate-cuneate to lanceolate, 1–2.5 (–5 in late-season) cm, ± evenly incised 1/4–1/2 to midvein, teeth 2–4 (–6 in late-season) per side.</text>
      <biological_entity constraint="central" id="o5643" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate-cuneate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="2.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less evenly" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o5644" from="1/4" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity id="o5644" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o5645" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o5646" from="2" name="quantity" src="d0_s3" to="4" />
      </biological_entity>
      <biological_entity id="o5646" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals (2.5–) 3.5–5 (–6) mm;</text>
      <biological_entity id="o5647" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o5648" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 3–5 (–8) mm.</text>
      <biological_entity id="o5649" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o5650" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Achenes 1.5–2 mm, lightly rugose, with small scar.</text>
      <biological_entity id="o5651" name="achene" name_original="achenes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="lightly" name="relief" src="d0_s6" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity id="o5652" name="scar" name_original="scar" src="d0_s6" type="structure">
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
      </biological_entity>
      <relation from="o5651" id="r346" name="with" negation="false" src="d0_s6" to="o5652" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open conifer woodlands, edges of decomposing granite outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="conifer woodlands" modifier="open" />
        <character name="habitat" value="edges" constraint="of decomposing granite outcrops" />
        <character name="habitat" value="decomposing granite outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety ramulosa is the primary representative of sect. Subviscosae in the Rincon and Santa Catalina ranges in Pima County (the only other being Potentilla albiflora). Comparable plants have been collected on Aztec Peak in the Sierra Ancha of Gila County, where they overlap the range of and possibly intergrade with var. subviscosa. Intermediates also occur in the nearby Mazatzal Range and Fort Apache Indian Reservation. Historic collections of var. ramulosa exist from the Chiricahua, Huachuca, and Santa Rita ranges in Cochise and Santa Cruz counties, apparently from lower elevations than the known populations of P. rhyolitica.</discussion>
  
</bio:treatment>