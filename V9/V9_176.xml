<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="illustration_page">115</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1880" rank="species">spithamea</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 444. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species spithamea;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100425</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rosa</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">granulata</taxon_name>
    <taxon_hierarchy>genus Rosa;species granulata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">sonomensis</taxon_name>
    <taxon_hierarchy>genus R.;species sonomensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">R.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spithamea</taxon_name>
    <taxon_name authority="(Greene) Jepson" date="unknown" rank="variety">sonomensis</taxon_name>
    <taxon_hierarchy>genus R.;species spithamea;variety sonomensis</taxon_hierarchy>
  </taxon_identification>
  <number>32.</number>
  <other_name type="common_name">Coast ground rose</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, forming open colonies.</text>
      <biological_entity id="o23698" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 1–5 (–10) dm, openly branched;</text>
      <biological_entity id="o23699" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark dark reddish-brown, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>infrastipular prickles paired, erect to slightly curved, largest sometimes ± flattened, subulate, 2–8 (–12) × 2–5 mm, base glabrous, internodal prickles absent or sparse to dense, mixed with aciculi, aciculi sometimes absent, stipitate-glandular or eglandular.</text>
      <biological_entity id="o23700" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23701" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character is_modifier="false" modifier="sometimes more or less" name="shape" src="d0_s3" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23702" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o23703" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s3" value="sparse to dense" value_original="sparse to dense" />
        <character constraint="with aciculi" constraintid="o23704" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o23704" name="aciculus" name_original="aciculi" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" notes="" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves 3–10 cm;</text>
      <biological_entity id="o23705" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules 6–15 × 1.5–2.5 (–3.5) mm, auricles flared to ± erect, 1.5–6 mm, margins entire, stipitate-glandular, surfaces glabrous, eglandular;</text>
      <biological_entity id="o23706" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23707" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flared" value_original="flared" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23708" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23709" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and rachis with pricklets, usually glabrous, rarely finely hairy, stipitate-glandular;</text>
      <biological_entity id="o23710" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely finely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23711" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely finely" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23712" name="pricklet" name_original="pricklets" src="d0_s6" type="structure" />
      <relation from="o23710" id="r1542" name="with" negation="false" src="d0_s6" to="o23712" />
      <relation from="o23711" id="r1543" name="with" negation="false" src="d0_s6" to="o23712" />
    </statement>
    <statement id="d0_s7">
      <text>leaflets 5–7 (–9), terminal: petiolule 3–13 mm, blade ovate-elliptic, sometimes obovate-elliptic, 10–30 × 8–20 mm, membranous to ± leathery, base obtuse, margins ± 2-serrate, teeth 8–20 per side, acute to obtuse, gland-tipped, apex obtuse to ± truncate, abaxial surfaces pale green to green, glabrous, sometimes sparsely hairy, sessile-glandular or eglandular, adaxial green, dull, rarely sparsely hairy.</text>
      <biological_entity id="o23713" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="9" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o23714" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23715" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="obovate-elliptic" value_original="obovate-elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="membranous" name="texture" src="d0_s7" to="more or less leathery" />
      </biological_entity>
      <biological_entity id="o23716" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o23717" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="2-serrate" value_original="2-serrate" />
      </biological_entity>
      <biological_entity id="o23718" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o23719" from="8" name="quantity" src="d0_s7" to="20" />
        <character char_type="range_value" from="acute" name="shape" notes="" src="d0_s7" to="obtuse" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o23719" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o23720" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s7" to="more or less truncate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23721" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s7" to="green" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23722" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences corymbs, 1–10-flowered.</text>
      <biological_entity constraint="inflorescences" id="o23723" name="corymb" name_original="corymbs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-10-flowered" value_original="1-10-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, slender to stout, 2–15 mm, glabrous, rarely finely pubescent, stipitate-glandular, sometimes setose-glandular;</text>
      <biological_entity id="o23724" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="slender" name="size" src="d0_s9" to="stout" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely finely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s9" value="setose-glandular" value_original="setose-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 1 or 2, lanceolate, 2–15 × 1.5–5 mm, margins entire, stipitate-glandular, surfaces glabrous, eglandular.</text>
      <biological_entity id="o23725" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23726" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23727" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 2.5–3.8 cm diam.;</text>
      <biological_entity id="o23728" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="diameter" src="d0_s11" to="3.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium ± ovoid, 3–6 × 2.5–5 mm, glabrous, sparsely to densely stipitate to setose-glandular, rarely eglandular, neck 0.5–1 × 2.5–4 mm;</text>
      <biological_entity id="o23729" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="stipitate" modifier="sparsely to densely; densely" name="architecture" src="d0_s12" to="setose-glandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s12" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o23730" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s12" to="1" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals spreading, ovatelanceolate, 8–15 × 2.5–3.5 mm, tip 0–4 × 1.3 mm, margins entire, abaxial surfaces glabrous, stipitate-glandular;</text>
      <biological_entity id="o23731" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s13" to="15" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23732" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s13" to="4" to_unit="mm" />
        <character name="width" src="d0_s13" unit="mm" value="1.3" value_original="1.3" />
      </biological_entity>
      <biological_entity id="o23733" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23734" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, deep pink, 12–23 × 15 mm;</text>
      <biological_entity id="o23735" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character is_modifier="false" name="depth" src="d0_s14" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pink" value_original="pink" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s14" to="23" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 15–30, styles exsert 1 mm beyond stylar orifice (2 mm diam.) of hypanthial disc (3.5 mm diam.).</text>
      <biological_entity id="o23736" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s15" to="30" />
      </biological_entity>
      <biological_entity id="o23737" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exsert" value_original="exsert" />
        <character constraint="beyond stylar, orifice" constraintid="o23738, o23739" name="some_measurement" src="d0_s15" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23738" name="stylar" name_original="stylar" src="d0_s15" type="structure" />
      <biological_entity id="o23739" name="orifice" name_original="orifice" src="d0_s15" type="structure" />
      <biological_entity id="o23740" name="hypanthial" name_original="hypanthial" src="d0_s15" type="structure" />
      <biological_entity id="o23741" name="disc" name_original="disc" src="d0_s15" type="structure" />
      <relation from="o23738" id="r1544" name="part_of" negation="false" src="d0_s15" to="o23740" />
      <relation from="o23738" id="r1545" name="part_of" negation="false" src="d0_s15" to="o23741" />
      <relation from="o23739" id="r1546" name="part_of" negation="false" src="d0_s15" to="o23740" />
      <relation from="o23739" id="r1547" name="part_of" negation="false" src="d0_s15" to="o23741" />
    </statement>
    <statement id="d0_s16">
      <text>Hips scarlet, ± depressed-subglobose, 7–15 × 7–15 mm, fleshy, glabrous, sparsely to densely stipitate to setose-glandular, rarely eglandular, neck 0.5–2 × 2–5 mm;</text>
      <biological_entity id="o23742" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="depressed-subglobose" value_original="depressed-subglobose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s16" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s16" to="15" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="stipitate" modifier="sparsely to densely; densely" name="architecture" src="d0_s16" to="setose-glandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s16" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o23743" name="neck" name_original="neck" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s16" to="2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals persistent, erect.</text>
      <biological_entity id="o23744" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basiparietal, 4–17, cream to pale-brown, 3.5–5 × 2.5–3.5 mm. 2n = 14.</text>
      <biological_entity id="o23745" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s18" to="17" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s18" to="pale-brown" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s18" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s18" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23746" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Edges and understory of chaparral and mixed forests, post-burn openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="edges" constraint="of chaparral and mixed forests , post-burn openings" />
        <character name="habitat" value="understory" constraint="of chaparral and mixed forests , post-burn openings" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="mixed forests" />
        <character name="habitat" value="post-burn openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The combination of miniature size and stipitate- to setose-glandular hips makes Rosa spithamea one of the more easily and reliably identified species in the genus, although the density of hypanthial glands can vary. The species occurs from Douglas County, Oregon, to San Luis Obispo County, California. Rosa spithamea is fire-adapted and blooms profusely only after fires or equivalent disturbances; at other times, it persists in the understory in a vegetative state.</discussion>
  <discussion>E. W. Erlanson (1934) referred to Rosa spithamea as tetraploid (2n = 28); twice in her key to R. spithamea she questioned that count.</discussion>
  
</bio:treatment>