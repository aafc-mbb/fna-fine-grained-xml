<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="(Torrey) A. Gray" date="1874" rank="species">fasciculata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 70. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species fasciculata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100389</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Emplectocladus</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">fasciculatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Assoc. Advancem. Sci.</publication_title>
      <place_in_publication>4: 192. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Emplectocladus;species fasciculatus</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <other_name type="common_name">Desert almond</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, suckering, much branched, 10–20 (–30) dm, thorny.</text>
      <biological_entity id="o23554" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with axillary end buds, glabrous or canescent.</text>
      <biological_entity id="o23555" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="canescent" value_original="canescent" />
      </biological_entity>
      <biological_entity constraint="end" id="o23556" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="axillary end" />
      <relation from="o23555" id="r1530" name="with" negation="false" src="d0_s1" to="o23556" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o23557" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to linear, 0.5–2 × 0.1–0.2 (–0.4) cm, base long-attenuate, margins nearly entire or obscurely and remotely serrulate in distal 1/3, teeth blunt to sharp, sometimes glandular, apex rounded to acute, surfaces puberulent or glabrous or low-papillate (var. punctata).</text>
      <biological_entity id="o23558" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="width" src="d0_s4" to="0.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23559" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-attenuate" value_original="long-attenuate" />
      </biological_entity>
      <biological_entity id="o23560" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="obscurely" value_original="obscurely" />
        <character constraint="in distal 1/3" constraintid="o23561" is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o23561" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o23562" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s4" to="sharp" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o23563" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o23564" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s4" value="low-papillate" value_original="low-papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences solitary flowers or 2-flowered fascicles.</text>
      <biological_entity id="o23565" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure" />
      <biological_entity id="o23566" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-flowered" value_original="2-flowered" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="fascicles" value_original="fascicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0–4 mm, glabrous.</text>
      <biological_entity id="o23567" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers unisexual, plants dioecious, blooming at leaf emergence;</text>
      <biological_entity id="o23568" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <biological_entity id="o23569" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="dioecious" value_original="dioecious" />
        <character constraint="at leaf" constraintid="o23570" is_modifier="false" name="life_cycle" src="d0_s7" value="blooming" value_original="blooming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23570" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>hypanthium campanulate, 1.5–3 mm, glabrous externally;</text>
      <biological_entity id="o23571" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals erect-spreading, triangular, 0.7–1 mm, margins entire, surfaces glabrous;</text>
      <biological_entity id="o23572" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23573" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o23574" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white to yellowish, elliptic, obovate, or suborbiculate, 1.4–2.5 (–4) mm;</text>
      <biological_entity id="o23575" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="yellowish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovaries hairy.</text>
      <biological_entity id="o23576" name="ovary" name_original="ovaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Drupes gray to redbrown, ovoid, ± compressed, 7–15 mm, densely puberulent;</text>
      <biological_entity id="o23577" name="drupe" name_original="drupes" src="d0_s12" type="structure">
        <character char_type="range_value" from="gray" name="coloration" src="d0_s12" to="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium tardily deciduous;</text>
      <biological_entity id="o23578" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="tardily" name="duration" src="d0_s13" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>mesocarps leathery to dry;</text>
      <biological_entity id="o23579" name="mesocarp" name_original="mesocarps" src="d0_s14" type="structure">
        <character char_type="range_value" from="leathery" name="texture" src="d0_s14" to="dry" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stones ovoid, ± flattened.</text>
      <biological_entity id="o23580" name="stone" name_original="stones" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades sparsely to densely puberulent, not papillate.</description>
      <determination>17a Prunus fasciculata var. fasciculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades glabrous, sometimes papillate.</description>
      <determination>17b Prunus fasciculata var. punctata</determination>
    </key_statement>
  </key>
</bio:treatment>