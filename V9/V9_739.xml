<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="mention_page">439</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sorbus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">sorbus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Sorbus</taxon_name>
    <taxon_name authority="Linnaeus" date="1762" rank="species">hybrida</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 1: 684. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus sorbus;subgenus sorbus;section sorbus;species hybrida;</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250100456</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Oak-leaf mountain ash</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees, 30–120 dm.</text>
      <biological_entity id="o10480" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="dm" name="some_measurement" src="d0_s0" to="120" to_unit="dm" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems single or multistemmed;</text>
      <biological_entity id="o10481" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="single" value_original="single" />
        <character is_modifier="false" name="architecture_or_growth_form" src="d0_s1" value="multistemmed" value_original="multistemmed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark gray;</text>
      <biological_entity id="o10482" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>winter buds brownish to purple, ovoid to ovoid-conic, 4–10 mm, dull, not glutinous, densely white-villous, at least distally and on scale margins.</text>
      <biological_entity id="o10483" name="bud" name_original="buds" src="d0_s3" type="structure">
        <character is_modifier="true" name="season" src="d0_s3" value="winter" value_original="winter" />
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s3" to="purple" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s3" to="ovoid-conic" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="not" name="coating" src="d0_s3" value="glutinous" value_original="glutinous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="white-villous" value_original="white-villous" />
      </biological_entity>
      <biological_entity constraint="scale" id="o10484" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <relation from="o10483" id="r644" modifier="at-least distally; distally" name="on" negation="false" src="d0_s3" to="o10484" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves proximally pinnately compound, lobed distally;</text>
      <biological_entity id="o10485" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally pinnately" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules deciduous or persistent, often white-villous;</text>
      <biological_entity id="o10486" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s5" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade paler green abaxially, dull green adaxially, ovate to oblong-ovate, rarely oblong, 7–13 × 5–11 cm, pinnately lobed with (1 or) 2 (or 3) pairs of sessile or decurrent, free, oblong leaflets, terminal leaflets 7–10-lobed, margins serrate at least distally, lobe and leaflet apex acute or obtuse, lateral-veins 7–10 pairs, abaxial surface whitish-tomentose.</text>
      <biological_entity id="o10487" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abaxially" name="coloration" src="d0_s6" value="paler green" value_original="paler green" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="oblong-ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="13" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="11" to_unit="cm" />
        <character constraint="with pairs" constraintid="o10488" is_modifier="false" modifier="pinnately" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o10488" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o10489" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
        <character is_modifier="true" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character is_modifier="true" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o10490" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="7-10-lobed" value_original="7-10-lobed" />
      </biological_entity>
      <biological_entity id="o10491" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="at-least distally; distally" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o10492" name="lobe" name_original="lobe" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="leaflet" id="o10493" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o10494" name="lateral-vein" name_original="lateral-veins" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10495" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="whitish-tomentose" value_original="whitish-tomentose" />
      </biological_entity>
      <relation from="o10488" id="r645" name="part_of" negation="false" src="d0_s6" to="o10489" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 25–75-flowered, flat-topped or rounded, 4.5–10 cm diam.;</text>
      <biological_entity id="o10496" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="25-75-flowered" value_original="25-75-flowered" />
        <character is_modifier="false" name="shape" src="d0_s7" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="diameter" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles white-villous.</text>
      <biological_entity id="o10497" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels white-villous.</text>
      <biological_entity id="o10498" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers 10–15 mm diam.;</text>
      <biological_entity id="o10499" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>hypanthium tomentose, hypanthium plus sepals 5–7.5 mm;</text>
      <biological_entity id="o10500" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o10501" name="hypanthium" name_original="hypanthium" src="d0_s11" type="structure" />
      <biological_entity id="o10502" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals 1.5–4 mm, margins usually entire, rarely with inconspicuous glands;</text>
      <biological_entity id="o10503" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10504" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10505" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o10504" id="r646" modifier="rarely" name="with" negation="false" src="d0_s12" to="o10505" />
    </statement>
    <statement id="d0_s13">
      <text>petals white, suborbiculate, broadly obovate, broadly ovate, or broadly elliptic, 5–7 mm;</text>
      <biological_entity id="o10506" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens 20;</text>
      <biological_entity id="o10507" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 1/2 adnate to hypanthium, apex conic, styles 2 or 3, 2–3 mm.</text>
      <biological_entity id="o10508" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
        <character constraint="to hypanthium" constraintid="o10509" is_modifier="false" name="fusion" src="d0_s15" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o10509" name="hypanthium" name_original="hypanthium" src="d0_s15" type="structure" />
      <biological_entity id="o10510" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o10511" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s15" unit="or" value="3" value_original="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Infructescences glabrate to villous.</text>
      <biological_entity id="o10512" name="infructescence" name_original="infructescences" src="d0_s16" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s16" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pomes red, globose to subglobose, 8–15 mm diam., shiny, not glaucous;</text>
      <biological_entity id="o10513" name="pome" name_original="pomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="red" value_original="red" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s17" to="subglobose" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s17" to="15" to_unit="mm" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s17" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>sepals inconspicuous, incurved.</text>
      <biological_entity id="o10514" name="sepal" name_original="sepals" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="orientation" src="d0_s18" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds redbrown, ovoid, 5–6 × 2 mm, asymmetric, slightly flattened.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 68.</text>
      <biological_entity id="o10515" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s19" to="6" to_unit="mm" />
        <character name="width" src="d0_s19" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10516" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring; fruiting fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
        <character name="fruiting time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods, rocky slopes, disturbed ground and edges near towns</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="disturbed ground" />
        <character name="habitat" value="edges" />
        <character name="habitat" value="towns" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; N.B.; Utah, Vt., Wash.; n Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="n Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sorbus hybrida is considered an apomictic tetraploid producing fully fertile pollen and seed and subject to dispersal. An almost undistinguishable European taxon, S. ×thuringiaca (Ilse) Fritsch, often confused with S. hybrida, has been reported for New Hampshire and Vermont. Described as an unstable diploid hybrid between S. aria and S. aucuparia, S. ×thuringiaca has oblong leaves, 1–3(–5) pairs of free leaflets, 10–12(–13) pairs of lateral veins, and produces sterile pollen and relatively few viable seeds. Uncritical reports of escaped S. ×thuringiaca in North America should be considered doubtful; they probably refer to the more likely S. hybrida. The authors have not seen a specimen to document a report of S. hybrida from Montana.</discussion>
  <discussion>Sorbus hybrida is placed in subg. Sorbus following J. J. Aldasoro et al. (2004), based on leaf division and pome characteristics, including the lack of tanniferous cells in parenchyma, lack of starch, and relatively small sclereid groups.</discussion>
  
</bio:treatment>