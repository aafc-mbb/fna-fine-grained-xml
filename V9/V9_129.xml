<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="Crépin" date="1889" rank="section">Gallicae</taxon_name>
    <place_of_publication>
      <publication_title>J. Roy. Hort. Soc.</publication_title>
      <place_in_publication>11: 221. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section Gallicae</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">318046</other_info_on_name>
  </taxon_identification>
  <number>7b.4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, single or forming patches;</text>
      <biological_entity id="o6451" name="patch" name_original="patches" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="single" value_original="single" />
      </biological_entity>
      <relation from="o6450" id="r392" name="single or forming patches" negation="false" src="d0_s0" to="o6451" />
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes relatively long.</text>
      <biological_entity id="o6450" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o6452" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or arching, (5–) 10–15 (–20) dm;</text>
      <biological_entity id="o6453" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character char_type="range_value" from="5" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="10" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="20" to_unit="dm" />
        <character char_type="range_value" from="10" from_unit="dm" name="some_measurement" src="d0_s2" to="15" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal branches glabrous, sparsely setose and stipitate-glandular;</text>
    </statement>
    <statement id="d0_s4">
      <text>infrastipular prickles absent, rarely present, internodal prickles curved, sometimes erect, rarely hooked or declined, mixed with aciculi, stipitate-glands, and smaller prickles to 3 mm.</text>
      <biological_entity constraint="distal" id="o6454" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="setose" value_original="setose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o6455" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o6456" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="declined" value_original="declined" />
        <character constraint="with smaller prickles" constraintid="o6458" is_modifier="false" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o6457" name="stipitate-gland" name_original="stipitate-glands" src="d0_s4" type="structure" />
      <biological_entity constraint="smaller" id="o6458" name="prickle" name_original="prickles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves deciduous, rarely persistent, 10.5–12 cm, leathery;</text>
      <biological_entity id="o6459" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="10.5" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules caducous, adnate, auricles flared or erect, margins entire or serrate, gland-tipped;</text>
      <biological_entity id="o6460" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="duration" src="d0_s6" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o6461" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flared" value_original="flared" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o6462" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 3–5 (–7), terminal: petiolule 6–8 mm, blade suborbiculate or ovate to narrowly elliptic, 20–26 (–60) × 17–30 mm, abaxial surfaces pubescent, sometimes sessile-glandular, adaxial dull, glabrous.</text>
      <biological_entity id="o6463" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="7" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o6464" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6465" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="narrowly elliptic" />
        <character char_type="range_value" from="26" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s7" to="26" to_unit="mm" />
        <character char_type="range_value" from="17" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6466" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6467" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s7" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences panicles, 1–3 (–8) -flowered.</text>
      <biological_entity constraint="inflorescences" id="o6468" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-3(-8)-flowered" value_original="1-3(-8)-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, stout, (10–) 20–60 mm, pubescent, stipitate-glandular;</text>
      <biological_entity id="o6469" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s9" to="60" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts caducous, 1 or 2, margins entire, stipitate-glandular.</text>
      <biological_entity id="o6470" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="caducous" value_original="caducous" />
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6471" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 6–9 cm diam.;</text>
      <biological_entity id="o6472" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="diameter" src="d0_s11" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium cupulate, glabrous, stipitate-glandular;</text>
      <biological_entity id="o6473" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals ± caducous, reflexed, spreading, or erect, deltate, 16–23 × 2.5–3.5 mm, margins usually pinnatifid, abaxial surfaces glabrous or pubescent, glandular;</text>
      <biological_entity id="o6474" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s13" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" src="d0_s13" to="23" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6475" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6476" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals usually double, deep pink to crimson;</text>
      <biological_entity id="o6477" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="deep" value_original="deep" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="crimson" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 30–47, styles free, pilose, rarely glabrous, hypanthium orifice 2 mm diam., hypanthial disc conic, 4–5 mm diam.</text>
      <biological_entity id="o6478" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s15" to="47" />
      </biological_entity>
      <biological_entity id="o6479" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="hypanthium" id="o6480" name="orifice" name_original="orifice" src="d0_s15" type="structure">
        <character name="diameter" src="d0_s15" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="hypanthial" id="o6481" name="disc" name_original="disc" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Hips bright to dark red, globose or subglobose, 8–9 × 10–12 mm, glabrous, setose, densely stipitate-glandular;</text>
      <biological_entity id="o6482" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s16" value="bright" value_original="bright" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s16" to="9" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s16" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals deciduous, erect to reflexed.</text>
      <biological_entity id="o6483" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s17" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basiparietal.</text>
      <biological_entity id="o6484" name="achene" name_original="achenes" src="d0_s18" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; c, s Europe, w Asia; introduced also in Mexico, Central America, South America, s Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="s Europe" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>