<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">415</other_info_on_meta>
    <other_info_on_meta type="illustration_page">407</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P de Candolle and A. L. P. P. de Candolle" date="unknown" rank="tribe">spiraeeae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="unknown" rank="genus">kelseya</taxon_name>
    <taxon_name authority="(S. Watson) Rydberg in N. L. Britton et al." date="1908" rank="species">uniflora</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 254. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus kelseya;species uniflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100284</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogynia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">uniflora</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>25: 130. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogynia;species uniflora</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">One-flower kelseya or spiraea</other_name>
  <other_name type="common_name">alpine laurel</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs 3–6 dm diam.</text>
      <biological_entity id="o27948" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="diameter" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves light green to grayish green, becoming brownish, marcescent, hardened.</text>
      <biological_entity id="o27949" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="light green" name="coloration" src="d0_s1" to="grayish green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="condition" src="d0_s1" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" name="texture" src="d0_s1" value="hardened" value_original="hardened" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals persistent, inflating radially with fruit maturity.</text>
      <biological_entity id="o27950" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o27951" name="sepal" name_original="sepals" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o27952" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
      <relation from="o27951" id="r1820" name="inflating" negation="false" src="d0_s2" to="o27952" />
    </statement>
    <statement id="d0_s3">
      <text>Follicles brown, coriaceous, opening first along adaxial suture, later on abaxial suture.</text>
      <biological_entity constraint="adaxial" id="o27954" name="suture" name_original="suture" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o27955" name="suture" name_original="suture" src="d0_s3" type="structure" />
      <relation from="o27953" id="r1821" name="along" negation="false" src="d0_s3" to="o27954" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity id="o27953" name="follicle" name_original="follicles" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="texture" src="d0_s3" value="coriaceous" value_original="coriaceous" />
        <character constraint="on abaxial suture" constraintid="o27955" is_modifier="false" name="condition" notes="" src="d0_s3" value="later" value_original="later" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27956" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cracks of limestone rock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cracks" constraint="of limestone rock outcrops" />
        <character name="habitat" value="limestone rock outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Kelseya uniflora can form dense carpets or mats that may cover areas of cliff faces. The solitary flower is unique in Spiraeeae. This species is of interest as an unusual rock-garden plant; it is difficult to cultivate.</discussion>
  
</bio:treatment>