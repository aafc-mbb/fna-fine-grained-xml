<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Maximowicz" date="1879" rank="tribe">gillenieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crataegus</taxon_name>
    <taxon_name authority="Loudon" date="1838" rank="section">Coccineae</taxon_name>
    <taxon_name authority="J. B. Phipps" date="unknown" rank="series">Lacrimatae</taxon_name>
    <taxon_name authority="Beadle" date="1901" rank="species">lepida</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 36. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe gillenieae;genus crataegus;section coccineae;series lacrimatae;species lepida;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100124</other_info_on_name>
  </taxon_identification>
  <number>127.</number>
  <other_name type="common_name">Dwarf hawthorn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 5–10 (–20) dm, dense and twiggy, branches not weeping except in larger specimens, capable of flowering at 3 dm.</text>
      <biological_entity id="o19134" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="twiggy" value_original="twiggy" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o19135" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="weeping" value_original="weeping" />
        <character is_modifier="false" name="size" src="d0_s0" value="larger" value_original="larger" />
        <character modifier="of flowering" name="some_measurement" src="d0_s0" unit="dm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: trunk bark dark gray, scaly;</text>
      <biological_entity id="o19136" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="trunk" id="o19137" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark gray" value_original="dark gray" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>twigs: new growth appressed-pubescent, 1-year old brown, slender;</text>
      <biological_entity id="o19138" name="twig" name_original="twigs" src="d0_s2" type="structure" />
      <biological_entity id="o19139" name="growth" name_original="growth" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>thorns on twigs straight, 1–2-years old color not recorded, fine, 0.6–2 cm.</text>
      <biological_entity id="o19140" name="twig" name_original="twigs" src="d0_s3" type="structure" />
      <biological_entity id="o19141" name="thorn" name_original="thorns" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" notes="" src="d0_s3" value="old" value_original="old" />
        <character is_modifier="false" name="width" src="d0_s3" value="fine" value_original="fine" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o19142" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o19141" id="r1235" name="on" negation="false" src="d0_s3" to="o19142" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves late deciduous to wintergreen;</text>
      <biological_entity id="o19143" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="deciduous" name="duration" src="d0_s4" to="wintergreen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules herbaceous, falcate, margins glandular;</text>
      <biological_entity id="o19144" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity id="o19145" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole length 15–30% blade, winged distally, pubescent, glandular;</text>
      <biological_entity id="o19146" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="15-30%; distally" name="length" notes="" src="d0_s6" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o19147" name="blade" name_original="blade" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade deep green, broadly obovate to ± isodiametric, 0.6–1.5 cm, coriaceous, base abruptly contracted, lobes 0 or 1 per side, subterminal, barely bumps, sinuses extremely shallow, lobe apex obtuse, margins minutely glandular-crenate to sometimes with conspicuous, glandular-scalloped teeth distally, veins 1 or 2 per side, apex obtuse or with slight apiculus, ± glossy, abaxial surface pubescent, especially on veins, adaxial very sparsely appressed-pilose young, particularly so on veins.</text>
      <biological_entity id="o19148" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s7" to="more or less isodiametric" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s7" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o19149" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o19150" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" unit="or per" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" unit="or per" value="1" value_original="1" />
        <character is_modifier="false" name="position" notes="" src="d0_s7" value="subterminal" value_original="subterminal" />
      </biological_entity>
      <biological_entity id="o19151" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o19152" name="bump" name_original="bumps" src="d0_s7" type="structure" />
      <biological_entity id="o19153" name="sinuse" name_original="sinuses" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="extremely" name="depth" src="d0_s7" value="shallow" value_original="shallow" />
      </biological_entity>
      <biological_entity constraint="lobe" id="o19154" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o19155" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character constraint="to " constraintid="o19156" is_modifier="false" modifier="minutely" name="shape" src="d0_s7" value="glandular-crenate" value_original="glandular-crenate" />
      </biological_entity>
      <biological_entity id="o19156" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="shape" src="d0_s7" value="glandular-scalloped" value_original="glandular-scalloped" />
      </biological_entity>
      <biological_entity id="o19157" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s7" unit="or per" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o19158" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o19159" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="with slight apiculus" value_original="with slight apiculus" />
        <character is_modifier="false" modifier="more or less" name="reflectance" notes="" src="d0_s7" value="glossy" value_original="glossy" />
      </biological_entity>
      <biological_entity id="o19160" name="apiculu" name_original="apiculus" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="slight" value_original="slight" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19161" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19162" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o19163" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="very sparsely" name="pubescence" src="d0_s7" value="appressed-pilose" value_original="appressed-pilose" />
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o19164" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <relation from="o19159" id="r1236" name="with" negation="false" src="d0_s7" to="o19160" />
      <relation from="o19161" id="r1237" modifier="especially" name="on" negation="false" src="d0_s7" to="o19162" />
      <relation from="o19163" id="r1238" modifier="particularly" name="on" negation="false" src="d0_s7" to="o19164" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences 1–3-flowered;</text>
      <biological_entity id="o19165" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches appressed-pubescent;</text>
      <biological_entity id="o19166" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles linear, gland-margined.</text>
      <biological_entity id="o19167" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="gland-margined" value_original="gland-margined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 12 mm diam.;</text>
      <biological_entity id="o19168" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character name="diameter" src="d0_s11" unit="mm" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium pubescent;</text>
      <biological_entity id="o19169" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals triangular, margins ± entire, abaxially pubescent;</text>
      <biological_entity id="o19170" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o19171" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ivory;</text>
      <biological_entity id="o19172" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="ivory" value_original="ivory" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 3 or 4.</text>
      <biological_entity id="o19173" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s15" unit="or" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pomes yellow-green to yellow or orange, broadly ellipsoid to suborbicular, 8–13 mm diam., black gland-dotted, pubescent;</text>
      <biological_entity id="o19174" name="pome" name_original="pomes" src="d0_s16" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s16" to="yellow or orange" />
        <character char_type="range_value" from="broadly ellipsoid" name="shape" src="d0_s16" to="suborbicular" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s16" to="13" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="black gland-dotted" value_original="black gland-dotted" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals broken off or recurved;</text>
      <biological_entity id="o19175" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="condition_or_fragility" src="d0_s17" value="broken" value_original="broken" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pyrenes 3.2n = 51, 68.</text>
      <biological_entity id="o19176" name="pyrene" name_original="pyrenes" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19177" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="51" value_original="51" />
        <character name="quantity" src="d0_s18" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May; fruiting Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dwarf scrub, sandy areas including inland dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scrub" modifier="dwarf" />
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="dunes" modifier="inland" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Crataegus lepida is a scarce, distinctive species occurring at scattered localities from central Florida to southern Georgia and South Carolina. A sterile specimen from Louisiana (Thomas 57507) may be this species.</discussion>
  <discussion>In central Florida, plants of Crataegus lepida start their extension growth and flower in May, long after growth of other nearby plants has started, while further north anthesis about the second week of April is usual. The southern populations are also the most wintergreen in the species. Thus, the central Florida plants appear to be ecophysiologically distinct and should be investigated further. Particularly small-leaved specimens with numerous, small, sharp marginal teeth may represent C. garrettii of Murrill.</discussion>
  <discussion>Crataegus lepida is a dwarf semi-evergreen shrub with dark, glossy, often suborbiculate leaves that are striking in the field. Larger specimens might be confused with C. egens.</discussion>
  
</bio:treatment>