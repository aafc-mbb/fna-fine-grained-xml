<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">213</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="Poeverlein in P. F. A. Ascherson et al." date="unknown" rank="section">Pensylvanicae</taxon_name>
    <taxon_name authority="Douglas in W. J. Hooker" date="unknown" rank="species">bipinnatifida</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker, Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 188. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section pensylvanicae;species bipinnatifida;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100309</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pensylvanica</taxon_name>
    <taxon_name authority="(Douglas) Torrey &amp; A. Gray" date="unknown" rank="variety">bipinnatifida</taxon_name>
    <taxon_hierarchy>genus Potentilla;species pensylvanica;variety bipinnatifida</taxon_hierarchy>
  </taxon_identification>
  <number>95.</number>
  <other_name type="common_name">potentille bipinnatifide</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending to erect, (1–) 2–5 dm.</text>
      <biological_entity id="o627" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves subpinnate to subpalmate, (6–) 10–25 cm;</text>
      <biological_entity constraint="basal" id="o628" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="subpinnate" name="architecture" src="d0_s1" to="subpalmate" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (2–) 5–15 cm, long hairs dense, appressed, 1–2 mm, soft to ± stiff, short hairs absent, crisped hairs sparse, glands absent, sparse, or obscured;</text>
      <biological_entity id="o629" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o630" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o631" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o632" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o633" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s2" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 2–3 per side, on distal 1/6–1/3 (–1/2) of leaf axis, separate to ± overlapping, terminal ones oblanceolate, (2–) 3–6 (–10) × 1–2 (–3.5) cm, margins revolute, incised 3/4+ to midvein, undivided medial blade 1.5–6 mm wide, teeth 5–8 per side, ± linear, surfaces ± to strongly dissimilar, abaxial usually white, rarely grayish, long hairs abundant especially on veins, 1–2 mm, ± weak, short hairs absent or obscured, cottony (and crisped) hairs ± dense, glands absent or obscured, adaxial green to grayish, long hairs sparse to abundant, loosely appressed, 0.5–1.5 mm, short hairs absent or sparse, crisped and/or cottony hairs sparse to common, glands sparse to common.</text>
      <biological_entity id="o634" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o635" from="2" name="quantity" src="d0_s3" to="3" />
        <character char_type="range_value" from="separate" name="arrangement" notes="" src="d0_s3" to="more or less overlapping" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o635" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o636" name="1/6-1/3(-1/2)" name_original="1/6-1/3(-1/2)" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o637" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o638" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character char_type="range_value" constraint="to midvein" constraintid="o639" from="3/4" name="quantity" src="d0_s3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o639" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity constraint="medial" id="o640" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="undivided" value_original="undivided" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o641" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o642" from="5" name="quantity" src="d0_s3" to="8" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" notes="" src="d0_s3" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o642" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o643" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o644" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly; usually" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s3" value="grayish" value_original="grayish" />
      </biological_entity>
      <biological_entity id="o645" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character constraint="on veins" constraintid="o646" is_modifier="false" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="fragility" src="d0_s3" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o646" name="vein" name_original="veins" src="d0_s3" type="structure" />
      <biological_entity id="o647" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o648" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="more or less" name="density" src="d0_s3" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o649" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o650" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="grayish" />
      </biological_entity>
      <biological_entity id="o651" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="abundant" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o652" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o653" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
      <biological_entity id="o654" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
      </biological_entity>
      <relation from="o634" id="r33" name="on" negation="false" src="d0_s3" to="o636" />
      <relation from="o636" id="r34" name="part_of" negation="false" src="d0_s3" to="o637" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 2–4.</text>
      <biological_entity constraint="cauline" id="o655" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (4–) 10–50 (–100) -flowered, congested or elongating in fruit.</text>
      <biological_entity id="o656" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(4-)10-50(-100)-flowered" value_original="(4-)10-50(-100)-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character constraint="in fruit" constraintid="o657" is_modifier="false" name="length" src="d0_s5" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o657" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.2–0.8 cm (proximal to 2 cm).</text>
      <biological_entity id="o658" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s6" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets narrowly elliptic to lanceolate, 2.5–6 mm, lengths ± 2/3 times sepals, margins flat;</text>
      <biological_entity id="o659" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o660" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character constraint="sepal" constraintid="o661" is_modifier="false" modifier="more or less" name="length" src="d0_s7" value="2/3 times sepals" value_original="2/3 times sepals" />
      </biological_entity>
      <biological_entity id="o661" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o662" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 3–5 mm diam.;</text>
      <biological_entity id="o663" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o664" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals 3–6 mm, apex ± acute, abaxial surfaces: venation indistinct, glands absent, sparse, or obscured;</text>
      <biological_entity id="o665" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o666" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o667" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o668" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="count_or_density" src="d0_s9" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, 3–5 × 3–4 mm, lengths ± equal to sepals;</text>
      <biological_entity id="o669" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o670" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o671" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <biological_entity id="o672" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="lengths" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="lengths" src="d0_s10" to="4" to_unit="mm" />
        <character constraint="to sepals" constraintid="o673" is_modifier="false" modifier="more or less" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o673" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>filaments 0.5–2 mm, anthers 0.3–0.5 mm;</text>
      <biological_entity id="o674" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o675" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o676" name="surface" name_original="surfaces" src="d0_s11" type="structure" />
      <biological_entity id="o677" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o678" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 50–80, styles papillate-swollen in proximal 1/2–3/4+, 1–1.2 mm.</text>
      <biological_entity id="o679" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o680" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o681" name="surface" name_original="surfaces" src="d0_s12" type="structure" />
      <biological_entity id="o682" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="80" />
      </biological_entity>
      <biological_entity id="o683" name="style" name_original="styles" src="d0_s12" type="structure">
        <character constraint="in proximal 1/2-3/4+ , 1-1.2 mm" is_modifier="false" name="shape" src="d0_s12" value="papillate-swollen" value_original="papillate-swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1–1.2 mm, smooth to faintly rugose.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 56.</text>
      <biological_entity id="o684" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s13" to="faintly rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o685" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open shortgrass prairie, alkaline bottoms, streamsides in sagebrush, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open shortgrass prairie" />
        <character name="habitat" value="alkaline bottoms" />
        <character name="habitat" value="sagebrush" modifier="streamsides in" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–3400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.W.T., Ont., Que., Sask., Yukon; Alaska, Colo., Idaho, Mich., Minn., Mont., Nebr., N.Mex., N.Dak., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla bipinnatifida is similar to P. litoralis in habit and leaf dissection but has flat, silky epicalyx bractlets and sepals with no evident glands. Vestiture is generally silkier, and the silvery to bicolor leaves are white-cottony abaxially. The two species are sympatric in the plains of Canada, with some intergradation; P. bipinnatifida is also common south to Colorado, where it is found in intermontane meadows and sagebrush flats. Outlying populations occur in Blaine and Custer counties, Idaho, and Duchesne and Piute counties, Utah. Eastern collections from disturbed sites might be adventive.</discussion>
  <discussion>Potentilla missourica Hornemann ex Lindley and P. normalis Besser ex Sprengel are older names for this species; both were rejected against a conserved P. bipinnatifida with designated lectotypes (see J. Soják 2008b).</discussion>
  
</bio:treatment>