<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">289</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Fourreau ex Rydberg" date="1898" rank="genus">drymocallis</taxon_name>
    <taxon_name authority="(Rydberg) Rydberg" date="1898" rank="species">pseudorupestris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pseudorupestris</taxon_name>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus drymocallis;species pseudorupestris;variety pseudorupestris</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100627</other_info_on_name>
  </taxon_identification>
  <number>9a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (1–) 2–4 dm, base (1.5–) 2–3 mm diam., short hairs usually absent or sparse, sometimes moderately abundant.</text>
      <biological_entity id="o24292" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o24293" name="base" name_original="base" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s0" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24294" name="hair" name_original="hairs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s0" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes moderately" name="quantity" src="d0_s0" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves (4–) 7–16 cm, sparsely to moderately hairy (hairs to 1 mm), peglike-glandular, not bristly;</text>
      <biological_entity constraint="basal" id="o24295" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="peglike-glandular" value_original="peglike-glandular" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaflet pairs (2–) 3 (–4);</text>
      <biological_entity id="o24296" name="leaflet" name_original="leaflet" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="4" />
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>terminal leaflet broadly obovate, (1–) 2–3 (–4) × 1.5–3 cm, teeth usually double, (5–) 8–15 per side.</text>
      <biological_entity constraint="terminal" id="o24297" name="leaflet" name_original="leaflet" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24298" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s3" to="8" to_inclusive="false" />
        <character char_type="range_value" constraint="per side" constraintid="o24299" from="8" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o24299" name="side" name_original="side" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 5–20 (proximal to 40) mm, not bristly, short hairs sparse to moderately abundant.</text>
      <biological_entity id="o24300" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s4" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o24301" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s4" to="moderately abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers (5–) 10–40;</text>
      <biological_entity id="o24302" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s5" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>hypanthia and sepals not bristly;</text>
      <biological_entity id="o24303" name="hypanthium" name_original="hypanthia" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
      </biological_entity>
      <biological_entity id="o24304" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>epicalyx bractlets linear to elliptic, 3–6 × 1–2 mm;</text>
      <biological_entity constraint="epicalyx" id="o24305" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="arrangement" src="d0_s7" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sepals 5–7 (–9) mm;</text>
      <biological_entity id="o24306" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals widely overlapping, not red-tinged, broadly obovate, 6–12 × 5–11 mm;</text>
      <biological_entity id="o24307" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s9" value="red-tinged" value_original="red-tinged" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 2–4 mm;</text>
      <biological_entity id="o24308" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles light reddish-brown.</text>
      <biological_entity id="o24309" name="style" name_original="styles" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light reddish-brown" value_original="light reddish-brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky areas, exposed slopes, grasslands on bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky areas" />
        <character name="habitat" value="exposed slopes" />
        <character name="habitat" value="grasslands" constraint="on bluffs" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety pseudorupestris is the tall, large-flowered extreme centered in the northern Rocky Mountains in Montana, extending into Alberta and to the Wind River Range in Wyoming. Some collections from northern Idaho are also tentatively included here. Unlike the other varieties, var. pseudorupestris occurs in grasslands as well as in more rocky habitats. It may intergrade with Drymocallis fissa, though petal color and leaflet number are usually distinct.</discussion>
  
</bio:treatment>