<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bryony Macmillan</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">agrimonieae</taxon_name>
    <taxon_name authority="Mutis ex Linnaeus" date="1771" rank="genus">ACAENA</taxon_name>
    <place_of_publication>
      <publication_title>Mant. Pl.,</publication_title>
      <place_in_publication>145, 200. 1771</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe agrimonieae;genus ACAENA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek akaina, thorn, alluding to barbed spines arising from wall of hypanthium</other_info_on_name>
    <other_info_on_name type="fna_id">100063</other_info_on_name>
  </taxon_identification>
  <number>26.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial, 1–15 dm diam.;</text>
    </statement>
    <statement id="d0_s1">
      <text>from branched, woody caudex or stoloniferous.</text>
      <biological_entity id="o12511" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character char_type="range_value" from="1" from_unit="dm" name="diameter" src="d0_s0" to="15" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–20, creeping and rooting or suberect, basally woody, clad in old leaf-bases, glabrous or sparsely pilose.</text>
      <biological_entity id="o12512" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="20" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="basally" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o12513" name="leaf-base" name_original="leaf-bases" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
      </biological_entity>
      <relation from="o12512" id="r783" name="clad in" negation="false" src="d0_s2" to="o12513" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves persistent, basal and/or cauline;</text>
      <biological_entity id="o12514" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="duration" src="d0_s3" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="basal cauline" id="o12516" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>stipules absent or persistent, adnate to petiole, free portion ovate to obovate, deeply 2–5-fid, or linear, margins entire;</text>
      <biological_entity id="o12517" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="to petiole" constraintid="o12518" is_modifier="false" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o12518" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o12519" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate deeply 2-5-fid or linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate deeply 2-5-fid or linear" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="obovate deeply 2-5-fid or linear" />
      </biological_entity>
      <biological_entity id="o12520" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole present;</text>
      <biological_entity id="o12521" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade spatulate, [1–] 2–15 [–18] cm, herbaceous, leaflets 4–10 per side, opposite, ovate, oblong, or elliptic, margins flat or revolute, incised or pinnatisect, dentate or entire, surfaces hairy or glabrous.</text>
      <biological_entity id="o12522" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s6" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o12523" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o12524" from="4" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o12524" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o12525" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12526" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal or axillary, [4–] 15–100-flowered, spikes or globose heads;</text>
      <biological_entity id="o12527" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="[4-]15-100-flowered" value_original="[4-]15-100-flowered" />
      </biological_entity>
      <biological_entity id="o12528" name="spike" name_original="spikes" src="d0_s7" type="structure" />
      <biological_entity id="o12529" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>peduncles present or absent;</text>
      <biological_entity id="o12530" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts absent;</text>
      <biological_entity id="o12531" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracteoles present.</text>
      <biological_entity id="o12532" name="bracteole" name_original="bracteoles" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pedicels present.</text>
      <biological_entity id="o12533" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 3.5–6 mm diam.;</text>
      <biological_entity id="o12534" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium obtriangular, ovoid, or obconic, 3.5–6 mm diam., spiny [or winged or neither], tomentose or glabrous;</text>
      <biological_entity id="o12535" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtriangular" value_original="obtriangular" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="diameter" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="spiny" value_original="spiny" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals 4–5 [–6], spreading, elliptic or triangular;</text>
      <biological_entity id="o12536" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s14" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals 0;</text>
      <biological_entity id="o12537" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 2–4 [–6], longer than sepals;</text>
      <biological_entity id="o12538" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s16" to="4" />
        <character constraint="than sepals" constraintid="o12539" is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o12539" name="sepal" name_original="sepals" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>carpels 1 [or 2], styles white or red, fimbriate.</text>
      <biological_entity id="o12540" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12541" name="style" name_original="styles" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits achenes, 1 [–2], spindle-shaped;</text>
      <biological_entity constraint="fruits" id="o12542" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="2" />
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spindle--shaped" value_original="spindle--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>hypanthium persistent, enclosing achenes, gray to redbrown, ovoid, oblong, globose, or obconic, 3–6 mm diam., hardened, sometimes winged, distinctly or obscurely angled with a single spine at apex of each of 4 angles, or covered with 10–20 [–50] spines [or without spines], spines usually bearing retrorse hairs or barbs at apices, pubescent or glabrous, eglandular or glandular;</text>
      <biological_entity id="o12543" name="hypanthium" name_original="hypanthium" src="d0_s19" type="structure">
        <character is_modifier="false" name="duration" src="d0_s19" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s19" to="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s19" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s19" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s19" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s19" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s19" value="hardened" value_original="hardened" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s19" value="winged" value_original="winged" />
        <character constraint="with spine" constraintid="o12545" is_modifier="false" modifier="distinctly; obscurely" name="shape" src="d0_s19" value="angled" value_original="angled" />
      </biological_entity>
      <biological_entity id="o12544" name="achene" name_original="achenes" src="d0_s19" type="structure" />
      <biological_entity id="o12545" name="spine" name_original="spine" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o12546" name="apex" name_original="apex" src="d0_s19" type="structure" />
      <biological_entity id="o12547" name="angle" name_original="angles" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o12548" name="spine" name_original="spines" src="d0_s19" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s19" to="50" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s19" to="20" />
      </biological_entity>
      <biological_entity id="o12549" name="spine" name_original="spines" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o12550" name="hair" name_original="hairs" src="d0_s19" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s19" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity id="o12551" name="barb" name_original="barbs" src="d0_s19" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s19" value="retrorse" value_original="retrorse" />
      </biological_entity>
      <biological_entity id="o12552" name="apex" name_original="apices" src="d0_s19" type="structure" />
      <relation from="o12543" id="r784" name="enclosing" negation="false" src="d0_s19" to="o12544" />
      <relation from="o12545" id="r785" name="at" negation="false" src="d0_s19" to="o12546" />
      <relation from="o12546" id="r786" modifier="of" name="consist_of" negation="false" src="d0_s19" to="o12547" />
      <relation from="o12543" id="r787" name="covered with" negation="false" src="d0_s19" to="o12548" />
      <relation from="o12549" id="r788" name="bearing" negation="false" src="d0_s19" to="o12550" />
      <relation from="o12549" id="r789" name="bearing" negation="false" src="d0_s19" to="o12551" />
      <relation from="o12549" id="r790" name="at" negation="false" src="d0_s19" to="o12552" />
    </statement>
    <statement id="d0_s20">
      <text>sepals persistent, spreading.</text>
    </statement>
    <statement id="d0_s21">
      <text>x = 21.</text>
      <biological_entity id="o12553" name="sepal" name_original="sepals" src="d0_s20" type="structure">
        <character is_modifier="false" name="duration" src="d0_s20" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="orientation" src="d0_s20" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="x" id="o12554" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="21" value_original="21" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, Mexico, South America (Argentina, Chile), s Africa (Cape Province), Atlantic Islands (Gough Island, South Georgia, Tristan da Cunha), Indian Ocean Islands (Kerguelen, Prince Edward Island), Pacific Islands (Hawaii, New Guinea, New Zealand), Australia; introduced in Europe (England, Ireland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
        <character name="distribution" value="s Africa (Cape Province)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Gough Island)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (South Georgia)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Tristan da Cunha)" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Kerguelen)" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Prince Edward Island)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Guinea)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value=" in Europe (England)" establishment_means="introduced" />
        <character name="distribution" value=" in Europe (Ireland)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 45 (3 in the flora).</discussion>
  <references>
    <reference>Bitter, F. A. G. [1910–]1911. Die Gattung Acaena. Biblioth. Bot. 74.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences interrupted spikes; fruiting hypanthia bearing 10–20 spines 1–3 mm; plants decumbent to suberect; leaflets pinnatisect.</description>
      <determination>1 Acaena pinnatifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences globose heads; fruiting hypanthia 4-angled, each angle with a spine at apex (arising distally) 7–15 mm; plants creeping or stoloniferous; leaflets dentate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting hypanthia: spines 7–12 mm, without subsidiary spines on angles; stems 1.5–2 mm diam.; leaflet adaxial surfaces bright shining green, smooth, abaxial surfaces glaucescent.</description>
      <determination>2 Acaena novae-zelandiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruiting hypanthia: spines 9–15 mm, with subsidiary spines on angles; stems 2–3 mm diam.; leaflet adaxial surfaces glossy green, rugulose, abaxial surfaces whitish with uneven wax layer.</description>
      <determination>3 Acaena pallida</determination>
    </key_statement>
  </key>
</bio:treatment>