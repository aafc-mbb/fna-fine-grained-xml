<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="de Candolle ex Poiret in J. Lamarck et al." date="unknown" rank="genus">purshia</taxon_name>
    <taxon_name authority="(Pursh) de Candolle" date="1818" rank="species">tridentata</taxon_name>
    <taxon_name authority="(Curran) M. E. Jones" date="1895" rank="variety">glandulosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci., ser.</publication_title>
      <place_in_publication>2, 5: 680. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus purshia;species tridentata;variety glandulosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250100719</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Purshia</taxon_name>
    <taxon_name authority="Curran" date="unknown" rank="species">glandulosa</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 153. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Purshia;species glandulosa</taxon_hierarchy>
  </taxon_identification>
  <number>1b.</number>
  <other_name type="common_name">Desert bitterbrush</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: young long-shoots glabrous or sparsely hirtellous, often stipitate-glandular.</text>
      <biological_entity id="o6168" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o6169" name="long-shoot" name_original="long-shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="young" value_original="young" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, both long and short-shoot leaves winter persistent;</text>
      <biological_entity id="o6170" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="short-shoot" id="o6171" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="season" src="d0_s1" value="winter" value_original="winter" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade obovate to spatulate (when small), coriaceous-thickened, 3 (–5) -lobed, lobes oblong-linear, narrow, central lobe deflexed, 2 lateral ascending, margins strongly revolute, often with large, resiniferous, punctate glands, abaxial surface arachnoid-villous, midvein glabrous, glabrate, or hirtellous, adaxial glabrous, sometimes weakly arachnoid-villous (this often obscured by broad, glabrous midvein in dried leaves) or hirtellous.</text>
      <biological_entity id="o6172" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="spatulate" />
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="coriaceous-thickened" value_original="coriaceous-thickened" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3(-5)-lobed" value_original="3(-5)-lobed" />
      </biological_entity>
      <biological_entity id="o6173" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="oblong-linear" value_original="oblong-linear" />
        <character is_modifier="false" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="central" id="o6174" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="deflexed" value_original="deflexed" />
        <character name="quantity" src="d0_s2" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s2" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o6175" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity id="o6176" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="resiniferous" value_original="resiniferous" />
        <character is_modifier="true" name="coloration_or_relief" src="d0_s2" value="punctate" value_original="punctate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6177" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="arachnoid-villous" value_original="arachnoid-villous" />
      </biological_entity>
      <biological_entity id="o6178" name="midvein" name_original="midvein" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <relation from="o6175" id="r373" modifier="often" name="with" negation="false" src="d0_s2" to="o6176" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 18.</text>
      <biological_entity constraint="adaxial" id="o6179" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes weakly" name="pubescence" src="d0_s2" value="arachnoid-villous" value_original="arachnoid-villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6180" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Juniper-pinyon, Joshua tree woodlands, desert blackbrush scrub, chaparral at desert margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="juniper-pinyon" />
        <character name="habitat" value="joshua tree woodlands" />
        <character name="habitat" value="desert blackbrush scrub" />
        <character name="habitat" value="desert margins" modifier="chaparral at" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(600–)800–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1500" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety glandulosa is a derivative of var. tridentata. Some sterile specimens of var. glandulosa cannot be distinguished from Purshia stansburyana; both taxa sometimes have similar leaves, and hirtellous, more or less stipitate-glandular young stems.</discussion>
  <discussion>G. L. Stebbins (1959) and H. C. Stutz and L. K. Thomas (1963) hypothesized that var. glandulosa is a product of hybridization and introgression between Purshia tridentata and P. stansburyana. Although introgression would explain certain similarities, P. tridentata stands as a distinct species strongly differentiated by ovary-fruit characteristics and dimorphic leaves, which allow it to adapt to a wide range of habitats. Hybrids with P. stansburyana are discussed under the latter species.</discussion>
  
</bio:treatment>