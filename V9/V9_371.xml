<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens" date="1959" rank="section">unguiculatae</taxon_name>
    <taxon_name authority="(Rydberg) Rydberg in N. L. Britton et al." date="1908" rank="species">sericoleuca</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>22: 284. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section unguiculatae;species sericoleuca;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100277</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">sericoleuca</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. N. Amer. Potentilleae,</publication_title>
      <place_in_publication>144, plate 85. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Horkelia;species sericoleuca</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(Rydberg) J. T. Howell" date="unknown" rank="species">sericoleuca</taxon_name>
    <taxon_hierarchy>genus Potentilla;species sericoleuca</taxon_hierarchy>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Plumas ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants silvery to grayish green;</text>
      <biological_entity id="o24310" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="silvery" name="coloration" src="d0_s0" to="grayish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>glands usually sparse, sometimes abundant.</text>
      <biological_entity id="o24311" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="count_or_density" src="d0_s1" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s1" value="abundant" value_original="abundant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent to ascending, 1.5–4.5 dm.</text>
      <biological_entity id="o24312" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="4.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 10–20 (–30) cm;</text>
      <biological_entity constraint="basal" id="o24313" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheathing base densely strigose abaxially;</text>
      <biological_entity id="o24314" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules absent;</text>
      <biological_entity id="o24315" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 2–6 (–10) cm, hairs abundant, usually spreading, 1–4 mm;</text>
      <biological_entity id="o24316" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24317" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>leaflets 20–35 per side, loosely overlapping, 3–15 mm, lobes 0–4, oblanceolate to elliptic, hairs abundant, spreading to ascending, (0.5–) 1–3 (–4) mm.</text>
      <biological_entity id="o24318" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o24319" from="20" name="quantity" src="d0_s7" to="35" />
        <character is_modifier="false" modifier="loosely" name="arrangement" notes="" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24319" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o24320" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="elliptic" />
      </biological_entity>
      <biological_entity id="o24321" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="abundant" value_original="abundant" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 3–8 (–10).</text>
      <biological_entity constraint="cauline" id="o24322" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences 20–120-flowered, (2–) 4–14 cm diam., flowers mostly arranged in several to many tight glomerules of 5–10 flowers.</text>
      <biological_entity id="o24323" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="20-120-flowered" value_original="20-120-flowered" />
        <character char_type="range_value" from="2" from_unit="cm" name="diameter" src="d0_s9" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s9" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24324" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character constraint="in glomerules" constraintid="o24325" is_modifier="false" modifier="mostly" name="arrangement" src="d0_s9" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o24325" name="glomerule" name_original="glomerules" src="d0_s9" type="structure">
        <character char_type="range_value" from="several" is_modifier="true" name="quantity" src="d0_s9" to="many" />
        <character is_modifier="true" name="arrangement_or_density" src="d0_s9" value="tight" value_original="tight" />
      </biological_entity>
      <biological_entity id="o24326" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <relation from="o24325" id="r1578" name="part_of" negation="false" src="d0_s9" to="o24326" />
    </statement>
    <statement id="d0_s10">
      <text>Pedicels 1–3 (–12) mm.</text>
      <biological_entity id="o24327" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 10–15 mm diam.;</text>
      <biological_entity id="o24328" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>epicalyx bractlets narrowly lanceolate to narrowly elliptic, (1.5–) 2–2.5 (–3) mm;</text>
      <biological_entity constraint="epicalyx" id="o24329" name="bractlet" name_original="bractlets" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s12" to="narrowly elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypanthium campanulate to shallowly turbinate, 1.5–3 × 2.5–4.5 (–5) mm, often nearly as deep as wide;</text>
      <biological_entity id="o24330" name="hypanthium" name_original="hypanthium" src="d0_s13" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s13" to="shallowly turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="often nearly; nearly" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" modifier="often nearly; nearly" name="atypical_width" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="as-deep-as wide" from="2.5" from_unit="mm" modifier="often nearly; nearly" name="width" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sepals sometimes purple-suffused, 3–5.5 mm, acute to acuminate;</text>
      <biological_entity id="o24331" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="purple-suffused" value_original="purple-suffused" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s14" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, broadly spatulate to broadly obovate or obcordate, 4–7 mm;</text>
      <biological_entity id="o24332" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s15" to="broadly obovate or obcordate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stamens 20, filaments filiform, 1.5–3 mm, anthers white to cream, 0.5–0.7 mm;</text>
      <biological_entity id="o24333" name="stamen" name_original="stamens" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
      </biological_entity>
      <biological_entity id="o24334" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24335" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="cream" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>carpels 2–7, styles 2.5–4 mm.</text>
      <biological_entity id="o24336" name="carpel" name_original="carpels" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s17" to="7" />
      </biological_entity>
      <biological_entity id="o24337" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes brown, 2–3 mm. 2n = 28.</text>
      <biological_entity id="o24338" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24339" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry gravelly meadows, margins of seeps, usually on vernally saturated volcanic soil, in sagebrush and grass communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry gravelly meadows" />
        <character name="habitat" value="margins" constraint="of seeps" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="vernally" modifier="usually on" />
        <character name="habitat" value="volcanic soil" modifier="saturated" constraint="in sagebrush and grass communities , conifer woodlands" />
        <character name="habitat" value="sagebrush" modifier="in" />
        <character name="habitat" value="grass communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Ivesia sericoleuca is known from valleys and flats in the northern Sierra Nevada. Many historic collections were identified and distributed as I. unguiculata, and circumscriptions prior to 1962 include I. aperta (hence reports from Nevada).</discussion>
  <discussion>Hairs are usually dense in plants of Ivesia sericoleuca, such that the leaves, and occasionally the stems and branches, are silvery gray, especially in Sierra Valley and the Feather River drainage. Plants in the Truckee River drainage tend to be less hairy with redder stems, less glomerate inflorescences, shallower hypanthia, and more conspicuous glandularity.</discussion>
  <discussion>As mentioned by J. T. Howell (1962), the chromosome count given for Ivesia sericoleuca by P. A. Munz (1959) most likely was based on a collection of I. aperta var. aperta. The chromosome count given here is instead based on Kruckeberg 3665, originally distributed as I. pickeringii.</discussion>
  
</bio:treatment>