<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1825" rank="tribe">amygdaleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prunus</taxon_name>
    <taxon_name authority="Ehrhart" date="unknown" rank="species">serotina</taxon_name>
    <place_of_publication>
      <publication_title>Gartenkalender</publication_title>
      <place_in_publication>3: 285. 1784</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe amygdaleae;genus prunus;species serotina</taxon_hierarchy>
    <other_info_on_name type="fna_id">242417060</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Black or rum cherry</other_name>
  <other_name type="common_name">cerisier tardif ou d’automne</other_name>
  <other_name type="common_name">merisier</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, not suckering, 40–400 dm, not thorny.</text>
      <biological_entity id="o11860" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="400" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="suckering" value_original="suckering" />
        <character char_type="range_value" from="40" from_unit="dm" name="some_measurement" src="d0_s0" to="400" to_unit="dm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="thorny" value_original="thorny" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Twigs with terminal end buds, glabrous or hairy.</text>
      <biological_entity id="o11862" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="end" id="o11863" name="bud" name_original="buds" src="d0_s1" type="structure" constraint_original="terminal end" />
      <relation from="o11862" id="r738" name="with" negation="false" src="d0_s1" to="o11863" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous;</text>
      <biological_entity id="o11864" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 2–23 (–30) mm, glabrous or sparsely to densely hairy, usually glandular distally or at petiole-blade junction, glands 1–6;</text>
      <biological_entity id="o11865" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="23" to_unit="mm" />
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s3" to="sparsely densely hairy" />
        <character is_modifier="false" modifier="usually; distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="at petiole-blade junction" value_original="at petiole-blade junction" />
      </biological_entity>
      <biological_entity constraint="petiole-blade" id="o11866" name="junction" name_original="junction" src="d0_s3" type="structure" />
      <biological_entity id="o11867" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <relation from="o11865" id="r739" name="at" negation="false" src="d0_s3" to="o11866" />
    </statement>
    <statement id="d0_s4">
      <text>blade usually narrowly elliptic, oblongelliptic, or obovate, sometimes lanceolate, rarely ovate, 2–13.5 × 1.1–6.5 cm, base cuneate to rounded, margins crenulate-serrulate to serrate, teeth incurved or appressed, sharp or blunt, glandular or callus-tipped, apex usually acute to acuminate, sometimes obtuse, rounded to emarginate in var. alabamensis, lateral-veins 15–30 per side, flush abaxially, abaxial surface usually densely hairy along midribs proximally, sometimes glabrous or sparsely hairy, adaxial glabrous.</text>
      <biological_entity id="o11868" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="13.5" to_unit="cm" />
        <character char_type="range_value" from="1.1" from_unit="cm" name="width" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11869" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o11870" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="crenulate-serrulate" name="architecture_or_shape" src="d0_s4" to="serrate" />
      </biological_entity>
      <biological_entity id="o11871" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="incurved" value_original="incurved" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="callus-tipped" value_original="callus-tipped" />
      </biological_entity>
      <biological_entity id="o11872" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually acute" name="shape" src="d0_s4" to="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" constraint="in var" from="rounded" name="shape" src="d0_s4" to="emarginate" />
      </biological_entity>
      <biological_entity id="o11873" name="lateral-vein" name_original="lateral-veins" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o11874" from="15" name="quantity" src="d0_s4" to="30" />
        <character is_modifier="false" modifier="abaxially" name="prominence" notes="" src="d0_s4" value="flush" value_original="flush" />
      </biological_entity>
      <biological_entity id="o11874" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o11875" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character constraint="along midribs" constraintid="o11876" is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o11876" name="midrib" name_original="midribs" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o11877" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences 18–55 (–90) -flowered, racemes;</text>
      <biological_entity id="o11878" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="18-55(-90)-flowered" value_original="18-55(-90)-flowered" />
      </biological_entity>
      <biological_entity id="o11879" name="raceme" name_original="racemes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>central axes (25–) 35–160 mm, leafy at bases.</text>
      <biological_entity constraint="central" id="o11880" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="35" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s6" to="160" to_unit="mm" />
        <character constraint="at bases" constraintid="o11881" is_modifier="false" name="architecture" src="d0_s6" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o11881" name="base" name_original="bases" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 1–10 mm, glabrous or hairy.</text>
      <biological_entity id="o11882" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers blooming after leaf emergence;</text>
      <biological_entity id="o11883" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character constraint="after leaf" constraintid="o11884" is_modifier="false" name="life_cycle" src="d0_s8" value="blooming" value_original="blooming" />
      </biological_entity>
      <biological_entity id="o11884" name="leaf" name_original="leaf" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>hypanthium cupulate, 1.5–3 mm, glabrous externally;</text>
      <biological_entity id="o11885" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="externally" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals erect-spreading to reflexed, semicircular, 0.5–1.5 mm, margins usually entire, rarely glandular-toothed, rarely ciliate, surfaces glabrous;</text>
      <biological_entity id="o11886" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect-spreading" name="orientation" src="d0_s10" to="reflexed" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s10" value="semicircular" value_original="semicircular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11887" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="glandular-toothed" value_original="glandular-toothed" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11888" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white, obovate to suborbiculate, 2–4 mm;</text>
      <biological_entity id="o11889" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="suborbiculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries glabrous.</text>
      <biological_entity id="o11890" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Drupes dark purple to nearly black, globose, 5–10 [–25] mm, glabrous;</text>
      <biological_entity id="o11891" name="drupe" name_original="drupes" src="d0_s13" type="structure">
        <character char_type="range_value" from="dark purple" name="coloration" src="d0_s13" to="nearly black" />
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium persistent;</text>
      <biological_entity id="o11892" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>mesocarps fleshy;</text>
      <biological_entity id="o11893" name="mesocarp" name_original="mesocarps" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stones subglobose, not flattened.</text>
      <biological_entity id="o11894" name="stone" name_original="stones" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., N.S., Ont., Que.; Ala., Ariz., Ark., Conn., D.C., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.H., N.J., N.Mex., N.Y., Nebr., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., Vt., W.Va., Wash., Wis.; Mexico, Central America, South America; introduced in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="in Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 4 (3 in the flora).</discussion>
  <discussion>Variety capuli (Cavanilles) Hatusima [subsp. capuli (Cavanilles) McVaugh] is known from Mexico (Chiapas, Jalisco, Distrito Federal, México, Puebla), Central America, and South America (Argentina, Bolivia, Colombia, Ecuador, Peru, Venezuela). It is distinguished from var. serotina by lanceolate to ovate-lanceolate leaves with decurrent bases (versus elliptic to obovate with obtuse to cuneate bases) and a long (to 2 cm), thin (1 mm) petiole; inflorescences tend to be subtended by 3 or 4 leaves [versus 2 (or 3), reduced in size], with a rachis relatively longer (greater than 15 cm), thicker, and flexuous compared to other varieties, and shorter pedicels (3–5 mm versus 5–10 mm, in var. serotina).</discussion>
  <discussion>The characters given in the key usually allow easy separation of Prunus serotina from P. virginiana; some specimens from Arizona and southern Utah are difficult to determine. In that area, it is not uncommon for the sepals of P. virginiana flowers to lack the glandular teeth that are common elsewhere and thus mimic those of P. serotina. Variety rufula has thicker leaves that are also much shinier on both surfaces than those of P. virginiana and shorter petioles (2–15 mm versus 4–27 mm).</discussion>
  <discussion>Compounding the confusion with chokecherry is a nomenclatural conundrum in which the epithet virginiana has been used for both chokecherry (the next species) and for black cherry (this species). The status of the two names Prunus serotina and P. virginiana was discussed by K. N. Gandhi et al. (2009), who proposed the conservation of both names.</discussion>
  <discussion>Of our native Prunus species only P. serotina grows large enough to produce commercial lumber, which is highly prized for its fine grain and rich, warm, reddish brown color.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades elliptic to obovate, abaxial surfaces sparsely hairy, apices usually obtuse, rounded, or emarginate, sometimes abruptly acute or short-acuminate.</description>
      <determination>6b Prunus serotina var. alabamensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually elliptic, lanceolate, or oblong, rarely obovate or ovate, abaxial surfaces glabrous or midribs hairy, apices acute to acuminate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 4–13.5 cm, membranous or slightly leathery; petioles (7–)10–23(–30) mm; e North America.</description>
      <determination>6a Prunus serotina var. serotina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 2–5.2(–7.4) cm, leathery; petioles (2–)4–10(–15) mm; Arizona to w Texas.</description>
      <determination>6c Prunus serotina var. rufula</determination>
    </key_statement>
  </key>
</bio:treatment>