<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="illustration_page">102</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="unknown" rank="tribe">roseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">rosa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Rosa</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="species">nitida</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.,</publication_title>
      <place_in_publication>544. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe roseae;genus rosa;subgenus rosa;section rosa;species nitida;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100420</other_info_on_name>
  </taxon_identification>
  <number>21.</number>
  <other_name type="common_name">Red-spined or shining rose</other_name>
  <other_name type="common_name">rosier brillant</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or subshrubs, some forming clusters.</text>
      <biological_entity id="o4708" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="cluster" value_original="cluster" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems spreading, procumbent, or erect, 2–8 (–10) dm, openly branched;</text>
      <biological_entity id="o4710" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="8" to_unit="dm" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark dark-brown, glabrous;</text>
    </statement>
    <statement id="d0_s3">
      <text>infrastipular prickles sometimes present, paired, erect or declined, rarely curved, subulate, 6 × 6 mm, base glabrous, internodal prickles usually dense, mixed with aciculi, erect or declined, subulate, terete, 2–7 × 2 mm, eglandular.</text>
      <biological_entity id="o4711" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4712" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="declined" value_original="declined" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character name="length" src="d0_s3" unit="mm" value="6" value_original="6" />
        <character name="width" src="d0_s3" unit="mm" value="6" value_original="6" />
      </biological_entity>
      <biological_entity id="o4713" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="internodal" id="o4714" name="prickle" name_original="prickles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="with aciculi" constraintid="o4715" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="declined" value_original="declined" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o4715" name="aciculus" name_original="aciculi" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves 3–10 cm;</text>
      <biological_entity id="o4716" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stipules 10–14 × 4–5.5 mm, auricles flared, 4–5 mm, margins entire or glandular-serrate, eglandular or gland-tipped, surfaces glabrous, eglandular;</text>
      <biological_entity id="o4717" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4718" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flared" value_original="flared" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4719" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="glandular-serrate" value_original="glandular-serrate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o4720" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole and rachis with pricklets, glabrous, rarely puberulent, eglandular;</text>
      <biological_entity id="o4721" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o4722" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o4723" name="pricklet" name_original="pricklets" src="d0_s6" type="structure" />
      <relation from="o4721" id="r287" name="with" negation="false" src="d0_s6" to="o4723" />
      <relation from="o4722" id="r288" name="with" negation="false" src="d0_s6" to="o4723" />
    </statement>
    <statement id="d0_s7">
      <text>leaflets (5–) 7–9, terminal: petiolule 3–5 (–9) mm, blade narrowly elliptic or lanceolate, rarely ovate, 13–27 (–40) × 7–17 mm, membranous, margins 1-serrate, eglandular, rarely gland-tipped, teeth 12–17 per side, sometimes gland-tipped, apex acute to subacuminate, abaxial surfaces green, glabrous or pubescent, eglandular, adaxial deep green, purplish red in fall, lustrous, glabrous.</text>
      <biological_entity id="o4724" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s7" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="9" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4725" name="petiolule" name_original="petiolule" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4726" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="27" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="40" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s7" to="27" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="17" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o4727" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="1-serrate" value_original="1-serrate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o4728" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o4729" from="12" name="quantity" src="d0_s7" to="17" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s7" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <biological_entity id="o4729" name="side" name_original="side" src="d0_s7" type="structure" />
      <biological_entity id="o4730" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="subacuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4731" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4732" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
        <character constraint="in fall" constraintid="o4733" is_modifier="false" name="coloration" src="d0_s7" value="purplish red" value_original="purplish red" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s7" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4733" name="fall" name_original="fall" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences corymbs, 1–3-flowered.</text>
      <biological_entity constraint="inflorescences" id="o4734" name="corymb" name_original="corymbs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-3-flowered" value_original="1-3-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect or recurved (as hips mature), slender, 13–25 mm, glabrous, densely long-stipitate-glandular;</text>
      <biological_entity id="o4735" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="long-stipitate-glandular" value_original="long-stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bracts 1 or 2, lanceolate, 9–18 × 3.5–7 mm, margins entire, rarely serrate, few gland-tipped, apically cleft 2–3.5 mm, surfaces glabrous, eglandular.</text>
      <biological_entity id="o4736" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s10" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s10" to="18" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4737" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="gland-tipped" value_original="gland-tipped" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s10" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4738" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 4–5 cm diam.;</text>
      <biological_entity id="o4739" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="diameter" src="d0_s11" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypanthium globose or subglobose, 3–4 × 3–5 mm, glabrous, densely stipitate-glandular, neck absent;</text>
      <biological_entity id="o4740" name="hypanthium" name_original="hypanthium" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4741" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sepals spreading to reflexed, lanceolate, 14–22 × 2–3 mm, tip 5–10 × 0.5–1 mm, margins entire, abaxial surfaces glabrous, densely stipitate-glandular;</text>
      <biological_entity id="o4742" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s13" to="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s13" to="22" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4743" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4744" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4745" name="surface" name_original="surfaces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals single, pink to rose, 19–23 × 20–23 mm;</text>
      <biological_entity id="o4746" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s14" to="rose" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s14" to="23" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s14" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>carpels 20–24, styles exsert 0.5–1 mm beyond stylar orifice (1.5–2 mm diam.) of hypanthial disc (4 mm diam.).</text>
      <biological_entity id="o4747" name="carpel" name_original="carpels" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s15" to="24" />
      </biological_entity>
      <biological_entity id="o4748" name="style" name_original="styles" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exsert" value_original="exsert" />
        <character char_type="range_value" constraint="beyond stylar, orifice" constraintid="o4749, o4750" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4749" name="stylar" name_original="stylar" src="d0_s15" type="structure" />
      <biological_entity id="o4750" name="orifice" name_original="orifice" src="d0_s15" type="structure" />
      <biological_entity id="o4751" name="hypanthial" name_original="hypanthial" src="d0_s15" type="structure" />
      <biological_entity id="o4752" name="disc" name_original="disc" src="d0_s15" type="structure" />
      <relation from="o4749" id="r289" name="part_of" negation="false" src="d0_s15" to="o4751" />
      <relation from="o4749" id="r290" name="part_of" negation="false" src="d0_s15" to="o4752" />
      <relation from="o4750" id="r291" name="part_of" negation="false" src="d0_s15" to="o4751" />
      <relation from="o4750" id="r292" name="part_of" negation="false" src="d0_s15" to="o4752" />
    </statement>
    <statement id="d0_s16">
      <text>Hips red to dark red, globose to depressed-globose, 8–10 × 7–10 mm, fleshy, glabrous, densely stipitate-glandular, neck absent;</text>
      <biological_entity id="o4753" name="hip" name_original="hips" src="d0_s16" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s16" to="dark red" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s16" to="depressed-globose" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s16" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s16" to="10" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s16" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4754" name="neck" name_original="neck" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>sepals deciduous, erect or spreading to reflexed.</text>
      <biological_entity id="o4755" name="sepal" name_original="sepals" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="deciduous" value_original="deciduous" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s17" to="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Achenes basal, 10–14, tan, darkening with age, 2.6–2.8 × 1.6–1.8 mm. 2n = 14.</text>
      <biological_entity id="o4756" name="achene" name_original="achenes" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="basal" value_original="basal" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s18" to="14" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="tan" value_original="tan" />
        <character constraint="with age" constraintid="o4757" is_modifier="false" name="coloration" src="d0_s18" value="darkening" value_original="darkening" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="length" notes="" src="d0_s18" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" notes="" src="d0_s18" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4757" name="age" name_original="age" src="d0_s18" type="structure" />
      <biological_entity constraint="2n" id="o4758" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet edges of spruce woods, bogs, swamps, rocky ledges, wet thickets, margins of ponds and streams, shores, rocky and grassy hills and bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet edges" constraint="of spruce woods , bogs , swamps , rocky ledges , wet thickets , margins of ponds and streams , shores , rocky and grassy hills and bluffs" />
        <character name="habitat" value="spruce woods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="rocky ledges" />
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="margins" constraint="of ponds and streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="shores" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="grassy hills" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Conn., Maine, Mass., N.H., N.Y., R.I., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rosa nitida is typical of the eastern Canadian Provincial Element (S. P. McLaughlin 2007) and is often found near or intermixed with R. virginiana. Putative hybrids between R. nitida (2x) and both R. palustris (2x) and R. virginiana (4x) are reported from Nova Scotia and New England. Because the armature of both R. palustris and R. virginiana is predominately or exclusively of infrastipular prickles, hybridity might explain the infrequent occurrences of R. nitida with infrastipular prickles. In a sample of 23 sheets of R. nitida having abundant prickles, two possess infrastipular prickles.</discussion>
  <discussion>The most diagnostic feature of these shrubs with weak, sprawling stems is their distal branches, which are densely covered with mixed red to reddish purple internodal prickles and aciculi with infrastipular prickles relatively few.</discussion>
  
</bio:treatment>