<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">228</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="unknown" rank="genus">ivesia</taxon_name>
    <taxon_name authority="(Rydberg) O. Stevens in N. L. Britton et al." date="1959" rank="section">SETOSAE</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="species">baileyi</taxon_name>
    <taxon_name authority="(A. Nelson &amp; J. F. Macbride) Ertter" date="1989" rank="variety">beneolens</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>14: 236. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus ivesia;section setosae;species baileyi;variety beneolens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100670</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Horkelia</taxon_name>
    <taxon_name authority="A. Nelson &amp; J. F. Macbride" date="unknown" rank="species">beneolens</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>55: 374. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Horkelia;species beneolens</taxon_hierarchy>
  </taxon_identification>
  <number>6b.</number>
  <other_name type="common_name">Owyhee ivesia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Basal leaves: sheathing base sparsely glandular abaxially, otherwise glabrous.</text>
      <biological_entity constraint="basal" id="o27719" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o27720" name="base" name_original="base" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s0" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" modifier="sparsely; abaxially" name="architecture_or_function_or_pubescence" src="d0_s0" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pedicels 5–6+ mm at flowering, to 15 (–30) mm in fruit.</text>
      <biological_entity id="o27721" name="pedicel" name_original="pedicels" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o27722" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o27722" name="fruit" name_original="fruit" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers 5–40, 7–10 mm diam.;</text>
      <biological_entity id="o27723" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="40" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>epicalyx bractlets narrowly lanceolate, 0.8–1 mm, usually less than 1/2 as long as sepals;</text>
      <biological_entity constraint="epicalyx" id="o27724" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as sepals" constraintid="o27725" from="0" modifier="usually" name="quantity" src="d0_s3" to="1/2" />
      </biological_entity>
      <biological_entity id="o27725" name="sepal" name_original="sepals" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>hypanthium interior pale green or cream to maroon;</text>
      <biological_entity id="o27726" name="hypanthium" name_original="hypanthium" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="interior" value_original="interior" />
        <character char_type="range_value" from="cream" name="coloration" src="d0_s4" to="maroon" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals (1.2–) 1.5–2.5 mm;</text>
      <biological_entity id="o27727" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white;</text>
      <biological_entity id="o27728" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments white, anther margins reddish.</text>
      <biological_entity id="o27729" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="anther" id="o27730" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes ± 2 mm, rugose.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 28.</text>
      <biological_entity id="o27731" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="relief" src="d0_s8" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27732" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices on north-facing cliffs or similarly protected sites in canyons and rocky outcrops mainly of volcanic origin, in sagebrush communities, conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="on north-facing cliffs or similarly protected sites in canyons and rocky outcrops mainly" />
        <character name="habitat" value="north-facing cliffs" />
        <character name="habitat" value="protected sites" modifier="similarly" constraint="in canyons and rocky outcrops mainly" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="volcanic origin" modifier="mainly" constraint="in sagebrush communities , conifer woodlands" />
        <character name="habitat" value="sagebrush communities" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety beneolens occurs from Harney and Malheur counties, Oregon, to Modoc County, California, and to Elmore, Owyhee, and Twin Falls counties, Idaho, and Elko and Humboldt counties, Nevada. Plants are particularly common on the vertical sides of the river canyons that cut through the Owyhee Plateau. The variety also barely enters the Idaho Batholith on volcanic intrusions along the South Fork of the Boise River.</discussion>
  
</bio:treatment>