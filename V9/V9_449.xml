<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">fragaria</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1768" rank="species">chiloensis</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Fragaria no. 4. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus fragaria;species chiloensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">242428073</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Fragaria</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">vesca</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="variety">chiloensis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 495. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Fragaria;species vesca;variety chiloensis</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Chilean or beach or sand strawberry</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants hermaphroditic or unisexual.</text>
      <biological_entity id="o7801" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="hermaphroditic" value_original="hermaphroditic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="unisexual" value_original="unisexual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves dark or dull green, not glaucous, thick, leathery, strongly reticulately veined abaxially, terminal tooth of terminal leaflet usually shorter than adjacent teeth, abaxial surface densely silky, adaxial glabrous, shiny.</text>
      <biological_entity id="o7802" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark" value_original="dark" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
        <character is_modifier="false" modifier="strongly reticulately; abaxially" name="architecture" src="d0_s1" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o7803" name="tooth" name_original="tooth" src="d0_s1" type="structure">
        <character constraint="than adjacent teeth" constraintid="o7805" is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o7804" name="leaflet" name_original="leaflet" src="d0_s1" type="structure" />
      <biological_entity id="o7805" name="tooth" name_original="teeth" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7806" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="silky" value_original="silky" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7807" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s1" value="shiny" value_original="shiny" />
      </biological_entity>
      <relation from="o7803" id="r486" name="part_of" negation="false" src="d0_s1" to="o7804" />
    </statement>
    <statement id="d0_s2">
      <text>Flowers bisexual, pistillate or staminate (plants dioecious or trioecious);</text>
      <biological_entity id="o7808" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s2" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hypanthium 14.5–27.8 mm diam.;</text>
      <biological_entity id="o7809" name="hypanthium" name_original="hypanthium" src="d0_s3" type="structure">
        <character char_type="range_value" from="14.5" from_unit="mm" name="diameter" src="d0_s3" to="27.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals 5 (or 6), obovate to widely depressed obovate, margins distinct or overlapping.</text>
      <biological_entity id="o7810" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="widely depressed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o7811" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Achenes in shallow pits or partially embedded, reddish-brown to dark-brown, 1.4–2 mm;</text>
      <biological_entity id="o7812" name="achene" name_original="achenes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="partially" name="position_relational" src="d0_s5" value="embedded" value_original="embedded" />
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s5" to="dark-brown" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7813" name="pit" name_original="pits" src="d0_s5" type="structure">
        <character is_modifier="true" name="depth" src="d0_s5" value="shallow" value_original="shallow" />
      </biological_entity>
      <relation from="o7812" id="r487" name="in" negation="false" src="d0_s5" to="o7813" />
    </statement>
    <statement id="d0_s6">
      <text>bractlets and sepals clasping;</text>
      <biological_entity id="o7814" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o7815" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>torus not easily separated from hypanthium.</text>
      <biological_entity id="o7816" name="torus" name_original="torus" src="d0_s7" type="structure">
        <character constraint="from hypanthium" constraintid="o7817" is_modifier="false" modifier="not easily" name="arrangement" src="d0_s7" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o7817" name="hypanthium" name_original="hypanthium" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Subspecies 4 (2 in the flora).</discussion>
  <discussion>Fragaria chiloensis was first described from cultivated plants that had been selected in Chile at least 400 years ago and brought to Europe in 1714. Subsequently, it was realized that wild-growing plants of this species exist in North America and South America. The typical subspecies is restricted to South America; subspp. lucida and pacifica are present in North America, and the latter in Hawaii as well.</discussion>
  <discussion>The proposal by K. E. Hokanson et al. (2006) to classify subsp. lucida and subsp. pacifica as forms of one North American subspecies seems inappropriate.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stolons, petioles, peduncles, and pedicels usually appressed ascending-hairy, sometimes almost glabrous.</description>
      <determination>3a Fragaria chiloensis subsp. lucida</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stolons, petioles, peduncles, and pedicels spreading-hairy (usually densely so).</description>
      <determination>3b Fragaria chiloensis subsp. pacifica</determination>
    </key_statement>
  </key>
</bio:treatment>