<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James Henrickson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">394</other_info_on_meta>
    <other_info_on_meta type="mention_page">343</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">amygdaloideae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1908" rank="tribe">sorbarieae</taxon_name>
    <taxon_name authority="(Porter ex W. H. Brewer &amp; S. Watson) Maximowicz" date="1879" rank="genus">CHAMAEBATIARIA</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Peterburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 225. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe sorbarieae;genus CHAMAEBATIARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Chamaebatia and Latin - aria, connection, alluding to resemblance</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">106416</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Spiraea</taxon_name>
    <taxon_name authority="Porter ex W. H. Brewer &amp; S. Watson" date="unknown" rank="section">Chamaebatiaria</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 170. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Spiraea;section Chamaebatiaria</taxon_hierarchy>
  </taxon_identification>
  <number>42.</number>
  <other_name type="common_name">Fern bush</other_name>
  <other_name type="common_name">desert sweet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, erect to widely spreading, 6–20 dm, herbage strongly aromatic.</text>
      <biological_entity id="o18009" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="widely spreading" />
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s0" to="20" to_unit="dm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity id="o18010" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="strongly" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, ascending-spreading, strongly branched from base and above;</text>
      <biological_entity id="o18011" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending-spreading" value_original="ascending-spreading" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bark russet, with age gray, smooth;</text>
      <biological_entity id="o18012" name="bark" name_original="bark" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="russet" value_original="russet" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o18013" name="age" name_original="age" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray" value_original="gray" />
      </biological_entity>
      <relation from="o18012" id="r1171" name="with" negation="false" src="d0_s2" to="o18013" />
    </statement>
    <statement id="d0_s3">
      <text>long-shoots present;</text>
    </statement>
    <statement id="d0_s4">
      <text>strongly stipitate-glandular, stipitate-stellate, hirtellous, sometimes strongly glutinous.</text>
      <biological_entity id="o18014" name="long-shoot" name_original="long-shoots" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="strongly" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="stipitate-stellate" value_original="stipitate-stellate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="sometimes strongly" name="coating" src="d0_s4" value="glutinous" value_original="glutinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves persistent, cauline, alternate, 1 (–2) -pinnate-pinnatifid;</text>
      <biological_entity id="o18015" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o18016" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="1(-2)-pinnate-pinnatifid" value_original="1(-2)-pinnate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>stipules free, subulate to oblanceolate, margins entire;</text>
      <biological_entity id="o18017" name="stipule" name_original="stipules" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s6" value="free" value_original="free" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s6" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o18018" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole present;</text>
      <biological_entity id="o18019" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblong-ovate, 2.6–7 (–9.6) cm, herbaceous, pinnae 13–25 per side, ascending, opposite to alternate on rachises, narrow, deeply divided into obovate segments, each segment decurrent on rachilla on proximal base, margins entire or secondarily toothed-lobed, surfaces glabrous or stellate-hairy, glandular.</text>
      <biological_entity id="o18020" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="9.6" to_unit="cm" />
        <character char_type="range_value" from="2.6" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o18021" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o18022" from="13" name="quantity" src="d0_s8" to="25" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" constraint="on rachises" constraintid="o18023" from="opposite" name="arrangement" src="d0_s8" to="alternate" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s8" value="narrow" value_original="narrow" />
        <character constraint="into segments" constraintid="o18024" is_modifier="false" modifier="deeply" name="shape" src="d0_s8" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o18022" name="side" name_original="side" src="d0_s8" type="structure" />
      <biological_entity id="o18023" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o18024" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o18025" name="segment" name_original="segment" src="d0_s8" type="structure">
        <character constraint="on rachilla" constraintid="o18026" is_modifier="false" name="shape" src="d0_s8" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o18026" name="rachillum" name_original="rachilla" src="d0_s8" type="structure" />
      <biological_entity constraint="proximal" id="o18027" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o18028" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="secondarily" name="shape" src="d0_s8" value="toothed-lobed" value_original="toothed-lobed" />
      </biological_entity>
      <biological_entity id="o18029" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-hairy" value_original="stellate-hairy" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <relation from="o18026" id="r1172" name="on" negation="false" src="d0_s8" to="o18027" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal on long-shoots of season, 20–400-flowered, panicles narrowly ovoid, lateral branches leafy at bases;</text>
      <biological_entity id="o18030" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character constraint="on long-shoots" constraintid="o18031" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="20-400-flowered" value_original="20-400-flowered" />
      </biological_entity>
      <biological_entity id="o18031" name="long-shoot" name_original="long-shoots" src="d0_s9" type="structure" />
      <biological_entity id="o18032" name="season" name_original="season" src="d0_s9" type="structure" />
      <biological_entity id="o18033" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o18034" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character constraint="at bases" constraintid="o18035" is_modifier="false" name="architecture" src="d0_s9" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o18035" name="base" name_original="bases" src="d0_s9" type="structure" />
      <relation from="o18031" id="r1173" name="part_of" negation="false" src="d0_s9" to="o18032" />
    </statement>
    <statement id="d0_s10">
      <text>bracts present, proximal bracts of reduced leaves, distal bracts lanceolate, pinnate-lacerate, or entire;</text>
      <biological_entity id="o18036" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18037" name="bract" name_original="bracts" src="d0_s10" type="structure" />
      <biological_entity id="o18038" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18039" name="bract" name_original="bracts" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pinnate-lacerate" value_original="pinnate-lacerate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s10" value="pinnate-lacerate" value_original="pinnate-lacerate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o18037" id="r1174" name="part_of" negation="false" src="d0_s10" to="o18038" />
    </statement>
    <statement id="d0_s11">
      <text>bracteoles present.</text>
      <biological_entity id="o18040" name="bracteole" name_original="bracteoles" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels present.</text>
      <biological_entity id="o18041" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Flowers 10–13 mm diam.;</text>
      <biological_entity id="o18042" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>hypanthium funnelform, to 3–4 mm diam.;</text>
      <biological_entity id="o18043" name="hypanthium" name_original="hypanthium" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>sepals 5, spreading, ovate-deltate;</text>
      <biological_entity id="o18044" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate-deltate" value_original="ovate-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals 5, white, orbiculate-reniform, base short-clawed;</text>
      <biological_entity id="o18045" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s16" value="orbiculate-reniform" value_original="orbiculate-reniform" />
      </biological_entity>
      <biological_entity id="o18046" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="short-clawed" value_original="short-clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stamens 70–110 in 2 or 3 series, shorter than petals;</text>
      <biological_entity id="o18047" name="stamen" name_original="stamens" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="in petals" constraintid="o18049" from="70" name="quantity" src="d0_s17" to="110" />
      </biological_entity>
      <biological_entity id="o18049" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o18048" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>carpels (4 or) 5, distinct, appearing connivent at base, sericeous, styles caducous, terminal, slender, stigmas broadened;</text>
      <biological_entity id="o18050" name="carpel" name_original="carpels" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s18" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o18051" name="base" name_original="base" src="d0_s18" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s18" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o18052" name="style" name_original="styles" src="d0_s18" type="structure">
        <character is_modifier="false" name="duration" src="d0_s18" value="caducous" value_original="caducous" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s18" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s18" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o18053" name="stigma" name_original="stigmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="width" src="d0_s18" value="broadened" value_original="broadened" />
      </biological_entity>
      <relation from="o18050" id="r1175" name="appearing" negation="false" src="d0_s18" to="o18051" />
    </statement>
    <statement id="d0_s19">
      <text>ovules 10–12 in 2 series.</text>
      <biological_entity id="o18054" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o18055" from="10" name="quantity" src="d0_s19" to="12" />
      </biological_entity>
      <biological_entity id="o18055" name="series" name_original="series" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruits aggregations of (4–) 5 follicles, each obliquely lanceoloid, 3–5 mm, hirtellous, dehiscent to bases on ventral sutures and halfway along dorsal sutures;</text>
      <biological_entity constraint="fruits" id="o18056" name="aggregation" name_original="aggregations" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" notes="" src="d0_s20" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s20" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="hirtellous" value_original="hirtellous" />
        <character constraint="to bases" constraintid="o18058" is_modifier="false" name="dehiscence" src="d0_s20" value="dehiscent" value_original="dehiscent" />
        <character constraint="along dorsal sutures" constraintid="o18060" is_modifier="false" name="position" notes="" src="d0_s20" value="halfway" value_original="halfway" />
      </biological_entity>
      <biological_entity id="o18057" name="follicle" name_original="follicles" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s20" to="5" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s20" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o18058" name="base" name_original="bases" src="d0_s20" type="structure" />
      <biological_entity constraint="ventral" id="o18059" name="suture" name_original="sutures" src="d0_s20" type="structure" />
      <biological_entity constraint="dorsal" id="o18060" name="suture" name_original="sutures" src="d0_s20" type="structure" />
      <relation from="o18056" id="r1176" name="consist_of" negation="false" src="d0_s20" to="o18057" />
      <relation from="o18058" id="r1177" name="on" negation="false" src="d0_s20" to="o18059" />
    </statement>
    <statement id="d0_s21">
      <text>hypanthium persistent;</text>
      <biological_entity id="o18061" name="hypanthium" name_original="hypanthium" src="d0_s21" type="structure">
        <character is_modifier="false" name="duration" src="d0_s21" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>sepals persistent, erect to spreading;</text>
      <biological_entity id="o18062" name="sepal" name_original="sepals" src="d0_s22" type="structure">
        <character is_modifier="false" name="duration" src="d0_s22" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s22" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>styles persistent or not.</text>
      <biological_entity id="o18063" name="style" name_original="styles" src="d0_s23" type="structure">
        <character is_modifier="false" name="duration" src="d0_s23" value="persistent" value_original="persistent" />
        <character name="duration" src="d0_s23" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds 3–8, narrowly fusiform, thin walled.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 9.</text>
      <biological_entity id="o18064" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s24" to="8" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s24" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="width" src="d0_s24" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="walled" value_original="walled" />
      </biological_entity>
      <biological_entity constraint="x" id="o18065" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Vestiture on young stems, inflorescences, leaves, hypanthia, and sepals of Chamaebatiaria is complex, consisting of short, straight or curved, sessile hairs; sessile or thick-stalked stellate clusters of flattened, more or less twisted hairs; and sessile, subsessile, or thick-stalked stipitate glands. In some plants, glands will exude material rendering the stems and leaves glutinous. They may have hairs on the gland tip or on the stalk below the gland.</discussion>
  <discussion>Molecular data indicate relationships of Chamaebatiaria with the Sorbariaeae, with the Asian Sorbaria and Spiraeanthus (Fischer &amp; C. A. Meyer) Maximowicz, and the North American Adenostoma (D. Potter et al. 2007). The similarity to Chamaebatia is striking. J. A. Wolfe and W. C. Wehr (1988) presented interesting fossil evidence of leaf structure development.</discussion>
  
</bio:treatment>