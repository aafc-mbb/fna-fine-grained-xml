<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">164</other_info_on_meta>
    <other_info_on_meta type="mention_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Arnott" date="unknown" rank="subfamily">rosoideae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="tribe">potentilleae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">potentilla</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson in J. M. Coulter and A. Nelson" date="1909" rank="section">Leucophyllae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="species">crinita</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 41. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily rosoideae;tribe potentilleae;genus potentilla;section leucophyllae;species crinita;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250100317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ivesia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">lemmonii</taxon_name>
    <taxon_hierarchy>genus Ivesia;species lemmonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Potentilla</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">crinita</taxon_name>
    <taxon_name authority="(S. Watson) Kearney &amp; Peebles" date="unknown" rank="variety">lemmonii</taxon_name>
    <taxon_hierarchy>genus Potentilla;species crinita;variety lemmonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">P.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lemmonii</taxon_name>
    <taxon_hierarchy>genus P.;species lemmonii</taxon_hierarchy>
  </taxon_identification>
  <number>33.</number>
  <other_name type="common_name">Bearded or Lemmon’s cinquefoil</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (0.5–) 1.5–4.5 dm, lengths 2–4 (–5) times basal leaves.</text>
      <biological_entity id="o27577" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="4.5" to_unit="dm" />
        <character constraint="leaf" constraintid="o27578" is_modifier="false" name="length" src="d0_s0" value="2-4(-5) times basal leaves" value_original="2-4(-5) times basal leaves" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27578" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves pinnate, 3–15 (–20) cm;</text>
      <biological_entity constraint="basal" id="o27579" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–10 (–15) cm, long hairs dense, appressed, 1.5–2.5 mm, usually stiff, short and crisped hairs usually absent, cottony hairs absent, glands sparse, often obscured;</text>
      <biological_entity id="o27580" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27581" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o27582" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o27583" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s2" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o27584" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s2" value="obscured" value_original="obscured" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets often conduplicate, lateral ones evenly paired, (3–) 4–6 (–7) per side on distal 1/3–2/3 of leaf axis, distal pairs ± decurrent, often confluent with terminal leaflet, larger leaflets narrowly cuneate or oblanceolate to obovate, 1–3 (–4) × 0.2–0.8 (–1) cm, distal 1/4–1/2 (–2/3) or less of margin incised ± 1/4 or less to midvein, teeth (0–) 1–5 (–9) per side, 1–2 mm, surfaces ± similar to ± dissimilar, abaxial silvery to greenish, long hairs usually dense (at least on primary-veins), 1–2 mm, stiff, short-crisped hairs absent or sparse, cottony hairs usually absent, glands sparse to common, often obscured, adaxial ± green, long hairs sparse to common, sometimes absent, short, crisped, and cottony hairs absent, glands sparse.</text>
      <biological_entity id="o27585" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement_or_vernation" src="d0_s3" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="evenly" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s3" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="7" />
        <character char_type="range_value" constraint="per side" constraintid="o27586" from="4" name="quantity" src="d0_s3" to="6" />
      </biological_entity>
      <biological_entity id="o27586" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o27587" name="1/3-2/3" name_original="1/3-2/3" src="d0_s3" type="structure" />
      <biological_entity constraint="leaf" id="o27588" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o27589" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character constraint="with terminal leaflet" constraintid="o27590" is_modifier="false" modifier="often" name="arrangement" src="d0_s3" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o27590" name="leaflet" name_original="leaflet" src="d0_s3" type="structure" />
      <biological_entity constraint="larger" id="o27591" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="obovate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s3" to="1/2-2/3" />
        <character name="quantity" src="d0_s3" value="less" value_original="less" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="incised" value_original="incised" />
        <character name="quantity" src="d0_s3" value="1/4" value_original="1/4" />
        <character name="quantity" src="d0_s3" value="less" value_original="less" />
      </biological_entity>
      <biological_entity id="o27592" name="margin" name_original="margin" src="d0_s3" type="structure" />
      <biological_entity id="o27593" name="midvein" name_original="midvein" src="d0_s3" type="structure" />
      <biological_entity id="o27594" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s3" to="1" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="9" />
        <character char_type="range_value" constraint="per side" constraintid="o27595" from="1" name="quantity" src="d0_s3" to="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27595" name="side" name_original="side" src="d0_s3" type="structure" />
      <biological_entity id="o27596" name="surface" name_original="surfaces" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o27597" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="silvery" modifier="more or less" name="coloration" src="d0_s3" to="greenish" />
      </biological_entity>
      <biological_entity id="o27598" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" modifier="usually" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o27599" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="short-crisped" value_original="short-crisped" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o27600" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o27601" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s3" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27602" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o27603" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character char_type="range_value" from="sparse" name="quantity" src="d0_s3" to="common" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o27604" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s3" value="cottony" value_original="cottony" />
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o27605" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="false" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o27586" id="r1798" name="on" negation="false" src="d0_s3" to="o27587" />
      <relation from="o27587" id="r1799" name="part_of" negation="false" src="d0_s3" to="o27588" />
      <relation from="o27591" id="r1800" name="part_of" negation="false" src="d0_s3" to="o27592" />
      <relation from="o27591" id="r1801" name="to" negation="false" src="d0_s3" to="o27593" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 1–3 (–4).</text>
      <biological_entity constraint="cauline" id="o27606" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences (5–) 10–30-flowered.</text>
      <biological_entity id="o27607" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="(5-)10-30-flowered" value_original="(5-)10-30-flowered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pedicels 0.5–2 (–4) cm.</text>
      <biological_entity id="o27608" name="pedicel" name_original="pedicels" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Flowers: epicalyx bractlets lanceolate, 1.5–4.5 × 0.5 (–1) mm, 1/2–2/3 as long as sepals, abaxial vestiture similar to or ± sparser than sepals, not glabrescent, straight hairs common, crisped or cottony hairs usually absent;</text>
      <biological_entity id="o27609" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="epicalyx" id="o27610" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="1" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="0.5" value_original="0.5" />
        <character char_type="range_value" constraint="as-long-as sepals" constraintid="o27611" from="1/2" name="quantity" src="d0_s7" to="2/3" />
      </biological_entity>
      <biological_entity id="o27611" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o27612" name="vestiture" name_original="vestiture" src="d0_s7" type="structure">
        <character constraint="than sepals" constraintid="o27613" is_modifier="false" name="arrangement_or_density" src="d0_s7" value="more or less sparser" value_original="more or less sparser" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o27613" name="sepal" name_original="sepals" src="d0_s7" type="structure" />
      <biological_entity id="o27614" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="common" value_original="common" />
        <character is_modifier="false" name="shape" src="d0_s7" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o27615" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="cottony" value_original="cottony" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypanthium 2.5–5 mm diam.;</text>
      <biological_entity id="o27616" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o27617" name="hypanthium" name_original="hypanthium" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals (3–) 4–7 mm, apex acute to long acuminate;</text>
      <biological_entity id="o27618" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o27619" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27620" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="long acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (3–) 4.5–7.5 (–8) × 4–6 mm;</text>
      <biological_entity id="o27621" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o27622" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s10" to="4.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1–3 mm, anthers 0.6–1.1 mm;</text>
      <biological_entity id="o27623" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o27624" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27625" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>carpels 5–20, styles 1.6–2.6 mm.</text>
      <biological_entity id="o27626" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o27627" name="carpel" name_original="carpels" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <biological_entity id="o27628" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Achenes 1.4–1.7 mm, smooth or slightly rugose.</text>
      <biological_entity id="o27629" name="achene" name_original="achenes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry meadows, pygmy conifer, oak, aspen, or montane conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry meadows" />
        <character name="habitat" value="pygmy conifer" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="montane conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nev., N.Mex., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Potentilla crinita occurs mainly in the upper foothills and mountains from southern Nevada to south-central Utah, northern Arizona, and northwestern New Mexico and is disjunct to southwestern Colorado (Archuleta County). It tends to grow on somewhat drier, rockier sites than co-occurring species of Potentilla. The often conduplicate leaflets, falcate in outline, bear relatively few, small teeth. Two varieties are sometimes recognized, based on leaflet and vestiture characters that do not reliably coincide.</discussion>
  <discussion>Potentilla crinita can hybridize with P. hippiana where the two species overlap, in spite of ecological partitioning. N. H. Holmgren (1997b) noted the type of P. crinita may be such a hybrid. If correct, then P. lemmonii would be used for the species unless the name P. crinita were to be conserved with a conserved type.</discussion>
  
</bio:treatment>