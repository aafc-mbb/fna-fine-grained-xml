<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/15 19:57:41</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">331</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">rosaceae</taxon_name>
    <taxon_name authority="Sweet" date="unknown" rank="subfamily">dryadoideae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">dryadeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">dryas</taxon_name>
    <taxon_name authority="Richardson ex Hooker" date="1830" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>57: plate 2972. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily dryadoideae;tribe dryadeae;genus dryas;species drummondii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100202</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryas</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="A. E. Porsild" date="unknown" rank="variety">eglandulosa</taxon_name>
    <taxon_hierarchy>genus Dryas;species drummondii;variety eglandulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="Farr" date="unknown" rank="species">drummondii</taxon_name>
    <taxon_name authority="(Farr) L. O. Williams" date="unknown" rank="variety">tomentosa</taxon_name>
    <taxon_hierarchy>genus D.;species drummondii;variety tomentosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">octopetala</taxon_name>
    <taxon_name authority="(Richardson ex Hooker) S. Watson" date="unknown" rank="variety">drummondii</taxon_name>
    <taxon_hierarchy>genus D.;species octopetala;variety drummondii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tomentosa</taxon_name>
    <taxon_hierarchy>genus D.;species tomentosa</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Drummond’s or yellow mountain-avens</other_name>
  <other_name type="common_name">dryade de Drummond</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–23 cm.</text>
      <biological_entity id="o16601" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="23" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades oblongelliptic to obovate, 4–38 (–46) × 2–24 mm, base usually cuneate, sometimes truncate or cordate, margins flat, crenate, dentate, or ± serrate, sinuses 10–25% to midvein, apex rounded to obtuse, surfaces smooth, tomentose, feathery hairs and stipitate-glands absent or on midveins and petioles;</text>
      <biological_entity id="o16602" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s1" to="obovate" />
        <character char_type="range_value" from="38" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="46" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s1" to="38" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16603" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s1" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s1" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o16604" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s1" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o16605" name="sinuse" name_original="sinuses" src="d0_s1" type="structure" />
      <biological_entity id="o16606" name="midvein" name_original="midvein" src="d0_s1" type="structure" />
      <biological_entity id="o16607" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s1" to="obtuse" />
      </biological_entity>
      <biological_entity id="o16608" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o16609" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="on midveins and petioles" value_original="on midveins and petioles" />
      </biological_entity>
      <biological_entity id="o16610" name="stipitate-gland" name_original="stipitate-glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16611" name="midvein" name_original="midveins" src="d0_s1" type="structure" />
      <biological_entity id="o16612" name="petiole" name_original="petioles" src="d0_s1" type="structure" />
      <relation from="o16605" id="r1068" modifier="10-25%" name="to" negation="false" src="d0_s1" to="o16606" />
      <relation from="o16609" id="r1069" name="on" negation="false" src="d0_s1" to="o16611" />
      <relation from="o16610" id="r1070" name="on" negation="false" src="d0_s1" to="o16611" />
      <relation from="o16609" id="r1071" name="on" negation="false" src="d0_s1" to="o16612" />
      <relation from="o16610" id="r1072" name="on" negation="false" src="d0_s1" to="o16612" />
    </statement>
    <statement id="d0_s2">
      <text>basal leaflets 1 (–2), 0.5–3 × 0.5–2 mm.</text>
      <biological_entity constraint="basal" id="o16613" name="leaflet" name_original="leaflets" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="2" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 40–263 mm.</text>
      <biological_entity id="o16614" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s3" to="263" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers nodding at flowering;</text>
      <biological_entity id="o16615" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character constraint="at flowering" is_modifier="false" name="orientation" src="d0_s4" value="nodding" value_original="nodding" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals ovate, 4–6 × 3–4 mm;</text>
      <biological_entity id="o16616" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 8–10 (–12), erect, yellow, 8–12 × 4–7 mm;</text>
      <biological_entity id="o16617" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="12" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments hairy.</text>
      <biological_entity id="o16618" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes 2.5–3 mm;</text>
      <biological_entity id="o16619" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>styles 15–39 mm. 2n = 18.</text>
      <biological_entity id="o16620" name="style" name_original="styles" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="39" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16621" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and gravelly beaches, flood plains, stream bars, rocky streams, grassy areas, alpine, calcicolous</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly beaches" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="stream bars" />
        <character name="habitat" value="rocky streams" />
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="calcicolous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Ont., Que., Sask., Yukon; Alaska, Mont., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In some states and provinces, Dryas drummondii has a limited distribution: northwestern Saskatchewan, Lake Superior islands of Ontario (relatively rare elsewhere in Ontario), Gaspé Peninsula, Quebec, northern Montana (Glacier National Park and surrounding area), and northeastern Oregon.</discussion>
  <discussion>Dryas ×suendermannii Kellerer ex Sündermann, a hybrid between D. drummondii and D. hookeriana, has been reported from Alberta (J. G. Packer 1994). The hybrid shares with D. drummondii abaxial median and lateral leaf veins covered by tomentum, not viscid, stipitate glands absent; adaxial surface not viscid, and punctate glands absent. The hybrid shares with D. hookeriana an adaxially shiny leaf surface. It is intermediate in feathery hairs occasionally present (absent in D. hookeriana).</discussion>
  
</bio:treatment>