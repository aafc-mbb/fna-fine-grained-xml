
# coding: utf-8

# In[1]:


from __future__ import print_function
import sys
import glob
from lxml import etree

#file = sys.argv[1]
#file = '../../../coarse_grained_fna_xml/V2/V2_134.xml'
for volume_path in glob.glob("/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/V*"):
    volume = volume_path.split("/")[-1]
#volume = "25"
    my_file=open("/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/treatment_lists/" + volume +                  "_treatment_list.txt","w")
    for file_name in sorted(glob.glob("/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/"                                       + volume + "/*.xml")):
        tree = etree.parse(file_name)
        # define root
        root = tree.getroot()
        # find ACCEPTED taxon_indentification:
        for taxon_identification in root.findall("./taxon_identification/[@status='ACCEPTED']"):
            # define empty string to hold treatment name
            treat_name = ""
            #define empty string to hold the last taxon rank (rank of interest)
            rank_name = ""
            # define empty strings to hold names of different taxon ranks
            fam_name = ""
            subfam_name = ""
            tribe_name = ""
            subtribe_name = ""
            genus_name = ""
            subgenus_name = ""
            sect_name = ""
            subsect_name = ""
            ser_name = ""
            subser_name = ""
            species_name = ""
            subspecies_name = ""
            variety_name = ""
            # find all taxon_name under ACCEPTED taxon_identification
            for taxon_name in taxon_identification.findall('./taxon_name'):
                # use if/elif statements to append taxon_name values to appropriate empty strings above
                if taxon_name.get('rank') == 'family':
                    fam_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subfamily':
                    subfam_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'tribe':
                    tribe_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subtribe':
                    subtribe_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'genus':
                    genus_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subgenus':
                    subgenus_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'section':
                    sect_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subsection':
                    subsect_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'series':
                    ser_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subseries':
                    subser_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'species':
                    species_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'subspecies':
                    subspecies_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'variety':
                    variety_name+=(taxon_name.text)
                elif taxon_name.get('rank') == 'northovariety':
                    northovariety_name+=(taxon_name.text)
            # find the last taxon_name using xpath
            # the value of the last taxon_name in the hierarchy determines the concatenation pattern for...
            # ...writing the treatment name
            for taxon_name in taxon_identification.xpath('taxon_name[last()]'):
                #grab the rank that matches the last taxon_name in taxon_identification
                rank_name = taxon_name.get('rank')
                if taxon_name.get('rank') == 'family':
                    treat_name = (fam_name)
                elif taxon_name.get('rank') == 'subfamily':
                    treat_name = (fam_name+' '+'subfam.'+' '+subfam_name)
                elif taxon_name.get('rank') == 'tribe':
                    treat_name = (fam_name+' '+'tribe'+' '+tribe_name)
                elif taxon_name.get('rank') == 'subtribe':
                    treat_name = (fam_name+' '+'tribe '+tribe_name+' '+'subtribe'+' '+subtribe_name)
                elif taxon_name.get('rank') == 'genus':
                    treat_name = (genus_name)
                elif taxon_name.get('rank') == 'subgenus':
                    treat_name = (genus_name+' '+'subg.'+' '+subgenus_name)
                elif taxon_name.get('rank') == 'section':
                    treat_name = (genus_name+' '+'sect.'+' '+sect_name)
                elif taxon_name.get('rank') == 'subsection':
                    treat_name = (genus_name+' '+sect_name+' '+'subsect.'+' '+subsect_name)
                elif taxon_name.get('rank') == 'series':
                    treat_name = (genus_name+' '+'('+'sect.'+' '+sect_name+')'+' '+'ser.'+' '+ser_name)
                elif taxon_name.get('rank') == 'subseries':
                    treat_name = (genus_name+' '+'('+'sect.'+' '+sect_name+')'+' '+'ser.'+' '+ser_name+' '+'subser'+' '+subser_name)
                elif taxon_name.get('rank') == 'species':
                    treat_name = (genus_name+' '+species_name)
                elif taxon_name.get('rank') == 'subspecies':
                    treat_name = (genus_name+' '+species_name+' '+'subsp.'+' '+subspecies_name)
                elif taxon_name.get('rank') == 'variety':
                    treat_name = (genus_name+' '+species_name+' '+'var.'+' '+variety_name)
                elif taxon_name.get('rank') == 'northovariety':
                    treat_name = (genus_name+' '+species_name+' '+'var.'+' '+northovariety_name)
        # use the encode function to set encoding to UTF-8, since ASCII fails to encode certain special characters
        file_name = file_name.split('/')[-1]
        print (file_name+','+str(treat_name)+','+rank_name) 
        my_file.write(file_name+','+str(treat_name)+','+rank_name+'\n')

    my_file.close()


