<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="T. J. Koponen" date="unknown" rank="genus">plagiomnium</taxon_name>
    <taxon_name authority="(Mitten) T. J. Koponen" date="1968" rank="species">venustum</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>5: 146. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus plagiomnium;species venustum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099249</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">venustum</taxon_name>
    <place_of_publication>
      <publication_title>Hooker's J. Bot. Kew Gard. Misc.</publication_title>
      <place_in_publication>8: 231, plate 12, fig. B. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mnium;species venustum</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Erect stems 2–4 (–6) cm, not dendroid;</text>
      <biological_entity id="o71" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sterile stems absent.</text>
      <biological_entity id="o72" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green or yellow-green, crisped and contorted, distally often densely twisted around stem when dry, flat when moist, obovate or elliptic, 3–5 (–7) mm;</text>
      <biological_entity id="o73" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character constraint="around stem" constraintid="o74" is_modifier="false" modifier="distally often densely" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o74" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when moist" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base decurrent or not;</text>
      <biological_entity id="o75" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
        <character name="shape" src="d0_s3" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins toothed to 2/3 leaf length or sometimes just past mid leaf or to base, teeth sharp, of 1–2 (–3) cells;</text>
      <biological_entity id="o76" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="2/3" />
      </biological_entity>
      <biological_entity id="o77" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <biological_entity constraint="mid" id="o78" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes just" name="position_relational" src="d0_s4" value="past" value_original="past" />
      </biological_entity>
      <biological_entity id="o79" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o80" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o81" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <relation from="o78" id="r9" name="to" negation="false" src="d0_s4" to="o79" />
      <relation from="o80" id="r10" name="consist_of" negation="false" src="d0_s4" to="o81" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute, acuminate, or rarely obtuse or rounded, cuspidate, cusp toothed;</text>
      <biological_entity id="o82" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o83" name="cusp" name_original="cusp" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent, excurrent, or rarely subpercurrent;</text>
      <biological_entity id="o84" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells short-elongate or ± isodiametric, (17–) 25–40 (–45) µm, slightly smaller near margins, in longitudinal, rarely diagonal rows, strongly collenchymatous, walls not pitted;</text>
      <biological_entity constraint="medial laminal" id="o85" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="17" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="25" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="45" to_unit="um" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s7" to="40" to_unit="um" />
        <character constraint="near margins" constraintid="o86" is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s7" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity id="o86" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o87" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s7" value="diagonal" value_original="diagonal" />
      </biological_entity>
      <biological_entity id="o88" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s7" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o85" id="r11" name="in" negation="false" src="d0_s7" to="o87" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells linear, in (3–) 4–5 rows.</text>
      <biological_entity id="o90" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o89" id="r12" name="in" negation="false" src="d0_s8" to="o90" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition synoicous.</text>
      <biological_entity constraint="marginal" id="o89" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 1–4 (–6), yellow, reddish at base, 3–4 cm.</text>
      <biological_entity id="o91" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="4" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character constraint="at base" constraintid="o92" is_modifier="false" name="coloration" src="d0_s10" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s10" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o92" name="base" name_original="base" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule horizontal to pendent, oblong, 3–4.5 mm, neck distinct, brown, often wrinkled;</text>
      <biological_entity id="o93" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s11" to="pendent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o94" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="often" name="relief" src="d0_s11" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic-apiculate.</text>
      <biological_entity id="o95" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="conic-apiculate" value_original="conic-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 34–40 µm.</text>
      <biological_entity id="o96" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="34" from_unit="um" name="some_measurement" src="d0_s13" to="40" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humus, soil, sand, logs, stumps, tree bases, rock, concrete, well-drained sites, forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="stumps" modifier="logs" />
        <character name="habitat" value="tree bases" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="concrete" />
        <character name="habitat" value="well-drained sites" />
        <character name="habitat" value="stumps" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plagiomnium venustum is the only species of the genus that lacks sterile stems. It is a common western species that forms large, handsome mats or turfs in many northwest coastal forests, especially noticeable on tree bases. Sporophytes are common. Another distinctive feature of P. venustum is the presence of dark and mammillose stomatal guard cells in the necks of the capsules.</discussion>
  
</bio:treatment>