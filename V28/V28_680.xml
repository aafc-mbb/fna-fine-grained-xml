<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">437</other_info_on_meta>
    <other_info_on_meta type="mention_page">430</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Ignatov &amp; Huttunen" date="unknown" rank="genus">eurhynchiastrum</taxon_name>
    <taxon_name authority="(Hedwig) Ignatov &amp; Huttunen" date="2003" rank="species">pulchellum</taxon_name>
    <place_of_publication>
      <publication_title>Arctoa</publication_title>
      <place_in_publication>11: 262. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus eurhynchiastrum;species pulchellum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099099</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">pulchellum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>265, plate 68 [top], figs. 1 – 4. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species pulchellum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glossy.</text>
      <biological_entity id="o11327" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–6 (–13) cm, branches 3–8 (–17) mm.</text>
      <biological_entity id="o11328" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="13" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11329" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="17" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves broadest at 1/10 leaf length or below, 0.5–1.3 (–2.6) × 0.4–0.9 (–1.8) mm;</text>
      <biological_entity id="o11330" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character constraint="at leaf" constraintid="o11331" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" modifier="below" name="atypical_length" notes="" src="d0_s2" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="below" name="length" notes="" src="d0_s2" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" modifier="below" name="atypical_width" notes="" src="d0_s2" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11331" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/10" value_original="1/10" />
        <character char_type="range_value" from="0.4" from_unit="mm" modifier="below" name="width" notes="" src="d0_s2" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base rounded to insertion;</text>
      <biological_entity id="o11332" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="insertion" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or recurved proximally;</text>
    </statement>
    <statement id="d0_s5">
      <text>alar cells 12–20 × 10–15 µm, region subquadrate, small, ± distinct, often surrounded by subquadrate cells in ± extensive, indistinctly delimited group;</text>
      <biological_entity id="o11333" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o11334" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="length" src="d0_s5" to="20" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s5" to="15" to_unit="um" />
      </biological_entity>
      <biological_entity id="o11335" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" modifier="more or less" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o11336" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
      <relation from="o11335" id="r1601" modifier="often" name="surrounded by" negation="false" src="d0_s5" to="o11336" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 30–75 (–100) × 4–6 µm, smooth;</text>
      <biological_entity constraint="laminal" id="o11337" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s6" to="100" to_unit="um" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s6" to="75" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s6" to="6" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal juxtacostal cells 7–10 µm wide.</text>
      <biological_entity constraint="basal" id="o11338" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o11339" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch leaves 0.2–1.3 (–1.9) × 0.2–0.5 (–1) mm;</text>
      <biological_entity constraint="branch" id="o11340" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s8" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa to 60–90% leaf length;</text>
      <biological_entity id="o11341" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <biological_entity id="o11342" name="leaf" name_original="leaf" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="60-90%" name="character" src="d0_s9" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminal cells just before apex short, nearly isodiametric or irregular in shape.</text>
      <biological_entity constraint="laminal" id="o11343" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s10" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="irregular" value_original="irregular" />
      </biological_entity>
      <biological_entity id="o11344" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
      <relation from="o11343" id="r1602" name="before" negation="false" src="d0_s10" to="o11344" />
    </statement>
    <statement id="d0_s11">
      <text>Seta 1–1.8 cm.</text>
      <biological_entity id="o11345" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="1.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule 1.5 mm.</text>
      <biological_entity id="o11346" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores slightly papillose.</text>
      <biological_entity id="o11347" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, Eurasia, n, e Africa, Atlantic Islands, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="e Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Eurhynchiastrum pulchellum is easy to recognize by its blunt branch leaves. The common phenotype for plants on soil and tree trunk bases in forests is light green plants with rigid erect leaves. In xeric and arctic environments, leaves are strongly appressed forming julaceous shoots, and plants are often brownish and quite fragile. They have been segregated as var. praecox (temperate areas, branch leaves 0.5–0.8 mm, alar cells as in typical variety) and var. diversifolium (arctic-alpine, branch leaves 0.5–0.6 mm, alar cells very numerous) but these grade to typical phenotypes and thus do not merit taxonomic recognition. Some collections described as Eurhynchium strigosum var. scabrisetum Grout have roughened setae, although the mammillae are very low, appearing as a shallowly wavy outline mostly at mid seta. In various parts of North America, plants of more robust stature occur. They were described at the level of variety (var. barnesii, var. robusta), of even species (Eurhynchium fallax, E. substrigosum, E. tayloriae). Plants with broadly triangular branch leaves up to 1 mm have been reported from California and Colorado as Eurhynchium striatum (D. H. Norris and J. R. Shevock 2004; W. A. Weber and R. C. Wittmann 2007). Among the taxa of large Eurhynchium pulchellum, H. A. Crum et al. (1965) accepted only var. barnesii, as the largest one in all dimensions, and this treatment is followed here.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves 0.5-1.3 mm; branch leaves 0.2-1.1 × 0.2-0.5(-0.6) mm, not or slightly plicate.</description>
      <determination>1a Eurhynchiastrum pulchellum var. pulchellum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves 1.2-2.6 mm; branch leaves 1-1.9 × 0.4-0.8(-1) mm, strongly plicate.</description>
      <determination>1b Eurhynchiastrum pulchellum var. barnesii</determination>
    </key_statement>
  </key>
</bio:treatment>