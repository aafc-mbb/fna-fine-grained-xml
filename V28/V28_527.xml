<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">rhytidiaceae</taxon_name>
    <taxon_name authority="(Sullivant) Kindberg" date="unknown" rank="genus">RHYTIDIUM</taxon_name>
    <place_of_publication>
      <publication_title>Bih. Kongl. Svenska Vetensk.-Akad. Handl.</publication_title>
      <place_in_publication>6(19): 8. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhytidiaceae;genus RHYTIDIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek rhytis, wrinkle, alluding to strongly rugose leaves</other_info_on_name>
    <other_info_on_name type="fna_id">128533</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="section">Rhytidium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 675. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;section rhytidium</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem-leaves erect, crowded, imbricate, falcate-secund, obscurely plicate, strongly rugose;</text>
      <biological_entity id="o17427" name="stem-leaf" name_original="stem-leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s0" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" modifier="obscurely" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s0" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="strongly" name="relief" src="d0_s0" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>base not or short-decurrent;</text>
      <biological_entity id="o17428" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>laminal cells coarsely prorate at distal ends.</text>
      <biological_entity constraint="laminal" id="o17429" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o17430" name="end" name_original="ends" src="d0_s2" type="structure" />
      <relation from="o17429" id="r2516" name="prorate at" negation="false" src="d0_s2" to="o17430" />
    </statement>
    <statement id="d0_s3">
      <text>Branch leaves similar, less falcate-secund, smaller.</text>
      <biological_entity constraint="branch" id="o17431" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="less" name="arrangement" src="d0_s3" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsule oblong-ellipsoid to cylindric, arcuate, not plicate, constricted below mouth when dry;</text>
      <biological_entity id="o17432" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong-ellipsoid" name="shape" src="d0_s4" to="cylindric" />
        <character is_modifier="false" name="course_or_shape" src="d0_s4" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
        <character constraint="below mouth" constraintid="o17433" is_modifier="false" name="size" src="d0_s4" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o17433" name="mouth" name_original="mouth" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>operculum high-conic to short-rostrate.</text>
      <biological_entity id="o17434" name="operculum" name_original="operculum" src="d0_s5" type="structure">
        <character char_type="range_value" from="high-conic" name="shape" src="d0_s5" to="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spores 10–17 µm.</text>
      <biological_entity id="o17435" name="spore" name_original="spores" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s6" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Guatemala), South America (Bolivia), Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>