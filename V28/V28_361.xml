<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">mnium</taxon_name>
    <taxon_name authority="(Voit) Schwagrichen" date="1816" rank="species">spinosum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(2): 130. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus mnium;species spinosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001498</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Voit" date="unknown" rank="species">spinosum</taxon_name>
    <place_of_publication>
      <publication_title>in J. Sturm et al., Deutschl. Fl.</publication_title>
      <place_in_publication>2(11): [16], plate s.n. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species spinosum</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3.5–6 (–8) cm.</text>
      <biological_entity id="o3907" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems red or reddish-brown.</text>
      <biological_entity id="o3908" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green to dark green, crisped or undulate, twisted when dry, elliptic, ovate-elliptic, oblong-ovate, or rarely obovate, rarely narrow, 4–6 mm, proximal stem-leaves narrowly triangular;</text>
      <biological_entity id="o3909" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="dark green" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="rarely" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o3910" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base long-decurrent;</text>
      <biological_entity id="o3911" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins reddish-brown, multistratose with stereid band, toothed to below mid leaf, teeth paired, large, sharp;</text>
      <biological_entity id="o3912" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish-brown" value_original="reddish-brown" />
        <character constraint="with band" constraintid="o3913" is_modifier="false" name="architecture" src="d0_s4" value="multistratose" value_original="multistratose" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o3913" name="band" name_original="band" src="d0_s4" type="structure" />
      <biological_entity id="o3914" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s4" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o3915" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
        <character is_modifier="false" name="size" src="d0_s4" value="large" value_original="large" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute or rounded-acute, cuspidate or sometimes apiculate, cusp toothed;</text>
      <biological_entity id="o3916" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-acute" value_original="rounded-acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o3917" name="cusp" name_original="cusp" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent or excurrent, distal abaxial surface toothed, adaxial surface rarely toothed;</text>
      <biological_entity id="o3918" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o3919" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3920" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells elongate, (20–) 25–40 µm, slightly smaller towards margins, in diagonal or weak longitudinal rows, not or weakly collenchymatous, blue postmortal color absent;</text>
      <biological_entity constraint="medial laminal" id="o3921" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="20" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="25" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s7" to="40" to_unit="um" />
        <character constraint="towards margins" constraintid="o3922" is_modifier="false" modifier="slightly" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="not; weakly" name="architecture" notes="" src="d0_s7" value="collenchymatous" value_original="collenchymatous" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue" value_original="blue" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o3922" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity constraint="diagonal" id="o3923" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s7" value="weak" value_original="weak" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o3921" id="r514" name="in" negation="false" src="d0_s7" to="o3923" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells linear, in (2–) 3–4 rows.</text>
      <biological_entity id="o3925" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s8" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o3924" id="r515" name="in" negation="false" src="d0_s8" to="o3925" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous.</text>
    </statement>
    <statement id="d0_s10">
      <text>[Seta single to several. Capsule yellowbrown, 1.5–2.5 mm; operculum rostrate; exostome light-brown. Spores 20–32 µm].</text>
      <biological_entity constraint="marginal" id="o3924" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forests on humus, soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" constraint="on humus , soil" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate (high) elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.W.T., Yukon; Alaska, Colo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Well-developed leaves of Mnium spinosum, a northern species, are characterized by long, sharp, and often highly divergent marginal teeth, present to well below mid leaf. The longer leaves and the angular, not or weakly collenchymatous laminal cells separate this species from M. lycopodioides and M. marginatum. Sporophytes have not been observed in North American collections. Characteristics of the sporophyte that are presented here are taken from A. J. E. Smith (2004).</discussion>
  <references>
    <reference>Steere, W. C. 1973b. The occurrence of Mnium spinosum in North America. Bryologist 76: 430–434.</reference>
  </references>
  
</bio:treatment>