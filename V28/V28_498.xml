<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
    <other_info_on_meta type="illustration_page">318</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="A. L. Andrews" date="unknown" rank="genus">platylomella</taxon_name>
    <taxon_name authority="(Sullivant) A. L. Andrews" date="1950" rank="species">lescurii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>53: 58. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus platylomella;species lescurii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062110</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">lescurii</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 679. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species lescurii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblystegium</taxon_name>
    <taxon_name authority="(Sullivant) Austin" date="unknown" rank="species">lescurii</taxon_name>
    <taxon_hierarchy>genus amblystegium;species lescurii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sciaromium</taxon_name>
    <taxon_name authority="(Sullivant) Brotherus" date="unknown" rank="species">lescurii</taxon_name>
    <taxon_hierarchy>genus sciaromium;species lescurii</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in fine sods or loose mats.</text>
      <biological_entity id="o323" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o324" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o323" id="r47" name="in" negation="false" src="d0_s0" to="o324" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–6 (–10) cm, brownish black, prostrate, defoliate proximally, branches 1–2 cm, ascending, simple;</text>
      <biological_entity id="o325" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish black" value_original="brownish black" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="defoliate" value_original="defoliate" />
      </biological_entity>
      <biological_entity id="o326" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia becoming scarce in emergent plants;</text>
      <biological_entity id="o327" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character constraint="in plants" constraintid="o328" is_modifier="false" modifier="becoming" name="count_or_density" src="d0_s2" value="scarce" value_original="scarce" />
      </biological_entity>
      <biological_entity id="o328" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="emergent" value_original="emergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia foliose;</text>
      <biological_entity id="o329" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids matted, brown.</text>
      <biological_entity id="o330" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s4" value="matted" value_original="matted" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves somewhat contorted when dry, 0.5–1.3 (–1.5) mm;</text>
      <biological_entity id="o331" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s5" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s5" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s5" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>limbidia sometimes disappearing before acumen or base, sometimes with spurs extending into lamina;</text>
      <biological_entity id="o332" name="limbidium" name_original="limbidia" src="d0_s6" type="structure">
        <character constraint="before base" constraintid="o334" is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="disappearing" value_original="disappearing" />
      </biological_entity>
      <biological_entity id="o333" name="acumen" name_original="acumen" src="d0_s6" type="structure" />
      <biological_entity id="o334" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o335" name="spur" name_original="spurs" src="d0_s6" type="structure" />
      <biological_entity id="o336" name="lamina" name_original="lamina" src="d0_s6" type="structure" />
      <relation from="o332" id="r48" modifier="sometimes" name="with" negation="false" src="d0_s6" to="o335" />
      <relation from="o335" id="r49" name="extending into" negation="false" src="d0_s6" to="o336" />
    </statement>
    <statement id="d0_s7">
      <text>costa 40–60 (–75) µm wide at base;</text>
      <biological_entity id="o337" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="um" name="width" src="d0_s7" to="75" to_unit="um" />
        <character char_type="range_value" constraint="at base" constraintid="o338" from="40" from_unit="um" name="width" src="d0_s7" to="60" to_unit="um" />
      </biological_entity>
      <biological_entity id="o338" name="base" name_original="base" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>basal laminal cell-walls thick, sometimes pitted.</text>
      <biological_entity constraint="basal laminal" id="o339" name="cell-wall" name_original="cell-walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perichaetia with leaves lanceolate, 0.5–0.8 mm, margins serrulate, apex acuminate, costate.</text>
      <biological_entity id="o340" name="perichaetium" name_original="perichaetia" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o341" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o342" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o343" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="costate" value_original="costate" />
      </biological_entity>
      <relation from="o340" id="r50" name="with" negation="false" src="d0_s9" to="o341" />
    </statement>
    <statement id="d0_s10">
      <text>Seta single, yellow to reddish-brown, 1–3 cm, straight or flexuose, tortuose in aquatic plants.</text>
      <biological_entity id="o344" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="reddish-brown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s10" value="flexuose" value_original="flexuose" />
      </biological_entity>
      <biological_entity id="o345" name="plant" name_original="plants" src="d0_s10" type="structure">
        <character is_modifier="true" name="growth_form_or_habitat" src="d0_s10" value="aquatic" value_original="aquatic" />
      </biological_entity>
      <relation from="o344" id="r51" name="in" negation="false" src="d0_s10" to="o345" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule reddish-brown, 1.5–3 mm, usually shrunken below mouth when dry;</text>
      <biological_entity id="o346" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character constraint="below mouth" constraintid="o347" is_modifier="false" modifier="usually" name="size" src="d0_s11" value="shrunken" value_original="shrunken" />
      </biological_entity>
      <biological_entity id="o347" name="mouth" name_original="mouth" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>exothecial cells rectangular;</text>
      <biological_entity constraint="exothecial" id="o348" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stomata superficial;</text>
      <biological_entity id="o349" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="superficial" value_original="superficial" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus deciduous, 2-seriate or 3-seriate;</text>
      <biological_entity id="o350" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="2-seriate" value_original="2-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="3-seriate" value_original="3-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum conic-apiculate;</text>
      <biological_entity id="o351" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="conic-apiculate" value_original="conic-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth 16, yellowish-brown, lanceolate, external surface cross-striolate proximally, papillose distally, internal surface trabeculate;</text>
      <biological_entity constraint="exostome" id="o352" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="external" id="o353" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="internal" id="o354" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="trabeculate" value_original="trabeculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome pale-yellow, basal membrane high, papillose.</text>
      <biological_entity id="o355" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
      <biological_entity constraint="basal" id="o356" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Calyptra yellow.</text>
      <biological_entity id="o357" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores finely papillose, yellowish-brown.</text>
      <biological_entity id="o358" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s19" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet rock, tree roots, rotting wood, base of trees along streams, waterfalls, seeps, submerged or in splash zone, lowland and montane deciduous or mixed deciduous-coniferous forests, acid and calcareous rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet rock" />
        <character name="habitat" value="tree roots" />
        <character name="habitat" value="wood" modifier="rotting" />
        <character name="habitat" value="base" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="waterfalls" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="splash zone" modifier="submerged or in" />
        <character name="habitat" value="lowland" />
        <character name="habitat" value="montane deciduous" />
        <character name="habitat" value="mixed deciduous-coniferous forests" />
        <character name="habitat" value="acid" />
        <character name="habitat" value="calcareous rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., Que.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Mo., N.H., N.J., N.Y., N.C., Okla., Pa., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Platylomella lescurii is distinguished by its mostly 2-stratose margins, filiform paraphyllia, and saxicolous substrate in streams and seeps. Sciaromium laxirete Abramova &amp; I. I. Abramov, listed provisionally by R. Ochyra (1987) as a synonym of Platylomella lescurii, is known only as a Pliocene fossil from Bashkiria, western Eurasia.</discussion>
  
</bio:treatment>