<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">389</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="illustration_page">390</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Vanderpoorten" date="unknown" rank="family">calliergonaceae</taxon_name>
    <taxon_name authority="(Schimper) Limpricht" date="1899" rank="genus">scorpidium</taxon_name>
    <taxon_name authority="(Hedwig) Limpricht" date="1899" rank="species">scorpioides</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Deutschl.</publication_title>
      <place_in_publication>3: 571. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calliergonaceae;genus scorpidium;species scorpioides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099379</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">scorpioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>295. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species scorpioides</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large or very large, occasionally medium-sized, turgid, green, brown, or often red to blackish red.</text>
      <biological_entity id="o20582" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="shape" src="d0_s0" value="turgid" value_original="turgid" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character char_type="range_value" from="red" modifier="often" name="coloration" src="d0_s0" to="blackish red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely or irregularly pinnate, shoot apices often clawlike;</text>
      <biological_entity id="o20583" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o20584" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="clawlike" value_original="clawlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis partial or complete (more than 25% of stem circumference).</text>
      <biological_entity id="o20585" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="partial" value_original="partial" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves almost orbicular to broadly ovatelanceolate, abruptly or gradually narrowed to apex, suddenly curved distally or rarely straight, strongly concave, (0.7–) 1–2.4 mm wide;</text>
      <biological_entity id="o20586" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s3" to="broadly ovatelanceolate abruptly or gradually narrowed" />
        <character char_type="range_value" constraint="to apex" constraintid="o20587" from="orbicular" name="shape" src="d0_s3" to="broadly ovatelanceolate abruptly or gradually narrowed" />
        <character is_modifier="false" modifier="suddenly" name="course" notes="" src="d0_s3" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="distally" name="course" src="d0_s3" value="or" value_original="or" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s3" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20587" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex obtuse, usually apiculate, acute, or acuminate;</text>
      <biological_entity id="o20588" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa double or occasionally single, rarely ecostate, when single often 2-fid, reaching below to rarely slightly beyond mid leaf;</text>
      <biological_entity constraint="mid" id="o20590" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>alar cells 5–20;</text>
      <biological_entity id="o20589" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="occasionally" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character constraint="rarely slightly beyond mid leaf" constraintid="o20590" is_modifier="false" modifier="rarely; when single often 2-fid; below to rarely" name="position_relational" src="d0_s5" value="reaching" value_original="reaching" />
      </biological_entity>
      <biological_entity id="o20591" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells (shorter 1/2 of leaf) 32–200 (–210) µm, cell ends square, rounded, or short fusiform-narrowed.</text>
      <biological_entity constraint="medial laminal" id="o20592" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="210" to_unit="um" />
        <character char_type="range_value" from="32" from_unit="um" name="some_measurement" src="d0_s7" to="200" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="cell" id="o20593" name="end" name_original="ends" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s7" value="fusiform-narrowed" value_original="fusiform-narrowed" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule with exostome external surface almost entirely cross-striolate to almost entirely dotted proximally.</text>
      <biological_entity id="o20594" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
      <biological_entity id="o20595" name="exostome" name_original="exostome" src="d0_s9" type="structure" />
      <biological_entity constraint="external" id="o20596" name="surface" name_original="surface" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="almost entirely" name="pubescence" src="d0_s9" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="almost entirely; proximally" name="coloration" src="d0_s9" value="dotted" value_original="dotted" />
      </biological_entity>
      <relation from="o20594" id="r2958" name="with" negation="false" src="d0_s9" to="o20595" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fens, pools, lakeshores, submerged in lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fens" />
        <character name="habitat" value="pools" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="lakes" modifier="submerged in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Yukon; Alaska, Colo., Conn., Ind., Maine, Mich., Minn., Mont., Utah, Vt., Wis., Wyo.; South America; Eurasia; Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Scorpidium scorpioides is usually recognized by its large size and strongly concave, broad and usually shortly pointed leaves with a short single or double costa. Straight-leaved phenotypes, which are rare, look very different from the falcate-leaved ones, but, except for leaf curvature, there does not seem to be any difference between these phenotypes. Specimens with straight leaves could be confused with Pseudocalliergon turgescens, and the differences between the two are given under the latter. Although S. scorpioides superficially looks very different from the other two members of the genus, these differences are mainly found in characters, such as size and leaf shape, that affect the appearance of the plant to the naked eye. Molecular evidence and critical microscopic features of, for example, stems, alar cells, and exostomes relate this species to S. cossonii and S. revolvens. Based on molecular evidence, S. scorpioides is an ingroup within S. cossonii, and is thus an example of a species with a number of autapomorphies that are easily visible to the naked eye, in a genus where the two other members have a more generalized appearance.</discussion>
  
</bio:treatment>