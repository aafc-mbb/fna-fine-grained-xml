<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">465</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">scleropodium</taxon_name>
    <taxon_name authority="(Lesquereux) Kindberg" date="1888" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Bryin. Exot.,</publication_title>
      <place_in_publication>35. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus scleropodium;species californicum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099369</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Lesquereux" date="unknown" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>13: 13. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species californicum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, in loose mats, green to yellowish.</text>
      <biological_entity id="o13451" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13452" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o13451" id="r1895" name="in" negation="false" src="d0_s0" to="o13452" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 5 cm, leafy shoots 0.3 mm wide, branches filiform.</text>
      <biological_entity id="o13453" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13454" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character name="width" src="d0_s1" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o13455" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves loosely imbricate, erect when moist, ovatelanceolate, 0.8–1.2 × 0.3–0.4 (–0.5) mm;</text>
      <biological_entity id="o13456" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s2" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrate to serrulate, occasionally subentire;</text>
      <biological_entity id="o13457" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="serrate to serrulate" value_original="serrate to serrulate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex narrowly acute to acuminate, at least some apices long-acute to moderately long-acuminate;</text>
      <biological_entity id="o13458" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="narrowly acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>alar cells irregularly polygonal, size variable, 6–13 µm, walls moderately thick, region extensive, reaching from margin (50–) 100% distance to costa, indistinctly delimited;</text>
      <biological_entity id="o13459" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="long-acute" modifier="at least" name="shape" src="d0_s4" to="moderately long-acuminate" />
      </biological_entity>
      <biological_entity id="o13460" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="polygonal" value_original="polygonal" />
        <character is_modifier="false" name="size" src="d0_s5" value="variable" value_original="variable" />
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s5" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13461" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13462" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="extensive" value_original="extensive" />
        <character is_modifier="false" modifier="indistinctly" name="prominence" notes="" src="d0_s5" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o13463" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity id="o13464" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <relation from="o13462" id="r1896" name="reaching from" negation="false" src="d0_s5" to="o13463" />
      <relation from="o13462" id="r1897" modifier="(50-)100%" name="to" negation="false" src="d0_s5" to="o13464" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 30–65 × 4–6 µm; basal juxtacostal cells elongate, 10–30 × 5–10 (–13) µm, or similar to alar cells.</text>
      <biological_entity constraint="laminal" id="o13465" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s6" to="65" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s6" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o13466" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o13467" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s6" to="30" to_unit="um" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s6" to="13" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s6" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13468" name="alar" name_original="alar" src="d0_s6" type="structure" />
      <biological_entity id="o13469" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <relation from="o13467" id="r1898" name="to" negation="false" src="d0_s6" to="o13468" />
      <relation from="o13467" id="r1899" name="to" negation="false" src="d0_s6" to="o13469" />
    </statement>
    <statement id="d0_s7">
      <text>Seta 1.5 cm, smooth to slightly rough proximally, rough distally.</text>
      <biological_entity id="o13470" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="cm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="smooth to slightly" value_original="smooth to slightly" />
        <character is_modifier="false" modifier="slightly; proximally" name="pubescence_or_relief" src="d0_s7" value="rough" value_original="rough" />
        <character is_modifier="false" modifier="distally" name="pubescence_or_relief" src="d0_s7" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule inclined.</text>
      <biological_entity id="o13471" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="inclined" value_original="inclined" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spores 10–14 µm.</text>
      <biological_entity id="o13472" name="spore" name_original="spores" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s9" to="14" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil on rock in sunny places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" constraint="on rock in sunny places" />
        <character name="habitat" value="rock" constraint="in sunny places" />
        <character name="habitat" value="sunny places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Scleropodium californicum is somewhat similar to Brachythecium albicans but has distinctly thinner shoots. In anatomical details, the species differs in having short laminal cells, opaque basal cell regions, and serrate or serrulate margins, at least distally. From the other species of Scleropodium, it differs in being less julaceous when dry and growing in loose, thin mats. The stems are ascending at their apices; the branches are erect; the basal juxtacostal cells are in 1–3 rows; and the laminal cells have obtuse ends.</discussion>
  
</bio:treatment>