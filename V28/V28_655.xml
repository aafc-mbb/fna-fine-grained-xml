<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="(Hedwig) Schimper in P. Bruch and W. P. Schimper" date="1853" rank="species">albicans</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>6: 23. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species albicans</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002220</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">albicans</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>251. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species albicans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="Cardot &amp; Thériot" date="unknown" rank="species">albicans</taxon_name>
    <taxon_name authority="Renauld &amp; Cardot" date="unknown" rank="variety">occidentale</taxon_name>
    <taxon_hierarchy>genus brachythecium;species albicans;variety occidentale</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">beringianum</taxon_name>
    <taxon_hierarchy>genus b.;species beringianum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="(Hedwig) H. Robinson" date="unknown" rank="species">pseudoalbicans</taxon_name>
    <taxon_hierarchy>genus b.;species pseudoalbicans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chamberlainia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">albicans</taxon_name>
    <taxon_hierarchy>genus chamberlainia;species albicans</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, occasionally quite small, in loose to moderately dense mats, usually pale stramineous, occasionally light green, very rarely deep green.</text>
      <biological_entity id="o14793" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" modifier="occasionally quite" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" modifier="usually" name="coloration" notes="" src="d0_s0" value="pale stramineous" value_original="pale stramineous" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" modifier="very rarely" name="depth" src="d0_s0" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14794" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o14793" id="r2085" name="in" negation="false" src="d0_s0" to="o14794" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 10 (–16) cm, creeping to ascending, terete-foliate, irregularly pinnate, branches to 8 mm, straight to flexuose, terete-foliate.</text>
      <biological_entity id="o14795" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="16" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="creeping" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o14796" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="8" to_unit="mm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s1" to="flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect appressed, straight or slightly falcate, closely imbricate, ovatelanceolate, broadest at 1/7–1/5 leaf length, concave, not or slightly plicate, (1.3–) 1.7–2.8 × (0.4–) 0.6–1.1 mm;</text>
      <biological_entity id="o14797" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="length" src="d0_s2" value="broadest" value_original="broadest" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not; slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_length" src="d0_s2" to="1.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s2" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_width" src="d0_s2" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base tapered, broadly or occasionally narrowly long-decurrent;</text>
      <biological_entity id="o14798" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="broadly; occasionally narrowly" name="shape" src="d0_s3" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or often recurved almost throughout, entire, rarely serrulate;</text>
      <biological_entity id="o14799" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="often; almost throughout; throughout" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex gradually tapered to acumen, acumen long;</text>
      <biological_entity id="o14800" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character constraint="to acumen" constraintid="o14801" is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o14801" name="acumen" name_original="acumen" src="d0_s5" type="structure" />
      <biological_entity id="o14802" name="acumen" name_original="acumen" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 40–70% leaf length, slender, terminal spine absent;</text>
      <biological_entity id="o14803" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o14804" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="40-70%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells subquadrate to short-rectangular, small, 15–30 × 11–15 µm, walls moderately thick, region extensive, of 10–15 × 6–8 cells;</text>
      <biological_entity constraint="terminal" id="o14805" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14806" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="11" from_unit="um" name="width" src="d0_s7" to="15" to_unit="um" />
      </biological_entity>
      <biological_entity id="o14807" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o14808" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o14809" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <relation from="o14808" id="r2086" name="consist_of" negation="false" src="d0_s7" to="o14809" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear, 40–100 (–125) × 5–9 µm; basal-cells 10–15 µm wide, region in 3 rows.</text>
      <biological_entity constraint="laminal" id="o14810" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s8" to="125" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s8" to="100" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity id="o14811" name="basal-cell" name_original="basal-cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s8" to="15" to_unit="um" />
      </biological_entity>
      <biological_entity id="o14812" name="region" name_original="region" src="d0_s8" type="structure" />
      <biological_entity id="o14813" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <relation from="o14812" id="r2087" name="in" negation="false" src="d0_s8" to="o14813" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves similar, smaller.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="branch" id="o14814" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta reddish orange, 1–2 cm, smooth.</text>
      <biological_entity id="o14815" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish orange" value_original="reddish orange" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined to horizontal, reddish orange, elongate, curved, 2 mm;</text>
      <biological_entity id="o14816" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s12" to="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish orange" value_original="reddish orange" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>annulus separating by fragments;</text>
      <biological_entity id="o14817" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character constraint="by fragments" constraintid="o14818" is_modifier="false" name="arrangement" src="d0_s13" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o14818" name="fragment" name_original="fragments" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>operculum long-conic.</text>
      <biological_entity id="o14819" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="long-conic" value_original="long-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores dimorphic: 15–18 µm or 11–13 µm; these two types in proportion 1: 1, as tetrads have two big and two small spores.</text>
      <biological_entity id="o14820" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s15" to="18" to_unit="um" />
        <character name="some_measurement" src="d0_s15" value="11-13 um" value_original="11-13 um" />
        <character constraint="in tetrads" constraintid="o14821" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14821" name="tetrad" name_original="tetrads" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o14822" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s15" value="big" value_original="big" />
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s15" value="small" value_original="small" />
      </biological_entity>
      <relation from="o14821" id="r2088" name="have" negation="false" src="d0_s15" to="o14822" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, sandy, open and grassy places, rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="open" />
        <character name="habitat" value="grassy places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr., N.S., Que.; Alaska, Calif., Colo., Idaho, Mont., Oreg., S.Dak., Wash., Wyo.; Mexico (Nuevo León); South America; Europe; n Asia; Atlantic Islands; Pacific Islands (New Zealand); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Despite numerous records and misidentified herbarium collections from many of the states, Brachythecium albicans is common only in the Pacific coast region and the mountains of the West. The species is distinguished by the pale color, julaceous foliage, filiform and somewhat secund acumina, entire leaf margins that are often recurved along most of the leaf length, and elongate alar region. This species is variable, often producing very thin shoots, with stem leaves as small as 1.3 × 0.4 mm, but usually such collections include some larger shoots that imply more typical B. albicans. Branches of specimens from California can be as long as 18 mm. Differentiation from the Arctic 15. B. coruscum is discussed under that species. The name B. albicans is used, despite preliminary molecular data that demonstrate differences between American and northeastern Asian populations from European plants; if further study proves that they are not conspecific, then the name B. pseudoalbicans should be used for at least the populations from western North America.</discussion>
  
</bio:treatment>