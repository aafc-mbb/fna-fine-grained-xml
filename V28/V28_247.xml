<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">155</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">132</other_info_on_meta>
    <other_info_on_meta type="mention_page">173</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">PTYCHOSTOMUM</taxon_name>
    <place_of_publication>
      <publication_title>Syll. Pl. Nov.</publication_title>
      <place_in_publication>1: 62. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus PTYCHOSTOMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek ptychos, fold, and stoma, mouth, alluding to pleated appearance of capsule mouth</other_info_on_name>
    <other_info_on_name type="fna_id">127600</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to very large, in dense or open turfs, red, pink, yellow-green, or brown-green.</text>
      <biological_entity id="o1786" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small to very" value_original="small to very" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown-green" value_original="brown-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown-green" value_original="brown-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1787" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o1786" id="r242" name="in" negation="false" src="d0_s0" to="o1787" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–4 (–12) cm, tufted, comose or evenly foliate, freely branching by subfloral innovations;</text>
      <biological_entity id="o1788" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
        <character constraint="by innovations" constraintid="o1789" is_modifier="false" modifier="freely" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o1789" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="subfloral" value_original="subfloral" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids few-to-many, micronemata and macronemata present.</text>
      <biological_entity id="o1790" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s2" to="many" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves weakly to strongly contorted or shrunken when dry, erect to erect-spreading when moist, ovate, ovatelanceolate, or orbicular, flat to concave, (0.5–) 1–4 (–5) mm;</text>
      <biological_entity id="o1791" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly to strongly" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="size" src="d0_s3" value="shrunken" value_original="shrunken" />
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s3" to="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="orbicular flat" name="shape" src="d0_s3" to="concave" />
        <character char_type="range_value" from="orbicular flat" name="shape" src="d0_s3" to="concave" />
        <character char_type="range_value" from="orbicular flat" name="shape" src="d0_s3" to="concave" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base decurrent or not;</text>
      <biological_entity id="o1792" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character name="shape" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane or revolute, entire to denticulate distally, 1-stratose or 2-stratose, limbidium usually present;</text>
      <biological_entity id="o1793" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="revolute entire" name="shape" src="d0_s5" to="denticulate" />
        <character char_type="range_value" from="revolute entire" modifier="distally" name="shape" src="d0_s5" to="denticulate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o1794" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex obtuse to acuminate;</text>
      <biological_entity id="o1795" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa usually percurrent to long-excurrent, awn smooth or denticulate, guide cells usually present in 1 layer;</text>
      <biological_entity id="o1796" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually percurrent" name="architecture" src="d0_s7" to="long-excurrent" />
      </biological_entity>
      <biological_entity id="o1797" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o1799" name="layer" name_original="layer" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o1798" id="r243" modifier="usually" name="present in" negation="false" src="d0_s7" to="o1799" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells not differentiated;</text>
      <biological_entity constraint="guide" id="o1798" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o1800" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal areolation heterogeneous;</text>
      <biological_entity constraint="laminal" id="o1801" name="areolation" name_original="areolation" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="heterogeneous" value_original="heterogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells gradually short to long-rectangular, usually longer than more distal cells, 2–4: 1;</text>
      <biological_entity constraint="proximal laminal" id="o1802" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="gradually" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s10" value="long-rectangular" value_original="long-rectangular" />
        <character constraint="than more distal cells" constraintid="o1803" is_modifier="false" name="length_or_size" src="d0_s10" value="usually longer" value_original="usually longer" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1803" name="cell" name_original="cells" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>medial and distal cells rhomboidal to hexagonal, usually 2–4: 1, walls thin to very incrassate, porose or not.</text>
      <biological_entity constraint="medial and distal" id="o1804" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="rhomboidal" name="shape" src="d0_s11" to="hexagonal" />
        <character char_type="range_value" from="2" modifier="usually" name="quantity" src="d0_s11" to="4" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction rare, by filiform gemmae in stem-leaf axils and on rhizoids.</text>
      <biological_entity id="o1806" name="rhizoid" name_original="rhizoids" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="rare" value_original="rare" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous, synoicous, autoicous, or polyoicous;</text>
      <biological_entity id="o1805" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="porose" value_original="porose" />
        <character name="architecture" src="d0_s11" value="not" value_original="not" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="polyoicous" value_original="polyoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="polyoicous" value_original="polyoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perigonia and perichaetia terminal;</text>
      <biological_entity id="o1807" name="perigonium" name_original="perigonia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o1808" name="perichaetium" name_original="perichaetia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perigonial and perichaetial leaves somewhat differentiated, outer leaves somewhat enlarged and more acuminate, inner leaves smaller, narrowly ovatelanceolate to triangular.</text>
      <biological_entity constraint="perigonial and perichaetial" id="o1809" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="somewhat" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1810" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s15" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o1811" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="narrowly ovatelanceolate" name="shape" src="d0_s15" to="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seta single, straight to flexuose.</text>
      <biological_entity id="o1812" name="seta" name_original="seta" src="d0_s16" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s16" value="single" value_original="single" />
        <character char_type="range_value" from="straight" name="course" src="d0_s16" to="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsule highly variable, suberect, inclined, or nutant, ovate, obovate, pyriform, clavate, or turbinate, 2–6 (–7) mm;</text>
      <biological_entity id="o1813" name="capsule" name_original="capsule" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="highly" name="variability" src="d0_s17" value="variable" value_original="variable" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="nutant" value_original="nutant" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="nutant" value_original="nutant" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>hypophysis slender or thick;</text>
      <biological_entity id="o1814" name="hypophysis" name_original="hypophysis" src="d0_s18" type="structure">
        <character is_modifier="false" name="size" src="d0_s18" value="slender" value_original="slender" />
        <character is_modifier="false" name="width" src="d0_s18" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum conic to low-convex, rounded to apiculate;</text>
      <biological_entity id="o1815" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s19" to="low-convex rounded" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s19" to="low-convex rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>peristome double;</text>
      <biological_entity id="o1816" name="peristome" name_original="peristome" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>exostome yellow, orange, or brown basally, usually hyaline distally, teeth well developed, lanceolate;</text>
      <biological_entity id="o1817" name="exostome" name_original="exostome" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="usually; distally" name="coloration" src="d0_s21" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o1818" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s21" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s21" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endostome sometimes adherent to exostome at base, basal membrane high, segments usually well developed, narrowly to broadly perforated, cilia variously short and appendiculate, or absent.</text>
      <biological_entity id="o1819" name="endostome" name_original="endostome" src="d0_s22" type="structure">
        <character constraint="to exostome" constraintid="o1820" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s22" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o1820" name="exostome" name_original="exostome" src="d0_s22" type="structure" />
      <biological_entity id="o1821" name="base" name_original="base" src="d0_s22" type="structure" />
      <biological_entity constraint="basal" id="o1822" name="membrane" name_original="membrane" src="d0_s22" type="structure">
        <character is_modifier="false" name="height" src="d0_s22" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o1823" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s22" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="narrowly to broadly" name="architecture" src="d0_s22" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o1824" name="cilium" name_original="cilia" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="variously" name="height_or_length_or_size" src="d0_s22" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="appendiculate" value_original="appendiculate" />
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o1820" id="r244" name="at" negation="false" src="d0_s22" to="o1821" />
    </statement>
    <statement id="d0_s23">
      <text>Spores shed singly, size often variable in same collection and capsule, 10–50 µm, smooth to distinctly papillose, yellow, brown, black, or green.</text>
      <biological_entity id="o1825" name="spore" name_original="spores" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="singly" value_original="singly" />
        <character constraint="in capsule" constraintid="o1826" is_modifier="false" modifier="often" name="size" src="d0_s23" value="variable" value_original="variable" />
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" notes="" src="d0_s23" to="50" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s23" to="distinctly papillose" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1826" name="capsule" name_original="capsule" src="d0_s23" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; mostly in Northern Hemisphere in arctic, boreal and alpine regions, plus the Southern Hemisphere in cold-temperate to subantarctic regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="mostly in Northern Hemisphere in arctic" establishment_means="native" />
        <character name="distribution" value="boreal and alpine regions" establishment_means="native" />
        <character name="distribution" value="plus the Southern Hemisphere in cold-temperate to subantarctic regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 60 (31 in the flora).</discussion>
  <discussion>Species of Ptychostomum exhibit a primary radiation in the Northern Hemisphere, where they form a maze of polyploid arctic-boreal populations that exhibit high variability and often grade into one another. The plants occur on soil, mud, peat, or less commonly on rock or wood. Many species are distinguished by minor differences in capsule shape, operculum development, and peristome structure. The two main subgenera of Ptychostomum reflect those species with relatively short proximal laminal cells, inflated subalar cells, 1-stratose limbidium, and comose stems (subg. Cladodium), and those with more elongate foliate stems, not particularly comose, with long-rectangular proximal laminal cells, a thin, partially 2-stratose border, and non-inflated subalar cells (subg. Ptychostomum). This treatment does not make use of taxonomic sections since there is considerable confusion over the correct names, with many invalidly published. A. L. Andrews (1935), E. Nyholm (1986+, fasc. 3), A. J. E. Smith (2004), and V. I. Zolotov (2000) provided valuable treatments of most species in the flora area. For many species, mature capsules, spores, and sexual condition are needed for correct determination.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf bases red or pink; distal leaves usually somewhat enlarged; limbidium distinct to indistinct, 1-stratose; distal laminal cells not lax, usually 3-4:1, rhomboidal to hexagonal; proximal cells same length as medial, sometimes longer, rectangular, rarely short-rectangular; subalar cells inflated, pink, on gametoecial and inner comal leaves.</description>
      <determination>10a Ptychostomum subg. Cladodium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf bases green, or if red then same color as rest of leaf; distal leaves sometimes enlarged; limbidium indistinct, partially 2-stratose (1-stratose in P. marratii); distal laminal cells lax, 2-4:1, short-rhomboidal; proximal cells longer than medial, rectangular; subalar cells not inflated or pink.</description>
      <determination>10b Ptychostomum subg. Ptychostomum</determination>
    </key_statement>
  </key>
</bio:treatment>