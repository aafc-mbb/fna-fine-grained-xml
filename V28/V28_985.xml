<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Clayton C. Newberry</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">584</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="mention_page">624</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leptodontaceae</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="genus">ALSIA</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>3: 184. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leptodontaceae;genus ALSIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Anagram of generic name Lasia (now Forsstroemia), alluding to similarity</other_info_on_name>
    <other_info_on_name type="fna_id">101197</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Secondary stems pinnate and frondiform distally;</text>
      <biological_entity constraint="secondary" id="o9760" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s0" value="frondiform" value_original="frondiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>paraphyllia present;</text>
      <biological_entity id="o9761" name="paraphyllium" name_original="paraphyllia" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia dissected-lanceolate to subfoliose.</text>
      <biological_entity id="o9762" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="dissected-lanceolate" value_original="dissected-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves loosely imbricate when dry, spreading when moist, lanceolate to oblong-lanceolate;</text>
      <biological_entity id="o9763" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblong-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins strongly recurved at base, plane at apex, entire or sometimes weakly serrulate at apex;</text>
      <biological_entity id="o9764" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="at base" constraintid="o9765" is_modifier="false" modifier="strongly" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character constraint="at apex" constraintid="o9766" is_modifier="false" name="shape" notes="" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="at apex" constraintid="o9767" is_modifier="false" modifier="sometimes weakly" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o9765" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o9766" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o9767" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o9768" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa variable on same plant, weak and almost ecostate, double and short, or strong and disappearing mid leaf;</text>
      <biological_entity id="o9770" name="plant" name_original="plant" src="d0_s6" type="structure" />
      <biological_entity constraint="mid" id="o9771" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" name="fragility" src="d0_s6" value="strong" value_original="strong" />
        <character is_modifier="true" name="prominence" src="d0_s6" value="disappearing" value_original="disappearing" />
      </biological_entity>
      <relation from="o9769" id="r1389" modifier="almost" name="double" negation="false" src="d0_s6" to="o9771" />
    </statement>
    <statement id="d0_s7">
      <text>alar region filling basal angles, extending up margins to 1/2 leaf length;</text>
      <biological_entity id="o9769" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character constraint="on plant" constraintid="o9770" is_modifier="false" name="variability" src="d0_s6" value="variable" value_original="variable" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s6" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o9772" name="region" name_original="region" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9773" name="angle" name_original="angles" src="d0_s7" type="structure" />
      <biological_entity id="o9774" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o9775" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <relation from="o9772" id="r1390" name="filling" negation="false" src="d0_s7" to="o9773" />
      <relation from="o9772" id="r1391" name="extending up" negation="false" src="d0_s7" to="o9774" />
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells oval-oblong.</text>
      <biological_entity constraint="medial laminal" id="o9776" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oval-oblong" value_original="oval-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves smaller, narrower.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="branch" id="o9777" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaf apex filiform-acuminate.</text>
      <biological_entity constraint="leaf" id="o9778" name="apex" name_original="apex" src="d0_s11" type="structure" constraint_original="perichaetial leaf">
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform-acuminate" value_original="filiform-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 3–5 mm.</text>
      <biological_entity id="o9779" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule erect-symmetric or nearly so, barely exserted beyond perichaetial leaves, oblong-cylindric;</text>
      <biological_entity id="o9780" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="erect-symmetric" value_original="erect-symmetric" />
        <character name="architecture_or_shape" src="d0_s13" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindric" value_original="oblong-cylindric" />
      </biological_entity>
      <biological_entity id="o9781" name="perichaetial" name_original="perichaetial" src="d0_s13" type="structure" />
      <biological_entity id="o9782" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <relation from="o9780" id="r1392" modifier="barely" name="exserted beyond" negation="false" src="d0_s13" to="o9781" />
      <relation from="o9780" id="r1393" modifier="barely" name="exserted beyond" negation="false" src="d0_s13" to="o9782" />
    </statement>
    <statement id="d0_s14">
      <text>stomata basal, sunken;</text>
      <biological_entity id="o9783" name="stoma" name_original="stomata" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="basal" value_original="basal" />
        <character is_modifier="false" name="prominence" src="d0_s14" value="sunken" value_original="sunken" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum oblique-rostrate;</text>
      <biological_entity id="o9784" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblique-rostrate" value_original="oblique-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth free, broadly subulate, punctulate-scabrous;</text>
      <biological_entity constraint="exostome" id="o9785" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s16" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="punctulate-scabrous" value_original="punctulate-scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome well developed.</text>
      <biological_entity id="o9786" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s17" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Calyptra naked.</text>
      <biological_entity id="o9787" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 20 µm, light-brown.</text>
      <biological_entity id="o9788" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character name="some_measurement" src="d0_s19" unit="um" value="20" value_original="20" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Alsia is strikingly similar to Forsstroemia trichomitria, but the two taxa are completely distinct geographically. Alsia is endemic to the west coast of North America, while F. trichomitria is broadly distributed in eastern North America, occurring no further west than Oklahoma. The stems of Alsia are densely invested with paraphyllia, while paraphyllia are absent in F. trichomitria.</discussion>
  <references>
    <reference>Manuel, M. G. 1974. A revised classification of the Leucodontaceae and a revision of the subfamily Alsioideae. Bryologist 77: 531–550.</reference>
  </references>
  
</bio:treatment>