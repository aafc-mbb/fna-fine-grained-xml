<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">638</other_info_on_meta>
    <other_info_on_meta type="mention_page">637</other_info_on_meta>
    <other_info_on_meta type="mention_page">639</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">theliaceae</taxon_name>
    <taxon_name authority="Sullivant in A. Gray" date="unknown" rank="genus">THELIA</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 660. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family theliaceae;genus THELIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek thele, nipple, alluding to prominent leaf papillae</other_info_on_name>
    <other_info_on_name type="fna_id">132762</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized.</text>
      <biological_entity id="o14360" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1-pinnate or irregularly branched, branches erect-ascending, simple or irregularly branched;</text>
      <biological_entity id="o14361" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o14362" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>epidermal-cells elongate;</text>
      <biological_entity id="o14363" name="epidermal-cell" name_original="epidermal-cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rhizoids reddish-brown, densely branched, smooth.</text>
      <biological_entity id="o14364" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves deltoid-ovate;</text>
      <biological_entity id="o14365" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="deltoid-ovate" value_original="deltoid-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base decurrent;</text>
      <biological_entity id="o14366" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex broadly acute to obtuse, abruptly contracted, acumen long, apiculate or piliferous;</text>
      <biological_entity id="o14367" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s6" to="obtuse" />
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o14368" name="acumen" name_original="acumen" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s6" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="piliferous" value_original="piliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa single or sometimes double, 1/2–3/4 leaf length, sometimes spurred or 2-fid;</text>
      <biological_entity id="o14369" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="single" value_original="single" />
        <character name="quantity" src="d0_s7" value="sometimes" value_original="sometimes" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s7" to="3/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>alar cells quadrate or subrectangular;</text>
      <biological_entity id="o14370" name="leaf" name_original="leaf" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="length" src="d0_s7" value="spurred" value_original="spurred" />
        <character is_modifier="false" name="length" src="d0_s7" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o14371" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subrectangular" value_original="subrectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal cells rhombic to fusiform, stoutly 1-papillose abaxially, papillae columnar, simple or 3–6-branched.</text>
      <biological_entity constraint="laminal" id="o14372" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s9" to="fusiform" />
        <character is_modifier="false" modifier="stoutly; abaxially" name="relief" src="d0_s9" value="1-papillose" value_original="1-papillose" />
      </biological_entity>
      <biological_entity id="o14373" name="papilla" name_original="papillae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="columnar" value_original="columnar" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-6-branched" value_original="3-6-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta yellowish when young, red when mature, flexuose when dry, smooth.</text>
      <biological_entity id="o14374" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s10" value="red" value_original="red" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s10" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule cylindric to ovoid-cylindric, symmetric to somewhat curved, smooth to weakly wrinkled when dry;</text>
      <biological_entity id="o14375" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s11" to="ovoid-cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character char_type="range_value" from="smooth" modifier="when dry" name="relief" src="d0_s11" to="weakly wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exothecial cells at mouth oblate to quadrate, proximal cells quadrate to short-rectangular, enlarged, red, walls thin to thick;</text>
      <biological_entity constraint="exothecial" id="o14376" name="cell" name_original="cells" src="d0_s12" type="structure" />
      <biological_entity id="o14377" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character char_type="range_value" from="oblate" name="shape" src="d0_s12" to="quadrate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14378" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s12" to="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o14379" name="wall" name_original="walls" src="d0_s12" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s12" to="thick" />
      </biological_entity>
      <relation from="o14376" id="r2011" name="at" negation="false" src="d0_s12" to="o14377" />
    </statement>
    <statement id="d0_s13">
      <text>stomata on neck;</text>
      <biological_entity id="o14380" name="stoma" name_original="stomata" src="d0_s13" type="structure" />
      <biological_entity id="o14381" name="neck" name_original="neck" src="d0_s13" type="structure" />
      <relation from="o14380" id="r2012" name="on" negation="false" src="d0_s13" to="o14381" />
    </statement>
    <statement id="d0_s14">
      <text>annulus rudimentary, persistent, 2-seriate or 3-seriate, cell-walls thin;</text>
      <biological_entity id="o14382" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="2-seriate" value_original="2-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="3-seriate" value_original="3-seriate" />
      </biological_entity>
      <biological_entity id="o14383" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="false" name="width" src="d0_s14" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum conic-subulate;</text>
      <biological_entity id="o14384" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic-subulate" value_original="conic-subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth united at base on short, smooth membrane, white, linear, external surface smooth basally, finely papillose distally, trabeculae and median line thickened, internal surface lightly papillose, trabeculae weak;</text>
      <biological_entity constraint="exostome" id="o14385" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character constraint="at base" constraintid="o14386" is_modifier="false" name="fusion" src="d0_s16" value="united" value_original="united" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o14386" name="base" name_original="base" src="d0_s16" type="structure" />
      <biological_entity id="o14387" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="external" id="o14388" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely; distally" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o14389" name="trabecula" name_original="trabeculae" src="d0_s16" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="median" id="o14390" name="line" name_original="line" src="d0_s16" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s16" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="internal" id="o14391" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="lightly" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o14392" name="trabecula" name_original="trabeculae" src="d0_s16" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s16" value="weak" value_original="weak" />
      </biological_entity>
      <relation from="o14386" id="r2013" name="on" negation="false" src="d0_s16" to="o14387" />
    </statement>
    <statement id="d0_s17">
      <text>endostome rudimentary, white, lightly papillose, basal membrane low or high, segments reduced to stubs or to 1/2 exostome length, cilia 1 or 2, nublike or absent.</text>
      <biological_entity id="o14393" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" modifier="lightly" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14394" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="low" value_original="low" />
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o14395" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character constraint="to stubs, exostome" constraintid="o14396, o14397" is_modifier="false" name="size" src="d0_s17" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o14396" name="stub" name_original="stubs" src="d0_s17" type="structure" />
      <biological_entity id="o14397" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity id="o14398" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s17" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="length" src="d0_s17" value="nublike" value_original="nublike" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores smooth to lightly papillose.</text>
      <biological_entity id="o14399" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="smooth" name="relief" src="d0_s18" to="lightly papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, ne Mexico, West Indies (Dominican Republic).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>Thelia is remarkable in its glaucous, bluish green color, thick, densely tomentose mats, and tumid to julaceous stems and branches. The stem leaves are distinctively deltoid-ovate with rhombic cells, simple or 3–6-branched columnar papillae, and frequently long-ciliate margins. The sporophyte has a long seta and typically erect, symmetric, cylindric to ovoid-cylindric capsules. The diplolepidous peristome is yellowish white. The endostome segments and cilia are often reduced to nublike projections on a low or high basal membrane.</discussion>
  <discussion>In the field, Thelia could be confused with Myurella (Pterigynandraceae) because both genera have terete-foliate stems and branches that occur in mats or cushions, radiculose stems, and a similar color. Myurella rarely occurs in dense mats, and its leaves have short, double costae, serrate or sinuate-dentate margins, and lower, unbranched papillae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaf laminal cell papillae simple.</description>
      <determination>2 Thelia hirtella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaf laminal cell papillae usually 3-6-branched</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems creeping, densely and regularly 1-pinnate, often densely tomentose, branches simple; leaf acumina usually spreading-piliferous.</description>
      <determination>1 Thelia asprella</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems often ascending, sparsely and irregularly branched, lightly tomentose, branches irregularly branched; leaf acumina erect, apiculate.</description>
      <determination>3 Thelia lescurii</determination>
    </key_statement>
  </key>
</bio:treatment>