<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Bruch ex Bridel" date="1827" rank="species">pallens</taxon_name>
    <taxon_name authority="Vitt" date="1971" rank="variety">johnseniae</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>74: 162, figs. 13 – 16. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species pallens;variety johnseniae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250061912</other_info_on_name>
  </taxon_identification>
  <number>26c.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem-leaves broadly lanceolate to lanceolate;</text>
      <biological_entity id="o9336" name="stem-leaf" name_original="stem-leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s0" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>margins entire;</text>
      <biological_entity id="o9337" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>apex narrowly obtuse or bluntly acute.</text>
      <biological_entity id="o9338" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="bluntly" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Seta 0.5–1 mm.</text>
      <biological_entity id="o9339" name="seta" name_original="seta" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Capsule oblong-cylindric, 1.5–2 mm;</text>
      <biological_entity id="o9340" name="capsule" name_original="capsule" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>exostome teeth recurved-reflexed, finely papillose;</text>
      <biological_entity constraint="exostome" id="o9341" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="recurved-reflexed" value_original="recurved-reflexed" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s5" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>endostome segments 8, of 1 row of cells, shorter than exostome, smooth.</text>
      <biological_entity constraint="endostome" id="o9342" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="8" value_original="8" />
        <character constraint="than exostome" constraintid="o9345" is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o9343" name="row" name_original="row" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9344" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o9345" name="exostome" name_original="exostome" src="d0_s6" type="structure" />
      <relation from="o9342" id="r1318" name="consist_of" negation="false" src="d0_s6" to="o9343" />
      <relation from="o9343" id="r1319" name="part_of" negation="false" src="d0_s6" to="o9344" />
    </statement>
    <statement id="d0_s7">
      <text>Calyptra sparsely hairy.</text>
      <biological_entity id="o9346" name="calyptra" name_original="calyptra" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spores 13–20 µm.</text>
      <biological_entity id="o9347" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s8" to="20" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Basaltic boulders in spruce-popular forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basaltic boulders" constraint="in spruce-popular forests" />
        <character name="habitat" value="spruce-popular forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="3000" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety johnseniae is distinguished by its hairy calyptrae. The capsules have eight endostome segments and are shorter and moderately constricted below the mouth. The stomata are barely covered with subsidiary cells which usually have their interior walls thickened, and the exostome teeth tend to be somewhat recurved instead of reflexed.</discussion>
  
</bio:treatment>