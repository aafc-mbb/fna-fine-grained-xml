<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Terry T. McIntosh,Steven G. Newmaster</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">LEUCOLEPIS</taxon_name>
    <place_of_publication>
      <publication_title>Not. Sällsk. Fauna Fl. Fenn. Förh.</publication_title>
      <place_in_publication>9: 80. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus LEUCOLEPIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leucos, white, and lepis, scale, alluding to stem leaves</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">118402</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–8 (–10) cm, in tufts or mats.</text>
      <biological_entity id="o23089" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23090" name="tuft" name_original="tufts" src="d0_s0" type="structure" />
      <biological_entity id="o23091" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o23089" id="r3286" name="in" negation="false" src="d0_s0" to="o23090" />
      <relation from="o23089" id="r3287" name="in" negation="false" src="d0_s0" to="o23091" />
    </statement>
    <statement id="d0_s1">
      <text>Stems reddish-brown to black, erect, branched distally, dendroid;</text>
      <biological_entity id="o23092" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="reddish-brown" name="coloration" src="d0_s1" to="black" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids brown, macronemata matted proximally, occasionally along stem into branched portion of stem, micronemata absent.</text>
      <biological_entity id="o23093" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="proximally" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
        <character is_modifier="false" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23094" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <biological_entity id="o23095" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o23096" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o23093" id="r3288" modifier="occasionally" name="along" negation="false" src="d0_s2" to="o23094" />
      <relation from="o23094" id="r3289" name="into" negation="false" src="d0_s2" to="o23095" />
      <relation from="o23095" id="r3290" name="part_of" negation="false" src="d0_s2" to="o23096" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves dimorphic, pale to dark green or reddish-brown to hyaline, slightly contorted or crisped when dry, appressed or spreading, flat or keeled when moist, ovatelanceolate, (1.3–) 2–3 (–4) mm;</text>
      <biological_entity id="o23097" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s3" value="dimorphic" value_original="dimorphic" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s3" to="dark green or reddish-brown" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base decurrent;</text>
      <biological_entity id="o23098" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane, green, hyaline, or reddish-brown, 1-stratose, usually toothed to near base, teeth single, sharp;</text>
      <biological_entity id="o23099" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character constraint="to base" constraintid="o23100" is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o23100" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o23101" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute or long-acuminate, cuspidate or not;</text>
      <biological_entity id="o23102" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="cuspidate" value_original="cuspidate" />
        <character name="architecture_or_shape" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa subpercurrent or percurrent, distal abaxial surface strongly toothed;</text>
      <biological_entity id="o23103" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o23104" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells elongate, short-elongate, short-rhombic, or ± isodiametric, 17–50 µm, sometimes in longitudinal or diagonal rows, not or weakly collenchymatous, walls not pitted;</text>
      <biological_entity constraint="medial laminal" id="o23105" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-rhombic" value_original="short-rhombic" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-rhombic" value_original="short-rhombic" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="17" from_unit="um" name="some_measurement" src="d0_s8" to="50" to_unit="um" />
        <character is_modifier="false" modifier="not; weakly" name="architecture" notes="" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o23106" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o23107" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o23105" id="r3291" modifier="sometimes" name="in" negation="false" src="d0_s8" to="o23106" />
    </statement>
    <statement id="d0_s9">
      <text>marginal cells differentiated, linear, short-linear, or rhomboidal, in 1 or 2 rows.</text>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o23108" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta single or rarely double (or 3), reddish-brown, 4–5 cm, somewhat flexuose.</text>
      <biological_entity id="o23109" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character name="quantity" src="d0_s12" value="rarely" value_original="rarely" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s12" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s12" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule pendent, yellowbrown or brown, cylindric, 6–8 mm;</text>
      <biological_entity id="o23110" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum hemispheric;</text>
      <biological_entity id="o23111" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome yellowbrown;</text>
      <biological_entity id="o23112" name="exostome" name_original="exostome" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome yellowbrown.</text>
      <biological_entity id="o23113" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 28–30 µm.</text>
      <biological_entity id="o23114" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="28" from_unit="um" name="some_measurement" src="d0_s17" to="30" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Leucolepis is distinguished by a dendroid growth form and strongly differentiated stem leaves. Morphological (T. J. Koponen 1968) and cytological (R. J. Lowry 1948; W. C. Steere et al. 1954) research supports the split of Leucolepis from Mnium. The chromosome number of Leucolepis is n = 5, whereas Mnium is n = 6, 7, and 12.</discussion>
  
</bio:treatment>