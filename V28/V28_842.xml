<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">542</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="mention_page">539</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">imponens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>290, plate 77, figs. 1–5. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus hypnum;species imponens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250064857</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stereodon</taxon_name>
    <taxon_name authority="(Hedwig) Mitten" date="unknown" rank="species">imponens</taxon_name>
    <taxon_hierarchy>genus stereodon;species imponens</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, golden to yellow-green or brownish.</text>
      <biological_entity id="o21519" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="golden" name="coloration" src="d0_s0" to="yellow-green or brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–10 cm, reddish-brown, creeping, regularly pinnate, occasionally partly 2-pinnate or irregularly branched, branches 0.3–1.2 cm;</text>
      <biological_entity id="o21520" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="occasionally partly" name="architecture" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21521" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s1" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand weak;</text>
      <biological_entity id="o21522" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o21523" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia lanceolate or foliose, margins usually incised.</text>
      <biological_entity id="o21524" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
      </biological_entity>
      <biological_entity id="o21525" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves falcate-secund, triangular-ovate to oblong-lanceolate, gradually narrowed to apex, 1.8–2 × 0.6–0.8 mm;</text>
      <biological_entity id="o21526" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="falcate-secund" value_original="falcate-secund" />
        <character char_type="range_value" from="triangular-ovate" name="shape" src="d0_s4" to="oblong-lanceolate" />
        <character constraint="to apex" constraintid="o21527" is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" notes="" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" notes="" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21527" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>base somewhat decurrent, not auriculate;</text>
      <biological_entity id="o21528" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane or weakly recurved basally, serrulate distally or rarely nearly entire;</text>
      <biological_entity id="o21529" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="weakly; basally" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s6" value="or" value_original="or" />
        <character is_modifier="false" modifier="rarely nearly" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>acumen slender;</text>
      <biological_entity id="o21530" name="acumen" name_original="acumen" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa double, short, or indistinct;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar cells subquadrate, basalmost cells deep orangebrown, outer cells hyaline, region weakly defined, not to weakly excavate, 5–10 cells high on margin;</text>
      <biological_entity id="o21531" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o21532" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
      <biological_entity constraint="basalmost" id="o21533" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="depth" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21534" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o21535" name="region" name_original="region" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="weakly" name="prominence" src="d0_s9" value="defined" value_original="defined" />
        <character is_modifier="false" modifier="not to weakly" name="architecture" src="d0_s9" value="excavate" value_original="excavate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <biological_entity id="o21536" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character constraint="on margin" constraintid="o21537" is_modifier="false" name="height" src="d0_s9" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o21537" name="margin" name_original="margin" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>basal laminal cells broader than medial cells, yellow-orange to orangebrown, walls pitted;</text>
      <biological_entity constraint="basal laminal" id="o21538" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="than medial cells" constraintid="o21539" is_modifier="false" name="width" src="d0_s10" value="broader" value_original="broader" />
        <character char_type="range_value" from="yellow-orange" name="coloration" src="d0_s10" to="orangebrown" />
      </biological_entity>
      <biological_entity constraint="medial" id="o21539" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <biological_entity id="o21540" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>medial cells 50–70 (–80) × 3–4 (–5) µm, walls sometimes porose, not pitted.</text>
      <biological_entity constraint="medial" id="o21541" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s11" to="80" to_unit="um" />
        <character char_type="range_value" from="50" from_unit="um" name="length" src="d0_s11" to="70" to_unit="um" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s11" to="5" to_unit="um" />
        <character char_type="range_value" from="3" from_unit="um" name="width" src="d0_s11" to="4" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o21542" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="porose" value_original="porose" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner perichaetial leaves oblong-lanceolate, margins serrate in apex, apex long, costa indistinct.</text>
      <biological_entity constraint="inner perichaetial" id="o21543" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o21544" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character constraint="in apex" constraintid="o21545" is_modifier="false" name="architecture_or_shape" src="d0_s13" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o21545" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <biological_entity id="o21546" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o21547" name="costa" name_original="costa" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seta redbrown, 1–3 cm.</text>
      <biological_entity id="o21548" name="seta" name_original="seta" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule erect to slightly inclined, redbrown, cylindric, 1.5–3 mm;</text>
      <biological_entity id="o21549" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s15" to="slightly inclined" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>annulus scarcely differentiated;</text>
      <biological_entity id="o21550" name="annulus" name_original="annulus" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="scarcely" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum conic to rostellate;</text>
      <biological_entity id="o21551" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s17" to="rostellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia 1, sometimes 2 or rudimentary.</text>
      <biological_entity constraint="endostome" id="o21552" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1" value_original="1" />
        <character modifier="sometimes" name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="prominence" src="d0_s18" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Decaying logs, rock, soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="decaying" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypnum imponens is an amphi-Atlantic species that produces sporophytes, sometimes abundantly, in summer. The species is distinguished by the often pigmented, heterogeneous alar cells, usually reddish stems, long-toothed, foliose, usually numerous pseudoparaphyllia, and nearly erect, cylindric capsules. The branches usually emerge in one horizontal plane; the leaves are not to weakly plicate; the perichaetial leaf margins are often strongly revolute proximally; and the capsules are somewhat curved below the mouth. The lanceolate or filamentous pseudoparaphyllia and generally homogeneous alar cells of H. cupressiforme and its near relatives are usually sufficient to separate them from this species.</discussion>
  
</bio:treatment>