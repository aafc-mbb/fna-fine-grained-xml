<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="mention_page">17</other_info_on_meta>
    <other_info_on_meta type="illustration_page">16</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">splachnaceae</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="genus">tayloria</taxon_name>
    <taxon_name authority="(Hedwig) Bruch &amp; Schimper" date="1844" rank="species">serrata</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 204. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnaceae;genus tayloria;species serrata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099390</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Splachnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">serratum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>53, plate 8, figs. 1 – 3. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus splachnum;species serratum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–3 cm, in loose tufts, clear green.</text>
      <biological_entity id="o21917" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="clear green" value_original="clear green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21918" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o21917" id="r3136" name="in" negation="false" src="d0_s0" to="o21918" />
    </statement>
    <statement id="d0_s1">
      <text>Stems infrequently branched.</text>
      <biological_entity id="o21919" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves loosely arranged, somewhat contorted when dry, obovate to oblong-obovate, 2–5 × 1–1.5 mm;</text>
      <biological_entity id="o21920" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s2" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s2" to="oblong-obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins usually recurved proximally, plane distally, often reflexed at apex, entire proximally, toothed distally;</text>
      <biological_entity id="o21921" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character constraint="at apex" constraintid="o21922" is_modifier="false" modifier="often" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" notes="" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o21922" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex acuminate, acute, or sometimes obtuse;</text>
      <biological_entity id="o21923" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa ending before apex.</text>
      <biological_entity id="o21925" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o21924" id="r3137" name="ending before" negation="false" src="d0_s5" to="o21925" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by brood bodies sometimes produced on radicles.</text>
      <biological_entity id="o21926" name="brood" name_original="brood" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o21927" name="body" name_original="bodies" src="d0_s6" type="structure" />
      <biological_entity id="o21928" name="radicle" name_original="radicles" src="d0_s6" type="structure" />
      <relation from="o21926" id="r3138" name="produced on" negation="false" src="d0_s6" to="o21928" />
      <relation from="o21927" id="r3139" name="produced on" negation="false" src="d0_s6" to="o21928" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous or sometimes apparently dioicous.</text>
      <biological_entity id="o21924" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" modifier="sometimes apparently" name="reproduction" src="d0_s7" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta yellow to dark red or brown, 1–3 cm, flexuose, ± stout.</text>
      <biological_entity id="o21929" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="dark red or brown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s8" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="more or less" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule cylindric or oblong-cylindric, 2.5–5 mm including hypophysis;</text>
      <biological_entity id="o21930" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21931" name="hypophysis" name_original="hypophysis" src="d0_s9" type="structure" />
      <relation from="o21930" id="r3140" name="including" negation="false" src="d0_s9" to="o21931" />
    </statement>
    <statement id="d0_s10">
      <text>hypophysis somewhat darker than urn, 1–2 times urn length;</text>
      <biological_entity id="o21932" name="hypophysis" name_original="hypophysis" src="d0_s10" type="structure">
        <character constraint="than urn" constraintid="o21933" is_modifier="false" name="coloration" src="d0_s10" value="somewhat darker" value_original="somewhat darker" />
        <character constraint="urn" constraintid="o21934" is_modifier="false" name="length" src="d0_s10" value="1-2 times urn length" value_original="1-2 times urn length" />
      </biological_entity>
      <biological_entity id="o21933" name="urn" name_original="urn" src="d0_s10" type="structure" />
      <biological_entity id="o21934" name="urn" name_original="urn" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>columella somewhat exserted;</text>
      <biological_entity id="o21935" name="columella" name_original="columella" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="somewhat" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum deciduous, hemispheric, apex sometimes bluntly apiculate or short-rostrate;</text>
      <biological_entity id="o21936" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o21937" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes bluntly" name="shape" src="d0_s12" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome inserted below mouth;</text>
      <biological_entity id="o21938" name="peristome" name_original="peristome" src="d0_s13" type="structure" />
      <biological_entity id="o21939" name="mouth" name_original="mouth" src="d0_s13" type="structure" />
      <relation from="o21938" id="r3141" modifier="below" name="inserted" negation="false" src="d0_s13" to="o21939" />
    </statement>
    <statement id="d0_s14">
      <text>exostome teeth 16, not split, reflexed when dry, dark red or redbrown, lanceolate.</text>
      <biological_entity constraint="exostome" id="o21940" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
        <character is_modifier="false" modifier="when dry" name="orientation" notes="" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o21941" name="split" name_original="split" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Spores 9–12 µm, smooth.</text>
      <biological_entity id="o21942" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s15" to="12" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Organic material of animal origin, humus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="organic material" constraint="of animal origin , humus" />
        <character name="habitat" value="animal origin" />
        <character name="habitat" value="humus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.B., Nfld. and Labr., Ont., Que.; Alaska, Maine, N.Y., Oreg., Vt., Wash.; n, c Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tayloria serrata is distinguished by several leaf characters, including obovate to oblong-obovate leaves with an acuminate or acute apex and unbordered, serrate margins. In addition, the exostome teeth of T. serrata are not divided or recurved as in T. acuminata and T. splachnoides. The brood bodies are dark red-brown, narrowly ellipsoid, and small. The capsule is brown or red-brown and gradually narrowed to the hypophysis.</discussion>
  
</bio:treatment>