<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">167</other_info_on_meta>
    <other_info_on_meta type="mention_page">170</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">PTYCHOSTOMUM</taxon_name>
    <taxon_name authority="(Hedwig) Hornschuch" date="1822" rank="species">cernuum</taxon_name>
    <place_of_publication>
      <publication_title>Syll. Pl. Nov.</publication_title>
      <place_in_publication>1: 64. 1822</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus ptychostomum;species cernuum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099306</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynontodium</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">cernuum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>58, plate 9. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cynontodium;species cernuum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="(Bridel) Bruch &amp; Schimper" date="unknown" rank="species">uliginosum</taxon_name>
    <taxon_hierarchy>genus bryum;species uliginosum</taxon_hierarchy>
  </taxon_identification>
  <number>20.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense or open turfs, green or yellow-green.</text>
      <biological_entity id="o7057" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7058" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o7057" id="r966" name="in" negation="false" src="d0_s0" to="o7058" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–3 cm, fertile stems comose, innovations evenly foliate.</text>
      <biological_entity id="o7059" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7060" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
      <biological_entity id="o7061" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves green to yellow-green, crowded, strongly contorted to shrunken when dry, ovatelanceolate, flat, 1–3.5 (–4) mm, often gradually enlarged toward stem apex;</text>
      <biological_entity id="o7062" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s2" to="yellow-green" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="strongly" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when dry" name="size" src="d0_s2" value="shrunken" value_original="shrunken" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="3.5" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o7063" is_modifier="false" modifier="often gradually" name="size" src="d0_s2" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="stem" id="o7063" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>base green, not decurrent;</text>
      <biological_entity id="o7064" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins revolute proximally, plane distally, limbidium strong, in 2 or 3 rows;</text>
      <biological_entity id="o7065" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o7066" name="limbidium" name_original="limbidium" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate;</text>
      <biological_entity id="o7067" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent to short-excurrent, awn stout;</text>
      <biological_entity id="o7068" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s6" to="short-excurrent" />
      </biological_entity>
      <biological_entity id="o7069" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s6" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells long-rectangular, 3–5: 1;</text>
      <biological_entity constraint="proximal laminal" id="o7070" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-rectangular" value_original="long-rectangular" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial and distal cells 18–22 µm wide, 3–4: 1, walls thin.</text>
      <biological_entity constraint="medial and distal" id="o7071" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="width" src="d0_s8" to="22" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o7072" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta yellowbrown or brown, 2–4 cm, stout, straight to somewhat flexuose.</text>
      <biological_entity id="o7073" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s11" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule brown, shape highly variable, elongate-pyriform to clavate, somewhat to strongly curved, (3–) 4–6 (–7) mm, mouth yellowbrown;</text>
      <biological_entity id="o7074" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="highly" name="shape" src="d0_s12" value="variable" value_original="variable" />
        <character char_type="range_value" from="elongate-pyriform" name="shape" src="d0_s12" to="clavate" />
        <character is_modifier="false" modifier="somewhat to strongly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7075" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>hypophysis slender;</text>
      <biological_entity id="o7076" name="hypophysis" name_original="hypophysis" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic, apiculate;</text>
      <biological_entity id="o7077" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome reduced;</text>
      <biological_entity id="o7078" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth yellow throughout or rarely hyaline distally, lamellae straight, pores absent near base along mid line;</text>
      <biological_entity constraint="exostome" id="o7079" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="throughout; throughout; rarely distally; distally" name="coloration" src="d0_s16" value="yellow throughout or rarely hyaline" />
      </biological_entity>
      <biological_entity id="o7080" name="lamella" name_original="lamellae" src="d0_s16" type="structure">
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o7081" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character constraint="near base" constraintid="o7082" is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7082" name="base" name_original="base" src="d0_s16" type="structure" />
      <biological_entity constraint="mid" id="o7083" name="line" name_original="line" src="d0_s16" type="structure" />
      <relation from="o7082" id="r967" name="along" negation="false" src="d0_s16" to="o7083" />
    </statement>
    <statement id="d0_s17">
      <text>endostome not adherent to exostome, basal membrane 1/2 exostome height, segments with ovate perforations, cilia absent or rudimentary.</text>
      <biological_entity id="o7084" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character constraint="to exostome" constraintid="o7085" is_modifier="false" modifier="not" name="fusion" src="d0_s17" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o7085" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o7086" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o7087" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity id="o7088" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity id="o7089" name="perforation" name_original="perforations" src="d0_s17" type="structure">
        <character is_modifier="true" name="height" src="d0_s17" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o7090" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o7088" id="r968" name="with" negation="false" src="d0_s17" to="o7089" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 28–32 (–35) µm, yellow to green.</text>
      <biological_entity id="o7091" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="32" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s18" to="35" to_unit="um" />
        <character char_type="range_value" from="28" from_unit="um" name="some_measurement" src="d0_s18" to="32" to_unit="um" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s18" to="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, along streams, wetlands, calcareous habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" constraint="along streams , wetlands" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="wetlands" />
        <character name="habitat" value="habitats" modifier="calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Colo., Ill., Iowa, Mich., Minn., Nebr., N.Mex., N.Y., N.Dak., Ohio, Pa., S.Dak.; s South America; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" value="s South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum cernuum is a circumpolar arctic-boreal to north-temperate species characterized by long ovate-lanceolate leaves with a short awn, green leaf base, autoicous sexual condition, and extremely long, curved capsule. The species is related to P. pallens, but differs in its longer more strongly curved capsules, autoicous sexual condition, larger spores, and yellow-green color. Ptychostomum turbinatum is similar, and generally capsules are needed to separate these species. Ptychostomum cernuum is autoicous, smaller, and has longer narrower ovate-lanceolate leaves, while P. turbinatum is dioicous, often very large, and has broader, more ovate leaves. Ptychostomum cernuum superficially resembles Pohlia elongata.</discussion>
  
</bio:treatment>