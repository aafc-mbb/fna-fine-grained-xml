<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">304</other_info_on_meta>
    <other_info_on_meta type="mention_page">303</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Loeske" date="unknown" rank="genus">hygroamblystegium</taxon_name>
    <taxon_name authority="(Hedwig) Mönkemeyer" date="unknown" rank="species">varium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">varium</taxon_name>
    <taxon_hierarchy>family amblystegiaceae;genus hygroamblystegium;species varium;subspecies varium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099160</other_info_on_name>
  </taxon_identification>
  <number>1a.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to moderately large.</text>
      <biological_entity id="o5023" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="moderately large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 10 cm, usually shorter;</text>
      <biological_entity id="o5024" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>central strand usually present.</text>
      <biological_entity constraint="central" id="o5025" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves 0.6–2 × 0.3–0.8 mm, lamina 1-stratose;</text>
      <biological_entity id="o5026" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5027" name="lamina" name_original="lamina" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire or nearly so to serrate;</text>
      <biological_entity id="o5028" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="nearly" value_original="nearly" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa to mid leaf, percurrent, or rarely short-excurrent, narrower than 100 µm at base;</text>
      <biological_entity id="o5029" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="short-excurrent" value_original="short-excurrent" />
        <character constraint="at base" constraintid="o5031" modifier="narrower than" name="some_measurement" src="d0_s5" unit="um" value="100" value_original="100" />
      </biological_entity>
      <biological_entity constraint="mid" id="o5030" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o5031" name="base" name_original="base" src="d0_s5" type="structure" />
      <relation from="o5029" id="r675" name="to" negation="false" src="d0_s5" to="o5030" />
    </statement>
    <statement id="d0_s6">
      <text>medial laminal cells 10–60 × 5–15 µm.</text>
      <biological_entity constraint="medial laminal" id="o5032" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s6" to="60" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s6" to="15" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Central America, South America, Eurasia, s Africa, Pacific Islands (New Zealand), Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Subspecies varium occurs in a wide range of habitats and displays, accordingly, a wide range of morphological variability. Many features, like the presence of a central strand in the stem, leaf shape, and shape of the leaf apex, are plastic and depend on water availability.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costae percurrent; alar cells somewhat differentiated; leaves ovate-lanceolate to ovate-triangular; apices obtuse to acute; stem leaves not complanate</description>
      <determination>1a1. Hygroamblystegium varium var. varium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costae ending before apex; alar cells not sharply differentiated; leaves broadly ovate; apices acuminate; stem leaves sometimes complanate</description>
      <determination>1a2. Hygroamblystegium varium var. humile</determination>
    </key_statement>
  </key>
</bio:treatment>