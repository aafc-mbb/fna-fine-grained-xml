<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">578</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">sematophyllaceae</taxon_name>
    <taxon_name authority="Loeske ex M. Fleischer" date="unknown" rank="genus">brotherella</taxon_name>
    <taxon_name authority="(Duby) M. Fleischer" date="unknown" rank="species">henonii</taxon_name>
    <place_of_publication>
      <publication_title>Nova Guinea</publication_title>
      <place_in_publication>12: 120. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sematophyllaceae;genus brotherella;species henonii</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002407</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Duby" date="unknown" rank="species">henonii</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>60: 93. 1877 (as henoni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species henonii</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, in loose mats, yellowish green to yellowish-brown.</text>
      <biological_entity id="o19454" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" from="yellowish green" name="coloration" notes="" src="d0_s0" to="yellowish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19455" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o19454" id="r2785" name="in" negation="false" src="d0_s0" to="o19455" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 4–6 (–12) cm, 1.5–2 mm wide across main leafy shoot, complanate-foliate, pinnate to irregularly branched, penicillate at shoot and branch apices;</text>
      <biological_entity id="o19456" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="12" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" constraint="across main shoot" constraintid="o19457" from="1.5" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
        <character char_type="range_value" from="pinnate" name="architecture" src="d0_s1" to="irregularly branched" />
        <character constraint="at shoot; at shoot and branch apices" constraintid="o19459, o19460" is_modifier="false" name="shape" src="d0_s1" value="penicillate" value_original="penicillate" />
      </biological_entity>
      <biological_entity constraint="main" id="o19457" name="shoot" name_original="shoot" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o19458" name="shoot" name_original="shoot" src="d0_s1" type="structure" />
      <biological_entity id="o19459" name="shoot" name_original="shoot" src="d0_s1" type="structure" />
      <biological_entity constraint="branch" id="o19460" name="apex" name_original="apices" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia filamentous or lanceolate.</text>
      <biological_entity id="o19461" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves erect, ovatelanceolate, tapering abruptly to apex, 1.7–1.8 mm;</text>
      <biological_entity id="o19462" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="to apex" constraintid="o19463" is_modifier="false" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19463" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>margins long-toothed or occasionally entire in acumen;</text>
      <biological_entity id="o19465" name="acumen" name_original="acumen" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>alar cells 3 or 4, yellowish.</text>
      <biological_entity id="o19464" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="long-toothed" value_original="long-toothed" />
        <character constraint="in acumen" constraintid="o19465" is_modifier="false" modifier="occasionally" name="shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition sterile.</text>
      <biological_entity id="o19466" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" unit="or" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sporophytes unknown.</text>
      <biological_entity id="o19467" name="sporophyte" name_original="sporophytes" src="d0_s7" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsule maturity unknown.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humid shaded sites near streams or cliff bases in forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humid shaded sites" constraint="near streams or cliff bases in forests" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="cliff bases" constraint="in forests" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Asia (China, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">henoni</other_name>
  <discussion>Brotherella henonii suggests an overgrown version of B. canadensis, but is paler green, forms looser mats, and grows in more humid shaded sites. The penicillate rather than falcate-secund shoot and branch tips also easily distinguish B. henonii. The British Columbia populations from Queen Charlotte Islands are few, lack sporophytes, and are slightly different from much of Asiatic B. henonii, but this species is sufficiently variable to include the North American material.</discussion>
  
</bio:treatment>