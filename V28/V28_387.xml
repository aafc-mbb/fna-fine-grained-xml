<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="(Mitten ex Brotherus) T. J. Koponen" date="unknown" rank="genus">rhizomnium</taxon_name>
    <taxon_name authority="(Hedwig) T. J. Koponen" date="1968" rank="species">punctatum</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>5: 143. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus rhizomnium;species punctatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001528</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">punctatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>193. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mnium;species punctatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhizomnium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">punctatum</taxon_name>
    <taxon_name authority="(Kindberg) T. J. Koponen" date="unknown" rank="subspecies">chlorophyllosum</taxon_name>
    <taxon_hierarchy>genus rhizomnium;species punctatum;subspecies chlorophyllosum</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 1–3 (–5) cm.</text>
      <biological_entity id="o12546" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems dark red or reddish-brown when old;</text>
    </statement>
    <statement id="d0_s2">
      <text>micronemata absent.</text>
      <biological_entity id="o12547" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s1" value="dark red" value_original="dark red" />
        <character is_modifier="false" modifier="when old" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green to light green, contorted when dry, broadly obovate or elliptic, (2–) 3–5 (–7) mm;</text>
      <biological_entity id="o12548" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="light green" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins pale green or reddish, 2–4-stratose;</text>
      <biological_entity id="o12549" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="2-4-stratose" value_original="2-4-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex rounded or sometimes obtuse or truncate, short-mucronate or apiculate;</text>
      <biological_entity id="o12550" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character name="shape" src="d0_s5" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-mucronate" value_original="short-mucronate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-mucronate" value_original="short-mucronate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent or occasionally subpercurrent;</text>
      <biological_entity id="o12551" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="occasionally" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells short-elongate or elongate, (50–) 65–100 (–120) µm, collenchymatous or not, walls pitted;</text>
      <biological_entity constraint="medial laminal" id="o12552" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="50" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="65" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="120" to_unit="um" />
        <character char_type="range_value" from="65" from_unit="um" name="some_measurement" src="d0_s7" to="100" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="collenchymatous" value_original="collenchymatous" />
        <character name="architecture" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o12553" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>marginal cells linear or short-linear, in 3 or 4 rows.</text>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction by green protonematous rhizoids.</text>
      <biological_entity id="o12555" name="protonematou" name_original="protonematous" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12556" name="rhizoid" name_original="rhizoids" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o12554" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta 1.5–3.5 cm.</text>
      <biological_entity id="o12557" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule oblong-cylindric, 2–3.5 mm;</text>
      <biological_entity id="o12558" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum conic-rostrate;</text>
      <biological_entity id="o12559" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="conic-rostrate" value_original="conic-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>exostome greenish yellow, lamellae 18+.</text>
      <biological_entity id="o12560" name="exostome" name_original="exostome" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o12561" name="lamella" name_original="lamellae" src="d0_s14" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s14" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 29–41 µm.</text>
      <biological_entity id="o12562" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="29" from_unit="um" name="some_measurement" src="d0_s15" to="41" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Forests, stream banks on logs, moist soil, humus, rock, on shaded cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forests" />
        <character name="habitat" value="stream banks" constraint="on logs" />
        <character name="habitat" value="soil" modifier="logs moist" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="rock" constraint="on shaded cliffs" />
        <character name="habitat" value="shaded cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask.; Ala., Alaska, Ark., Calif., Ga., Idaho, Maine, Md., Mass., Mich., Miss., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., Tex., Wash., W.Va., Wis., Wyo.; Europe; Asia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rhizomnium punctatum is distinguished by the lack of micronemata, percurrent costa, presence of an apiculus, and thick margins.</discussion>
  
</bio:treatment>