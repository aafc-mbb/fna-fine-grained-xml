<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Paul C. Marino</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">SPLACHNACEAE</taxon_name>
    <taxon_hierarchy>family SPLACHNACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10842</other_info_on_name>
  </taxon_identification>
  <number>34.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, green, yellowish, or sometimes brownish, acrocarpous.</text>
      <biological_entity id="o15699" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 2-fid by subfloral innovations;</text>
      <biological_entity id="o15700" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="by innovations" constraintid="o15701" is_modifier="false" name="shape" src="d0_s1" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o15701" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="subfloral" value_original="subfloral" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hairs common, minute, claviform.</text>
      <biological_entity constraint="axillary" id="o15702" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="common" value_original="common" />
        <character is_modifier="false" name="size" src="d0_s2" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="claviform" value_original="claviform" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves soft, homogeneous along stem or larger and crowded at stem apices, ovatelanceolate, oblong, or spatulate, broad;</text>
      <biological_entity id="o15703" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character constraint="along " constraintid="o15707" is_modifier="false" name="variability" src="d0_s3" value="homogeneous" value_original="homogeneous" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="width" src="d0_s3" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o15704" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity id="o15705" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <biological_entity constraint="stem" id="o15706" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="larger" value_original="larger" />
        <character is_modifier="true" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity constraint="stem" id="o15707" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire to dentate, sometimes bordered;</text>
      <biological_entity id="o15708" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="entire to dentate" value_original="entire to dentate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa single, strong, usually ending before apex;</text>
      <biological_entity id="o15709" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o15710" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o15709" id="r2228" modifier="usually" name="ending before" negation="false" src="d0_s5" to="o15710" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells rhomboidal, large;</text>
      <biological_entity constraint="laminal" id="o15711" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="size" src="d0_s6" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal-cells oblong;</text>
      <biological_entity id="o15712" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal cells oblong or oblong-hexagonal.</text>
      <biological_entity constraint="distal" id="o15713" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-hexagonal" value_original="oblong-hexagonal" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perichaetia with leaves similar, often larger.</text>
      <biological_entity id="o15714" name="perichaetium" name_original="perichaetia" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="size" notes="" src="d0_s9" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o15715" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o15714" id="r2229" name="with" negation="false" src="d0_s9" to="o15715" />
    </statement>
    <statement id="d0_s10">
      <text>Seta erect, usually elongate, thin or thick.</text>
      <biological_entity id="o15716" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="width" src="d0_s10" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule erect, exserted, symmetric or slightly curved, neck elongate, or hypophysis wide and inflated or long and narrow, proximal to urn;</text>
      <biological_entity id="o15717" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s11" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o15718" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o15719" name="hypophysis" name_original="hypophysis" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="wide" value_original="wide" />
        <character is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="position" src="d0_s11" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o15720" name="urn" name_original="urn" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>columella sometimes exserted;</text>
      <biological_entity id="o15721" name="columella" name_original="columella" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stomata many, guard cells 2;</text>
      <biological_entity id="o15722" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="guard" id="o15723" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus usually absent;</text>
      <biological_entity id="o15724" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum convex to conic;</text>
      <biological_entity id="o15725" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s15" to="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome usually present, single (double in Splachnum);</text>
      <biological_entity id="o15726" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s16" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome teeth 8–12 or 16, rarely 2-fid, approximate in groups of 2 and 4, densely and finely papillose.</text>
      <biological_entity constraint="exostome" id="o15727" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s17" to="12" />
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s17" value="2-fid" value_original="2-fid" />
        <character constraint="in groups" constraintid="o15728" is_modifier="false" name="arrangement" src="d0_s17" value="approximate" value_original="approximate" />
        <character is_modifier="false" modifier="densely; finely" name="relief" notes="" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o15728" name="group" name_original="groups" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character name="quantity" src="d0_s17" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Calyptra mitrate or rarely cucullate, smooth, sometimes hairy.</text>
      <biological_entity id="o15729" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s18" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; tropical to subpolar regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="tropical to subpolar regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 6, species 73 (5 genera, 20 species in the flora).</discussion>
  <discussion>Almost half the species of Splachnaceae possess three noteworthy ecological features. First, their gametophytes are coprophilous, growing on feces, occasionally old bones, and other animal matter. Second, their spores are commonly small and sticky, making them suitable for insect dispersal. Third, the sporophytes of all entomophilous Splachnaceae examined to date produce complex, species-specific odors that are thought to promote the attraction of flies (order Diptera).</discussion>
  <discussion>Although the leaves of Splachnaceae are soft textured and similar in shape to those of Funariaceae, recent phylogenetic studies suggest that Splachnaceae are more closely related to Meesiaceae than to Funariales as previously proposed. Like many species of Meesiaceae, most Splachnaceae grow in moist habitats such as peatlands in temperate and boreal forests. Splachnaceae differs from Meesiaceae in the structure of the capsule, which in Splachnaceae is erect with a mitrate calyptra, whereas in Meesiaceae the capsule is curved with a cucullate calyptra (B. Goffinet et al. 2004).</discussion>
  <references>
    <reference>Bequaert, J. 1921. On the dispersal by flies of the spores of certain mosses of the family Splachnaceae. Bryologist 24: 1–4.</reference>
    <reference>Goffinet, B., A. J. Shaw, and C. J. Cox. 2004. Phylogenetic inferences in the dung moss family Splachnaceae from analysis of cpDNA sequence data and implications for the evolution of entomophily. Amer. J. Bot. 91: 748–759.</reference>
    <reference>Koponen, A. 1978. The peristome and spores in Splachnaceae and their evolutionary and systematic significance. Bryophyt. Biblioth. 13: 535–567.</reference>
    <reference>Koponen, A. 1982. The classification of the Splachnaceae. Beih. Nova Hedwigia 71: 237–245.</reference>
    <reference>Marino, P. C. 1988. The North American distribution of the circumboreal species of Splachnum and Tetraplodon. Bryologist 91: 161–166.</reference>
    <reference>Marino, P. C. 1997. Competition, dispersal and coexistence of Splachnaceae in patchy habitats. Advances Bryol. 6: 241–264.</reference>
    <reference>Marino, P. C., R. Raguso, and B. Goffinet. 2008. The ecology and evolution of fly dispersed dung mosses (family Splachnaceae): Manipulating insect behaviour through odor and visual cues. Symbiosis 47: 61–76.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules cleistocarpous; plants coprophilous; hypophysis present or absent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules not cleistocarpous; plants coprophilous or not; hypophysis clearly differentiated</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peristome absent; hypophysis absent; capsules dark red.</description>
      <determination>1 Voitia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peristome present; hypophysis present; capsules yellowish.</description>
      <determination>3 Tetraplodon</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypophysis short to elongate, narrower than to somewhat wider than urn; calyptrae constricted at base or not; plants coprophilous or not</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypophysis elongate, not to much wider than urn; calyptrae not constricted beyond base; plants coprophilous or on old bones</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypophysis narrower than or occasionally as wide as urn, same color as urn; exostome teeth 8 or 16; calyptrae constricted at base; plants not coprophilous.</description>
      <determination>2 Tayloria</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypophysis barely wider than urn, same color or darker than urn; exostome teeth 16; calyptrae not constricted beyond base; plants coprophilous or on old bones.</description>
      <determination>3 Tetraplodon</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Hypophysis globose to turbinate, sometimes umbrelliform, greatly differentiated from urn in size and color; peristome double; exostome teeth not rudimentary.</description>
      <determination>4 Splachnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Hypophysis rounded, not or slightly wider than urn, similar to urn in size and color; peristome single; exostome teeth rudimentary.</description>
      <determination>5 Aplodon</determination>
    </key_statement>
  </key>
</bio:treatment>