<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">splachnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">splachnum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">vasculosum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>53. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnaceae;genus splachnum;species vasculosum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001367</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants light to dark green.</text>
      <biological_entity id="o22318" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s0" to="dark green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 cm.</text>
      <biological_entity id="o22319" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves distant distally, ovate to obovate, 2.5 × 4 mm;</text>
      <biological_entity id="o22320" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s2" value="distant" value_original="distant" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="obovate" />
        <character name="length" src="d0_s2" unit="mm" value="2.5" value_original="2.5" />
        <character name="width" src="d0_s2" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire, not bordered;</text>
      <biological_entity id="o22321" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex blunt, rounded, or obtuse in proximal leaves, apiculate in distal leaves;</text>
      <biological_entity id="o22322" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character constraint="in proximal leaves" constraintid="o22323" is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character constraint="in distal leaves" constraintid="o22324" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o22323" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o22324" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa ending somewhat before apex.</text>
      <biological_entity id="o22325" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o22326" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o22325" id="r3179" name="ending" negation="false" src="d0_s5" to="o22326" />
    </statement>
    <statement id="d0_s6">
      <text>Seta reddish orange, 1–3 cm, twisted.</text>
      <biological_entity id="o22327" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish orange" value_original="reddish orange" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule urn dark orange, 1 mm;</text>
      <biological_entity constraint="capsule" id="o22328" name="urn" name_original="urn" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark orange" value_original="dark orange" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypophysis red-purple when moist, globose to ovoid, much wider than urn, rugose and narrower when dry;</text>
      <biological_entity id="o22329" name="hypophysis" name_original="hypophysis" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when moist" name="coloration_or_density" src="d0_s8" value="red-purple" value_original="red-purple" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s8" to="ovoid" />
        <character constraint="than urn" constraintid="o22330" is_modifier="false" name="width" src="d0_s8" value="much wider" value_original="much wider" />
        <character is_modifier="false" name="relief" src="d0_s8" value="rugose" value_original="rugose" />
        <character is_modifier="false" modifier="when dry" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o22330" name="urn" name_original="urn" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>operculum convex and apiculate to short-conic;</text>
      <biological_entity id="o22331" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character char_type="range_value" from="apiculate" name="shape" src="d0_s9" to="short-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exostome teeth inserted near mouth, connate in pairs, pale-brown or orangebrown.</text>
      <biological_entity constraint="exostome" id="o22332" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character constraint="in pairs" constraintid="o22334" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
      <biological_entity id="o22333" name="mouth" name_original="mouth" src="d0_s10" type="structure" />
      <biological_entity id="o22334" name="pair" name_original="pairs" src="d0_s10" type="structure" />
      <relation from="o22332" id="r3180" name="inserted near" negation="false" src="d0_s10" to="o22333" />
    </statement>
    <statement id="d0_s11">
      <text>Spores spheric, 8–13 µm, greenish yellow.</text>
      <biological_entity id="o22335" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s11" to="13" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dung in bogs, wet places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dung" constraint="in bogs , wet places" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="wet places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Man., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska; n Europe; Asia (Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Splachnum vasculosum has a more northerly distribution than S. ampullaceum, with which it could be confused. The leaves of S. vasculosum are entire and ovate to obovate with a blunt or blunt-rounded apex and more or less rhomboidal laminal cells. The hypophysis is dark purple and similar in size to the hypophysis of S. ampullaceum, but it is more globose and narrows more abruptly to the thin seta.</discussion>
  
</bio:treatment>