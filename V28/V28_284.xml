<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">176</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">180</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">651</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">ROSULABRYUM</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>99: 222. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ROSULABRYUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin rosula, rosette, and Greek bryon, moss, alluding to clustering of leaves</other_info_on_name>
    <other_info_on_name type="fna_id">202976</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="J. J. Amann" date="unknown" rank="section">Trichophora</taxon_name>
    <taxon_hierarchy>genus bryum;section trichophora</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in open to dense low turfs or gregarious (tall turfs in R. andersonii), green to red-green.</text>
      <biological_entity id="o15398" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="red-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15399" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="position" src="d0_s0" value="low" value_original="low" />
      </biological_entity>
      <relation from="o15398" id="r2182" name="in" negation="false" src="d0_s0" to="o15399" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.3–6 cm, usually strongly rosulate, sometimes in 2 or more interrupted rosettes, rarely evenly foliate or subjulaceous, subfloral innovations common;</text>
      <biological_entity id="o15400" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="usually strongly" name="arrangement" src="d0_s1" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="rarely evenly" name="architecture" notes="" src="d0_s1" value="foliate" value_original="foliate" />
        <character name="architecture" src="d0_s1" value="subjulaceous" value_original="subjulaceous" />
      </biological_entity>
      <biological_entity id="o15401" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o15402" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="subfloral" value_original="subfloral" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="common" value_original="common" />
      </biological_entity>
      <relation from="o15400" id="r2183" modifier="sometimes" name="in" negation="false" src="d0_s1" to="o15401" />
    </statement>
    <statement id="d0_s2">
      <text>rhizoids often many, micronemata and macronemata present.</text>
      <biological_entity id="o15403" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s2" value="many" value_original="many" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves variously contorted to spirally twisted around stem when dry or rarely nearly imbricate, erect to erect-spreading when moist, ovate, obovate, or spathulate, flat or weakly concave, 0.4–4.5 mm;</text>
      <biological_entity id="o15404" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="variously" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character constraint="around stem" constraintid="o15405" is_modifier="false" modifier="spirally" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o15405" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s3" to="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spathulate" value_original="spathulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spathulate" value_original="spathulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spathulate" value_original="spathulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base sometimes decurrent;</text>
      <biological_entity id="o15406" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins recurved proximally or sometimes plane, plane distally, nearly entire to distinctly serrate near apex, 1-stratose, limbidium present or absent;</text>
      <biological_entity id="o15407" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character constraint="entire to distinctly apex" constraintid="o15408" is_modifier="false" modifier="nearly" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o15408" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape_or_architecture" src="d0_s5" value="entire to distinctly" value_original="entire to distinctly" />
        <character is_modifier="true" modifier="distinctly" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15409" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex broadly rounded to acute;</text>
      <biological_entity id="o15410" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa sometimes not reaching apex, usually short to long-excurrent, awn pigmented or hyaline, stereid band well developed, guide cells present, in 1 (or 2) layers;</text>
      <biological_entity id="o15411" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="long-excurrent" value_original="long-excurrent" />
      </biological_entity>
      <biological_entity id="o15412" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o15413" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="pigmented" value_original="pigmented" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o15414" name="band" name_original="band" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o15416" name="layer" name_original="layers" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o15411" id="r2184" modifier="sometimes not" name="reaching" negation="false" src="d0_s7" to="o15412" />
      <relation from="o15415" id="r2185" name="in" negation="false" src="d0_s7" to="o15416" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells not differentiated;</text>
      <biological_entity constraint="guide" id="o15415" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15417" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal areolation heterogeneous;</text>
      <biological_entity constraint="laminal" id="o15418" name="areolation" name_original="areolation" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="heterogeneous" value_original="heterogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells rectangular, longer than more distal cells, 2–4: 1;</text>
      <biological_entity constraint="proximal laminal" id="o15419" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
        <character constraint="than more distal cells" constraintid="o15420" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15420" name="cell" name_original="cells" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>medial and distal cells rhomboidal, 3–5: 1, walls thin to thick, sometimes porose.</text>
      <biological_entity constraint="medial and distal" id="o15421" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="5" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction common, by rhizoidal tubers or filiform leaf-axil gemmae.</text>
      <biological_entity constraint="rhizoidal" id="o15423" name="tuber" name_original="tubers" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="common" value_original="common" />
      </biological_entity>
      <biological_entity constraint="leaf-axil" id="o15424" name="gemma" name_original="gemmae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous, rarely synoicous, polyoicous, or autoicous;</text>
      <biological_entity id="o15422" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s11" to="thick" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s11" value="porose" value_original="porose" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s13" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="polyoicous" value_original="polyoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="polyoicous" value_original="polyoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perigonia and perichaetia appearing terminal or lateral;</text>
      <biological_entity id="o15425" name="perigonium" name_original="perigonia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o15426" name="perichaetium" name_original="perichaetia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perigonial leaves often enlarged and distinctly rosulate;</text>
      <biological_entity constraint="perigonial" id="o15427" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s15" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="distinctly" name="arrangement" src="d0_s15" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>perichaetial leaves same size as vegetative leaves, not forming rosette, inner leaves differentiated, more acuminate.</text>
      <biological_entity constraint="perichaetial" id="o15428" name="leaf" name_original="leaves" src="d0_s16" type="structure" />
      <biological_entity id="o15429" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o15430" name="rosette" name_original="rosette" src="d0_s16" type="structure" />
      <biological_entity constraint="inner" id="o15431" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o15428" id="r2186" name="as" negation="false" src="d0_s16" to="o15429" />
      <relation from="o15428" id="r2187" name="forming" negation="true" src="d0_s16" to="o15430" />
    </statement>
    <statement id="d0_s17">
      <text>Seta single, rarely double or triple, straight.</text>
      <biological_entity id="o15432" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s17" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s17" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule nutant to inclined, clavate to cylindric or rarely pyriform, 2–6 mm;</text>
      <biological_entity id="o15433" name="capsule" name_original="capsule" src="d0_s18" type="structure">
        <character char_type="range_value" from="nutant" name="orientation" src="d0_s18" to="inclined" />
        <character char_type="range_value" from="clavate" name="shape" src="d0_s18" to="cylindric or rarely pyriform" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s18" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>hypophysis little differentiated;</text>
      <biological_entity id="o15434" name="hypophysis" name_original="hypophysis" src="d0_s19" type="structure">
        <character is_modifier="false" name="variability" src="d0_s19" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>operculum short-conic to umbonate;</text>
      <biological_entity id="o15435" name="operculum" name_original="operculum" src="d0_s20" type="structure">
        <character char_type="range_value" from="short-conic" name="shape" src="d0_s20" to="umbonate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>peristome double;</text>
      <biological_entity id="o15436" name="peristome" name_original="peristome" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>exostome yellow, teeth lanceolate to narrowly lanceolate;</text>
      <biological_entity id="o15437" name="exostome" name_original="exostome" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o15438" name="tooth" name_original="teeth" src="d0_s22" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s22" to="narrowly lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>endostome well developed, not adherent to exostome, basal membrane high, 1/2–2/3 exostome length, segments same height as exostome, widely perforated, cilia 2 or 3, appendiculate.</text>
      <biological_entity id="o15439" name="endostome" name_original="endostome" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s23" value="developed" value_original="developed" />
        <character constraint="to exostome" constraintid="o15440" is_modifier="false" modifier="not" name="fusion" src="d0_s23" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o15440" name="exostome" name_original="exostome" src="d0_s23" type="structure" />
      <biological_entity constraint="basal" id="o15441" name="membrane" name_original="membrane" src="d0_s23" type="structure">
        <character is_modifier="false" name="height" src="d0_s23" value="high" value_original="high" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s23" to="2/3" />
      </biological_entity>
      <biological_entity id="o15442" name="exostome" name_original="exostome" src="d0_s23" type="structure" />
      <biological_entity id="o15443" name="segment" name_original="segments" src="d0_s23" type="structure">
        <character constraint="as exostome" constraintid="o15444" is_modifier="false" name="length" src="d0_s23" value="height" value_original="height" />
        <character is_modifier="false" modifier="widely" name="architecture" notes="" src="d0_s23" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o15444" name="exostome" name_original="exostome" src="d0_s23" type="structure" />
      <biological_entity id="o15445" name="cilium" name_original="cilia" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s23" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Spores shed singly, 8–20 µm, smooth to finely papillose, dark yellow.</text>
      <biological_entity id="o15446" name="spore" name_original="spores" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s24" value="singly" value_original="singly" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s24" to="20" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s24" to="finely papillose" />
        <character is_modifier="false" name="coloration" src="d0_s24" value="dark yellow" value_original="dark yellow" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; concentrated in the Southern Hemisphere, especially Africa, and in subtropical to tropical mountains.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="concentrated in the Southern Hemisphere" establishment_means="native" />
        <character name="distribution" value="especially Africa" establishment_means="native" />
        <character name="distribution" value="and in subtropical to tropical mountains" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 80 (13 in the flora).</discussion>
  <discussion>Rosulabryum is a large, distinctive genus mainly found in tropical mountainous areas and in the Southern Hemisphere in areas of seasonal temperate climates, occurring typically on soil, less commonly on rock or wood, rarely epiphytic. The center of diversity appears to be sub-Saharan Africa.</discussion>
  <discussion>Most Northern Hemisphere bryologists are not familiar with the great morphological diversity of the species of Rosulabryum sect. Rosulabryum, traditionally placed in Bryum sect. Rosulata (Müller Hal.) J. J. Amann, as very few taxa extend beyond 20° N latitude. The most widespread representatives of Rosulabryum in the Northern Hemisphere are the small somewhat atypical species of Bryum sect. Trichophora J. J. Amann centered on R. capillare. Recent molecular work suggests that this section of Rosulabryum may be closer to Ptychostomum, and thus convergent on the robust Rosulata clade of the genus. However, very few species have been sampled for molecular work, and it seems unlikely that the complex of characters defining Rosulabryum could have evolved twice in unrelated clades. Plants of Rosulabryum have concolorous leaf apices and symmetric capsules. The rhizoidal tubers, when present, are spheric; the endostome is papillose.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filiform gemmae present in distal leaf axils</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filiform gemmae absent or very rare</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Innovations short, rosulate; leaves usually greater than 3 mm; margins strongly serrate distally, limbidium strong.</description>
      <determination>2 Rosulabryum andicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Innovations short and rosulate or elongate and evenly foliate; leaves usually less than 2 mm; margins entire to serrulate distally, limbidium weak or absent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Innovations rosulate; leaves obovate, flat; filiform gemmae brown when mature.</description>
      <determination>10 Rosulabryum laevifilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Innovations evenly foliate; leaves ovate to obovate, concave or flat; filiform gemmae red or brown</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rhizoidal tubers and filiform gemmae brown to red-brown, same color as rhizoids.</description>
      <determination>8 Rosulabryum flaccidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rhizoidal tubers orange, red, or pink, brighter than rhizoids, filiform gemmae red.</description>
      <determination>11 Rosulabryum pseudocapillare</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems elongate, evenly foliate; leaves 3-4.5 mm; bases somewhat decurrent; margins sharply serrate distally.</description>
      <determination>1 Rosulabryum andersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems rosulate, or if evenly foliate then leaves less than 2 mm; bases decurrent or not; margins serrate to entire distally</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves usually 2-4 mm, often in interrupted tufts; margins distinctly and strongly serrate distally, limbidium strong to absent; costae short-excurrent; laminal cell walls porose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves less than 2.5 mm, usually not in interrupted tufts; margins entire to serrate distally, limbidium present or rarely absent; costae not reaching apex to long-excurrent; laminal cell walls not or weakly porose</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Limbidium strong; costal awns variously straight to curved when dry.</description>
      <determination>2 Rosulabryum andicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Limbidium absent or weak; costal awns recurved when dry.</description>
      <determination>4 Rosulabryum canariense</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems evenly foliate; leaves ± imbricate when dry; rhizoidal tubers very rare.</description>
      <determination>6 Rosulabryum elegans</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems usually rosulate, although innovations sometimes evenly foliate; leaves usually contorted or twisted when dry, rarely innovation leaves imbricate; rhizoidal tubers often present</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants maroon, red, or red-green; leaf bases decurrent; costae not reaching apex, percurrent, or short-excurrent.</description>
      <determination>7 Rosulabryum erythroloma</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants green, brown, or red-green; leaf bases not decurrent; costae short- to long-excurrent, rarely not reaching apex</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Sexual condition dioicous, synoicous, autoicous, or polyoicous; rhizoidal tubers amber, orange, orange-red, scarlet, crimson, or red, usually brighter than rhizoids; leaf margins distinctly serrate or serrulate distally; capsules often strongly nutant, red to red-brown</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Sexual condition dioicous; rhizoidal tubers crimson, red, red-brown, or orange-red, usually same color as rhizoids, if brighter than either tubers distinctly warty with protuberant cells or leaf margins nearly entire distally; leaf margins entire to distinctly serrulate distally; capsules inclined to nutant, red to brown</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Tubers orange to amber, becoming brown with age; leaves ovate; margins weakly serrulate distally, limbidium moderately distinct.</description>
      <determination>3 Rosulabryum bornholmense</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Tubers scarlet, crimson, or red; leaves broadly ovate to obovate; margins strongly serrate distally, limbidium strong.</description>
      <determination>13 Rosulabryum torquescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rhizoidal tubers with strongly protuberant cells, crimson, red to dark red, on short rhizoids at base of stem; leaf margins distinctly serrulate distally; leaves irregularly contorted when dry.</description>
      <determination>12 Rosulabryum rubens</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rhizoidal tubers smooth or almost so, red-brown or brown, usually on long rhizoids away from stem base; leaf margins ± entire distally, if serrulate then leaves spirally twisted around stem when dry</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Limbidium present, margins serrulate distally; innovations short, rosulate; rosette leaves spirally twisted around stem.</description>
      <determination>5 Rosulabryum capillare</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Limbidium absent or weak, margins entire or weakly serrulate distally; innovations elongate, evenly foliate; rosette leaves irregularly twisted to contorted.</description>
      <determination>9 Rosulabryum gemmascens</determination>
    </key_statement>
  </key>
</bio:treatment>