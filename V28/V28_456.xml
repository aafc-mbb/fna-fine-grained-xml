<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lars Hedenäs</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">296</other_info_on_meta>
    <other_info_on_meta type="mention_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) G. Roth" date="unknown" rank="genus">DREPANOCLADUS</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>38(Beibl.): 6. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus DREPANOCLADUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek drepane, sickle, and clados, branch, alluding to curvature of branch leaves</other_info_on_name>
    <other_info_on_name type="fna_id">110923</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="(Renauld) Grout" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="subsection">Drepanocladus</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>2: 321. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;subsection drepanocladus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliergidium</taxon_name>
    <taxon_hierarchy>genus calliergidium</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, green, yellow-green, yellowish, or brownish.</text>
      <biological_entity id="o6077" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely and irregularly branched to ± pinnate, ± in one plane;</text>
      <biological_entity id="o6078" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched to more" value_original="branched to more" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present;</text>
      <biological_entity id="o6079" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o6080" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia absent;</text>
      <biological_entity id="o6081" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials on stem or abaxial costa insertion, rarely forming tomentum, slightly or occasionally strongly branched, smooth;</text>
      <biological_entity id="o6082" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly; occasionally strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="rhizoid" id="o6083" name="initial" name_original="initials" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly; occasionally strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6084" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o6085" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o6086" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o6082" id="r834" name="on" negation="false" src="d0_s4" to="o6084" />
      <relation from="o6083" id="r835" name="on" negation="false" src="d0_s4" to="o6084" />
      <relation from="o6082" id="r836" name="on" negation="false" src="d0_s4" to="o6085" />
      <relation from="o6083" id="r837" name="on" negation="false" src="d0_s4" to="o6085" />
      <relation from="o6082" id="r838" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o6086" />
      <relation from="o6083" id="r839" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o6086" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hair distal cells 1–3, hyaline.</text>
      <biological_entity constraint="axillary" id="o6087" name="hair" name_original="hair" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o6088" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="3" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves erect-spreading, spreading, or sometimes squarrose, straight or falcate, ovate or broadly so, ovatelanceolate, triangular, rounded-triangular, or cordate, not plicate, 0.9–6.4 × 0.4–1.6 mm;</text>
      <biological_entity id="o6089" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s6" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character name="shape" src="d0_s6" value="broadly" value_original="broadly" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-triangular" value_original="rounded-triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-triangular" value_original="rounded-triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s6" to="6.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s6" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base not or hardly decurrent (narrowly decurrent in D. cardotii);</text>
      <biological_entity id="o6090" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="hardly" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane, entire, slightly sinuate, or slightly denticulate, limbidia absent;</text>
      <biological_entity id="o6091" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o6092" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex long-acuminate to acute, acumen gradually differentiated or distinctly set off, plane or furrowed;</text>
      <biological_entity id="o6093" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="long-acuminate" name="shape" src="d0_s9" to="acute" />
      </biological_entity>
      <biological_entity id="o6094" name="acumen" name_original="acumen" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="gradually" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o6095" name="set" name_original="set" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa double and short, single and long, or sometimes excurrent;</text>
    </statement>
    <statement id="d0_s11">
      <text>alar cells differentiated, short-rectangular to short-linear, strongly inflated, hyaline, widest cells 17–32 µm wide, region distinct, transversely triangular or ± quadrate, reaching from margin (25–) 40–100% distance to costa at insertion;</text>
      <biological_entity id="o6096" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o6097" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="short-rectangular" name="shape" src="d0_s11" to="short-linear" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="widest" id="o6098" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="um" name="width" src="d0_s11" to="32" to_unit="um" />
      </biological_entity>
      <biological_entity id="o6099" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o6100" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <biological_entity id="o6101" name="costa" name_original="costa" src="d0_s11" type="structure" />
      <relation from="o6099" id="r840" name="reaching from" negation="false" src="d0_s11" to="o6100" />
      <relation from="o6099" id="r841" modifier="(25-)40-100%" name="to" negation="false" src="d0_s11" to="o6101" />
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cells linear;</text>
      <biological_entity constraint="medial laminal" id="o6102" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>marginal cells 1-stratose.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition autoicous [synoicous] or dioicous.</text>
      <biological_entity constraint="marginal" id="o6103" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule horizontal, cylindric, curved;</text>
      <biological_entity id="o6104" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome perfect;</text>
      <biological_entity id="o6105" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="perfect" value_original="perfect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome margins entire or slightly dentate distally;</text>
      <biological_entity constraint="exostome" id="o6106" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly; distally" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia 2–4, well developed, nodose or distally sometimes appendiculate.</text>
      <biological_entity constraint="endostome" id="o6107" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s18" to="4" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s18" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="nodose" value_original="nodose" />
        <character is_modifier="false" modifier="distally sometimes" name="architecture" src="d0_s18" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 11–32 µm.</text>
      <biological_entity id="o6108" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s19" to="32" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 10 (7 in the flora).</discussion>
  <discussion>Drepanocladus is found in more or less mineral- and nutrient-rich habitats. Differences between members of Drepanocladus and those of Campyliadelphus and Campylophyllum are discussed under 6. Campyliadelphus; characters separating Drepanocladus from 7. Campylium are mentioned under Campylium. The genus Calliergidium has been distinguished from Drepanocladus by its acute to obtuse leaf apices. However, the type of this genus, C. bakeri (Renauld) Grout, is a synonym of Drepanocladus aduncus, and Calliergidium is thus a synonym of Drepanocladus. Drepanocladus is widely distributed in the Holarctic region, in the temperate regions of the southern hemisphere, and in higher mountains in tropical and subtropical areas.</discussion>
  <discussion>In the following key, as well as in the species descriptions, the ratios between the laminal cell and leaf sizes should be based on measurements in 8–10 adjacent stem leaves. The total size ranges should be searched for, and the median values of these should be used for the calculations of the ratios.</discussion>
  <discussion>The name Drepanocladus sendtneri (Schimper ex H. Müller) Warnstorf has been used in the American literature, but this species is apparently restricted to Eurasia and Africa. It is distinguished from D. sordidus by a different ratio of medial laminal cell length (/um) to leaf length (mm), 17.9–24.4 (23.3–36.5 in D. sordidus).</discussion>
  <references>
    <reference>Hedenäs, L. 1996. On the interdependence of some leaf characters within the Drepanocladus aduncus-polycarpus complex. J. Bryol. 19: 311–324.</reference>
    <reference>Hedenäs, L. 1997b. The Drepanocladus s. str. species with excurrent costae (Amblystegiaceae). Nova Hedwigia 64: 535–547.</reference>
    <reference>Hedenäs, L. 1998. An overview of the Drepanocladus sendtneri complex. J. Bryol. 20: 83–102.</reference>
    <reference>Janssens, J. A. 1983. Past and extant distribution of Drepanocladus in North America, with notes on the differentiation of fossil fragments. J. Hattori Bot. Lab. 54: 251–298.</reference>
    <reference>Wynne, F. E. 1944b. Studies in Drepanocladus. IV. Taxonomy. Bryologist 47: 147–189.</reference>
    <reference>Żarnowiec, J. T. 2001. A Taxonomic Monograph of the Drepanocladus aduncus Group (Bryopsida: Amblystegiaceae). Bielsko-Biala.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sexual condition autoicous (unknown in D. cardotii); leaves spreading to squarrose, straight or falcate; acumina furrowed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sexual condition dioicous; leaves falcate-secund or rarely straight; acumina plane or furrowed</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants small; stem leaves 1-1.4 mm; bases narrowly decurrent 1/4 -3/4 distance to leaf below.</description>
      <determination>4 Drepanocladus cardotii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants medium-sized to large; stem leaves usually 1.7-3.6 mm; bases not or hardly decurrent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf insertions slightly curved; bases erectopatent to ± spreading; acumina not or gradually differentiated, furrowed; costae single or 2-fid to 1/2 -2/3 leaf length, or double and ending before mid leaf; leaves 0.6-1(-1.1) mm wide; temperate, Arctic.</description>
      <determination>2 Drepanocladus polygamus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf insertions deeply U-shaped; bases erect, subclasping; acumina usually sharply differentiated, furrowed or strongly furrowed; costae double and short, very rarely single and ending well before mid leaf; leaves 0.6-1.5 mm wide; Arctic.</description>
      <determination>3 Drepanocladus arcticus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem leaf costae excurrent or longly so, rarely ending few cells before apex.</description>
      <determination>5 Drepanocladus longifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem leaf costae ending well before apex</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ratio of medial laminal cell length (µm) to leaf length (mm) 37.6-45.6; stem leaves1.1-2.1 × 0.5-0.7 mm; alar regions quadrate, transversely short-rectangular, or transversely triangular, not reaching costa; Arctic.</description>
      <determination>7 Drepanocladus latinervis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Ratio of medial laminal cell length (µm) to leaf length (mm) 17.9-36.5(-38.2); stem leaves 0.9-5.2 × 0.4-1.6 mm; alar regions either transversely triangular and reaching costa or almost so, or quadrate or transversely short-triangular and not reaching costa; widespread, sometimes Arctic</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Alar regions transversely triangular, reaching costa or almost so; stem leaves variously straight or falcate-secund, the latter especially in small plants growing under relatively dry conditions.</description>
      <determination>1 Drepanocladus aduncus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Alar regions quadrate or transversely short-triangular, not reaching costa; stem leaves strongly falcate-secund, rarely weakly so, the latter especially in submerged plants.</description>
      <determination>6 Drepanocladus sordidus</determination>
    </key_statement>
  </key>
</bio:treatment>