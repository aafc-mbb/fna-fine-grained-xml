<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">471</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">657</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="W. R. Buck &amp; Ireland" date="unknown" rank="family">stereophyllaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="unknown" rank="genus">ENTODONTOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>227/228[I,3]: 895, fig. 657. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family stereophyllaceae;genus ENTODONTOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Entodon and - opsis, resembling, alluding to similarity</other_info_on_name>
    <other_info_on_name type="fna_id">111746</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hampe" date="unknown" rank="subgenus">Complanato-hypnum</taxon_name>
    <taxon_hierarchy>genus hypnum;subgenus complanato-hypnum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in thin to dense mats, light green to yellowish or brownish, glossy.</text>
      <biological_entity id="o10168" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="yellowish or brownish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10169" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o10168" id="r1461" name="in" negation="false" src="d0_s0" to="o10169" />
    </statement>
    <statement id="d0_s1">
      <text>Stems with central strand absent or poorly developed;</text>
      <biological_entity id="o10170" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="poorly" name="development" src="d0_s1" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="central" id="o10171" name="strand" name_original="strand" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o10170" id="r1462" name="with" negation="false" src="d0_s1" to="o10171" />
    </statement>
    <statement id="d0_s2">
      <text>axillary hair apical cells 3 [–6].</text>
      <biological_entity constraint="axillary" id="o10172" name="hair" name_original="hair" src="d0_s2" type="structure" />
      <biological_entity constraint="apical" id="o10173" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="6" />
        <character name="quantity" src="d0_s2" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem and branch leaves similar, stiff to lax, close to distant, imbricate and somewhat contorted when dry, erect-spreading when moist, sometimes homomallous toward substrate, ovatelanceolate to oblong-ovate, symmetric or asymmetric, not plicate, rarely wrinkled;</text>
      <biological_entity id="o10174" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character char_type="range_value" from="close" name="arrangement" src="d0_s3" to="distant" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="ovatelanceolate" modifier="toward substrate" name="shape" src="d0_s3" to="oblong-ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s3" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
      <biological_entity constraint="branch" id="o10175" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character char_type="range_value" from="close" name="arrangement" src="d0_s3" to="distant" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="ovatelanceolate" modifier="toward substrate" name="shape" src="d0_s3" to="oblong-ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s3" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or incurved proximally, serrulate to entire proximally, serrate to serrulate distally, sometimes entire throughout;</text>
      <biological_entity id="o10176" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s4" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="serrulate" modifier="proximally" name="architecture_or_shape" src="d0_s4" to="entire" />
        <character char_type="range_value" from="serrate" modifier="distally" name="architecture_or_shape" src="d0_s4" to="serrulate" />
        <character is_modifier="false" modifier="sometimes; throughout" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate, rarely acute or obtuse;</text>
      <biological_entity id="o10177" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa 1/3–1/2 leaf length;</text>
      <biological_entity id="o10178" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s6" to="1/2" />
      </biological_entity>
      <biological_entity id="o10179" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cell-walls thin;</text>
      <biological_entity constraint="laminal" id="o10180" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal cells broadly fusiform with tapering ends, sometimes short, rhomboidal in obtuse leaves, smooth or occasionally prorulose at distal ends on abaxial surface.</text>
      <biological_entity constraint="distal" id="o10181" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="with ends" constraintid="o10182" is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" notes="" src="d0_s8" value="short" value_original="short" />
        <character constraint="in leaves" constraintid="o10183" is_modifier="false" name="shape" src="d0_s8" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s8" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s8" value="occasionally" value_original="occasionally" />
      </biological_entity>
      <biological_entity id="o10182" name="end" name_original="ends" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o10183" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10184" name="end" name_original="ends" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o10185" name="surface" name_original="surface" src="d0_s8" type="structure" />
      <relation from="o10181" id="r1463" name="at" negation="false" src="d0_s8" to="o10184" />
      <relation from="o10184" id="r1464" name="on" negation="false" src="d0_s8" to="o10185" />
    </statement>
    <statement id="d0_s9">
      <text>Perigonia with leaves ecostate.</text>
      <biological_entity id="o10186" name="perigonium" name_original="perigonia" src="d0_s9" type="structure" />
      <biological_entity id="o10187" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o10186" id="r1465" name="with" negation="false" src="d0_s9" to="o10187" />
    </statement>
    <statement id="d0_s10">
      <text>Perichaetia at base of stems, leaves ovate, costate to ecostate within one perichaetium, proximal laminal cells laxly rectangular, distal cells linear-rhomboidal.</text>
      <biological_entity id="o10188" name="perichaetium" name_original="perichaetia" src="d0_s10" type="structure" />
      <biological_entity id="o10189" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o10190" name="stem" name_original="stems" src="d0_s10" type="structure" />
      <biological_entity id="o10191" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character constraint="within perichaetium" constraintid="o10192" is_modifier="false" name="architecture" src="d0_s10" value="costate" value_original="costate" />
      </biological_entity>
      <biological_entity id="o10192" name="perichaetium" name_original="perichaetium" src="d0_s10" type="structure" />
      <biological_entity constraint="proximal laminal" id="o10193" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="laxly" name="shape" src="d0_s10" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10194" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-rhomboidal" value_original="linear-rhomboidal" />
      </biological_entity>
      <relation from="o10188" id="r1466" name="at" negation="false" src="d0_s10" to="o10189" />
      <relation from="o10189" id="r1467" name="part_of" negation="false" src="d0_s10" to="o10190" />
    </statement>
    <statement id="d0_s11">
      <text>Seta orange to reddish, straight to somewhat flexuose.</text>
      <biological_entity id="o10195" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s11" to="reddish" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight to somewhat" value_original="straight to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s11" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule erect, inclined, or cernuous, orange to brown, cylindric, ellipsoid, or ovoid;</text>
      <biological_entity id="o10196" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="cernuous" value_original="cernuous" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="cernuous" value_original="cernuous" />
        <character char_type="range_value" from="orange" name="coloration" src="d0_s12" to="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exothecial cell-walls thin;</text>
      <biological_entity constraint="exothecial" id="o10197" name="cell-wall" name_original="cell-walls" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus often falling with operculum, 2-seriate or 3-seriate, cell-walls firm;</text>
      <biological_entity id="o10198" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character constraint="with operculum" constraintid="o10199" is_modifier="false" modifier="often" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="2-seriate" value_original="2-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s14" value="3-seriate" value_original="3-seriate" />
      </biological_entity>
      <biological_entity id="o10199" name="operculum" name_original="operculum" src="d0_s14" type="structure" />
      <biological_entity id="o10200" name="cell-wall" name_original="cell-walls" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum obliquely conic to short-rostrate;</text>
      <biological_entity id="o10201" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character char_type="range_value" from="obliquely conic" name="shape" src="d0_s15" to="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth shouldered, internal surface strongly to scarcely projecting;</text>
      <biological_entity constraint="exostome" id="o10202" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="shouldered" value_original="shouldered" />
      </biological_entity>
      <biological_entity constraint="internal" id="o10203" name="surface" name_original="surface" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly to scarcely" name="orientation" src="d0_s16" value="projecting" value_original="projecting" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome smooth or papillose, basal membrane high to low, segments somewhat shorter than teeth, cilia 1–3, sometimes rudimentary, shorter than segments.</text>
      <biological_entity id="o10204" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10205" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
        <character is_modifier="false" name="position" src="d0_s17" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o10206" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character constraint="than teeth" constraintid="o10207" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="somewhat shorter" value_original="somewhat shorter" />
      </biological_entity>
      <biological_entity id="o10207" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
      <biological_entity id="o10208" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="3" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
        <character constraint="than segments" constraintid="o10209" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o10209" name="segment" name_original="segments" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Spores spheric to ovoid, 15–27 µm.</text>
      <biological_entity id="o10210" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s18" to="ovoid" />
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s18" to="27" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 17 (1 in the flora).</discussion>
  <discussion>A. J. Grout (1945) revised the North and Central American species, placing them in the genus Stereophyllum.</discussion>
  
</bio:treatment>