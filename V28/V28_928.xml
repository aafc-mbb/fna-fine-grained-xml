<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">591</other_info_on_meta>
    <other_info_on_meta type="mention_page">592</other_info_on_meta>
    <other_info_on_meta type="mention_page">596</other_info_on_meta>
    <other_info_on_meta type="illustration_page">589</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leucodontaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">antitrichia</taxon_name>
    <taxon_name authority="Sullivant ex Lesquereux" date="1865" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>13: 11. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leucodontaceae;genus antitrichia;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099001</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antitrichia</taxon_name>
    <taxon_name authority="(Sullivant ex Lesquereux) Kindberg" date="unknown" rank="species">curtipendula</taxon_name>
    <taxon_name authority="(Sullivant ex Lesquereux) Braithwaite" date="unknown" rank="variety">californica</taxon_name>
    <taxon_hierarchy>genus antitrichia;species curtipendula;variety californica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cryphaea</taxon_name>
    <taxon_name authority="(Sullivant ex Lesquereux) Kindberg" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus cryphaea;species californica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Macouniella</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus macouniella;species californica</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in mats, widespreading, dark green when dry, bright green when moist.</text>
      <biological_entity id="o23282" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s0" value="widespreading" value_original="widespreading" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s0" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="when moist" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23283" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o23282" id="r3310" name="in" negation="false" src="d0_s0" to="o23283" />
    </statement>
    <statement id="d0_s1">
      <text>Secondary stems to 10 cm, ± regularly pinnate and subfrondiform, lateral branches subequal, to 1.5 cm, flagelliform distally, occasionally fertile or bearing small (less than 5 mm) branchlets.</text>
      <biological_entity constraint="secondary" id="o23284" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="more or less regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o23285" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="flagelliform" value_original="flagelliform" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character name="reproduction" src="d0_s1" value="bearing small branchlets" value_original="bearing small branchlets" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves julaceous when dry, erect-spreading when moist, ovatelanceolate;</text>
      <biological_entity id="o23286" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="architecture_or_shape" src="d0_s2" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate to sharply denticulate at apex;</text>
      <biological_entity id="o23287" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="at apex" constraintid="o23288" from="serrulate" name="shape" src="d0_s3" to="sharply denticulate" />
      </biological_entity>
      <biological_entity id="o23288" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa sometimes 2-fid distally, supplementary costae absent or rudimentary, 1–3 on each side of main costa base, length less than 6 cells;</text>
      <biological_entity id="o23289" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="shape" src="d0_s4" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity constraint="supplementary" id="o23290" name="costa" name_original="costae" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" constraint="on side" constraintid="o23291" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o23291" name="side" name_original="side" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <biological_entity constraint="costa main" id="o23292" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o23293" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o23291" id="r3311" name="part_of" negation="false" src="d0_s4" to="o23292" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cell-walls not or poorly pitted;</text>
      <biological_entity constraint="laminal" id="o23294" name="cell-wall" name_original="cell-walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="poorly" name="relief" src="d0_s5" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>medial cells 15–22 × 3–6 µm. Branch leaves julaceous and apically recurved when dry, erect-spreading when moist.</text>
      <biological_entity constraint="medial" id="o23295" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s6" to="22" to_unit="um" />
        <character char_type="range_value" from="3" from_unit="um" name="width" src="d0_s6" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="branch" id="o23296" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s6" value="erect-spreading" value_original="erect-spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Perichaetial leaves deltoid to elongate-acuminate, basal laminal cells rectangular, 4–5: 1, walls thin, medial cells flexuose-elongate, 5–8: 1.</text>
      <biological_entity constraint="perichaetial" id="o23297" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="deltoid" name="shape" src="d0_s7" to="elongate-acuminate" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o23298" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o23299" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="medial" id="o23300" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flexuose-elongate" value_original="flexuose-elongate" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="8" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 0.8–1.1 cm.</text>
      <biological_entity id="o23301" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s8" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect to asymmetric-curved, brown, cylindric to oblong-cylindric, 2.5–5 mm.</text>
      <biological_entity id="o23302" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="asymmetric-curved" value_original="asymmetric-curved" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s9" to="oblong-cylindric" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spores 18–22 µm.</text>
      <biological_entity id="o23303" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s10" to="22" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bark, decorticated wood, on Quercus, siliceous rock, soil, humic soil, full sun or partial shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bark" />
        <character name="habitat" value="decorticated wood" constraint="on quercus" />
        <character name="habitat" value="rock" modifier="siliceous" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="humic soil" />
        <character name="habitat" value="full sun" />
        <character name="habitat" value="partial shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Idaho, Oreg., Wash.; s Europe; sw Asia; n Africa; Atlantic Islands (Canary Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="s Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Canary Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Antitrichia californica is a common moss of the more temperate regions of far western North America; the southernmost report for the species is from San Diego County, California, in the peninsular massif within 1 km of the border with Mexico. Northward, the species usually occurs in drier low-elevation inland locations throughout much of cismontane California, in the Columbia, Rogue, Umpqua, and Willamette watersheds, and in Puget Trough. In interior British Columbia, the range follows the Fraser Watershed, mostly south of 50° north latitude; along the Pacific Coast, it occurs from Vancouver Island northwestward as far as Ivanof Bay on the Alaska Peninsula. In cismontane California, southern Oregon, and Willamette Valley, A. californica often dominates the trunks and, more especially, the horizontal branches of oak trees, often in large monoculture patches. The species has been reported only rarely on conifer bark; at higher latitudes it seems to favor rock substrates. The plants are stiff and wiry; the secondary stems curl downward and inward when dry, uncurling to erect when moist; the costae are broad basally, narrowing to the apex; and the juxtacostal laminal cells are sometimes slightly more elongate than the medial cells.</discussion>
  
</bio:treatment>