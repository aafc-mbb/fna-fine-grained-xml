<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">299</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Limpricht) Loeske" date="unknown" rank="genus">pseudocalliergon</taxon_name>
    <taxon_name authority="(Lindberg) Hedenas" date="1992" rank="species">brevifolium</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>16: 89. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus pseudocalliergon;species brevifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099280</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="species">brevifolium</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>23: 541. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species brevifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drepanocladus</taxon_name>
    <taxon_name authority="(Lindberg) Warnstorf" date="unknown" rank="species">brevifolius</taxon_name>
    <taxon_hierarchy>genus drepanocladus;species brevifolius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="(Lindberg &amp; Arnell) Warnstorf" date="unknown" rank="species">latifolius</taxon_name>
    <taxon_hierarchy>genus d.;species latifolius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">D.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lycopodioides</taxon_name>
    <taxon_name authority="(Lindberg) Mönkemeyer" date="unknown" rank="variety">brevifolius</taxon_name>
    <taxon_hierarchy>genus d.;species lycopodioides;variety brevifolius</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, sometimes turgid, often with golden gloss when dry.</text>
      <biological_entity id="o15185" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s0" value="turgid" value_original="turgid" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems slightly and irregularly branched.</text>
      <biological_entity id="o15186" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erectopatent to patent, falcate or strongly falcate, ovate to very broadly ovate, gradually or somewhat suddenly narrowed to apex;</text>
      <biological_entity id="o15187" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate to very" value_original="ovate to very" />
        <character is_modifier="false" modifier="very; broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="gradually; somewhat suddenly" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o15188" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="erectopatent" name="orientation" src="d0_s2" to="patent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate to very" value_original="ovate to very" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire or occasionally partly very finely denticulate;</text>
      <biological_entity id="o15189" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="occasionally partly very finely" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex short to long-acuminate, furrowed or almost tubular;</text>
      <biological_entity id="o15190" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s4" value="tubular" value_original="tubular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa single, to 3/5–3/4 leaf length, 21–53 µm wide at base, 2-stratose or 3-stratose, or double to 1/4–2/5 (–1/2) leaf length;</text>
      <biological_entity id="o15191" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="3/5-3/4" />
      </biological_entity>
      <biological_entity id="o15193" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o15194" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s5" to="2/5-1/2" />
      </biological_entity>
      <relation from="o15192" id="r2150" name="double to" negation="false" src="d0_s5" to="o15194" />
    </statement>
    <statement id="d0_s6">
      <text>alar cells rectangular or long-rectangular, inflated, walls slightly or strongly incrassate, region transversely triangular;</text>
      <biological_entity id="o15192" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="at base" constraintid="o15193" from="21" from_unit="um" name="width" src="d0_s5" to="53" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-stratose" value_original="3-stratose" />
      </biological_entity>
      <biological_entity id="o15195" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="long-rectangular" value_original="long-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o15196" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s6" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o15197" name="region" name_original="region" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells smooth.</text>
      <biological_entity constraint="distal laminal" id="o15198" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Perichaetia with inner leaves gradually or ± suddenly narrowed to apex, apex acuminate or short-acuminate.</text>
      <biological_entity id="o15199" name="perichaetium" name_original="perichaetia" src="d0_s8" type="structure">
        <character constraint="to apex" constraintid="o15201" is_modifier="false" modifier="more or less suddenly" name="shape" notes="" src="d0_s8" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity constraint="inner" id="o15200" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o15201" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <biological_entity id="o15202" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <relation from="o15199" id="r2151" name="with" negation="false" src="d0_s8" to="o15200" />
    </statement>
    <statement id="d0_s9">
      <text>Capsule ± horizontal.</text>
      <biological_entity id="o15203" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra, calcium-rich wetland habitats, rich fens, moist excavated soil, surrounding pools and percolation areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" />
        <character name="habitat" value="calcium-rich wetland habitats" />
        <character name="habitat" value="rich fens" />
        <character name="habitat" value="soil" modifier="moist excavated" />
        <character name="habitat" value="pools" modifier="surrounding" />
        <character name="habitat" value="percolation areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr. (Labr.), N.W.T., Nunavut, Yukon; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pseudocalliergon brevifolium is usually recognized by the yellow-brown, slightly and irregularly branched shoots, relatively broad, falcate leaves, and furrowed or almost tubular leaf acumen. The species could possibly be confused with P. angustifolium, but the latter has more narrowly acuminate stem and branch leaves. In P. angustifolium, the costa is normally single and 3–5-stratose, whereas it is only 2- or 3-stratose or frequently short and double in P. brevifolium. In P. angustifolium, the stem and branch leaf margins are at least partly distinctly denticulate or finely denticulate and the distal laminal cells are frequently prorate abaxially; in P. brevifolium the leaf margins are entire or occasionally very finely denticulate and the distal laminal cells are smooth. Pseudocalliergon brevifolium is often distinctly turgid, unlike P. angustifolium.</discussion>
  
</bio:treatment>