<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">537</other_info_on_meta>
    <other_info_on_meta type="illustration_page">539</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">cupressiforme</taxon_name>
    <taxon_name authority="Molendo" date="1865" rank="variety">subjulaceum</taxon_name>
    <place_of_publication>
      <publication_title>Ber. Naturhist. Vereins Augsburg</publication_title>
      <place_in_publication>18: 183. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus hypnum;species cupressiforme;variety subjulaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099171</other_info_on_name>
  </taxon_identification>
  <number>5d</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, yellowish green to brown.</text>
      <biological_entity id="o20927" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s0" to="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 5–8+ cm, creeping, subjulaceous to complanate-foliate, regularly to irregularly pinnate, to 2-pinnate.</text>
      <biological_entity id="o20928" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
        <character char_type="range_value" from="pinnate" modifier="regularly to irregularly; irregularly" name="architecture_or_shape" src="d0_s1" to="2-pinnate" />
        <character char_type="range_value" from="pinnate" name="architecture_or_shape" src="d0_s1" to="2-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves straight to weakly falcate, sometimes homomallous, oblong-lanceolate, gradually narrowed to apex;</text>
      <biological_entity id="o20929" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="to apex" constraintid="o20930" is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o20930" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins subentire to weakly toothed;</text>
    </statement>
    <statement id="d0_s4">
      <text>alar cells many, subquadrate, pigmented, region excavate.</text>
      <biological_entity id="o20931" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s3" to="weakly toothed" />
      </biological_entity>
      <biological_entity id="o20932" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o20933" name="region" name_original="region" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="excavate" value_original="excavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Branch leaves 1.5–2 × 0.4–0.6 mm or slightly larger.</text>
      <biological_entity constraint="branch" id="o20934" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s5" value="larger" value_original="larger" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsule maturity unknown.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial, cliff shelves, horizontal rock surfaces, exposed and sheltered sites, usually calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliff shelves" modifier="terrestrial" />
        <character name="habitat" value="horizontal rock surfaces" />
        <character name="habitat" value="exposed" />
        <character name="habitat" value="sheltered sites" />
        <character name="habitat" value="calcareous substrates" modifier="usually" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-4000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Yukon; Alaska, Colo., N.Mex., N.Dak.; Europe; Asia; Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Variety subjulaceum is frequent in the Alaskan Peninsula but uncommon (or unrecorded) elsewhere in North America; it is found mainly at high elevations in Europe. The variety has somewhat concave leaves. No sporophytes were found in North American material. The acutely attached branches, closely imbricate, straight leaves with sharp apices, and frequently excavate pigmented alar cells make this a distinctive variety.</discussion>
  
</bio:treatment>