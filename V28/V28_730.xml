<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Michael S. Ignatov,William D. Reese†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
    <other_info_on_meta type="mention_page">404</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Manuel" date="unknown" rank="genus">ZELOMETEORIUM</taxon_name>
    <place_of_publication>
      <publication_title>J. Hattori Bot. Lab.</publication_title>
      <place_in_publication>43: 110, figs. 5 – 9. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus ZELOMETEORIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek zelos, emulation, and genus Meteorium, alluding to similarity</other_info_on_name>
    <other_info_on_name type="fna_id">135281</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, in intricate tangles, sometimes distally filiform, green or more commonly yellowish to brownish, blackish in old parts.</text>
      <biological_entity id="o16015" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" modifier="sometimes distally" name="shape" notes="" src="d0_s0" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="commonly yellowish" name="coloration" src="d0_s0" to="brownish" />
        <character constraint="in parts" constraintid="o16017" is_modifier="false" name="coloration" src="d0_s0" value="blackish" value_original="blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16016" name="tangle" name_original="tangles" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="intricate" value_original="intricate" />
      </biological_entity>
      <biological_entity id="o16017" name="part" name_original="parts" src="d0_s0" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s0" value="old" value_original="old" />
      </biological_entity>
      <relation from="o16015" id="r2287" name="in" negation="false" src="d0_s0" to="o16016" />
    </statement>
    <statement id="d0_s1">
      <text>Stems both creeping and pendent flexuose, terete-foliate, unevenly branched, sometimes fairly regularly pinnate, branches terete-foliate;</text>
      <biological_entity id="o16018" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="course" src="d0_s1" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="unevenly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes fairly regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o16019" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>central strand weak, sometimes absent;</text>
      <biological_entity constraint="central" id="o16020" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia broadly triangular;</text>
      <biological_entity id="o16021" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary hairs of 3 or 4 cells.</text>
      <biological_entity constraint="axillary" id="o16022" name="hair" name_original="hairs" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves in dense foliage erect-spreading to squarrose from clasping base, broadly ovate;</text>
      <biological_entity id="o16023" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o16024" name="foliage" name_original="foliage" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character char_type="range_value" constraint="from base" constraintid="o16025" from="erect-spreading" name="orientation" src="d0_s5" to="squarrose" />
      </biological_entity>
      <biological_entity id="o16025" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <relation from="o16023" id="r2288" name="in" negation="false" src="d0_s5" to="o16024" />
    </statement>
    <statement id="d0_s6">
      <text>base cordate;</text>
      <biological_entity id="o16026" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins subentire to serrulate;</text>
      <biological_entity id="o16027" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s7" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o16028" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa to 60–80% leaf length, slender, terminal abaxial tooth small;</text>
      <biological_entity id="o16029" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <biological_entity id="o16030" name="leaf" name_original="leaf" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="60-80%" name="character" src="d0_s9" value="length" value_original="length" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity constraint="terminal abaxial" id="o16031" name="tooth" name_original="tooth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminal cells linear;</text>
      <biological_entity constraint="laminal" id="o16032" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>basal juxtacostal cells short-rectangular;</text>
      <biological_entity constraint="basal" id="o16033" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity id="o16034" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>leaves in loose foliage erect to erect-spreading from clasping base, narrowly to broadly ovate;</text>
      <biological_entity id="o16035" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" notes="" src="d0_s12" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o16036" name="foliage" name_original="foliage" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s12" value="loose" value_original="loose" />
        <character char_type="range_value" constraint="from base" constraintid="o16037" from="erect" name="orientation" src="d0_s12" to="erect-spreading" />
      </biological_entity>
      <biological_entity id="o16037" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s12" value="clasping" value_original="clasping" />
      </biological_entity>
      <relation from="o16035" id="r2289" name="in" negation="false" src="d0_s12" to="o16036" />
    </statement>
    <statement id="d0_s13">
      <text>base auriculate;</text>
      <biological_entity id="o16038" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apex abruptly acuminate;</text>
      <biological_entity id="o16039" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>costa to 30–70% leaf length, terminal spine absent;</text>
      <biological_entity id="o16040" name="costa" name_original="costa" src="d0_s15" type="structure" />
      <biological_entity id="o16041" name="leaf" name_original="leaf" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="30-70%" name="character" src="d0_s15" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>submarginal alar cells in leaf corners, usually only 2–4, sometimes slightly enlarged and pellucid;</text>
      <biological_entity constraint="terminal" id="o16042" name="spine" name_original="spine" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s16" value="submarginal" value_original="submarginal" />
      </biological_entity>
      <biological_entity id="o16043" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sometimes slightly" name="size" notes="" src="d0_s16" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o16044" name="corner" name_original="corners" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" modifier="usually only; only" name="quantity" src="d0_s16" to="4" />
      </biological_entity>
      <relation from="o16043" id="r2290" name="in" negation="false" src="d0_s16" to="o16044" />
    </statement>
    <statement id="d0_s17">
      <text>laminal cells linear;</text>
      <biological_entity constraint="laminal" id="o16045" name="cell" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>basal juxtacostal cells shorter.</text>
      <biological_entity constraint="basal" id="o16046" name="cell" name_original="cells" src="d0_s18" type="structure" />
      <biological_entity id="o16047" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s18" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Branch leaves not differentiated.</text>
    </statement>
    <statement id="d0_s20">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="branch" id="o16048" name="leaf" name_original="leaves" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s19" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="reproduction" src="d0_s20" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s20" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>perichaetial leaves erect, acumen erect to spreading, costa extending to base of acumen.</text>
      <biological_entity constraint="perichaetial" id="o16049" name="leaf" name_original="leaves" src="d0_s21" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s21" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16050" name="acumen" name_original="acumen" src="d0_s21" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s21" to="spreading" />
      </biological_entity>
      <biological_entity id="o16052" name="base" name_original="base" src="d0_s21" type="structure" />
      <biological_entity id="o16053" name="acumen" name_original="acumen" src="d0_s21" type="structure" />
      <relation from="o16051" id="r2291" name="extending to" negation="false" src="d0_s21" to="o16052" />
      <relation from="o16051" id="r2292" name="extending to" negation="false" src="d0_s21" to="o16053" />
    </statement>
    <statement id="d0_s22">
      <text>[Seta reddish, short, rough. Capsule erect, brownish, cylindric, straight; annulus separating by fragments; operculum conic-rostrate; peristome hygrocastique, modified. Calyptra mitrate, pilose. Spores 14–23 µm].</text>
      <biological_entity id="o16051" name="costa" name_original="costa" src="d0_s21" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, c Africa, Pacific Islands (Galapagos Islands).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="c Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Galapagos Islands)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (1 in the flora).</discussion>
  <discussion>Zelometeorium has long been treated as a member of the tropical and subtropical family Meteoriaceae, although its closest relative, Meteoridium, was considered a member of Brachytheciaceae by some authors (M. A. Lewis 1992). Results of molecular phylogenetics (M. S. Ignatov and S. Huttunen 2002) definitely indicate the position of both within Brachytheciaceae, a sister family to Meteoriaceae. Sporophytes are very rare, and the description is based on Latin American plants.</discussion>
  <references>
    <reference>Manuel, M. G. 1977. A monograph of the genus Zelometeorium Manuel, gen. nov. (Bryopsida: Meteoriaceae). J. Hattori Bot. Lab. 43: 107–126.</reference>
  </references>
  
</bio:treatment>