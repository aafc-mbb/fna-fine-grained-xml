<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">bartramia</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">ithyphylla</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent.</publication_title>
      <place_in_publication>2(3): 132, plate 1, fig. 6. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus bartramia;species ithyphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200001553</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bartramia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ithyphylla</taxon_name>
    <taxon_name authority="(Lindberg) Kindberg" date="unknown" rank="variety">breviseta</taxon_name>
    <taxon_hierarchy>genus bartramia;species ithyphylla;variety breviseta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ithyphylla</taxon_name>
    <taxon_name authority="(Lindberg) Kindberg" date="unknown" rank="subspecies">rigidula</taxon_name>
    <taxon_hierarchy>genus b.;species ithyphylla;subspecies rigidula</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in lax to dense tufts, soft green to glaucous.</text>
      <biological_entity id="o20729" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" notes="" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20730" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o20729" id="r2989" name="in" negation="false" src="d0_s0" to="o20730" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–5) cm.</text>
      <biological_entity id="o20731" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves stiffly erect when dry, spreading when moist, linear, 4–5 mm;</text>
      <biological_entity id="o20732" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base sheathing, shoulders well developed, firm;</text>
      <biological_entity id="o20733" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o20734" name="shoulder" name_original="shoulders" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, serrulate to serrate distally, teeth paired distally;</text>
      <biological_entity id="o20735" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character char_type="range_value" from="serrulate" modifier="distally" name="architecture_or_shape" src="d0_s4" to="serrate" />
      </biological_entity>
      <biological_entity id="o20736" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate, subulate;</text>
      <biological_entity id="o20737" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa excurrent, obscure in distal limb;</text>
      <biological_entity id="o20738" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character constraint="in distal limb" constraintid="o20739" is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity constraint="distal" id="o20739" name="limb" name_original="limb" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>basal laminal cell-walls thin;</text>
      <biological_entity constraint="basal laminal" id="o20740" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial and distal cells 25–45 × 5–7 µm, prorulae relatively low.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition synoicous;</text>
      <biological_entity constraint="medial and distal" id="o20741" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s8" to="45" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="7" to_unit="um" />
        <character is_modifier="false" modifier="relatively" name="position" src="d0_s8" value="low" value_original="low" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves somewhat longer than stem-leaves, 6 mm, more strongly clasping.</text>
      <biological_entity constraint="perichaetial" id="o20742" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="than stem-leaves" constraintid="o20743" is_modifier="false" name="length_or_size" src="d0_s10" value="somewhat longer" value_original="somewhat longer" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="6" value_original="6" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_fixation" src="d0_s10" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o20743" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seta 0.8–3 cm, straight.</text>
      <biological_entity id="o20744" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s11" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined, subglobose to ovoid, asymmetric, 1 mm;</text>
      <biological_entity id="o20745" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s12" to="ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="asymmetric" value_original="asymmetric" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>operculum short-conic;</text>
      <biological_entity id="o20746" name="operculum" name_original="operculum" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="short-conic" value_original="short-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome double;</text>
      <biological_entity id="o20747" name="peristome" name_original="peristome" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth 300–400 µm, strongly transversely barred, finely papillose proximally, smooth distally;</text>
      <biological_entity constraint="exostome" id="o20748" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character char_type="range_value" from="300" from_unit="um" name="some_measurement" src="d0_s15" to="400" to_unit="um" />
        <character is_modifier="false" modifier="strongly transversely" name="coloration_or_relief" src="d0_s15" value="barred" value_original="barred" />
        <character is_modifier="false" modifier="finely; proximally" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome basal membrane present, segments 1/2–2/3 length of teeth and somewhat adherent to them, smooth, cilia absent or rudimentary.</text>
      <biological_entity id="o20749" name="endostome" name_original="endostome" src="d0_s16" type="structure" />
      <biological_entity constraint="basal" id="o20750" name="membrane" name_original="membrane" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o20751" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character char_type="range_value" from="1/2 length of teeth" name="length" src="d0_s16" to="2/3 length of teeth" />
        <character is_modifier="false" modifier="somewhat" name="fusion" src="d0_s16" value="adherent" value_original="adherent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20752" name="cilium" name_original="cilia" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 25–40 µm.</text>
      <biological_entity id="o20753" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s17" to="40" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jul–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Nov" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil, rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.B., Nfld. and Labr., Nunavut, Que., Yukon; Alaska, Calif., Colo., Idaho, Maine, Mich., Mont., Nev., N.H., Oreg., Utah, Wash., Wyo.; s South America (Argentina); Europe; e Asia (Taiwan); n, c Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="s South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e Asia (Taiwan)" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Barthramia</other_name>
  <discussion>Bartramia ithyphylla is essentially an arctic-alpine species with disjunct populations in austral South America and the high mountains of Africa. In the flora area, the species frequents tundra and montane forest habitats with occasional occurrence at low to moderate elevations at northern latitudes. The glistening white leaf base is distinctive. The obscure costa in the distal limb and elongate distal laminal cells bearing low prorulae distinguish B. ithyphylla from other small species of the genus in the flora area. The distal leaves are sometimes divergent. Reports of Bartramia breviseta Lindberg [B. ithyphylla var. breviseta (Lindberg) Kindberg by some authors] from high elevations in Colorado likely represent misidentifications. In B. breviseta the capsules are overtopped by the perichaetial leaves (the seta is 1–3 mm), and the costa fills the acumen. As presently understood, B. breviseta is an arctic-alpine species of the Old World.</discussion>
  
</bio:treatment>