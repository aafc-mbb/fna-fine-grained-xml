<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">161</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="(Bridel) J. R. Spence" date="2009" rank="subgenus">CLADODIUM</taxon_name>
    <taxon_name authority="(Blandow ex Schwagrichen) J. R. Spence" date="2005" rank="species">longisetum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 21. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus cladodium;species longisetum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099314</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Blandow ex Schwägrichen" date="unknown" rank="species">longisetum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(2): 105, plate 74 [bottom]. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species longisetum</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense or open turfs, green or yellow-green.</text>
      <biological_entity id="o11292" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11293" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o11292" id="r1599" name="in" negation="false" src="d0_s0" to="o11293" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–2 (–3) cm, comose, innovations comose or somewhat elongate and evenly foliate;</text>
      <biological_entity id="o11294" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>not strongly radiculose.</text>
      <biological_entity id="o11295" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green or yellow-green, twisted to contorted when dry, ovate to broadly ovatelanceolate, weakly concave, 1–2 (–3) mm, enlarged toward stem apex;</text>
      <biological_entity id="o11296" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="broadly ovatelanceolate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o11297" is_modifier="false" name="size" src="d0_s3" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity constraint="stem" id="o11297" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base not or weakly decurrent;</text>
      <biological_entity id="o11298" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins revolute to mid leaf or beyond, limbidium strong, in 2 or 3 rows;</text>
      <biological_entity id="o11299" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="mid" id="o11300" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o11301" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s5" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute;</text>
      <biological_entity id="o11302" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa short-excurrent, awn recurved, denticulate;</text>
      <biological_entity id="o11303" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o11304" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells 3–4: 1, same width or sometimes wider than more distal cells;</text>
      <biological_entity constraint="proximal laminal" id="o11305" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character constraint="than more distal cells" constraintid="o11306" is_modifier="false" name="width" src="d0_s8" value="sometimes wider" value_original="sometimes wider" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11306" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>medial and distal cells rhomboidal, 12–18 µm wide, 2–3: 1, walls usually thin to firm.</text>
      <biological_entity constraint="medial and distal" id="o11307" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s9" to="18" to_unit="um" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition synoicous.</text>
      <biological_entity id="o11308" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 3–6 (–8) cm.</text>
      <biological_entity id="o11309" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="8" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s12" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule brown, elongate-pyriform, symmetric, 2–4 mm, mouth yellow;</text>
      <biological_entity id="o11310" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate-pyriform" value_original="elongate-pyriform" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11311" name="mouth" name_original="mouth" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic, apiculate;</text>
      <biological_entity id="o11312" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome well developed;</text>
      <biological_entity id="o11313" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth yellow basally, hyaline distally, lamellae usually straight mid-tooth, pores absent along mid line;</text>
      <biological_entity constraint="exostome" id="o11314" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o11315" name="lamella" name_original="lamellae" src="d0_s16" type="structure" />
      <biological_entity id="o11316" name="mid-tooth" name_original="mid-tooth" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="usually" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o11317" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character constraint="along mid line" constraintid="o11318" is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="mid" id="o11318" name="line" name_original="line" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>endostome not adherent to exostome, basal membrane high, less than 1/2 exostome height, segments with broadly ovate perforations, cilia short, rudimentary.</text>
      <biological_entity id="o11319" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character constraint="to exostome" constraintid="o11320" is_modifier="false" modifier="not" name="fusion" src="d0_s17" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o11320" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o11321" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s17" to="1/2" />
      </biological_entity>
      <biological_entity id="o11322" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity id="o11323" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity id="o11324" name="perforation" name_original="perforations" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="broadly" name="height" src="d0_s17" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o11325" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o11323" id="r1600" name="with" negation="false" src="d0_s17" to="o11324" />
    </statement>
    <statement id="d0_s18">
      <text>Spores variable in size, (38–) 40–50 µm, finely papillose, pale yellowbrown or green.</text>
      <biological_entity id="o11326" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character is_modifier="false" name="size" src="d0_s18" value="variable" value_original="variable" />
        <character char_type="range_value" from="38" from_unit="um" name="atypical_some_measurement" src="d0_s18" to="40" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="some_measurement" src="d0_s18" to="50" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale yellowbrown" value_original="pale yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="green" value_original="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp soil in wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="damp" constraint="in wetlands" />
        <character name="habitat" value="wetlands" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Nfld. and Labr., N.W.T., Nunavut, Yukon; Alaska; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum longisetum is a circumpolar arctic-boreal species related to P. inclinatum. Gametophytically it is similar to P. intermedium and P. salinum but differs in the extremely long seta and very large spores.</discussion>
  
</bio:treatment>