<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Inés Sastre-De Jesús</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">614</other_info_on_meta>
    <other_info_on_meta type="mention_page">602</other_info_on_meta>
    <other_info_on_meta type="mention_page">603</other_info_on_meta>
    <other_info_on_meta type="mention_page">613</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">neckeraceae</taxon_name>
    <taxon_name authority="Nieuwland" date="unknown" rank="genus">THAMNOBRYUM</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>5: 50. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family neckeraceae;genus THAMNOBRYUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek thamnos, shrub, and bryon, moss, alluding to growth form</other_info_on_name>
    <other_info_on_name type="fna_id">132697</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Schimper" date="unknown" rank="genus">Thamnium</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>5: 211, plate 518. 1852,</place_in_publication>
      <other_info_on_pub>not Ventenat 1799 [Lichen]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus thamnium</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, dendroid, dark green to yellow-green, slightly shiny to dull.</text>
      <biological_entity id="o14547" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s0" to="yellow-green" />
        <character char_type="range_value" from="slightly shiny" name="reflectance" src="d0_s0" to="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems forming stipe perpendicular to substrate, pinnate to sparsely branched distally;</text>
      <biological_entity id="o14548" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="pinnate" modifier="distally" name="architecture" src="d0_s1" to="sparsely branched" />
      </biological_entity>
      <biological_entity id="o14549" name="stipe" name_original="stipe" src="d0_s1" type="structure" />
      <relation from="o14548" id="r2029" name="forming" negation="false" src="d0_s1" to="o14549" />
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent.</text>
      <biological_entity id="o14550" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Secondary stem-leaves erect to erect-spreading, ovate, somewhat asymmetric, concave;</text>
      <biological_entity constraint="secondary" id="o14551" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s3" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire proximally, serrate at apex, teeth straight;</text>
      <biological_entity id="o14552" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character constraint="at apex" constraintid="o14553" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o14553" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o14554" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex broadly acute to obtuse;</text>
      <biological_entity id="o14555" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa single, stout, subpercurrent;</text>
      <biological_entity id="o14556" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s6" value="stout" value_original="stout" />
        <character is_modifier="false" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal and medial distal laminal cells linear to rectangular, walls pitted or not.</text>
      <biological_entity constraint="basal and medial distal laminal" id="o14557" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition synoicous, autoicous, or dioicous;</text>
      <biological_entity id="o14558" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="pitted" value_original="pitted" />
        <character name="relief" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perichaetial inner leaves oblong, tapering to subula.</text>
      <biological_entity constraint="perichaetial inner" id="o14559" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character constraint="to subula" constraintid="o14560" is_modifier="false" name="shape" src="d0_s9" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o14560" name="subulum" name_original="subula" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Seta 1 cm.</text>
      <biological_entity id="o14561" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule cylindric;</text>
      <biological_entity id="o14562" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exostome teeth linear-lanceolate, striate basally, becoming striate with overlying papillae, papillose at apex;</text>
      <biological_entity constraint="exostome" id="o14563" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="basally" name="coloration_or_pubescence_or_relief" src="d0_s12" value="striate" value_original="striate" />
        <character constraint="with " constraintid="o14564" is_modifier="false" modifier="becoming" name="coloration_or_pubescence_or_relief" src="d0_s12" value="striate" value_original="striate" />
        <character constraint="at apex" constraintid="o14565" is_modifier="false" name="relief" notes="" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o14564" name="papilla" name_original="papillae" src="d0_s12" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s12" value="overlying" value_original="overlying" />
      </biological_entity>
      <biological_entity id="o14565" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>endostome basal membrane high, segments papillose.</text>
      <biological_entity id="o14566" name="endostome" name_original="endostome" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o14567" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character is_modifier="false" name="height" src="d0_s13" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o14568" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 10 µm.</text>
      <biological_entity id="o14569" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="um" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Europe, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 42 (2 in the flora).</discussion>
  <discussion>Thamnobryum is distinguished from other dendroid mosses in the flora by the stout costa and the branches mostly hanging, not upright as in Climacium. The pseudoparaphyllia are foliose. The primary stem leaves are appressed to erect-spreading and deltoid, with entire margins, acute apices, single costa, and irregularly rhomboidal to rectangular distal medial laminal cells. The stem leaf costae have 1–5 teeth distally. The stem leaf apical cells are hexagonal, rhombic, or rhomboidal. The branch leaves are concave with broadly acute to acuminate apices and rectangular basal and distal medial laminal cells. The inner perichaetial leaves are erect to erect-spreading, 1.5–2 mm, with margins entire, apex acute, costa single, slender, to ± mid leaf, basal laminal cells rectangular with pitted walls, distal medial cells linear to rectangular, and apical cells rhomboidal. The seta is reddish brown. The capsule is horizontal with endostome cilia present.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves ovate, 1.5-2 × 0.5-0.6 mm; apices acute to acuminate; margins coarsely serrate at apex; apical laminal cells elongate-rhombic, more than 2:1.</description>
      <determination>1 Thamnobryum alleghaniense</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Branch leaves oblong-ovate to ovate, 1-2.2 × 0.5-0.8 mm; apices obtuse to broadly acute; margins serrate at apex; apical laminal cells subquadrate to short-rhomboidal, 2:1.</description>
      <determination>2 Thamnobryum neckeroides</determination>
    </key_statement>
  </key>
</bio:treatment>