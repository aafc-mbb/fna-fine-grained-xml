<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">465</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">scleropodium</taxon_name>
    <taxon_name authority="E. Lawton" date="1967" rank="species">julaceum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>94: 22, figs. 1 – 7. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus scleropodium;species julaceum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099371</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in moderately dense mats, green to golden green.</text>
      <biological_entity id="o15376" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="golden green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15377" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o15376" id="r2181" name="in" negation="false" src="d0_s0" to="o15377" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–5 cm, leafy shoots 0.3–0.4 mm wide, branches strongly julaceous.</text>
      <biological_entity id="o15378" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15379" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s1" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15380" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves closely imbricate, erect and slightly spreading when moist, broadly ovate to semiorbicular, 0.7–0.9 (–1.2) × 0.5–0.6 (–0.7) mm, length to width ratio usually 1.2–1.5:1;</text>
      <biological_entity id="o15381" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s2" to="semiorbicular" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s2" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="length" modifier="usually" name="ratio" src="d0_s2" to="width" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s2" to="1.5" />
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins ± serrulate distally, sometimes almost entire throughout;</text>
      <biological_entity id="o15382" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="semiorbicular" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s3" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="length" modifier="usually" name="ratio" src="d0_s3" to="width" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s3" to="1.5" />
      </biological_entity>
      <biological_entity id="o15383" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less; distally" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="sometimes almost; throughout" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex broadly acute or obtuse in smaller leaves;</text>
      <biological_entity id="o15384" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s4" to="semiorbicular" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s4" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="length" modifier="usually" name="ratio" src="d0_s4" to="width" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s4" to="1.5" />
      </biological_entity>
      <biological_entity id="o15385" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character constraint="in smaller leaves" constraintid="o15386" is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o15386" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>alar cells isodiametric, 9–12 µm, walls moderately thick, region extensive, moderately distinctly delimited;</text>
      <biological_entity id="o15387" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s5" to="semiorbicular" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s5" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="length" modifier="usually" name="ratio" src="d0_s5" to="width" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s5" to="1.5" />
      </biological_entity>
      <biological_entity id="o15388" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s5" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o15389" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o15390" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="extensive" value_original="extensive" />
        <character is_modifier="false" modifier="moderately distinctly" name="prominence" src="d0_s5" value="delimited" value_original="delimited" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 20–60 × 5–9 µm; basal juxtacostal cells quadrate to short-rectangular, 10–15 × 8–10 (–12) µm. Seta 0.6–0.7 cm, rough throughout.</text>
      <biological_entity id="o15391" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="semiorbicular" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s6" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="length" modifier="usually" name="ratio" src="d0_s6" to="width" />
        <character char_type="range_value" from="1.2" name="quantity" src="d0_s6" to="1.5" />
      </biological_entity>
      <biological_entity constraint="laminal" id="o15392" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="length" src="d0_s6" to="60" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s6" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o15393" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o15394" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="short-rectangular" />
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s6" to="15" to_unit="um" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s6" to="12" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s6" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity id="o15395" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s6" to="0.7" to_unit="cm" />
        <character is_modifier="false" modifier="throughout" name="pubescence_or_relief" src="d0_s6" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule horizontal.</text>
      <biological_entity id="o15396" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spores 12–19 µm.</text>
      <biological_entity id="o15397" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s8" to="19" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, tree trunks, open and sunny places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="open" />
        <character name="habitat" value="sunny places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Scleropodium julaceum occurs only in southern California, with scattered localities north to Mendocino County. This species often has broadly rounded leaves that easily distinguish it from other species in the genus. Small phenotypes of S. cespitans may be similar in appearance and leaf shape, but they differ in their alar cells, numerous and small in S. julaceum, few and enlarged in S. cespitans. Also, the capsule is horizontal in S. julaceum, suberect in S. cespitans. The stems are fragile; the basal juxtacostal cells are in two to five rows; and the laminal cells are rhombic to linear-flexuose.</discussion>
  
</bio:treatment>