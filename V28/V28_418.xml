<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lars Hedenäs</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Sullivant) Spruce" date="unknown" rank="genus">CRATONEURON</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Musc.,</publication_title>
      <place_in_publication>21. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus CRATONEURON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek cratos, strong, and neuron, nerve, alluding to leaf costa</other_info_on_name>
    <other_info_on_name type="fna_id">108289</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="section">Cratoneuron</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 673. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;section cratoneuron</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, green, yellowish, or rarely brownish.</text>
      <biological_entity id="o1975" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems pinnate or irregularly branched;</text>
      <biological_entity id="o1976" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present;</text>
      <biological_entity id="o1977" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o1978" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia few-to-many, rarely absent, lanceolate;</text>
      <biological_entity id="o1979" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character char_type="range_value" from="few" name="quantity" src="d0_s3" to="many" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials on stem or abaxial costa insertion, often forming tomentum, usually strongly branched, smooth;</text>
      <biological_entity id="o1980" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; usually strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="rhizoid" id="o1981" name="initial" name_original="initials" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often; usually strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1982" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o1983" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o1984" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o1980" id="r262" name="on" negation="false" src="d0_s4" to="o1982" />
      <relation from="o1981" id="r263" name="on" negation="false" src="d0_s4" to="o1982" />
      <relation from="o1980" id="r264" name="on" negation="false" src="d0_s4" to="o1983" />
      <relation from="o1981" id="r265" name="on" negation="false" src="d0_s4" to="o1983" />
      <relation from="o1980" id="r266" modifier="often" name="forming" negation="false" src="d0_s4" to="o1984" />
      <relation from="o1981" id="r267" modifier="often" name="forming" negation="false" src="d0_s4" to="o1984" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hair distal cells 1 or 2, hyaline.</text>
      <biological_entity constraint="axillary" id="o1985" name="hair" name_original="hair" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o1986" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves not recurved or squarrose, straight or falcate, narrowly to broadly triangular or rounded-triangular, sometimes ovate, not plicate, longer than 1 mm;</text>
      <biological_entity id="o1987" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character name="course" src="d0_s6" value="falcate to narrowly broadly triangular or rounded-triangular" />
        <character char_type="range_value" from="falcate" name="shape" src="d0_s6" to="narrowly broadly triangular or rounded-triangular" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character modifier="longer than" name="distance" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base decurrent;</text>
      <biological_entity id="o1988" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane or near base slightly recurved, denticulate or serrulate almost throughout, limbidia absent;</text>
      <biological_entity id="o1989" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s8" value="near base" value_original="near base" />
        <character is_modifier="false" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="almost throughout; throughout" name="shape" src="d0_s8" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o1990" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o1991" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o1989" id="r268" name="near" negation="false" src="d0_s8" to="o1990" />
    </statement>
    <statement id="d0_s9">
      <text>apex acuminate, acumen plane or furrowed;</text>
      <biological_entity id="o1992" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o1993" name="acumen" name_original="acumen" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa strong, single, usually percurrent or excurrent, sometimes ending well before apex;</text>
      <biological_entity id="o1995" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <relation from="o1994" id="r269" modifier="sometimes" name="ending" negation="false" src="d0_s10" to="o1995" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells differentiated, strongly inflated, hyaline, region well defined, transversely triangular;</text>
      <biological_entity id="o1994" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s10" value="strong" value_original="strong" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o1996" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o1997" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s11" value="defined" value_original="defined" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cells elongate-hexagonal, short-rectangular, rectangular, short-linear, or rarely linear, smooth;</text>
      <biological_entity constraint="medial laminal" id="o1998" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate-hexagonal" value_original="elongate-hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s12" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" modifier="rarely" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>marginal cells 1-stratose.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o1999" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule horizontal, cylindric, curved;</text>
      <biological_entity id="o2000" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome perfect;</text>
      <biological_entity id="o2001" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="perfect" value_original="perfect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome margins slightly dentate distally;</text>
      <biological_entity constraint="exostome" id="o2002" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia 2 or 3, well developed, nodose.</text>
      <biological_entity constraint="endostome" id="o2003" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s18" unit="or" value="3" value_original="3" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s18" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 14–21 (–25) µm.</text>
      <biological_entity id="o2004" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="21" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s19" to="25" to_unit="um" />
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s19" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Cratoneuron grows in slightly or strongly calcareous habitats that are at least periodically wet; plants have a strong leaf costa, relatively short, smooth laminal cells, and mostly lanceolate and leaflike paraphyllia. The differences between Cratoneuron and 2. Palustriella are described under the latter.</discussion>
  
</bio:treatment>