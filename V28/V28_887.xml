<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">568</other_info_on_meta>
    <other_info_on_meta type="mention_page">567</other_info_on_meta>
    <other_info_on_meta type="illustration_page">569</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="M. Fleischer" date="unknown" rank="genus">taxiphyllum</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper ex Sullivant) M. Fleischer" date="1923" rank="species">deplanatum</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Buitenzorg</publication_title>
      <place_in_publication>4: 1435. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus taxiphyllum;species deplanatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062372</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper ex Sullivant" date="unknown" rank="species">deplanatum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual,</publication_title>
      <place_in_publication>670. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species deplanatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isopterygium</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper ex Sullivant) Mitten" date="unknown" rank="species">deplanatum</taxon_name>
    <taxon_hierarchy>genus isopterygium;species deplanatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Plagiothecium</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper ex Sullivant) Spruce" date="unknown" rank="species">deplanatum</taxon_name>
    <taxon_hierarchy>genus plagiothecium;species deplanatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rhynchostegium</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper ex Sullivant) Rau &amp; Hervey" date="unknown" rank="species">deplanatum</taxon_name>
    <taxon_hierarchy>genus rhynchostegium;species deplanatum</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in thin to dense mats, light green to yellowish.</text>
      <biological_entity id="o20754" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20755" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o20754" id="r2990" name="in" negation="false" src="d0_s0" to="o20755" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 4 cm, 1–3 mm wide, complanate-foliate;</text>
    </statement>
    <statement id="d0_s2">
      <text>radiculose ventrally.</text>
      <biological_entity id="o20756" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="some_measurement" src="d0_s1" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves appressed-imbricate, close, ovate to ovatelanceolate, rarely oblong-lanceolate or oblong-ovate, symmetric to somewhat asymmetric, somewhat concave or flat, 0.9–2 × 0.4–0.8 mm;</text>
      <biological_entity id="o20757" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="symmetric to somewhat" value_original="symmetric to somewhat" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="appressed-imbricate" value_original="appressed-imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="close" value_original="close" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovatelanceolate rarely oblong-lanceolate or oblong-ovate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="ovatelanceolate rarely oblong-lanceolate or oblong-ovate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s3" value="symmetric to somewhat" value_original="symmetric to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s3" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or narrowly recurved just beyond base, serrulate throughout;</text>
      <biological_entity id="o20758" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="beyond base" constraintid="o20759" is_modifier="false" modifier="narrowly" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_shape" notes="" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o20759" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate or often abruptly acute or filiform, not twisted;</text>
      <biological_entity id="o20760" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character name="shape" src="d0_s5" value="often abruptly acute or filiform" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa double and very short or ecostate;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells many, quadrate to short-rectangular, 12–27 × 9–17 µm, in 1–several rows, 3–8 cells in marginal row;</text>
      <biological_entity id="o20761" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s6" value="ecostate" value_original="ecostate" />
      </biological_entity>
      <biological_entity id="o20762" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character char_type="range_value" from="12" from_unit="um" name="length" src="d0_s7" to="27" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s7" to="17" to_unit="um" />
      </biological_entity>
      <biological_entity id="o20763" name="row" name_original="rows" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="several" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o20764" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity constraint="marginal" id="o20765" name="row" name_original="row" src="d0_s7" type="structure" />
      <relation from="o20762" id="r2991" name="in" negation="false" src="d0_s7" to="o20763" />
      <relation from="o20764" id="r2992" name="in" negation="false" src="d0_s7" to="o20765" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells smooth;</text>
      <biological_entity constraint="laminal" id="o20766" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial cells 47–136 × 7–12 µm. Seta yellowish to red, 0.7–1 cm.</text>
      <biological_entity constraint="medial" id="o20767" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="47" from_unit="um" name="length" src="d0_s9" to="136" to_unit="um" />
        <character char_type="range_value" from="7" from_unit="um" name="width" src="d0_s9" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o20768" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s9" to="red" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s9" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule yellowish to light-brown, oblong or ovoid, straight or arcuate, 0.8–1.5 mm;</text>
      <biological_entity id="o20769" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s10" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s10" value="arcuate" value_original="arcuate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>operculum 0.3–0.5 mm.</text>
      <biological_entity id="o20770" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores 11–13 µm.</text>
      <biological_entity id="o20771" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s12" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded siliceous or calcareous soil and rock, base of trees, exposed tree roots, rotten logs, cedar swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="siliceous" modifier="shaded" />
        <character name="habitat" value="calcareous soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="base" constraint="of trees , exposed tree roots ," />
        <character name="habitat" value="trees" />
        <character name="habitat" value="exposed tree roots" />
        <character name="habitat" value="cedar swamps" modifier="rotten logs" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (60-2700 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="60" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Ont., Que., Sask.; Ariz., Ark., Conn., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Mex., N.Y., N.C., Ohio, Okla., Pa., S.Dak., Tenn., Vt., Va., W.Va., Wis.; Mexico; Central America (Honduras).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Taxiphyllum deplanatum is widespread in northeastern North America with disjunct populations in New Brunswick, Quebec, Saskatchewan, Arizona, and New Mexico. Taxiphyllum deplanatum has often been confused with T. taxirameum, but the two are easily distinguished superficially and microscopically. The flaccid, appressed-imbricate leaves characteristic of T. deplanatum will easily separate it from T. taxirameum with its rigid, usually distant, wide-spreading to squarrose leaves. The alar regions of T. deplanatum are well differentiated with quadrate to short-rectangular cells, 3–8 in the marginal rows, which is in striking contrast to the poorly differentiated alar regions of T. taxirameum that have only a few rectangular cells on the margins. Taxiphyllum deplanatum has been reported for the Gulf Coast region, based on a Louisiana specimen (R. R. Ireland 1969b). However, it was re-examined and found to be a misidentified specimen of T. taxirameum as W. D. Reese (1984) noted. The capsules are extremely rare.</discussion>
  
</bio:treatment>