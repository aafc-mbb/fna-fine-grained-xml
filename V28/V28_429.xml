<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">hygrohypnum</taxon_name>
    <taxon_name authority="(Schimper) Loeske" date="1905" rank="species">eugyrium</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Bot. Vereins Prov. Brandenburg</publication_title>
      <place_in_publication>46: 198. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus hygrohypnum;species eugyrium</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002195</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Limnobium</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="species">eugyrium</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>6: 73, plate 579. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus limnobium;species eugyrium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliergon</taxon_name>
    <taxon_name authority="(Schimper) Kindberg" date="unknown" rank="species">eugyrium</taxon_name>
    <taxon_hierarchy>genus calliergon;species eugyrium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hygrohypnum</taxon_name>
    <taxon_name authority="(Schimper) Kanda" date="unknown" rank="species">eugyrium</taxon_name>
    <taxon_name authority="(Schimper) Brotherus" date="unknown" rank="variety">mackayi</taxon_name>
    <taxon_hierarchy>genus hygrohypnum;species eugyrium;variety mackayi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pseudohygrohypnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">eugyrium</taxon_name>
    <taxon_hierarchy>genus pseudohygrohypnum;species eugyrium</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants soft, yellow-green, pale green, or bright green, often with golden brown to deep metallic red mottling, usually with satinlike sheen, infrequently becoming dark red, reddish-brown, or brown with age.</text>
      <biological_entity id="o13479" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character char_type="range_value" from="golden brown" modifier="often" name="coloration" src="d0_s0" to="deep metallic red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="mottling" value_original="mottling" />
        <character is_modifier="false" modifier="with satinlike sheen; infrequently becoming; becoming" name="coloration" src="d0_s0" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character constraint="with age" constraintid="o13480" is_modifier="false" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13480" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 6 cm, usually shorter, somewhat denuded basally, irregularly branched;</text>
      <biological_entity id="o13481" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s1" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="somewhat; basally" name="pubescence" src="d0_s1" value="denuded" value_original="denuded" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis poorly developed with outer tangential wall slightly thinner, epidermal-cells small, walls thick, similar to or differentiated from subadjacent cortical cells, central strand present.</text>
      <biological_entity id="o13482" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s2" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity constraint="outer tangential" id="o13483" name="wall" name_original="wall" src="d0_s2" type="structure" />
      <biological_entity id="o13484" name="epidermal-cell" name_original="epidermal-cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o13485" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thick" value_original="thick" />
        <character constraint="from cortical, cells" constraintid="o13486, o13487" is_modifier="false" name="variability" src="d0_s2" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o13486" name="cortical" name_original="cortical" src="d0_s2" type="structure" />
      <biological_entity id="o13487" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="central" id="o13488" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o13482" id="r1900" modifier="poorly" name="developed with" negation="false" src="d0_s2" to="o13483" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves loosely appressed, imbricate, or loosely spreading, straight or falcate, little different when dry or moist, ovate, oblong-lanceolate, lanceolate, or rarely broadly ovate or broadly oblong-lanceolate, usually quite concave, (0.9–) 1.1–1.8 (–2) × (0.3–) 0.5–0.8 (–1.2) mm;</text>
      <biological_entity id="o13489" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="when dry or moist" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="usually quite" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_length" src="d0_s3" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s3" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_width" src="d0_s3" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, folded along one side as wing or involute distally to become somewhat tubular, entire or with few small teeth in apex;</text>
      <biological_entity id="o13490" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="along side" constraintid="o13491" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o13491" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o13492" name="wing" name_original="wing" src="d0_s4" type="structure" />
      <biological_entity id="o13493" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o13494" name="wing" name_original="wing" src="d0_s4" type="structure" />
      <biological_entity id="o13495" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s4" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o13496" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o13491" id="r1901" name="as" negation="false" src="d0_s4" to="o13492" />
      <relation from="o13491" id="r1902" modifier="as wing or involute to become somewhat tubular , entire or with few small teeth" name="as" negation="false" src="d0_s4" to="o13493" />
      <relation from="o13493" id="r1903" name="as" negation="false" src="d0_s4" to="o13494" />
      <relation from="o13493" id="r1904" modifier="as wing or involute to become somewhat tubular , entire or with few small teeth" name="as" negation="false" src="d0_s4" to="o13495" />
      <relation from="o13495" id="r1905" name="in" negation="false" src="d0_s4" to="o13496" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute or gradually to abruptly long-tapering, may give appearance of apiculus due to involute margins;</text>
      <biological_entity id="o13497" name="apex" name_original="apex" src="d0_s5" type="structure" constraint="apiculu; apiculu">
        <character char_type="range_value" from="acute or" name="shape" src="d0_s5" to="gradually abruptly long-tapering" />
      </biological_entity>
      <biological_entity id="o13498" name="appearance" name_original="appearance" src="d0_s5" type="structure" />
      <biological_entity id="o13499" name="apiculu" name_original="apiculus" src="d0_s5" type="structure" />
      <biological_entity id="o13500" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s5" value="due-to-involute" value_original="due-to-involute" />
      </biological_entity>
      <relation from="o13497" id="r1906" name="give" negation="false" src="d0_s5" to="o13498" />
      <relation from="o13497" id="r1907" name="part_of" negation="false" src="d0_s5" to="o13499" />
    </statement>
    <statement id="d0_s6">
      <text>costa double, usually short, slender, frequently one or both arms reaching mid leaf, rarely single to mid leaf or beyond;</text>
      <biological_entity id="o13501" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o13502" name="arm" name_original="arms" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="frequently; rarely" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
      <biological_entity constraint="mid" id="o13503" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <relation from="o13502" id="r1908" modifier="frequently" name="reaching" negation="false" src="d0_s6" to="o13503" />
    </statement>
    <statement id="d0_s7">
      <text>alar cells 6–12, quadrate to rectangular, enlarged or inflated, walls thin, hyaline, dark red, or reddish-brown with age, region clearly defined;</text>
      <biological_entity constraint="mid" id="o13504" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o13505" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="12" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="rectangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity id="o13506" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark red" value_original="dark red" />
        <character constraint="with age" constraintid="o13507" is_modifier="false" name="coloration" src="d0_s7" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity id="o13507" name="age" name_original="age" src="d0_s7" type="structure" />
      <biological_entity id="o13508" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="clearly" name="prominence" src="d0_s7" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>basal laminal cells shorter, or longer and wider than medial cells, walls incrassate, pitted;</text>
      <biological_entity constraint="basal laminal" id="o13509" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
        <character constraint="than medial cells" constraintid="o13510" is_modifier="false" name="width" src="d0_s8" value="longer and wider" value_original="longer and wider" />
      </biological_entity>
      <biological_entity constraint="medial" id="o13510" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o13511" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial cells fusiform-flexuose or linear-flexuose, 45–75 × 4–5 µm; apical cells shorter;</text>
      <biological_entity constraint="medial" id="o13512" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="fusiform-flexuose" value_original="fusiform-flexuose" />
        <character is_modifier="false" name="course" src="d0_s9" value="linear-flexuose" value_original="linear-flexuose" />
        <character char_type="range_value" from="45" from_unit="um" name="length" src="d0_s9" to="75" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s9" to="5" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="apical" id="o13513" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>marginal cells similar to medial cells.</text>
      <biological_entity constraint="medial" id="o13515" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <relation from="o13514" id="r1909" name="to" negation="false" src="d0_s10" to="o13515" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="marginal" id="o13514" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perichaetial inner leaves linear-lanceolate, long, plicae 2–4, margins entire or coarsely serrulate in apex, apex acute to acuminate, costa variable.</text>
      <biological_entity constraint="perichaetial inner" id="o13516" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o13517" name="plica" name_original="plicae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
      <biological_entity id="o13518" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character constraint="in apex" constraintid="o13519" is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s12" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13519" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity id="o13520" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
      </biological_entity>
      <biological_entity id="o13521" name="costa" name_original="costa" src="d0_s12" type="structure">
        <character is_modifier="false" name="variability" src="d0_s12" value="variable" value_original="variable" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta yellowbrown to red, 1.3–1.6 cm.</text>
      <biological_entity id="o13522" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s13" to="red" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s13" to="1.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule with endostome cilia 2 or 3.</text>
      <biological_entity id="o13523" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
      <biological_entity constraint="endostome" id="o13524" name="cilium" name_original="cilia" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s14" unit="or" value="3" value_original="3" />
      </biological_entity>
      <relation from="o13523" id="r1910" name="with" negation="false" src="d0_s14" to="o13524" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Irrigated acidic rock in or along montane streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic rock" modifier="irrigated" constraint="in" />
        <character name="habitat" value="montane streams" modifier="in or along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (150-800 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="150" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Que.; Conn., Ga., Maine, Md., N.H., N.Y., N.C., Ohio, Pa., Tenn., Vt., Va., W.Va.; Europe; e Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Falcate-leaved plants of Hygrohypnum eugyrium may be confused with H. luridum and H. ochraceum. Hygrohypnum eugyrium is distinguished by quadrate to rectangular alar cells conspicuously inflated and regularly excavated, becoming red to reddish brown with age, which often occludes cell lumens. In addition, the hyalodermis is much weaker and less conspicuous than that of H. ochraceum, and under a hand lens or dissecting scope the abaxial leaf surface exhibits a striking satinlike sheen. The stems may be prostrate or ascending; the leaves clasp the stems basally and usually have alar cells that are thin-walled at the margins and incrassate in the inner leaf.</discussion>
  
</bio:treatment>