<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William R. Buck</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">616</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">617</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">LEMBOPHYLLACEAE</taxon_name>
    <taxon_hierarchy>family LEMBOPHYLLACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10487</other_info_on_name>
  </taxon_identification>
  <number>78.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, pale to dark green or golden.</text>
      <biological_entity id="o3729" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s0" to="dark green or golden" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems terete-foliate, irregularly pinnate to regularly 2-pinnate or 3-pinnate, often somewhat stipitate;</text>
      <biological_entity id="o3730" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character char_type="range_value" from="irregularly pinnate" name="architecture_or_shape" src="d0_s1" to="regularly 2-pinnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="3-pinnate" value_original="3-pinnate" />
        <character is_modifier="false" modifier="often somewhat" name="architecture" src="d0_s1" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, cortical cells small, walls incrassate;</text>
      <biological_entity id="o3731" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="cortical" id="o3732" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o3733" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia absent;</text>
      <biological_entity id="o3734" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pseudoparaphyllia foliose;</text>
      <biological_entity id="o3735" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliose" value_original="foliose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>axillary hair basal-cells 1 or 2, short, brown, apical cells 1–4, elongate, hyaline.</text>
      <biological_entity constraint="hair" id="o3736" name="basal-cell" name_original="basal-cells" src="d0_s5" type="structure" constraint_original="axillary hair">
        <character name="quantity" src="d0_s5" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3737" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem and branch leaves often differentiated, loosely appressed, usually ovate, usually concave;</text>
      <biological_entity id="o3738" name="stem" name_original="stem" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="branch" id="o3739" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base decurrent;</text>
      <biological_entity id="o3740" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa single or double, short, sometimes weak on stipe and stem-leaves and strong on branch leaves;</text>
      <biological_entity id="o3742" name="stipe" name_original="stipe" src="d0_s8" type="structure" />
      <biological_entity id="o3743" name="stem-leaf" name_original="stem-leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="branch" id="o3744" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>alar cells usually well differentiated, usually quadrate, often excavate;</text>
      <biological_entity id="o3741" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character name="quantity" src="d0_s8" value="double" value_original="double" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character constraint="on stem-leaves" constraintid="o3743" is_modifier="false" modifier="sometimes" name="fragility" src="d0_s8" value="weak" value_original="weak" />
        <character constraint="on branch leaves" constraintid="o3744" is_modifier="false" name="fragility" notes="" src="d0_s8" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o3745" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually well" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s9" value="excavate" value_original="excavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>medial laminal cells short to linear, smooth or prorulose.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="medial laminal" id="o3746" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s10" value="prorulose" value_original="prorulose" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta long or sometimes short, smooth or roughened throughout or distally.</text>
      <biological_entity id="o3747" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="throughout; throughout; distally" name="relief" src="d0_s12" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule erect or sometimes horizontal, cylindric, symmetric or sometimes curved;</text>
      <biological_entity id="o3748" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus usually differentiated;</text>
      <biological_entity id="o3749" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s14" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum conic to short-rostrate, straight;</text>
      <biological_entity id="o3750" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s15" to="short-rostrate" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome double, perfect or sometimes reduced;</text>
      <biological_entity id="o3751" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="perfect" value_original="perfect" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s16" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome, when not reduced, with external surface cross-striolate proximally, papillose distally, when reduced, striate, papillose, or smooth;</text>
      <biological_entity id="o3752" name="exostome" name_original="exostome" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="distally" name="relief" notes="" src="d0_s17" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="when reduced" name="relief" src="d0_s17" value="striate" value_original="striate" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s17" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s17" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="external" id="o3753" name="surface" name_original="surface" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s17" value="cross-striolate" value_original="cross-striolate" />
      </biological_entity>
      <relation from="o3752" id="r485" modifier="when not reduced" name="with" negation="false" src="d0_s17" to="o3753" />
    </statement>
    <statement id="d0_s18">
      <text>endostome basal membrane high or low, segments long, narrowly perforate, cilia nodose or appendiculate.</text>
      <biological_entity id="o3754" name="endostome" name_original="endostome" src="d0_s18" type="structure" />
      <biological_entity constraint="basal" id="o3755" name="membrane" name_original="membrane" src="d0_s18" type="structure">
        <character is_modifier="false" name="height" src="d0_s18" value="high" value_original="high" />
        <character is_modifier="false" name="position" src="d0_s18" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o3756" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s18" value="long" value_original="long" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s18" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o3757" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="nodose" value_original="nodose" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Calyptra cucullate, smooth or slightly roughened, naked or hairy.</text>
      <biological_entity id="o3758" name="calyptra" name_original="calyptra" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s19" value="roughened" value_original="roughened" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="naked" value_original="naked" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia, Atlantic Islands, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 14, species ca. 50 (3 genera, 7 species in the flora).</discussion>
  <discussion>Lembophyllaceae are largely a south-temperate family, but also extend into the tropics. The plants tend to have terete-foliate stems with ovate, concave leaves. The costa is variable, sometimes on different leaves of the same plant. Many of the genera have perfect hypnoid peristomes, but there are genera (for example, Bestia) with reduced peristomes. The calyptrae being frequently hairy is an unusual character in the flora area.</discussion>
  <references>
    <reference>Crum, H. A. 1987. Bestia, Tripterocladium, and Isothecium: An explication of relationships. Bryologist 90: 40–42.</reference>
    <reference>Quandt, D. et al. 2009. Back to the future? Molecules take us back to the 1925 classification of the Lembophyllaceae (Bryopsida). Syst. Bot. 34: 443–454.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costae short and double or rarely single in isolated leaves; stem and branch leaves differentiated.</description>
      <determination>3 Tripterocladium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Costae single; stem and branch leaves similar</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Medial laminal cells less than 4:1; capsules erect; peristome reduced.</description>
      <determination>1 Bestia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Medial laminal cells more than 5:1; capsules suberect, inclined, or rarely erect; peristome perfect.</description>
      <determination>2 Isothecium</determination>
    </key_statement>
  </key>
</bio:treatment>