<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jillian D. Bainard,Terry T. McIntosh</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">TRACHYCYSTIS</taxon_name>
    <place_of_publication>
      <publication_title>Not. Sällsk. Fauna Fl. Fenn. Förh.</publication_title>
      <place_in_publication>9: 80. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus TRACHYCYSTIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek trachys, rough, and cystis, bladder or bag, alluding to mammillose laminal cells</other_info_on_name>
    <other_info_on_name type="fna_id">133230</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (0.7–) 1–2.5 cm, in loose tufts or mats.</text>
      <biological_entity id="o1634" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1635" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o1636" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o1634" id="r216" name="in" negation="false" src="d0_s0" to="o1635" />
      <relation from="o1634" id="r217" name="in" negation="false" src="d0_s0" to="o1636" />
    </statement>
    <statement id="d0_s1">
      <text>Stems brown or reddish-brown, erect, branches few, short, not dendroid;</text>
      <biological_entity id="o1637" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o1638" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids reddish-brown, macronemata and micronemata present proximally on stem.</text>
      <biological_entity id="o1639" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="reddish-brown" value_original="reddish-brown" />
        <character constraint="on stem" constraintid="o1640" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1640" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves green, incurved upwards, crisped or contorted when dry, erect-spreading, flat, or weakly keeled when moist, ovate-elliptic or sometimes elliptic, (1.5–) 2.2–2.6 mm;</text>
      <biological_entity id="o1641" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="upwards" name="orientation" src="d0_s3" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s3" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base decurrent;</text>
      <biological_entity id="o1642" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane, green, pale-brown, or sometimes reddish, 2-stratose, toothed to below mid leaf, sometimes to base, teeth usually paired and sharp, sometimes single;</text>
      <biological_entity id="o1643" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character constraint="below mid leaf" constraintid="o1644" is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o1644" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s5" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o1645" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o1646" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="paired" value_original="paired" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sharp" value_original="sharp" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s5" value="single" value_original="single" />
      </biological_entity>
      <relation from="o1643" id="r218" modifier="sometimes" name="to" negation="false" src="d0_s5" to="o1645" />
    </statement>
    <statement id="d0_s6">
      <text>apex acute or acuminate, usually cuspidate, cusp often lightly toothed;</text>
      <biological_entity id="o1647" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o1648" name="cusp" name_original="cusp" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often lightly" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa percurrent, distal abaxial surface toothed;</text>
      <biological_entity id="o1649" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o1650" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells short-elongate or ± isodiametric, (9–) 12–18 µm, often in longitudinal rows, mammillose on both surfaces, strongly collenchymatous, walls not pitted;</text>
      <biological_entity constraint="medial laminal" id="o1651" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="9" from_unit="um" name="atypical_some_measurement" src="d0_s8" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s8" to="18" to_unit="um" />
        <character constraint="on surfaces" constraintid="o1653" is_modifier="false" name="relief" notes="" src="d0_s8" value="mammillose" value_original="mammillose" />
        <character is_modifier="false" modifier="strongly" name="architecture" notes="" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity id="o1652" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o1653" name="surface" name_original="surfaces" src="d0_s8" type="structure" />
      <biological_entity id="o1654" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o1651" id="r219" modifier="often" name="in" negation="false" src="d0_s8" to="o1652" />
    </statement>
    <statement id="d0_s9">
      <text>marginal cells differentiated, linear, in 1–2 (–3) rows.</text>
      <biological_entity id="o1656" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s9" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <relation from="o1655" id="r220" name="in" negation="false" src="d0_s9" to="o1656" />
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction as flagelliform branches, often copious, 2–4 mm, from distal leaf-axils, with small, appressed leaves.</text>
      <biological_entity id="o1657" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="shape" src="d0_s10" value="flagelliform" value_original="flagelliform" />
        <character is_modifier="false" modifier="often" name="quantity" src="d0_s10" value="copious" value_original="copious" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1658" name="leaf-axil" name_original="leaf-axils" src="d0_s10" type="structure" />
      <biological_entity id="o1659" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
    </statement>
    <statement id="d0_s12">
      <text>[Seta single, red, 2–3 cm, flexuose. Capsule horizontal to pendent, yellowish-brown, cylindric, 2–3 mm; operculum conic or short-rostrate; exostome yellow to yellowish green; endostome reddish-brown. Spores 20 µm].</text>
      <biological_entity constraint="marginal" id="o1655" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska, Europe, Asia; circumtemperate.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="circumtemperate" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (1 in the flora).</discussion>
  <discussion>Trachycystis is distinguished from other genera in Mniaceae by mammillose laminal cells and asexual reproduction by flagelliform branches. Characteristics of the sporophyte, as presented here, are taken from Li X. J. et al. (2007) and from examination of Asian material.</discussion>
  
</bio:treatment>