<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">150</other_info_on_meta>
    <other_info_on_meta type="illustration_page">152</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">plagiobryoides</taxon_name>
    <taxon_name authority="(Röll ex Renauld &amp; Cardot) J. R. Spence" date="2009" rank="species">renauldii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>91: 499. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus plagiobryoides;species renauldii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099241</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Röll ex Renauld &amp; Cardot" date="unknown" rank="species">renauldii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Roy. Bot. Belgique</publication_title>
      <place_in_publication>38(Mém.): 13. 1900 (as renauldi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species renauldii</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dark green to olive green.</text>
      <biological_entity id="o1617" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="dark green" name="coloration" src="d0_s0" to="olive green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–3 (–4) cm, not or weakly julaceous, innovations few;</text>
      <biological_entity id="o1618" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="not; weakly" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
      <biological_entity id="o1619" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids few on proximal stem.</text>
      <biological_entity id="o1620" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character constraint="on proximal stem" constraintid="o1621" is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1621" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves distant, somewhat contorted when dry, erect when moist, broadly ovate to suborbicular, concave, 0.5–2 (–3.5) mm;</text>
      <biological_entity id="o1622" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distant" value_original="distant" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s3" to="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base somewhat decurrent, red-green;</text>
      <biological_entity id="o1623" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="red-green" value_original="red-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane or recurved proximally, 1-stratose, limbidium absent;</text>
      <biological_entity id="o1624" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o1625" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex broadly acute to obtuse;</text>
      <biological_entity id="o1626" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa not reaching apex to rarely percurrent;</text>
      <biological_entity id="o1627" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o1628" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o1627" id="r215" name="reaching" negation="true" src="d0_s7" to="o1628" />
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells rectangular, (60–) 80–100 × 16–24 µm, 3–5: 1, bulging, walls thin;</text>
      <biological_entity constraint="proximal laminal" id="o1629" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character char_type="range_value" from="60" from_unit="um" name="atypical_length" src="d0_s8" to="80" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="80" from_unit="um" name="length" src="d0_s8" to="100" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="width" src="d0_s8" to="24" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s8" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o1630" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>distal cells irregular, 25–50 × 16–25 µm, 1–2 (–3):1, walls thin.</text>
      <biological_entity constraint="distal" id="o1631" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_course" src="d0_s9" value="irregular" value_original="irregular" />
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s9" to="50" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="width" src="d0_s9" to="25" to_unit="um" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction by rhizoidal tubers, on rhizoids at base of stem and in most proximal leaves, (200–) 300–500 µm. Sexual condition apparently dioicous.</text>
      <biological_entity id="o1632" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" modifier="apparently" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sporophytes unknown.</text>
      <biological_entity id="o1633" name="sporophyte" name_original="sporophytes" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, soil over rock, in streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (2200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="2200" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Collections of Plagiobryoides renauldii were made in 2007 on the east side of the Chiricahua Mountains along a stream, occurring with P. incrassatolimbata. This is a robust aquatic species of Plagiobryoides similar overall to P. incrassatolimbata, but differing in the lack of a limbidium, the rounded apex, distant leaves, and rhizoidal tubers.</discussion>
  
</bio:treatment>