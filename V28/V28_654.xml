<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">419</other_info_on_meta>
    <other_info_on_meta type="mention_page">426</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="(Mitten) Sullivant" date="1874" rank="species">acutum</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Musc., suppl.,</publication_title>
      <place_in_publication>99. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species acutum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099018</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">acutum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 33, plate 6 [upper right]. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species acutum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="species">pseudocollinum</taxon_name>
    <taxon_hierarchy>genus brachythecium;species pseudocollinum</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in loose to dense mats, yellowish to brownish.</text>
      <biological_entity id="o13616" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s0" to="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13617" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o13616" id="r1920" name="in" negation="false" src="d0_s0" to="o13617" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 7 cm, creeping, terete or occasionally subcomplanate-foliate, irregularly pinnate, branches to 8 mm, straight, terete to subcomplanate-foliate.</text>
      <biological_entity id="o13618" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="subcomplanate-foliate" value_original="subcomplanate-foliate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o13619" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="8" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subcomplanate-foliate" value_original="subcomplanate-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect to erect-spreading, loosely imbricate to somewhat spaced, broadly to narrowly ovate-triangular, broadest at 1/10 leaf length or below, not concave, not or slightly plicate, 2–2.8 × 0.8–1.2 mm;</text>
      <biological_entity id="o13620" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate to somewhat" value_original="imbricate to somewhat" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="erect-spreading" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate to somewhat" value_original="imbricate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s2" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s2" value="ovate-triangular" value_original="ovate-triangular" />
        <character constraint="at leaf" constraintid="o13621" is_modifier="false" name="width" src="d0_s2" value="broadest" value_original="broadest" />
        <character is_modifier="false" modifier="not; slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13621" name="leaf" name_original="leaf" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="1/10" value_original="1/10" />
        <character is_modifier="false" modifier="below; not" name="length" notes="" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base rounded, inconspicuously short-decurrent;</text>
      <biological_entity id="o13622" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="inconspicuously" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, occasionally recurved distally, minutely and evenly serrulate to subentire;</text>
      <biological_entity id="o13623" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="occasionally; distally" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="evenly serrulate" modifier="minutely" name="shape" src="d0_s4" to="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex gradually tapered to narrow acumen or gradually acuminate, occasionally short-apiculate;</text>
      <biological_entity id="o13624" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o13625" name="acumen" name_original="acumen" src="d0_s5" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s5" value="short-apiculate" value_original="short-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 60–70% leaf length, strong, terminal spine absent;</text>
      <biological_entity id="o13626" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o13627" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="60-70%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells subquadrate or short-rectangular, small to enlarged, 15–30 × 10–14 µm, walls moderately thick, region somewhat differentiated or almost undifferentiated, usually inconspicuous, of 4–7 × 5 cells, pellucid;</text>
      <biological_entity constraint="terminal" id="o13628" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o13629" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="small" name="size" src="d0_s7" to="enlarged" />
        <character char_type="range_value" from="15" from_unit="um" name="length" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s7" to="14" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13630" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13631" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="somewhat" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" modifier="almost; almost" name="prominence" src="d0_s7" value="undifferentiated" value_original="undifferentiated" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <biological_entity id="o13632" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="7" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
      <relation from="o13631" id="r1921" name="consist_of" negation="false" src="d0_s7" to="o13632" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear, 60–135 × 8–11 µm, walls not or moderately porose;</text>
      <biological_entity constraint="laminal" id="o13633" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="60" from_unit="um" name="length" src="d0_s8" to="135" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s8" to="11" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13634" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s8" value="porose" value_original="porose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal-cells to 20–25 × 10–13 µm, region in 2–5 rows.</text>
      <biological_entity id="o13635" name="basal-cell" name_original="basal-cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="length" src="d0_s9" to="25" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s9" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity id="o13636" name="region" name_original="region" src="d0_s9" type="structure" />
      <biological_entity id="o13637" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o13636" id="r1922" name="in" negation="false" src="d0_s9" to="o13637" />
    </statement>
    <statement id="d0_s10">
      <text>Branch leaves with margins often recurved at base, usually less serrulate;</text>
      <biological_entity constraint="branch" id="o13638" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually less" name="architecture_or_shape" notes="" src="d0_s10" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13639" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character constraint="at base" constraintid="o13640" is_modifier="false" modifier="often" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o13640" name="base" name_original="base" src="d0_s10" type="structure" />
      <relation from="o13638" id="r1923" name="with" negation="false" src="d0_s10" to="o13639" />
    </statement>
    <statement id="d0_s11">
      <text>basal laminal cells almost undifferentiated or alar region of 2–7 cells, pellucid, (often hidden by recurved margins).</text>
      <biological_entity id="o13643" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <relation from="o13641" id="r1924" name="consist_of" negation="false" src="d0_s11" to="o13643" />
      <relation from="o13642" id="r1925" name="consist_of" negation="false" src="d0_s11" to="o13643" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="basal laminal" id="o13641" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="pellucid" value_original="pellucid" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="autoicous" value_original="autoicous" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o13642" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="almost" name="prominence" src="d0_s11" value="undifferentiated" value_original="undifferentiated" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="pellucid" value_original="pellucid" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta redbrown, 2–4 cm, smooth.</text>
      <biological_entity id="o13644" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s13" to="4" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule inclined to horizontal, redbrown, ovate, slightly curved, 15–20 mm;</text>
      <biological_entity id="o13645" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s14" to="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>annulus separating by fragments;</text>
      <biological_entity id="o13646" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character constraint="by fragments" constraintid="o13647" is_modifier="false" name="arrangement" src="d0_s15" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o13647" name="fragment" name_original="fragments" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>operculum conic.</text>
      <biological_entity id="o13648" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 13–18 µm.</text>
      <biological_entity id="o13649" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s17" to="18" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, peat, fens, swamps, rotten logs in swampy forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="peat" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="swampy forests" modifier="rotten logs in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Man., Nfld. and Labr. (Nfld.), Nunavut, Ont., Que., Sask., Yukon; Alaska, Calif., Colo., Del., Idaho, Ill., Ind., Iowa, Maine, Mo., Mont., Nebr., N.J., N.Y., N.Dak., Ohio, Pa., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachythecium acutum is distinguished by the evenly tapered, triangular, and weakly or not plicate leaves. Brachythecium acutum has been treated as an eplicate form of B. salebrosum (for example, H. A. Crum and L. E. Anderson 1981). This conclusion may have been based on the fact that sometimes, especially in drier habitats, its alar cells are relatively smaller and form a square group; this pattern is also known for the Eurasian B. mildeanum (Schimper) Schimper. However, optimally developed B. acutum is distinct in having homogeneous areolation across the leaf base, quite regularly triangular leaves, minute teeth along leaf margins, not or very weakly plicate leaves, and unusually long setae.</discussion>
  
</bio:treatment>