<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="mention_page">156</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="mention_page">171</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hornschuch" date="unknown" rank="genus">ptychostomum</taxon_name>
    <taxon_name authority="(Bridel) J. R. Spence" date="2009" rank="subgenus">CLADODIUM</taxon_name>
    <taxon_name authority="(Itzigsohn) J. R. Spence" date="2005" rank="species">neodamense</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 21. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus ptychostomum;subgenus cladodium;species neodamense;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Itzigsohn" date="unknown" rank="species">neodamense</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. A. Müller, Syn. Musc. Frond.</publication_title>
      <place_in_publication>1: 258. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species neodamense</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense or open turfs, green, red-green, or yellow-green.</text>
      <biological_entity id="o16160" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red-green" value_original="red-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red-green" value_original="red-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16161" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
      <relation from="o16160" id="r2314" name="in" negation="false" src="d0_s0" to="o16161" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 2–4 (–6) cm, tufted, comose, innovations elongate and evenly foliate;</text>
      <biological_entity id="o16162" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="comose" value_original="comose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>often strongly radiculose.</text>
      <biological_entity id="o16163" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves yellow-green to green, proximal leaves distinctly black-gray with age, crowded, strongly twisted to contorted when dry, ovate, strongly concave, (1–) 2–3 (–4) mm, not much enlarged toward stem apex;</text>
      <biological_entity id="o16164" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s3" to="green" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16165" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="with age" constraintid="o16166" is_modifier="false" modifier="distinctly" name="coloration" src="d0_s3" value="black-gray" value_original="black-gray" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character constraint="toward stem apex" constraintid="o16167" is_modifier="false" modifier="not much" name="size" src="d0_s3" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o16166" name="age" name_original="age" src="d0_s3" type="structure" />
      <biological_entity constraint="stem" id="o16167" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base wide, not or weakly decurrent;</text>
      <biological_entity id="o16168" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="not; weakly" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins revolute to mid leaf or beyond, limbidium strong, in 2 or 3 rows;</text>
      <biological_entity id="o16169" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="mid" id="o16170" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o16171" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s5" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex broadly acute to obtuse;</text>
      <biological_entity id="o16172" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa not reaching apex to percurrent, rarely short-excurrent, awn smooth;</text>
      <biological_entity id="o16173" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <biological_entity id="o16174" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o16175" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
        <character is_modifier="true" modifier="rarely" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o16173" id="r2315" name="reaching" negation="true" src="d0_s7" to="o16174" />
      <relation from="o16173" id="r2316" name="to" negation="false" src="d0_s7" to="o16175" />
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells 3–4: 1, same width or wider than more distal cells;</text>
      <biological_entity constraint="proximal laminal" id="o16176" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character constraint="than more distal cells" constraintid="o16177" is_modifier="false" name="width" src="d0_s8" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16177" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>medial and distal cells rhomboidal, 14–20 µm wide, 2–3: 1, walls usually firm to incrassate.</text>
      <biological_entity constraint="medial and distal" id="o16178" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="14" from_unit="um" name="width" src="d0_s9" to="20" to_unit="um" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o16179" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" name="size" src="d0_s9" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 1–3 (–4) cm.</text>
      <biological_entity id="o16180" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule brown, elongate-ovate, symmetric, 3–5 mm, mouth yellow;</text>
      <biological_entity id="o16181" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="elongate-ovate" value_original="elongate-ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16182" name="mouth" name_original="mouth" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic, apiculate;</text>
      <biological_entity id="o16183" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome well developed;</text>
      <biological_entity id="o16184" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s15" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth yellow basally, hyaline distally, lamellae usually straight mid-tooth, pores absent along mid line;</text>
      <biological_entity constraint="exostome" id="o16185" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o16186" name="lamella" name_original="lamellae" src="d0_s16" type="structure" />
      <biological_entity id="o16187" name="mid-tooth" name_original="mid-tooth" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="usually" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o16188" name="pore" name_original="pores" src="d0_s16" type="structure">
        <character constraint="along mid line" constraintid="o16189" is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="mid" id="o16189" name="line" name_original="line" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>endostome not adherent to exostome, basal membrane high, 1/2 exostome height, segments with ovate perforations, cilia long, appendiculate.</text>
      <biological_entity id="o16190" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character constraint="to exostome" constraintid="o16191" is_modifier="false" modifier="not" name="fusion" src="d0_s17" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o16191" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity constraint="basal" id="o16192" name="membrane" name_original="membrane" src="d0_s17" type="structure">
        <character is_modifier="false" name="height" src="d0_s17" value="high" value_original="high" />
        <character name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o16193" name="exostome" name_original="exostome" src="d0_s17" type="structure" />
      <biological_entity id="o16194" name="segment" name_original="segments" src="d0_s17" type="structure" />
      <biological_entity id="o16195" name="perforation" name_original="perforations" src="d0_s17" type="structure">
        <character is_modifier="true" name="height" src="d0_s17" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o16196" name="cilium" name_original="cilia" src="d0_s17" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
      <relation from="o16194" id="r2317" name="with" negation="false" src="d0_s17" to="o16195" />
    </statement>
    <statement id="d0_s18">
      <text>Spores (10–) 12–16 µm, finely papillose, pale-yellow or green.</text>
      <biological_entity id="o16197" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="atypical_some_measurement" src="d0_s18" to="12" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s18" to="16" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="green" value_original="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet soil, soil over rock, often calcareous</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" />
        <character name="habitat" value="soil" constraint="over rock" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="calcareous" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr., N.W.T., Nunavut, Ont., Yukon; Alaska, Calif., Colo.; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ptychostomum neodamense is closely related to P. bimum, P. subneodamense, and P. pseudotriquetrum, differing in its ovate, blunt, mostly non-decurrent leaves crowded along the stem. The costal awns are stout. D. T. Holyoak and L. Hedenäs (2006) did not consider P. neodamense a good species, based on a very limited sampling from Europe, but their results could also be interpreted to suggest that it may be distinct. Disjunct material from California closely matches European collections, but Colorado material differs somewhat in overall habit and ecology.</discussion>
  
</bio:treatment>