<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">148</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="illustration_page">149</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="(Müller Hal. ex Brotherus) J. R. Spence &amp; H. A. Ramsay" date="unknown" rank="genus">leptostomopsis</taxon_name>
    <taxon_name authority="(Müller Hal.) J. R. Spence" date="unknown" rank="species">systylia</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 70. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus leptostomopsis;species systylia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099207</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="species">systylium</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>1: 320. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species systylium</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense cushions or turfs, green-silver to pink-silver.</text>
      <biological_entity id="o5760" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green-silver" name="coloration" notes="" src="d0_s0" to="pink-silver" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5761" name="cushion" name_original="cushions" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o5762" name="turf" name_original="turfs" src="d0_s0" type="structure" />
      <relation from="o5760" id="r789" name="in" negation="false" src="d0_s0" to="o5761" />
      <relation from="o5760" id="r790" name="in" negation="false" src="d0_s0" to="o5762" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.4–2 cm.</text>
      <biological_entity id="o5763" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect when moist, spathulate to somewhat elongate-ovate, concave, 0.3–2 mm;</text>
      <biological_entity id="o5764" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spathulate to somewhat" value_original="spathulate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="elongate-ovate" value_original="elongate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate to serrate near apex, limbidium absent;</text>
      <biological_entity id="o5765" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="near apex" constraintid="o5766" from="serrulate" name="architecture_or_shape" src="d0_s3" to="serrate" />
      </biological_entity>
      <biological_entity id="o5766" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o5767" name="limbidium" name_original="limbidium" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex broadly rounded to acute, not hyaline;</text>
      <biological_entity id="o5768" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s4" to="acute" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent in proximal leaves to long-excurrent in distal leaves, awn pigmented proximally, hyaline distally, weakly to strongly recurved when dry;</text>
      <biological_entity id="o5769" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character constraint="in proximal leaves" constraintid="o5770" is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5770" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="in distal leaves" constraintid="o5771" is_modifier="false" name="architecture" src="d0_s5" value="long-excurrent" value_original="long-excurrent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o5771" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o5772" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s5" value="pigmented" value_original="pigmented" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells green, hexagonal to rhomboidal, 40–60 µm, 3–4+:1, walls not thick or sinuate.</text>
      <biological_entity constraint="distal laminal" id="o5773" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character char_type="range_value" from="hexagonal" name="shape" src="d0_s6" to="rhomboidal" />
        <character char_type="range_value" from="40" from_unit="um" name="some_measurement" src="d0_s6" to="60" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="4" upper_restricted="false" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>[Capsule cylindric, 2–4 mm; hypophysis well differentiated, somewhat expanded and rugose. Spores 17–25 µm].</text>
      <biological_entity id="o5774" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks, especially Quercus, rock, soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Fla., N.Mex., N.C., Tex.; Mexico; Central America; South America; Asia (India, Java); Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Java)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">systylium</other_name>
  <discussion>The shiny pale yellow-green to pink-tinged plants of Leptostomopsis systylia are found primarily on tree trunks. This pantropical to subtropical species is much more common and widespread than L. nivea, and the two can be readily distinguished by laminal cell differences.</discussion>
  
</bio:treatment>