<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">philonotis</taxon_name>
    <taxon_name authority="(Schwagrichen) Bridel" date="1827" rank="species">uncinata</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 22. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus philonotis;species uncinata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250062082</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bartramia</taxon_name>
    <taxon_name authority="Schwägrichen" date="unknown" rank="species">uncinata</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>1(2): 60, plate 57 [bottom]. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bartramia;species uncinata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Philonotis</taxon_name>
    <taxon_name authority="(Hornschuch) Brotherus" date="unknown" rank="species">glaucescens</taxon_name>
    <taxon_hierarchy>genus philonotis;species glaucescens</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in lax to dense tufts, yellowish.</text>
      <biological_entity id="o6392" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6393" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s0" value="lax" value_original="lax" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o6392" id="r878" name="in" negation="false" src="d0_s0" to="o6393" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1.5 (–3) cm, erect to inclined, simple, tomentose proximally.</text>
      <biological_entity id="o6394" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="inclined" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect and straight or curved and homomallous when dry, erect-spreading when moist, triangular-lanceolate, 0.7–1.3 mm;</text>
      <biological_entity id="o6395" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="triangular-lanceolate" value_original="triangular-lanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s2" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane proximally, narrowly revolute distally, bluntly serrulate nearly to base, teeth usually single proximally, paired distally;</text>
      <biological_entity id="o6396" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="narrowly; distally" name="shape_or_vernation" src="d0_s3" value="revolute" value_original="revolute" />
        <character constraint="to base" constraintid="o6397" is_modifier="false" modifier="bluntly" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o6397" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o6398" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; proximally" name="quantity" src="d0_s3" value="single" value_original="single" />
        <character is_modifier="false" modifier="distally" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex acuminate;</text>
      <biological_entity id="o6399" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent to short-excurrent, distal abaxial surface rough;</text>
      <biological_entity id="o6400" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s5" to="short-excurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o6401" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells quadrate to rectangular, less than 5: 1, prorulose at distal ends, prorulae not conspicuous, sometimes obscure, few;</text>
      <biological_entity constraint="laminal" id="o6402" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s6" to="rectangular" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="5" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" modifier="not" name="prominence" notes="" src="d0_s6" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6403" name="end" name_original="ends" src="d0_s6" type="structure" />
      <relation from="o6402" id="r879" name="at" negation="false" src="d0_s6" to="o6403" />
    </statement>
    <statement id="d0_s7">
      <text>basal-cells quadrate to rectangular, wider than distal, 10–65 × 8–12 µm; distal cells narrowly oblong, 20–50 × 5–8 µm. Specialized asexual reproduction by brood branches in distal leaf-axils.</text>
      <biological_entity constraint="laminal" id="o6404" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="rectangular" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o6405" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="rectangular" />
        <character constraint="than distal cells" constraintid="o6406" is_modifier="false" name="width" src="d0_s7" value="wider" value_original="wider" />
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s7" to="65" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6406" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o6408" name="brood" name_original="brood" src="d0_s7" type="structure" />
      <biological_entity id="o6409" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o6410" name="leaf-axil" name_original="leaf-axils" src="d0_s7" type="structure" />
      <relation from="o6408" id="r880" name="in" negation="false" src="d0_s7" to="o6410" />
      <relation from="o6409" id="r881" name="in" negation="false" src="d0_s7" to="o6410" />
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="distal" id="o6407" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="20" from_unit="um" name="length" src="d0_s7" to="50" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s7" to="8" to_unit="um" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character constraint="by brood, branches" constraintid="o6408, o6409" is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonia gemmiform.</text>
      <biological_entity id="o6411" name="perigonium" name_original="perigonia" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 1.3–3 cm, straight.</text>
      <biological_entity id="o6412" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule 1.5–2 mm.</text>
      <biological_entity id="o6413" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores subspheric to reniform, 23–26 µm.</text>
      <biological_entity id="o6414" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character char_type="range_value" from="subspheric" name="shape" src="d0_s12" to="reniform" />
        <character char_type="range_value" from="23" from_unit="um" name="some_measurement" src="d0_s12" to="26" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Feb–Mar.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Mar" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, soil, open habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open habitats" modifier="rock soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations (0-30 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
        <character name="elevation" char_type="range_value" to="30" to_unit="m" from="0" from_unit="m" constraint="low elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Ky., La., Miss., S.C., Tex.; Mexico; West Indies; Central America; n, c South America; Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c South America" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Philonotis uncinata is restricted in the flora area largely to states bordering the Gulf of Mexico. The species is recognized by its diminutive habit, percurrent to short-excurrent costa, doubly toothed leaf margin and laminal cells prorulose at distal ends. Philonotis glaucescens, treated as a synonym here, has been recognized elsewhere as P. uncinata var. glaucescens (Hornschuch) Florschütz. The features by which P. glaucescens is recognized (whether as a variety or as a species) include straight leaves and a percurrent costa; these seem to fall within the variation displayed by P. uncinata, the oldest name available for this group of related forms.</discussion>
  
</bio:treatment>