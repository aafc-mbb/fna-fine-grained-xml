<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">schlotheimia</taxon_name>
    <taxon_name authority="E. B. Bartram" date="1932" rank="species">lancifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>35: 9, plate 3. 1932</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus schlotheimia;species lancifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250062002</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants bright green, reddish-brown, or dark-brown.</text>
      <biological_entity id="o10363" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="dark-brown" value_original="dark-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1.6 cm.</text>
      <biological_entity id="o10364" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves oblong-lanceolate to narrowly lanceolate, not rugose, 1.5–2.3 mm;</text>
      <biological_entity id="o10365" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="narrowly lanceolate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s2" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins sometimes serrulate at apex;</text>
      <biological_entity id="o10366" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="at apex" constraintid="o10367" is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o10367" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex gradually acute or acuminate;</text>
      <biological_entity id="o10368" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent;</text>
      <biological_entity id="o10369" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal laminal cells irregularly quadrate to rounded-elliptic, 6–8 µm. Seta 3–5 mm.</text>
      <biological_entity constraint="distal laminal" id="o10370" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="irregularly quadrate" name="shape" src="d0_s6" to="rounded-elliptic" />
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s6" to="8" to_unit="um" />
      </biological_entity>
      <biological_entity id="o10371" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule 1.4–2 mm.</text>
      <biological_entity id="o10372" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyptra smooth.</text>
      <biological_entity id="o10373" name="calyptra" name_original="calyptra" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Logs, trunks of trees to 8 m, undisturbed hemlock hardwood zone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="trunks" modifier="logs" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="to 8 m" />
        <character name="habitat" value="undisturbed hemlock hardwood zone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Schlotheimia lancifolia is extremely rare and endemic to the southern Blue Ridge Escarpment. It differs from S. rugifolia in having non-rugose, gradually acute leaves.</discussion>
  
</bio:treatment>