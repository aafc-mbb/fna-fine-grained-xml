<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">131</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence &amp; H. P. Ramsay" date="unknown" rank="genus">gemmabryum</taxon_name>
    <taxon_name authority="(Podpera) J. R. Spence" date="2009" rank="section">CAESPITIBRYUM</taxon_name>
    <taxon_name authority="(Bruch ex Bridel) J. R. Spence" date="2009" rank="species">badium</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>91: 497. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus gemmabryum;section caespitibryum;species badium;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099113</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="(Bruch ex Bridel) Schimper" date="unknown" rank="species">caespiticium</taxon_name>
    <taxon_name authority="Bruch ex Bridel" date="unknown" rank="variety">badium</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>1: 850. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species caespiticium;variety badium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="(Bruch ex Bridel) J. R. Spence" date="unknown" rank="species">badium</taxon_name>
    <taxon_hierarchy>genus b.;species badium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ptychostomum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">badium</taxon_name>
    <taxon_hierarchy>genus ptychostomum;species badium</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large.</text>
      <biological_entity id="o8108" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.1–2 (–3) cm.</text>
      <biological_entity id="o8109" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves strongly concave, 0.5–2 (–3) mm;</text>
      <biological_entity id="o8110" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane to strongly revolute, limbidium distinct to weak, of 1 or 2 rows of elongate incrassate cells;</text>
      <biological_entity id="o8111" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s3" to="strongly revolute" />
      </biological_entity>
      <biological_entity id="o8112" name="limbidium" name_original="limbidium" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o8113" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="size" src="d0_s3" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <relation from="o8112" id="r1140" modifier="of 1 or 2rows" name="part_of" negation="false" src="d0_s3" to="o8113" />
    </statement>
    <statement id="d0_s4">
      <text>apex not hyaline with age;</text>
      <biological_entity id="o8114" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character constraint="with age" constraintid="o8115" is_modifier="false" modifier="not" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8115" name="age" name_original="age" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa long-excurrent, awn brown, yellowbrown, or sometimes hyaline, denticulate, more than 1/2 leaf length, often as long as leaf;</text>
      <biological_entity id="o8116" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="long-excurrent" value_original="long-excurrent" />
      </biological_entity>
      <biological_entity id="o8117" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" value="/2" value_original="/2" />
      </biological_entity>
      <biological_entity id="o8118" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o8119" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <relation from="o8118" id="r1141" modifier="often" name="as long as" negation="false" src="d0_s5" to="o8119" />
    </statement>
    <statement id="d0_s6">
      <text>proximal laminal cells abruptly quadrate to short-rectangular, 1–2: 1 away from costa, 2–4: 1 along costa;</text>
      <biological_entity constraint="proximal laminal" id="o8120" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="abruptly quadrate" name="shape" src="d0_s6" to="short-rectangular" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
        <character constraint="away-from costa" constraintid="o8121" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character constraint="along costa" constraintid="o8122" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8121" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o8122" name="costa" name_original="costa" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>medial and distal cells long-hexagonal, 12–18 (–22) µm wide, (3–) 4–5: 1.</text>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction by deciduous brood branchlets in distal leaf-axils, rhizoidal tubers absent.</text>
      <biological_entity constraint="medial and distal" id="o8123" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="long-hexagonal" value_original="long-hexagonal" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="um" name="width" src="d0_s7" to="22" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s7" to="18" to_unit="um" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="5" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o8124" name="brood" name_original="brood" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o8125" name="branchlet" name_original="branchlets" src="d0_s8" type="structure">
        <character is_modifier="true" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8126" name="leaf-axil" name_original="leaf-axils" src="d0_s8" type="structure" />
      <relation from="o8124" id="r1142" name="in" negation="false" src="d0_s8" to="o8126" />
      <relation from="o8125" id="r1143" name="in" negation="false" src="d0_s8" to="o8126" />
    </statement>
    <statement id="d0_s9">
      <text>[Capsule with endostome segments bright to pale-yellow. Spores 14–18 µm].</text>
      <biological_entity constraint="rhizoidal" id="o8127" name="tuber" name_original="tubers" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry soil, rock, semiarid climates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="climates" modifier="semiarid" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1000-2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1000" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.; w Eurasia (including Middle East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="w Eurasia (including Middle East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gemmabryum badium is similar to G. caespiticium but distinguished by much longer awns, more strongly concave leaves, and absence of rhizoidal tubers. Gemmabryum badium has only recently been recognized from collections in California and Nevada, and is likely to be found elsewhere in North America.</discussion>
  
</bio:treatment>