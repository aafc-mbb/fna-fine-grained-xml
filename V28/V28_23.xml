<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="illustration_page">26</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">splachnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">splachnum</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="species">sphaericum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>55. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnaceae;genus splachnum;species sphaericum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200001366</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Splachnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">ovatum</taxon_name>
    <taxon_hierarchy>genus splachnum;species ovatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">S.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ovatum</taxon_name>
    <taxon_name authority="(Hedwig) Dixon" date="unknown" rank="variety">sphaericum</taxon_name>
    <taxon_hierarchy>genus s.;species ovatum;variety sphaericum</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants light green or yellow-green.</text>
      <biological_entity id="o6006" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–3 cm.</text>
      <biological_entity id="o6007" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves somewhat crowded at stem apices, broadly obovate, 2.5–3.5 mm;</text>
      <biological_entity id="o6008" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at stem apices" constraintid="o6009" is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s2" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s2" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o6009" name="apex" name_original="apices" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins entire or toothed distally, not bordered;</text>
      <biological_entity id="o6010" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="bordered" value_original="bordered" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex abruptly short to long-acuminate;</text>
      <biological_entity id="o6011" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa disappearing in acumen just before apex.</text>
      <biological_entity id="o6012" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character constraint="in acumen" constraintid="o6013" is_modifier="false" name="prominence" src="d0_s5" value="disappearing" value_original="disappearing" />
      </biological_entity>
      <biological_entity id="o6013" name="acumen" name_original="acumen" src="d0_s5" type="structure" />
      <biological_entity id="o6014" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o6013" id="r826" name="before" negation="false" src="d0_s5" to="o6014" />
    </statement>
    <statement id="d0_s6">
      <text>Seta yellow, becoming reddish, 1.5–10 cm, flexuose.</text>
      <biological_entity id="o6015" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s6" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule urn orange-red, 2 mm;</text>
      <biological_entity constraint="capsule" id="o6016" name="urn" name_original="urn" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="orange-red" value_original="orange-red" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hypophysis green when mature, redbrown and wrinkled upon aging and drying, ± spheric to obovoid, slightly wider than urn, weakly rugose to wrinkled when dry;</text>
      <biological_entity id="o6017" name="hypophysis" name_original="hypophysis" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
        <character constraint="upon aging" is_modifier="false" name="relief" src="d0_s8" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" name="condition" src="d0_s8" value="drying" value_original="drying" />
        <character char_type="range_value" from="less spheric" name="shape" src="d0_s8" to="obovoid" />
        <character constraint="than urn" constraintid="o6018" is_modifier="false" name="width" src="d0_s8" value="slightly wider" value_original="slightly wider" />
        <character char_type="range_value" from="weakly rugose" modifier="when dry" name="relief" src="d0_s8" to="wrinkled" />
      </biological_entity>
      <biological_entity id="o6018" name="urn" name_original="urn" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>operculum hemispheric, apiculate;</text>
      <biological_entity id="o6019" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>exostome teeth inserted below mouth, connate in pairs, orangebrown.</text>
      <biological_entity constraint="exostome" id="o6020" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character constraint="in pairs" constraintid="o6022" is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
      <biological_entity id="o6021" name="mouth" name_original="mouth" src="d0_s10" type="structure" />
      <biological_entity id="o6022" name="pair" name_original="pairs" src="d0_s10" type="structure" />
      <relation from="o6020" id="r827" modifier="below" name="inserted" negation="false" src="d0_s10" to="o6021" />
    </statement>
    <statement id="d0_s11">
      <text>Spores spheric, 7–13 µm, yellow-green.</text>
      <biological_entity id="o6023" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s11" to="13" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dung of large boreal herbivores (such as moose), muskeg, boggy habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dung" constraint="of large boreal herbivores" />
        <character name="habitat" value="large boreal herbivores" />
        <character name="habitat" value="moose" />
        <character name="habitat" value="muskeg" />
        <character name="habitat" value="boggy habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Man., Nfld. and Labr., N.W.T., Ont., Que.; Alaska, Colo., Mich., Mont., Wash., Wyo.; n, c Europe; n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Splachnum sphaericum often grow intermixed with S. luteum in western Canada and central Alaska. Unlike in other North American species of Splachnum, the hypophysis is scarcely wider than the capsule and is green upon maturity, only turning purplish as it ages and dries. Immature sporophytes of the other North American species of Splachnum resemble the mature sporophytes of S. sphaericum. The gametophyte of S. sphaericum can be distinguished from the other North American species by its broadly obovate, narrowly acuminate leaves. The hypophysis of S. sphaericum is much more strongly scented than that of S. luteum. Because of the size, shape, and color of the hypophysis, S. sphaericum could be confused with Tetraplodon mnioides, but T. mnioides is usually found growing on carnivore droppings in drier habitats and has oblong-obovate leaves that narrow abruptly.</discussion>
  
</bio:treatment>