<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Dale H. Vitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">366</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">pterigynandraceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">PTERIGYNANDRUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>80, plates 16, 17, 18, figs. 1–5. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pterigynandraceae;genus PTERIGYNANDRUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pter- , winged, gyne, female, and andros, male, presumably alluding to lateral position of gametoecia</other_info_on_name>
    <other_info_on_name type="fna_id">127425</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in pendent to prostrate mats, green to yellow-green.</text>
      <biological_entity id="o19755" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19756" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character char_type="range_value" from="pendent" is_modifier="true" name="orientation" src="d0_s0" to="prostrate" />
      </biological_entity>
      <relation from="o19755" id="r2832" name="in" negation="false" src="d0_s0" to="o19756" />
    </statement>
    <statement id="d0_s1">
      <text>Stems freely and irregularly branched;</text>
      <biological_entity id="o19757" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent or few.</text>
      <biological_entity id="o19758" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves erect, somewhat falcate to homomallous, imbricate, oblongelliptic;</text>
      <biological_entity id="o19759" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="to homomallous" constraintid="o19760" is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblongelliptic" value_original="oblongelliptic" />
      </biological_entity>
      <biological_entity id="o19760" name="homomallou" name_original="homomallous" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>margins narrowly reflexed proximally, erect and serrulate distally;</text>
      <biological_entity id="o19761" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly; proximally" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex obtuse to acute;</text>
      <biological_entity id="o19762" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa double, short, narrow;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells weakly differentiated;</text>
      <biological_entity id="o19763" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o19764" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminal cells oblong-rhombic, prorulose abaxially, walls generally thick.</text>
      <biological_entity constraint="laminal" id="o19765" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-rhombic" value_original="oblong-rhombic" />
      </biological_entity>
      <biological_entity id="o19766" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially; generally" name="width" src="d0_s8" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect, cylindric, symmetric.</text>
      <biological_entity id="o19767" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, West Indies, Central America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  
</bio:treatment>