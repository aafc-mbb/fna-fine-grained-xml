<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">543</other_info_on_meta>
    <other_info_on_meta type="mention_page">534</other_info_on_meta>
    <other_info_on_meta type="illustration_page">541</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">hypnum</taxon_name>
    <taxon_name authority="Holmen &amp; E. Warncke" date="1969" rank="species">jutlandicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Tidsskr.</publication_title>
      <place_in_publication>65: 179. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus hypnum;species jutlandicum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250064859</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cupressiforme</taxon_name>
    <taxon_name authority="Schimper" date="unknown" rank="variety">ericetorum</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>6: 101, plate 595, fig. Γ. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species cupressiforme;variety ericetorum</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, pale green.</text>
      <biological_entity id="o17582" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale green" value_original="pale green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 4–10 cm, yellowish green to pale-brown, creeping, ascending, or erect, strongly complanate-foliate, densely to sparsely pinnate, branches 0.4–0.7+ cm;</text>
      <biological_entity id="o17583" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s1" to="pale-brown" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="strongly complanate-foliate" name="architecture" src="d0_s1" to="densely sparsely pinnate" />
        <character char_type="range_value" from="strongly complanate-foliate" name="architecture" src="d0_s1" to="densely sparsely pinnate" />
      </biological_entity>
      <biological_entity id="o17584" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s1" to="0.7" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand weak;</text>
      <biological_entity id="o17585" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o17586" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia filamentous to lanceolate, occasionally 2-fid.</text>
      <biological_entity id="o17587" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves loosely imbricate, straight to falcate-secund, ovate or oblong-lanceolate, gradually narrowed to apex, 1.7–2.2 (–2.5) × 0.6–0.8 (–0.9) mm;</text>
      <biological_entity id="o17588" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="to apex" constraintid="o17589" is_modifier="false" modifier="gradually" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_length" notes="" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" notes="" src="d0_s4" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_width" notes="" src="d0_s4" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" notes="" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17589" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>base not decurrent, not auriculate;</text>
      <biological_entity id="o17590" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane or recurved basally, usually markedly serrulate distally;</text>
      <biological_entity id="o17591" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="basally" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="usually markedly; distally" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>acumen short;</text>
      <biological_entity id="o17592" name="acumen" name_original="acumen" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa double and short or indistinct;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar region not well defined;</text>
      <biological_entity id="o17593" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o17594" name="region" name_original="region" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not well" name="prominence" src="d0_s9" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal cells somewhat enlarged and somewhat decurrent at extreme marginal cell;</text>
      <biological_entity constraint="proximal" id="o17595" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s10" value="enlarged" value_original="enlarged" />
        <character constraint="at extreme marginal cell" constraintid="o17596" is_modifier="false" modifier="somewhat" name="shape" src="d0_s10" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity constraint="extreme marginal" id="o17596" name="cell" name_original="cell" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>basal laminal cells shorter, broader than medial cells, light yellow or unpigmented, walls often porose;</text>
      <biological_entity constraint="basal laminal" id="o17597" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character constraint="than medial cells" constraintid="o17598" is_modifier="false" name="width" src="d0_s11" value="broader" value_original="broader" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="light yellow" value_original="light yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="unpigmented" value_original="unpigmented" />
      </biological_entity>
      <biological_entity constraint="medial" id="o17598" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity id="o17599" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s11" value="porose" value_original="porose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>medial cells (60–) 70–90 (–100) × 3 µm. Sexual condition dioicous;</text>
      <biological_entity constraint="medial" id="o17600" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_unit="um" name="atypical_length" src="d0_s12" to="70" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s12" to="100" to_unit="um" />
        <character char_type="range_value" from="70" from_unit="um" name="length" src="d0_s12" to="90" to_unit="um" />
        <character name="width" src="d0_s12" unit="um" value="3" value_original="3" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner perichaetial leaves oblong-lanceolate, margins distinctly serrulate in apex, costa indistinct.</text>
      <biological_entity constraint="inner perichaetial" id="o17601" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-lanceolate" value_original="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o17602" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character constraint="in apex" constraintid="o17603" is_modifier="false" modifier="distinctly" name="architecture_or_shape" src="d0_s13" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o17603" name="apex" name_original="apex" src="d0_s13" type="structure" />
      <biological_entity id="o17604" name="costa" name_original="costa" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Sporophytes unknown in North America.</text>
      <biological_entity id="o17605" name="sporophyte" name_original="sporophytes" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr. (Nfld.), N.S.; Europe; Atlantic Islands (Azores).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Azores)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hypnum jutlandicum is largely an amphi-Atlantic species that may occur in Alaska. The pale yellowish green, glossy, pinnate plants are very distinctive; they might be confused with H. imponens, but H. jutlandicum has yellow rather than red-brown stems and golden yellow rather than pale yellow leaves. The pseudoparaphyllia of H. imponens are more broadly foliose than the nearly filiform ones of H. jutlandicum. Distinction from H. cupressiforme is less apparent, but the usually more complanate pale green plants of H. jutlandicum are usually sufficient to separate it. Plants of H. jutlandicum have branches emerging in a horizontal plane; the pseudoparaphyllia are terminated by a 1-seriate tip of several elongate cells; the alar cells are usually strongly excavate and sometimes brown; and the distal laminal cells are subquadrate and 3–5(–7) along the margin.</discussion>
  
</bio:treatment>