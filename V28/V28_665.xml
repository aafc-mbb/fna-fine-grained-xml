<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">427</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">428</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="Ignatov" date="2011" rank="species">jacuticum</taxon_name>
    <place_of_publication>
      <publication_title>Arctoa</publication_title>
      <place_in_publication>19: 19, fig. 7. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species jacuticum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099028</other_info_on_name>
  </taxon_identification>
  <number>21.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender to medium-sized, in loose to moderately dense mats, light yellowish or whitish.</text>
      <biological_entity id="o21175" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="slender" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="light yellowish" value_original="light yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" value_original="whitish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21176" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o21175" id="r3046" name="in" negation="false" src="d0_s0" to="o21176" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 5 cm, creeping, terete-foliate, irregularly or often sparsely pinnate, branches to 5 mm, straight to slightly curved, terete-foliate.</text>
      <biological_entity id="o21177" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly; often sparsely" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o21178" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s1" to="slightly curved" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect, moderately loosely arranged, ovatelanceolate or lanceolate, broadest at 1/7 leaf length, slightly concave, not or weakly plicate, 2–2.4 × 0.6–0.8 mm;</text>
      <biological_entity id="o21179" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="moderately loosely" name="arrangement" src="d0_s2" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="length" src="d0_s2" value="broadest" value_original="broadest" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not; weakly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base indistinctly to distinctly narrowed, narrowly short-decurrent;</text>
      <biological_entity id="o21180" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="indistinctly to distinctly" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or recurved near base, entire;</text>
      <biological_entity id="o21181" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="near base" constraintid="o21182" is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21182" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex gradually acuminate;</text>
      <biological_entity id="o21183" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 60% leaf length, slender, terminal tooth indistinct or absent;</text>
      <biological_entity id="o21184" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o21185" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="0-60%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells subquadrate to short-rectangular, small, 13–30 × 12–16 µm, walls thick, region ± distinctly delimited, of 4 or 5 × 3–5 cells;</text>
      <biological_entity constraint="terminal" id="o21186" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="indistinct" value_original="indistinct" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21187" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character char_type="range_value" from="13" from_unit="um" name="length" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="width" src="d0_s7" to="16" to_unit="um" />
      </biological_entity>
      <biological_entity id="o21188" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o21189" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less distinctly" name="prominence" src="d0_s7" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o21190" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="true" name="quantity" src="d0_s7" unit="×3-" value="5" value_original="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" unit="×3-" />
      </biological_entity>
      <relation from="o21189" id="r3047" name="consist_of" negation="false" src="d0_s7" to="o21190" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear, 70–125 × 5–9 µm; basal-cells 25–45 × 9–13 µm, walls somewhat porose, region in 1–3 rows.</text>
      <biological_entity constraint="laminal" id="o21191" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="70" from_unit="um" name="length" src="d0_s8" to="125" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="9" to_unit="um" />
      </biological_entity>
      <biological_entity id="o21192" name="basal-cell" name_original="basal-cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s8" to="45" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s8" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity id="o21193" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s8" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o21194" name="region" name_original="region" src="d0_s8" type="structure" />
      <biological_entity id="o21195" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <relation from="o21194" id="r3048" name="in" negation="false" src="d0_s8" to="o21195" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves similar, somewhat smaller.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="branch" id="o21196" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta redbrown, 1.5–2 cm, smooth.</text>
      <biological_entity id="o21197" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined to horizontal, elongate, redbrown, curved, 2 mm;</text>
      <biological_entity id="o21198" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s12" to="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character name="some_measurement" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>annulus separating by fragments;</text>
      <biological_entity id="o21199" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character constraint="by fragments" constraintid="o21200" is_modifier="false" name="arrangement" src="d0_s13" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o21200" name="fragment" name_original="fragments" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>operculum conic.</text>
      <biological_entity id="o21201" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spores 12–16 µm.</text>
      <biological_entity id="o21202" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s15" to="16" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock, open and moderately shaded habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="open" />
        <character name="habitat" value="shaded habitats" modifier="moderately" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations (0 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" constraint="low elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; ne Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="ne Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachythecium jacuticum is characterized as rather slender, usually pale whitish plants, with lanceolate leaves, entire margins, narrow laminal cells reaching almost to the leaf base, thick-walled and porose basal cells, and a small group of quadrate alar cells. Perigonia and perichaetia are usually expressed in the severe conditions where this species occurs, and its autoicous sexuality differentiates B. jacuticum from slender forms of B. albicans. Although related to the B. salebrosum group, this species differs by having entire leaves that are usually totally eplicate. Brachythecium turgidum is probably most closely related to B. jacuticum with its autoicous sexual condition, entire leaf margins, and incrassate porose basal cells, but B. turgidum is a larger plant (leaves 2.5–3.5 × 0.9–1.3 mm versus 2–2.4 × 0.6–0.8 mm in B. jacuticum), and has strongly plicate leaves which are conspicuous in almost all collections, except plants from very wet places near melting late snow beds. However, plants from the latter type of habitats are still more robust, and have strongly concave and abruptly acuminate leaves, approaching the habit of B. cirrosum, thus a confusion of B. jacuticum and B. turgidum is unlikely. In addition, B. jacuticum is often rather regularly pinnate in contrast to weakly branched B. turgidum.</discussion>
  
</bio:treatment>