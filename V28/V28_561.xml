<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leskeaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">pseudoleskea</taxon_name>
    <taxon_name authority="(Mitten) Macoun &amp; Kindberg" date="1892" rank="species">radicosa</taxon_name>
    <place_of_publication>
      <publication_title>Cat. Canad. Pl., Musci,</publication_title>
      <place_in_publication>181. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leskeaceae;genus pseudoleskea;species radicosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002045</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">radicosum</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 31. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species radicosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lescuraea</taxon_name>
    <taxon_name authority="(Mitten) Mönke meyer" date="unknown" rank="species">radicosa</taxon_name>
    <taxon_hierarchy>genus lescuraea;species radicosa</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in thick mats, green, yellow-green, or orange-green.</text>
      <biological_entity id="o9488" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="orange-green" value_original="orange-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="orange-green" value_original="orange-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9489" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o9488" id="r1342" name="in" negation="false" src="d0_s0" to="o9489" />
    </statement>
    <statement id="d0_s1">
      <text>Stems with branches robust, julaceous, apices usually curving up;</text>
      <biological_entity id="o9490" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
      <biological_entity id="o9491" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity id="o9492" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s1" value="curving" value_original="curving" />
      </biological_entity>
      <relation from="o9490" id="r1343" name="with" negation="false" src="d0_s1" to="o9491" />
    </statement>
    <statement id="d0_s2">
      <text>central strand present;</text>
      <biological_entity constraint="central" id="o9493" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia many, filamentous to foliose, branched.</text>
      <biological_entity id="o9494" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="many" value_original="many" />
        <character is_modifier="false" name="texture" src="d0_s3" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves appressed to somewhat erect when dry, erect-spreading when moist, glossy or dull, ovate to ovatelanceolate, asymmetric, usually falcate to falcate-secund, 0.6–2 (–2.4) mm;</text>
      <biological_entity id="o9495" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="appressed to somewhat" value_original="appressed to somewhat" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s4" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="dull" value_original="dull" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="ovatelanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="falcate-secund" value_original="falcate-secund" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins narrowly recurved to mid leaf or to near acumen;</text>
      <biological_entity id="o9496" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="mid" id="o9497" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o9498" name="acumen" name_original="acumen" src="d0_s5" type="structure" />
      <relation from="o9497" id="r1344" name="to" negation="false" src="d0_s5" to="o9498" />
    </statement>
    <statement id="d0_s6">
      <text>apex abruptly acute to short or long-acuminate, hairpoint absent;</text>
      <biological_entity id="o9499" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s6" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o9500" name="hairpoint" name_original="hairpoint" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa subpercurrent to percurrent, yellow-green, sometimes sinuate;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells transversely elongate to quadrate, region small to medium-sized;</text>
      <biological_entity id="o9501" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o9502" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="transversely elongate" name="shape" src="d0_s8" to="quadrate" />
      </biological_entity>
      <biological_entity id="o9503" name="region" name_original="region" src="d0_s8" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s8" to="medium-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells homogeneous, short-rhomboidal, elliptic, or fusiform, to 40 µm, 2–3 (–4):1, pellucid to opaque, prorate to near base, lumina larger than 10 µm, walls thin or rarely firm, not pitted;</text>
      <biological_entity constraint="medial laminal" id="o9504" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="homogeneous" value_original="homogeneous" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rhomboidal" value_original="short-rhomboidal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="fusiform" value_original="fusiform" />
        <character char_type="range_value" from="0" from_unit="um" name="some_measurement" src="d0_s9" to="40" to_unit="um" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="3" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="pellucid" name="coloration" src="d0_s9" to="opaque" />
      </biological_entity>
      <biological_entity id="o9505" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o9506" name="lumen" name_original="lumina" src="d0_s9" type="structure">
        <character modifier="larger than" name="some_measurement" src="d0_s9" unit="um" value="10" value_original="10" />
      </biological_entity>
      <biological_entity id="o9507" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="rarely" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o9504" id="r1345" name="prorate to" negation="false" src="d0_s9" to="o9505" />
    </statement>
    <statement id="d0_s10">
      <text>apical cells 2–3: 1;</text>
      <biological_entity constraint="apical" id="o9508" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s10" to="3" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>juxtacostal cells somewhat shorter than more distal cells, walls not pitted.</text>
      <biological_entity constraint="apical" id="o9509" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
      <biological_entity id="o9510" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character constraint="than more distal cells" constraintid="o9511" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="somewhat shorter" value_original="somewhat shorter" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9511" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity id="o9512" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule erect to suberect, symmetric, 1–2 mm;</text>
      <biological_entity id="o9513" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s12" to="suberect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>endostome basal membrane 1/4–1/3 exostome length, segments shorter than exostome, cilia usually present, 1 or 2, sometimes reduced.</text>
      <biological_entity id="o9514" name="endostome" name_original="endostome" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o9515" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s13" to="1/3" />
      </biological_entity>
      <biological_entity id="o9516" name="exostome" name_original="exostome" src="d0_s13" type="structure" />
      <biological_entity id="o9517" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character constraint="than exostome" constraintid="o9518" is_modifier="false" name="length" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9518" name="exostome" name_original="exostome" src="d0_s13" type="structure" />
      <biological_entity id="o9519" name="cilium" name_original="cilia" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="length" src="d0_s13" value="present" value_original="present" />
        <character name="quantity" src="d0_s13" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s13" unit="or" value="2" value_original="2" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores 16–22 µm.</text>
      <biological_entity id="o9520" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s14" to="22" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Pseudoleskea radicosa is a common and variable species found at moderate to high elevations; the capsules mature in summer. Two of the three varieties are quite distinctive. Pseudoleskea radicosa is closely related to P. incurvata but differs in the longer, more homogeneous, thin-walled, and wider laminal cells. Combined ecological, molecular and morphological studies of the varieties are needed to determine their distinctiveness.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants large; stem apices hooked; branches few or absent; leaves 1.6-2.3 mm, strongly falcate-secund, pluriplicate; apices long-acuminate.</description>
      <determination>5c Pseudoleskea radicosa var. denudata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants small to medium-sized; stem apices not distinctly hooked; branches many; leaves usually less than 1.6 mm, straight, falcate, or falcate-secund, 2-plicate; apices predominantly short-acuminate to acute</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants medium-sized; leaves weakly to moderately concave, not incurved, 1.2-1.7 mm; apices usually long-acuminate; medial laminal cells opaque.</description>
      <determination>5a Pseudoleskea radicosa var. radicosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants small; leaves strongly concave, often incurved, 0.8-1.3 mm; apices acute to short-acuminate; medial laminal cells pellucid.</description>
      <determination>5b Pseudoleskea radicosa var. compacta</determination>
    </key_statement>
  </key>
</bio:treatment>