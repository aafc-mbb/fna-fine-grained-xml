<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">361</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">342</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">362</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leskeaceae</taxon_name>
    <taxon_name authority="Kindberg" date="unknown" rank="genus">PSEUDOLESKEELLA</taxon_name>
    <place_of_publication>
      <publication_title>Eur. N. Amer. Bryin.,</publication_title>
      <place_in_publication>47. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leskeaceae;genus PSEUDOLESKEELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudes, false, and genus Leskeella</other_info_on_name>
    <other_info_on_name type="fna_id">127121</other_info_on_name>
  </taxon_identification>
  <number>9.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in thin mats or patches, yellow to red-green, redbrown, green, or olive green.</text>
      <biological_entity id="o1489" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="yellow" name="coloration" notes="" src="d0_s0" to="red-green redbrown green or olive green" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="red-green redbrown green or olive green" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="red-green redbrown green or olive green" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="red-green redbrown green or olive green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1490" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o1491" name="patch" name_original="patches" src="d0_s0" type="structure" />
      <relation from="o1489" id="r194" name="in" negation="false" src="d0_s0" to="o1490" />
      <relation from="o1489" id="r195" name="in" negation="false" src="d0_s0" to="o1491" />
    </statement>
    <statement id="d0_s1">
      <text>Stems irregularly branched, branches appressed to ascending or erect, not clustered;</text>
      <biological_entity id="o1492" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o1493" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s1" to="ascending or erect" />
        <character is_modifier="false" modifier="not" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia absent;</text>
      <biological_entity id="o1494" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rhizoids in clusters arising from base of leaves.</text>
      <biological_entity id="o1495" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure" />
      <biological_entity id="o1496" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="orientation" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o1497" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o1495" id="r196" name="in" negation="false" src="d0_s3" to="o1496" />
      <relation from="o1496" id="r197" name="part_of" negation="false" src="d0_s3" to="o1497" />
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves differentiated.</text>
      <biological_entity id="o1498" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1499" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves catenulate or not, appressed to erect when dry, erect-spreading when moist, ovate to lanceolate, not to sometimes 2-plicate;</text>
      <biological_entity id="o1500" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="catenulate" value_original="catenulate" />
        <character name="arrangement" src="d0_s5" value="not" value_original="not" />
        <character char_type="range_value" from="appressed" modifier="when dry" name="orientation" src="d0_s5" to="erect" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s5" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" modifier="not to sometimes" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="2-plicate" value_original="2-plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane or recurved, entire to serrulate distally;</text>
      <biological_entity id="o1501" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" modifier="distally" name="architecture_or_shape" src="d0_s6" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex abruptly to gradually acuminate, hairpoint absent;</text>
      <biological_entity id="o1502" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly to gradually" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o1503" name="hairpoint" name_original="hairpoint" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa single, double, 2-fid, or rarely ecostate, strong to weak, short to percurrent, not sinuate, sometimes somewhat obscure distally;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar cells not well differentiated, quadrate to short-rectangular;</text>
      <biological_entity id="o1504" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s8" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="strong" modifier="rarely" name="fragility" src="d0_s8" to="weak" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="sometimes somewhat; distally" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o1505" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not well" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s9" to="short-rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells 1–2: 1, walls not or obscurely pitted;</text>
      <biological_entity constraint="proximal laminal" id="o1506" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1507" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>medial cells isodiametric, 2–3: 1, smooth, walls firm to incrassate.</text>
      <biological_entity constraint="medial" id="o1508" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="3" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o1509" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="firm" value_original="firm" />
        <character is_modifier="false" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Branch leaves more ovate, smaller;</text>
      <biological_entity constraint="branch" id="o1510" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="size" src="d0_s12" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apex often especially recurved or falcate;</text>
      <biological_entity id="o1511" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="often especially" name="orientation" src="d0_s13" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s13" value="falcate" value_original="falcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>costa usually weaker;</text>
      <biological_entity id="o1512" name="costa" name_original="costa" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="fragility" src="d0_s14" value="weaker" value_original="weaker" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>medial laminal cells usually shorter.</text>
    </statement>
    <statement id="d0_s16">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="medial laminal" id="o1513" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="development" src="d0_s16" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s16" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s17" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s17" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>perichaetial leaves pale translucent, appressed to rarely recurved, longer, apex more acuminate.</text>
      <biological_entity constraint="perichaetial" id="o1514" name="leaf" name_original="leaves" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="pale translucent" value_original="pale translucent" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s18" to="rarely recurved" />
        <character is_modifier="false" name="length_or_size" src="d0_s18" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1515" name="apex" name_original="apex" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seta 0.1–0.3 cm.</text>
      <biological_entity id="o1516" name="seta" name_original="seta" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s19" to="0.3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Capsule erect or inclined, subcylindric to cylindric, curved-asymmetric or sometimes symmetric;</text>
      <biological_entity id="o1517" name="capsule" name_original="capsule" src="d0_s20" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s20" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s20" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="subcylindric" name="shape" src="d0_s20" to="cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="curved-asymmetric" value_original="curved-asymmetric" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s20" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>annulus present;</text>
      <biological_entity id="o1518" name="annulus" name_original="annulus" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>operculum conic, short-rostrate;</text>
      <biological_entity id="o1519" name="operculum" name_original="operculum" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s22" value="short-rostrate" value_original="short-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>peristome well developed;</text>
      <biological_entity id="o1520" name="peristome" name_original="peristome" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s23" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>exostome teeth lanceolate or linear-lanceolate, finely horizontally striate;</text>
      <biological_entity constraint="exostome" id="o1521" name="tooth" name_original="teeth" src="d0_s24" type="structure">
        <character is_modifier="false" name="shape" src="d0_s24" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s24" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="finely horizontally" name="coloration_or_pubescence_or_relief" src="d0_s24" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>endostome basal membrane moderately high, segments slender, lanceolate to filiform, cilia present, usually well developed.</text>
      <biological_entity id="o1522" name="endostome" name_original="endostome" src="d0_s25" type="structure" />
      <biological_entity constraint="basal" id="o1523" name="membrane" name_original="membrane" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="moderately" name="height" src="d0_s25" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o1524" name="segment" name_original="segments" src="d0_s25" type="structure">
        <character is_modifier="false" name="size" src="d0_s25" value="slender" value_original="slender" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s25" to="filiform" />
      </biological_entity>
      <biological_entity id="o1525" name="cilium" name_original="cilia" src="d0_s25" type="structure">
        <character is_modifier="false" name="presence" src="d0_s25" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s25" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>Spores 8–18 µm, finely papillose.</text>
      <biological_entity id="o1526" name="spore" name_original="spores" src="d0_s26" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s26" to="18" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s26" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 (4 in the flora).</discussion>
  <discussion>Pseudoleskeella is found mostly in cool-temperate, montane, arctic-alpine, and boreal-temperate regions; the plants are small, slender, and appressed, with many branches, smooth laminal cells, no paraphyllia, straight to flexuose setae, and a relatively weak costa, often 2-fid. The older stoloniferous leaves are distant, often denuded, scalelike, and sometimes hyaline; occasionally some distal laminal cells become weakly prorate. However, the species seem to be quite different from each other morphologically beyond these few shared traits. Molecular studies have shown that P. serpentinensis is not related to the other species; rather it appears to be closest to Heterocladium (Pterigynandraceae) (M. S. Ignatov et al. 2007). Following P. S. Wilson and D. H. Norris (1989), P. catenulata (Bridel ex Schrader) Kindberg and P. papillosa (Lindberg) Kindberg are excluded from North America.</discussion>
  <references>
    <reference>Lewinsky, J. 1974. The genera Leskeella and Pseudoleskeella in Greenland. Bryologist 77: 601–611.</reference>
    <reference>Wilson, P. S. and D. H. Norris. 1989. Pseudoleskeella in North America and Europe. Bryologist 92: 387–396.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves ± catenulate when dry; costae usually double, not reaching mid leaf; stems tightly appressed to substrate.</description>
      <determination>4 Pseudoleskeella tectorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves not catenulate when dry; costae usually single, rarely 2-fid, usually to mid leaf or sometimes percurrent; stems not tightly appressed to substrate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants green to yellow-green; leaves distinctly plicate; bases flaring, cordate; margins recurved to mid leaf or beyond, distinctly serrulate distally.</description>
      <determination>1 Pseudoleskeella arizonae</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants green to red; leaves not or weakly plicate; bases not flaring or cordate; margins plane throughout or recurved proximally, ± entire or finely serrulate distally</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaves broadly lanceolate to ovate-lanceolate, not homomallous, gradually narrowed to apex; apices not falcate; costae usually beyond mid leaf, rarely 2-fid; plants green, red-green, or orange-green; branch leaf laminal cell walls thin.</description>
      <determination>2 Pseudoleskeella rupestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaves ovate to lanceolate, homomallous, abruptly narrowed to apex; apices sometimes falcate; costae short and 2-fid to single and reaching mid leaf or sometimes beyond; plants red, red-brown, or green; branch leaf laminal cell walls incrassate.</description>
      <determination>3 Pseudoleskeella serpentinensis</determination>
    </key_statement>
  </key>
</bio:treatment>