<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">23</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">650</other_info_on_meta>
    <other_info_on_meta type="mention_page">663</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">splachnaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">SPLACHNUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>51, plate 8. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnaceae;genus SPLACHNUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek splachnon, tree moss</other_info_on_name>
    <other_info_on_name type="fna_id">131059</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in soft, loose tufts, light to dark green or yellow-green.</text>
      <biological_entity id="o4756" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light" name="coloration" notes="" src="d0_s0" to="dark green or yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4757" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o4756" id="r635" name="in" negation="false" src="d0_s0" to="o4757" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.2–3.5 cm;</text>
    </statement>
    <statement id="d0_s2">
      <text>radiculose.</text>
      <biological_entity id="o4758" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s1" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves obovate to long-lanceolate;</text>
      <biological_entity id="o4759" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s3" to="long-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire or serrate distally;</text>
      <biological_entity id="o4760" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex obtuse, acute, or acuminate;</text>
      <biological_entity id="o4761" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa ending in or before apex;</text>
      <biological_entity id="o4762" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o4763" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o4762" id="r636" name="ending in" negation="false" src="d0_s6" to="o4763" />
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells rectangular;</text>
      <biological_entity constraint="proximal laminal" id="o4764" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal cells oblong-hexagonal.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous (autoicous in S. pensylvanicum).</text>
      <biological_entity constraint="distal" id="o4765" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-hexagonal" value_original="oblong-hexagonal" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 0.2–15.5 cm, flexuose or twisted.</text>
      <biological_entity id="o4766" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s10" to="15.5" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s10" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule not cleistocarpous, red, orange, or yellowbrown, or orange-red, cylindric;</text>
      <biological_entity id="o4767" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="dehiscence" src="d0_s11" value="cleistocarpous" value_original="cleistocarpous" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypophysis with color greatly differentiated from urn, globose to turbinate, sometimes umbrelliform, elongate, usually much larger, wider than urn;</text>
      <biological_entity id="o4768" name="hypophysis" name_original="hypophysis" src="d0_s12" type="structure">
        <character constraint="from urn" constraintid="o4769" is_modifier="false" modifier="greatly" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="globose" name="shape" notes="" src="d0_s12" to="turbinate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s12" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="usually much" name="size" src="d0_s12" value="larger" value_original="larger" />
        <character constraint="than urn" constraintid="o4770" is_modifier="false" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o4769" name="urn" name_original="urn" src="d0_s12" type="structure" />
      <biological_entity id="o4770" name="urn" name_original="urn" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>annulus absent;</text>
      <biological_entity id="o4771" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic to hemispheric, blunt to apiculate;</text>
      <biological_entity id="o4772" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s14" to="hemispheric blunt" />
        <character char_type="range_value" from="conic" name="shape" src="d0_s14" to="hemispheric blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome double;</text>
      <biological_entity id="o4773" name="peristome" name_original="peristome" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth 16, sometimes connate, reflexed when dry, inflexed when moist, of 3 layers of cells.</text>
      <biological_entity constraint="exostome" id="o4774" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character is_modifier="false" modifier="sometimes" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s16" value="inflexed" value_original="inflexed" />
      </biological_entity>
      <biological_entity id="o4775" name="layer" name_original="layers" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o4776" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <relation from="o4774" id="r637" name="consist_of" negation="false" src="d0_s16" to="o4775" />
      <relation from="o4775" id="r638" name="part_of" negation="false" src="d0_s16" to="o4776" />
    </statement>
    <statement id="d0_s17">
      <text>Calyptra conic-mitrate, small, reaching hypophysis, not constricted beyond base.</text>
      <biological_entity id="o4777" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="conic-mitrate" value_original="conic-mitrate" />
        <character is_modifier="false" name="size" src="d0_s17" value="small" value_original="small" />
        <character constraint="beyond base" constraintid="o4779" is_modifier="false" modifier="not" name="size" src="d0_s17" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o4778" name="hypophysis" name_original="hypophysis" src="d0_s17" type="structure" />
      <biological_entity id="o4779" name="base" name_original="base" src="d0_s17" type="structure" />
      <relation from="o4777" id="r639" name="reaching" negation="false" src="d0_s17" to="o4778" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 7–13 µm, smooth.</text>
      <biological_entity id="o4780" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s18" to="13" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, except Antarctica; tropical to subarctic regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="except Antarctica" establishment_means="native" />
        <character name="distribution" value="tropical to subarctic regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 11 (6 in the flora).</discussion>
  <discussion>Plants of Splachnum are often shiny, entomophilous and coprophilous, and all circumboreal species appear to be restricted to growing on herbivore dung. The genus is characterized by its chambered exostome teeth as well as several species having greatly enlarged, colorful, differentiated hypophyses. The spores of this genus are sticky; the smooth setae usually continue growing after the spores mature. The stems are loosely foliate and simple or 2-fid. The stem leaves are erect or erect-spreading, usually more crowded toward stem apices, more or less contorted when dry, and usually spreading when moist. The perigonia are cupulate and large; the perichaetia are terminal on the stems or branches with leaves fewer and smaller. The exothecial cells are quadrate-hexagonal and rectangular at the capsule base, and transversely elongate in distal rows. The columella is short-exserted.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Hypophysis ± globose, ovoid, or turbinate, much wider than urn</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Hypophysis umbrelliform, or if globose or narrowly pyriform, not much wider than urn</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Hypophysis yellow or pink, rarely reddish; leaves narrowly oblong-obovate to long- lanceolate; margins spinose-dentate distally; apices slender-acuminate.</description>
      <determination>1 Splachnum ampullaceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Hypophysis red-purple when moist; leaves ovate to obovate; margins entire; apices blunt, rounded, or obtuse.</description>
      <determination>2 Splachnum vasculosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypophysis umbrelliform, much wider than urn</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Hypophysis spheric, narrowly pyriform, or obovoid, not much wider than urn</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypophysis discoid-umbrelliform, yellow; leaf margins serrate to subentire distally.</description>
      <determination>3 Splachnum luteum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Hypophysis convex-umbrelliform, bright magenta red; leaf margins coarsely serrate distally, toothed nearly to base.</description>
      <determination>4 Splachnum rubrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Setae longer than 1 cm; hypophysis green, red-brown when dry; leaves broadly obovate; apices abruptly short- to long-acuminate.</description>
      <determination>5 Splachnum sphaericum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Setae shorter than 1 cm; hypophysis greenish, dark red or purplish distally; leaves long- lanceolate; apices slenderly long-acuminate.</description>
      <determination>6 Splachnum pensylvanicum</determination>
    </key_statement>
  </key>
</bio:treatment>