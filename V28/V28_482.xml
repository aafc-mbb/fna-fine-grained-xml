<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="illustration_page">310</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Loeske" date="unknown" rank="genus">sanionia</taxon_name>
    <taxon_name authority="(Lindberg) Loeske" date="1907" rank="species">orthothecioides</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>46: 309. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus sanionia;species orthothecioides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099361</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">uncinatum</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="subspecies">orthothecioides</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>23: 540. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species uncinatum;subspecies orthothecioides</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large.</text>
      <biological_entity id="o9228" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched, sparsely and irregularly branched, or rarely ± pinnate.</text>
      <biological_entity id="o9229" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparsely; irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely more or less" name="architecture" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely more or less" name="architecture" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves falcate or strongly falcate, more rarely straight or almost so, strongly or sometimes slightly plicate, 0.6–1.5 mm wide;</text>
      <biological_entity id="o9230" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character name="course" src="d0_s2" value="almost" value_original="almost" />
        <character is_modifier="false" modifier="strongly; sometimes slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base ovate-triangular to broadly ovate;</text>
      <biological_entity id="o9231" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate-triangular" name="shape" src="d0_s3" to="broadly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins partly or entirely recurved (most distinct in ventral leaves), sometimes incurved proximally, finely denticulate or almost entire distally;</text>
      <biological_entity id="o9232" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="entirely" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="sometimes; proximally" name="orientation" src="d0_s4" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="almost; almost; distally" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex long or very long-acuminate;</text>
      <biological_entity id="o9233" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" modifier="very" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa in bottom of deep narrow-angled fold;</text>
      <biological_entity id="o9235" name="bottom" name_original="bottom" src="d0_s6" type="structure" />
      <biological_entity id="o9236" name="fold" name_original="fold" src="d0_s6" type="structure">
        <character is_modifier="true" name="depth" src="d0_s6" value="deep" value_original="deep" />
        <character is_modifier="true" name="shape" src="d0_s6" value="narrow-angled" value_original="narrow-angled" />
      </biological_entity>
      <relation from="o9234" id="r1296" name="in" negation="false" src="d0_s6" to="o9235" />
      <relation from="o9235" id="r1297" name="bottom of" negation="false" src="d0_s6" to="o9236" />
    </statement>
    <statement id="d0_s7">
      <text>alar region transversely triangular, transition to supra-alar cells sudden, supra-alar cells quadrate or short-rectangular, chlorophyllose, walls slightly or strongly incrassate, porose, region equal in size to or much larger than alar region;</text>
      <biological_entity id="o9234" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o9237" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character constraint="to cells" constraintid="o9238" is_modifier="false" name="duration" src="d0_s7" value="transition" value_original="transition" />
      </biological_entity>
      <biological_entity id="o9238" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o9239" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o9240" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s7" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o9241" name="region" name_original="region" src="d0_s7" type="structure">
        <character constraint="in " constraintid="o9242" is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o9242" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="much" name="size" src="d0_s7" value="larger" value_original="larger" />
        <character is_modifier="true" name="character" src="d0_s7" value="size" value_original="size" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apical laminal cells smooth.</text>
      <biological_entity constraint="apical laminal" id="o9243" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Perichaetia with inner leaves gradually narrowed to apex, margins finely denticulate to denticulate distally, apex long-acuminate.</text>
      <biological_entity id="o9244" name="perichaetium" name_original="perichaetia" src="d0_s9" type="structure" />
      <biological_entity constraint="inner" id="o9245" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character constraint="to apex" constraintid="o9246" is_modifier="false" modifier="gradually" name="shape" src="d0_s9" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o9246" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o9247" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character char_type="range_value" from="finely denticulate" modifier="distally" name="shape" src="d0_s9" to="denticulate" />
      </biological_entity>
      <biological_entity id="o9248" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <relation from="o9244" id="r1298" name="with" negation="false" src="d0_s9" to="o9245" />
    </statement>
    <statement id="d0_s10">
      <text>Capsule erect or inclined;</text>
      <biological_entity id="o9249" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="inclined" value_original="inclined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exothecial cells isodiametric or transversely rectangular, in 3–7 rows;</text>
      <biological_entity constraint="exothecial" id="o9250" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o9251" name="row" name_original="rows" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <relation from="o9250" id="r1299" name="in" negation="false" src="d0_s11" to="o9251" />
    </statement>
    <statement id="d0_s12">
      <text>exostome specialized, teeth long, narrow, border not widened at transitional zone in pattern of external tooth;</text>
      <biological_entity id="o9252" name="exostome" name_original="exostome" src="d0_s12" type="structure">
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o9253" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o9254" name="border" name_original="border" src="d0_s12" type="structure">
        <character constraint="at zone" constraintid="o9255" is_modifier="false" modifier="not" name="width" src="d0_s12" value="widened" value_original="widened" />
      </biological_entity>
      <biological_entity id="o9255" name="zone" name_original="zone" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="transitional" value_original="transitional" />
      </biological_entity>
      <biological_entity id="o9256" name="pattern" name_original="pattern" src="d0_s12" type="structure" />
      <biological_entity constraint="external" id="o9257" name="tooth" name_original="tooth" src="d0_s12" type="structure" />
      <relation from="o9255" id="r1300" name="in" negation="false" src="d0_s12" to="o9256" />
      <relation from="o9256" id="r1301" name="part_of" negation="false" src="d0_s12" to="o9257" />
    </statement>
    <statement id="d0_s13">
      <text>endostome specialized, in recently dehisced capsules strongly yellow, basal membrane constituting 20–30% endostome height, processes perforated only along midline, cilia rudimentary.</text>
      <biological_entity id="o9258" name="endostome" name_original="endostome" src="d0_s13" type="structure">
        <character is_modifier="false" name="development" src="d0_s13" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o9259" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="basal" id="o9260" name="membrane" name_original="membrane" src="d0_s13" type="structure" />
      <biological_entity id="o9261" name="endostome" name_original="endostome" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s13" to="30%" />
      </biological_entity>
      <biological_entity id="o9262" name="process" name_original="processes" src="d0_s13" type="structure">
        <character constraint="along midline" constraintid="o9263" is_modifier="false" name="height" src="d0_s13" value="perforated" value_original="perforated" />
      </biological_entity>
      <biological_entity id="o9263" name="midline" name_original="midline" src="d0_s13" type="structure" />
      <biological_entity id="o9264" name="cilium" name_original="cilia" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <relation from="o9258" id="r1302" name="in" negation="false" src="d0_s13" to="o9259" />
      <relation from="o9260" id="r1303" name="constituting" negation="false" src="d0_s13" to="o9261" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal, inland, open habitats, rock crevices, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open habitats" modifier="coastal inland" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="coastal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., N.B., Nfld. and Labr. (Nfld.), Nunavut, Que., Yukon; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sanionia orthothecioides is normally considerably larger than the other three members of the genus. The species is easily separated from S. uncinata by its reduced and strongly yellow endostome. From S. uncinata and S. symmetrica it differs by its many (3–7 as compared with 1–4) rows of isodiametric or transversely rectangular cells below the capsule mouth, and by the structure of its alar and supra-alar cells. The leaf margin is usually recurved in S. orthothecioides, more rarely so in the other three species, and the costa is usually situated in the bottom of a deep fold in the proximal portion of the leaves in the first species, which is rare in the other three.</discussion>
  
</bio:treatment>