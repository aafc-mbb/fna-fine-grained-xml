<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">bartramia</taxon_name>
    <taxon_name authority="Bridel" date="1827" rank="species">brevifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 737. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus bartramia;species brevifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250062066</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense tufts, glaucous or brownish green.</text>
      <biological_entity id="o13068" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish green" value_original="brownish green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13069" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o13068" id="r1840" name="in" negation="false" src="d0_s0" to="o13069" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–3 cm.</text>
      <biological_entity id="o13070" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect to erect-spreading and somewhat flexuose when dry, spreading when moist, narrowly lanceolate, 2–5 mm;</text>
      <biological_entity id="o13071" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="erect-spreading" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base sheathing, shoulders well developed, firm, not eroded;</text>
      <biological_entity id="o13072" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o13073" name="shoulder" name_original="shoulders" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="architecture_or_relief" src="d0_s3" value="eroded" value_original="eroded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins strongly revolute, serrulate distally, teeth paired;</text>
      <biological_entity id="o13074" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o13075" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="paired" value_original="paired" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex subulate, usually intact;</text>
      <biological_entity id="o13076" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character is_modifier="false" modifier="usually" name="condition" src="d0_s5" value="intact" value_original="intact" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa excurrent, prominent in distal limb, distal abaxial surface rough;</text>
      <biological_entity id="o13077" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character constraint="in distal limb" constraintid="o13078" is_modifier="false" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13078" name="limb" name_original="limb" src="d0_s6" type="structure" />
      <biological_entity constraint="distal abaxial" id="o13079" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal laminal cell-walls thick toward costa, thin toward margins;</text>
      <biological_entity constraint="basal laminal" id="o13080" name="cell-wall" name_original="cell-walls" src="d0_s7" type="structure">
        <character constraint="toward costa" constraintid="o13081" is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o13081" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character constraint="toward margins" constraintid="o13082" is_modifier="false" name="width" notes="" src="d0_s7" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o13082" name="margin" name_original="margins" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>distal cells 8–25 × 4–8 µm, prorulae high.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition synoicous or dioicous.</text>
      <biological_entity constraint="distal" id="o13083" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="length" src="d0_s8" to="25" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s8" to="8" to_unit="um" />
        <character is_modifier="false" name="height" src="d0_s8" value="high" value_original="high" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 0.6–1.6 cm, straight to slightly arcuate.</text>
      <biological_entity id="o13084" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s10" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s10" to="slightly arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule inclined, globose to ovoid, asymmetric, 1.5–2.5 mm;</text>
      <biological_entity id="o13085" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="globose" name="shape" src="d0_s11" to="ovoid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="asymmetric" value_original="asymmetric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum short-convex;</text>
      <biological_entity id="o13086" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="short-convex" value_original="short-convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>peristome single or double;</text>
      <biological_entity id="o13087" name="peristome" name_original="peristome" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character name="quantity" src="d0_s13" value="double" value_original="double" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>exostome teeth 200–350 µm, finely papillose proximally, vertically striate distally;</text>
      <biological_entity constraint="exostome" id="o13088" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character char_type="range_value" from="200" from_unit="um" name="some_measurement" src="d0_s14" to="350" to_unit="um" />
        <character is_modifier="false" modifier="finely; proximally" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="vertically; distally" name="coloration_or_pubescence_or_relief" src="d0_s14" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>endostome absent or, if present, basal membrane rudimentary, segments sometimes present, less than 1/3 length of teeth, finely papillose, cilia absent.</text>
      <biological_entity id="o13089" name="endostome" name_original="endostome" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s15" value="," value_original="," />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o13090" name="membrane" name_original="membrane" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o13091" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character char_type="range_value" from="0 length of teeth" name="length" src="d0_s15" to="1/3 length of teeth" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o13092" name="cilium" name_original="cilia" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 25–35 (–40) µm.</text>
      <biological_entity id="o13093" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s16" to="40" to_unit="um" />
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s16" to="35" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Nov" from="Nov" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices, canyon ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="canyon ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (1800-4000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="1800" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico; West Indies (Dominican Republic); Central America (Costa Rica, Guatemala); South America (Bolivia, Colombia, Ecuador, Peru, Venezuela).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="Central America (Costa Rica)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="South America (Ecuador)" establishment_means="native" />
        <character name="distribution" value="South America (Peru)" establishment_means="native" />
        <character name="distribution" value="South America (Venezuela)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bartramia brevifolia is recognized by its differentiated leaf base with thick-walled cells toward the costa and thin-walled cells toward the margins. Material collected in Arizona by Bartram and identified as either B. glauca Lorentz or B. microstoma Mitten is B. brevifolia. The type of B. microstoma is not included with the Mitten herbarium at NY, but several collections of this species from Latin America have been annotated by S. Fransén as B. brevifolia. Fransén (1995) reduced B. glauca to synonymy under B. potosica.</discussion>
  
</bio:treatment>