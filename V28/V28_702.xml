<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Michael S. Ignatov</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="genus">Palamocladium</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>82: 465. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus Palamocladium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek palame, palm, and clados, branch, alluding to spreading branches, although inappropriately</other_info_on_name>
    <other_info_on_name type="fna_id">123682</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, in moderately loose tufts, deep green to brownish or olive green.</text>
      <biological_entity id="o9026" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="deep" value_original="deep" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="brownish or olive green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9027" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="moderately" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o9026" id="r1271" name="in" negation="false" src="d0_s0" to="o9027" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping to ascending, moderately to very densely terete-foliate, irregularly pinnate, branches terete-foliate;</text>
      <biological_entity id="o9028" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="creeping" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
      <biological_entity id="o9029" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="very densely" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="true" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
      <relation from="o9028" id="r1272" modifier="moderately" name="to" negation="false" src="d0_s1" to="o9029" />
    </statement>
    <statement id="d0_s2">
      <text>central strand weak;</text>
      <biological_entity constraint="central" id="o9030" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia acute;</text>
      <biological_entity id="o9031" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary hairs of 3–6 cells.</text>
      <biological_entity constraint="axillary" id="o9032" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o9033" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <relation from="o9032" id="r1273" name="consist_of" negation="false" src="d0_s4" to="o9033" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves erect, imbricate, lanceolate-triangular, ovatelanceolate, or lanceolate, weakly concave, deeply plicate, or not plicate in slender plants;</text>
      <biological_entity id="o9034" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate-triangular" value_original="lanceolate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="deeply" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="deeply" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
        <character constraint="in slender plants" constraintid="o9035" is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="slender" id="o9035" name="plant" name_original="plants" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>base slightly auriculate proximally;</text>
      <biological_entity id="o9036" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly; proximally" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins coarsely serrate;</text>
      <biological_entity id="o9037" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex gradually tapered, narrowly acute, or somewhat acuminate;</text>
      <biological_entity id="o9038" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa subpercurrent, slender, terminal spine small, inconspicuous;</text>
      <biological_entity id="o9039" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>alar cells subquadrate, small, region of 10–15 × 5–8 cells;</text>
      <biological_entity constraint="terminal" id="o9040" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o9041" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subquadrate" value_original="subquadrate" />
        <character is_modifier="false" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o9042" name="region" name_original="region" src="d0_s10" type="structure" />
      <biological_entity id="o9043" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s10" to="15" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
      <relation from="o9042" id="r1274" name="consist_of" negation="false" src="d0_s10" to="o9043" />
    </statement>
    <statement id="d0_s11">
      <text>laminal cells elongate, relatively short, walls moderately thick;</text>
      <biological_entity constraint="laminal" id="o9044" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o9045" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s11" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>basal juxtacostal cells short-ovate.</text>
      <biological_entity constraint="basal" id="o9046" name="cell" name_original="cells" src="d0_s12" type="structure" />
      <biological_entity id="o9047" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="short-ovate" value_original="short-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Branch leaves similar, somewhat narrower, somewhat shorter.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous or phyllodioicous;</text>
      <biological_entity constraint="branch" id="o9048" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="somewhat" name="width" src="d0_s13" value="narrower" value_original="narrower" />
        <character is_modifier="false" modifier="somewhat" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perichaetial leaf apex reflexed.</text>
    </statement>
    <statement id="d0_s16">
      <text>[Seta redbrown, smooth. Capsule erect, reddish-brown, ovoid-cylindric; annulus separating by fragments; operculum long, gradually tapered. Calyptra naked. Spores 9–20 µm].</text>
      <biological_entity constraint="leaf" id="o9049" name="apex" name_original="apex" src="d0_s15" type="structure" constraint_original="perichaetial leaf">
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sc, se United States, Mexico, Central America, South America, sw, e Asia, Africa, Indian Ocean Islands (Madagascar), w Pacific Islands; tropical to south temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sc" establishment_means="native" />
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="sw" establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands (Madagascar)" establishment_means="native" />
        <character name="distribution" value="w Pacific Islands" establishment_means="native" />
        <character name="distribution" value="tropical to south temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3–6 (1 in the flora).</discussion>
  <discussion>Palamocladium was included in Homalothecium by H. Robinson (1962), but this decision was not accepted by H. A. Crum and L. E. Anderson (1981) or H. Hofmann (1997). According to M. S. Ignatov and S. Huttunen (2002), these genera belong in different subfamilies. Palamocladium can be recognized by narrow, rigid, and, when well developed, plicate leaves. Although these characters are the same as for Homalothecium, Palamocladium appears superficially different, as its leaves are erect to somewhat spreading when dry whereas more appressed in Homalothecium. The plants are olive or brownish and rather oily; in Homalothecium they are rich golden and rather silky. Microscopically, the coarse serration in the acumen is helpful in species recognition, as well as the extensive groups of small alar cells forming opaque areas.</discussion>
  <references>
    <reference>Hofmann, H. 1997b. A monograph of the genus Palamocladium (Brachytheciaceae, Musci). Lindbergia 22: 3–20.</reference>
  </references>
  
</bio:treatment>