<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce Allen</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">489</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">FONTINALACEAE</taxon_name>
    <taxon_hierarchy>family FONTINALACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10345</other_info_on_name>
  </taxon_identification>
  <number>68.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in loose or dense masses.</text>
      <biological_entity id="o19468" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19469" name="mass" name_original="masses" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o19468" id="r2786" name="in" negation="false" src="d0_s0" to="o19469" />
    </statement>
    <statement id="d0_s1">
      <text>Primary-stems creeping.</text>
      <biological_entity id="o19470" name="primary-stem" name_original="primary-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Secondary stems prostrate, pendent, or trailing, freely and irregularly branched;</text>
      <biological_entity constraint="secondary" id="o19471" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="trailing" value_original="trailing" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="trailing" value_original="trailing" />
        <character is_modifier="false" modifier="freely; irregularly" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>central strand absent;</text>
      <biological_entity constraint="central" id="o19472" name="strand" name_original="strand" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids on stems and at base of secondary stems, from initials abaxial to leaf insertions and around secondary stem primordia;</text>
      <biological_entity id="o19473" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o19474" name="insertion" name_original="insertions" src="d0_s4" type="structure">
        <character is_modifier="true" name="position" src="d0_s4" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity id="o19475" name="primordium" name_original="primordia" src="d0_s4" type="structure" constraint="stem" constraint_original="secondary around stem" />
      <relation from="o19473" id="r2787" name="on stems and at base of secondary stems , from initials" negation="false" src="d0_s4" to="o19474" />
      <relation from="o19473" id="r2788" name="on stems and at base of secondary stems , from initials" negation="false" src="d0_s4" to="o19475" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hairs elongate.</text>
      <biological_entity constraint="axillary" id="o19476" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves 3-ranked, keeled, conduplicate, concave, or plane;</text>
      <biological_entity id="o19477" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="3-ranked" value_original="3-ranked" />
        <character is_modifier="false" name="shape" src="d0_s6" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s6" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base decurrent;</text>
      <biological_entity id="o19478" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins entire or serrulate;</text>
      <biological_entity id="o19479" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa subpercurrent to long-excurrent or nearly ecostate;</text>
    </statement>
    <statement id="d0_s10">
      <text>alar cells enlarged and bulging or walls firm;</text>
      <biological_entity id="o19480" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="long-excurrent" value_original="long-excurrent" />
        <character name="architecture" src="d0_s9" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity id="o19481" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence_or_shape" src="d0_s10" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o19482" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>medial laminal cells linear to linear-rhomboidal.</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="medial laminal" id="o19483" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="linear-rhomboidal" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perigonia gemmate;</text>
      <biological_entity id="o19484" name="perigonium" name_original="perigonia" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>perichaetia lateral on short branches.</text>
      <biological_entity id="o19485" name="perichaetium" name_original="perichaetia" src="d0_s14" type="structure">
        <character constraint="on branches" constraintid="o19486" is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity id="o19486" name="branch" name_original="branches" src="d0_s14" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seta short or elongate.</text>
      <biological_entity id="o19487" name="seta" name_original="seta" src="d0_s15" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsule immersed, laterally emergent, emergent, or exserted, oval, oval-oblong, subcylindric, or cylindric;</text>
      <biological_entity id="o19488" name="capsule" name_original="capsule" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="immersed" value_original="immersed" />
        <character is_modifier="false" modifier="laterally" name="location" src="d0_s16" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="location" src="d0_s16" value="emergent" value_original="emergent" />
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oval-oblong" value_original="oval-oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subcylindric" value_original="subcylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subcylindric" value_original="subcylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stomata absent;</text>
      <biological_entity id="o19489" name="stoma" name_original="stomata" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>annulus rudimentary or rarely massive;</text>
      <biological_entity id="o19490" name="annulus" name_original="annulus" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s18" value="massive" value_original="massive" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum obtuse-conic, acute, or obliquely rostrate;</text>
      <biological_entity id="o19491" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="obtuse-conic" value_original="obtuse-conic" />
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s19" value="rostrate" value_original="rostrate" />
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s19" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>peristome diplolepidous;</text>
      <biological_entity id="o19492" name="peristome" name_original="peristome" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="diplolepidous" value_original="diplolepidous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>exostome teeth 16, linear, sometimes united in pairs at apices, external surface smooth, occasionally papillose or spiculose-papillose, lightly thickened, internal surface heavily thickened, granulose, sometimes obscurely striate, inner trabeculae heavily thickened;</text>
      <biological_entity constraint="exostome" id="o19493" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="16" value_original="16" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s21" value="linear" value_original="linear" />
        <character constraint="in pairs" constraintid="o19494" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s21" value="united" value_original="united" />
      </biological_entity>
      <biological_entity id="o19494" name="pair" name_original="pairs" src="d0_s21" type="structure" />
      <biological_entity id="o19495" name="apex" name_original="apices" src="d0_s21" type="structure" />
      <biological_entity constraint="external" id="o19496" name="surface" name_original="surface" src="d0_s21" type="structure">
        <character is_modifier="false" name="relief" src="d0_s21" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="occasionally" name="relief" src="d0_s21" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s21" value="spiculose-papillose" value_original="spiculose-papillose" />
        <character is_modifier="false" modifier="lightly" name="size_or_width" src="d0_s21" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="internal" id="o19497" name="surface" name_original="surface" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="heavily" name="size_or_width" src="d0_s21" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s21" value="granulose" value_original="granulose" />
        <character is_modifier="false" modifier="sometimes obscurely" name="coloration_or_pubescence_or_relief" src="d0_s21" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o19498" name="trabecula" name_original="trabeculae" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="heavily" name="size_or_width" src="d0_s21" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o19494" id="r2789" name="at" negation="false" src="d0_s21" to="o19495" />
    </statement>
    <statement id="d0_s22">
      <text>endostome segments linear, red to brownish orange, papillose, united by lateral bars only distally (trellis imperfect) or throughout (trellis perfect).</text>
      <biological_entity constraint="endostome" id="o19499" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s22" value="linear" value_original="linear" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s22" to="brownish orange" />
        <character is_modifier="false" name="relief" src="d0_s22" value="papillose" value_original="papillose" />
        <character constraint="by lateral bars" constraintid="o19500" is_modifier="false" name="fusion" src="d0_s22" value="united" value_original="united" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19500" name="bar" name_original="bars" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>Calyptra mitrate or cucullate, smooth, naked, sometimes clasping base of seta when young.</text>
      <biological_entity id="o19501" name="calyptra" name_original="calyptra" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="mitrate" value_original="mitrate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s23" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o19502" name="base" name_original="base" src="d0_s23" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture_or_fixation" src="d0_s23" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o19503" name="seta" name_original="seta" src="d0_s23" type="structure" />
      <relation from="o19502" id="r2790" name="part_of" negation="false" src="d0_s23" to="o19503" />
    </statement>
    <statement id="d0_s24">
      <text>Spores small or large, smooth to lightly papillose.</text>
      <biological_entity id="o19504" name="spore" name_original="spores" src="d0_s24" type="structure">
        <character is_modifier="false" name="size" src="d0_s24" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s24" value="large" value_original="large" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s24" to="lightly papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, Eurasia, Africa, Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 3, species 19 (3 genera, 15 species in the flora).</discussion>
  <discussion>Fontinalaceae are distinguished by the aquatic or semi-aquatic habitat, long axillary hairs, absence of stem central strand, lack of stomata, odd exostome morphology, and unusual trellised endostome. The exostome teeth in Fontinalaceae are linear, with outer and inner plates equally high. The endostome is firm rather than membranous because of relatively heavy secondary thickening.</discussion>
  <discussion>All three genera and most species of Fontinalaceae are found in North America. Brachelyma, which is semi-aquatic with a strong single costa and occasional stem paraphyllia, appears to be the basal genus in the family. Fontinalis, on the other hand, is generally aquatic with a nearly absent costa, well-developed, often bulging alar cells, and immersed to short-emergent capsules, and it appears to be the most derived genus in Fontinalaceae.</discussion>
  <references>
    <reference>Buck, W. R. and B. H. Allen. 1997. Ordinal placement of the Fontinalaceae. Cryptog. Bryol. Lichénol. 18: 227–234.</reference>
    <reference>Cardot, J. 1892. Monographie des Fontinalacées. Mém. Soc. Sci. Nat. Math. Cherbourg 28: 1–152.</reference>
    <reference>Welch, W. H. 1960. A Monograph of the Fontinalaceae. The Hague.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ecostate or nearly so.</description>
      <determination>3 Fontinalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves with costae present</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves oblong-lanceolate to lanceolate; calyptrae covering operculum only; setae0.7-1.5 mm; capsules immersed.</description>
      <determination>1 Brachelyma</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves lanceolate to linear-lanceolate; calyptrae covering capsule; setae 3-15(-20) mm; capsules immersed, emergent, or exserted.</description>
      <determination>2 Dichelyma</determination>
    </key_statement>
  </key>
</bio:treatment>