<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence &amp; H. P. Ramsay" date="unknown" rank="genus">gemmabryum</taxon_name>
    <taxon_name authority="J. R. Spence" date="2009" rank="section">TUBERIBRYUM</taxon_name>
    <taxon_name authority="(Crundwell &amp; Nyholm) J. R. Spence" date="2007" rank="species">violaceum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>89: 112. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus gemmabryum;section tuberibryum;species violaceum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099130</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Crundwell &amp; Nyholm" date="unknown" rank="species">violaceum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>116: 94. 1963</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species violaceum</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, green, yellow-green, often reddish.</text>
      <biological_entity id="o23783" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.4–1 (–1.5) cm;</text>
      <biological_entity id="o23784" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids pale to bright violet, purple, or rarely red-purple.</text>
      <biological_entity id="o23785" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s2" to="bright violet" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s2" value="red-purple" value_original="red-purple" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_density" src="d0_s2" value="red-purple" value_original="red-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves loosely set, ovatelanceolate, weakly concave, 0.4–1 (–1.5) mm;</text>
      <biological_entity id="o23786" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23787" name="set" name_original="set" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base not decurrent;</text>
      <biological_entity id="o23788" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane to weakly revolute basally, entire to serrulate distally, limbidium absent;</text>
      <biological_entity id="o23789" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="plane" modifier="basally" name="shape" src="d0_s5" to="weakly revolute" />
        <character char_type="range_value" from="entire" modifier="distally" name="architecture_or_shape" src="d0_s5" to="serrulate" />
      </biological_entity>
      <biological_entity id="o23790" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute;</text>
      <biological_entity id="o23791" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa short-excurrent, awn slender;</text>
      <biological_entity id="o23792" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>alar cells similar to adjacent juxtacostal cells;</text>
      <biological_entity id="o23793" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o23794" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o23795" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o23794" id="r3381" name="to" negation="false" src="d0_s8" to="o23795" />
    </statement>
    <statement id="d0_s9">
      <text>proximal laminal cells abruptly quadrate to short-rectangular, 2–4: 1;</text>
      <biological_entity constraint="proximal laminal" id="o23796" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="abruptly quadrate" name="shape" src="d0_s9" to="short-rectangular" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>medial and distal cells (30–) 40–60 × 8–14 µm, 3–4: 1.</text>
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction by rhizoidal tubers, on long rhizoids in soil, purple-red or rarely orange, irregularly spheric, 60–80 (–100) µm, cells 25–30 µm, smooth.</text>
      <biological_entity constraint="medial and distal" id="o23797" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" from_unit="um" name="atypical_length" src="d0_s10" to="40" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s10" to="60" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s10" to="14" to_unit="um" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="4" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o23798" name="rhizoidal" name_original="rhizoidal" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o23799" name="tuber" name_original="tubers" src="d0_s11" type="structure" />
      <biological_entity id="o23800" name="rhizoid" name_original="rhizoids" src="d0_s11" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s11" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o23801" name="soil" name_original="soil" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple-red" value_original="purple-red" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s11" value="orange" value_original="orange" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s11" to="100" to_unit="um" />
        <character char_type="range_value" from="60" from_unit="um" name="some_measurement" src="d0_s11" to="80" to_unit="um" />
      </biological_entity>
      <relation from="o23800" id="r3382" name="in" negation="false" src="d0_s11" to="o23801" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous.</text>
    </statement>
    <statement id="d0_s13">
      <text>[Capsule nutant, 1–3 mm].</text>
      <biological_entity id="o23802" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="some_measurement" src="d0_s11" to="30" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Apr–Jul (spring–summer).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Jul" from="Apr" />
        <character name="capsules maturing time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp soil, soil over rock, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" modifier="damp" />
        <character name="habitat" value="soil" constraint="over rock , disturbed sites" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.S., Ont., Que.; Ariz., Calif., Idaho, Mass., Mo., Nev., Utah, Wash., Wis.; s South America (Argentina, Chile); Eurasia; Atlantic Islands (Tenerife); Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="s South America (Argentina)" establishment_means="native" />
        <character name="distribution" value="s South America (Chile)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Tenerife)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Gemmabryum violaceum is distinguished by the combination of violet rhizoids and small, spheric, red to purple-red or orange rhizoidal tubers. Gemmabryum ruderale is similar but has larger tubers, and European material at least has strongly papillose rhizoids compared to relatively smooth rhizoids of G. violaceum.</discussion>
  
</bio:treatment>