<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">plagiotheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">plagiothecium</taxon_name>
    <taxon_name authority="Frisvoll" date="1982" rank="species">berggrenianum</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>7: 96, fig. 2a – i. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plagiotheciaceae;genus plagiothecium;species berggrenianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250062241</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense tufts, light green to yellowish, glossy.</text>
      <biological_entity id="o16622" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="light green" name="coloration" notes="" src="d0_s0" to="yellowish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16623" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o16622" id="r2397" name="in" negation="false" src="d0_s0" to="o16623" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 8 cm, 1–1.5 mm wide across leafy stem, erect, julaceous.</text>
      <biological_entity id="o16624" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" constraint="across stem" constraintid="o16625" from="1" from_unit="mm" name="width" src="d0_s1" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
      <biological_entity id="o16625" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, closely imbricate, ovate or oblong-ovate, symmetric, concave, 1.5–2.5 × 0.8–1.2 mm;</text>
      <biological_entity id="o16626" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins broadly recurved throughout, entire or denticulate at apex;</text>
      <biological_entity id="o16627" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly; throughout" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="at apex" constraintid="o16628" is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o16628" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex acuminate, not abruptly contracted, usually recurved;</text>
      <biological_entity id="o16629" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="not abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa with one branch sometimes reaching 1/3 leaf length;</text>
      <biological_entity id="o16631" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o16632" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1/3" value_original="1/3" />
      </biological_entity>
      <relation from="o16630" id="r2398" name="with" negation="false" src="d0_s5" to="o16631" />
      <relation from="o16631" id="r2399" modifier="sometimes" name="reaching" negation="false" src="d0_s5" to="o16632" />
    </statement>
    <statement id="d0_s6">
      <text>alar cells rectangular or inflated and oval, 40–60 × 9–24 µm, in 2–4 vertical rows, terminating in 1 cell at base, region triangular or rarely somewhat oval;</text>
      <biological_entity id="o16630" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o16633" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oval" value_original="oval" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s6" to="60" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s6" to="24" to_unit="um" />
      </biological_entity>
      <biological_entity id="o16634" name="row" name_original="rows" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o16635" name="cell" name_original="cell" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16636" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o16637" name="region" name_original="region" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="rarely somewhat" name="shape" src="d0_s6" value="oval" value_original="oval" />
      </biological_entity>
      <relation from="o16633" id="r2400" name="in" negation="false" src="d0_s6" to="o16634" />
      <relation from="o16633" id="r2401" name="terminating in" negation="false" src="d0_s6" to="o16635" />
      <relation from="o16635" id="r2402" name="at" negation="false" src="d0_s6" to="o16636" />
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells 85–141 × 6–12 µm. Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition unknown.</text>
      <biological_entity constraint="medial laminal" id="o16638" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="85" from_unit="um" name="length" src="d0_s7" to="141" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sporophytes unknown.</text>
      <biological_entity id="o16639" name="sporophyte" name_original="sporophytes" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Swales, wet tundra, rocky slopes, cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="swales" />
        <character name="habitat" value="wet tundra" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T.; Alaska; n Europe (Norway).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Europe (Norway)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plagiothecium berggrenianum is an Arctic species that has been confused with P. cavifolium, or with julaceous forms of P. denticulatum and P. laetum. The acuminate leaves with recurved apices separate P. berggrenianum from P. denticulatum, while the inflated, oval cells in the decurrencies will distinguish it from P. cavifolium and P. laetum.</discussion>
  
</bio:treatment>