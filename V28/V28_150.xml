<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">98</other_info_on_meta>
    <other_info_on_meta type="mention_page">661</other_info_on_meta>
    <other_info_on_meta type="mention_page">663</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bartramiaceae</taxon_name>
    <taxon_name authority="Swartz ex F. Weber &amp; D. Mohr in F. Weber" date="unknown" rank="genus">CONOSTOMUM</taxon_name>
    <place_of_publication>
      <publication_title>in F. Weber, Naturh. Reise Schweden,</publication_title>
      <place_in_publication>121. 1804</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bartramiaceae;genus CONOSTOMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek konos, cone, and stoma, opening, alluding to operculum</other_info_on_name>
    <other_info_on_name type="fna_id">107893</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense tufts, glaucous or yellow-green.</text>
      <biological_entity id="o8494" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8495" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o8494" id="r1196" name="in" negation="false" src="d0_s0" to="o8495" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–3 cm, erect, simple or 2-fid;</text>
    </statement>
    <statement id="d0_s2">
      <text>pentagonal in cross-section, hyalodermis present, epidermis not prorulose;</text>
      <biological_entity id="o8496" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-fid" value_original="2-fid" />
        <character constraint="in cross-section" constraintid="o8497" is_modifier="false" name="shape" src="d0_s2" value="pentagonal" value_original="pentagonal" />
      </biological_entity>
      <biological_entity id="o8497" name="cross-section" name_original="cross-section" src="d0_s2" type="structure" />
      <biological_entity id="o8498" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>radiculose proximally, rhizoids smooth.</text>
      <biological_entity id="o8499" name="epidermis" name_original="epidermis" src="d0_s2" type="structure" />
      <biological_entity id="o8500" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves in 5 distinct rows, imbricate, appressed when dry or moist, narrowly lanceolate, 1-stratose;</text>
      <biological_entity id="o8501" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry or moist" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o8502" name="row" name_original="rows" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="5" value_original="5" />
        <character is_modifier="true" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o8501" id="r1197" name="in" negation="false" src="d0_s4" to="o8502" />
    </statement>
    <statement id="d0_s5">
      <text>base not sheathing;</text>
      <biological_entity id="o8503" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s5" value="sheathing" value_original="sheathing" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins slightly revolute in distal 1/2, serrulate near apex, teeth single;</text>
      <biological_entity id="o8504" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character constraint="in distal 1/2" constraintid="o8505" is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s6" value="revolute" value_original="revolute" />
        <character constraint="near apex" constraintid="o8506" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8505" name="1/2" name_original="1/2" src="d0_s6" type="structure" />
      <biological_entity id="o8506" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o8507" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex acuminate or rarely blunt;</text>
      <biological_entity id="o8508" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa percurrent to excurrent, sometimes subpercurrent in proximal leaves, abaxial surface mammillose-toothed;</text>
      <biological_entity id="o8509" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s8" to="excurrent" />
        <character constraint="in proximal leaves" constraintid="o8510" is_modifier="false" modifier="sometimes" name="position" src="d0_s8" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8510" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o8511" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="mammillose-toothed" value_original="mammillose-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basal laminal cells more lax than distal cells;</text>
      <biological_entity constraint="basal laminal" id="o8512" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character constraint="than distal cells" constraintid="o8513" is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="more lax" value_original="more lax" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8513" name="cell" name_original="cells" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>distal cells rectangular to short-rhomboidal, smooth or mammillose at distal ends on both surfaces, walls thick.</text>
      <biological_entity constraint="distal" id="o8514" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s10" to="short-rhomboidal" />
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character constraint="at distal ends" constraintid="o8515" is_modifier="false" name="relief" src="d0_s10" value="mammillose" value_original="mammillose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8515" name="end" name_original="ends" src="d0_s10" type="structure" />
      <biological_entity id="o8516" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <relation from="o8515" id="r1198" name="on" negation="false" src="d0_s10" to="o8516" />
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous [or autoicous];</text>
      <biological_entity id="o8517" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thick" value_original="thick" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perigonia cupulate or gemmiform;</text>
      <biological_entity id="o8518" name="perigonium" name_original="perigonia" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="gemmiform" value_original="gemmiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perichaetial leaves larger than stem-leaves.</text>
      <biological_entity constraint="perichaetial" id="o8519" name="leaf" name_original="leaves" src="d0_s14" type="structure">
        <character constraint="than stem-leaves" constraintid="o8520" is_modifier="false" name="size" src="d0_s14" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o8520" name="stem-leaf" name_original="stem-leaves" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Seta single, elongate, flexuose.</text>
      <biological_entity id="o8521" name="seta" name_original="seta" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s15" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="course" src="d0_s15" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsule erect to inclined, ovoid, wrinkled and furrowed when dry, mouth not small, not oblique;</text>
      <biological_entity id="o8522" name="capsule" name_original="capsule" src="d0_s16" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s16" to="inclined" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="relief" src="d0_s16" value="wrinkled" value_original="wrinkled" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s16" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity id="o8523" name="mouth" name_original="mouth" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s16" value="small" value_original="small" />
        <character is_modifier="false" modifier="not" name="orientation_or_shape" src="d0_s16" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>annulus absent;</text>
      <biological_entity id="o8524" name="annulus" name_original="annulus" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>operculum conic, rostrate;</text>
      <biological_entity id="o8525" name="operculum" name_original="operculum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s18" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>peristome single or rarely absent;</text>
      <biological_entity id="o8526" name="peristome" name_original="peristome" src="d0_s19" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s19" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>exostome teeth reddish orange to dark-brown, linear-lanceolate, smooth throughout or finely papillose basally, apically connate.</text>
      <biological_entity constraint="exostome" id="o8527" name="tooth" name_original="teeth" src="d0_s20" type="structure">
        <character char_type="range_value" from="reddish orange" name="coloration" src="d0_s20" to="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely; finely; basally" name="relief" src="d0_s20" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="apically" name="fusion" src="d0_s20" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Spores spheric to reniform, coarsely papillose [to smooth].</text>
      <biological_entity id="o8528" name="spore" name_original="spores" src="d0_s21" type="structure">
        <character char_type="range_value" from="spheric" name="shape" src="d0_s21" to="reniform" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s21" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, South America, Europe, Asia, Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 7–15 (1 in the flora).</discussion>
  <references>
    <reference>Frahm, J.-P. et al. 1996b. Revision der Gattung Conostomum (Musci; Bartramiaceae). Trop. Bryol. 12: 97–114.</reference>
  </references>
  
</bio:treatment>