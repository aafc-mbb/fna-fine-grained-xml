<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">650</other_info_on_meta>
    <other_info_on_meta type="mention_page">663</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Greville &amp; Arnott" date="unknown" rank="family">splachnaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="genus">TETRAPLODON</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 211, plates 288 – 290. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family splachnaceae;genus TETRAPLODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek tetraplo - , fourfold, and odon, tooth, alluding to arrangement of exostome teeth</other_info_on_name>
    <other_info_on_name type="fna_id">132623</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense tufts, often brown proximally, bright green to yellow-green distally.</text>
      <biological_entity id="o7487" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often; proximally" name="coloration" notes="" src="d0_s0" value="brown" value_original="brown" />
        <character char_type="range_value" from="bright green" modifier="distally" name="coloration" src="d0_s0" to="yellow-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7488" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o7487" id="r1026" name="in" negation="false" src="d0_s0" to="o7488" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–3 (–8) cm;</text>
    </statement>
    <statement id="d0_s2">
      <text>often matted with rhizoids proximally.</text>
      <biological_entity id="o7489" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character constraint="with rhizoids" constraintid="o7490" is_modifier="false" modifier="often" name="growth_form" src="d0_s2" value="matted" value_original="matted" />
      </biological_entity>
      <biological_entity id="o7490" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves slender to oblong-lanceolate, or obovate and acuminate;</text>
      <biological_entity id="o7491" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins toothed or entire;</text>
      <biological_entity id="o7492" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute, acuminate, or subulate;</text>
      <biological_entity id="o7493" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa usually ending in subula;</text>
      <biological_entity id="o7494" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o7495" name="subulum" name_original="subula" src="d0_s6" type="structure" />
      <relation from="o7494" id="r1027" name="ending in" negation="false" src="d0_s6" to="o7495" />
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells elongate, rectangular;</text>
      <biological_entity constraint="proximal laminal" id="o7496" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal cells rectangular, hexagonal, or oblong-hexagonal.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous or rarely dioicous.</text>
      <biological_entity constraint="distal" id="o7497" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-hexagonal" value_original="oblong-hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="hexagonal" value_original="hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong-hexagonal" value_original="oblong-hexagonal" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 0.2–5 cm, not twisted.</text>
      <biological_entity id="o7498" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="twisted" value_original="twisted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule cleistocarpous or not, yellowish or reddish to dark-brown or black, cylindric to ovoid or spindle-shaped;</text>
      <biological_entity id="o7499" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="cleistocarpous" value_original="cleistocarpous" />
        <character name="dehiscence" src="d0_s11" value="not" value_original="not" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s11" to="dark-brown or black" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s11" to="ovoid or spindle-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>hypophysis same color or darker than urn, short-to-elongate, narrower to barely wider than urn;</text>
      <biological_entity id="o7500" name="hypophysis" name_original="hypophysis" src="d0_s12" type="structure">
        <character constraint="than urn" constraintid="o7501" is_modifier="false" name="coloration" src="d0_s12" value="darker" value_original="darker" />
        <character char_type="range_value" from="short" name="coloration" src="d0_s12" to="elongate" />
        <character char_type="range_value" constraint="than urn" constraintid="o7502" from="narrower" name="width" src="d0_s12" to="barely wider" value="narrower to barely wider" value_original="narrower to barely wider" />
      </biological_entity>
      <biological_entity id="o7501" name="urn" name_original="urn" src="d0_s12" type="structure" />
      <biological_entity id="o7502" name="urn" name_original="urn" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>annulus usually absent;</text>
      <biological_entity id="o7503" name="annulus" name_original="annulus" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum hemispheric to bluntly conic;</text>
      <biological_entity id="o7504" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s14" to="bluntly conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>peristome single;</text>
      <biological_entity id="o7505" name="peristome" name_original="peristome" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth 16, at first ± coherent in 4’s, later in 2’s, usually reflexed when dry, inflexed when moist, of 2 layers of cells.</text>
      <biological_entity constraint="exostome" id="o7506" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character constraint="in ’" constraintid="o7507" is_modifier="false" modifier="more or less" name="fusion" src="d0_s16" value="coherent" value_original="coherent" />
        <character constraint="in 2 ’ s" is_modifier="false" name="condition" notes="" src="d0_s16" value="later" value_original="later" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s16" value="inflexed" value_original="inflexed" />
      </biological_entity>
      <biological_entity id="o7507" name="’" name_original="’" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o7508" name="layer" name_original="layers" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7509" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <relation from="o7506" id="r1028" name="consist_of" negation="false" src="d0_s16" to="o7508" />
      <relation from="o7508" id="r1029" name="part_of" negation="false" src="d0_s16" to="o7509" />
    </statement>
    <statement id="d0_s17">
      <text>Calyptra conic-mitrate or cucullate, small, not constricted beyond base.</text>
      <biological_entity id="o7510" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="conic-mitrate" value_original="conic-mitrate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="size" src="d0_s17" value="small" value_original="small" />
        <character constraint="beyond base" constraintid="o7511" is_modifier="false" modifier="not" name="size" src="d0_s17" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o7511" name="base" name_original="base" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 8–12 µm, smooth or slightly papillose.</text>
      <biological_entity id="o7512" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s18" to="12" to_unit="um" />
        <character is_modifier="false" name="relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; alpine, subalpine, and temperate to subarctic regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="alpine" establishment_means="native" />
        <character name="distribution" value="subalpine" establishment_means="native" />
        <character name="distribution" value="and temperate to subarctic regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 10 (5 in the flora).</discussion>
  <discussion>Species of Tetraplodon are entomophilous and coprophilous although apparently restricted to the dung of carnivores, bones, and owl pellets. The hypophysis is well developed, elongate, and usually wrinkled when dry; the exostome teeth are connate in 4’s when young but in 2’s as they age; and the seta is stout.</discussion>
  <references>
    <reference>Steere, W. C. 1977b. Tetraplodon paradoxus and T. pallidus (Musci: Splachnaceae) in northern North America. Brittonia 29: 353–367.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves long-lanceolate; margins with large teeth, occasionally entire.</description>
      <determination>2 Tetraplodon angustatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ovate or oblong-ovate; margins entire or nearly so</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Capsules cleistocarpous, clear pale yellow to stramineous; hypophysis conspicuously narrower than urn.</description>
      <determination>1 Tetraplodon paradoxus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Capsules not cleistocarpous, usually reddish to black, rarely yellow to stramineous; hypophysis usually as wide or wider than urn</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Setae shorter than 1 cm; leaf apices broadly acute; costae ending before subula.</description>
      <determination>3 Tetraplodon urceolatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Setae 1-5 cm; leaf apices subulate or acuminate; costae ending in subula</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf apices abruptly subulate; capsules red, dark red to black with age.</description>
      <determination>4 Tetraplodon mnioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf apices acuminate; capsules pale yellow to stramineous.</description>
      <determination>5 Tetraplodon pallidus</determination>
    </key_statement>
  </key>
</bio:treatment>