<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Cardot &amp; Thériot" date="1902" rank="species">fenestratum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Wash. Acad. Sci.</publication_title>
      <place_in_publication>4: 310, plate 16, fig. 2. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species fenestratum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250061868</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 1.2 cm.</text>
      <biological_entity id="o11699" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1.2" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves erect-appressed when dry, ovatelanceolate to narrowly lanceolate, 2.2–3.2 mm;</text>
      <biological_entity id="o11700" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s1" value="erect-appressed" value_original="erect-appressed" />
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s1" to="narrowly lanceolate" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="distance" src="d0_s1" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins narrowly revolute to below apex, entire;</text>
      <biological_entity id="o11701" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="below apex" constraintid="o11702" is_modifier="false" modifier="narrowly" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11702" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apex acute and often cuspidate-apiculate;</text>
      <biological_entity id="o11703" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s3" value="cuspidate-apiculate" value_original="cuspidate-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells elongate, walls thick, nodose;</text>
      <biological_entity constraint="basal laminal" id="o11704" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o11705" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" name="shape" src="d0_s4" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cells 9–15 µm, 1-stratose, papillae 1 or 2 per cell, conic, small.</text>
      <biological_entity constraint="distal" id="o11706" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s5" to="15" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o11708" name="cell" name_original="cell" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity id="o11707" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or per" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or per" value="2" value_original="2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 2 mm.</text>
      <biological_entity id="o11709" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule exserted, ovate-oblong when dry, ovate when moist, 1–1.8 mm, smooth to moderately 8-ribbed;</text>
      <biological_entity id="o11710" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s9" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="architecture" src="d0_s9" to="moderately 8-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stomata superficial;</text>
      <biological_entity id="o11711" name="stoma" name_original="stomata" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="superficial" value_original="superficial" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>peristome single, rarely double;</text>
      <biological_entity id="o11712" name="peristome" name_original="peristome" src="d0_s11" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s11" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>prostome absent;</text>
      <biological_entity id="o11713" name="prostome" name_original="prostome" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome teeth 8, splitting to 16, erect to recurved when old, finely papillose to almost smooth basally, coarsely papillose distally, fenestrate, cancellate;</text>
      <biological_entity constraint="exostome" id="o11714" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="8" value_original="8" />
        <character is_modifier="false" name="architecture_or_dehiscence" src="d0_s13" value="splitting" value_original="splitting" />
        <character name="quantity" src="d0_s13" value="16" value_original="16" />
        <character char_type="range_value" from="erect" modifier="when old" name="orientation" src="d0_s13" to="recurved" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s13" value="papillose to almost" value_original="papillose to almost" />
        <character is_modifier="false" modifier="almost; basally" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="coarsely; distally" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="fenestrate" value_original="fenestrate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="cancellate" value_original="cancellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>endostome segments 8, well developed, usually present when capsule old and dry, narrow, of 1 row of cells, papillose-reticulate.</text>
      <biological_entity constraint="endostome" id="o11715" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s14" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="when capsule old and dry" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" notes="" src="d0_s14" value="papillose-reticulate" value_original="papillose-reticulate" />
      </biological_entity>
      <biological_entity id="o11716" name="row" name_original="row" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11717" name="cell" name_original="cells" src="d0_s14" type="structure" />
      <relation from="o11715" id="r1647" name="consist_of" negation="false" src="d0_s14" to="o11716" />
      <relation from="o11716" id="r1648" name="part_of" negation="false" src="d0_s14" to="o11717" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra short-conic, smooth, hairs few, smooth.</text>
      <biological_entity id="o11718" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="short-conic" value_original="short-conic" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o11719" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="few" value_original="few" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 20–27 µm.</text>
      <biological_entity id="o11720" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" src="d0_s16" to="27" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Habitat unknown</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="habitat unknown" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>elevations (0 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" constraint="elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orthotrichum fenestratum was collected only once by J. M. Macoun, who did not record the substrate, but it was probably near sea level. The plants are distinguished by their exostome teeth that are cancellate to the base, fenestrate and perforate only distally, and the ovate capsule.</discussion>
  
</bio:treatment>