<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="Hampe" date="unknown" rank="genus">haplodontium</taxon_name>
    <taxon_name authority="(Showers) J. R. Spence" date="2005" rank="species">tehamense</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 26. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus haplodontium;species tehamense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099136</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mielichhoferia</taxon_name>
    <taxon_name authority="Showers" date="unknown" rank="species">tehamensis</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>83: 365, figs. 1–3. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mielichhoferia;species tehamensis</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants shiny light green, yellow-green, or white-green, light-brown below.</text>
      <biological_entity id="o11623" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="white-green" value_original="white-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="white-green" value_original="white-green" />
        <character is_modifier="false" modifier="below" name="coloration" src="d0_s0" value="light-brown" value_original="light-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1 cm, somewhat julaceous, strongly branched.</text>
      <biological_entity id="o11624" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves imbricate when dry, erect to erect-spreading when moist, elliptic to oblong, 0.5–1 mm;</text>
      <biological_entity id="o11625" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s2" to="erect-spreading" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins plane;</text>
      <biological_entity id="o11626" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex rounded-obtuse to broadly acute, apiculus absent;</text>
      <biological_entity id="o11627" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded-obtuse" name="shape" src="d0_s4" to="broadly acute" />
      </biological_entity>
      <biological_entity id="o11628" name="apiculu" name_original="apiculus" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa subpercurrent, yellowish;</text>
      <biological_entity id="o11629" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="subpercurrent" value_original="subpercurrent" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 40–80 (–100) × 10–20 µm; distal cells at apex irregularly short-rectangular to subquadrate.</text>
      <biological_entity constraint="laminal" id="o11630" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s6" to="100" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s6" to="80" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s6" to="20" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11631" name="cell" name_original="cells" src="d0_s6" type="structure" />
      <biological_entity id="o11632" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="irregularly short-rectangular" name="shape" src="d0_s6" to="subquadrate" />
      </biological_entity>
      <relation from="o11631" id="r1635" name="at" negation="false" src="d0_s6" to="o11632" />
    </statement>
    <statement id="d0_s7">
      <text>Seta redbrown, 0.3–0.7 cm, flexuose.</text>
      <biological_entity id="o11633" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s7" to="0.7" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s7" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule horizontal to nutant, short and broadly pyriform, 1.2–2.5 mm;</text>
      <biological_entity id="o11634" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s8" to="nutant" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="pyriform" value_original="pyriform" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>operculum convex, umbonate;</text>
      <biological_entity id="o11635" name="operculum" name_original="operculum" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s9" value="umbonate" value_original="umbonate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome absent.</text>
      <biological_entity id="o11636" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 10–14 µm, ± smooth.</text>
      <biological_entity id="o11637" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s11" to="14" to_unit="um" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature Aug (summer).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="Aug" from="Aug" />
        <character name="capsules maturing time" char_type="atypical_range" to="summer" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices of volcanic rock in montane areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="of volcanic rock in montane areas" />
        <character name="habitat" value="volcanic rock" constraint="in montane areas" />
        <character name="habitat" value="montane areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>high elevations (2200-2600 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="high" />
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2200" from_unit="m" constraint="high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Haplodontium tehamense is a distinctive species, with eperistomate capsules and somewhat julaceous stems. The species is known only from a few locations in Lassen Volcanic National Park.</discussion>
  <discussion>of conservation concern</discussion>
  
</bio:treatment>