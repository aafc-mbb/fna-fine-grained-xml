<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lloyd R. Stark</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">624</other_info_on_meta>
    <other_info_on_meta type="mention_page">584</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">593</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="mention_page">625</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leptodontaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">FORSSTROEMIA</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>19: 605. 1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leptodontaceae;genus FORSSTROEMIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Johan Erik Forsström, 1775 – 1824, Swedish pastor and plant collector</other_info_on_name>
    <other_info_on_name type="fna_id">112947</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="P. Beauvois" date="unknown" rank="genus">Lasia</taxon_name>
    <place_of_publication>
      <publication_title>Mag. Encycl.</publication_title>
      <place_in_publication>5: 315. 1804,</place_in_publication>
      <other_info_on_pub>not Loureiro 1790 [Araceae]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus lasia</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Secondary stems irregularly pinnate;</text>
      <biological_entity constraint="secondary" id="o958" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s0" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>paraphyllia absent;</text>
      <biological_entity id="o959" name="paraphyllium" name_original="paraphyllia" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia subfoliose to occasionally filamentous.</text>
      <biological_entity id="o960" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally" name="texture" src="d0_s2" value="filamentous" value_original="filamentous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves loosely appressed to imbricate when dry, erect-spreading to squarrose when moist, lanceolate, ovatelanceolate or broadly so, ovate, ovate-acuminate, oval, or deltoid;</text>
      <biological_entity id="o961" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="erect-spreading" modifier="when moist" name="orientation" src="d0_s3" to="squarrose" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character name="shape" src="d0_s3" value="broadly" value_original="broadly" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s3" value="deltoid" value_original="deltoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins recurved to mid leaf or beyond, entire or serrulate distally;</text>
      <biological_entity id="o962" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity constraint="mid" id="o963" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute to short-acuminate;</text>
      <biological_entity id="o964" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa single or double, ending before apex;</text>
      <biological_entity id="o966" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o965" id="r121" name="ending before" negation="false" src="d0_s6" to="o966" />
    </statement>
    <statement id="d0_s7">
      <text>alar region extending up margins to 1/3 leaf length;</text>
      <biological_entity id="o965" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character name="quantity" src="d0_s6" value="double" value_original="double" />
      </biological_entity>
      <biological_entity id="o967" name="region" name_original="region" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s7" to="1/3" />
      </biological_entity>
      <biological_entity id="o968" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o969" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <relation from="o967" id="r122" name="extending up" negation="false" src="d0_s7" to="o968" />
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells elliptic to linear.</text>
      <biological_entity constraint="medial laminal" id="o970" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="elliptic" name="arrangement" src="d0_s8" to="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves similar, smaller.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="branch" id="o971" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaf apex straight.</text>
      <biological_entity constraint="leaf" id="o972" name="apex" name_original="apex" src="d0_s11" type="structure" constraint_original="perichaetial leaf">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 0.4–3.9 mm.</text>
      <biological_entity id="o973" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule erect, immersed to exserted, cylindric;</text>
      <biological_entity id="o974" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata absent;</text>
      <biological_entity id="o975" name="stoma" name_original="stomata" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum conic to erect or obliquely rostrate;</text>
      <biological_entity id="o976" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic" value_original="conic" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s15" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth free to base or rarely connate proximally, linear-lanceolate, smooth, granulose, or minutely papillose;</text>
      <biological_entity constraint="exostome" id="o977" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character constraint="to base" constraintid="o978" is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" modifier="rarely; proximally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s16" value="granulose" value_original="granulose" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o978" name="base" name_original="base" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>endostome rudimentary to absent.</text>
      <biological_entity id="o979" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s17" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Calyptra hairy.</text>
      <biological_entity id="o980" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 15–35 µm, green.</text>
      <biological_entity id="o981" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s19" to="35" to_unit="um" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="green" value_original="green" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America, s Asia (India, Nepal), Africa, se Australia; temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="s Asia (India)" establishment_means="native" />
        <character name="distribution" value="s Asia (Nepal)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="se Australia" establishment_means="native" />
        <character name="distribution" value="temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 11 (2 in the flora).</discussion>
  <discussion>Plants of Forsstroemia are epiphytic or occasionally found on rock. Sporophyte generations (cohorts) overlap in their development, with individual sporophytes requiring approximately 18 months to mature. As a result, embryonic sporophytes from the present year are found along the same stem with maturing sporophytes initiated in the previous year. Spores are dispersed in winter. This pattern of sporophyte maturation is shared with species in the related genera Alsia, Dendroalsia, Leptodon, and Neckera. The reddish, smooth seta is twisted when dry and straight when moist; the peristome is inserted below the capsule mouth; the exostome teeth are occasionally perforate distally or rarely cribrose, with median lines straight or moderately zigzag, lamellae and trabeculae low or absent, plates usually symmetric, occasionally irregularly shaped, and additional external depositions rare; and the rudimentary endostome segments are often fragmentary and adherent to the exostome.</discussion>
  <references>
    <reference>Stark, L. R. 1987. A taxonomic monograph of Forsstroemia Lindb. (Bryopsida: Leptodontaceae). J. Hattori Bot. Lab. 63: 133–218.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Laminal cells fusiform to linear, 4-8:1; costae single or double, weak.</description>
      <determination>1 Forsstroemia trichomitria</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Laminal cells isodiametric to short-oblong, 1-3:1; costae single, extending beyond mid leaf.</description>
      <determination>2 Forsstroemia producta</determination>
    </key_statement>
  </key>
</bio:treatment>