<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert R. Ireland Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">524</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Ireland" date="unknown" rank="genus">DACRYOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>14: 70, fig. 1. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus DACRYOPHYLLUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek dakryo, weep, and phyllon, leaf, alluding to tearlike appearance of proximal prorulae of basal laminal cells</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">317902</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in thin mats, light to yellow-green, glossy.</text>
      <biological_entity id="o6584" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="light" name="coloration" notes="" src="d0_s0" to="yellow-green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6585" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o6584" id="r901" name="in" negation="false" src="d0_s0" to="o6585" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, simple or sparingly and irregularly branched;</text>
      <biological_entity id="o6586" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s1" value="sparingly" value_original="sparingly" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand absent;</text>
      <biological_entity id="o6587" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o6588" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia narrowly foliose or filamentous.</text>
      <biological_entity id="o6589" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
        <character is_modifier="false" name="texture" src="d0_s3" value="filamentous" value_original="filamentous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves similar, appressed-imbricate, oblong-lanceolate, not plicate;</text>
      <biological_entity id="o6590" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="appressed-imbricate" value_original="appressed-imbricate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o6591" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="appressed-imbricate" value_original="appressed-imbricate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base not decurrent;</text>
      <biological_entity id="o6592" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane, broadly incurved on one side at base, serrate basally, serrulate distally;</text>
      <biological_entity id="o6593" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character constraint="on side" constraintid="o6594" is_modifier="false" modifier="broadly" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" notes="" src="d0_s6" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o6594" name="side" name_original="side" src="d0_s6" type="structure" />
      <biological_entity id="o6595" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o6594" id="r902" name="at" negation="false" src="d0_s6" to="o6595" />
    </statement>
    <statement id="d0_s7">
      <text>apex acute to obtuse;</text>
      <biological_entity id="o6596" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ecostate or costa indistinctly double;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar cells differentiated, quadrate to rectangular;</text>
      <biological_entity id="o6597" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity id="o6598" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s9" to="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminal cells smooth or often with minute prorulae on abaxial surface at distal and proximal ends;</text>
      <biological_entity constraint="laminal" id="o6599" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character name="architecture_or_pubescence_or_relief" src="d0_s10" value="often" value_original="often" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6600" name="surface" name_original="surface" src="d0_s10" type="structure" />
      <biological_entity constraint="distal and proximal" id="o6601" name="end" name_original="ends" src="d0_s10" type="structure" />
      <relation from="o6599" id="r903" modifier="with minute" name="on" negation="false" src="d0_s10" to="o6600" />
      <relation from="o6600" id="r904" name="at" negation="false" src="d0_s10" to="o6601" />
    </statement>
    <statement id="d0_s11">
      <text>basal row of cells at insertion with 1 large prorula at proximal end on abaxial surface.</text>
      <biological_entity id="o6603" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <biological_entity constraint="proximal" id="o6604" name="end" name_original="end" src="d0_s11" type="structure" />
      <biological_entity constraint="abaxial" id="o6605" name="surface" name_original="surface" src="d0_s11" type="structure" />
      <relation from="o6602" id="r905" name="part_of" negation="false" src="d0_s11" to="o6603" />
      <relation from="o6604" id="r906" name="on" negation="false" src="d0_s11" to="o6605" />
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition unknown.</text>
      <biological_entity constraint="basal" id="o6602" name="row" name_original="row" src="d0_s11" type="structure">
        <character modifier="with" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character constraint="at proximal end" constraintid="o6604" is_modifier="false" name="size" src="d0_s11" value="large" value_original="large" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Dacryophyllum occurs in the west-central part of California in terrestrial habitats on calcareous substrates in coast Redwood forests. Dacryophyllum appears most closely related to Taxiphyllum, another genus in Hypnaceae that occurs on calcareous substrates, but it differs from all Taxiphyllum species primarily because they lack large prorulae on the abaxial surface at proximal ends of basal cells at the stem insertion. Dacryophyllum also has filamentous, entire pseudoparaphyllia occurring in clusters mixed with foliose, serrulate to toothed ones without an evident branch primordium. In Taxiphyllum, on the other hand, the branch primordia are nearly always evident and the species only have foliose pseudoparaphyllia.</discussion>
  <references>
    <reference>Kellman, K. and J. R. Shevock. 2006. Notes on Dacryophyllum falcifolium Ireland. Evansia 23: 36–39.</reference>
  </references>
  
</bio:treatment>