<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Dale H. Vitt</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Hooker &amp; Wilson" date="unknown" rank="genus">CLASMATODON</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>4: 421, plate 25, fig. A. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus CLASMATODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek klasma, fragment, and odon, tooth, alluding to irregularly bifid endostome</other_info_on_name>
    <other_info_on_name type="fna_id">107230</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in loose mats, dull to bright green.</text>
      <biological_entity id="o18012" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18013" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o18012" id="r2587" name="in" negation="false" src="d0_s0" to="o18013" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, densely terete-foliate, occasionally subsecund, irregularly branched, branches densely foliate;</text>
      <biological_entity id="o18014" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="occasionally" name="arrangement" src="d0_s1" value="subsecund" value_original="subsecund" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o18015" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>central strand absent;</text>
      <biological_entity constraint="central" id="o18016" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia narrowly acute;</text>
      <biological_entity id="o18017" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary hairs of 4–6 cells.</text>
      <biological_entity constraint="axillary" id="o18018" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o18019" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <relation from="o18018" id="r2588" name="consist_of" negation="false" src="d0_s4" to="o18019" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves appressed when dry, spreading when moist, imbricate, ovate to ovatelanceolate, concave, not plicate, 0.4–0.7 mm;</text>
      <biological_entity id="o18020" name="leaf-stem" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="distance" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base scarcely decurrent;</text>
      <biological_entity id="o18021" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins entire, subentire to serrulate distally;</text>
      <biological_entity id="o18022" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character char_type="range_value" from="subentire" modifier="distally" name="shape" src="d0_s7" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute to narrowly obtuse;</text>
      <biological_entity id="o18023" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="narrowly obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa to 33–66% leaf length, slender, terminal spine absent;</text>
      <biological_entity id="o18024" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <biological_entity id="o18025" name="leaf" name_original="leaf" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="33-66%" name="character" src="d0_s9" value="length" value_original="length" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>alar cells subquadrate to oblate-quadrate in several rows, little different from more distal cells;</text>
      <biological_entity constraint="terminal" id="o18026" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18027" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="in rows" constraintid="o18028" from="subquadrate" name="shape" src="d0_s10" to="oblate-quadrate" />
      </biological_entity>
      <biological_entity id="o18028" name="row" name_original="rows" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18029" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <relation from="o18027" id="r2589" name="from" negation="false" src="d0_s10" to="o18029" />
    </statement>
    <statement id="d0_s11">
      <text>laminal cells rhombic to oblong-rhombic, 1.5–3: 1, walls thin;</text>
      <biological_entity constraint="laminal" id="o18030" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s11" to="oblong-rhombic" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s11" to="3" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o18031" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>proximal cells quadrate to subquadrate, shorter at margins.</text>
      <biological_entity constraint="laminal" id="o18032" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s12" to="oblong-rhombic" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18033" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s12" to="subquadrate" />
        <character constraint="at margins" constraintid="o18034" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o18034" name="margin" name_original="margins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Branch leaves similar.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="branch" id="o18035" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perichaetial leaves erect to flexuose, apex abruptly acuminate.</text>
      <biological_entity constraint="perichaetial" id="o18036" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s15" value="flexuose" value_original="flexuose" />
      </biological_entity>
      <biological_entity id="o18037" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seta reddish, smooth.</text>
      <biological_entity id="o18038" name="seta" name_original="seta" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsule erect, brown, narrowly elliptic to oblong, symmetric;</text>
      <biological_entity id="o18039" name="capsule" name_original="capsule" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s17" to="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>annulus not separating;</text>
      <biological_entity id="o18040" name="annulus" name_original="annulus" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s18" value="separating" value_original="separating" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum long-rostrate;</text>
      <biological_entity id="o18041" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>peristome without apparent hygroscopic movement, modified;</text>
      <biological_entity id="o18042" name="peristome" name_original="peristome" src="d0_s20" type="structure">
        <character is_modifier="false" name="development" notes="" src="d0_s20" value="modified" value_original="modified" />
      </biological_entity>
      <biological_entity id="o18043" name="movement" name_original="movement" src="d0_s20" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s20" value="apparent" value_original="apparent" />
        <character is_modifier="true" name="architecture" src="d0_s20" value="hygroscopic" value_original="hygroscopic" />
      </biological_entity>
      <relation from="o18042" id="r2590" name="without" negation="false" src="d0_s20" to="o18043" />
    </statement>
    <statement id="d0_s21">
      <text>exostome teeth very short, 1/2 endostome length.</text>
      <biological_entity constraint="exostome" id="o18044" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s21" value="short" value_original="short" />
        <character name="quantity" src="d0_s21" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o18045" name="endostome" name_original="endostome" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>Calyptra naked.</text>
      <biological_entity id="o18046" name="calyptra" name_original="calyptra" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s22" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Spores 14–21 µm.</text>
      <biological_entity id="o18047" name="spore" name_original="spores" src="d0_s23" type="structure">
        <character char_type="range_value" from="14" from_unit="um" name="some_measurement" src="d0_s23" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e United States, Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e United States" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Clasmatodon has been variously placed in Fabroniaceae or Myriniaceae, but molecular data strongly suggest a position in Brachytheciaceae as a small, epiphytic, monospecific genus with a reduced peristome.</discussion>
  
</bio:treatment>