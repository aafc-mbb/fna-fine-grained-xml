<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">252</other_info_on_meta>
    <other_info_on_meta type="mention_page">251</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">daltoniaceae</taxon_name>
    <taxon_name authority="Hooker &amp; Taylor" date="unknown" rank="genus">DALTONIA</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Brit.,</publication_title>
      <place_in_publication>80, plates 3 [near upper left], 22 [lower center left &amp; right]. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family daltoniaceae;genus DALTONIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Rev. John Dalton, 1764 – 1843, British botanist and bryologist</other_info_on_name>
    <other_info_on_name type="fna_id">109250</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants glossy.</text>
      <biological_entity id="o16562" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate or suberect, branches short, ascending.</text>
      <biological_entity id="o16563" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
      </biological_entity>
      <biological_entity id="o16564" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, sometimes flexuose-twisted and contorted when dry, flexuose when moist, ovatelanceolate, ligulate to lance-acuminate, symmetric, straight, keeled;</text>
      <biological_entity id="o16565" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="flexuose-twisted" value_original="flexuose-twisted" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="course" src="d0_s2" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="ligulate" value_original="ligulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lance-acuminate" value_original="lance-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire, strongly bordered, cells linear, incrassate, in several rows;</text>
      <biological_entity id="o16566" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s3" value="bordered" value_original="bordered" />
      </biological_entity>
      <biological_entity id="o16567" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s3" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o16568" name="row" name_original="rows" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="several" value_original="several" />
      </biological_entity>
      <relation from="o16567" id="r2389" name="in" negation="false" src="d0_s3" to="o16568" />
    </statement>
    <statement id="d0_s4">
      <text>costa ending before apex in distal 1/4;</text>
      <biological_entity id="o16569" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o16570" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o16571" name="1/4" name_original="1/4" src="d0_s4" type="structure" />
      <relation from="o16569" id="r2390" name="ending before" negation="false" src="d0_s4" to="o16570" />
      <relation from="o16569" id="r2391" name="in" negation="false" src="d0_s4" to="o16571" />
    </statement>
    <statement id="d0_s5">
      <text>laminal cells usually uniformly rhomboidal to elongate-hexagonal, walls usually incrassate;</text>
      <biological_entity constraint="laminal" id="o16572" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually uniformly rhomboidal" name="shape" src="d0_s5" to="elongate-hexagonal" />
      </biological_entity>
      <biological_entity id="o16573" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s5" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal-cells sometimes rounded, longer, smooth.</text>
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous or synoicous.</text>
      <biological_entity id="o16574" name="basal-cell" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta roughened distally [smooth].</text>
      <biological_entity id="o16575" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally" name="relief_or_texture" src="d0_s8" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule erect to subinclined;</text>
      <biological_entity id="o16576" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character constraint="to subinclined" constraintid="o16577" is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o16577" name="subinclined" name_original="subinclined" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>exothecial cells strongly collenchymatous;</text>
      <biological_entity constraint="exothecial" id="o16578" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s10" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exostome teeth hygrocastique (teeth incurved when dry and reflexed when moist), densely papillose, with zigzag longitudinal lines, not striate, not furrowed;</text>
      <biological_entity constraint="exostome" id="o16579" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="hygrocastique" value_original="hygrocastique" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s11" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" notes="" src="d0_s11" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity id="o16580" name="line" name_original="lines" src="d0_s11" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s11" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <relation from="o16579" id="r2392" name="with" negation="false" src="d0_s11" to="o16580" />
    </statement>
    <statement id="d0_s12">
      <text>endostome segments linear, narrow, same length as teeth, perforate along keel.</text>
      <biological_entity constraint="endostome" id="o16581" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
        <character constraint="along keel" constraintid="o16583" is_modifier="false" name="length" notes="" src="d0_s12" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o16582" name="tooth" name_original="teeth" src="d0_s12" type="structure" />
      <biological_entity id="o16583" name="keel" name_original="keel" src="d0_s12" type="structure" />
      <relation from="o16581" id="r2393" name="as" negation="false" src="d0_s12" to="o16582" />
    </statement>
    <statement id="d0_s13">
      <text>Calyptra smooth to somewhat roughened distally, basal fringe hairs dense.</text>
      <biological_entity id="o16584" name="calyptra" name_original="calyptra" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth to somewhat" value_original="smooth to somewhat" />
        <character is_modifier="false" modifier="somewhat; distally" name="relief_or_texture" src="d0_s13" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity constraint="fringe" id="o16585" name="hair" name_original="hairs" src="d0_s13" type="structure" constraint_original="basal fringe">
        <character is_modifier="false" name="density" src="d0_s13" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores papillose.</text>
      <biological_entity id="o16586" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>nw North America, Mexico, West Indies, Central America, South America, Europe, Asia, Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="nw North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 22 (1 in the flora).</discussion>
  <references>
    <reference>Majestyk, P. 2011. A taxonomic treatment of Daltonia (Musci: Daltoniaceae) in the Americas. J. Bot. Res. Inst. Texas 5: 553–575.</reference>
  </references>
  
</bio:treatment>