<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Terry T. McIntosh,Steven G. Newmaster</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">225</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">MNIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>188, plate 45. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus MNIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek mnion, moss</other_info_on_name>
    <other_info_on_name type="fna_id">120886</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (0.5–) 2–4 (–8) cm, in open to compact tufts or mats.</text>
      <biological_entity id="o16874" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16875" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character char_type="range_value" from="open" is_modifier="true" name="architecture" src="d0_s0" to="compact" />
      </biological_entity>
      <biological_entity id="o16876" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o16874" id="r2435" name="in" negation="false" src="d0_s0" to="o16875" />
      <relation from="o16874" id="r2436" name="in" negation="false" src="d0_s0" to="o16876" />
    </statement>
    <statement id="d0_s1">
      <text>Stems red, reddish-brown, or brown, rarely yellowish-brown, erect, simple or branched distally, not dendroid;</text>
      <biological_entity id="o16877" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s1" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids brown, macronemata mainly proximal, micronemata absent.</text>
      <biological_entity id="o16878" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="presence" notes="" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16879" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves green to dark green, sometimes yellowish, often crisped and contorted, sometimes undulate, sometimes spirally twisted when dry, patent, erect-spreading, or spreading, usually ± flat when moist, elliptic, obovate, oblong-ovate, ovate, ovate-elliptic, ovate or elliptic-lanceolate, or obovate-spatulate, 1.5–6.5 mm, proximal leaves much smaller;</text>
      <biological_entity id="o16880" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="dark green" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="patent" value_original="patent" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when moist" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16881" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s3" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base short to long-decurrent;</text>
      <biological_entity id="o16882" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s4" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane, green, reddish-brown, or brown, 1-stratose or 2-stratose, sometimes multistratose with stereid band, toothed distally or to below mid leaf, teeth usually paired and often sharp, sometimes single, small, and blunt, rarely rounded or indistinct;</text>
      <biological_entity id="o16883" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="2-stratose" value_original="2-stratose" />
        <character constraint="with band" constraintid="o16884" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="multistratose" value_original="multistratose" />
        <character constraint="below mid leaf" constraintid="o16885" is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16884" name="band" name_original="band" src="d0_s5" type="structure" />
      <biological_entity id="o16885" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s5" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o16886" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="sharp" value_original="sharp" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="shape" src="d0_s5" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute, obtuse, rarely rounded, or acuminate, apiculate or sometimes cuspidate, cusp often toothed;</text>
      <biological_entity id="o16887" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o16888" name="cusp" name_original="cusp" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa percurrent or excurrent, rarely subpercurrent or ending well before apex, distal abaxial surface smooth or toothed;</text>
      <biological_entity id="o16889" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s7" value="subpercurrent" value_original="subpercurrent" />
        <character name="position" src="d0_s7" value="ending well before apex" value_original="ending well before apex" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o16890" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial laminal cells usually elongate or ± isodiametric, sometimes oblong, (10–) 17–50 µm, sometimes in diagonal or longitudinal rows, collenchymatous or not, walls usually not pitted (pitted in M. arizonicum);</text>
      <biological_entity constraint="medial laminal" id="o16891" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="10" from_unit="um" name="atypical_some_measurement" src="d0_s8" to="17" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="17" from_unit="um" name="some_measurement" src="d0_s8" to="50" to_unit="um" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s8" value="collenchymatous" value_original="collenchymatous" />
        <character name="architecture" src="d0_s8" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o16892" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s8" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o16893" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually not" name="relief" src="d0_s8" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o16891" id="r2437" modifier="sometimes" name="in" negation="false" src="d0_s8" to="o16892" />
    </statement>
    <statement id="d0_s9">
      <text>marginal cells differentiated, linear, short-linear, or rhomboidal, in 1–4 (–5) rows.</text>
      <biological_entity id="o16895" name="row" name_original="rows" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s9" to="5" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <relation from="o16894" id="r2438" name="in" negation="false" src="d0_s9" to="o16895" />
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition synoicous or dioicous.</text>
      <biological_entity constraint="marginal" id="o16894" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta single, rarely double or triple, usually yellowish and becoming red from base with age, 1–5 cm, straight or flexuose.</text>
      <biological_entity id="o16896" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s12" value="flexuose" value_original="flexuose" />
      </biological_entity>
      <biological_entity id="o16897" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="usually" name="coloration" src="d0_s12" value="yellowish" value_original="yellowish" />
        <character is_modifier="true" modifier="becoming" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o16898" name="age" name_original="age" src="d0_s12" type="structure" />
      <relation from="o16896" id="r2439" modifier="rarely" name="double" negation="false" src="d0_s12" to="o16897" />
      <relation from="o16896" id="r2440" modifier="rarely; rarely" name="with" negation="false" src="d0_s12" to="o16898" />
    </statement>
    <statement id="d0_s13">
      <text>Capsule horizontal to pendent, yellow, yellow-brown, or brown, oblong-cylindric, 2–7 mm;</text>
      <biological_entity id="o16899" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s13" to="pendent" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-brown" value_original="yellow-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-brown" value_original="yellow-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>operculum conic-mammillate or conic-rostrate;</text>
      <biological_entity id="o16900" name="operculum" name_original="operculum" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="conic-mammillate" value_original="conic-mammillate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="conic-rostrate" value_original="conic-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome brown, greenish yellow, yellowish-brown, reddish-brown, or purplish;</text>
      <biological_entity id="o16901" name="exostome" name_original="exostome" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish yellow" value_original="greenish yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome yellowish or brown.</text>
      <biological_entity id="o16902" name="endostome" name_original="endostome" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Spores 15–40 µm.</text>
      <biological_entity id="o16903" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s17" to="40" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America (Guatemala), South America, Eurasia, Africa; circumboreal.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="circumboreal" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 19 (9 in the flora).</discussion>
  <discussion>In most cases, Mnium is distinguished from other genera of Mniaceae by the presence of paired teeth along the leaf margins. However, M. blyttii and M. stellare usually have single, indistinct marginal teeth, and leaves may lack teeth altogether. Trachycystis flagellaris also has paired marginal teeth but is readily distinguished by mammillose laminal cells. Depauperate plants of M. arizonicum and M. thomsonii, but also, in a few cases, M. marginatum, have leaves with only a few single, small teeth. Although Mnium is characterized by a wide range in morphological variation related to environmental conditions, plant age, and sexuality (T. J. Koponen 1974), most vigorous collections with well-developed leaves should be readily identified.</discussion>
  <discussion>Blue postmortal color (mnioindigon) is often used to separate Mnium blyttii and M. stellare from other species of Mnium. Although often observed in younger collections when leaves are soaked only in water, it is most readily observed in older specimens if plants are dipped sequentially in alcohol (70–90%), 2% KOH, and water.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Marginal cells in 1-2(-3) rows; margins 1-stratose, rarely 2-stratose in short sections; margins entire or weakly toothed distally, teeth single or rarely paired, small, blunt; blue postmortal color often present in laminal cells</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Marginal cells usually in more than 2 rows; margins 1- to multistratose, toothed often to below mid leaf, teeth usually paired, usually large and sharp, rarely nearly absent; blue postmortal color absent from laminal cells</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves usually more than 2.5 mm; marginal cells linear; plants usually more than 2 cm.</description>
      <determination>2 Mnium blyttii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves usually less than 2.5 mm; marginal cells short-linear or rhomboidal; plants usually less than 2 cm.</description>
      <determination>8 Mnium stellare</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Laminal cell walls pitted; marginal cells short-linear or rhomboidal.</description>
      <determination>1 Mnium arizonicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Laminal cell walls not pitted; marginal cells linear</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves to 6 mm; costa ending well before apex or occasionally percurrent, distal abaxial surface strongly toothed, adaxial surface occasionally toothed; e North America.</description>
      <determination>3 Mnium hornum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves usually to 4(-5) mm; costa percurrent or excurrent, abaxial surface toothed or smooth, adaxial surface smooth (rarely toothed in M. spinosum); mostly widespread</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Medial laminal cells (10-)14-22(-25) µm.</description>
      <determination>9 Mnium thomsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Medial laminal cells mostly more than 25 µm</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves usually obovate; marginal teeth long, sharp, usually restricted to distal 1/2 of leaf; margins multistratose with stereid band; laminal cells not collenchymatous.</description>
      <determination>7 Mnium spinulosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves ovate-elliptic, elliptic, or oblong-ovate, rarely obovate; marginal teeth large or small, sharp or blunt, usually to below mid leaf; margins 2- or multistratose, stereid band usually absent (present in M. spinosum); laminal cells collenchymatous or not</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Laminal cells not or weakly collenchymatous; marginal teeth large, sharp; arctic or alpine.</description>
      <determination>6 Mnium spinosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Laminal cells usually strongly collenchymatous; marginal teeth small to large, sharp or blunt; widespread</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves narrowly elliptic, ovate-elliptic, or ovate-oblong; marginal teeth long, sharp, occasionally short and blunt; costae with distal abaxial surface toothed; sexual condition dioicous.</description>
      <determination>4 Mnium lycopodioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves usually ovate-elliptic; marginal teeth small to large, blunt or occasionally sharp; costae usually smooth, on well-developed leaves distal abaxial surface rarely with few blunt teeth; sexual condition synoicous or rarely dioicous.</description>
      <determination>5 Mnium marginatum</determination>
    </key_statement>
  </key>
</bio:treatment>