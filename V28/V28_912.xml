<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wilfred B. Schofield†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">582</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">sematophyllaceae</taxon_name>
    <taxon_name authority="Spruce ex Mitten" date="unknown" rank="genus">TAXITHELIUM</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>12: [20 – ]21, 496. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sematophyllaceae;genus TAXITHELIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek taxis, order, and thele, nipple, alluding to single row of papillae over cell lumina</other_info_on_name>
    <other_info_on_name type="fna_id">132352</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, yellow-green, somewhat glossy.</text>
      <biological_entity id="o10448" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10 cm, complanate-foliate, irregularly to regularly pinnate.</text>
      <biological_entity id="o10449" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
        <character is_modifier="false" modifier="irregularly to regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading to appressed, narrowly to broadly ovate to ovatelanceolate;</text>
      <biological_entity id="o10450" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="appressed" />
        <character char_type="range_value" from="ovate" modifier="narrowly to broadly; broadly" name="shape" src="d0_s2" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire or weakly to sharply toothed;</text>
      <biological_entity id="o10451" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="entire or" name="shape" src="d0_s3" to="weakly sharply toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex short-acuminate;</text>
      <biological_entity id="o10452" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ecostate or costa double;</text>
    </statement>
    <statement id="d0_s6">
      <text>alar cells rounded-rhomboid, enlarged or weakly inflated, pigmented, walls thin, without proximal row of inflated, elongate cells, middle lamella not apparent, supra-alar cells differentiated;</text>
      <biological_entity id="o10453" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o10454" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-rhomboid" value_original="rounded-rhomboid" />
        <character is_modifier="false" name="size" src="d0_s6" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o10455" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10456" name="row" name_original="row" src="d0_s6" type="structure" />
      <biological_entity id="o10457" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="true" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="middle" id="o10458" name="lamella" name_original="lamella" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="apparent" value_original="apparent" />
      </biological_entity>
      <biological_entity id="o10459" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o10455" id="r1491" name="without" negation="false" src="d0_s6" to="o10456" />
      <relation from="o10456" id="r1492" name="part_of" negation="false" src="d0_s6" to="o10457" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cells rhomboid to linear, 1-seriate multipapillose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="laminal" id="o10460" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rhomboid to linear" value_original="rhomboid to linear" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="1-seriate" value_original="1-seriate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta 1.5 cm.</text>
      <biological_entity id="o10461" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="cm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule inclined to horizontal, ovoid;</text>
      <biological_entity id="o10462" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s10" to="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exothecial cell-walls not or weakly collenchymatous;</text>
      <biological_entity constraint="exothecial" id="o10463" name="cell-wall" name_original="cell-walls" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s11" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic, short or long-rostrate.</text>
      <biological_entity id="o10464" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s12" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, West Indies, Central America, South America, Asia, Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 40 (1 in the flora).</discussion>
  <discussion>Taxithelium occurs mainly in forests, generally as corticolous on tree trunks and branches, but also on logs, occasionally on rock and soil.</discussion>
  <references>
    <reference>Dumanhuri, A. and R. E. Longton. 1996. Towards a revision of the moss genus Taxithelium (Sematophyllaceae). Anales Inst. Biol. Univ. Nac. Autón. México, Bot. 67: 35–58.</reference>
  </references>
  
</bio:treatment>