<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">177</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">rosulabryum</taxon_name>
    <taxon_name authority="(H. A. Crum) J. R. Spence" date="2009" rank="species">andersonii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>19: 398. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus rosulabryum;species andersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099347</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachymenium</taxon_name>
    <taxon_name authority="H. A. Crum" date="unknown" rank="species">andersonii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>74: 47. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus brachymenium;species andersonii</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, in tall turfs, bright green.</text>
      <biological_entity id="o23383" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o23384" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="height" src="d0_s0" value="tall" value_original="tall" />
      </biological_entity>
      <relation from="o23383" id="r3320" name="in" negation="false" src="d0_s0" to="o23384" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–6 cm, elongate, evenly foliate, innovations elongate, evenly foliate.</text>
      <biological_entity id="o23385" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="6" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
      <biological_entity id="o23386" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves of main-stem and innovations similar, spirally twisted when dry, erect-spreading when moist, spathulate, flat to weakly concave, 3–4.5 mm;</text>
      <biological_entity id="o23387" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spathulate" value_original="spathulate" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s2" to="weakly concave" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23388" name="main-stem" name_original="main-stem" src="d0_s2" type="structure" />
      <biological_entity id="o23389" name="innovation" name_original="innovations" src="d0_s2" type="structure" />
      <relation from="o23387" id="r3321" name="part_of" negation="false" src="d0_s2" to="o23388" />
      <relation from="o23387" id="r3322" name="part_of" negation="false" src="d0_s2" to="o23389" />
    </statement>
    <statement id="d0_s3">
      <text>base somewhat decurrent;</text>
      <biological_entity id="o23390" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins recurved proximally, plane distally, sharply serrate from near mid leaf to apex, limbidium strong, of 2 or 3 rows of cells;</text>
      <biological_entity id="o23391" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="from mid leaf" constraintid="o23392" is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="mid" id="o23392" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <biological_entity id="o23393" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o23394" name="limbidium" name_original="limbidium" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="strong" value_original="strong" />
      </biological_entity>
      <biological_entity id="o23395" name="cell" name_original="cells" src="d0_s4" type="structure" />
      <relation from="o23392" id="r3323" name="to" negation="false" src="d0_s4" to="o23393" />
      <relation from="o23394" id="r3324" modifier="of 2 or 3rows" name="part_of" negation="false" src="d0_s4" to="o23395" />
    </statement>
    <statement id="d0_s5">
      <text>apex broadly acute;</text>
      <biological_entity id="o23396" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent, awn short;</text>
      <biological_entity id="o23397" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o23398" name="awn" name_original="awn" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal laminal cells rectangular;</text>
      <biological_entity constraint="proximal laminal" id="o23399" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>medial and distal cells rhomboidal, 15–18 µm wide, 3: 1, walls thin, not porose.</text>
      <biological_entity constraint="medial and distal" id="o23400" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s8" to="18" to_unit="um" />
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition probably dioicous.</text>
      <biological_entity id="o23401" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="porose" value_original="porose" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" modifier="probably" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule unknown.</text>
      <biological_entity id="o23402" name="capsule" name_original="capsule" src="d0_s11" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist humic soil in broad-leaved forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humic soil" modifier="moist" constraint="in broad-leaved forests" />
        <character name="habitat" value="broad-leaved forests" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (1300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="1300" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Rosulabryum andersonii was transferred from Brachymenium because it does not fit in the type section, which consists of tropical epiphytes with erect capsules. Rosulabryum andersonii is morphologically similar to several robust tropical species of Rosulabryum that have elongate, evenly foliate stems. Despite repeated searches, the population has never been relocated, and the species may be extinct. A specimen reportedly collected in 1988 from Mexico needs to be evaluated.</discussion>
  
</bio:treatment>