<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">337</other_info_on_meta>
    <other_info_on_meta type="mention_page">643</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">hylocomiaceae</taxon_name>
    <taxon_name authority="Schwagrichen" date="unknown" rank="genus">LEPTOHYMENIUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>3(1,2): sub plate 246, fig. c. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hylocomiaceae;genus LEPTOHYMENIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leptos, thin, and hymen, membrane, alluding to endostomial basal membrane</other_info_on_name>
    <other_info_on_name type="fna_id">118192</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, 1 mm wide across leafy stem, monopodial or sympodial, freely and irregularly branched;</text>
      <biological_entity id="o10035" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character constraint="across stem" constraintid="o10036" name="width" src="d0_s0" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="monopodial" value_original="monopodial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="sympodial" value_original="sympodial" />
        <character is_modifier="false" modifier="freely; irregularly" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o10036" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>paraphyllia absent.</text>
      <biological_entity id="o10037" name="paraphyllium" name_original="paraphyllia" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves loosely erect, heteromallous, not falcate-secund, not crowded, ovate to oblong-lanceolate, sometimes plicate, not rugose, 0.7–1.6 mm;</text>
      <biological_entity id="o10038" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s2" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="oblong-lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="distance" src="d0_s2" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base narrowly decurrent;</text>
      <biological_entity id="o10039" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins nearly entire proximally, serrulate distally;</text>
      <biological_entity id="o10040" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly; proximally" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o10041" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa double, 1/4–1/2 leaf length;</text>
      <biological_entity id="o10042" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character char_type="range_value" from="1/4" name="quantity" src="d0_s6" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells differentiated;</text>
      <biological_entity id="o10043" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o10044" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminal cells scattered-prorulate.</text>
      <biological_entity constraint="laminal" id="o10045" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="scattered-prorulate" value_original="scattered-prorulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovate to elliptic-lanceolate.</text>
    </statement>
    <statement id="d0_s10">
      <text>[Capsule erect; operculum long-conic or obliquely rostrate; exostome teeth smooth proximally; endostome segments not perforate].</text>
      <biological_entity constraint="branch" id="o10046" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="elliptic-lanceolate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, Mexico, se Asia; montane in warm temperate to tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="se Asia" establishment_means="native" />
        <character name="distribution" value="montane in warm temperate to tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (1 in the flora).</discussion>
  <discussion>Generic concepts of Leptohymenium have varied greatly, but a constant critical character has been the erect, elongate capsules. The genus is circumscribed here in the restricted sense of V. F. Brotherus (1908, 1925) and J. R. Rohrer (1985).</discussion>
  
</bio:treatment>