<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lars Hedenäs</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">384</other_info_on_meta>
    <other_info_on_meta type="mention_page">13</other_info_on_meta>
    <other_info_on_meta type="mention_page">294</other_info_on_meta>
    <other_info_on_meta type="mention_page">300</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Vanderpoorten" date="unknown" rank="family">CALLIERGONACEAE</taxon_name>
    <taxon_hierarchy>family CALLIERGONACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">20920</other_info_on_name>
  </taxon_identification>
  <number>61.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to very large, green, yellowish, brownish, or sometimes red.</text>
      <biological_entity id="o3429" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized to very" value_original="medium-sized to very" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems distichously or radially branched, sometimes nearly simple;</text>
      <biological_entity id="o3430" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes nearly" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis present or absent, inner cortical cells small, walls thin (incrassate in Loeskypnum);</text>
      <biological_entity id="o3431" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="inner cortical" id="o3432" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o3433" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia absent;</text>
      <biological_entity id="o3434" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials on stem at or just before leaf insertions or from various points on stems and leaves, rhizoids redbrown, slightly branched, smooth;</text>
      <biological_entity id="o3435" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure" />
      <biological_entity constraint="rhizoid" id="o3436" name="initial" name_original="initials" src="d0_s4" type="structure" />
      <biological_entity id="o3437" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o3438" name="point" name_original="points" src="d0_s4" type="structure">
        <character is_modifier="true" name="variability" src="d0_s4" value="various" value_original="various" />
      </biological_entity>
      <biological_entity id="o3439" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <relation from="o3435" id="r442" name="on" negation="false" src="d0_s4" to="o3437" />
      <relation from="o3436" id="r443" name="on" negation="false" src="d0_s4" to="o3437" />
      <relation from="o3435" id="r444" modifier="just" name="before leaf insertions or from" negation="false" src="d0_s4" to="o3438" />
      <relation from="o3436" id="r445" modifier="just" name="before leaf insertions or from" negation="false" src="d0_s4" to="o3438" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hairs usually well developed.</text>
      <biological_entity constraint="axillary" id="o3440" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s5" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves straight or falcate, or from straight base suddenly curved, plicate or not;</text>
      <biological_entity id="o3441" name="stem-leaf" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" notes="" src="d0_s6" value="plicate" value_original="plicate" />
        <character name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o3442" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="true" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="suddenly" name="course" src="d0_s6" value="curved" value_original="curved" />
      </biological_entity>
      <relation from="o3441" id="r446" name="from" negation="false" src="d0_s6" to="o3442" />
    </statement>
    <statement id="d0_s7">
      <text>costa single, usually long, or double and usually short;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells differentiated or not, often inflated;</text>
      <biological_entity id="o3443" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="single" value_original="single" />
        <character is_modifier="false" modifier="usually" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o3444" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
        <character name="variability" src="d0_s8" value="not" value_original="not" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s8" value="inflated" value_original="inflated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells linear or short-linear, 1-stratose, smooth.</text>
      <biological_entity constraint="medial laminal" id="o3445" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="short-linear" value_original="short-linear" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Perichaetia with inner leaves erect, straight, lanceolate, ovate, oblong, or slightly obovate, plicate or not, costa single, usually well developed;</text>
      <biological_entity id="o3446" name="perichaetium" name_original="perichaetia" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" notes="" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s10" value="plicate" value_original="plicate" />
        <character name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s10" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="inner" id="o3447" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o3448" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" modifier="usually well" name="development" src="d0_s10" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o3446" id="r447" name="with" negation="false" src="d0_s10" to="o3447" />
    </statement>
    <statement id="d0_s11">
      <text>vaginulae with paraphyses or naked.</text>
      <biological_entity id="o3449" name="vaginula" name_original="vaginulae" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o3450" name="paraphyse" name_original="paraphyses" src="d0_s11" type="structure" />
      <relation from="o3449" id="r448" name="with" negation="false" src="d0_s11" to="o3450" />
    </statement>
    <statement id="d0_s12">
      <text>Seta long, smooth.</text>
      <biological_entity id="o3451" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule ± horizontal (occasionally inclined in Loeskypnum), cylindric, curved;</text>
      <biological_entity id="o3452" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata long-pored;</text>
      <biological_entity id="o3453" name="stoma" name_original="stomata" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>annulus separating or not;</text>
      <biological_entity id="o3454" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="separating" value_original="separating" />
        <character name="arrangement" src="d0_s15" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum conic;</text>
      <biological_entity id="o3455" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>peristome perfect;</text>
      <biological_entity id="o3456" name="peristome" name_original="peristome" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="perfect" value_original="perfect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>exostome yellowbrown or brownish, external surface ± reticulate proximally, rarely entirely cross-striolate, papillose distally, border ± widened at transitional zone in outer peristomial layer pattern, margins dentate or slightly so;</text>
      <biological_entity id="o3457" name="exostome" name_original="exostome" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="external" id="o3458" name="surface" name_original="surface" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="more or less; proximally" name="architecture_or_coloration_or_relief" src="d0_s18" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="rarely entirely" name="pubescence" src="d0_s18" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o3459" name="border" name_original="border" src="d0_s18" type="structure">
        <character constraint="at zone" constraintid="o3460" is_modifier="false" modifier="more or less" name="width" src="d0_s18" value="widened" value_original="widened" />
      </biological_entity>
      <biological_entity id="o3460" name="zone" name_original="zone" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="transitional" value_original="transitional" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3461" name="peristomial" name_original="peristomial" src="d0_s18" type="structure" />
      <biological_entity constraint="outer" id="o3462" name="layer" name_original="layer" src="d0_s18" type="structure" />
      <biological_entity id="o3463" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="dentate" value_original="dentate" />
        <character name="architecture_or_shape" src="d0_s18" value="slightly" value_original="slightly" />
      </biological_entity>
      <relation from="o3460" id="r449" name="in" negation="false" src="d0_s18" to="o3461" />
      <relation from="o3460" id="r450" name="in" negation="false" src="d0_s18" to="o3462" />
    </statement>
    <statement id="d0_s19">
      <text>endostome basal membrane high, segments long, not or narrowly perforate, cilia nodose.</text>
      <biological_entity id="o3464" name="endostome" name_original="endostome" src="d0_s19" type="structure" />
      <biological_entity constraint="basal" id="o3465" name="membrane" name_original="membrane" src="d0_s19" type="structure">
        <character is_modifier="false" name="height" src="d0_s19" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o3466" name="segment" name_original="segments" src="d0_s19" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s19" value="long" value_original="long" />
        <character is_modifier="false" modifier="not; narrowly" name="architecture" src="d0_s19" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o3467" name="cilium" name_original="cilia" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Calyptra cucullate, smooth.</text>
      <biological_entity id="o3468" name="calyptra" name_original="calyptra" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s20" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; temperate to subpolar regions and tropical mountains.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="temperate to subpolar regions and tropical mountains" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Genera 7, species ca. 22 (7 genera, 20 species in the flora).</discussion>
  <discussion>Genera of Calliergonaceae are fairly well circumscribed based on morphology and molecular data, and are well differentiated from Amblystegiaceae. All species, except sometimes Scorpidium revolvens and S. scorpioides, have a reticulate proximal external surface of the exostome, whereas all members here treated in Amblystegiaceae, except Conardia and Tomentypnum, are cross striolate on the outer basal exostome. Many Calliergonaceae species become translucent red when growing in habitats exposed to sunlight; this is never the case among Amblystegiaceae.</discussion>
  <discussion>Morphological features further characterize the genera Calliergon, Loeskypnum, Sarmentypnum, Straminergon, and Warnstorfia. When shoots grow erect, these are radially rather than distichously branched, except in species that are sparsely branched. Rhizoid initials, and sometimes rhizoids, are frequently found in various parts of the leaf lamina, especially close to the apex. Leaf-borne rhizoid initial cells are easily recognized in being slightly wider than the surrounding laminal cells and lacking the pigments of the latter. Among Amblystegiaceae genera, rhizoid initials and rhizoids are found in leaves only in Conardia, in which they often occur both close to apices and on the abaxial costa, and in Tomentypnum, in which they are found only on the abaxial surface of the costa.</discussion>
  <references>
    <reference>Hedenäs, L. 1993. A generic revision of the Warnstorfia-Calliergon group. J. Bryol. 17: 447–479.</reference>
    <reference>Hedenäs, L. 2006. Additional insights into the phylogeny of Calliergon, Loeskypnum, Straminergon, and Warnstorfia (Bryophyta: Calliergonaceae). J. Hattori Bot. Lab. 100: 125–134.</reference>
    <reference>Hedenäs, L., G. Oliván and P. K. Eldenäs. 2005. Phylogeny of the Calliergonaceae (Bryophyta) based on molecular and morphological data. Pl. Syst. Evol. 252: 49–61.</reference>
    <reference>Hedenäs, L. and A. Vanderpoorten. 2006. The Amblystegiaceae and Calliergonaceae. In: A. E. Newton and R. Tangney, eds. 2006. Pleurocarpous Mosses: Systematics and Evolution. Boca Raton. Pp. 163–176.</reference>
    <reference>Janssens, J. A. 1983. Past and extant distribution of Drepanocladus in North America, with notes on the differentiation of fossil fragments. J. Hattori Bot. Lab. 54: 251–298.</reference>
    <reference>Wynne, F. E. 1944b. Studies in Drepanocladus. IV. Taxonomy. Bryologist 47: 147–189.</reference>
    <reference>Wynne, F. E. 1945. Studies in Calliergon and related genera. Bryologist 48: 131–155.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with hyalodermis present or partial</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with hyalodermis absent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Alar cells few, strongly inflated, regions small, from margins at most 50% distance to costa; rhizoid initials on stem, at or just before leaf insertions.</description>
      <determination>2 Scorpidium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Alar cells many, inflated or strongly inflated, regions large, from margins ± to costa; rhizoid initials frequently present in leaves, especially near apex.</description>
      <determination>7 Sarmentypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem central strands absent; stem leaves usually plicate; alar cells not differentiated.</description>
      <determination>1 Hamatocaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem central strands present; stem leaves not or indistinctly plicate; alar cells differentiated</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem leaf alar cells slightly inflated, walls incrassate, regions indistinctly delimited; axillary hairs well developed, many, distal cells yellow-brown or brownish when young; plants often yellow-brown or copper brown, never red.</description>
      <determination>4 Loeskypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stem leaf alar cells inflated or strongly so, walls thin or sometimes incrassate, regions distinctly or indistinctly delimited; axillary hairs well developed and many, or not well developed and few, distal cells hyaline (brownish in Sarmentypnum trichophyllum); plants variously colored, sometimes red</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaves straight or falcate, triangular to ovate or narrowly ovate, gradually narrowed to apex; apices acuminate or obtuse</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaves ± straight, ovate, broadly ovate, or rounded-triangular, ± abruptly narrowed to apex; apices rounded, rounded-obtuse, or rounded-apiculate</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sexual condition autoicous; translucent red pigment extremely rare, when present usually overlain by brown pigment, appearing reddish brown; outer pseudoparaphyllia narrow; alar regions narrowly transversely triangular, from margins possibly to costa, or regions almost quadrate, with supra-alar cells often in ovate group along basal margin, region somewhat indistinctly delimited.</description>
      <determination>5 Warnstorfia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sexual condition dioicous; translucent red pigment often present; outer pseudoparaphyllia broad; alar regions transversely triangular, from margins ± to costa, distinctly delimited.</description>
      <determination>7 Sarmentypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stem leaves broadly ovate to broadly rounded-triangular; apices rounded or obtuse; axillary hair distal cells 2-8(-10), hairs well developed, many.</description>
      <determination>3 Calliergon</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stem leaves ovate, narrowly ovate, or triangular; apices short rounded-apiculate or rounded; axillary hair distal cells 1-2(-7), hairs not well developed, usually few</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stem leaf apices rounded; alar regions ovate or broadly ovate; stems sparsely branched; plants pale or whitish green to yellow-green.</description>
      <determination>6 Straminergon</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stem leaf apices usually rounded-apiculate, rarely rounded; alar regions transversely triangular; stems sparsely to densely branched; plants deep green to clear red.</description>
      <determination>7 Sarmentypnum</determination>
    </key_statement>
  </key>
</bio:treatment>