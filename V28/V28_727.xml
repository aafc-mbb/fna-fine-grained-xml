<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">scleropodium</taxon_name>
    <taxon_name authority="(Bridel) L. F. Koch" date="unknown" rank="species">touretii</taxon_name>
    <taxon_name authority="(Sullivant) E. Lawton ex H. A. Crum" date="1969" rank="variety">colpophyllum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>72: 245. 1969</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus scleropodium;species touretii;variety colpophyllum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099375</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eurhynchium</taxon_name>
    <taxon_name authority="(Sullivant) Grout" date="unknown" rank="species">colpophyllum</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Musc., suppl.,</publication_title>
      <place_in_publication>95, plate 71. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus eurhynchium;species colpophyllum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Scleropodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">colpophyllum</taxon_name>
    <taxon_hierarchy>genus scleropodium;species colpophyllum</taxon_hierarchy>
  </taxon_identification>
  <number>4b.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems with leafy shoots 0.5–1 mm wide, branches sometimes julaceous, loosely foliate, straight to curved.</text>
      <biological_entity id="o21096" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o21097" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21098" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s0" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s0" value="foliate" value_original="foliate" />
        <character char_type="range_value" from="straight" name="course" src="d0_s0" to="curved" />
      </biological_entity>
      <relation from="o21096" id="r3029" name="with" negation="false" src="d0_s0" to="o21097" />
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves moderately concave, 0.9–1.6 × 0.4–0.9 mm.</text>
      <biological_entity id="o21099" name="stem-leaf" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s1" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s1" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s1" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil banks, soil over rock, concrete, rotten logs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil banks" />
        <character name="habitat" value="soil" constraint="over rock , concrete , rotten" />
        <character name="habitat" value="rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (300-1000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; Mexico (Baja California, Durango).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">tourettei</other_name>
  
</bio:treatment>