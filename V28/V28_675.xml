<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Hooker &amp; Wilson" date="unknown" rank="genus">clasmatodon</taxon_name>
    <taxon_name authority="(Hampe) Sullivant in A. Gray" date="1856" rank="species">parvulus</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 660. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus clasmatodon;species parvulus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099070</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leskea</taxon_name>
    <taxon_name authority="Hampe" date="unknown" rank="species">parvula</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>13: 46. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus leskea;species parvula</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Clasmatodon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parvulus</taxon_name>
    <taxon_name authority="Lesquereux &amp; James" date="unknown" rank="variety">rupestris</taxon_name>
    <taxon_hierarchy>genus clasmatodon;species parvulus;variety rupestris</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants slender, in interwoven mats.</text>
      <biological_entity id="o22123" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22124" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s0" value="interwoven" value_original="interwoven" />
      </biological_entity>
      <relation from="o22123" id="r3161" name="in" negation="false" src="d0_s0" to="o22124" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 1 (–2) cm, branches to 2 (–5) mm, erect;</text>
      <biological_entity id="o22125" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22126" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>axillary hair distal cell obtuse, pale-brown.</text>
      <biological_entity constraint="axillary" id="o22127" name="hair" name_original="hair" src="d0_s2" type="structure" />
      <biological_entity constraint="distal" id="o22128" name="cell" name_original="cell" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves ± subsecund, 0.3–0.5 mm wide;</text>
      <biological_entity id="o22129" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s3" value="subsecund" value_original="subsecund" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>laminal cells 18–30 × 8–10 µm, smooth.</text>
      <biological_entity constraint="laminal" id="o22130" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="length" src="d0_s4" to="30" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s4" to="10" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Seta 0.2–0.5 cm.</text>
      <biological_entity id="o22131" name="seta" name_original="seta" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s5" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Capsule 0.7–1 mm;</text>
      <biological_entity id="o22132" name="capsule" name_original="capsule" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>annulus 3-seriate or 4-seriate, cells small, brown;</text>
      <biological_entity id="o22133" name="annulus" name_original="annulus" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="3-seriate" value_original="3-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="4-seriate" value_original="4-seriate" />
      </biological_entity>
      <biological_entity id="o22134" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>exostome teeth blunt, partially connate;</text>
      <biological_entity constraint="exostome" id="o22135" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s8" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>endostome basal membrane inconspicuous, segments 16, short, slender, unequally 2-fid, partially fused to basal membrane.</text>
      <biological_entity id="o22136" name="endostome" name_original="endostome" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o22137" name="membrane" name_original="membrane" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o22138" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="16" value_original="16" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="unequally" name="shape" src="d0_s9" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="partially" name="fusion" src="d0_s9" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22139" name="membrane" name_original="membrane" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spores finely roughened.</text>
      <biological_entity id="o22140" name="spore" name_original="spores" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="finely" name="relief_or_texture" src="d0_s10" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks and bases, flood plain forests, calcareous rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="bases" />
        <character name="habitat" value="flood plain forests" />
        <character name="habitat" value="rock" modifier="calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., Ill., Ind., Ky., La., Md., Miss., Mo., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The inconspicuous plants of Clasmatodon parvulus form small, slender, straggly mats on tree trunks in southeastern North America north of Mexico. The species is characterized by a short costa (Leskea species have a strong costa ending near the apex), ovate, mostly bluntly acute leaves (Lindbergia has long-acuminate leaves), and a lack of erect, flagelliform branchlets (as in Platygyrium repens). The elliptic, erect capsules with a reduced peristome on short setae also are diagnostic.</discussion>
  
</bio:treatment>