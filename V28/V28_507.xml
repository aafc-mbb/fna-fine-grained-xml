<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="mention_page">643</other_info_on_meta>
    <other_info_on_meta type="mention_page">648</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="M. Fleischer" date="unknown" rank="family">hylocomiaceae</taxon_name>
    <taxon_name authority="M. Fleischer ex Brotherus in H. G. A. Engler et al." date="unknown" rank="genus">HYLOCOMIASTRUM</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler et al., Nat. Pflanzenfam. ed.</publication_title>
      <place_in_publication>2, 11: 486. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hylocomiaceae;genus HYLOCOMIASTRUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Hylocomium and Latin - astrum, incomplete resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">116059</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping, 1–3 mm wide across leafy stem, sympodial, remotely and irregularly pinnate to regularly 1-pinnate or 2-pinnate;</text>
      <biological_entity id="o8529" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character char_type="range_value" constraint="across stem" constraintid="o8530" from="1" from_unit="mm" name="width" src="d0_s0" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="sympodial" value_original="sympodial" />
        <character char_type="range_value" from="irregularly pinnate" modifier="remotely" name="architecture_or_shape" src="d0_s0" to="regularly 1-pinnate" />
        <character is_modifier="false" modifier="remotely" name="architecture_or_shape" src="d0_s0" value="2-pinnate" value_original="2-pinnate" />
      </biological_entity>
      <biological_entity id="o8530" name="stem" name_original="stem" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>paraphyllia many, base multiseriate, branches 1-seriate or 2-seriate.</text>
      <biological_entity id="o8531" name="paraphyllium" name_original="paraphyllia" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o8532" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="multiseriate" value_original="multiseriate" />
      </biological_entity>
      <biological_entity id="o8533" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="1-seriate" value_original="1-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect to spreading, heteromallous, often remote, broadly ovate, ovate-deltoid, or ovate, strongly plicate (often obscuring costa), not rugose, 0.8–2.5 mm;</text>
      <biological_entity id="o8534" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="spreading" />
        <character is_modifier="false" modifier="often" name="arrangement_or_density" src="d0_s2" value="remote" value_original="remote" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate-deltoid" value_original="ovate-deltoid" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="distance" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base not cordate-clasping, not to somewhat decurrent;</text>
      <biological_entity id="o8535" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="cordate-clasping" value_original="cordate-clasping" />
        <character is_modifier="false" modifier="not to somewhat" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins serrulate to nearly entire basally, spinose-serrate to serrate in distal 2/3;</text>
      <biological_entity id="o8536" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="serrulate" modifier="basally" name="architecture_or_shape" src="d0_s4" to="nearly entire" />
        <character constraint="in distal 2/3" constraintid="o8537" is_modifier="false" name="shape_or_architecture" src="d0_s4" value="spinose-serrate to serrate" value_original="spinose-serrate to serrate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8537" name="2/3" name_original="2/3" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o8538" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa single, double, or rarely triple, sometimes 2-fid, 1/3–3/4 leaf length;</text>
      <biological_entity id="o8539" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" modifier="rarely; sometimes" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s6" to="3/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells not differentiated;</text>
      <biological_entity id="o8540" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o8541" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s7" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminal cells smooth.</text>
      <biological_entity constraint="laminal" id="o8542" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves ovate to lanceolate;</text>
      <biological_entity constraint="branch" id="o8543" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa single, double, or 2-fid, 1/2–3/4 leaf length.</text>
      <biological_entity id="o8544" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s10" value="2-fid" value_original="2-fid" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s10" to="3/4" />
      </biological_entity>
      <biological_entity id="o8545" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Capsule inclined to horizontal;</text>
      <biological_entity id="o8546" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s11" to="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic;</text>
      <biological_entity id="o8547" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome teeth irregularly cross-striolate to somewhat reticulate proximally;</text>
      <biological_entity constraint="exostome" id="o8548" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="irregularly" name="pubescence" src="d0_s13" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="somewhat; proximally" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>endostome segments narrowly perforate.</text>
      <biological_entity constraint="endostome" id="o8549" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s14" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, n Africa; cool temperate and boreal regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="cool temperate and boreal regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Proximal stem and branch leaves of Hylocomiastrum are distinctly shorter than the more distal and have nearly entire margins, acute to bluntly apiculate apices, and weaker costae. Hylocomiastrum differs from Hylocomium, with which it is often combined, in having shorter-branched paraphyllia, strongly plicate leaves, strong costae that are often single and end in a spine, smooth laminal cells, endostome segments with narrow perforations, and conic-apiculate opercula. The third species in the genus, Hylocomiastrum himalayanum (Mitten) Brotherus, is known from Asia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves erect to erect-spreading, loosely imbricate, ovate to elliptic-ovate, usually longer than 1.7 mm; branch leaves longer than 1.2 mm; costae single.</description>
      <determination>1 Hylocomiastrum pyrenaicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves erect-spreading to wide-spreading, distant, broadly ovate to deltoid, usually shorter than 2 mm; branch leaves shorter than 1.2 mm; costae usually double in stem leaves, single in branch leaves, but variable.</description>
      <determination>2 Hylocomiastrum umbratum</determination>
    </key_statement>
  </key>
</bio:treatment>