<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">212</other_info_on_meta>
    <other_info_on_meta type="mention_page">196</other_info_on_meta>
    <other_info_on_meta type="mention_page">209</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">mielichhoferiaceae</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="genus">pohlia</taxon_name>
    <taxon_name authority="(Limpricht) H. Lindberg" date="1899" rank="species">vexans</taxon_name>
    <place_of_publication>
      <publication_title>Acta Soc. Fauna Fl. Fenn.</publication_title>
      <place_in_publication>16(5): 20. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mielichhoferiaceae;genus pohlia;species vexans</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099275</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mniobryum</taxon_name>
    <taxon_name authority="Limpricht" date="unknown" rank="species">vexans</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Deutschl.</publication_title>
      <place_in_publication>2: 273. 1892</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mniobryum;species vexans</taxon_hierarchy>
  </taxon_identification>
  <number>31.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, green to reddish, very glossy.</text>
      <biological_entity id="o1294" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="reddish" />
        <character is_modifier="false" modifier="very" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1.5 cm, cherry red.</text>
      <biological_entity id="o1295" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="cherry red" value_original="cherry red" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves stiffly erect and ± imbricate to erect-spreading, lanceolate, 0.6–1.3 mm;</text>
      <biological_entity id="o1296" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s2" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base not or scarcely decurrent;</text>
      <biological_entity id="o1297" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins weakly serrulate in distal 1/3;</text>
      <biological_entity id="o1298" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="in distal 1/3" constraintid="o1299" is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o1299" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>costa ending well before apex;</text>
      <biological_entity id="o1300" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o1301" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o1300" id="r169" name="ending" negation="false" src="d0_s5" to="o1301" />
    </statement>
    <statement id="d0_s6">
      <text>distal medial laminal cells broadly rhomboidal, 65–110 µm, walls thin.</text>
      <biological_entity constraint="distal medial laminal" id="o1302" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="rhomboidal" value_original="rhomboidal" />
        <character char_type="range_value" from="65" from_unit="um" name="some_measurement" src="d0_s6" to="110" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o1303" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s7" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perigonial leaves to 3 mm;</text>
      <biological_entity constraint="perigonial" id="o1304" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves somewhat differentiated, lanceolate.</text>
      <biological_entity constraint="perichaetial" id="o1305" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta orangebrown.</text>
      <biological_entity id="o1306" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="orangebrown" value_original="orangebrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule inclined ± 180°, brown to redbrown, sometimes stramineous, short-pyriform to urceolate, neck less than 1/3 urn length;</text>
      <biological_entity id="o1307" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="180°" name="orientation" src="d0_s12" value="inclined" value_original="inclined" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s12" to="redbrown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character char_type="range_value" from="short-pyriform" name="shape" src="d0_s12" to="urceolate" />
      </biological_entity>
      <biological_entity id="o1308" name="neck" name_original="neck" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s12" to="1/3" />
      </biological_entity>
      <biological_entity id="o1309" name="urn" name_original="urn" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>exothecial cells isodiametric, walls sinuate;</text>
      <biological_entity constraint="exothecial" id="o1310" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o1311" name="wall" name_original="walls" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stomata immersed;</text>
      <biological_entity id="o1312" name="stoma" name_original="stomata" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>annulus absent;</text>
      <biological_entity id="o1313" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum short to long-conic;</text>
      <biological_entity id="o1314" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s16" value="long-conic" value_original="long-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome teeth yellow to light-brown, triangular-acute;</text>
      <biological_entity constraint="exostome" id="o1315" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s17" to="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s17" value="triangular-acute" value_original="triangular-acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome yellow to yellowbrown, basal membrane 1/2 exostome length or slightly longer, segments tapered apically, distinctly keeled, broadly perforate, cilia long, nodulose.</text>
      <biological_entity id="o1316" name="endostome" name_original="endostome" src="d0_s18" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s18" to="yellowbrown" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1317" name="membrane" name_original="membrane" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1318" name="exostome" name_original="exostome" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="length" src="d0_s18" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1319" name="segment" name_original="segments" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s18" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="distinctly" name="shape" src="d0_s18" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s18" value="perforate" value_original="perforate" />
      </biological_entity>
      <biological_entity id="o1320" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s18" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s18" value="nodulose" value_original="nodulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 15–21 µm, finely roughened.</text>
      <biological_entity id="o1321" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="15" from_unit="um" name="some_measurement" src="d0_s19" to="21" to_unit="um" />
        <character is_modifier="false" modifier="finely" name="relief_or_texture" src="d0_s19" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring (Apr–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" constraint="Apr-Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed clay or rarely sandy soil, path banks, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="clay" modifier="disturbed" />
        <character name="habitat" value="sandy soil" modifier="rarely" />
        <character name="habitat" value="path banks" />
        <character name="habitat" value="streams" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T.; Alaska, Mont., Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pohlia vexans is a slender species characterized by abundant, elongate sterile shoots with erect, rather glossy leaves. The exostome teeth are pale brown. The species is common on moist calcareous clays in cold continental regions of northwestern North America.</discussion>
  
</bio:treatment>