<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="(Lesquereux) A. Jaeger" date="1878" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Ber. Thätigk. St. Gallischen Naturwiss. Ges.</publication_title>
      <place_in_publication>1876 – 1877: 324. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species bolanderi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099020</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Lesquereux" date="unknown" rank="species">bolanderi</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>13: 12. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species bolanderi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryhnia</taxon_name>
    <taxon_name authority="(Lesquereux) Brotherus" date="unknown" rank="species">bolanderi</taxon_name>
    <taxon_hierarchy>genus bryhnia;species bolanderi</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, in loose to moderately dense mats, yellow-green to brownish or dark green.</text>
      <biological_entity id="o24651" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="yellow-green" name="coloration" notes="" src="d0_s0" to="brownish or dark green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24652" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o24651" id="r3495" name="in" negation="false" src="d0_s0" to="o24652" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 4 cm, creeping, terete-foliate, irregularly pinnate, branches to 5 mm, straight, terete-foliate.</text>
      <biological_entity id="o24653" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o24654" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect-spreading, loosely imbricate to distant, ovate to ovatelanceolate, broadest at 1/10–1/6 leaf length, slightly concave, not or slightly plicate, 0.6–1.4 × 0.4–0.6 mm;</text>
      <biological_entity id="o24655" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect-spreading" value_original="erect-spreading" />
        <character char_type="range_value" from="loosely imbricate" name="arrangement" src="d0_s2" to="distant" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character is_modifier="false" name="length" src="d0_s2" value="broadest" value_original="broadest" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not; slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s2" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base rounded, narrowly short-decurrent;</text>
      <biological_entity id="o24656" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or rarely recurved in places, serrulate throughout;</text>
      <biological_entity id="o24657" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="in places" constraintid="o24658" is_modifier="false" modifier="rarely" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_shape" notes="" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o24658" name="place" name_original="places" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex gradually tapered or short-acuminate;</text>
      <biological_entity id="o24659" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="shape" src="d0_s5" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 60–75% leaf length, strong, terminal spine present;</text>
      <biological_entity id="o24660" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o24661" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="60-75%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="strong" value_original="strong" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>alar cells quadrate to short-rectangular, same size as or smaller than adjacent basal-cells, 10–15 × 10–12 µm, walls moderately thick, region inconspicuous, of 2–5 × 2–4 cells;</text>
      <biological_entity constraint="terminal" id="o24662" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o24663" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s7" to="short-rectangular" />
        <character char_type="range_value" from="10" from_unit="um" name="length" notes="" src="d0_s7" to="15" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" notes="" src="d0_s7" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o24665" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o24664" name="basal-cell" name_original="basal-cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="arrangement" src="d0_s7" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o24666" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o24667" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o24668" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o24663" id="r3496" name="as" negation="false" src="d0_s7" to="o24665" />
      <relation from="o24667" id="r3497" name="consist_of" negation="false" src="d0_s7" to="o24668" />
    </statement>
    <statement id="d0_s8">
      <text>laminal cells linear, 25–60 × 5–8 µm; basal-cells weakly differentiated, inconspicuous, 25–40 × 9–12 µm, region in 1 or 2 rows.</text>
      <biological_entity constraint="laminal" id="o24669" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s8" to="60" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s8" to="8" to_unit="um" />
      </biological_entity>
      <biological_entity id="o24670" name="basal-cell" name_original="basal-cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s8" to="40" to_unit="um" />
        <character char_type="range_value" from="9" from_unit="um" name="width" src="d0_s8" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o24671" name="region" name_original="region" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Branch leaves more elliptic, 0.7–0.8 × 0.2–0.4 mm;</text>
      <biological_entity constraint="branch" id="o24672" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s9" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costal terminal spine with several distal teeth.</text>
      <biological_entity constraint="distal" id="o24674" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="several" value_original="several" />
      </biological_entity>
      <relation from="o24673" id="r3498" name="with" negation="false" src="d0_s10" to="o24674" />
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="costal terminal" id="o24673" name="spine" name_original="spine" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta redbrown, 1–2.5 cm, rough.</text>
      <biological_entity id="o24675" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s12" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s12" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule horizontal, redbrown, ovate-ventricose, curved, 1–1.6 mm;</text>
      <biological_entity id="o24676" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-ventricose" value_original="ovate-ventricose" />
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus separating by fragments;</text>
      <biological_entity id="o24677" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character constraint="by fragments" constraintid="o24678" is_modifier="false" name="arrangement" src="d0_s14" value="separating" value_original="separating" />
      </biological_entity>
      <biological_entity id="o24678" name="fragment" name_original="fragments" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>operculum long-conic.</text>
      <biological_entity id="o24679" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="long-conic" value_original="long-conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 9–12 µm.</text>
      <biological_entity id="o24680" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="9" from_unit="um" name="some_measurement" src="d0_s16" to="12" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (400-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachythecium bolanderi occurs mainly in California; E. Lawton (1971) reported it from Oregon. Superficially, B. bolanderi resembles small and short-leaved phenotypes of Brachytheciastrum velutinum and Brachytheciastrum fendleri; it differs from the former in its shorter leaves and shorter cells (6–8:1 versus 8–12:1), and from the latter in having erect-spreading and loosely imbricate to distantly arranged leaves rather than erect-appressed and closely imbricate leaves. Small plants of Oxyrrhynchium hians may also be similar, but the branch leaves of O. hians are broader.</discussion>
  
</bio:treatment>