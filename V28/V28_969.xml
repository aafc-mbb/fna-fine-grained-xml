<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James R. Shevock</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">617</other_info_on_meta>
    <other_info_on_meta type="mention_page">590</other_info_on_meta>
    <other_info_on_meta type="mention_page">616</other_info_on_meta>
    <other_info_on_meta type="mention_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">654</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">lembophyllaceae</taxon_name>
    <taxon_name authority="Brotherus in H. G. A. Engler and K. Prantl" date="1906" rank="genus">BESTIA</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>226[I,3]: 858, fig. 631. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lembophyllaceae;genus BESTIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For George Newton Best, 1846 – 1926 American bryologist</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">103881</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large.</text>
      <biological_entity id="o3022" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems pinnate or irregularly 2-pinnate.</text>
      <biological_entity id="o3023" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem and branch leaves similar, ovate;</text>
      <biological_entity id="o3024" name="stem" name_original="stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o3025" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins serrulate in distal 1/5;</text>
      <biological_entity id="o3026" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="in distal 1/5" constraintid="o3027" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3027" name="1/5" name_original="1/5" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>costa single;</text>
      <biological_entity id="o3028" name="costa" name_original="costa" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells less than 4: 1.</text>
      <biological_entity constraint="medial laminal" id="o3029" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="4" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta stramineous, rarely reddish tinged, to 1.5 cm.</text>
      <biological_entity id="o3030" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s6" value="reddish tinged" value_original="reddish tinged" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule erect, symmetric;</text>
      <biological_entity id="o3031" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="symmetric" value_original="symmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>operculum inclined-rostrate;</text>
      <biological_entity id="o3032" name="operculum" name_original="operculum" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="inclined-rostrate" value_original="inclined-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>peristome somewhat reduced.</text>
      <biological_entity id="o3033" name="peristome" name_original="peristome" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Bestia has a confusing taxonomic history, which causes difficulty in determining its family placement depending on which species of Bestia was examined and what combination of morphological characters was considered most important for inferring relationships (J. R. Shevock et al. 2008). H. A. Crum (1987) placed Bestia in close relationship to Isothecium, in Brachytheciaceae. Bestia vancouveriensis was transferred to Thamnobryaceae as Porotrichum vancouveriense and subsequently elevated to generic rank as Bryolawtonia vancouveriensis.</discussion>
  <discussion>Although Bestia has been attributed to several families, recent DNA studies suggest that Bestia and Isothecium are closely related and belong to Lembophyllaceae. Bestia is morphologically similar to some species of Isothecium (especially I. myosuroides), but it has several distinguishing gametophytic features, primarily medial laminal cells uniformly short (less than 4:1) whereas the juxtacostal cells in Isothecium are elongate to 8:1. The long, straight, cylindric capsules with smaller peristome teeth differ markedly from the rather hypnaceous curved capsules in Isothecium.</discussion>
  <references>
    <reference>Shevock, J. R, D. H. Norris and A. J. Shaw. 2008. Identification, distribution and family placement of the pleurocarpous moss Bestia longipes (Sull. &amp; Lesq.) Broth. Madroño 55: 291–296.</reference>
  </references>
  
</bio:treatment>