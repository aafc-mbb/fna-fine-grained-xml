<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wilfred B. Schofield†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">sematophyllaceae</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="genus">ACROPORIUM</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>10: 182. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sematophyllaceae;genus ACROPORIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek akros, top, and poros, pore, possibly alluding to tubulose points of branches</other_info_on_name>
    <other_info_on_name type="fna_id">100384</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, yellowish to golden yellow, glossy.</text>
      <biological_entity id="o805" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s0" to="golden yellow" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 [–10] cm, not or weakly complanate-foliate, irregularly to regularly pinnate or sometimes simple.</text>
      <biological_entity id="o806" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="cm" />
        <character name="some_measurement" src="d0_s1" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="weakly complanate-foliate" modifier="not" name="architecture" src="d0_s1" to="irregularly regularly pinnate" />
        <character char_type="range_value" from="weakly complanate-foliate" name="architecture" src="d0_s1" to="irregularly regularly pinnate" />
        <character is_modifier="false" modifier="sometimes; sometimes" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves imbricate or strongly divergent, sometimes somewhat falcate, narrowly to broadly ovatelanceolate;</text>
      <biological_entity id="o807" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="falcate" modifier="sometimes somewhat; somewhat" name="shape" src="d0_s2" to="narrowly broadly ovatelanceolate" />
        <character char_type="range_value" from="falcate" name="shape" src="d0_s2" to="narrowly broadly ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire;</text>
      <biological_entity id="o808" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex acute to acuminate;</text>
      <biological_entity id="o809" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa double or ecostate;</text>
    </statement>
    <statement id="d0_s6">
      <text>alar cells elongate, inflated, pigmented, walls thin, region in 1 row, middle lamella not apparent, supra-alar cells not apparently differentiated;</text>
      <biological_entity id="o810" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o811" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o812" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o813" name="region" name_original="region" src="d0_s6" type="structure" />
      <biological_entity id="o814" name="row" name_original="row" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="middle" id="o815" name="lamella" name_original="lamella" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="apparent" value_original="apparent" />
      </biological_entity>
      <biological_entity id="o816" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not apparently" name="variability" src="d0_s6" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o813" id="r109" name="in" negation="false" src="d0_s6" to="o814" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cells narrowly rhomboid to linear, smooth or rarely 1-papillose.</text>
    </statement>
    <statement id="d0_s8">
      <text>Sexual condition autoicous [synoicous, dioicous, or phyllodioicous].</text>
      <biological_entity constraint="laminal" id="o817" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="narrowly rhomboid" name="shape" src="d0_s7" to="linear" />
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely" name="relief" src="d0_s7" value="1-papillose" value_original="1-papillose" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seta 0.4–0.7 cm.</text>
      <biological_entity id="o818" name="seta" name_original="seta" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s9" to="0.7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule erect to inclined, ovoid to cylindric;</text>
      <biological_entity id="o819" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="inclined" />
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exothecial cell-walls collenchymatous;</text>
      <biological_entity constraint="exothecial" id="o820" name="cell-wall" name_original="cell-walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic, finely rostrate.</text>
      <biological_entity id="o821" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s12" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Mexico, Central America, South America, Asia, Africa, Pacific Islands, Australia; tropical and subtropical regions, predominantly Indo-Malaysia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="predominantly Indo-Malaysia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 60 (1 in the flora).</discussion>
  
</bio:treatment>