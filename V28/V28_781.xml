<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
    <other_info_on_meta type="mention_page">506</other_info_on_meta>
    <other_info_on_meta type="mention_page">645</other_info_on_meta>
    <other_info_on_meta type="mention_page">646</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">entodontaceae</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="genus">ENTODON</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>18: 704. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family entodontaceae;genus ENTODON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek entos, inside, and odon, tooth, alluding to peristome teeth inserted below capsule mouth</other_info_on_name>
    <other_info_on_name type="fna_id">111744</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to moderately large, in extensive mats, lustrous.</text>
      <biological_entity id="o21001" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="moderately large" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s0" value="lustrous" value_original="lustrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21002" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="extensive" value_original="extensive" />
      </biological_entity>
      <relation from="o21001" id="r3017" name="in" negation="false" src="d0_s0" to="o21002" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping (spreading to ascending in E. concinnus), irregularly branched to subpinnate, branches relatively short, terete or complanate-foliate.</text>
      <biological_entity id="o21003" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="irregularly branched" name="architecture" src="d0_s1" to="subpinnate" />
      </biological_entity>
      <biological_entity id="o21004" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="complanate-foliate" value_original="complanate-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves oblong-lanceolate to oblong-ovate, not plicate (slightly plicate in E. brevisetus);</text>
      <biological_entity id="o21005" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s2" to="oblong-ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base not decurrent;</text>
    </statement>
    <statement id="d0_s4">
      <text>alar cells quadrate to subquadrate;</text>
      <biological_entity id="o21006" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o21007" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s4" to="subquadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>laminal cells straight to ± flexuose;</text>
      <biological_entity constraint="laminal" id="o21008" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="straight" name="course" src="d0_s5" to="more or less flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal-cells shorter, walls usually porose.</text>
      <biological_entity id="o21009" name="basal-cell" name_original="basal-cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o21010" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="porose" value_original="porose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta single.</text>
      <biological_entity id="o21011" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule yellow to redbrown;</text>
      <biological_entity id="o21012" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>columella exserted;</text>
      <biological_entity id="o21013" name="columella" name_original="columella" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>operculum long-conic to obliquely rostrate;</text>
      <biological_entity id="o21014" name="operculum" name_original="operculum" src="d0_s10" type="structure">
        <character char_type="range_value" from="long-conic" name="shape" src="d0_s10" to="obliquely rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>prostome absent;</text>
      <biological_entity id="o21015" name="prostome" name_original="prostome" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exostome teeth yellowbrown to reddish;</text>
      <biological_entity constraint="exostome" id="o21016" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s12" to="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>endostome basal membrane low, segments linear, as long as or shorter than exostome teeth, rarely rudimentary.</text>
      <biological_entity id="o21017" name="endostome" name_original="endostome" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o21018" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o21019" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="prominence" notes="" src="d0_s13" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o21021" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="exostome" id="o21020" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <relation from="o21019" id="r3018" name="as long as" negation="false" src="d0_s13" to="o21021" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 70 (10 in the flora).</discussion>
  <discussion>Plants of Entodon are characterized as flattened or terete pleurocarpous mosses with concave leaves and a weak double costa. The alar cells are quadrate and numerous. The two subgenera, Entodon and Erythropus, are separated on sporophytic characters, specifically the color of the seta, the presence or absence of an annulus, and the color and ornamentation of the exostome teeth. The genus is well represented in Andean South America and East Asia. Species are often best identified using exostomial ornamentation, but sterile plants can be identified from leaf characters.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf apices rounded-obtuse; alar cells 2- or 3-stratose; sexual condition dioicous.</description>
      <determination>10 Entodon concinnus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf apices acute, acuminate, apiculate, or occasionally obtuse; alar cells 1-stratose; sexual condition autoicous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branches terete-foliate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Branches complanate-foliate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Setae yellow; leaf apices slender acuminate.</description>
      <determination>3 Entodon brevisetus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Setae reddish; leaf apices not slender acuminate</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Exostome teeth papillose proximally; sw North America.</description>
      <determination>9 Entodon beyrichii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Exostome teeth ± smooth or striolate proximally; e North America</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Exostome teeth ± smooth proximally; leaves oblong-ovate to elliptic; apices abruptly acute to apiculate.</description>
      <determination>7 Entodon seductrix</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Exostome teeth cross striolate proximally; leaves oblong-ovate; apices acute to gradually short-acuminate.</description>
      <determination>8 Entodon sullivantii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Setae yellow</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Setae reddish</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Exostome teeth with internal surface striate; leaves widest near insertion; se United States.</description>
      <determination>1 Entodon macropodus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Exostome teeth with internal surface papillose to smooth; leaves widest at 1/3 leaf length; South Carolina.</description>
      <determination>2 Entodon hampeanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Exostome teeth cross striolate; annulus not differentiated.</description>
      <determination>4 Entodon schleicheri</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Exostome teeth papillose; annulus differentiated</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf apices gradually acute; margins serrulate and often notched distally.</description>
      <determination>5 Entodon cladorrhizans</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf apices broadly acute; margins entire to subentire or with 1 or 2 teeth at extreme apex.</description>
      <determination>6 Entodon challengeri</determination>
    </key_statement>
  </key>
</bio:treatment>