<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leskeaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) Müller Hal." date="unknown" rank="genus">haplocladium</taxon_name>
    <taxon_name authority="(Bridel) Brotherus in H. G. A. Engler and K. Prantl" date="1907" rank="species">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pfla n zenfam.</publication_title>
      <place_in_publication>229[I,3]: 1007. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leskeaceae;genus haplocladium;species virginianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099134</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Bri del" date="unknown" rank="species">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 576. 1827 (as virginianus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species virginianum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized.</text>
      <biological_entity id="o14061" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems regularly pinnate;</text>
      <biological_entity id="o14062" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia many, filamentous to subfoliose, frequently branched.</text>
      <biological_entity id="o14063" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="many" value_original="many" />
        <character is_modifier="false" name="texture" src="d0_s2" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" modifier="frequently" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves erect to erect-spreading, dense, ovate, not plicate, 0.5–0.7 mm;</text>
      <biological_entity id="o14064" name="leaf-stem" name_original="stem-leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="erect-spreading" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s3" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, weakly serrulate;</text>
      <biological_entity id="o14065" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa ending in apex.</text>
      <biological_entity id="o14066" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o14067" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o14066" id="r1979" name="ending in" negation="false" src="d0_s5" to="o14067" />
    </statement>
    <statement id="d0_s6">
      <text>Branch leaves crowded, imbricate, oval, 0.2–0.5 mm;</text>
      <biological_entity constraint="branch" id="o14068" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oval" value_original="oval" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex acute, apiculate, or short-acuminate;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells not differentiated;</text>
      <biological_entity id="o14069" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-acuminate" value_original="short-acuminate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity id="o14070" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s8" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells roundedquadrate, strongly 1-papillose over lumen, walls incrassate.</text>
      <biological_entity constraint="medial laminal" id="o14071" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="roundedquadrate" value_original="roundedquadrate" />
        <character constraint="over lumen" constraintid="o14072" is_modifier="false" modifier="strongly" name="relief" src="d0_s9" value="1-papillose" value_original="1-papillose" />
      </biological_entity>
      <biological_entity id="o14072" name="lumen" name_original="lumen" src="d0_s9" type="structure" />
      <biological_entity id="o14073" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="incrassate" value_original="incrassate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Capsule strongly inclined to horizontal.</text>
      <biological_entity id="o14074" name="capsule" name_original="capsule" src="d0_s10" type="structure">
        <character char_type="range_value" from="strongly inclined" name="orientation" src="d0_s10" to="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 11–12 µm, weakly granulate.</text>
      <biological_entity id="o14075" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s11" to="12" to_unit="um" />
        <character is_modifier="false" modifier="weakly" name="texture" src="d0_s11" value="granulate" value_original="granulate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="late summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, wood, rock of mesic habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="wood" />
        <character name="habitat" value="rock" constraint="of mesic habitats" />
        <character name="habitat" value="mesic habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1200 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ark., Conn., Fla., Ga., Ill., Iowa, Kans., La., Md., Mass., Mich., Minn., Mo., Nebr., N.J., N.Y., N.C., Ohio, Okla., Pa., Tenn., Tex., Va., Wash., W.Va., Wis.; c Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Haplocladium virginianum has more regularly branched stems than H. angustifolium and H. microphyllum, while the crowded, rounded, weakly concave branch leaves have shorter apices. The branches are julaceous. The small laminal cells and relatively large single papilla centered over the lumen will also help to identify this species.</discussion>
  
</bio:treatment>