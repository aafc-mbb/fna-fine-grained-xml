<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hedwigiaceae</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="genus">BRAUNIA</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Europ.</publication_title>
      <place_in_publication>3: 159, plate 275. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hedwigiaceae;genus BRAUNIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Alexander Carl Heinrich Braun, 1805 – 1877, Director of the Berlin Botanic Garden</other_info_on_name>
    <other_info_on_name type="fna_id">104560</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, deep yellow-green or reddish-brown to blackish brown with age, not hoary distally.</text>
      <biological_entity id="o16310" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="deep" value_original="deep" />
        <character char_type="range_value" constraint="with age" constraintid="o16311" from="reddish-brown" name="coloration" src="d0_s0" to="blackish brown" />
        <character is_modifier="false" modifier="not; distally" name="pubescence" notes="" src="d0_s0" value="hoary" value_original="hoary" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16311" name="age" name_original="age" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems with stoloniform-flagelliform branches present.</text>
      <biological_entity id="o16312" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16313" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="stoloniform-flagelliform" value_original="stoloniform-flagelliform" />
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o16312" id="r2345" name="with" negation="false" src="d0_s1" to="o16313" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves ± loosely imbricate, ± longitudinally plicate, moderately to distinctly plicate-sulcate when dry, 1.5–2 mm;</text>
      <biological_entity id="o16314" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less loosely" name="arrangement" src="d0_s2" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="more or less longitudinally" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s2" value="plicate-sulcate" value_original="plicate-sulcate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins recurved proximally, recurved or plane in apex, erose-denticulate at apex;</text>
      <biological_entity id="o16315" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character constraint="in apex" constraintid="o16316" is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character constraint="at apex" constraintid="o16317" is_modifier="false" name="shape" notes="" src="d0_s3" value="erose-denticulate" value_original="erose-denticulate" />
      </biological_entity>
      <biological_entity id="o16316" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity id="o16317" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex erect to somewhat secund and spreading when dry, erect to widespreading when moist, acute to abruptly short-acuminate, sometimes apiculate in larger leaves, concolorous, strongly channeled, multipapillose;</text>
      <biological_entity constraint="larger" id="o16319" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>alar cells ± isodiametric, smooth to mostly 3-papillose, walls thick, or thinner near insertion, region concolorous or more strongly pigmented in 1 or 2 rows, sometimes gradually forming excavate auricle;</text>
      <biological_entity id="o16318" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="when dry" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="erect" modifier="when moist" name="orientation" src="d0_s4" to="widespreading" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="abruptly short-acuminate" />
        <character constraint="in larger leaves" constraintid="o16319" is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="concolorous" value_original="concolorous" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="channeled" value_original="channeled" />
      </biological_entity>
      <biological_entity id="o16320" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s5" to="mostly 3-papillose" />
      </biological_entity>
      <biological_entity id="o16321" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="width" src="d0_s5" value="thinner" value_original="thinner" />
        <character is_modifier="false" name="insertion" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="insertion" src="d0_s5" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o16322" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="concolorous" value_original="concolorous" />
        <character constraint="in auricle" constraintid="o16323" is_modifier="false" modifier="strongly" name="coloration" src="d0_s5" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o16323" name="auricle" name_original="auricle" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="true" modifier="sometimes gradually" name="architecture" src="d0_s5" value="excavate" value_original="excavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>mid basal laminal cells quadrate in 1 row, becoming long-rectangular to linear, papillae 4–8, in 1 row, walls ± thin to thick, not to moderately porose, region more deeply yellow to yellow-orange at insertion in 1 or 2 rows, rarely to 1/5 leaf length;</text>
      <biological_entity constraint="mid basal laminal" id="o16324" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character constraint="in row" constraintid="o16325" is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="long-rectangular" modifier="becoming" name="shape" notes="" src="d0_s6" to="linear" />
      </biological_entity>
      <biological_entity id="o16325" name="row" name_original="row" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16326" name="papilla" name_original="papillae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <biological_entity id="o16327" name="row" name_original="row" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16328" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character char_type="range_value" from="less thin" name="width" src="d0_s6" to="thick" />
        <character is_modifier="false" modifier="not to moderately" name="architecture" src="d0_s6" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity id="o16329" name="region" name_original="region" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in leaf" constraintid="o16330" from="deeply yellow" name="insertion" src="d0_s6" to="yellow-orange" />
      </biological_entity>
      <biological_entity id="o16330" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character char_type="range_value" from="0" is_modifier="true" modifier="rarely" name="quantity" src="d0_s6" to="1/5" />
      </biological_entity>
      <relation from="o16326" id="r2346" name="in" negation="false" src="d0_s6" to="o16327" />
    </statement>
    <statement id="d0_s7">
      <text>medial cells 3–4 (–6) -papillose over lumen, papillae simple, low and irregularly rounded, walls thin to thick, even or weakly to strongly porose-sinuate;</text>
      <biological_entity constraint="medial" id="o16331" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character constraint="over lumen" constraintid="o16332" is_modifier="false" name="relief" src="d0_s7" value="3-4(-6)-papillose" value_original="3-4(-6)-papillose" />
      </biological_entity>
      <biological_entity id="o16332" name="lumen" name_original="lumen" src="d0_s7" type="structure" />
      <biological_entity id="o16333" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="position" src="d0_s7" value="low" value_original="low" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o16334" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character char_type="range_value" from="thin" name="width" src="d0_s7" to="thick" />
        <character is_modifier="false" modifier="even; weakly to strongly" name="shape" src="d0_s7" value="porose-sinuate" value_original="porose-sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal cells chlorophyllose, similar to and concolorous with medial cells, less papillose;</text>
      <biological_entity constraint="distal" id="o16335" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="with medial cells" constraintid="o16336" is_modifier="false" name="coloration" src="d0_s8" value="concolorous" value_original="concolorous" />
        <character is_modifier="false" modifier="less" name="relief" notes="" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="medial" id="o16336" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>apical cells occasionally hyaline, or chlorophyllose with pellucid margins.</text>
      <biological_entity id="o16338" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="pellucid" value_original="pellucid" />
      </biological_entity>
      <relation from="o16337" id="r2347" name="with" negation="false" src="d0_s9" to="o16338" />
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition autoicous or paroicous;</text>
      <biological_entity constraint="apical" id="o16337" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="paroicous" value_original="paroicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaves similar to vegetative leaves, longer, 2–2.5 (–3) mm, margins entire.</text>
      <biological_entity constraint="perichaetial" id="o16339" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s11" value="longer" value_original="longer" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16340" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o16341" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o16339" id="r2348" name="to" negation="false" src="d0_s11" to="o16340" />
    </statement>
    <statement id="d0_s12">
      <text>Vaginula with paraphyses many, smooth, not extending onto developing calyptra.</text>
      <biological_entity id="o16342" name="vaginulum" name_original="vaginula" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s12" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o16343" name="paraphyse" name_original="paraphyses" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o16344" name="calyptra" name_original="calyptra" src="d0_s12" type="structure">
        <character is_modifier="true" name="development" src="d0_s12" value="developing" value_original="developing" />
      </biological_entity>
      <relation from="o16342" id="r2349" name="with" negation="false" src="d0_s12" to="o16343" />
      <relation from="o16342" id="r2350" modifier="not" name="extending onto" negation="false" src="d0_s12" to="o16344" />
    </statement>
    <statement id="d0_s13">
      <text>Seta dark reddish yellow, (5–) 7–14 (–25) mm, stout, extending beyond perichaetial leaves.</text>
      <biological_entity id="o16345" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="25" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s13" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o16346" name="perichaetial" name_original="perichaetial" src="d0_s13" type="structure" />
      <biological_entity id="o16347" name="leaf" name_original="leaves" src="d0_s13" type="structure" />
      <relation from="o16345" id="r2351" name="extending beyond" negation="false" src="d0_s13" to="o16346" />
      <relation from="o16345" id="r2352" name="extending beyond" negation="false" src="d0_s13" to="o16347" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule exserted, brown, red at mouth, cylindric-fusiform when dry, ellipsoid or sometimes ovate (broadest at base) when moist, (1.9–) 2–2.2 mm, irregularly striate-wrinkled to irregularly furrowed when dry, smooth when moist, mouth narrow;</text>
      <biological_entity id="o16348" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="exserted" value_original="exserted" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character constraint="at mouth" constraintid="o16349" is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" modifier="when dry" name="shape" notes="" src="d0_s14" value="cylindric-fusiform" value_original="cylindric-fusiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly" name="relief" src="d0_s14" value="striate-wrinkled" value_original="striate-wrinkled" />
        <character is_modifier="false" modifier="when dry" name="architecture" src="d0_s14" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" modifier="when moist" name="architecture_or_pubescence_or_relief" src="d0_s14" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o16349" name="mouth" name_original="mouth" src="d0_s14" type="structure" />
      <biological_entity id="o16350" name="mouth" name_original="mouth" src="d0_s14" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s14" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stomata phaneroporic;</text>
      <biological_entity id="o16351" name="stoma" name_original="stomata" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>operculum conic, stoutly, bluntly, and obliquely medium to long-rostrate.</text>
      <biological_entity id="o16352" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="stoutly; bluntly; obliquely" name="size" src="d0_s16" value="medium" value_original="medium" />
        <character is_modifier="false" name="shape" src="d0_s16" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra cucullate, 2–4 mm, covering to below mid capsule, naked.</text>
      <biological_entity id="o16353" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cucullate" value_original="cucullate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character constraint="below mid capsule" constraintid="o16354" is_modifier="false" name="position_relational" src="d0_s17" value="covering" value_original="covering" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s17" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o16354" name="capsule" name_original="capsule" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s17" value="mid" value_original="mid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 18–23 µm, finely and evenly papillose.</text>
      <biological_entity id="o16355" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s18" to="23" to_unit="um" />
        <character is_modifier="false" modifier="finely; evenly" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; mostly tropical and subtropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="mostly tropical and subtropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 23 (2 in the flora).</discussion>
  <discussion>Braunia is distinguished from the other two genera of the family in the flora area by the flagelliform-stoloniferous branches emerging laterally from the stem bases or from branch apices, bearing small, distantly spaced, squarrose-recurved, abruptly setaceous-acuminate leaves. The undersides of the mats are a tangle of wiry stolons. The stolons do not appear to be involved in the proliferation of the plant and may have more of a rhizoidal function, perhaps keeping the acrocarpous stem closer to the substrate. The essentially concolorous apices of the leaves are without whitened areas or filiform processes, although occasionally larger leaves will have several hyaline apical cells or chlorophyllose cells with pellucid margins; the apical cells are somewhat longer (3–4:1) in long-acuminate leaves. The leaves are plicate-undulate, like the ridges in a scallop shell. The capsules are not immersed as in Hedwigia, but distinctly exserted. The cells are multipapillose and the papillae are low, irregularly rounded rather than tuberculate or obviously branched. The perichaetial leaf apices are not pilose, with channeled acumina and a sharp apical cell.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins recurved proximally, plane distally; distal medial laminal cell walls straight or weakly sinuate; sexual condition autoicous.</description>
      <determination>1 Braunia andrieuxii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf margins recurved to near apex; distal medial laminal cell walls strongly sinuate; sexual condition paroicous.</description>
      <determination>2 Braunia secunda</determination>
    </key_statement>
  </key>
</bio:treatment>