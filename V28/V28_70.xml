<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Vitt" date="1971" rank="species">flowersii</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>74: 159, figs. 1 – 8. 1971</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species flowersii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250061869</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 0.5 cm.</text>
      <biological_entity id="o6806" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="0.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stem-leaves stiff, erect-appressed when dry, ovate-oblong to oblong-ligulate, 2–2.8 mm;</text>
      <biological_entity id="o6807" name="leaf-stem" name_original="stem-leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s1" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="oblong-ligulate" value_original="oblong-ligulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="distance" src="d0_s1" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins revolute, entire or rough near apex by means of projecting papillae;</text>
      <biological_entity id="o6808" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s2" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="near apex" constraintid="o6809" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="rough" value_original="rough" />
      </biological_entity>
      <biological_entity id="o6809" name="apex" name_original="apex" src="d0_s2" type="structure" />
      <biological_entity id="o6810" name="papilla" name_original="papillae" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="projecting" value_original="projecting" />
      </biological_entity>
      <relation from="o6809" id="r937" name="by means of" negation="false" src="d0_s2" to="o6810" />
    </statement>
    <statement id="d0_s3">
      <text>apex rounded-obtuse, often mucronate, carinate;</text>
      <biological_entity id="o6811" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded-obtuse" value_original="rounded-obtuse" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="carinate" value_original="carinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal laminal cells rectangular to short-rectangular, walls thin, not nodose;</text>
      <biological_entity constraint="basal laminal" id="o6812" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s4" to="short-rectangular" />
      </biological_entity>
      <biological_entity id="o6813" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal cells 13–19 µm, 1-stratose, papillae 2 per cell, conic, small.</text>
      <biological_entity constraint="distal" id="o6814" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" from_unit="um" name="some_measurement" src="d0_s5" to="19" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o6816" name="cell" name_original="cell" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Specialized asexual reproduction by gemmae on leaves.</text>
      <biological_entity id="o6817" name="gemma" name_original="gemmae" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="asexual" value_original="asexual" />
      </biological_entity>
      <biological_entity id="o6818" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o6817" id="r938" name="on" negation="false" src="d0_s6" to="o6818" />
    </statement>
    <statement id="d0_s7">
      <text>Sexual condition autoicous.</text>
      <biological_entity id="o6815" name="papilla" name_original="papillae" src="d0_s5" type="structure">
        <character constraint="per cell" constraintid="o6816" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
        <character is_modifier="false" name="development" src="d0_s6" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 0.5 mm.</text>
      <biological_entity id="o6819" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule immersed or short-emergent, ovate-oblong to oblong when mature, 1–1.4 mm, strongly 8-ribbed 1/2–3/4 capsule length when dry, constricted below mouth when dry;</text>
      <biological_entity id="o6820" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="location" src="d0_s9" value="short-emergent" value_original="short-emergent" />
        <character char_type="range_value" from="ovate-oblong" modifier="when mature" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s9" value="8-ribbed" value_original="8-ribbed" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s9" to="3/4" />
      </biological_entity>
      <biological_entity id="o6821" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character constraint="below mouth" constraintid="o6822" is_modifier="false" modifier="when dry" name="length" src="d0_s9" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o6822" name="mouth" name_original="mouth" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stomata immersed, to 1/2 covered by subsidiary-cells, cells not projecting, inner walls not much thickened;</text>
      <biological_entity id="o6823" name="stoma" name_original="stomata" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="immersed" value_original="immersed" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="1/2" />
      </biological_entity>
      <biological_entity id="o6824" name="subsidiary-cell" name_original="subsidiary-cells" src="d0_s10" type="structure" />
      <biological_entity id="o6825" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="orientation" src="d0_s10" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6826" name="wall" name_original="walls" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not much" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o6823" id="r939" name="covered by" negation="false" src="d0_s10" to="o6824" />
    </statement>
    <statement id="d0_s11">
      <text>peristome double;</text>
      <biological_entity id="o6827" name="peristome" name_original="peristome" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>prostome absent;</text>
      <biological_entity id="o6828" name="prostome" name_original="prostome" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>exostome teeth 8, reflexed, densely papillose or papillose-striate distally;</text>
      <biological_entity constraint="exostome" id="o6829" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="8" value_original="8" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s13" value="papillose" value_original="papillose" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s13" value="papillose-striate" value_original="papillose-striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>endostome segments 8, not well developed, usually of 1 row of cells, finely roughened.</text>
      <biological_entity constraint="endostome" id="o6830" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="8" value_original="8" />
        <character is_modifier="false" modifier="not well" name="development" src="d0_s14" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="finely" name="relief_or_texture" notes="" src="d0_s14" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o6831" name="row" name_original="row" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6832" name="cell" name_original="cells" src="d0_s14" type="structure" />
      <relation from="o6830" id="r940" modifier="usually" name="consist_of" negation="false" src="d0_s14" to="o6831" />
      <relation from="o6831" id="r941" name="part_of" negation="false" src="d0_s14" to="o6832" />
    </statement>
    <statement id="d0_s15">
      <text>Calyptra oblong, smooth, naked or hairs few, smooth.</text>
      <biological_entity id="o6833" name="calyptra" name_original="calyptra" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o6834" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s15" value="few" value_original="few" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Spores 10–17 µm.</text>
      <biological_entity id="o6835" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s16" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Base of trees, trunks of deciduous trees, xeric and exposed habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="base" constraint="of trees , trunks of deciduous trees , xeric and exposed habitats" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="trunks" constraint="of deciduous trees" />
        <character name="habitat" value="deciduous trees" />
        <character name="habitat" value="xeric" />
        <character name="habitat" value="exposed habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (500- 2000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Utah; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orthotrichum flowersii is distinguished from O. pallens and O. pumilum by its blunt, carinate leaves that are ovate or ovate-oblong, almost always with a small mucro. The leaf apices are subcucullate; the stomata are found in one or two rows mid capsule.</discussion>
  
</bio:treatment>