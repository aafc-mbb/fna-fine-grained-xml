<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John R. Spence</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">131</other_info_on_meta>
    <other_info_on_meta type="mention_page">658</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="mention_page">662</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence &amp; H. P. Ramsay" date="unknown" rank="genus">GEMMABRYUM</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>87: 63. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus GEMMABRYUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin gemma, bud, and Greek bryon, moss, alluding to asexual reproduction</other_info_on_name>
    <other_info_on_name type="fna_id">317904</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in open to dense turfs or gregarious, green, yellow-green, pink, or red.</text>
      <biological_entity id="o132" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="gregarious" value_original="gregarious" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o133" name="turf" name_original="turfs" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o132" id="r25" name="in" negation="false" src="d0_s0" to="o133" />
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.1–2 (–3) cm, gemmiform to evenly foliate, not or strongly branched;</text>
      <biological_entity id="o134" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="gemmiform" value_original="gemmiform" />
        <character is_modifier="false" modifier="evenly" name="architecture" src="d0_s1" value="foliate" value_original="foliate" />
        <character is_modifier="false" modifier="not; strongly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids usually few, micronemata present, macronemata few or absent.</text>
      <biological_entity id="o135" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves imbricate to loosely set and slightly twisted when dry, spreading when moist, ovate, ovatelanceolate, or triangular, flat or concave, 0.4–2.5 (–3) mm;</text>
      <biological_entity id="o136" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="to set" constraintid="o137" is_modifier="false" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="architecture" notes="" src="d0_s3" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o137" name="set" name_original="set" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>base rarely decurrent;</text>
      <biological_entity id="o138" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane to strongly revolute, entire to serrulate distally, 1-stratose, limbidium absent or weak;</text>
      <biological_entity id="o139" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="plane" name="shape" src="d0_s5" to="strongly revolute" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o140" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape_or_architecture" src="d0_s5" value="entire to serrulate" value_original="entire to serrulate" />
        <character is_modifier="true" modifier="distally" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o139" id="r26" name="entire to serrulate" negation="false" src="d0_s5" to="o140" />
    </statement>
    <statement id="d0_s6">
      <text>apex broadly rounded to acuminate;</text>
      <biological_entity id="o141" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s6" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa not reaching apex to long-excurrent, awn present, apiculus occasionally present, guide cells usually in 1 layer;</text>
      <biological_entity id="o142" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="long-excurrent" value_original="long-excurrent" />
      </biological_entity>
      <biological_entity id="o143" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o144" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o145" name="apiculu" name_original="apiculus" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o147" name="layer" name_original="layer" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o142" id="r27" name="reaching" negation="true" src="d0_s7" to="o143" />
      <relation from="o146" id="r28" name="in" negation="false" src="d0_s7" to="o147" />
    </statement>
    <statement id="d0_s8">
      <text>alar cells quadrate;</text>
      <biological_entity constraint="guide" id="o146" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o148" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminal areolation heterogeneous;</text>
      <biological_entity constraint="laminal" id="o149" name="areolation" name_original="areolation" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="heterogeneous" value_original="heterogeneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>proximal laminal cells gradually or abruptly quadrate to rectangular, shorter than more distal cells, 1–2 (–4):1;</text>
      <biological_entity constraint="proximal laminal" id="o150" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character char_type="range_value" from="quadrate" modifier="abruptly" name="shape" src="d0_s10" to="rectangular" />
        <character constraint="than more distal cells" constraintid="o151" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o151" name="cell" name_original="cells" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>medial and distal cells rhomboidal to elongate-hexagonal, 8–16 (–20) µm wide, usually (3–) 4–8: 1, walls thin to moderately incrassate, not porose.</text>
      <biological_entity constraint="medial and distal" id="o152" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="rhomboidal" name="shape" src="d0_s11" to="elongate-hexagonal" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="um" name="width" src="d0_s11" to="20" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s11" to="16" to_unit="um" />
        <character char_type="range_value" from="3" modifier="usually" name="atypical_quantity" src="d0_s11" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" modifier="usually" name="quantity" src="d0_s11" to="8" />
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Specialized asexual reproduction common, of several types, including leaf-axil bulbils or brood branchlets, rhizoidal filiform gemmae and tubers, and stem tubers.</text>
      <biological_entity id="o153" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="moderately" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="porose" value_original="porose" />
        <character is_modifier="false" name="development" src="d0_s12" value="specialized" value_original="specialized" />
      </biological_entity>
      <biological_entity id="o154" name="type" name_original="types" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="quantity" src="d0_s12" value="common" value_original="common" />
        <character is_modifier="true" name="quantity" src="d0_s12" value="several" value_original="several" />
      </biological_entity>
      <biological_entity constraint="leaf-axil" id="o155" name="bulbil" name_original="bulbils" src="d0_s12" type="structure" />
      <biological_entity constraint="leaf-axil" id="o156" name="branchlet" name_original="branchlets" src="d0_s12" type="structure" />
      <biological_entity constraint="stem" id="o159" name="tuber" name_original="tubers" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous or rarely synoicous;</text>
      <biological_entity constraint="rhizoidal" id="o157" name="gemma" name_original="gemmae" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s13" value="synoicous" value_original="synoicous" />
      </biological_entity>
      <biological_entity constraint="rhizoidal" id="o158" name="tuber" name_original="tubers" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" modifier="rarely" name="reproduction" src="d0_s13" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>perigonia and perichaetia terminal;</text>
      <biological_entity id="o160" name="perigonium" name_original="perigonia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o161" name="perichaetium" name_original="perichaetia" src="d0_s14" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perigonial and perichaetial leaves not much differentiated, outer leaves larger, inner leaves smaller than leaves of innovations.</text>
      <biological_entity constraint="perigonial and perichaetial" id="o162" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not much" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity constraint="outer" id="o163" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="inner" id="o164" name="leaf" name_original="leaves" src="d0_s15" type="structure">
        <character constraint="than leaves" constraintid="o165" is_modifier="false" name="size" src="d0_s15" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o165" name="leaf" name_original="leaves" src="d0_s15" type="structure" />
      <biological_entity id="o166" name="innovation" name_original="innovations" src="d0_s15" type="structure" />
      <relation from="o165" id="r29" name="part_of" negation="false" src="d0_s15" to="o166" />
    </statement>
    <statement id="d0_s16">
      <text>Seta usually single, straight or somewhat flexuose, not geniculate.</text>
      <biological_entity id="o167" name="seta" name_original="seta" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s16" value="single" value_original="single" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s16" value="flexuose" value_original="flexuose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s16" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsule erect, inclined, or nutant, pyriform or ovate, not zygomorphic, 1–3 (–5) mm;</text>
      <biological_entity id="o168" name="capsule" name_original="capsule" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="nutant" value_original="nutant" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s17" value="nutant" value_original="nutant" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="zygomorphic" value_original="zygomorphic" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>hypophysis slender or thick, sometimes inflated and rugose;</text>
      <biological_entity id="o169" name="hypophysis" name_original="hypophysis" src="d0_s18" type="structure">
        <character is_modifier="false" name="size" src="d0_s18" value="slender" value_original="slender" />
        <character is_modifier="false" name="width" src="d0_s18" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s18" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="relief" src="d0_s18" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>operculum short-conic to long-convex, sometimes apiculate or rarely rostrate;</text>
      <biological_entity id="o170" name="operculum" name_original="operculum" src="d0_s19" type="structure">
        <character char_type="range_value" from="short-conic" name="shape" src="d0_s19" to="long-convex" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s19" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s19" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>peristome double;</text>
      <biological_entity id="o171" name="peristome" name_original="peristome" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>exostome yellow to brown, teeth lanceolate, acuminate;</text>
      <biological_entity id="o172" name="exostome" name_original="exostome" src="d0_s21" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s21" to="brown" />
      </biological_entity>
      <biological_entity id="o173" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s21" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>endostome rarely adherent to exostome, basal membrane high to low, segments well developed or occasionally reduced, perforations lanceolate to ovate, cilia long and appendiculate to short or absent.</text>
      <biological_entity id="o174" name="endostome" name_original="endostome" src="d0_s22" type="structure">
        <character constraint="to exostome" constraintid="o175" is_modifier="false" modifier="rarely" name="fusion" src="d0_s22" value="adherent" value_original="adherent" />
      </biological_entity>
      <biological_entity id="o175" name="exostome" name_original="exostome" src="d0_s22" type="structure" />
      <biological_entity constraint="basal" id="o176" name="membrane" name_original="membrane" src="d0_s22" type="structure">
        <character is_modifier="false" name="height" src="d0_s22" value="high" value_original="high" />
        <character is_modifier="false" name="position" src="d0_s22" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o177" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s22" value="developed" value_original="developed" />
        <character is_modifier="false" modifier="occasionally; occasionally" name="size" src="d0_s22" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o178" name="perforation" name_original="perforations" src="d0_s22" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s22" to="ovate" />
      </biological_entity>
      <biological_entity id="o179" name="cilium" name_original="cilia" src="d0_s22" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s22" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="appendiculate" value_original="appendiculate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s22" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Spores shed singly, 8–18 (–20) µm, smooth to papillose, light-brown.</text>
      <biological_entity id="o180" name="spore" name_original="spores" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="singly" value_original="singly" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s23" to="20" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s23" to="18" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s23" to="papillose" />
        <character is_modifier="false" name="coloration" src="d0_s23" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; tropical, subtropical, temperate to boreal regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="tropical" establishment_means="native" />
        <character name="distribution" value="subtropical" establishment_means="native" />
        <character name="distribution" value="temperate to boreal regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 100 (19 in the flora).</discussion>
  <discussion>Species of Gemmabryum are small and often sterile, and can be extremely difficult to identify in the absence of gemmae. This genus exhibits a remarkable array of asexual reproductive structures, including bulbils, rhizoidal tubers, stem tubers, and filiform rhizoidal gemmae. Sterile collections should be made whenever possible, as fertile material often lacks the diagnostic gemmae.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Specialized asexual reproduction by leaf axil bulbils or brood branchlets, rarely by rhizoidal tubers; stems gemmiform or evenly foliate, often in 2 or more clumps along stem; leaves imbricate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Specialized asexual reproduction by rhizoidal tubers; stems evenly foliate; leaves imbricate to loosely set, usually slightly twisted when dry</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bulbils absent; costae usually long-excurrent; inflated group of pinkish subalar cells present; leaf axil brood branchlets sometimes present; limbidium often present.</description>
      <determination>4a Gemmabryum sect. Caespitibryum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bulbils present or absent; costae percurrent to short-excurrent; inflated group of pinkish subalar cells absent; brood branchlets absent; limbidium absent.</description>
      <determination>4b Gemmabryum sect. Gemmabryum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants pale green to red-green; stems gemmiform to evenly foliate, often in 2 or more clumps along stem; leaf costae long-excurrent; inflated group of pinkish subalar cells often present; brood branchlets sometimes present; limbidium often present.</description>
      <determination>4a Gemmabryum sect. Caespitibryum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants green or yellow-green, often with pinkish or reddish tinge; stems evenly foliate; leaf costae percurrent to short-excurrent, if long-excurrent then rhizoidal tubers common; inflated group of pinkish subalar cells absent; brood branchlets absent; limbidium usually absent.</description>
      <determination>4c Gemmabryum sect. Tuberibryum</determination>
    </key_statement>
  </key>
</bio:treatment>