<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">600</other_info_on_meta>
    <other_info_on_meta type="mention_page">599</other_info_on_meta>
    <other_info_on_meta type="mention_page">601</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">pterobryaceae</taxon_name>
    <taxon_name authority="Cardot" date="unknown" rank="genus">pireella</taxon_name>
    <taxon_name authority="(Sullivant) Cardot" date="1913" rank="species">cymbifolia</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>40: 17. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pterobryaceae;genus pireella;species cymbifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062408</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pilotrichum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="species">cymbifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 681. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus pilotrichum;species cymbifolium</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.2–2.5 cm.</text>
      <biological_entity id="o18354" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or sparsely branched.</text>
      <biological_entity id="o18355" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Distal stipe leaves ovatelanceolate, 1.8 × 0.9 mm;</text>
      <biological_entity constraint="stipe" id="o18356" name="leaf" name_original="leaves" src="d0_s2" type="structure" constraint_original="distal stipe">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character name="length" src="d0_s2" unit="mm" value="1.8" value_original="1.8" />
        <character name="width" src="d0_s2" unit="mm" value="0.9" value_original="0.9" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base not auriculate or rounded, decurrent;</text>
      <biological_entity id="o18357" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex broadly acuminate;</text>
      <biological_entity id="o18358" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa percurrent in distal leaves;</text>
      <biological_entity constraint="distal" id="o18360" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>alar cells in 7–14 rows of 2–23 cells, quadrate or short-rectangular, into decurrent wing and distally along margin;</text>
      <biological_entity id="o18359" name="costa" name_original="costa" src="d0_s5" type="structure">
        <character constraint="in distal leaves" constraintid="o18360" is_modifier="false" name="architecture" src="d0_s5" value="percurrent" value_original="percurrent" />
      </biological_entity>
      <biological_entity id="o18361" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o18362" name="row" name_original="rows" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s6" to="14" />
      </biological_entity>
      <biological_entity id="o18363" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="23" />
      </biological_entity>
      <biological_entity id="o18364" name="wing" name_original="wing" src="d0_s6" type="structure">
        <character is_modifier="true" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o18365" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <relation from="o18361" id="r2636" name="in" negation="false" src="d0_s6" to="o18362" />
      <relation from="o18362" id="r2637" name="part_of" negation="false" src="d0_s6" to="o18363" />
      <relation from="o18361" id="r2638" name="into" negation="false" src="d0_s6" to="o18364" />
      <relation from="o18361" id="r2639" modifier="distally" name="along" negation="false" src="d0_s6" to="o18365" />
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells 16–46 × 2–3 µm. Branch leaves strongly seriate, strongly concave, 1.1–1.7 × 0.3–0.6 mm;</text>
      <biological_entity constraint="medial laminal" id="o18366" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="length" src="d0_s7" to="46" to_unit="um" />
        <character char_type="range_value" from="2" from_unit="um" name="width" src="d0_s7" to="3" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="branch" id="o18367" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_arrangement" src="d0_s7" value="seriate" value_original="seriate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s7" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s7" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex short, broad;</text>
      <biological_entity id="o18368" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s8" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells 16–46 × 2–3 µm, strongly prorate.</text>
    </statement>
    <statement id="d0_s10">
      <text>[Seta 0.5–0.9 cm. Capsule 1.8–2.7 × 0.9 mm. Spores spheric or ovoid, 30–41 × 28–35 µm].</text>
      <biological_entity constraint="medial laminal" id="o18369" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="16" from_unit="um" modifier="strongly" name="length" src="d0_s9" to="46" to_unit="um" />
        <character char_type="range_value" from="2" from_unit="um" modifier="strongly" name="width" src="d0_s9" to="3" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry evergreen forests, lowland deciduous forests and hammocks, flooded palm woodlands, disturbed woodlands, trees, tree roots, bushes, decaying wood, limestone, sandstone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry evergreen forests" />
        <character name="habitat" value="lowland deciduous forests" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="palm woodlands" modifier="flooded" />
        <character name="habitat" value="disturbed woodlands" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="tree roots" />
        <character name="habitat" value="bushes" />
        <character name="habitat" value="decaying wood" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-700 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; s Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="s Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plants of Pireella cymbifolia are usually small and scarcely branched, but well-grown plants (for example, in the Florida Keys) may be larger and irregularly pinnate. The leaf bases are decurrent, with several rows of quadrate alar cells extending distally along the margin. Pireella cymbifolia is restricted to southern Florida in the flora area. The leaves are often very small for most of the stipe, 0.5 × 0.3 mm. Superficially, P. cymbifolium may resemble Henicodium; under the microscope, the more extensive area of oblate cells in the leaf base and distinctly plicate leaves of Henicodium should prevent confusion.</discussion>
  
</bio:treatment>