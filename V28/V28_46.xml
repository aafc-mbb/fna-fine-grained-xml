<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">38</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hooker in T. Drummond" date="unknown" rank="genus">DRUMMONDIA</taxon_name>
    <place_of_publication>
      <publication_title>in T. Drummond, Musc. Amer.,</publication_title>
      <place_in_publication>62. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus DRUMMONDIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Thomas Drummond, 1780 – 1835, Scottish botanist who collected extensively on two expeditions to North America</other_info_on_name>
    <other_info_on_name type="fna_id">110959</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, in loose mats.</text>
      <biological_entity id="o2350" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2351" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o2350" id="r299" name="in" negation="false" src="d0_s0" to="o2351" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, branches erect.</text>
      <biological_entity id="o2352" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o2353" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect-appressed and stiff when dry, spreading to widespreading when moist, broadly lanceolate to oblong-lanceolate, not rugose;</text>
      <biological_entity id="o2354" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" modifier="when dry" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character char_type="range_value" from="spreading" modifier="when moist" name="orientation" src="d0_s2" to="widespreading" />
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s2" to="oblong-lanceolate" />
        <character is_modifier="false" modifier="not" name="relief" src="d0_s2" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>margins entire;</text>
      <biological_entity id="o2355" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex obtuse, acute, or cuspidate;</text>
      <biological_entity id="o2356" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuspidate" value_original="cuspidate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa ending just below apex;</text>
      <biological_entity id="o2357" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o2358" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o2357" id="r300" modifier="just below" name="ending" negation="false" src="d0_s5" to="o2358" />
    </statement>
    <statement id="d0_s6">
      <text>basal laminal cells rectangular to quadrate;</text>
      <biological_entity constraint="basal laminal" id="o2359" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s6" to="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells ± roundedquadrate, 6–10 µm, smooth;</text>
      <biological_entity constraint="distal" id="o2360" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="roundedquadrate" value_original="roundedquadrate" />
        <character char_type="range_value" from="6" from_unit="um" name="some_measurement" src="d0_s7" to="10" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>marginal cells not distinct from basal.</text>
      <biological_entity constraint="basal marginal" id="o2362" name="cell" name_original="cells" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="marginal" id="o2361" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character constraint="from basal marginal cells" constraintid="o2362" is_modifier="false" modifier="not" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves longer than stem-leaves.</text>
      <biological_entity constraint="perichaetial" id="o2363" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="than stem-leaves" constraintid="o2364" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2364" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seta 2–3.5 mm.</text>
      <biological_entity id="o2365" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule long-exserted, ovate to ovate-oblong, wrinkled when old and dry, not constricted below mouth;</text>
      <biological_entity id="o2366" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="long-exserted" value_original="long-exserted" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="ovate-oblong" />
        <character is_modifier="false" modifier="when old and dry" name="relief" src="d0_s12" value="wrinkled" value_original="wrinkled" />
        <character constraint="below mouth" constraintid="o2367" is_modifier="false" modifier="not" name="size" src="d0_s12" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o2367" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stomata absent;</text>
      <biological_entity id="o2368" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome single;</text>
      <biological_entity id="o2369" name="peristome" name_original="peristome" src="d0_s14" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s14" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth 16, smooth.</text>
      <biological_entity constraint="exostome" id="o2370" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Calyptra cucullate, long-conic, smooth, naked, not plicate, covering distal 1/3 of capsule.</text>
      <biological_entity id="o2371" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cucullate" value_original="cucullate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="long-conic" value_original="long-conic" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="naked" value_original="naked" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s16" value="plicate" value_original="plicate" />
        <character is_modifier="false" name="position_relational" src="d0_s16" value="covering" value_original="covering" />
        <character is_modifier="false" name="position_or_shape" src="d0_s16" value="distal" value_original="distal" />
        <character constraint="of capsule" constraintid="o2372" name="quantity" src="d0_s16" value="1/3" value_original="1/3" />
      </biological_entity>
      <biological_entity id="o2372" name="capsule" name_original="capsule" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Spores isosporous, multicellular.</text>
      <biological_entity id="o2373" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="multicellular" value_original="multicellular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, ne Mexico, South America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="ne Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 6 (1 in the flora).</discussion>
  <discussion>Drummondia is characterized by branched, prostrate stems and cucullate calyptrae.</discussion>
  
</bio:treatment>