<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lars Hedenäs,Norton G. Miller†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="mention_page">298</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="mention_page">656</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Limpricht) Loeske" date="unknown" rank="genus">PSEUDOCALLIERGON</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>46: 311. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus PSEUDOCALLIERGON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pseudes, false, and genus Calliergon</other_info_on_name>
    <other_info_on_name type="fna_id">126995</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Limpricht" date="unknown" rank="section">Pseudocalliergon</taxon_name>
    <place_of_publication>
      <publication_title>Laubm. Deutschl.</publication_title>
      <place_in_publication>3: 547. 1899 (as Pseudo-calliergon)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;section pseudocalliergon</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, green, brown-green, yellowish, or yellowbrown.</text>
      <biological_entity id="o21203" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brown-green" value_original="brown-green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowbrown" value_original="yellowbrown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched to irregularly pinnate, ± in one plane;</text>
      <biological_entity id="o21204" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched to irregularly" value_original="unbranched to irregularly" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present;</text>
      <biological_entity id="o21205" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o21206" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia absent;</text>
      <biological_entity id="o21207" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials on stem or abaxial costa insertion, rarely forming tomentum, slightly branched, smooth;</text>
      <biological_entity id="o21208" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="rhizoid" id="o21209" name="initial" name_original="initials" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o21210" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o21211" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o21212" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o21208" id="r3049" name="on" negation="false" src="d0_s4" to="o21210" />
      <relation from="o21209" id="r3050" name="on" negation="false" src="d0_s4" to="o21210" />
      <relation from="o21208" id="r3051" name="on" negation="false" src="d0_s4" to="o21211" />
      <relation from="o21209" id="r3052" name="on" negation="false" src="d0_s4" to="o21211" />
      <relation from="o21208" id="r3053" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o21212" />
      <relation from="o21209" id="r3054" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o21212" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hairs well developed, many, distal cells 1–2 (–3), yellow or brownish when young.</text>
      <biological_entity constraint="axillary" id="o21213" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s5" value="developed" value_original="developed" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
      </biological_entity>
      <biological_entity constraint="distal" id="o21214" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s5" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves imbricate, erectopatent, patent, or sometimes spreading, straight or falcate, ovatelanceolate to very broadly ovate, not plicate, longer than 1 mm;</text>
      <biological_entity id="o21215" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erectopatent" value_original="erectopatent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovatelanceolate to very" value_original="ovatelanceolate to very" />
        <character is_modifier="false" modifier="very; broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character modifier="longer than" name="distance" src="d0_s6" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base not decurrent (decurrent in P. trifarium);</text>
      <biological_entity id="o21216" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane, entire, very finely or distinctly denticulate, limbidia absent;</text>
      <biological_entity id="o21217" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="very finely; finely; distinctly" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o21218" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex broadly rounded, apiculate, or acuminate, acumen plane or furrowed;</text>
      <biological_entity id="o21219" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s9" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o21220" name="acumen" name_original="acumen" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa single to mid leaf or beyond, or double and short;</text>
      <biological_entity id="o21221" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>alar cells ± differentiated, quadrate to long-rectangular, inflated or not, yellow when mature, walls thin to strongly incrassate, region indistinctly delimited, transversely triangular or sometimes in one transverse basal row, large;</text>
      <biological_entity constraint="mid" id="o21222" name="leaf" name_original="leaf" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o21223" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="quadrate" name="shape" src="d0_s11" to="long-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character name="shape" src="d0_s11" value="not" value_original="not" />
        <character is_modifier="false" modifier="when mature" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o21224" name="wall" name_original="walls" src="d0_s11" type="structure">
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s11" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o21225" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="prominence" src="d0_s11" value="delimited" value_original="delimited" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
        <character name="shape" src="d0_s11" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="basal" id="o21226" name="row" name_original="row" src="d0_s11" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s11" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o21225" id="r3055" name="in" negation="false" src="d0_s11" to="o21226" />
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cells linear;</text>
      <biological_entity constraint="medial laminal" id="o21227" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>marginal cells 1-stratose.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="marginal" id="o21228" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule horizontal to inclined, cylindric, curved;</text>
      <biological_entity id="o21229" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s15" to="inclined" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome perfect or nearly so;</text>
      <biological_entity id="o21230" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="perfect" value_original="perfect" />
        <character name="architecture" src="d0_s16" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome margins weakly dentate distally;</text>
      <biological_entity constraint="exostome" id="o21231" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="weakly; distally" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia 1–4, well developed, nodose.</text>
      <biological_entity constraint="endostome" id="o21232" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s18" to="4" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s18" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 10–18 µm.</text>
      <biological_entity id="o21233" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s19" to="18" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, West Indies, South America, Eurasia, Africa, Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 5 (4 in the flora).</discussion>
  <discussion>Pseudocalliergon is typically found in mineral-rich to strongly calcareous habitats. Excepting P. angustifolium, the shoots are generally turgid, with markedly broad leaves. All species become yellow-brown when old; when dry they frequently have a golden metallic gloss in spots, that is rare only in P. trifarium (readily visible in the dissecting microscope). The alar regions consist of few or often many quadrate to long-rectangular cells, often with yellow walls, thin-walled to often strongly incrassate, and form indistinctly delimited transversely triangular groups or sometimes single transverse basal rows. Species of Pseudocalliergon are somewhat similar to those of Drepanocladus, but dry plants of Pseudocalliergon usually have a golden metallic gloss that is absent in Drepanocladus, alar regions are more diffusely delimited and consist of less strongly inflated cells than in the latter, axillary hairs are usually yellowish when young only in Pseudocalliergon, and habitats of the Pseudocalliergon species are usually nutrient-poor, whereas those of Drepanocladus species are more or less nutrient-rich. Despite morphological distinctness, molecular data suggest that the Pseudocalliergon species are nested within Drepanocladus (L. Hedenäs and C. Rosborg 2008). Characters differentiating Pseudocalliergon from 14. Sanionia are mentioned under the latter genus.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves falcate, gradually or somewhat suddenly narrowed to apex; apices acuminate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaves straight, suddenly narrowed to apex; apices rounded or apiculate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaves ovate-lanceolate to broadly ovate; apices long-acuminate; costae single, 3-5-stratose, (31-)38-70(-74) µm wide at base; distal laminal cells often prorate abaxially; boreal or mountainous areas.</description>
      <determination>1 Pseudocalliergon angustifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaves ovate to very broadly ovate; apices short- to long-acuminate; costae single, 2- or 3-stratose, 21-53 µm wide at base, or sometimes double; distal laminal cells smooth; Arctic areas.</description>
      <determination>2 Pseudocalliergon brevifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf apices broadly obtuse or rounded, short-apiculate.</description>
      <determination>3 Pseudocalliergon turgescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf apices broadly rounded.</description>
      <determination>4 Pseudocalliergon trifarium</determination>
    </key_statement>
  </key>
</bio:treatment>