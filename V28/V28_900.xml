<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="illustration_page">576</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Brotherus" date="unknown" rank="family">sematophyllaceae</taxon_name>
    <taxon_name authority="Brotherus" date="unknown" rank="genus">hageniella</taxon_name>
    <taxon_name authority="(Mitten) B. C. Tan &amp; Y. Jia" date="1999" rank="species">micans</taxon_name>
    <place_of_publication>
      <publication_title>J. Hattori Bot. Lab.</publication_title>
      <place_in_publication>86: 37. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family sematophyllaceae;genus hageniella;species micans</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stereodon</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">micans</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>2: 114. 1859,</place_in_publication>
      <other_info_on_pub>based on Hypnum micans Wilson in W. J. Hooker, Brit. Fl. ed. 4, 2: 86. 1833, not Swartz 1829</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus stereodon;species micans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hygrohypnum</taxon_name>
    <taxon_name authority="(Austin) Grout" date="unknown" rank="species">novae-caesarae</taxon_name>
    <taxon_hierarchy>genus hygrohypnum;species novae-caesarae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schofieldiella</taxon_name>
    <taxon_name authority="(Mitten) W. R. Buck" date="unknown" rank="species">micans</taxon_name>
    <taxon_hierarchy>genus schofieldiella;species micans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sematophyllum</taxon_name>
    <taxon_name authority="(Mitten) Brotherus" date="unknown" rank="species">micans</taxon_name>
    <taxon_hierarchy>genus sematophyllum;species micans</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in dense, shallow, creeping mats.</text>
      <biological_entity id="o2295" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2296" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="depth" src="d0_s0" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <relation from="o2295" id="r295" name="in" negation="false" src="d0_s0" to="o2296" />
    </statement>
    <statement id="d0_s1">
      <text>Stems with shoots facing downward;</text>
      <biological_entity id="o2297" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="downward" value_original="downward" />
      </biological_entity>
      <biological_entity id="o2298" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s1" value="facing" value_original="facing" />
      </biological_entity>
      <relation from="o2297" id="r296" name="with" negation="false" src="d0_s1" to="o2298" />
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia narrowly foliose.</text>
      <biological_entity id="o2299" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s2" value="foliose" value_original="foliose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves concave, 0.2–0.5 mm;</text>
      <biological_entity id="o2300" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base curved to insertion, somewhat decurrent;</text>
    </statement>
    <statement id="d0_s5">
      <text>supra-alar cells rectangular, smaller than alar cells.</text>
      <biological_entity id="o2301" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="insertion" src="d0_s4" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o2302" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rectangular" value_original="rectangular" />
        <character constraint="than alar cells" constraintid="o2303" is_modifier="false" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o2303" name="cell" name_original="cells" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Seta redbrown.</text>
      <biological_entity id="o2304" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule asymmetric, slightly curved, 0.5–1 mm;</text>
      <biological_entity id="o2305" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>exostome teeth bordered, cross-striolate basally, papillose distally;</text>
      <biological_entity constraint="exostome" id="o2306" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="bordered" value_original="bordered" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s8" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>endostome basal membrane high, segments nearly same length as exostome teeth, cilia in groups of 2.</text>
      <biological_entity id="o2307" name="endostome" name_original="endostome" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o2308" name="membrane" name_original="membrane" src="d0_s9" type="structure">
        <character is_modifier="false" name="height" src="d0_s9" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o2309" name="segment" name_original="segments" src="d0_s9" type="structure" />
      <biological_entity constraint="exostome" id="o2310" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
      <biological_entity id="o2311" name="cilium" name_original="cilia" src="d0_s9" type="structure" />
      <biological_entity id="o2312" name="group" name_original="groups" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o2309" id="r297" name="as" negation="false" src="d0_s9" to="o2310" />
      <relation from="o2311" id="r298" name="in" negation="false" src="d0_s9" to="o2312" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature early winter.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="early winter" from="early winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, usually somewhat shaded, near streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="shaded" modifier="usually somewhat" />
        <character name="habitat" value="near streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ga., Ky., N.J., N.Y., N.C., Pa., S.C., Tenn., Vt., Va.; Mexico; Central America (Honduras); Europe; Asia (Borneo, sw China, India, Sikkim); Pacific Islands (Hawaii, Philippines).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Honduras)" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Borneo)" establishment_means="native" />
        <character name="distribution" value="Asia (sw China)" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Asia (Sikkim)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Philippines)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As shown in the synonymy, Hageniella micans has been baffling in its generic and familial placement. Specimens from Mexico and British Columbia tend to be larger than those from eastern North America. The habitat of H. micans resembles that of many species of Hygrohypnum, but it is not aquatic. Vegetatively, the species strongly resembles Sematophyllum, but Hageniella may be distinguished by the capsule shrinking below the mouth when mature, whether operculate or inoperculate. Also, the exothecial cells of the somewhat similar Brotherella are collenchymatous, while in Hageniella the walls are thickened but not collenchymatous.</discussion>
  
</bio:treatment>