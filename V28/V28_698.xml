<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Wilfred B. Schofield†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">405</other_info_on_meta>
    <other_info_on_meta type="mention_page">408</other_info_on_meta>
    <other_info_on_meta type="mention_page">431</other_info_on_meta>
    <other_info_on_meta type="mention_page">653</other_info_on_meta>
    <other_info_on_meta type="mention_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Bescherelle" date="unknown" rank="genus">MYUROCLADA</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Sci. Nat., Bot., sér.</publication_title>
      <place_in_publication>7, 17: 379. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus MYUROCLADA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek mys, mouse, oura, tail, and clados, branch, alluding to resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">121551</other_info_on_name>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized, in loose mats, pale silvery green.</text>
      <biological_entity id="o24116" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="pale silvery" value_original="pale silvery" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24117" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o24116" id="r3428" name="in" negation="false" src="d0_s0" to="o24117" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping to erect-arcuate, often attenuate, strongly julaceous, unbranched to weakly irregularly branched, branches terete-foliate;</text>
      <biological_entity id="o24118" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="course_or_shape" src="d0_s1" value="erect-arcuate" value_original="erect-arcuate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character char_type="range_value" from="unbranched" name="architecture" src="d0_s1" to="weakly irregularly branched" />
      </biological_entity>
      <biological_entity id="o24119" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>central strand present;</text>
      <biological_entity constraint="central" id="o24120" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia orbicular-triangular;</text>
      <biological_entity id="o24121" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="orbicular-triangular" value_original="orbicular-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary hairs of 2–4 cells.</text>
      <biological_entity constraint="axillary" id="o24122" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o24123" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s4" to="4" />
      </biological_entity>
      <relation from="o24122" id="r3429" name="consist_of" negation="false" src="d0_s4" to="o24123" />
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves erect-appressed, crowded, imbricate, orbicular to broadly ovate, concave, not or indistinctly plicate;</text>
      <biological_entity id="o24124" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s5" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s5" to="broadly ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="not; indistinctly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s5" value="plicate" value_original="plicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base moderately decurrent;</text>
      <biological_entity id="o24125" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins nearly entire to serrulate;</text>
      <biological_entity id="o24126" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="nearly entire" name="architecture_or_shape" src="d0_s7" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex very broadly acute to rounded, short-apiculate when young, blunt when mature;</text>
      <biological_entity id="o24127" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="very broadly acute" name="shape" src="d0_s8" to="rounded" />
        <character is_modifier="false" modifier="when young" name="architecture_or_shape" src="d0_s8" value="short-apiculate" value_original="short-apiculate" />
        <character is_modifier="false" modifier="when mature" name="shape" src="d0_s8" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costa to mid leaf or slightly beyond, moderately stout, terminal spine absent;</text>
      <biological_entity constraint="mid" id="o24129" name="leaf" name_original="leaf" src="d0_s9" type="structure" />
      <biological_entity constraint="terminal" id="o24130" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="moderately" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity constraint="mid" id="o24131" name="leaf" name_original="leaf" src="d0_s9" type="structure" />
      <biological_entity constraint="terminal" id="o24132" name="spine" name_original="spine" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="moderately" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o24128" id="r3430" name="to" negation="false" src="d0_s9" to="o24129" />
      <relation from="o24128" id="r3431" name="to" negation="false" src="d0_s9" to="o24130" />
      <relation from="o24130" id="r3432" name="to" negation="false" src="d0_s9" to="o24131" />
      <relation from="o24130" id="r3433" name="to" negation="false" src="d0_s9" to="o24132" />
    </statement>
    <statement id="d0_s10">
      <text>alar cells smaller than basal juxtacostal cells;</text>
      <biological_entity id="o24128" name="costa" name_original="costa" src="d0_s9" type="structure" />
      <biological_entity id="o24133" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character constraint="than basal cells" constraintid="o24134" is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24134" name="cell" name_original="cells" src="d0_s10" type="structure" />
      <biological_entity id="o24135" name="cell" name_original="cells" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>laminal cells short-rhombic to rhombic.</text>
      <biological_entity constraint="laminal" id="o24136" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="short-rhombic" name="shape" src="d0_s11" to="rhombic" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Branch leaves similar.</text>
    </statement>
    <statement id="d0_s13">
      <text>Sexual condition dioicous.</text>
    </statement>
    <statement id="d0_s14">
      <text>[Seta redbrown, smooth. Capsule inclined to horizontal, nearly black when old, oblong, weakly curved; annulus separating in fragments; operculum long-conic, obliquely rostrate; peristome xerocastique, perfect. Calyptra naked. Spores 12–16 µm].</text>
      <biological_entity constraint="branch" id="o24137" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska, e Asia (Japan, Korea, Russia); introduced in Europe. The above description of the sporophyte is from Asian material.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="e Asia (Korea)" establishment_means="native" />
        <character name="distribution" value="e Asia (Russia)" establishment_means="native" />
        <character name="distribution" value="in Europe. The above description of the sporophyte is from Asian material" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>The above description of the sporophyte is from Asian material.</discussion>
  <references>
    <reference>Steere, W. C. and W. B. Schofield. 1956. Myuroclada, a genus new to North America. Bryologist 59: 1–5.</reference>
  </references>
  
</bio:treatment>