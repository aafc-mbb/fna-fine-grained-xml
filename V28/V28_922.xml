<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William D. Reese†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">584</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">cryphaeaceae</taxon_name>
    <taxon_name authority="Dozy &amp; Molkenboer" date="unknown" rank="genus">SCHOENOBRYUM</taxon_name>
    <place_of_publication>
      <publication_title>Musc. Frond. Ined. Archip. Ind.</publication_title>
      <place_in_publication>6: 183, plate 60. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cryphaeaceae;genus SCHOENOBRYUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek schoinos, rope, and bryon, moss, alluding to appearance of stems, particularly when dry</other_info_on_name>
    <other_info_on_name type="fna_id">129629</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small, brownish, sometimes with reddish tinge.</text>
      <biological_entity id="o12024" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with secondary stems ascending, branches elongate [usually short], irregularly branched;</text>
      <biological_entity id="o12025" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity constraint="secondary" id="o12026" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o12027" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o12025" id="r1689" name="with" negation="false" src="d0_s1" to="o12026" />
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia and pseudoparaphyllia absent.</text>
      <biological_entity id="o12028" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12029" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves tightly imbricate when dry, widespreading when moist;</text>
      <biological_entity id="o12030" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="widespreading" value_original="widespreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane [to revolute];</text>
      <biological_entity id="o12031" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>medial laminal cells smooth to prorulose.</text>
    </statement>
    <statement id="d0_s6">
      <text>Sexual condition autoicous;</text>
      <biological_entity constraint="medial laminal" id="o12032" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>perichaetia terminal, inner leaves awned, awns nearly smooth to denticulate.</text>
      <biological_entity id="o12033" name="perichaetium" name_original="perichaetia" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12034" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o12035" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta very short.</text>
      <biological_entity id="o12036" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule immersed;</text>
      <biological_entity id="o12037" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s9" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>peristome single;</text>
      <biological_entity id="o12038" name="peristome" name_original="peristome" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>exostome teeth narrowly triangular.</text>
      <biological_entity constraint="exostome" id="o12039" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Calyptra conic, mostly smooth, somewhat papillose distally.</text>
      <biological_entity id="o12040" name="calyptra" name_original="calyptra" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="somewhat; distally" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide, except Pacific Islands; tropical and subtropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="except Pacific Islands" establishment_means="native" />
        <character name="distribution" value="tropical and subtropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 24 (1 in the flora).</discussion>
  
</bio:treatment>