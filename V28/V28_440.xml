<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Berkeley" date="unknown" rank="genus">platydictya</taxon_name>
    <taxon_name authority="(Bridel) H. A. Crum" date="1964" rank="species">jungermannioides</taxon_name>
    <place_of_publication>
      <publication_title>Michigan Bot.</publication_title>
      <place_in_publication>3: 60. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus platydictya;species jungermannioides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250062344</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="species">jungermannioides</taxon_name>
    <place_of_publication>
      <publication_title>Muscol. Recent., suppl.</publication_title>
      <place_in_publication>2: 255. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species jungermannioides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblystegiella</taxon_name>
    <taxon_name authority="(Spruce) Loeske" date="unknown" rank="species">sprucei</taxon_name>
    <taxon_hierarchy>genus amblystegiella;species sprucei</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants dense, soft, silky, green to yellowbrown.</text>
      <biological_entity id="o14924" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="silky" value_original="silky" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="yellowbrown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems with branching angle narrow, branches easily detached;</text>
      <biological_entity id="o14925" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o14926" name="angle" name_original="angle" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="branching" value_original="branching" />
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o14927" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="easily" name="fusion" src="d0_s1" value="detached" value_original="detached" />
      </biological_entity>
      <relation from="o14925" id="r2107" name="with" negation="false" src="d0_s1" to="o14926" />
    </statement>
    <statement id="d0_s2">
      <text>pseudoparaphyllia absent;</text>
      <biological_entity id="o14928" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rhizoids axillary, purplish and granular-papillose at least when young.</text>
      <biological_entity id="o14929" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="when young" name="relief" src="d0_s3" value="granular-papillose" value_original="granular-papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves erect-spreading or sometimes ± secund, 0.1–0.3 (–0.5) mm;</text>
      <biological_entity id="o14930" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="sometimes more or less" name="architecture" src="d0_s4" value="secund" value_original="secund" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base not or somewhat narrowed to insertion;</text>
      <biological_entity id="o14931" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="insertion" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins serrulate, especially at insertion;</text>
    </statement>
    <statement id="d0_s7">
      <text>alar cells subquadrate, region of 3–7 cells;</text>
      <biological_entity id="o14932" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o14933" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subquadrate" value_original="subquadrate" />
      </biological_entity>
      <biological_entity id="o14934" name="region" name_original="region" src="d0_s7" type="structure" />
      <biological_entity id="o14935" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <relation from="o14934" id="r2108" name="consist_of" negation="false" src="d0_s7" to="o14935" />
    </statement>
    <statement id="d0_s8">
      <text>distal laminal cells shortly oblong-rhomboid, 3–4: 1.</text>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction by axillary obcuneate propagula.</text>
      <biological_entity constraint="axillary" id="o14937" name="propagulum" name_original="propagula" src="d0_s9" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="shape" src="d0_s9" value="obcuneate" value_original="obcuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="distal laminal" id="o14936" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s8" value="oblong-rhomboid" value_original="oblong-rhomboid" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="4" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaf margins ciliate-dentate distally.</text>
      <biological_entity constraint="leaf" id="o14938" name="margin" name_original="margins" src="d0_s11" type="structure" constraint_original="perichaetial leaf">
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s11" value="ciliate-dentate" value_original="ciliate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta 0.6–1.1 cm.</text>
      <biological_entity id="o14939" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s12" to="1.1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule erect, oblong-cylindric, symmetric or nearly so, 0.5–1 mm, contracted below mouth and at neck when dry, neck short;</text>
      <biological_entity id="o14940" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="symmetric" value_original="symmetric" />
        <character name="architecture_or_shape" src="d0_s13" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character constraint="below mouth" constraintid="o14941" is_modifier="false" name="condition_or_size" src="d0_s13" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o14941" name="mouth" name_original="mouth" src="d0_s13" type="structure" />
      <biological_entity id="o14942" name="neck" name_original="neck" src="d0_s13" type="structure" />
      <biological_entity id="o14943" name="neck" name_original="neck" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="when dry" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
      </biological_entity>
      <relation from="o14940" id="r2109" name="at" negation="false" src="d0_s13" to="o14942" />
    </statement>
    <statement id="d0_s14">
      <text>stomata in neck;</text>
      <biological_entity id="o14944" name="stoma" name_original="stomata" src="d0_s14" type="structure" />
      <biological_entity id="o14945" name="neck" name_original="neck" src="d0_s14" type="structure" />
      <relation from="o14944" id="r2110" name="in" negation="false" src="d0_s14" to="o14945" />
    </statement>
    <statement id="d0_s15">
      <text>annulus 2-seriate;</text>
      <biological_entity id="o14946" name="annulus" name_original="annulus" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s15" value="2-seriate" value_original="2-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>operculum convex-conic, stoutly mammillate to ± rostellate;</text>
      <biological_entity id="o14947" name="operculum" name_original="operculum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="convex-conic" value_original="convex-conic" />
        <character char_type="range_value" from="stoutly mammillate" name="shape" src="d0_s16" to="more or less rostellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome teeth whitish yellow;</text>
      <biological_entity constraint="exostome" id="o14948" name="tooth" name_original="teeth" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="whitish yellow" value_original="whitish yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia 1 or 2, rudimentary to well developed.</text>
      <biological_entity constraint="endostome" id="o14949" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s18" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="prominence" src="d0_s18" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s18" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 11–13 µm.</text>
      <biological_entity id="o14950" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s19" to="13" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock in damp, sheltered places, crevices of cliffs, under rock ledges, peaty soil, humus under overhanging turf, hollows under roots of trees, lower sides of logs, calcareous habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" constraint="in damp" />
        <character name="habitat" value="damp" />
        <character name="habitat" value="sheltered places" />
        <character name="habitat" value="crevices" constraint="of cliffs" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rock ledges" modifier="under" />
        <character name="habitat" value="peaty soil" />
        <character name="habitat" value="turf" modifier="humus under overhanging" />
        <character name="habitat" value="roots" modifier="hollows under" constraint="of trees" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="lower sides" constraint="of" />
        <character name="habitat" value="calcareous habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Alaska, Ariz., Ark., Calif., Colo., Idaho, Iowa, Mich., Mont., N.Mex., N.Y., N.Dak., Vt., Wash., Wyo.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Platydictya jungermannioides is easily recognized by its small size, absent or almost absent leaf costa, easily detached branches, and axillary rhizoids that are purplish and granular-papillose at least when young. The branching and rhizoid characters differentiate P. jungermannioides from the other three species of Platydictya.</discussion>
  
</bio:treatment>