<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William D. Reese†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">557</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">558</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">PLATYGYRIUM</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>5: 95, plate 458. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus PLATYGYRIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek platys, broad, and gyros, circle, alluding to wide annulus</other_info_on_name>
    <other_info_on_name type="fna_id">125794</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to large, in thin, intricate mats, yellowish to brownish or darker, glossy.</text>
      <biological_entity id="o1386" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="large" />
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s0" to="brownish or darker" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1387" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="arrangement" src="d0_s0" value="intricate" value_original="intricate" />
      </biological_entity>
      <relation from="o1386" id="r183" name="in" negation="false" src="d0_s0" to="o1387" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, subpinnate;</text>
      <biological_entity id="o1388" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subpinnate" value_original="subpinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present, weak;</text>
      <biological_entity id="o1389" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o1390" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="fragility" src="d0_s2" value="weak" value_original="weak" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia filamentous or broader.</text>
      <biological_entity id="o1391" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="filamentous" value_original="filamentous" />
        <character is_modifier="false" name="width" src="d0_s3" value="broader" value_original="broader" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves similar, ascending-imbricate to homomallous, ovate-acuminate, not to somewhat plicate;</text>
      <biological_entity id="o1392" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character constraint="to homomallous" constraintid="o1394" is_modifier="false" name="arrangement" src="d0_s4" value="ascending-imbricate" value_original="ascending-imbricate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character is_modifier="false" modifier="not to somewhat" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1393" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="to homomallous" constraintid="o1394" is_modifier="false" name="arrangement" src="d0_s4" value="ascending-imbricate" value_original="ascending-imbricate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="ovate-acuminate" value_original="ovate-acuminate" />
        <character is_modifier="false" modifier="not to somewhat" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o1394" name="homomallou" name_original="homomallous" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>base not decurrent;</text>
      <biological_entity id="o1395" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins somewhat to strongly recurved, entire to rarely slightly serrulate distally;</text>
      <biological_entity id="o1396" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="somewhat to strongly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" modifier="distally" name="architecture_or_shape" src="d0_s6" to="rarely slightly serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex acute to slenderly acuminate;</text>
      <biological_entity id="o1397" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="slenderly acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa double, short, often indistinct;</text>
    </statement>
    <statement id="d0_s9">
      <text>alar cells differentiated, quadrate;</text>
      <biological_entity id="o1398" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character is_modifier="false" modifier="often" name="prominence" src="d0_s8" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity id="o1399" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="quadrate" value_original="quadrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminal cells smooth.</text>
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction by dehiscent brood branchlets at branch apices.</text>
      <biological_entity id="o1401" name="brood" name_original="brood" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="true" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o1402" name="branchlet" name_original="branchlets" src="d0_s11" type="structure">
        <character is_modifier="true" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1403" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <relation from="o1401" id="r184" name="at" negation="false" src="d0_s11" to="o1403" />
      <relation from="o1402" id="r185" name="at" negation="false" src="d0_s11" to="o1403" />
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="laminal" id="o1400" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perichaetia small, inconspicuous.</text>
      <biological_entity id="o1404" name="perichaetium" name_original="perichaetia" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="small" value_original="small" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seta reddish.</text>
      <biological_entity id="o1405" name="seta" name_original="seta" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule erect, cylindric to subcylindric, symmetric or asymmetric, not contracted below mouth;</text>
      <biological_entity id="o1406" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="subcylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="asymmetric" value_original="asymmetric" />
        <character constraint="below mouth" constraintid="o1407" is_modifier="false" modifier="not" name="condition_or_size" src="d0_s15" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o1407" name="mouth" name_original="mouth" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>annulus sharply differentiated, 2-seriate or 3-seriate;</text>
      <biological_entity id="o1408" name="annulus" name_original="annulus" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="sharply" name="variability" src="d0_s16" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s16" value="2-seriate" value_original="2-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s16" value="3-seriate" value_original="3-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum obliquely rostrate;</text>
      <biological_entity id="o1409" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s17" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>peristome double;</text>
      <biological_entity id="o1410" name="peristome" name_original="peristome" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>exostome teeth with external surface finely papillose to papillose-striolate, smooth at apices;</text>
      <biological_entity constraint="exostome" id="o1411" name="tooth" name_original="teeth" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="papillose-striolate" value_original="papillose-striolate" />
        <character constraint="at apices" constraintid="o1413" is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="external" id="o1412" name="surface" name_original="surface" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s19" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o1413" name="apex" name_original="apices" src="d0_s19" type="structure" />
      <relation from="o1411" id="r186" name="with" negation="false" src="d0_s19" to="o1412" />
    </statement>
    <statement id="d0_s20">
      <text>endostome basal membrane low or absent, segments narrowly linear, nearly as long as teeth, jointed, cilia absent.</text>
      <biological_entity id="o1414" name="endostome" name_original="endostome" src="d0_s20" type="structure" />
      <biological_entity constraint="basal" id="o1415" name="membrane" name_original="membrane" src="d0_s20" type="structure">
        <character is_modifier="false" name="position" src="d0_s20" value="low" value_original="low" />
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1416" name="segment" name_original="segments" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s20" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="nearly" name="architecture" notes="" src="d0_s20" value="jointed" value_original="jointed" />
      </biological_entity>
      <biological_entity id="o1417" name="tooth" name_original="teeth" src="d0_s20" type="structure" />
      <biological_entity id="o1418" name="cilium" name_original="cilia" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Calyptra naked.</text>
      <biological_entity id="o1419" name="calyptra" name_original="calyptra" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Spores round, finely granular.</text>
      <biological_entity id="o1420" name="spore" name_original="spores" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="round" value_original="round" />
        <character is_modifier="false" modifier="finely" name="pubescence_or_relief_or_texture" src="d0_s22" value="granular" value_original="granular" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Europe, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 8 (2 in the flora).</discussion>
  <discussion>Platygyrium has concave leaves with yellowish bases. The alar region has vertical rows of cells with conspicuously thick walls. Platygyrium is superficially similar to Homomallium and Pylaisia. Fruiting specimens of these three similar genera are easily distinguished; the capsules of Homomallium are curved and asymmetric, often strongly so, in contrast to the erect symmetric capsules of Platygyrium and Pylaisia. The long, slender, rather obliquely rostrate operculum of Platygyrium (conspicuous when dry) distinguishes it from Pylaisia, in which the operculum is only conic or with a short blunt rostrum. Platygyrium and Pylaisia commonly grow on xylic substrates but Homomallium typically occurs on rock. Dehiscent branchlets clustered at the branch apices are very common in Platygyrium, particularly P. repens, but such branchlets also occur infrequently in Homomallium. Also, the leaf margins of Platygyrium are usually recurved, sometimes strongly so, while those of Homomallium and Pylaisia are erect. Recurvature of the leaf margins is not always well defined in P. repens but is usually conspicuous in P. fuscoluteum.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 0.8-1.1 mm, ascending to imbricate, not plicate; branches usually short, ascending, straight or curved; brood branchlets many, conspicuous.</description>
      <determination>1 Platygyrium repens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 1.3-1.6 mm, homomallous, somewhat plicate; branches elongate, creeping, straight; brood branchlets rare, inconspicuous.</description>
      <determination>2 Platygyrium fuscoluteum</determination>
    </key_statement>
  </key>
</bio:treatment>