<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">386</other_info_on_meta>
    <other_info_on_meta type="illustration_page">383</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Vanderpoorten" date="unknown" rank="family">calliergonaceae</taxon_name>
    <taxon_name authority="Hedenas" date="unknown" rank="genus">hamatocaulis</taxon_name>
    <taxon_name authority="(Norrlin) Hedenas" date="1989" rank="species">lapponicus</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>15: 30. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calliergonaceae;genus hamatocaulis;species lapponicus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099132</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="(Norrlin) Smirnova" date="unknown" rank="species">lycopodioides</taxon_name>
    <taxon_name authority="Norrlin" date="unknown" rank="variety">lapponicum</taxon_name>
    <place_of_publication>
      <publication_title>Not. Sällsk. Fauna Fl. Fenn. Förh.</publication_title>
      <place_in_publication>13: 293. 1873 (as lapponica)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species lycopodioides;variety lapponicum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drepanocladus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lapponicus</taxon_name>
    <taxon_hierarchy>genus drepanocladus;species lapponicus</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large to very large, occasionally smaller, turgid, variegated red and green, brownish red, blackish red, or sometimes green throughout.</text>
      <biological_entity id="o14951" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large to very" value_original="large to very" />
        <character is_modifier="false" modifier="very" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" modifier="occasionally" name="size" src="d0_s0" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="shape" src="d0_s0" value="turgid" value_original="turgid" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="variegated red and green" value_original="variegated red and green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish red" value_original="brownish red" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="blackish red" value_original="blackish red" />
        <character is_modifier="false" modifier="sometimes; throughout" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely and irregularly branched, shoot apices sometimes hooked.</text>
      <biological_entity id="o14952" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o14953" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves often with red pigment present in large parts of leaf or sometimes only in transverse subbasal zone, broadly ovate, concave or strongly so, plicate or not, 0.8–2 mm wide;</text>
      <biological_entity id="o14954" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character name="shape" src="d0_s2" value="strongly" value_original="strongly" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="not" value_original="not" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14955" name="part" name_original="parts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o14956" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity constraint="subbasal" id="o14957" name="zone" name_original="zone" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o14958" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
      <biological_entity constraint="subbasal" id="o14959" name="zone" name_original="zone" src="d0_s2" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s2" value="transverse" value_original="transverse" />
      </biological_entity>
      <relation from="o14954" id="r2111" modifier="with red pigment present" name="in" negation="false" src="d0_s2" to="o14955" />
      <relation from="o14955" id="r2112" name="part_of" negation="false" src="d0_s2" to="o14956" />
      <relation from="o14955" id="r2113" name="part_of" negation="false" src="d0_s2" to="o14957" />
      <relation from="o14955" id="r2114" name="part_of" negation="false" src="d0_s2" to="o14958" />
      <relation from="o14955" id="r2115" name="part_of" negation="false" src="d0_s2" to="o14959" />
    </statement>
    <statement id="d0_s3">
      <text>base patent, distinctly constricted at insertion;</text>
      <biological_entity id="o14960" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="patent" value_original="patent" />
        <character is_modifier="false" modifier="distinctly" name="insertion" src="d0_s3" value="constricted" value_original="constricted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apex acute or acuminate.</text>
      <biological_entity id="o14961" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, mesotrophic, often spring-influenced mires, lakeshores, submerged in lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="mesotrophic" />
        <character name="habitat" value="spring-influenced mires" modifier="often" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="lakes" modifier="submerged in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1100 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Yukon; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hamatocaulis lapponicus is rare in western parts of the flora area. The species is recognized by its large size (approximately as large as Scorpidium scorpioides), usually slightly plicate stem leaves without differentiated alar cells, and stem with neither central strand nor hyalodermis. Besides the larger size, H. lapponicus differs from H. vernicosus in its more or less patent stem leaf bases, and in stem leaves broadly ovate and more distinctly constricted at their insertion. Hamatocaulis lapponicus is usually more sparsely and irregularly branched than H. vernicosus, has somewhat less plicate leaves, and large parts of its shoots are more often red.</discussion>
  
</bio:treatment>