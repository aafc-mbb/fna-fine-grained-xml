<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Lars Hedenäs</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">288</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="mention_page">295</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">315</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">560</other_info_on_meta>
    <other_info_on_meta type="mention_page">645</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Sullivant) Mitten" date="unknown" rank="genus">CAMPYLIUM</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>12: 631. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus CAMPYLIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek campylos, curved, alluding to reflexed leaves</other_info_on_name>
    <other_info_on_name type="fna_id">105453</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Sullivant" date="unknown" rank="section">Campylium</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray, Manual ed.</publication_title>
      <place_in_publication>2, 677. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;section campylium</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small or medium-sized, green, yellowish green, golden brown, golden yellow, or brownish.</text>
      <biological_entity id="o20670" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden brown" value_original="golden brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched, irregularly branched, or irregularly pinnate;</text>
      <biological_entity id="o20671" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand absent;</text>
      <biological_entity id="o20672" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o20673" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia absent or sometimes present;</text>
      <biological_entity id="o20674" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids or rhizoid initials on stem or abaxial costa insertion, rarely forming tomentum, slightly or strongly branched, smooth or slightly warty-papillose;</text>
      <biological_entity id="o20675" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly; strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s4" value="warty-papillose" value_original="warty-papillose" />
      </biological_entity>
      <biological_entity constraint="rhizoid" id="o20676" name="initial" name_original="initials" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="rarely; slightly; strongly" name="insertion" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s4" value="warty-papillose" value_original="warty-papillose" />
      </biological_entity>
      <biological_entity id="o20677" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o20678" name="costa" name_original="costa" src="d0_s4" type="structure" />
      <biological_entity id="o20679" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o20675" id="r2973" name="on" negation="false" src="d0_s4" to="o20677" />
      <relation from="o20676" id="r2974" name="on" negation="false" src="d0_s4" to="o20677" />
      <relation from="o20675" id="r2975" name="on" negation="false" src="d0_s4" to="o20678" />
      <relation from="o20676" id="r2976" name="on" negation="false" src="d0_s4" to="o20678" />
      <relation from="o20675" id="r2977" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o20679" />
      <relation from="o20676" id="r2978" modifier="rarely" name="forming" negation="false" src="d0_s4" to="o20679" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hair distal cells 1–5, hyaline.</text>
      <biological_entity constraint="axillary" id="o20680" name="hair" name_original="hair" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o20681" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves erect, spreading, or ± squarrose, occasionally distinctly falcate-secund, cordate, cordate-ovate, ovate, or rounded-triangular, not plicate, 1–4.6 mm;</text>
      <biological_entity id="o20682" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s6" value="squarrose" value_original="squarrose" />
        <character is_modifier="false" modifier="occasionally distinctly" name="arrangement" src="d0_s6" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate-ovate" value_original="cordate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-triangular" value_original="rounded-triangular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded-triangular" value_original="rounded-triangular" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s6" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base not or hardly decurrent;</text>
      <biological_entity id="o20683" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="hardly" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane, entire or slightly sinuate, limbidia absent;</text>
      <biological_entity id="o20684" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o20685" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex acuminate, acumen differentiated or not, furrowed;</text>
      <biological_entity id="o20686" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o20687" name="acumen" name_original="acumen" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character name="variability" src="d0_s9" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa double or single, ending below mid leaf;</text>
      <biological_entity id="o20689" name="leaf" name_original="leaf" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s10" value="mid" value_original="mid" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="single" value_original="single" />
      </biological_entity>
      <relation from="o20688" id="r2979" name="double" negation="false" src="d0_s10" to="o20689" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells differentiated, rectangular, quadrate, or sometimes transversely rectangular or short-rectangular, strongly inflated, hyaline, widest cells 17–30 µm wide, region distinct, quadrate, broadly ovate, ovate, or rectangular, along basal margins, reaching from margin 15–60% distance to costa at insertion;</text>
      <biological_entity id="o20688" name="costa" name_original="costa" src="d0_s10" type="structure" />
      <biological_entity id="o20690" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="sometimes transversely" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="widest" id="o20691" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" from_unit="um" name="width" src="d0_s11" to="30" to_unit="um" />
      </biological_entity>
      <biological_entity id="o20692" name="region" name_original="region" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20693" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <biological_entity id="o20694" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <biological_entity id="o20695" name="costa" name_original="costa" src="d0_s11" type="structure" />
      <relation from="o20692" id="r2980" name="along" negation="false" src="d0_s11" to="o20693" />
      <relation from="o20692" id="r2981" name="reaching from" negation="false" src="d0_s11" to="o20694" />
      <relation from="o20692" id="r2982" modifier="15-60%" name="to" negation="false" src="d0_s11" to="o20695" />
    </statement>
    <statement id="d0_s12">
      <text>medial laminal cells linear, rarely shortly so;</text>
      <biological_entity constraint="medial laminal" id="o20696" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>marginal cells 1-stratose.</text>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition dioicous or autoicous.</text>
      <biological_entity constraint="marginal" id="o20697" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="dioicous" value_original="dioicous" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule horizontal, cylindric, curved;</text>
      <biological_entity id="o20698" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome perfect;</text>
      <biological_entity id="o20699" name="peristome" name_original="peristome" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="perfect" value_original="perfect" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>exostome margins dentate or slightly dentate distally;</text>
      <biological_entity constraint="exostome" id="o20700" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="slightly; distally" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia usually 1–3, well developed, nodulose.</text>
      <biological_entity constraint="endostome" id="o20701" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s18" to="3" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s18" value="developed" value_original="developed" />
        <character is_modifier="false" name="shape" src="d0_s18" value="nodulose" value_original="nodulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores (10–) 11–24 µm.</text>
      <biological_entity id="o20702" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="atypical_some_measurement" src="d0_s19" to="11" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s19" to="24" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies (Haiti), Central America (Guatemala), Eurasia, Pacific Islands (New Zealand).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Haiti)" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 4 (4 in the flora).</discussion>
  <discussion>Campylium occurs in more or less mineral-rich wetlands that are mostly permanently wet or moist. Differences between members of this genus and those of 6. Campyliadelphus and 18. Campylophyllum are discussed with the latter two genera. Members of Drepanocladus with similarly oriented leaves have transversely triangular alar groups that extend from the margin 60–100% of the distance to the costa at the insertion. Other species of Drepanocladus may have approximately quadrate alar groups, but not ovate or rectangular and extending up the leaf margin as in Campylium. The typification of Campylium was discussed by P. Isoviita and L. Hedenäs (1997).</discussion>
  <references>
    <reference>Andrews, A. L. 1957. Taxonomic notes. XIII. The genus Campylium. Bryologist 60: 127–135.</reference>
    <reference>Hedenäs, L. 1997. A partial generic revision of Campylium (Musci). Bryologist 100: 65–88.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sexual condition autoicous; stem leaves erect or spreading; bases subsheathing, broadly cordate-ovate, ovate, or narrowly ovate; acumina when differentiated constituting at most 33% leaf length</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sexual condition dioicous; stem leaves spreading or ± squarrose; bases erect to erect-spreading, cordate or rounded-triangular; acumina frequently differentiated, constituting (33-)40-77% leaf length</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaves slightly concave, 1.8-2.4 mm; bases ± broadly cordate-ovate; leaves gradually narrowed to apex; acumina not differentiated.</description>
      <determination>1 Campylium laxifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaves concave or strongly so, 2.1-4.6 mm; bases ovate or narrowly ovate; leaves ± suddenly or more gradually narrowed to apex; acumina frequently differentiated.</description>
      <determination>2 Campylium longicuspis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems erect, irregularly branched or sometimes irregularly pinnate; stem leaves 1.7-2.8 × 0.7-1.2 mm; acumina constituting 40-65% (rarely 33-40% in Arctic plants) leaf length; paraphyllia absent.</description>
      <determination>3 Campylium stellatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems creeping, irregularly pinnate; stem leaves 1-2.3 × 0.4-1 mm; acumina constituting 55-77% leaf length; paraphyllia sometimes present.</description>
      <determination>4 Campylium protensum</determination>
    </key_statement>
  </key>
</bio:treatment>