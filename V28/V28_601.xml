<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">382</other_info_on_meta>
    <other_info_on_meta type="mention_page">380</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">thuidiaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">thuidium</taxon_name>
    <taxon_name authority="(Hedwig) Schimper in P. Bruch and W. P. Schimper" date="1852" rank="species">delicatulum</taxon_name>
    <place_of_publication>
      <publication_title>in P. Bruch and W. P. Schimper, Bryol. Europ.</publication_title>
      <place_in_publication>5: 164. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thuidiaceae;genus thuidium;species delicatulum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200002117</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">delicatulum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>260. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species delicatulum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thuidium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">recognitum</taxon_name>
    <taxon_name authority="(Hedwig) Warnstorf" date="unknown" rank="variety">delicatulum</taxon_name>
    <taxon_hierarchy>genus thuidium;species recognitum;variety delicatulum</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants green or yellowish-brown.</text>
      <biological_entity id="o3286" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish-brown" value_original="yellowish-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 3–8 cm, 2-pinnate or 3-pinnate, ± frondose;</text>
      <biological_entity id="o3287" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="3-pinnate" value_original="3-pinnate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="frondose" value_original="frondose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>paraphyllia usually papillose at cell ends.</text>
      <biological_entity id="o3288" name="paraphyllium" name_original="paraphyllia" src="d0_s2" type="structure">
        <character constraint="at cell ends" constraintid="o3289" is_modifier="false" modifier="usually" name="relief" src="d0_s2" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="cell" id="o3289" name="end" name_original="ends" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves ± erect, appressed when dry, erect-spreading when moist, triangular-ovate, not plicate, 0.6–1.5 mm;</text>
      <biological_entity id="o3290" name="leaf-stem" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="triangular-ovate" value_original="triangular-ovate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="distance" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins revolute throughout, papillose-serrulate;</text>
      <biological_entity id="o3291" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="papillose-serrulate" value_original="papillose-serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate;</text>
      <biological_entity id="o3292" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa ending well before apex;</text>
      <biological_entity id="o3293" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o3294" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o3293" id="r427" name="ending" negation="false" src="d0_s6" to="o3294" />
    </statement>
    <statement id="d0_s7">
      <text>distal laminal cells irregularly oblong-hexagonal or rhombic, 6–10 × 8–12 µm, papillae sometimes 2-fid, not appearing multipapillose.</text>
      <biological_entity constraint="distal laminal" id="o3295" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="oblong-hexagonal" value_original="oblong-hexagonal" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="6" from_unit="um" name="length" src="d0_s7" to="10" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="12" to_unit="um" />
      </biological_entity>
      <biological_entity id="o3296" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Branch leaves apical cell truncate, multipapillose.</text>
      <biological_entity constraint="branch" id="o3297" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="apical" id="o3298" name="cell" name_original="cell" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Primary branch leaves to 0.5 mm;</text>
      <biological_entity constraint="branch" id="o3299" name="leaf" name_original="leaves" src="d0_s9" type="structure" constraint_original="primary branch" />
    </statement>
    <statement id="d0_s10">
      <text>costa 1/2–2/3 leaf length.</text>
      <biological_entity id="o3300" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s10" to="2/3" />
      </biological_entity>
      <biological_entity id="o3301" name="leaf" name_original="leaf" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Secondary branch leaves 0.3 mm;</text>
      <biological_entity constraint="branch" id="o3302" name="leaf" name_original="leaves" src="d0_s11" type="structure" constraint_original="secondary branch">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apex acute;</text>
      <biological_entity id="o3303" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>costa to 1/2 leaf length.</text>
      <biological_entity id="o3304" name="costa" name_original="costa" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s13" to="1/2" />
      </biological_entity>
      <biological_entity id="o3305" name="leaf" name_original="leaf" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Perichaetial leaves to 5 mm, margins not ciliate to ciliate proximally, often denticulate distally.</text>
      <biological_entity constraint="perichaetial" id="o3306" name="leaf" name_original="leaves" src="d0_s14" type="structure" />
      <biological_entity id="o3307" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="to 5 mm; not" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="proximally" name="pubescence_or_shape_or_architecture" src="d0_s14" value="ciliate to ciliate" value_original="ciliate to ciliate" />
        <character is_modifier="false" modifier="often; distally" name="shape" src="d0_s14" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seta 1.5–4.5 cm.</text>
      <biological_entity id="o3308" name="seta" name_original="seta" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s15" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Capsule 1.8–4 mm;</text>
      <biological_entity id="o3309" name="capsule" name_original="capsule" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum 0.7–2 mm, long-rostrate;</text>
      <biological_entity id="o3310" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="long-rostrate" value_original="long-rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia in groups of 2 or 3.</text>
      <biological_entity constraint="endostome" id="o3311" name="cilium" name_original="cilia" src="d0_s18" type="structure" />
      <biological_entity id="o3312" name="group" name_original="groups" src="d0_s18" type="structure" />
      <relation from="o3311" id="r428" modifier="of 2 or 3" name="in" negation="false" src="d0_s18" to="o3312" />
    </statement>
    <statement id="d0_s19">
      <text>Spores 12–24 µm, smooth.</text>
      <biological_entity id="o3313" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s19" to="24" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, n South America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="n South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 10 (2 in the flora).</discussion>
  <discussion>Thuidium delicatulum, if confused with T. recognitum, can be recognized by the stem leaves erect or erect-spreading when moist, not plicate and rather gradually acuminate, with margins recurved to the base of the acumen and costa ending well before the apex. The paraphyllia have small papillae along the cell midpoints. The laminal cells are stoutly 1-papillose, but often, especially in the South, the papillae are 2-fid. The branch leaf laminal cells are rhombic, 6–8 × 8–12 µm, with curved papillae. The operculum is long-rostrate.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaf apices not ending in hyaline point of 1-seriate cells; perichaetial leaf margins ciliate proximally.</description>
      <determination>4a Thuidium delicatulum var. delicatulum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem leaf apices ending in hyaline point of 1-seriate cells; perichaetial leaf margins usually not ciliate, sometimes sparsely so.</description>
      <determination>4b Thuidium delicatulum var. radicans</determination>
    </key_statement>
  </key>
</bio:treatment>