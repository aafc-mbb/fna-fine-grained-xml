<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
    <other_info_on_meta type="mention_page">420</other_info_on_meta>
    <other_info_on_meta type="illustration_page">432</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Grout" date="unknown" rank="genus">cirriphyllum</taxon_name>
    <taxon_name authority="(Hedwig) Grout" date="1898" rank="species">piliferum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 225. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus cirriphyllum;species piliferum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099067</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">piliferum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>275. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species piliferum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants soft, glossy.</text>
      <biological_entity id="o5921" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 10 cm, branches to 15 mm, straight.</text>
      <biological_entity id="o5922" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5923" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves 1.8–2.6 × 1–1.5 mm;</text>
      <biological_entity id="o5924" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s2" to="2.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base slightly rounded to insertion;</text>
      <biological_entity id="o5925" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="insertion" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins recurved proximally, plane distally;</text>
    </statement>
    <statement id="d0_s5">
      <text>alar region triangular, abruptly delimited, reaching from margin 50% distance to costa;</text>
      <biological_entity id="o5926" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
      <biological_entity id="o5927" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="abruptly" name="prominence" src="d0_s5" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o5928" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <biological_entity id="o5929" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <relation from="o5927" id="r810" name="reaching from" negation="false" src="d0_s5" to="o5928" />
      <relation from="o5927" id="r811" modifier="50%" name="to" negation="false" src="d0_s5" to="o5929" />
    </statement>
    <statement id="d0_s6">
      <text>laminal cells 50–110 × 6–10 µm, walls sometimes strongly porose in proximal part of leaf.</text>
      <biological_entity constraint="laminal" id="o5930" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="50" from_unit="um" name="length" src="d0_s6" to="110" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s6" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity id="o5931" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character constraint="in proximal part" constraintid="o5932" is_modifier="false" modifier="sometimes strongly" name="architecture" src="d0_s6" value="porose" value_original="porose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5932" name="part" name_original="part" src="d0_s6" type="structure" />
      <biological_entity id="o5933" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <relation from="o5932" id="r812" name="part_of" negation="false" src="d0_s6" to="o5933" />
    </statement>
    <statement id="d0_s7">
      <text>Branch leaves to 2 × 0.7 mm.</text>
      <biological_entity constraint="branch" id="o5934" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="mm" value="2" value_original="2" />
        <character name="width" src="d0_s7" unit="mm" value="0.7" value_original="0.7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta 1–3 cm.</text>
      <biological_entity id="o5935" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule 2–2.5 mm.</text>
      <biological_entity id="o5936" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, humus, duff, decaying wood, limestone, mesic to wet forests, ravine slopes, tall herb vegetation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="humus" />
        <character name="habitat" value="duff" />
        <character name="habitat" value="decaying wood" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="wet forests" />
        <character name="habitat" value="ravine slopes" />
        <character name="habitat" value="tall herb vegetation" />
        <character name="habitat" value="mesic" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr., N.W.T., Ont.; Alaska, Conn., Maine, Mass., Mich., N.J., N.Y., N.C., Ohio, Pa., S.C., Tenn., Vt., Wis.; Europe; Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cirriphyllum piliferum is easy to recognize by its rather large size, usually pure green and somewhat whitish color, and regular pinnate branching. The concave-inflated and closely imbricate distal leaves are abruptly contracted into filiform acumens, resulting in a peculiar appearance when they stand at a sharp, broad angle from the upper shoot (when wet, as usually occurs in the field). When a shoot is examined against a dark background, the acumens are usually easily visible, allowing for ready field identification. Brachythecium (Cirriphyllum) cirrosum differs from C. piliferum by its more abruptly contracted, so more strongly piliferous, leaves, and branch leaves that do not much differ from stem leaves (in C. piliferum, the branch leaves are not or at least less strongly piliferous); the irregular branching (fairly regular in C. piliferum); the usual yellow color of the plants (usually green in C. piliferum); and rather small alar cells (enlarged in C. piliferum).</discussion>
  
</bio:treatment>