<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Terry T. McIntosh,Steven G. Newmaster</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">10</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">647</other_info_on_meta>
    <other_info_on_meta type="mention_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="T. J. Koponen" date="unknown" rank="genus">PLAGIOMNIUM</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>5: 145, figs. 14, 15, 20, 31, 32, 35, 36, 41, 43, 47, 60, 69, 78, 81, 98, 100. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus PLAGIOMNIUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek plagios, oblique, and mnion, moss, alluding to arching sterile stems</other_info_on_name>
    <other_info_on_name type="fna_id">125698</other_info_on_name>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (1–) 2–5 (–10) cm, dense or open mats.</text>
      <biological_entity id="o18150" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18151" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s0" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems green or yellow-green, usually brownish with age;</text>
      <biological_entity id="o18152" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-green" value_original="yellow-green" />
        <character constraint="with age" constraintid="o18153" is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o18153" name="age" name_original="age" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>stems of two types, fertile stems erect, branching distally or not, dendroid or not, sterile stems plagiotropic or arching, rarely erect, to 20 cm;</text>
      <biological_entity id="o18154" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o18155" name="type" name_original="types" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o18156" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally; distally; not" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="dendroid" value_original="dendroid" />
        <character name="architecture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o18157" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
      <relation from="o18154" id="r2610" name="consist_of" negation="false" src="d0_s2" to="o18155" />
    </statement>
    <statement id="d0_s3">
      <text>rhizoids brown, macronemata mainly proximal, occasionally along underside of sterile stems, micronemata present.</text>
      <biological_entity id="o18158" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
        <character is_modifier="false" name="presence" notes="" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o18159" name="rhizoid" name_original="rhizoids" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s3" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o18160" name="underside" name_original="underside" src="d0_s3" type="structure" />
      <biological_entity id="o18161" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o18158" id="r2611" modifier="occasionally" name="along" negation="false" src="d0_s3" to="o18160" />
      <relation from="o18160" id="r2612" name="part_of" negation="false" src="d0_s3" to="o18161" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves green or yellow-green, rarely black with age, variously crisped or contorted when dry, erect-spreading, usually flat, occasionally irregularly wavy or transversely undulate when moist, elliptic, obovate, oblong, oblong-lingulate, oblongelliptic, or rarely orbicular or diamond-shaped, 1–14 mm;</text>
      <biological_entity id="o18162" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellow-green" value_original="yellow-green" />
        <character constraint="with age" constraintid="o18163" is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="black" value_original="black" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s4" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s4" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s4" value="wavy" value_original="wavy" />
        <character is_modifier="false" modifier="when moist" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong-lingulate" value_original="oblong-lingulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="diamond--shaped" value_original="diamond--shaped" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s4" value="diamond--shaped" value_original="diamond--shaped" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18163" name="age" name_original="age" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>base decurrent or not;</text>
      <biological_entity id="o18164" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
        <character name="shape" src="d0_s5" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane, green or yellow-green, 1-stratose, toothed, often to near base, sometimes only distally, rarely entire, teeth single, sharp or blunt, of 1–2 (–4) cells, rarely hooked;</text>
      <biological_entity id="o18165" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-stratose" value_original="1-stratose" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="sometimes only; only distally; distally; rarely" name="architecture_or_shape" notes="" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18166" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o18167" name="tooth" name_original="teeth" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character is_modifier="false" name="shape" src="d0_s6" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="shape" src="d0_s6" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o18168" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <relation from="o18165" id="r2613" modifier="often" name="to" negation="false" src="d0_s6" to="o18166" />
      <relation from="o18167" id="r2614" name="consist_of" negation="false" src="d0_s6" to="o18168" />
    </statement>
    <statement id="d0_s7">
      <text>apex acute, acuminate, obtuse, rounded, truncate, retuse, or emarginate, mucronate, apiculate, or cuspidate, cusp toothed or not, rarely bent to one side;</text>
      <biological_entity id="o18169" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
      <biological_entity id="o18170" name="cusp" name_original="cusp" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character name="shape" src="d0_s7" value="not" value_original="not" />
        <character constraint="to side" constraintid="o18171" is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="bent" value_original="bent" />
      </biological_entity>
      <biological_entity id="o18171" name="side" name_original="side" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>costa percurrent, excurrent, or rarely subpercurrent, distal abaxial surface smooth;</text>
      <biological_entity id="o18172" name="costa" name_original="costa" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s8" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
      <biological_entity constraint="distal abaxial" id="o18173" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>medial laminal cells short-elongate, elongate, or ± isodiametric, (15–) 30–70 (–85) µm, sometimes in diagonal or longitudinal rows, collenchymatous or not, walls pitted or not;</text>
      <biological_entity constraint="medial laminal" id="o18174" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-elongate" value_original="short-elongate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="isodiametric" value_original="isodiametric" />
        <character char_type="range_value" from="15" from_unit="um" name="atypical_some_measurement" src="d0_s9" to="30" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s9" to="85" to_unit="um" />
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s9" to="70" to_unit="um" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="collenchymatous" value_original="collenchymatous" />
        <character name="architecture" src="d0_s9" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="diagonal" id="o18175" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o18176" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character name="relief" src="d0_s9" value="not" value_original="not" />
      </biological_entity>
      <relation from="o18174" id="r2615" modifier="sometimes" name="in" negation="false" src="d0_s9" to="o18175" />
    </statement>
    <statement id="d0_s10">
      <text>marginal cells differentiated, linear or rhomboidal, in 2–4 (–5) rows.</text>
      <biological_entity id="o18178" name="row" name_original="rows" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="5" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o18177" id="r2616" name="in" negation="false" src="d0_s10" to="o18178" />
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction usually absent (by stolonlike stems in P. undulatum).</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition synoicous or dioicous.</text>
      <biological_entity constraint="marginal" id="o18177" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rhomboidal" value_original="rhomboidal" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta single or multiple, yellow, yellow-green, brown, sometimes reddish or greenish, rarely orange, dark red, or blackish with age, 1.2–5 cm, straight to flexuose.</text>
      <biological_entity id="o18179" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character is_modifier="false" name="quantity" src="d0_s13" value="multiple" value_original="multiple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s13" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark red" value_original="dark red" />
        <character constraint="with age" constraintid="o18180" is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" notes="" src="d0_s13" to="5" to_unit="cm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s13" to="flexuose" />
      </biological_entity>
      <biological_entity id="o18180" name="age" name_original="age" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Capsule horizontal to pendent, yellow or yellowish-brown, cylindric, oblong, oblong-cylindric, obovoid, or ovoid, 1.5–5 mm;</text>
      <biological_entity id="o18181" name="capsule" name_original="capsule" src="d0_s14" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s14" to="pendent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum conic-apiculate or rostrate;</text>
      <biological_entity id="o18182" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="conic-apiculate" value_original="conic-apiculate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome yellow or brown;</text>
      <biological_entity id="o18183" name="exostome" name_original="exostome" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome yellow to yellowish-brown.</text>
      <biological_entity id="o18184" name="endostome" name_original="endostome" src="d0_s17" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s17" to="yellowish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Spores 18–40 µm.</text>
      <biological_entity id="o18185" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s18" to="40" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 26 (11 in the flora).</discussion>
  <discussion>Plagiomnium is characterized by singly serrate, 1-stratose leaf margins, the absence of red stem tissue, and, in most species, the production of long sterile plagiotropic or arching stems. Plagiotropic stems are absent in P. venustum and some fertile patches of a few other species, in particular P. insigne. In many Plagiomnium populations, fertile stems are absent with plagiotropic stems producing vegetative mats over and among litter and other mosses. Morphological variation is common in Plagiomnium and has led to misidentifications in regional herbaria. Many characters that have been used in extant keys, such as sharpness, size, and cell composition of marginal teeth, presence and length of leaf decurrencies, and degree of laminal cell pitting, can vary within and between populations. Extant keys often use sexuality to separate species; unfortunately, many collections are sterile, and this character is of little use. As with Brachythecium, sterile collections of Plagiomnium may be unidentifiable, especially weakly developed plants from wetland habitats. Based on the review of the collections examined for this treatment, more species are likely present within the range of the flora, and a thorough review of North American Plagiomnium is recommended.</discussion>
  <discussion>Three species, Plagiomnium carolinianum, P. rostratum, and P. undulatum, have rostrate opercula; the remaining species have conic-apiculate opercula. Plagiomnium includes four species endemic to the flora area, P. carolinianum, P. ciliare, P. floridanum, and P. venustum, and one apparently introduced species, P. undulatum.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves usually oblong-lingulate or lingulate, 5-14 mm, transversely undulate when moist</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves usually elliptic, ovate, or obovate, often less than 6 mm, usually flat, rarely weakly transversely undulate when moist</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 5-8(-10) mm; apices retuse or emarginate, rarely truncate or rounded, usually short-mucronate; marginal teeth usually blunt; e North America.</description>
      <determination>1 Plagiomnium carolinianum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 6-10(-14) mm; apices obtuse or rounded, occasionally acute, usually cuspidate; marginal teeth usually sharp; w North America.</description>
      <determination>10 Plagiomnium undulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf margins toothed from about mid leaf to apex, teeth sharp; leaves obovate, sometimes diamond-shaped or elliptic</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf margins toothed from apex to near base and teeth sharp, or toothed mainly distally and teeth usually blunt; leaves never diamond-shaped, usually elliptic, rarely obovate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Medial laminal cells 30-50(-60) µm, not or weakly collenchymatous; setae often multiple.</description>
      <determination>4 Plagiomnium drummondii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Medial laminal cells 20-30(-40) µm, strongly collenchymatous; setae single</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves obovate, ± diamond-shaped or occasionally elliptic; bases broadly decurrent; medial laminal cells 20-30(-40) µm; sexual condition synoicous.</description>
      <determination>3 Plagiomnium cuspidatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves elliptic; bases narrowly decurrent; medial laminal cells 20-30 µm; sexual condition dioicous.</description>
      <determination>6 Plagiomnium floridanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plagiotropic sterile stems absent; leaves often densely twisted distally around stem when dry; laminal cells strongly collenchymatous, walls not pitted; capsule necks distinct, brown, often wrinkled.</description>
      <determination>11 Plagiomnium venustum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plagiotropic sterile stems usually present; leaves crisped and contorted, not twisted around stem when dry; laminal cells collenchymatous or not, walls pitted or not; capsule necks not distinct</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf apices often rounded, retuse, or emarginate; leaves broadly elliptic, ovate, orbicular, or oblong-elliptic; bases not or short-decurrent; marginal teeth blunt, usually of 1 cell (sometimes margins entire)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf apices usually acute or obtuse, sometimes rounded, rarely retuse; leaves elliptic, narrowly elliptic, occasionally ovate, oblong, obovate, or oblong-elliptic; bases long-decurrent; marginal teeth sharp or blunt, of 1-3(-4) cells</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Medial laminal cells usually elongate, (30-)50-65(-85) µm, distinctly smaller near margins, not or weakly collenchymatous, walls pitted, pits sometimes indistinct; sexual condition dioicous.</description>
      <determination>5 Plagiomnium ellipticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Medial laminal cells short-elongate or ± isodiametric, 22-35(-45) µm, slightly smaller near margins, strongly collenchymatous, walls not pitted; sexual condition synoicous.</description>
      <determination>9 Plagiomnium rostratum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf marginal teeth often blunt, of (1-)2-3(-4) cells; leaf apices rounded, occasionally truncate or obtuse, rarely retuse.</description>
      <determination>2 Plagiomnium ciliare</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf marginal teeth usually sharp, of 1-2(-3) cells; leaf apices usually acute, acuminate, or obtuse, sometimes rounded, or rarely retuse</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Medial laminal cells usually short-elongate or ± isodiametric, somewhat smaller near margins to about 1/2 size; sexual condition dioicous; w North America.</description>
      <determination>7 Plagiomnium insigne</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Medial laminal cells usually elongate, short-elongate, or occasionally ± isodiametric, less than 1/2 size near margins; sexual condition synoicous; widespread.</description>
      <determination>8 Plagiomnium medium</determination>
    </key_statement>
  </key>
</bio:treatment>