<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert R. Ireland Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">518</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">644</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="Ireland" date="unknown" rank="genus">BUCKIELLA</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>11: 55, figs. 1, 2. 2001</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus BUCKIELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">For William Russel Buck, b. 1950, American bryologist, and Latin - ella, diminutive</other_info_on_name>
    <other_info_on_name type="fna_id">202914</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants large, in thin to loose mats, light green or whitish, sometimes yellowish, dull or somewhat glossy.</text>
      <biological_entity id="o8297" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="whitish" value_original="whitish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8298" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o8297" id="r1159" name="in" negation="false" src="d0_s0" to="o8298" />
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping or sometimes erect, sparingly and irregularly branched, occasionally simple;</text>
      <biological_entity id="o8299" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparingly; irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis present, sometimes indistinct, central strand sometimes present;</text>
      <biological_entity id="o8300" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s2" value="indistinct" value_original="indistinct" />
      </biological_entity>
      <biological_entity constraint="central" id="o8301" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia absent.</text>
      <biological_entity id="o8302" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem and branch leaves similar, imbricate, somewhat contorted, strongly undulate, especially near apex, ovate to ovatelanceolate, rarely oblong-lanceolate, not plicate;</text>
      <biological_entity id="o8303" name="stem" name_original="stem" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_shape" src="d0_s4" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s4" to="ovatelanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o8304" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="somewhat" name="arrangement_or_shape" src="d0_s4" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s4" to="ovatelanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s4" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o8305" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o8303" id="r1160" modifier="especially" name="near" negation="false" src="d0_s4" to="o8305" />
      <relation from="o8304" id="r1161" modifier="especially" name="near" negation="false" src="d0_s4" to="o8305" />
    </statement>
    <statement id="d0_s5">
      <text>base weakly decurrent;</text>
      <biological_entity id="o8306" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s5" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins plane, entire or usually serrulate to serrate at apex;</text>
      <biological_entity id="o8307" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character constraint="at apex" constraintid="o8308" is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o8308" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>apex acute to acuminate, rarely somewhat obtuse;</text>
      <biological_entity id="o8309" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s7" to="acuminate" />
        <character is_modifier="false" modifier="rarely somewhat" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costa double, ending just beyond base, rarely one branch reaching 1/3 leaf length;</text>
      <biological_entity id="o8310" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity id="o8311" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o8313" name="leaf" name_original="leaf" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1/3" value_original="1/3" />
      </biological_entity>
      <relation from="o8310" id="r1162" name="ending" negation="false" src="d0_s8" to="o8311" />
      <relation from="o8312" id="r1163" modifier="rarely" name="reaching" negation="false" src="d0_s8" to="o8313" />
    </statement>
    <statement id="d0_s9">
      <text>alar cells differentiated, rectangular;</text>
      <biological_entity id="o8312" name="branch" name_original="branch" src="d0_s8" type="structure" />
      <biological_entity id="o8314" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rectangular" value_original="rectangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminal cells papillose, papillae minute, granular, cuticular, much more abundant on abaxial surface.</text>
      <biological_entity constraint="laminal" id="o8315" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8317" name="surface" name_original="surface" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s12">
      <text>Sexual condition dioicous;</text>
      <biological_entity id="o8316" name="papilla" name_original="papillae" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="minute" value_original="minute" />
        <character is_modifier="false" name="pubescence_or_relief_or_texture" src="d0_s10" value="granular" value_original="granular" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s10" value="cuticular" value_original="cuticular" />
        <character constraint="on abaxial surface" constraintid="o8317" is_modifier="false" modifier="much" name="quantity" src="d0_s10" value="abundant" value_original="abundant" />
        <character is_modifier="false" name="development" src="d0_s11" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perichaetial leaves narrowly ovate to oblong-lanceolate, apex acute to acuminate.</text>
      <biological_entity constraint="perichaetial" id="o8318" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character char_type="range_value" from="narrowly ovate" name="shape" src="d0_s13" to="oblong-lanceolate" />
      </biological_entity>
      <biological_entity id="o8319" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seta dark red, reddish, or light-brown.</text>
      <biological_entity id="o8320" name="seta" name_original="seta" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark red" value_original="dark red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule inclined to pendulous, cylindric, arcuate or sometimes straight, wrinkled and contracted below mouth when dry;</text>
      <biological_entity id="o8321" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character char_type="range_value" from="inclined" name="orientation" src="d0_s15" to="pendulous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="course_or_shape" src="d0_s15" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="relief" src="d0_s15" value="wrinkled" value_original="wrinkled" />
        <character constraint="below mouth" constraintid="o8322" is_modifier="false" name="condition_or_size" src="d0_s15" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o8322" name="mouth" name_original="mouth" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>annulus 1-seriate or 2-seriate, deciduous, cells large;</text>
      <biological_entity id="o8323" name="annulus" name_original="annulus" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s16" value="1-seriate" value_original="1-seriate" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s16" value="2-seriate" value_original="2-seriate" />
        <character is_modifier="false" name="duration" src="d0_s16" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o8324" name="cell" name_original="cells" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>operculum conic to rostrate;</text>
      <biological_entity id="o8325" name="operculum" name_original="operculum" src="d0_s17" type="structure">
        <character char_type="range_value" from="conic" name="shape" src="d0_s17" to="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>peristome double;</text>
      <biological_entity id="o8326" name="peristome" name_original="peristome" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>exostome teeth with fine striations between lamellae proximally, papillose distally;</text>
      <biological_entity constraint="exostome" id="o8327" name="tooth" name_original="teeth" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="distally" name="relief" notes="" src="d0_s19" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="between lamellae" constraintid="o8329" id="o8328" name="striation" name_original="striations" src="d0_s19" type="structure" constraint_original="between  lamellae, ">
        <character is_modifier="true" name="width" src="d0_s19" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o8329" name="lamella" name_original="lamellae" src="d0_s19" type="structure" />
      <relation from="o8327" id="r1164" name="with" negation="false" src="d0_s19" to="o8328" />
    </statement>
    <statement id="d0_s20">
      <text>endostome basal membrane rather high, segments broad, keeled, cilia 2 or 3, as long or nearly as long as segments.</text>
      <biological_entity id="o8330" name="endostome" name_original="endostome" src="d0_s20" type="structure" />
      <biological_entity constraint="basal" id="o8331" name="membrane" name_original="membrane" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="rather" name="height" src="d0_s20" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o8332" name="segment" name_original="segments" src="d0_s20" type="structure">
        <character is_modifier="false" name="width" src="d0_s20" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s20" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o8333" name="cilium" name_original="cilia" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s20" unit="or" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8334" name="segment" name_original="segments" src="d0_s20" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s20" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o8335" name="segment" name_original="segments" src="d0_s20" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s20" value="long" value_original="long" />
      </biological_entity>
      <relation from="o8333" id="r1165" name="as" negation="false" src="d0_s20" to="o8334" />
      <relation from="o8334" id="r1166" name="as" negation="false" src="d0_s20" to="o8335" />
    </statement>
    <statement id="d0_s21">
      <text>Calyptra naked.</text>
      <biological_entity id="o8336" name="calyptra" name_original="calyptra" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Spores globose to ovoid, smooth or minutely papillose.</text>
      <biological_entity id="o8337" name="spore" name_original="spores" src="d0_s22" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s22" to="ovoid" />
        <character is_modifier="false" name="relief" src="d0_s22" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s22" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Europe, Asia, Pacific Islands (Hawaii, New Guinea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Guinea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Buckiella occurs in terrestrial habitats in temperate, boreal, and rarely tropical woods. Buckiella draytonii (Sullivant) Ireland is endemic to the Hawaiian Islands.</discussion>
  
</bio:treatment>