<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">297</other_info_on_meta>
    <other_info_on_meta type="mention_page">293</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) G. Roth" date="unknown" rank="genus">drepanocladus</taxon_name>
    <taxon_name authority="Warnstorf" date="unknown" rank="species">latinervis</taxon_name>
    <place_of_publication>
      <publication_title>Beih. Bot. Centralbl.</publication_title>
      <place_in_publication>13: 416. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus drepanocladus;species latinervis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099086</other_info_on_name>
  </taxon_identification>
  <number>7.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small.</text>
      <biological_entity id="o4181" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small" value_original="small" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely and irregularly pinnate.</text>
      <biological_entity id="o4182" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves falcate-secund, ovate or broadly ovate, gradually narrowed to apex, ± strongly concave, 1.1–2.1 × 0.5–0.7 mm;</text>
      <biological_entity id="o4183" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character constraint="to apex" constraintid="o4184" is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="more or less strongly" name="shape" notes="" src="d0_s2" value="concave" value_original="concave" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s2" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4184" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>base erect or erectopatent, insertion slightly curved;</text>
      <biological_entity id="o4185" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erectopatent" value_original="erectopatent" />
        <character is_modifier="false" modifier="slightly" name="insertion" src="d0_s3" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins entire or very weakly and obtusely denticulate;</text>
      <biological_entity id="o4186" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="very weakly" value_original="very weakly" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate, acumen gradually differentiated, furrowed;</text>
      <biological_entity id="o4187" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o4188" name="acumen" name_original="acumen" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa single, ending just below mid leaf to 4/5 leaf length;</text>
      <biological_entity id="o4189" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o4190" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="just below" name="position" src="d0_s6" value="mid" value_original="mid" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="4/5" />
      </biological_entity>
      <relation from="o4189" id="r551" name="ending just below mid" negation="false" src="d0_s6" to="o4190" />
    </statement>
    <statement id="d0_s7">
      <text>alar region transversely triangular, transversely short-rectangular, or quadrate, reaching from margin 33–66% distance to costa;</text>
      <biological_entity id="o4191" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o4193" name="margin" name_original="margin" src="d0_s7" type="structure" />
      <biological_entity id="o4194" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <relation from="o4192" id="r552" name="reaching from" negation="false" src="d0_s7" to="o4193" />
      <relation from="o4192" id="r553" modifier="33-66%" name="to" negation="false" src="d0_s7" to="o4194" />
    </statement>
    <statement id="d0_s8">
      <text>ratio of medial laminal cell length (µm) to leaf length (mm) 37.6–45.6.</text>
      <biological_entity constraint="medial" id="o4195" name="laminal" name_original="laminal" src="d0_s8" type="structure" />
      <biological_entity constraint="medial" id="o4196" name="cell" name_original="cell" src="d0_s8" type="structure" />
      <biological_entity id="o4197" name="leaf" name_original="leaf" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="length" value_original="length" />
      </biological_entity>
      <relation from="o4195" id="r554" name="to" negation="false" src="d0_s8" to="o4197" />
      <relation from="o4196" id="r555" name="to" negation="false" src="d0_s8" to="o4197" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition dioicous.</text>
      <biological_entity id="o4192" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character char_type="range_value" from="37.6" name="quantity" src="d0_s8" to="45.6" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nunavut; Alaska; n Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">latinervus</other_name>
  <discussion>Considering its wide arctic Asian distribution, Drepanocladus latinervis is most likely more widespread in North America than the two known records indicate. The species is most similar to D. sordidus, which also reaches into the Arctic where D. latinervis occurs. Both species have relatively small alar groups, and the two can be safely separated only on the basis of the ratio of medial laminal cell length (µm) to leaf length (mm), which is much larger in D. latinervis than in any other Drepanocladus species, namely between 37.6–45.6. The few D. latinervis specimens seen, one of which is from North America, are small, with stem leaves 1.1–2.1 × 0.5–0.7 mm, but because D. sordidus is frequently small in the Arctic, plant size is not sufficient for their separation.</discussion>
  
</bio:treatment>