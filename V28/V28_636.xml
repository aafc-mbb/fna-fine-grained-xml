<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">409</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Ignatov &amp; Huttunen" date="unknown" rank="genus">brachytheciastrum</taxon_name>
    <taxon_name authority="(Flowers) Ignatov" date="2011" rank="species">delicatulum</taxon_name>
    <place_of_publication>
      <publication_title>Arctoa</publication_title>
      <place_in_publication>19: 30. 2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachytheciastrum;species delicatulum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="Flowers" date="unknown" rank="species">delicatulum</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>76: 287. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus brachythecium;species delicatulum</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants moderately small to medium-sized, in loose or moderately dense tufts, green to yellowish.</text>
      <biological_entity id="o10556" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="moderately small" name="size" src="d0_s0" to="medium-sized" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s0" to="yellowish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o10557" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="moderately" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o10556" id="r1500" name="in" negation="false" src="d0_s0" to="o10557" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 5 cm, creeping, terete-foliate, not julaceous, irregularly to regularly pinnate, branches to 10 (–15) mm, terete or subcomplanate-foliate.</text>
      <biological_entity id="o10558" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" modifier="irregularly to regularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o10559" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subcomplanate-foliate" value_original="subcomplanate-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves erect at base, densely to loosely arranged, often loosely arranged at shoot ends, falcate-secund, ovate or ovatelanceolate, slightly to moderately plicate, rarely not plicate, 1.4–2.5 × 0.3–0.5 (–0.7) mm;</text>
      <biological_entity id="o10560" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o10561" is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely to loosely" name="arrangement" notes="" src="d0_s2" value="arranged" value_original="arranged" />
        <character constraint="at shoot ends" constraintid="o10562" is_modifier="false" modifier="often loosely" name="arrangement" src="d0_s2" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s2" value="falcate-secund" value_original="falcate-secund" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" modifier="slightly to moderately" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character is_modifier="false" modifier="rarely not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s2" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10561" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="shoot" id="o10562" name="end" name_original="ends" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>margins plane or recurved in places (more commonly proximally), serrulate to base, rarely subentire;</text>
      <biological_entity id="o10563" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character constraint="in places" constraintid="o10564" is_modifier="false" name="orientation" src="d0_s3" value="recurved" value_original="recurved" />
        <character constraint="to base" constraintid="o10565" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s3" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s3" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o10564" name="place" name_original="places" src="d0_s3" type="structure" />
      <biological_entity id="o10565" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>apex gradually or abruptly long-acuminate, acumen often falcate or flexuose;</text>
      <biological_entity id="o10566" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s4" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
      <biological_entity id="o10567" name="acumen" name_original="acumen" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
        <character is_modifier="false" name="course" src="d0_s4" value="flexuose" value_original="flexuose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>costa to 50–75% leaf length, terminal abaxial spine small;</text>
      <biological_entity id="o10568" name="costa" name_original="costa" src="d0_s5" type="structure" />
      <biological_entity id="o10569" name="leaf" name_original="leaf" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="50-75%" name="character" src="d0_s5" value="length" value_original="length" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>alar cells subquadrate, 10–25 × 8–15 µm, region extensive, triangular, of 8–10 × 5–8 cells, fairly distinctly delimited;</text>
      <biological_entity constraint="terminal abaxial" id="o10570" name="spine" name_original="spine" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o10571" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subquadrate" value_original="subquadrate" />
        <character char_type="range_value" from="10" from_unit="um" name="length" src="d0_s6" to="25" to_unit="um" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s6" to="15" to_unit="um" />
      </biological_entity>
      <biological_entity id="o10572" name="region" name_original="region" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="extensive" value_original="extensive" />
        <character is_modifier="false" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="fairly distinctly" name="prominence" notes="" src="d0_s6" value="delimited" value_original="delimited" />
      </biological_entity>
      <biological_entity id="o10573" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <relation from="o10572" id="r1501" name="consist_of" negation="false" src="d0_s6" to="o10573" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cells linear, 50–90 × 45–10 µm; basal juxtacostal cells shorter, to 8–10 µm wide, indistinctly delimited.</text>
      <biological_entity constraint="laminal" id="o10574" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character char_type="range_value" from="50" from_unit="um" name="length" src="d0_s7" to="90" to_unit="um" />
        <character char_type="range_value" from="45" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="basal laminal" id="o10575" name="cell" name_original="cells" src="d0_s7" type="structure" />
      <biological_entity id="o10576" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="8" from_unit="um" name="width" src="d0_s7" to="10" to_unit="um" />
        <character is_modifier="false" modifier="indistinctly" name="prominence" src="d0_s7" value="delimited" value_original="delimited" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Seta reddish, 1–1.5 cm, smooth or slightly rough distally.</text>
      <biological_entity id="o10577" name="seta" name_original="seta" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly; distally" name="pubescence_or_relief" src="d0_s8" value="rough" value_original="rough" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsule inclined, sometimes slightly erect, reddish, ovate, 1–2 mm;</text>
      <biological_entity id="o10578" name="capsule" name_original="capsule" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="inclined" value_original="inclined" />
        <character is_modifier="false" modifier="sometimes slightly" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>endostome basal membrane 1/5–1/3 endostome length, cilia short to absent.</text>
      <biological_entity id="o10579" name="endostome" name_original="endostome" src="d0_s10" type="structure" />
      <biological_entity constraint="basal" id="o10580" name="membrane" name_original="membrane" src="d0_s10" type="structure">
        <character char_type="range_value" from="1/5" name="quantity" src="d0_s10" to="1/3" />
      </biological_entity>
      <biological_entity id="o10581" name="endostome" name_original="endostome" src="d0_s10" type="structure" />
      <biological_entity id="o10582" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="false" name="length" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores 10–15 µm.</text>
      <biological_entity id="o10583" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s11" to="15" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, damp to dry rock, shaded places, under overhanging rock and exposed tree roots</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="damp to dry rock" />
        <character name="habitat" value="places" modifier="shaded" constraint="under overhanging rock and exposed tree roots" />
        <character name="habitat" value="rock" modifier="under overhanging" />
        <character name="habitat" value="exposed tree roots" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (1500-1900 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1500" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachytheciastrum delicatulum is characterized by narrow leaves, many of them more than five times longer than wide, rather numerous alar cells, and, usually, an almost erect capsule with a reduced peristome. The branch leaves are lanceolate to linear. The species is known only from Millard County.</discussion>
  
</bio:treatment>