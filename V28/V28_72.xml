<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Hedwig" date="1801" rank="genus">orthotrichum</taxon_name>
    <taxon_name authority="Sullivant &amp; Lesquereux in W. S. Sullivant" date="1874" rank="species">hallii</taxon_name>
    <place_of_publication>
      <publication_title>in W. S. Sullivant, Icon. Musc., suppl.,</publication_title>
      <place_in_publication>63, plate 45. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus orthotrichum;species hallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250061872</other_info_on_name>
  </taxon_identification>
  <number>16.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 2.5 cm.</text>
      <biological_entity id="o19045" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o19046" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>leaves similar on vegetative and fertile stems.</text>
      <biological_entity id="o19047" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o19048" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="vegetative" value_original="vegetative" />
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
      </biological_entity>
      <relation from="o19047" id="r2740" name="on" negation="false" src="d0_s2" to="o19048" />
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves loosely erect-appressed when dry, elongate-lanceolate to lanceolate, 1.7–3.5 mm;</text>
      <biological_entity id="o19049" name="leaf-stem" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s3" value="erect-appressed" value_original="erect-appressed" />
        <character char_type="range_value" from="elongate-lanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="distance" src="d0_s3" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins revolute proximally, recurved to almost plane near apex, entire;</text>
      <biological_entity id="o19050" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character constraint="near apex" constraintid="o19051" is_modifier="false" modifier="almost" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19051" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex ± narrowly obtuse, rarely bluntly acute;</text>
      <biological_entity id="o19052" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less narrowly" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="rarely bluntly" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal laminal cells rectangular to short-rectangular, grading to quadrate at margins, walls thin, not nodose;</text>
      <biological_entity constraint="basal laminal" id="o19053" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="rectangular" name="shape" src="d0_s6" to="short-rectangular" />
        <character constraint="at margins" constraintid="o19054" is_modifier="false" name="shape" src="d0_s6" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o19054" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o19055" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="nodose" value_original="nodose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells 7–13 µm, 2-stratose, rarely with few 1-stratose areas, papillae 1–3 per cell, conic, small;</text>
      <biological_entity constraint="distal" id="o19056" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="um" name="some_measurement" src="d0_s7" to="13" to_unit="um" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="2-stratose" value_original="2-stratose" />
      </biological_entity>
      <biological_entity id="o19057" name="area" name_original="areas" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o19058" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per cell" constraintid="o19059" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s7" value="conic" value_original="conic" />
        <character is_modifier="false" name="size" src="d0_s7" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o19059" name="cell" name_original="cell" src="d0_s7" type="structure" />
      <relation from="o19056" id="r2741" modifier="rarely" name="with" negation="false" src="d0_s7" to="o19057" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells mostly to completely 2-stratose.</text>
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition gonioautoicous.</text>
      <biological_entity constraint="marginal" id="o19060" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly to completely" name="architecture" src="d0_s8" value="2-stratose" value_original="2-stratose" />
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seta 0.5–1 mm.</text>
      <biological_entity id="o19061" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule 1/2 emergent when dry, immersed when moist, oblong or oblong-ovate, 1–1.8 mm, strongly 8-ribbed to mid capsule or entire length, rarely with 8 very short intermediate ribs, sometimes constricted below mouth when dry;</text>
      <biological_entity id="o19062" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="when dry" name="location" src="d0_s12" value="emergent" value_original="emergent" />
        <character is_modifier="false" modifier="when moist" name="prominence" src="d0_s12" value="immersed" value_original="immersed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="length" src="d0_s12" value="8-ribbed" value_original="8-ribbed" />
        <character constraint="below mouth" constraintid="o19064" is_modifier="false" modifier="sometimes" name="size" notes="" src="d0_s12" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o19063" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="8" value_original="8" />
        <character is_modifier="true" modifier="very" name="height_or_length_or_size" src="d0_s12" value="short" value_original="short" />
        <character is_modifier="true" name="size" src="d0_s12" value="intermediate" value_original="intermediate" />
      </biological_entity>
      <biological_entity id="o19064" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
      <relation from="o19062" id="r2742" modifier="rarely" name="with" negation="false" src="d0_s12" to="o19063" />
    </statement>
    <statement id="d0_s13">
      <text>stomata immersed;</text>
      <biological_entity id="o19065" name="stoma" name_original="stomata" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="immersed" value_original="immersed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>peristome double;</text>
      <biological_entity id="o19066" name="peristome" name_original="peristome" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>prostome present, occasionally rudimentary;</text>
      <biological_entity id="o19067" name="prostome" name_original="prostome" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="occasionally" name="prominence" src="d0_s15" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>exostome teeth 8, sometimes irregularly split to 16, incurved becoming spreading or rarely reflexed when old, coarsely striate-reticulate or striate-papillose;</text>
      <biological_entity constraint="exostome" id="o19068" name="tooth" name_original="teeth" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="8" value_original="8" />
      </biological_entity>
      <biological_entity id="o19069" name="split" name_original="split" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character is_modifier="false" modifier="becoming" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="when old" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when old" name="orientation" src="d0_s16" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="coarsely" name="relief" src="d0_s16" value="striate-reticulate" value_original="striate-reticulate" />
        <character is_modifier="false" name="relief" src="d0_s16" value="striate-papillose" value_original="striate-papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>endostome segments 8, not well developed, of 1 row of cells, short, finely longitudinally striate.</text>
      <biological_entity constraint="endostome" id="o19070" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
        <character is_modifier="false" modifier="not well" name="development" src="d0_s17" value="developed" value_original="developed" />
        <character is_modifier="false" name="height_or_length_or_size" notes="" src="d0_s17" value="short" value_original="short" />
        <character is_modifier="false" modifier="finely longitudinally" name="coloration_or_pubescence_or_relief" src="d0_s17" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o19071" name="row" name_original="row" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o19072" name="cell" name_original="cells" src="d0_s17" type="structure" />
      <relation from="o19070" id="r2743" name="consist_of" negation="false" src="d0_s17" to="o19071" />
      <relation from="o19071" id="r2744" name="part_of" negation="false" src="d0_s17" to="o19072" />
    </statement>
    <statement id="d0_s18">
      <text>Calyptra oblong, smooth, sparsely hairy, hairs papillose.</text>
      <biological_entity id="o19073" name="calyptra" name_original="calyptra" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s18" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19074" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character is_modifier="false" name="relief" src="d0_s18" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 10–17 µm.</text>
      <biological_entity id="o19075" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="10" from_unit="um" name="some_measurement" src="d0_s19" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock, usually limestone or calcareous sandstone, granite, quartzite, basalt, trunks of deciduous trees, open pine forests, spruce-fir forests, deciduous scrub oak-maple forests, vertical canyon walls, shaded cliff faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="calcareous" modifier="usually" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="quartzite" />
        <character name="habitat" value="basalt" />
        <character name="habitat" value="trunks" constraint="of deciduous trees" />
        <character name="habitat" value="deciduous trees" />
        <character name="habitat" value="open pine forests" />
        <character name="habitat" value="spruce-fir forests" />
        <character name="habitat" value="deciduous scrub oak-maple forests" />
        <character name="habitat" value="vertical canyon walls" />
        <character name="habitat" value="shaded cliff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate to high elevations (200-3000 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="moderate" />
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="200" from_unit="m" constraint="moderate to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orthotrichum hallii is distinguished by leaves that are completely or nearly 2-stratose in their distal portion, and emergent, oblong, 8-ribbed capsules. The peristome teeth vary from ridged-striate to papillose-striate; the proximal portions of the teeth are often coarsely papillose with the distal portions having striae mixed with papillae, and the papillae are always small and conic.</discussion>
  
</bio:treatment>