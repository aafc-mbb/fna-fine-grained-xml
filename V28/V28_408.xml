<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kindberg" date="unknown" rank="family">pilotrichaceae</taxon_name>
    <taxon_name authority="(Müller Hal.) Mitten" date="unknown" rank="genus">CALLICOSTELLA</taxon_name>
    <place_of_publication>
      <publication_title>J. Proc. Linn. Soc., Bot., suppl.</publication_title>
      <place_in_publication>2: 136. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pilotrichaceae;genus CALLICOSTELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin callum, hardened or thick, costa, rib, and -ella, diminutive, alluding to strong costae</other_info_on_name>
    <other_info_on_name type="fna_id">105108</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="genus">Hookeria</taxon_name>
    <taxon_name authority="Müller Hal." date="unknown" rank="section">Callicostella</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Musc. Frond.</publication_title>
      <place_in_publication>2: 216. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hookeria;section callicostella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hookeria</taxon_name>
    <taxon_name authority="(Müller Hal.) Hampe" date="unknown" rank="subgenus">Callicostella</taxon_name>
    <taxon_hierarchy>genus hookeria;subgenus callicostella</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Schizomitrium</taxon_name>
    <place_of_publication>
      <publication_title>name rejected</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus schizomitrium</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to medium-sized, in flattened mats, sordid pale green, yellowish, brownish, sometimes bluish, dull.</text>
      <biological_entity id="o20124" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="small" name="size" src="d0_s0" to="medium-sized" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="sordid pale" value_original="sordid pale" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s0" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o20125" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o20124" id="r2892" name="in" negation="false" src="d0_s0" to="o20125" />
    </statement>
    <statement id="d0_s1">
      <text>Stems regularly or irregularly pinnate;</text>
      <biological_entity id="o20126" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent.</text>
      <biological_entity id="o20127" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves ± complanate, shriveled, often crisped when dry, ovate-oblong to lingulate, somewhat asymmetric, ventral leaves often more acuminate, dorsal and lateral leaves broadly oblong to oblong-ovate;</text>
      <biological_entity id="o20128" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="complanate" value_original="complanate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="shriveled" value_original="shriveled" />
        <character is_modifier="false" modifier="when dry" name="shape" src="d0_s3" value="crisped" value_original="crisped" />
        <character char_type="range_value" from="ovate-oblong" name="shape" src="d0_s3" to="lingulate" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s3" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
      <biological_entity constraint="ventral" id="o20129" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="dorsal and lateral" id="o20130" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s3" to="oblong-ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, entire in basal 1/4, crenulate, serrulate, or strongly toothed in apical 1/4;</text>
      <biological_entity id="o20131" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="in basal 1/4" constraintid="o20132" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character constraint="in apical 1/4" constraintid="o20133" is_modifier="false" modifier="strongly" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="basal" id="o20132" name="1/4" name_original="1/4" src="d0_s4" type="structure" />
      <biological_entity constraint="apical" id="o20133" name="1/4" name_original="1/4" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex rounded-truncate, short-apiculate;</text>
      <biological_entity id="o20134" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-truncate" value_original="rounded-truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="short-apiculate" value_original="short-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa ending in apex, subpercurrent, divergent from base, parallel or somewhat convergent distally, distal abaxial surface toothed;</text>
      <biological_entity id="o20135" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
        <character constraint="from base" constraintid="o20137" is_modifier="false" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="parallel" value_original="parallel" />
        <character is_modifier="false" modifier="somewhat; distally" name="arrangement" src="d0_s6" value="convergent" value_original="convergent" />
      </biological_entity>
      <biological_entity id="o20136" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o20137" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity constraint="distal abaxial" id="o20138" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
      <relation from="o20135" id="r2893" name="ending in" negation="false" src="d0_s6" to="o20136" />
    </statement>
    <statement id="d0_s7">
      <text>laminal cells isodiametric or slightly longer than wide, irregularly hexagonal to quadrate, 1-papillose on both surfaces;</text>
      <biological_entity constraint="laminal" id="o20139" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="slightly longer than wide" value_original="slightly longer than wide" />
        <character char_type="range_value" from="irregularly hexagonal" name="shape" src="d0_s7" to="quadrate" />
        <character constraint="on surfaces" constraintid="o20140" is_modifier="false" name="relief" src="d0_s7" value="1-papillose" value_original="1-papillose" />
      </biological_entity>
      <biological_entity id="o20140" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells forming obscure border or not forming border.</text>
      <biological_entity id="o20142" name="border" name_original="border" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s8" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o20143" name="border" name_original="border" src="d0_s8" type="structure" />
      <relation from="o20141" id="r2894" name="forming" negation="false" src="d0_s8" to="o20142" />
      <relation from="o20141" id="r2895" name="forming" negation="true" src="d0_s8" to="o20143" />
    </statement>
    <statement id="d0_s9">
      <text>Specialized asexual reproduction unknown.</text>
    </statement>
    <statement id="d0_s10">
      <text>Sexual condition synoicous and autoicous, infrequently dioicous;</text>
      <biological_entity constraint="marginal" id="o20141" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="development" src="d0_s9" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="synoicous" value_original="synoicous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="autoicous" value_original="autoicous" />
        <character is_modifier="false" modifier="infrequently" name="reproduction" src="d0_s10" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>perichaetial leaf apex gradually acuminate to acute.</text>
      <biological_entity constraint="leaf" id="o20144" name="apex" name_original="apex" src="d0_s11" type="structure" constraint_original="perichaetial leaf">
        <character char_type="range_value" from="gradually acuminate" name="shape" src="d0_s11" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seta red to reddish yellow, moderately to strongly papillose-scabrous.</text>
      <biological_entity id="o20145" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s12" to="reddish yellow" />
        <character is_modifier="false" modifier="moderately to strongly" name="pubescence_or_relief" src="d0_s12" value="papillose-scabrous" value_original="papillose-scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsule inclined, horizontal, or pendulous, ovoid or oblong-cylindric;</text>
      <biological_entity id="o20146" name="capsule" name_original="capsule" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="inclined" value_original="inclined" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="pendulous" value_original="pendulous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong-cylindric" value_original="oblong-cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>annulus absent;</text>
      <biological_entity id="o20147" name="annulus" name_original="annulus" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>operculum abruptly short-rostrate or subulate.</text>
      <biological_entity id="o20148" name="operculum" name_original="operculum" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s15" value="short-rostrate" value_original="short-rostrate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Calyptra mitrate-campanulate, base narrowed and shortly or deeply lobed, smooth or sometimes split on one side from base, somewhat scabrous or with few hairs distally.</text>
      <biological_entity id="o20149" name="calyptra" name_original="calyptra" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="mitrate-campanulate" value_original="mitrate-campanulate" />
      </biological_entity>
      <biological_entity id="o20150" name="base" name_original="base" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="deeply; deeply" name="shape" src="d0_s16" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o20151" name="split" name_original="split" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_relief" notes="" src="d0_s16" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s16" value="with few hairs" value_original="with few hairs" />
      </biological_entity>
      <biological_entity id="o20152" name="side" name_original="side" src="d0_s16" type="structure" />
      <biological_entity id="o20153" name="base" name_original="base" src="d0_s16" type="structure" />
      <biological_entity id="o20154" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="few" value_original="few" />
      </biological_entity>
      <relation from="o20151" id="r2896" name="on" negation="false" src="d0_s16" to="o20152" />
      <relation from="o20152" id="r2897" name="from" negation="false" src="d0_s16" to="o20153" />
      <relation from="o20151" id="r2898" name="with" negation="false" src="d0_s16" to="o20154" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States, Mexico, West Indies, Central America, South America, Eurasia, Africa, Pacific Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species ca. 95 (1 in the flora).</discussion>
  <discussion>Callicostella is characterized by broad and abruptly acuminate leaves with no border. Other distinctive features include the long, tough double costa and the small, isodiametric, firm-walled, 1-papillose laminal cells. The distal leaf margins are strongly serrate, with teeth often 2-fid. Callicostella may also be distinguished from closely related genera by its stem epidermis composed of small, thick-walled cells.</discussion>
  
</bio:treatment>