<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">brachytheciaceae</taxon_name>
    <taxon_name authority="Schimper in P. Bruch and W. P. Schimper" date="unknown" rank="genus">brachythecium</taxon_name>
    <taxon_name authority="I. Hagen" date="1908" rank="species">coruscum</taxon_name>
    <place_of_publication>
      <publication_title>Kongel. Norske Vidensk. Selsk. Skr. (Trondheim)</publication_title>
      <place_in_publication>1908(3): 3. 1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brachytheciaceae;genus brachythecium;species coruscum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099023</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachythecium</taxon_name>
    <taxon_name authority="(C. E. O. Jensen) Schljakov" date="unknown" rank="species">albicans</taxon_name>
    <taxon_name authority="C. E. O. Jensen" date="unknown" rank="variety">groenlandicum</taxon_name>
    <taxon_hierarchy>genus brachythecium;species albicans;variety groenlandicum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">B.</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">groenlandicum</taxon_name>
    <taxon_hierarchy>genus b.;species groenlandicum</taxon_hierarchy>
  </taxon_identification>
  <number>15.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, in loose to fairly dense mats, pale stramineous, pale-yellow, rich golden, or light-brown.</text>
      <biological_entity id="o22092" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="pale stramineous" value_original="pale stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="rich" value_original="rich" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden" value_original="golden" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden" value_original="golden" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="light-brown" value_original="light-brown" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22093" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" modifier="fairly" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o22092" id="r3159" name="in" negation="false" src="d0_s0" to="o22093" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 7 cm, creeping, ascending to upright, terete-foliate, irregularly pinnate, branches to 7 mm, straight to slightly flexuose, terete-foliate.</text>
      <biological_entity id="o22094" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="upright" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o22095" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s1" to="slightly flexuose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves appressed, straight or slightly falcate, closely to more rarely loosely imbricate, ovate, broadest at 1/7–1/5 leaf length, concave, plicate, 1.5–2.4 × 0.5–1 mm;</text>
      <biological_entity id="o22096" name="stem-leaf" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="closely" name="shape" src="d0_s2" value="concave" value_original="concave" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s2" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base slightly narrowed, narrowly short-decurrent;</text>
      <biological_entity id="o22097" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s3" value="short-decurrent" value_original="short-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane or often recurved almost throughout, entire to slightly serrulate, usually more serrulate in acumen;</text>
      <biological_entity id="o22098" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="often; almost throughout; throughout" name="orientation" src="d0_s4" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s4" to="slightly serrulate" />
        <character constraint="in acumen" constraintid="o22099" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o22099" name="acumen" name_original="acumen" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>apex gradually tapered, short to moderately acuminate;</text>
      <biological_entity id="o22100" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa to 40–70% leaf length, strong proximally, gradually tapering distally, often very weak near end, terminal spine absent;</text>
      <biological_entity id="o22101" name="costa" name_original="costa" src="d0_s6" type="structure" />
      <biological_entity id="o22102" name="leaf" name_original="leaf" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="40-70%" name="character" src="d0_s6" value="length" value_original="length" />
        <character is_modifier="false" modifier="proximally" name="fragility" src="d0_s6" value="strong" value_original="strong" />
        <character is_modifier="false" modifier="gradually; distally" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
        <character constraint="near end" constraintid="o22103" is_modifier="false" modifier="often very" name="fragility" src="d0_s6" value="weak" value_original="weak" />
      </biological_entity>
      <biological_entity id="o22103" name="end" name_original="end" src="d0_s6" type="structure" />
      <biological_entity constraint="terminal" id="o22104" name="spine" name_original="spine" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>decurrency cells short-rectangular;</text>
    </statement>
    <statement id="d0_s8">
      <text>alar cells subquadrate to short-rectangular, small, 12–20 × 10–13 µm, walls moderately thick, region extensive, of 6–15 × 6–10 cells;</text>
      <biological_entity constraint="decurrency" id="o22105" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o22106" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="subquadrate" name="shape" src="d0_s8" to="short-rectangular" />
        <character is_modifier="false" name="size" src="d0_s8" value="small" value_original="small" />
        <character char_type="range_value" from="12" from_unit="um" name="length" src="d0_s8" to="20" to_unit="um" />
        <character char_type="range_value" from="10" from_unit="um" name="width" src="d0_s8" to="13" to_unit="um" />
      </biological_entity>
      <biological_entity id="o22107" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately" name="width" src="d0_s8" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o22108" name="region" name_original="region" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="extensive" value_original="extensive" />
      </biological_entity>
      <biological_entity id="o22109" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="15" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="10" />
      </biological_entity>
      <relation from="o22108" id="r3160" name="consist_of" negation="false" src="d0_s8" to="o22109" />
    </statement>
    <statement id="d0_s9">
      <text>laminal cells elongate or rarely short-rhombic throughout, 30–80 × 6–8 µm; basal-cells to 8 (–10) µm wide, region in 2 or 3 rows.</text>
      <biological_entity constraint="laminal" id="o22110" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="rarely; throughout" name="shape" src="d0_s9" value="short-rhombic" value_original="short-rhombic" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s9" to="80" to_unit="um" />
        <character char_type="range_value" from="6" from_unit="um" name="width" src="d0_s9" to="8" to_unit="um" />
      </biological_entity>
      <biological_entity id="o22111" name="basal-cell" name_original="basal-cells" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="um" name="width" src="d0_s9" to="10" to_unit="um" />
        <character char_type="range_value" from="0" from_unit="um" name="width" src="d0_s9" to="8" to_unit="um" />
      </biological_entity>
      <biological_entity id="o22112" name="region" name_original="region" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Branch leaves smaller, narrower.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
      <biological_entity constraint="branch" id="o22113" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="width" src="d0_s10" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sporophytes unknown.</text>
      <biological_entity id="o22114" name="sporophyte" name_original="sporophytes" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Soil, rock, Arctic and alpine environments</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="arctic" />
        <character name="habitat" value="alpine environments" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations (0-3300 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="0" from_unit="m" constraint="low to high elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Yukon; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Brachythecium coruscum is quite common in Greenland, Alaska, and Arctic Canada. The species is similar to B. albicans, from which it differs principally in short leaf decurrencies that rarely come off with detached leaves and shorter laminal cells, in many leaves only few cells being longer than 50 µm. Plants of B. coruscum are usually more enriched with yellow than B. albicans, which is much paler and whitish to stramineous. The leaf acumina are erectopatent to patent, often falcate-secund, and the alar regions are square to elongate.</discussion>
  
</bio:treatment>