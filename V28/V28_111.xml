<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="D. Mohr" date="unknown" rank="genus">ulota</taxon_name>
    <taxon_name authority="Mitten" date="1864" rank="species">barclayi</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 26. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus ulota;species barclayi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250062005</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 1 (–2.5) cm.</text>
      <biological_entity id="o19675" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o19676" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stem-leaves slightly crisped and curved when dry, narrowly lanceolate to oblong-lanceolate, 1.5–2.5 mm;</text>
      <biological_entity id="o19677" name="leaf-stem" name_original="stem-leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
        <character is_modifier="false" modifier="when dry" name="course" src="d0_s2" value="curved" value_original="curved" />
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s2" to="oblong-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="distance" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base oblong;</text>
      <biological_entity id="o19678" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins broadly reflexed or plane;</text>
      <biological_entity id="o19679" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="orientation" src="d0_s4" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex acute;</text>
      <biological_entity id="o19680" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal laminal cells hexagonal-rounded, not pigmented, walls thin;</text>
      <biological_entity constraint="basal laminal" id="o19681" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hexagonal-rounded" value_original="hexagonal-rounded" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s6" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o19682" name="wall" name_original="walls" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>distal cells 8–12 µm, smooth or papillae essentially absent.</text>
      <biological_entity constraint="distal" id="o19683" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="um" name="some_measurement" src="d0_s7" to="12" to_unit="um" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition autoicous;</text>
      <biological_entity id="o19684" name="papilla" name_original="papillae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="essentially" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="development" src="d0_s8" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perichaetial leaves differentiated from stem-leaves or not.</text>
      <biological_entity constraint="perichaetial" id="o19685" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character constraint="from stem-leaves" constraintid="o19686" is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o19686" name="stem-leaf" name_original="stem-leaves" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Seta 1.8–3.2 mm.</text>
      <biological_entity id="o19687" name="seta" name_original="seta" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsule oblong-ovate when mature, oblong-cylindric when old and dry, 1–2 mm, strongly 8-ribbed 3/4 length, mouth wide but constricted below mouth or evenly tapering to seta from mouth;</text>
      <biological_entity id="o19688" name="capsule" name_original="capsule" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="when mature" name="shape" src="d0_s12" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" modifier="when old and dry" name="shape" src="d0_s12" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="architecture_or_shape" src="d0_s12" value="8-ribbed" value_original="8-ribbed" />
        <character name="length" src="d0_s12" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity id="o19689" name="mouth" name_original="mouth" src="d0_s12" type="structure">
        <character is_modifier="false" name="width" src="d0_s12" value="wide" value_original="wide" />
        <character constraint="below " constraintid="o19691" is_modifier="false" name="size" src="d0_s12" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o19690" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
      <biological_entity id="o19691" name="seta" name_original="seta" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="evenly" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o19692" name="mouth" name_original="mouth" src="d0_s12" type="structure" />
      <relation from="o19691" id="r2814" name="from" negation="false" src="d0_s12" to="o19692" />
    </statement>
    <statement id="d0_s13">
      <text>stomata in neck and proximal capsule;</text>
      <biological_entity id="o19693" name="stoma" name_original="stomata" src="d0_s13" type="structure" />
      <biological_entity id="o19694" name="neck" name_original="neck" src="d0_s13" type="structure" />
      <biological_entity constraint="proximal" id="o19695" name="capsule" name_original="capsule" src="d0_s13" type="structure" />
      <relation from="o19693" id="r2815" name="in" negation="false" src="d0_s13" to="o19694" />
      <relation from="o19693" id="r2816" name="in" negation="false" src="d0_s13" to="o19695" />
    </statement>
    <statement id="d0_s14">
      <text>peristome double;</text>
      <biological_entity id="o19696" name="peristome" name_original="peristome" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>exostome teeth ± split to 16, reflexed, evenly papillose;</text>
      <biological_entity constraint="exostome" id="o19697" name="tooth" name_original="teeth" src="d0_s15" type="structure" />
      <biological_entity id="o19698" name="split" name_original="split" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="evenly" name="relief" src="d0_s15" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>endostome segments 8, slightly papillose.</text>
      <biological_entity constraint="endostome" id="o19699" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="8" value_original="8" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Calyptra short-conic, sparsely hairy or hairs few, near apex.</text>
      <biological_entity id="o19700" name="calyptra" name_original="calyptra" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="short-conic" value_original="short-conic" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o19701" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s17" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o19702" name="apex" name_original="apex" src="d0_s17" type="structure" />
      <relation from="o19701" id="r2817" name="near" negation="false" src="d0_s17" to="o19702" />
    </statement>
    <statement id="d0_s18">
      <text>Spores 16–21 µm.</text>
      <biological_entity id="o19703" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character char_type="range_value" from="16" from_unit="um" name="some_measurement" src="d0_s18" to="21" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks and branches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="branches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ulota barclayi, described from Alaska, may be a species distinct from the eastern Asian U. japonica. From other West Coast species of the genus, these populations differ by leaves only slightly twisted and curved, strongly differentiated perichaetial leaves, cushion-forming erect plants, reflexed exostome, and small spores.</discussion>
  
</bio:treatment>