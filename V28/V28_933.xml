<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">595</other_info_on_meta>
    <other_info_on_meta type="mention_page">594</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">leucodontaceae</taxon_name>
    <taxon_name authority="Schwagrichen" date="unknown" rank="genus">leucodon</taxon_name>
    <taxon_name authority="(H. A. Crum &amp; L. E. Anderson) W. D. Reese &amp; L. E. Anderson" date="1997" rank="species">andrewsianus</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>100: 92. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family leucodontaceae;genus leucodon;species andrewsianus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099209</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leucodon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">brachypus</taxon_name>
    <taxon_name authority="H. A. Crum &amp; L. E. Anderson" date="unknown" rank="variety">andrewsianus</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>75: 101. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus leucodon;species brachypus;variety andrewsianus</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized or rarely large, dull or glossy.</text>
      <biological_entity id="o1935" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="medium-sized" value_original="medium-sized" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s0" value="large" value_original="large" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems elongate, curved or rarely straight, not julaceous, conspicuous clusters of short, fragile branchlets present in distal leaf-axils and at branch apices, sometimes scarce or apparently absent.</text>
      <biological_entity id="o1936" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
        <character is_modifier="false" name="prominence" src="d0_s1" value="conspicuous" value_original="conspicuous" />
        <character constraint="of branchlets" constraintid="o1937" is_modifier="false" name="arrangement" src="d0_s1" value="cluster" value_original="cluster" />
        <character is_modifier="false" modifier="apparently" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1937" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="fragility" src="d0_s1" value="fragile" value_original="fragile" />
        <character constraint="in distal leaf-axils and at branch apices" constraintid="o1938" is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1938" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes scarce" name="quantity" notes="" src="d0_s1" value="or" value_original="or" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Branch leaves erect-appressed or rarely secund, plicate, 1.4–1.9 mm;</text>
      <biological_entity constraint="branch" id="o1939" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s2" value="erect-appressed" value_original="erect-appressed" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s2" value="secund" value_original="secund" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s2" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s2" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>apex lanceolate-acuminate;</text>
      <biological_entity id="o1940" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate-acuminate" value_original="lanceolate-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>apical laminal cells smooth abaxially or rarely papillose;</text>
      <biological_entity constraint="apical laminal" id="o1941" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abaxially" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="rarely; rarely" name="relief" src="d0_s4" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>terminal cell narrowly elongate, hyaline.</text>
      <biological_entity constraint="terminal" id="o1942" name="cell" name_original="cell" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Perigonia unknown.</text>
      <biological_entity id="o1943" name="perigonium" name_original="perigonia" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Perichaetia extremely rare.</text>
      <biological_entity id="o1944" name="perichaetium" name_original="perichaetia" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="extremely" name="quantity" src="d0_s7" value="rare" value_original="rare" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sporophytes unknown.</text>
      <biological_entity id="o1945" name="sporophyte" name_original="sporophytes" src="d0_s8" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tree trunks, rock, forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree trunks" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1500 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Maine, Mass., N.H., N.J., N.Y., N.C., Pa., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leucodon andrewsianus differs from the similar, slightly more robust L. brachypus most evidently in the common presence of tufts of tiny branchlets congested in axils of distal leaves and at branch apices; in some specimens of L. andrewsianus, the branch apices and entire older plants transition into branchlets. The branchlets are mostly very short but sometimes much elongated. In the absence of branchlets, the slender, elongate (35–60 µm) terminal cells of young leaves are helpful for identification of L. andrewsianus. Sporophytes of L. andrewsianus are unknown, and gametangia are extremely rare. Reproduction and dispersion are doubtless accomplished by the caducous branchlets. This taxon has been treated in the flora area as L. sciuroides (Hedwig) Schwägrichen, a moss that occurs in Africa, Asia, and Europe, but apparently not in North America.</discussion>
  
</bio:treatment>