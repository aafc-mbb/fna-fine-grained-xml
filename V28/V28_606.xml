<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Vanderpoorten" date="unknown" rank="family">calliergonaceae</taxon_name>
    <taxon_name authority="Hedenas" date="unknown" rank="genus">hamatocaulis</taxon_name>
    <taxon_name authority="(Mitten) Hedenas" date="1989" rank="species">vernicosus</taxon_name>
    <place_of_publication>
      <publication_title>Lindbergia</publication_title>
      <place_in_publication>15: 27. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family calliergonaceae;genus hamatocaulis;species vernicosus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099133</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stereodon</taxon_name>
    <taxon_name authority="Mitten" date="unknown" rank="species">vernicosus</taxon_name>
    <place_of_publication>
      <publication_title>J. Linn. Soc., Bot.</publication_title>
      <place_in_publication>8: 43. 1864,</place_in_publication>
      <other_info_on_pub>based on Hypnum vernicosum Lindberg in C. J. Hartman, Handb. Skand. Fl. ed. 8, 342. 1861, not Hampe 1836</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus stereodon;species vernicosus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Drepanocladus</taxon_name>
    <taxon_name authority="(Mitten) Warnstorf" date="unknown" rank="species">vernicosus</taxon_name>
    <taxon_hierarchy>genus drepanocladus;species vernicosus</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants medium-sized to large, not turgid, green, brownish, variegated green and red, or rarely almost entirely red.</text>
      <biological_entity id="o8391" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="medium-sized" name="size" src="d0_s0" to="large" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s0" value="turgid" value_original="turgid" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="variegated green and red" value_original="variegated green and red" />
        <character is_modifier="false" modifier="rarely almost entirely" name="coloration" src="d0_s0" value="red" value_original="red" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± pinnate;</text>
      <biological_entity id="o8392" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>shoot apices often hooked.</text>
      <biological_entity constraint="shoot" id="o8393" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="hooked" value_original="hooked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stem-leaves with red transverse subbasal zone or sometimes larger parts of leaf red, ovate, concave, strongly or sometimes slightly plicate, 0.6–1.1 mm wide;</text>
      <biological_entity id="o8394" name="stem-leaf" name_original="stem-leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="strongly; sometimes slightly" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s3" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s3" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="subbasal" id="o8395" name="zone" name_original="zone" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="red" value_original="red" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s3" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o8396" name="part" name_original="parts" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="sometimes" name="size" src="d0_s3" value="larger" value_original="larger" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o8397" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <relation from="o8394" id="r1174" name="with" negation="false" src="d0_s3" to="o8395" />
      <relation from="o8394" id="r1175" name="with" negation="false" src="d0_s3" to="o8396" />
      <relation from="o8396" id="r1176" name="part_of" negation="false" src="d0_s3" to="o8397" />
    </statement>
    <statement id="d0_s4">
      <text>base ± erect, slightly constricted at insertion;</text>
      <biological_entity id="o8398" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="insertion" src="d0_s4" value="constricted" value_original="constricted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>apex short or long-acuminate.</text>
      <biological_entity id="o8399" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s5" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mineral-rich, often slightly nutrient-enriched, spring-influenced habitats, lakeshores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mineral-rich" />
        <character name="habitat" value="nutrient-enriched" modifier="often slightly" />
        <character name="habitat" value="spring-influenced habitats" />
        <character name="habitat" value="lakeshores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to moderate elevations (0-1400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="low" />
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" constraint="low to moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., P.E.I., Que., Yukon; Alaska, Conn., Ill., Ind., Maine, Mich., Minn., N.J., N.Y., Ohio, Oreg., Vt., Wash.; West Indies (Dominican Republic); n South America (Colombia, Venezuela); Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="West Indies (Dominican Republic)" establishment_means="native" />
        <character name="distribution" value="n South America (Colombia)" establishment_means="native" />
        <character name="distribution" value="n South America (Venezuela)" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hamatocaulis vernicosus is widely distributed but rarely common, seemingly rare or absent in the Arctic, in the northern-central parts of the continent, and in most oceanic areas. The species is usually easily recognized by the distinctly plicate stem leaves having erect bases and suddenly curved distal parts. The stem leaves lack differentiated alar cells and the stem lacks a central strand and hyalodermis. Molecular studies have revealed that H. vernicosus consists of two cryptic species in Europe. Molecularly studied North American material (from Minnesota) belongs to one of these, but since the other species is known from South America, it could occur in North America as well. Much material originally filed under H. vernicosus in herbaria examined belonged to other species, mainly to Palustriella falcata, Sanionia uncinata, and Scorpidium cossonii; almost all earlier species of Drepanocladus (in the broad sense) have been confused with H. vernicosus.</discussion>
  <references>
    <reference>Hedenäs, L. and P. K. Eldenäs. 2007. Cryptic speciation, habitat differentiation, and geography in Hamatocaulis vernicosus (Calliergonaceae, Bryophyta). Pl. Syst. Evol. 268: 131–145.</reference>
  </references>
  
</bio:treatment>