<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
    <other_info_on_meta type="illustration_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">orthotrichaceae</taxon_name>
    <taxon_name authority="Bridel" date="unknown" rank="genus">macromitrium</taxon_name>
    <taxon_name authority="Schwagrichen" date="unknown" rank="species">richardii</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>2(2,1): 70, plate 173 [bottom]. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orthotrichaceae;genus macromitrium;species richardii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250061838</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems with branches to 1 cm, simple or 2-fid.</text>
      <biological_entity id="o12894" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s0" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s0" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o12895" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o12894" id="r1817" name="with" negation="false" src="d0_s0" to="o12895" />
    </statement>
    <statement id="d0_s1">
      <text>Branch leaves 0.7–1.8 mm;</text>
      <biological_entity constraint="branch" id="o12896" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s1" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal laminal cells tuberculate or smooth, walls thick;</text>
      <biological_entity constraint="basal laminal" id="o12897" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="relief" src="d0_s2" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o12898" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal cells bulging mid leaf, grading to papillose-bulging at tip.</text>
      <biological_entity constraint="distal" id="o12899" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character constraint="at tip" constraintid="o12901" is_modifier="false" name="pubescence_or_shape" notes="" src="d0_s3" value="papillose-bulging" value_original="papillose-bulging" />
      </biological_entity>
      <biological_entity constraint="mid" id="o12900" name="leaf" name_original="leaf" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence_or_shape" src="d0_s3" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o12901" name="tip" name_original="tip" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Seta dextrorse.</text>
      <biological_entity id="o12902" name="seta" name_original="seta" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="dextrorse" value_original="dextrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Capsule with exothecial cells not differentiated;</text>
      <biological_entity id="o12903" name="capsule" name_original="capsule" src="d0_s5" type="structure" />
      <biological_entity id="o12904" name="exothecial" name_original="exothecial" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o12905" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <relation from="o12903" id="r1818" name="with" negation="false" src="d0_s5" to="o12904" />
      <relation from="o12903" id="r1819" name="with" negation="false" src="d0_s5" to="o12905" />
    </statement>
    <statement id="d0_s6">
      <text>stomata at capsule base;</text>
      <biological_entity id="o12906" name="stoma" name_original="stomata" src="d0_s6" type="structure" />
      <biological_entity constraint="capsule" id="o12907" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o12906" id="r1820" name="at" negation="false" src="d0_s6" to="o12907" />
    </statement>
    <statement id="d0_s7">
      <text>exostome teeth rudimentary, delicate, pale-yellow.</text>
      <biological_entity constraint="exostome" id="o12908" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spores 22–32 µm, densely papillose.</text>
      <biological_entity id="o12909" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="22" from_unit="um" name="some_measurement" src="d0_s8" to="32" to_unit="um" />
        <character is_modifier="false" modifier="densely" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Branches and trunks of trees</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="branches" constraint="of trees" />
        <character name="habitat" value="trunks" constraint="of trees" />
        <character name="habitat" value="trees" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., La., Miss.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Macromitrion richardi</other_name>
  <discussion>Macromitrium richardii is distinguished from other mosses with similar creeping stems and erect branches by its non-rugose, inrolled leaves and uniformly elongate basal laminal cells. The papillose-bulging distal laminal cells and autoicous sexual condition distinguish M. richardii from other species of Macromitrium in the tropical portions of its range.</discussion>
  
</bio:treatment>