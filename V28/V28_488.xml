<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
    <other_info_on_meta type="illustration_page">312</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Loeske" date="unknown" rank="genus">calliergonella</taxon_name>
    <taxon_name authority="(Hedwig) Loeske" date="1911" rank="species">cuspidata</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>50: 248. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus calliergonella;species cuspidata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200002153</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">cuspidatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>254. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species cuspidatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliergon</taxon_name>
    <taxon_name authority="(Hedwig) Kindberg" date="unknown" rank="species">cuspidatum</taxon_name>
    <taxon_hierarchy>genus calliergon;species cuspidatum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 7 cm, in loose tufts or mats.</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1" name="tuft" name_original="tufts" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o2" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <relation from="o0" id="r0" name="in" negation="false" src="d0_s0" to="o1" />
      <relation from="o0" id="r1" name="in" negation="false" src="d0_s0" to="o2" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, semierect, or decumbent, terete-foliate, pinnate when erect, irregularly branched when decumbent, flattened in cross-section, usually turgid at stem and branch apices;</text>
      <biological_entity id="o3" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="semierect" value_original="semierect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="semierect" value_original="semierect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="terete-foliate" value_original="terete-foliate" />
        <character is_modifier="false" modifier="when erect" name="architecture_or_shape" src="d0_s1" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" modifier="when decumbent" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="in cross-section" constraintid="o4" is_modifier="false" name="shape" src="d0_s1" value="flattened" value_original="flattened" />
        <character constraint="at branch apices" constraintid="o6" is_modifier="false" modifier="usually" name="shape" notes="" src="d0_s1" value="turgid" value_original="turgid" />
      </biological_entity>
      <biological_entity id="o4" name="cross-section" name_original="cross-section" src="d0_s1" type="structure" />
      <biological_entity id="o5" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <biological_entity constraint="branch" id="o6" name="apex" name_original="apices" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>outer cortical cells in 3 or 4 layers, inner cortical cells large, walls thin;</text>
      <biological_entity constraint="outer cortical" id="o7" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity constraint="inner" id="o8" name="cortical" name_original="cortical" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="3" value_original="3" />
        <character is_modifier="false" name="size" src="d0_s2" value="large" value_original="large" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="3" value_original="3" />
        <character is_modifier="false" name="size" src="d0_s2" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o10" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <relation from="o7" id="r2" name="in" negation="false" src="d0_s2" to="o8" />
      <relation from="o7" id="r3" name="in" negation="false" src="d0_s2" to="o9" />
    </statement>
    <statement id="d0_s3">
      <text>pseudoparaphyllia foliose;</text>
      <biological_entity id="o11" name="pseudoparaphyllium" name_original="pseudoparaphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliose" value_original="foliose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary hair basal-cell 1, short, brown.</text>
      <biological_entity constraint="hair" id="o12" name="basal-cell" name_original="basal-cell" src="d0_s4" type="structure" constraint_original="axillary hair">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stem-leaves ovate, short, broad;</text>
      <biological_entity id="o13" name="stem-leaf" name_original="stem-leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s5" value="broad" value_original="broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute and weakly cuspidate to rounded;</text>
      <biological_entity id="o14" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character char_type="range_value" from="weakly cuspidate" name="shape" src="d0_s6" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa double, short, usually inconspicuous, or ecostate;</text>
    </statement>
    <statement id="d0_s8">
      <text>supra-alar cells often ± quadrate, walls thicker than those of alar cells;</text>
      <biological_entity id="o15" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s7" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity id="o16" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often more or less" name="shape" src="d0_s8" value="quadrate" value_original="quadrate" />
      </biological_entity>
      <biological_entity id="o17" name="wall" name_original="walls" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>inner basal laminal cells short-elongate, walls thick, pitted;</text>
      <biological_entity constraint="inner basal laminal" id="o18" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="short-elongate" value_original="short-elongate" />
      </biological_entity>
      <biological_entity id="o19" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thick" value_original="thick" />
        <character is_modifier="false" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apical cells short, weakly sinuate.</text>
      <biological_entity constraint="apical" id="o20" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="short" value_original="short" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s10" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Branch leaves lanceolate, longer;</text>
      <biological_entity constraint="branch" id="o21" name="leaf" name_original="leaves" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>apex usually cuspidate;</text>
      <biological_entity id="o22" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s12" value="cuspidate" value_original="cuspidate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ecostate or costa double, short;</text>
    </statement>
    <statement id="d0_s14">
      <text>alar region ± auriculate.</text>
      <biological_entity id="o23" name="costa" name_original="costa" src="d0_s13" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o24" name="region" name_original="region" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Perigonia axillary, budlike, apex flaring.</text>
      <biological_entity id="o25" name="perigonium" name_original="perigonia" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="budlike" value_original="budlike" />
      </biological_entity>
      <biological_entity id="o26" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="flaring" value_original="flaring" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Perichaetia axillary, inner leaves long-lanceolate, plicate, margins weakly serrate basally, apex flaring at archegonial maturity, ecostate.</text>
      <biological_entity id="o27" name="perichaetium" name_original="perichaetia" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="inner" id="o28" name="leaf" name_original="leaves" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="long-lanceolate" value_original="long-lanceolate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s16" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o29" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="weakly; basally" name="architecture_or_shape" src="d0_s16" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o30" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character constraint="at archegonial" constraintid="o31" is_modifier="false" name="shape" src="d0_s16" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity id="o31" name="archegonial" name_original="archegonial" src="d0_s16" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s16" value="maturity" value_original="maturity" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seta long, ± erect.</text>
      <biological_entity id="o32" name="seta" name_original="seta" src="d0_s17" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s17" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsule with exothecial cells collenchymatous;</text>
      <biological_entity id="o33" name="capsule" name_original="capsule" src="d0_s18" type="structure" />
      <biological_entity id="o34" name="exothecial" name_original="exothecial" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity id="o35" name="cell" name_original="cells" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <relation from="o33" id="r4" name="with" negation="false" src="d0_s18" to="o34" />
      <relation from="o33" id="r5" name="with" negation="false" src="d0_s18" to="o35" />
    </statement>
    <statement id="d0_s19">
      <text>annulus differentiated, 2–4-seriate;</text>
      <biological_entity id="o36" name="annulus" name_original="annulus" src="d0_s19" type="structure">
        <character is_modifier="false" name="variability" src="d0_s19" value="differentiated" value_original="differentiated" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s19" value="2-4-seriate" value_original="2-4-seriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>operculum conic-apiculate;</text>
      <biological_entity id="o37" name="operculum" name_original="operculum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="conic-apiculate" value_original="conic-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>exostome teeth with external surface striolate basally, papillose distally;</text>
      <biological_entity constraint="exostome" id="o38" name="tooth" name_original="teeth" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="distally" name="relief" notes="" src="d0_s21" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="external" id="o39" name="surface" name_original="surface" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="basally" name="relief" src="d0_s21" value="striolate" value_original="striolate" />
      </biological_entity>
      <relation from="o38" id="r6" name="with" negation="false" src="d0_s21" to="o39" />
    </statement>
    <statement id="d0_s22">
      <text>endostome basal membrane high, segments keeled, perforate.</text>
      <biological_entity id="o40" name="endostome" name_original="endostome" src="d0_s22" type="structure" />
      <biological_entity constraint="basal" id="o41" name="membrane" name_original="membrane" src="d0_s22" type="structure">
        <character is_modifier="false" name="height" src="d0_s22" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o42" name="segment" name_original="segments" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Spores finely punctate.</text>
      <biological_entity id="o43" name="spore" name_original="spores" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="finely" name="coloration_or_relief" src="d0_s23" value="punctate" value_original="punctate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Calcareous wetlands, moderately rich fens, among sedges, lake margins, roadside ditches, weed in lawns</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous wetlands" />
        <character name="habitat" value="rich fens" modifier="moderately" />
        <character name="habitat" value="sedges" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="weed" constraint="in lawns" />
        <character name="habitat" value="lawns" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low to high elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="high" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Alaska, Calif., Conn., Idaho, Iowa, Maine, Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., Ohio, Oreg., Pa., Tenn., Vt., Wash., Wis., Wyo.; Mexico; South America; Europe; Asia; n Africa; Atlantic Islands (Iceland, Macaronesia); Pacific Islands (New Zealand); Australia (including Tasmania); introduced in West Indies (Jamaica).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Australia (including Tasmania)" establishment_means="native" />
        <character name="distribution" value="in West Indies (Jamaica)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <discussion>As emphasized by L. Hedenäs (1990), Calliergonella cuspidata shares with Hypnum lindbergii features such as similar appendiculate endostome cilia, not or slightly denticulate stem and perichaetial leaves, hyaline alar cells in a well-defined group, broad and obtuse pseudoparaphyllia, and a plicate capsule wall when dry; both species group together in molecular phylogenies (M. S. Ignatov et al. 2006; H. Tsubota et al. 2002; A. Vanderpoorten et al. 2002). Plants of C. cuspidata are highly variable, like many wetland mosses. The typical semierect, pinnate habit is developed where the water table is high, while decumbent, irregularly branched habits are found in wetlands where the water table fluctuates. Oddly, plants of mesic lawns are often semierect and pinnate.</discussion>
  <discussion>Calliergonella cuspidata occurs widely in the Northern and Southern hemispheres; in the United States, particularly in the Pacific Northwest and California and in the Northeast, the species is established commonly on managed residential lawns and in those of cemeteries. In these regions its native habitat and range are not entirely clear. The species is naturalized in Jamaica in plantations. Sporophytes are rare, even when male and female plants grow together, as in northern Michigan.</discussion>
  
</bio:treatment>