<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">bryaceae</taxon_name>
    <taxon_name authority="J. R. Spence" date="unknown" rank="genus">plagiobryoides</taxon_name>
    <taxon_name authority="(Hooker) J. R. Spence" date="2009" rank="species">cellularis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>91: 499. 2009</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family bryaceae;genus plagiobryoides;species cellularis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250099239</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bryum</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">cellulare</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. Schwägrichen, Sp. Musc. Frond. Suppl.</publication_title>
      <place_in_publication>3(1,1): plate 214, fig. a. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus bryum;species cellulare</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants pale pink-green.</text>
      <biological_entity id="o23803" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="pale pink-green" value_original="pale pink-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 0.5–1 cm, weakly julaceous, innovations many;</text>
      <biological_entity id="o23804" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="weakly" name="architecture_or_shape" src="d0_s1" value="julaceous" value_original="julaceous" />
      </biological_entity>
      <biological_entity id="o23805" name="innovation" name_original="innovations" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" value_original="many" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizoids few on proximal stem.</text>
      <biological_entity id="o23806" name="rhizoid" name_original="rhizoids" src="d0_s2" type="structure">
        <character constraint="on proximal stem" constraintid="o23807" is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23807" name="stem" name_original="stem" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves crowded, imbricate when dry, erect when moist, narrowly to broadly ovate, flat or weakly concave, 0.4–1 (–2.5) mm;</text>
      <biological_entity id="o23808" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="when dry" name="arrangement" src="d0_s3" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly to broadly; broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>base not decurrent, pink;</text>
      <biological_entity id="o23809" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="pink" value_original="pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>margins plane or recurved proximally, 1-stratose, limbidium absent or rarely 1-stratose proximally;</text>
      <biological_entity id="o23810" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="proximally" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
      <biological_entity id="o23811" name="limbidium" name_original="limbidium" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely; proximally" name="architecture" src="d0_s5" value="1-stratose" value_original="1-stratose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>apex acute;</text>
      <biological_entity id="o23812" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>costa reaching apex to very short-excurrent;</text>
      <biological_entity id="o23813" name="costa" name_original="costa" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="very" name="architecture" src="d0_s7" value="short-excurrent" value_original="short-excurrent" />
      </biological_entity>
      <biological_entity id="o23814" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <relation from="o23813" id="r3383" name="reaching" negation="false" src="d0_s7" to="o23814" />
    </statement>
    <statement id="d0_s8">
      <text>proximal laminal cells long-rectangular, (60–) 80–100 × 18–24 µm, 4–5: 1, sometimes bulging, walls thin;</text>
      <biological_entity constraint="proximal laminal" id="o23815" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="long-rectangular" value_original="long-rectangular" />
        <character char_type="range_value" from="60" from_unit="um" name="atypical_length" src="d0_s8" to="80" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="80" from_unit="um" name="length" src="d0_s8" to="100" to_unit="um" />
        <character char_type="range_value" from="18" from_unit="um" name="width" src="d0_s8" to="24" to_unit="um" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="5" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_shape" src="d0_s8" value="bulging" value_original="bulging" />
      </biological_entity>
      <biological_entity id="o23816" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>distal cells elongate hexagonal, 30–70 × 16–22 µm, 2–4: 1, walls thin.</text>
      <biological_entity constraint="distal" id="o23817" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hexagonal" value_original="hexagonal" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s9" to="70" to_unit="um" />
        <character char_type="range_value" from="16" from_unit="um" name="width" src="d0_s9" to="22" to_unit="um" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="4" />
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Specialized asexual reproduction absent.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous.</text>
    </statement>
    <statement id="d0_s12">
      <text>[Seta thick, (1–) 2–3 cm, flexuose to twisted. Capsule inclined to nutant, 2–4 mm; hypophysis strongly differentiated, elongate; operculum apiculate; peristome well developed; exostome pale-yellow or tan proximally, teeth lanceolate, long; endostome hyaline to pale-yellow, not adherent to exostome, segments present, sometimes longer than exostome, cilia usually absent, occasionally 1 or 2. Spores 20–28 µm, papillose, yellowbrown].</text>
      <biological_entity id="o23818" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="development" src="d0_s10" value="specialized" value_original="specialized" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="asexual" value_original="asexual" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp to seepy rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="damp to seepy rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations (10 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="10" from_unit="m" constraint="low elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., N.C.; Mexico; West Indies; Central America; South America; se Asia (including Indonesia); Africa; Pacific Islands (New Guinea); Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="se Asia (including Indonesia)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Guinea)" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Plagiobryoides cellularis is a widespread pantropical species. The North American material is very poor, consisting of a few small sterile shoots from two sites: on rocks near the sea on the Florida Keys and wet rock in North Carolina.</discussion>
  
</bio:treatment>