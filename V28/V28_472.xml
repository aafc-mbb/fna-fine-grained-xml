<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">303</other_info_on_meta>
    <other_info_on_meta type="mention_page">302</other_info_on_meta>
    <other_info_on_meta type="mention_page">304</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Loeske" date="unknown" rank="genus">hygroamblystegium</taxon_name>
    <taxon_name authority="(Hedwig) Mönkemeyer" date="unknown" rank="species">varium</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>50: 275. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus hygroamblystegium;species varium</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099158</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leskea</taxon_name>
    <taxon_name authority="Hedwig" date="unknown" rank="species">varia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Musc. Frond.,</publication_title>
      <place_in_publication>216, plate 53, figs. 15–20. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus leskea;species varia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amblystegium</taxon_name>
    <taxon_name authority="(Hedwig) Lindberg" date="unknown" rank="species">varium</taxon_name>
    <taxon_hierarchy>genus amblystegium;species varium</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants in ± stiff mats, yellowish to dark green, dull.</text>
      <biological_entity id="o2604" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" notes="" src="d0_s0" to="dark green" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2605" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="more or less" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o2604" id="r337" name="in" negation="false" src="d0_s0" to="o2605" />
    </statement>
    <statement id="d0_s1">
      <text>Stems to 15 cm, creeping, stiff, rigid;</text>
      <biological_entity id="o2606" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cortical cells in 2–4 layers, walls incrassate;</text>
      <biological_entity constraint="cortical" id="o2607" name="cell" name_original="cells" src="d0_s2" type="structure" />
      <biological_entity id="o2608" name="layer" name_original="layers" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o2609" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <relation from="o2607" id="r338" name="in" negation="false" src="d0_s2" to="o2608" />
    </statement>
    <statement id="d0_s3">
      <text>axillary hair basal-cell 1, brown, distal cells elongate.</text>
      <biological_entity constraint="hair" id="o2610" name="basal-cell" name_original="basal-cell" src="d0_s3" type="structure" constraint_original="axillary hair">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2611" name="cell" name_original="cells" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stem-leaves ovatelanceolate to ovate-triangular;</text>
      <biological_entity id="o2612" name="stem-leaf" name_original="stem-leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s4" to="ovate-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal laminal cells short-rectangular, often pigmented, walls firm, eporose, region weakly differentiated;</text>
      <biological_entity constraint="basal laminal" id="o2613" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="short-rectangular" value_original="short-rectangular" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s5" value="pigmented" value_original="pigmented" />
      </biological_entity>
      <biological_entity id="o2614" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="firm" value_original="firm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="eporose" value_original="eporose" />
      </biological_entity>
      <biological_entity id="o2615" name="region" name_original="region" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s5" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>medial cells 2–7: 1.</text>
      <biological_entity constraint="medial" id="o2616" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="7" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Seta dark red, 1.2–2.2 cm.</text>
      <biological_entity id="o2617" name="seta" name_original="seta" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark red" value_original="dark red" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s7" to="2.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsule constricted below mouth when dry;</text>
      <biological_entity id="o2618" name="capsule" name_original="capsule" src="d0_s8" type="structure">
        <character constraint="below mouth" constraintid="o2619" is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity id="o2619" name="mouth" name_original="mouth" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>exothecial cells long to short-rectangular, 30–105 × 18–35 µm, walls firm, not collenchymatous;</text>
      <biological_entity constraint="exothecial" id="o2620" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="false" name="shape" src="d0_s9" value="short-rectangular" value_original="short-rectangular" />
        <character char_type="range_value" from="30" from_unit="um" name="length" src="d0_s9" to="105" to_unit="um" />
        <character char_type="range_value" from="18" from_unit="um" name="width" src="d0_s9" to="35" to_unit="um" />
      </biological_entity>
      <biological_entity id="o2621" name="wall" name_original="walls" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>annulus separating, 2–4-seriate, cell-walls thin;</text>
      <biological_entity id="o2622" name="annulus" name_original="annulus" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="separating" value_original="separating" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="2-4-seriate" value_original="2-4-seriate" />
      </biological_entity>
      <biological_entity id="o2623" name="cell-wall" name_original="cell-walls" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>operculum acute, apiculate, or rostrate;</text>
      <biological_entity id="o2624" name="operculum" name_original="operculum" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rostrate" value_original="rostrate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rostrate" value_original="rostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>exostome teeth yellowbrown, external surface cross-striolate basally, coarsely papillose distally, internal surface trabeculate;</text>
      <biological_entity constraint="exostome" id="o2625" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
      <biological_entity constraint="external" id="o2626" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s12" value="cross-striolate" value_original="cross-striolate" />
        <character is_modifier="false" modifier="coarsely; distally" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity constraint="internal" id="o2627" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="trabeculate" value_original="trabeculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>endostome basal membrane high, segments not or narrowly perforate.</text>
      <biological_entity id="o2628" name="endostome" name_original="endostome" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o2629" name="membrane" name_original="membrane" src="d0_s13" type="structure">
        <character is_modifier="false" name="height" src="d0_s13" value="high" value_original="high" />
      </biological_entity>
      <biological_entity id="o2630" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s13" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spores finely papillose.</text>
      <biological_entity id="o2631" name="spore" name_original="spores" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s14" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America, Eurasia, s Africa, Pacific Islands (New Zealand), Antarctica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="s Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="native" />
        <character name="distribution" value="Antarctica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="past_name">Hygramblystegium</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants small to moderately large; stems to 10 cm; costae narrower than 100 µm at base, to mid leaf, percurrent, or rarely short-excurrent; lamina 1-stratose.</description>
      <determination>1a Hygroamblystegium varium subsp. varium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants extremely large; stems to 15 cm; costae 110-140 µm wide at base, excurrent; lamina sometimes partially 2-stratose, especially at base near costa.</description>
      <determination>1b Hygroamblystegium varium subsp. noterophilum</determination>
    </key_statement>
  </key>
</bio:treatment>