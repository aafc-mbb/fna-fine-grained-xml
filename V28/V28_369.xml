<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">230</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schwagrichen" date="unknown" rank="family">mniaceae</taxon_name>
    <taxon_name authority="T. J. Koponen" date="unknown" rank="genus">plagiomnium</taxon_name>
    <taxon_name authority="(Bruch &amp; Schimper) T. J. Koponen" date="1968" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>5: 146. 1968</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family mniaceae;genus plagiomnium;species drummondii</taxon_hierarchy>
    <other_info_on_name type="fna_id">200001512</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Mnium</taxon_name>
    <taxon_name authority="Bruch &amp; Schimper" date="unknown" rank="species">drummondii</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>2: 669. 1843 (as drummondi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mnium;species drummondii</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Erect stems 1–4 cm, not dendroid;</text>
      <biological_entity id="o3144" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="dendroid" value_original="dendroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>sterile stems to 7 cm.</text>
      <biological_entity id="o3145" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves light green or yellow-green, moderately contorted when dry, flat when moist, obovate or sometimes elliptic, 2–6 mm;</text>
      <biological_entity id="o3146" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="when dry" name="arrangement_or_shape" src="d0_s2" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="when moist" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>base long-decurrent;</text>
      <biological_entity id="o3147" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="long-decurrent" value_original="long-decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins toothed from about mid leaf to apex, sometimes to just below mid leaf, teeth sharp, of 1–2 (–3) cells;</text>
      <biological_entity id="o3148" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="from mid leaf" constraintid="o3149" is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="mid" id="o3149" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <biological_entity id="o3150" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o3151" name="leaf" name_original="leaf" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="sometimes to just below" name="position" src="d0_s4" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o3152" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o3153" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <relation from="o3149" id="r410" name="to" negation="false" src="d0_s4" to="o3150" />
      <relation from="o3148" id="r411" name="sometimes to just below mid" negation="false" src="d0_s4" to="o3151" />
      <relation from="o3152" id="r412" name="consist_of" negation="false" src="d0_s4" to="o3153" />
    </statement>
    <statement id="d0_s5">
      <text>apex acuminate or acute, occasionally obtuse or rounded, short to long-cuspidate, cusp toothed;</text>
      <biological_entity id="o3154" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="long-cuspidate" value_original="long-cuspidate" />
      </biological_entity>
      <biological_entity id="o3155" name="cusp" name_original="cusp" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa percurrent, excurrent, or rarely subpercurrent;</text>
      <biological_entity id="o3156" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" modifier="rarely" name="position" src="d0_s6" value="subpercurrent" value_original="subpercurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>medial laminal cells ± isodiametric or short-elongate, 30–50 (–60) µm, much smaller near margins, often less than 1/2 size, in longitudinal, not diagonal rows, not or weakly collenchymatous, walls not pitted;</text>
      <biological_entity constraint="medial laminal" id="o3157" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="isodiametric" value_original="isodiametric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-elongate" value_original="short-elongate" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s7" to="60" to_unit="um" />
        <character char_type="range_value" from="30" from_unit="um" name="some_measurement" src="d0_s7" to="50" to_unit="um" />
        <character constraint="near margins" constraintid="o3158" is_modifier="false" modifier="much" name="size" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="not; weakly" name="architecture" notes="" src="d0_s7" value="collenchymatous" value_original="collenchymatous" />
      </biological_entity>
      <biological_entity id="o3158" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" modifier="often" name="size" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity id="o3159" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s7" value="longitudinal" value_original="longitudinal" />
        <character is_modifier="true" modifier="not" name="position" src="d0_s7" value="diagonal" value_original="diagonal" />
      </biological_entity>
      <biological_entity id="o3160" name="wall" name_original="walls" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="relief" src="d0_s7" value="pitted" value_original="pitted" />
      </biological_entity>
      <relation from="o3157" id="r413" name="in" negation="false" src="d0_s7" to="o3159" />
    </statement>
    <statement id="d0_s8">
      <text>marginal cells linear, in 2–4 rows.</text>
      <biological_entity id="o3162" name="row" name_original="rows" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o3161" id="r414" name="in" negation="false" src="d0_s8" to="o3162" />
    </statement>
    <statement id="d0_s9">
      <text>Sexual condition synoicous.</text>
      <biological_entity constraint="marginal" id="o3161" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="synoicous" value_original="synoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seta 1–3 (–4), yellow to reddish-brown, 1.2–3 cm.</text>
      <biological_entity id="o3163" name="seta" name_original="seta" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="3" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s10" to="reddish-brown" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Capsule horizontal to pendent, ovoid or obovoid, 2–3 mm, neck not distinct;</text>
      <biological_entity id="o3164" name="capsule" name_original="capsule" src="d0_s11" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s11" to="pendent" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3165" name="neck" name_original="neck" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="fusion" src="d0_s11" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>operculum conic-apiculate.</text>
      <biological_entity id="o3166" name="operculum" name_original="operculum" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="conic-apiculate" value_original="conic-apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores 18–25 µm.</text>
      <biological_entity id="o3167" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" from_unit="um" name="some_measurement" src="d0_s13" to="25" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Capsules mature late spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="capsules maturing time" char_type="range_value" to="late spring" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Humus, soil, logs, rock, stumps or tree bases in fire-dependent forests/woodlands, mesic hardwood forests, swamps, wet forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="humus" />
        <character name="habitat" value="soil" />
        <character name="habitat" value="rock" modifier="logs" />
        <character name="habitat" value="rock" />
        <character name="habitat" value="stumps" />
        <character name="habitat" value="tree bases" constraint="in fire-dependent forests\/woodlands , mesic hardwood forests , swamps , wet forests" />
        <character name="habitat" value="fire-dependent forests\/woodlands" />
        <character name="habitat" value="mesic hardwood forests" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="wet forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>low elevations</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="low" from="low" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., N.S., Ont., Que., Sask.; Idaho, Md., Mich., Minn., Mont., Nebr., N.Y., N.C., Ohio, Pa., Wash., W.Va.; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>See discussion for this species under 3. Plagiomnium cuspidatum.</discussion>
  
</bio:treatment>