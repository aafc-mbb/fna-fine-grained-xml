<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John A. Christy</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="mention_page">652</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="A. L. Andrews" date="unknown" rank="genus">PLATYLOMELLA</taxon_name>
    <place_of_publication>
      <publication_title>Bryologist</publication_title>
      <place_in_publication>53: 58. 1950</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus PLATYLOMELLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus name Platyloma and Latin -ella, diminutive, alluding to replaced later homonym</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">125803</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Kindberg" date="unknown" rank="genus">Platyloma</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Eur. N.-Amer. Bryin.,</publication_title>
      <place_in_publication>22. 1897,</place_in_publication>
      <other_info_on_pub>not Rafinesque 1833 [Brassicaceae]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus platyloma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sciaromium</taxon_name>
    <taxon_name authority="Brotherus" date="unknown" rank="section">Platyloma</taxon_name>
    <taxon_hierarchy>genus sciaromium;section platyloma</taxon_hierarchy>
  </taxon_identification>
  <number>20.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants small to somewhat large, dark green to blackish with yellowish green shoot apices.</text>
      <biological_entity id="o6024" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="small to somewhat" value_original="small to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="size" src="d0_s0" value="large" value_original="large" />
        <character char_type="range_value" constraint="with shoot apices" constraintid="o6025" from="dark green" name="coloration" src="d0_s0" to="blackish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="shoot" id="o6025" name="apex" name_original="apices" src="d0_s0" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems fine, prostrate, irregularly branched or subpinnate;</text>
      <biological_entity id="o6026" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subpinnate" value_original="subpinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis absent, central strand present;</text>
      <biological_entity id="o6027" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="central" id="o6028" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>paraphyllia present, filiform;</text>
      <biological_entity id="o6029" name="paraphyllium" name_original="paraphyllia" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rhizoids basal or sometimes adventitious distally on decumbent shoots, forming tomentum, slightly branched, smooth;</text>
      <biological_entity id="o6030" name="rhizoid" name_original="rhizoids" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character constraint="on shoots" constraintid="o6031" is_modifier="false" modifier="sometimes" name="derivation" src="d0_s4" value="adventitious" value_original="adventitious" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity id="o6031" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o6032" name="tomentum" name_original="tomentum" src="d0_s4" type="structure" />
      <relation from="o6030" id="r828" name="forming" negation="false" src="d0_s4" to="o6032" />
    </statement>
    <statement id="d0_s5">
      <text>axillary hair distal cells 1 or 2, hyaline.</text>
      <biological_entity constraint="axillary" id="o6033" name="hair" name_original="hair" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o6034" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s5" unit="or" value="2" value_original="2" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Stem-leaves erect-spreading, straight or incurved, cordate-ovate to ovatelanceolate, not plicate, 0.5–1.5 mm;</text>
      <biological_entity id="o6035" name="leaf-stem" name_original="stem-leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect-spreading" value_original="erect-spreading" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="cordate-ovate" name="shape" src="d0_s6" to="ovatelanceolate" />
        <character is_modifier="false" modifier="not" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s6" value="plicate" value_original="plicate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="distance" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base not decurrent;</text>
      <biological_entity id="o6036" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins plane, serrulate distally or to base, sometimes entire, limbidia conspicuous or sometimes inconspicuous, 2–4-stratose, cells incrassate;</text>
      <biological_entity id="o6037" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s8" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="to base" value_original="to base" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6038" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o6039" name="limbidium" name_original="limbidia" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="2-4-stratose" value_original="2-4-stratose" />
      </biological_entity>
      <biological_entity id="o6040" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <relation from="o6037" id="r829" name="to" negation="false" src="d0_s8" to="o6038" />
    </statement>
    <statement id="d0_s9">
      <text>apex gradually or abruptly acuminate, sometimes apiculate, tips often obtuse;</text>
      <biological_entity id="o6041" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s9" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o6042" name="tip" name_original="tips" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>costa single, strong, percurrent to short-excurrent, usually confluent distally with limbidia;</text>
      <biological_entity id="o6044" name="limbidium" name_original="limbidia" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>alar cells undifferentiated;</text>
      <biological_entity id="o6043" name="costa" name_original="costa" src="d0_s10" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s10" value="single" value_original="single" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="strong" value_original="strong" />
        <character char_type="range_value" from="percurrent" name="architecture" src="d0_s10" to="short-excurrent" />
        <character constraint="with limbidia" constraintid="o6044" is_modifier="false" modifier="usually" name="arrangement" src="d0_s10" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o6045" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>basal laminal cells rectangular or oblong-hexagonal;</text>
      <biological_entity constraint="basal laminal" id="o6046" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-hexagonal" value_original="oblong-hexagonal" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>distal cells short-rhomboidal to oblong-hexagonal, 6–12 × 15–40 µm. Branch leaves similar, slightly smaller and narrower.</text>
      <biological_entity constraint="distal" id="o6047" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character char_type="range_value" from="short-rhomboidal" name="shape" src="d0_s13" to="oblong-hexagonal" />
        <character char_type="range_value" from="6" from_unit="um" name="length" src="d0_s13" to="12" to_unit="um" />
        <character char_type="range_value" from="15" from_unit="um" name="width" src="d0_s13" to="40" to_unit="um" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Sexual condition autoicous.</text>
      <biological_entity constraint="branch" id="o6048" name="leaf" name_original="leaves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s13" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="width" src="d0_s13" value="narrower" value_original="narrower" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="autoicous" value_original="autoicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsule horizontal to cernuous, oblong-cylindric, asymmetric, sometimes arcuate;</text>
      <biological_entity id="o6049" name="capsule" name_original="capsule" src="d0_s15" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s15" to="cernuous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong-cylindric" value_original="oblong-cylindric" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" modifier="sometimes" name="course_or_shape" src="d0_s15" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>peristome hypnoid;</text>
      <biological_entity id="o6050" name="peristome" name_original="peristome" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>exostome margins dentate;</text>
      <biological_entity constraint="exostome" id="o6051" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>endostome cilia appendiculate.</text>
      <biological_entity constraint="endostome" id="o6052" name="cilium" name_original="cilia" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="appendiculate" value_original="appendiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Spores 12–17 (–25) µm.</text>
      <biological_entity id="o6053" name="spore" name_original="spores" src="d0_s19" type="structure">
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="um" name="atypical_some_measurement" src="d0_s19" to="25" to_unit="um" />
        <character char_type="range_value" from="12" from_unit="um" name="some_measurement" src="d0_s19" to="17" to_unit="um" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Species 1.</discussion>
  <discussion>Platylomella has previously been included in Sciaromium (Mitten) Mitten, a synonym of Echinodium Juratzka (S. P. Churchill 1986).</discussion>
  <references>
    <reference>Andrews, A. L. 1945. Taxonomic notes. IV. Sciaromium lescurii. Bryologist 48: 100–103.</reference>
    <reference>Ochyra, R. 1987. A revision of the moss genus Sciaromium (Mitt.) Mitt. III. The section Platyloma Broth. J. Hattori Bot. Lab. 63: 107–132.</reference>
  </references>
  
</bio:treatment>