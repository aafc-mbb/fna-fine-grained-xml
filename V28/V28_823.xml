<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">531</other_info_on_meta>
    <other_info_on_meta type="mention_page">530</other_info_on_meta>
    <other_info_on_meta type="mention_page">559</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Schimper" date="unknown" rank="family">hypnaceae</taxon_name>
    <taxon_name authority="(Schimper) Loeske" date="unknown" rank="genus">homomallium</taxon_name>
    <taxon_name authority="Cardot" date="1910" rank="species">mexicanum</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>37: 53. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypnaceae;genus homomallium;species mexicanum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099154</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 2–2.5 cm, green or yellowish green, branches curved at apices.</text>
      <biological_entity id="o18408" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity id="o18409" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character constraint="at apices" constraintid="o18410" is_modifier="false" name="course" src="d0_s0" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o18410" name="apex" name_original="apices" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves homomallous, 0.8–1.3 (–1.5) mm;</text>
      <biological_entity id="o18411" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s1" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>margins reflexed at base, entire or serrulate at apex;</text>
      <biological_entity id="o18412" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character constraint="at base" constraintid="o18413" is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character constraint="at apex" constraintid="o18414" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o18413" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o18414" name="apex" name_original="apex" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>apex abruptly slender-acuminate, not subulate;</text>
    </statement>
    <statement id="d0_s4">
      <text>alar cells quadrate-rectangular, walls thick, region extending 1/4–1/3 leaf margin;</text>
      <biological_entity id="o18415" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s3" value="slender-acuminate" value_original="slender-acuminate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o18416" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="quadrate-rectangular" value_original="quadrate-rectangular" />
      </biological_entity>
      <biological_entity id="o18417" name="wall" name_original="walls" src="d0_s4" type="structure">
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o18418" name="region" name_original="region" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o18419" name="margin" name_original="margin" src="d0_s4" type="structure">
        <character char_type="range_value" from="1/4" is_modifier="true" name="quantity" src="d0_s4" to="1/3" />
      </biological_entity>
      <relation from="o18418" id="r2642" name="extending" negation="false" src="d0_s4" to="o18419" />
    </statement>
    <statement id="d0_s5">
      <text>distal laminal cells 25–35 × 4–7 µm, walls thick.</text>
      <biological_entity constraint="distal laminal" id="o18420" name="cell" name_original="cells" src="d0_s5" type="structure">
        <character char_type="range_value" from="25" from_unit="um" name="length" src="d0_s5" to="35" to_unit="um" />
        <character char_type="range_value" from="4" from_unit="um" name="width" src="d0_s5" to="7" to_unit="um" />
      </biological_entity>
      <biological_entity id="o18421" name="wall" name_original="walls" src="d0_s5" type="structure">
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Seta yellow-orange, 1–1.6 cm.</text>
      <biological_entity id="o18422" name="seta" name_original="seta" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow-orange" value_original="yellow-orange" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="1.6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsule yellowish to reddish, 1–1.7 mm.</text>
      <biological_entity id="o18423" name="capsule" name_original="capsule" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s7" to="reddish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spores 11–20 µm, minutely papillose.</text>
      <biological_entity id="o18424" name="spore" name_original="spores" src="d0_s8" type="structure">
        <character char_type="range_value" from="11" from_unit="um" name="some_measurement" src="d0_s8" to="20" to_unit="um" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s8" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, Mexico, Asia, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Homomallium mexicanum is a largely subtropical species that appears more robust than the other two North American species of Homomallium because of its larger and more spreading leaves.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves to 1.3 mm, not imbricate, ovate-lanceolate; apices narrowly acuminate; margins denticulate distally.</description>
      <determination>3a Homomallium mexicanum var. mexicanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves to 1.5 mm, closely imbricate, broadly ovate; apices short-acuminate; margins entire.</description>
      <determination>3b Homomallium mexicanum var. latifolium</determination>
    </key_statement>
  </key>
</bio:treatment>