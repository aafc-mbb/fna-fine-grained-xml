<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/02/17 13:31:21</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="G. Roth" date="unknown" rank="family">amblystegiaceae</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="genus">hygrohypnum</taxon_name>
    <taxon_name authority="(Lindberg) Loeske" date="1905" rank="species">polare</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Bot. Vereins Prov. Brandenburg</publication_title>
      <place_in_publication>46: 198. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family amblystegiaceae;genus hygrohypnum;species polare</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250099168</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hypnum</taxon_name>
    <taxon_name authority="Lindberg" date="unknown" rank="species">polare</taxon_name>
    <place_of_publication>
      <publication_title>Öfvers. Kongl. Vetensk.-Akad. Förh.</publication_title>
      <place_in_publication>23: 540. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species polare</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calliergon</taxon_name>
    <taxon_name authority="(Lindberg) Kindberg" date="unknown" rank="species">polare</taxon_name>
    <taxon_hierarchy>genus calliergon;species polare</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hygrohypnella</taxon_name>
    <taxon_name authority="(Lindberg) Ignatov &amp; Ignatova" date="unknown" rank="species">polaris</taxon_name>
    <taxon_hierarchy>genus hygrohypnella;species polaris</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hygrohypnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustre</taxon_name>
    <taxon_name authority="(Arnell) Grout" date="unknown" rank="variety">ehlei</taxon_name>
    <taxon_hierarchy>genus hygrohypnum;species palustre;variety ehlei</taxon_hierarchy>
  </taxon_identification>
  <number>13.</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants soft, golden yellow, yellowish green, or rarely bright green.</text>
      <biological_entity id="o21597" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s0" value="soft" value_original="soft" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s0" value="bright green" value_original="bright green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems to 13 cm, not denuded basally, unbranched or irregularly branched from base;</text>
      <biological_entity id="o21598" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="13" to_unit="cm" />
        <character is_modifier="false" modifier="not; basally" name="pubescence" src="d0_s1" value="denuded" value_original="denuded" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character constraint="from base" constraintid="o21599" is_modifier="false" modifier="irregularly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o21599" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>hyalodermis incomplete, as outer row of thin-walled cortical cells, evident in older stems only as thickened inner concave walls, central strand well developed.</text>
      <biological_entity id="o21600" name="hyalodermi" name_original="hyalodermis" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="incomplete" value_original="incomplete" />
        <character constraint="in stems" constraintid="o21604" is_modifier="false" name="prominence" notes="" src="d0_s2" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21601" name="row" name_original="row" src="d0_s2" type="structure" />
      <biological_entity id="o21602" name="cortical" name_original="cortical" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o21603" name="cell" name_original="cells" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="thin-walled" value_original="thin-walled" />
      </biological_entity>
      <biological_entity id="o21604" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
      </biological_entity>
      <biological_entity constraint="inner" id="o21605" name="wall" name_original="walls" src="d0_s2" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="true" name="shape" src="d0_s2" value="concave" value_original="concave" />
      </biological_entity>
      <biological_entity constraint="central" id="o21606" name="strand" name_original="strand" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <relation from="o21600" id="r3093" name="as" negation="false" src="d0_s2" to="o21601" />
      <relation from="o21601" id="r3094" name="part_of" negation="false" src="d0_s2" to="o21602" />
      <relation from="o21601" id="r3095" name="part_of" negation="false" src="d0_s2" to="o21603" />
      <relation from="o21604" id="r3096" name="as" negation="false" src="d0_s2" to="o21605" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves appressed-imbricate to loosely spreading, straight or falcate, appressed when dry, spreading when moist, ovatelanceolate, oblong-lanceolate, elliptic or broadly so, shallowly to deeply concave, rarely almost plane, (0.9–) 1.1–1.6 (–2.1) × (0.6–) 0.8–1 (–1.1) mm;</text>
      <biological_entity id="o21607" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="appressed-imbricate" value_original="appressed-imbricate" />
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s3" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="when dry" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="when moist" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character name="arrangement_or_shape" src="d0_s3" value="broadly" value_original="broadly" />
        <character is_modifier="false" modifier="shallowly to deeply" name="shape" src="d0_s3" value="concave" value_original="concave" />
        <character is_modifier="false" modifier="rarely almost" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_length" src="d0_s3" to="1.1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s3" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_width" src="d0_s3" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins plane, sometimes slightly involute near apex, involute in falcate leaves, rendering leaf tubular distally, entire;</text>
      <biological_entity id="o21608" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="plane" value_original="plane" />
        <character constraint="near apex" constraintid="o21609" is_modifier="false" modifier="sometimes slightly" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
        <character constraint="in leaves" constraintid="o21610" is_modifier="false" name="shape_or_vernation" notes="" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s4" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21609" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity id="o21610" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity id="o21611" name="leaf" name_original="leaf" src="d0_s4" type="structure" />
      <relation from="o21608" id="r3097" name="rendering" negation="false" src="d0_s4" to="o21611" />
    </statement>
    <statement id="d0_s5">
      <text>apex tapered to acute or apiculate point, or deeply concave-cucullate with often recurved apiculus;</text>
      <biological_entity id="o21612" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="tapered" name="shape" src="d0_s5" to="acute or apiculate" />
        <character constraint="with apiculus" constraintid="o21614" is_modifier="false" modifier="deeply" name="shape" notes="" src="d0_s5" value="concave-cucullate" value_original="concave-cucullate" />
      </biological_entity>
      <biological_entity id="o21613" name="point" name_original="point" src="d0_s5" type="structure" />
      <biological_entity id="o21614" name="apiculu" name_original="apiculus" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>costa single, ending just below apex or percurrent in apiculus, very stout, rarely 2-fid;</text>
      <biological_entity id="o21616" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o21617" name="apiculu" name_original="apiculus" src="d0_s6" type="structure" />
      <relation from="o21615" id="r3098" modifier="just below" name="ending" negation="false" src="d0_s6" to="o21616" />
    </statement>
    <statement id="d0_s7">
      <text>alar cells many, quadrate or short-rectangular, region well defined;</text>
      <biological_entity id="o21615" name="costa" name_original="costa" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="single" value_original="single" />
        <character constraint="in apiculus" constraintid="o21617" is_modifier="false" name="architecture" src="d0_s6" value="percurrent" value_original="percurrent" />
        <character is_modifier="false" modifier="very" name="fragility_or_size" notes="" src="d0_s6" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="2-fid" value_original="2-fid" />
      </biological_entity>
      <biological_entity id="o21618" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character is_modifier="false" name="shape" src="d0_s7" value="quadrate" value_original="quadrate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="short-rectangular" value_original="short-rectangular" />
      </biological_entity>
      <biological_entity id="o21619" name="region" name_original="region" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s7" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>basal laminal cells shorter, wider than medial cells, walls incrassate, yellowing with age;</text>
      <biological_entity constraint="basal laminal" id="o21620" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character constraint="than medial cells" constraintid="o21621" is_modifier="false" name="width" src="d0_s8" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="medial" id="o21621" name="cell" name_original="cells" src="d0_s8" type="structure" />
      <biological_entity id="o21622" name="wall" name_original="walls" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="incrassate" value_original="incrassate" />
      </biological_entity>
      <biological_entity id="o21623" name="age" name_original="age" src="d0_s8" type="structure" />
      <relation from="o21622" id="r3099" name="yellowing with" negation="false" src="d0_s8" to="o21623" />
    </statement>
    <statement id="d0_s9">
      <text>medial cells fusiform to long linear-flexuose, (33–) 40–50 (–65) × 5–6 (–8) µm; apical cells shorter, usually rhomboid;</text>
      <biological_entity constraint="medial" id="o21624" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="false" name="course" src="d0_s9" value="linear-flexuose" value_original="linear-flexuose" />
        <character char_type="range_value" from="33" from_unit="um" name="atypical_length" src="d0_s9" to="40" to_inclusive="false" to_unit="um" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="um" name="atypical_length" src="d0_s9" to="65" to_unit="um" />
        <character char_type="range_value" from="40" from_unit="um" name="length" src="d0_s9" to="50" to_unit="um" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="um" name="atypical_width" src="d0_s9" to="8" to_unit="um" />
        <character char_type="range_value" from="5" from_unit="um" name="width" src="d0_s9" to="6" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="apical" id="o21625" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="rhomboid" value_original="rhomboid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>marginal cells somewhat shorter.</text>
    </statement>
    <statement id="d0_s11">
      <text>Sexual condition dioicous;</text>
      <biological_entity constraint="marginal" id="o21626" name="cell" name_original="cells" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sexual" value_original="sexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="dioicous" value_original="dioicous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perichaetial inner leaves long-lanceolate, plicate, apex gradually tapering, frayed with age, costa single.</text>
      <biological_entity constraint="perichaetial inner" id="o21627" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="long-lanceolate" value_original="long-lanceolate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_shape_or_vernation" src="d0_s12" value="plicate" value_original="plicate" />
      </biological_entity>
      <biological_entity id="o21628" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
        <character constraint="with age" constraintid="o21629" is_modifier="false" name="shape" src="d0_s12" value="frayed" value_original="frayed" />
      </biological_entity>
      <biological_entity id="o21629" name="age" name_original="age" src="d0_s12" type="structure" />
      <biological_entity id="o21630" name="costa" name_original="costa" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="single" value_original="single" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seta yellowbrown, 1–1.2 cm.</text>
      <biological_entity id="o21631" name="seta" name_original="seta" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellowbrown" value_original="yellowbrown" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s13" to="1.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsule with peristome unknown.</text>
      <biological_entity id="o21632" name="capsule" name_original="capsule" src="d0_s14" type="structure" />
      <biological_entity id="o21633" name="peristome" name_original="peristome" src="d0_s14" type="structure" />
      <relation from="o21632" id="r3100" name="with" negation="false" src="d0_s14" to="o21633" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acidic rock in montane or high latitude streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acidic rock" constraint="in montane or high latitude" />
        <character name="habitat" value="montane" />
        <character name="habitat" value="high latitude" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>moderate elevations (900-1400 m)</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="moderate" from="moderate" />
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="900" from_unit="m" constraint="moderate elevations " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Nfld. and Labr. (Labr.), Nunavut, Yukon; Alaska; Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hygrohypnum polare is easily recognized by its strong, single costa and outer layer of thin-walled cortical cells in the stem. The quadrate alar cells and the usual leaf concavity provide confirmation. The plants grow in appressed turfs or loose patches that may be prostrate, ascending, or erect; the branches are usually fastigiate; and the alar cells have thin hyaline walls that become brown with age.</discussion>
  
</bio:treatment>